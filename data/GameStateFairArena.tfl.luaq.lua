local GameStateFairArena = GameStateManager.GameStateFairArena
local GameUIFairArenaMain = LuaObjectManager:GetLuaObject("GameUIFairArenaMain")
local GameUIFairArenaEnter = LuaObjectManager:GetLuaObject("GameUIFairArenaEnter")
local GameUIFairArenaRank = LuaObjectManager:GetLuaObject("GameUIFairArenaRank")
local GameUIFairArenaFormation = LuaObjectManager:GetLuaObject("GameUIFairArenaFormation")
local GameObjectDragger = LuaObjectManager:GetLuaObject("GameObjectDragger")
local GameObjectDraggerItem = GameObjectDragger:NewInstance("common_avatars.tfs", false)
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
GameStateFairArena.baseData = nil
GameStateFairArena.beforeState = nil
GameStateFairArena.MainState = 1
GameStateFairArena.RankState = 2
GameStateFairArena.SelectState = 3
GameStateFairArena.curState = 1
GameStateFairArena.dragItemOffset = -70
function GameObjectDraggerItem:OnAddToGameState(state)
  local head_frame = ""
  local rank_frame = ""
  local vessels_frame = ""
  local commanderAbility = GameDataAccessHelper:GetCommanderAbility(self.fleet_id, self.fleet_level)
  if commanderAbility then
    if self.fleet_id == 1 then
      head_frame = GameGlobalData:GetUserInfo().sex == 1 and "male" or "female"
    else
      head_frame = GameDataAccessHelper:GetFleetAvatar(self.fleet_id, self.fleet_level, self.fleet_sex)
    end
    rank_frame = GameDataAccessHelper:GetFleetRankImageFrame(commanderAbility.Rank)
    vessels_frame = "TYPE_" .. tostring(commanderAbility.vessels)
  end
  self:GetFlashObject():InvokeASCallback("_root", "SetAvatarItem", head_frame, vessels_frame, rank_frame)
  self:GetFlashObject():InvokeASCallback("_root", "ChangeToBigger")
end
function GameObjectDraggerItem:OnEraseFromGameState(state)
  self.fleet_id = -1
  self.dragOnFormation = false
  self.formationIdx = nil
end
function GameObjectDraggerItem:OnReachedDestPos()
  DebugOut("GameObjectDraggerItem:OnReachedDestPos", self.fleet_id, self.formationIdx, self.dragFleetItem, self.isHero, self.notChangeFormation, self.unSelectFleet)
  if self.fleet_id and self.fleet_id ~= -1 and self.formationIdx and self.formationIdx > 0 then
    if self.dragFleetItem then
      if self.isHero then
        GameUIFairArenaFormation:SelectedHero(self.fleet_id, self.fleet_level, self.fleet_sex, self.fleet_vessels, self.formationIdx)
      else
        GameUIFairArenaFormation:SelectedAdjutant(self.fleet_id, self.fleet_level, self.fleet_sex, self.formationIdx)
      end
    elseif self.isHero then
      GameUIFairArenaFormation:ChangedHero(self.fleet_id, self.fleet_level, self.formationIdx)
    else
      GameUIFairArenaFormation:SelectedAdjutant(self.fleet_id, self.fleet_level, self.fleet_sex, self.formationIdx)
    end
  end
  if not self.dragFleetItem then
    if self.notChangeFormation then
      GameUIFairArenaFormation:NotChangeFormation()
      self.notChangeFormation = nil
    end
    if self.unSelectFleet then
      GameUIFairArenaFormation:UnSelectFleet()
      self.unSelectFleet = nil
    end
  end
  GameStateFairArena:onEndDragItem()
end
function GameStateFairArena:onEndDragItem()
  GameUIFairArenaFormation:onEndDragItem()
  GameStateFairArena:EraseObject(GameObjectDraggerItem)
end
function GameStateFairArena:BeginDragItem(isHero, dragFleetItem, fleet_id, fleet_level, fleet_sex, fleet_vessels, initPosX, initPosY, posX, posY)
  DebugOut("BeginDragItem: ", isHero, fleet_id, fleet_level, fleet_sex, initPosX, initPosY, posX, posY)
  GameObjectDraggerItem.fleet_id = fleet_id
  GameObjectDraggerItem.fleet_level = fleet_level
  GameObjectDraggerItem.fleet_sex = fleet_sex
  GameObjectDraggerItem.isHero = isHero
  GameObjectDraggerItem.fleet_vessels = fleet_vessels
  GameObjectDraggerItem.dragFleetItem = dragFleetItem
  GameObjectDraggerItem:SetDestPosition(initPosX, initPosY)
  GameObjectDraggerItem:BeginDrag(tonumber(self.dragItemOffset), tonumber(self.dragItemOffset), tonumber(posX), tonumber(posY))
  GameStateManager:GetCurrentGameState():AddObject(GameObjectDraggerItem)
  GameUIFairArenaFormation:onBeginDragItem(GameObjectDraggerItem)
end
function GameStateFairArena:OnTouchPressed(x, y)
  if GameStateFairArena:IsObjectInState(GameObjectDraggerItem) and GameObjectDraggerItem:IsAutoMove() or GameUIGlobalScreen.IsShowingLoading then
    return
  end
  GameStateBase.OnTouchPressed(self, x, y)
end
function GameStateFairArena:OnTouchMoved(x, y)
  if GameStateFairArena:IsObjectInState(GameObjectDraggerItem) and GameObjectDraggerItem:IsAutoMove() or GameUIGlobalScreen.IsShowingLoading then
    return
  end
  GameStateBase.OnTouchMoved(self, x, y)
  local fleet_id = GameObjectDraggerItem.fleet_id
  if fleet_id and fleet_id ~= -1 then
    GameObjectDraggerItem:UpdateDrag()
  end
end
function GameStateFairArena:OnTouchReleased(x, y)
  if GameStateFairArena:IsObjectInState(GameObjectDraggerItem) and GameObjectDraggerItem:IsAutoMove() or GameUIGlobalScreen.IsShowingLoading then
    return
  end
  GameStateBase.OnTouchReleased(self, x, y)
  local fleet_id = GameObjectDraggerItem.fleet_id
  if fleet_id and fleet_id ~= -1 then
    GameStateFairArena:DragFleetRelease()
    if not GameObjectDraggerItem.dragOnFormation then
      GameObjectDraggerItem.fleet_id = -1
    end
    if GameObjectDraggerItem.dragOnFormation then
      GameObjectDraggerItem:OnReachedDestPos()
    else
      GameObjectDraggerItem:StartAutoMove()
    end
  end
end
function GameStateFairArena:DragFleetRelease()
  local posInfo = ""
  GameObjectDraggerItem.formationIdx = tonumber(GameUIFairArenaFormation:GetFlashObject():InvokeASCallback("_root", "IsOnFormation"))
  if GameObjectDraggerItem.formationIdx and GameObjectDraggerItem.formationIdx > 0 then
    GameObjectDraggerItem.dragOnFormation = true
    posInfo = GameUIFairArenaFormation:GetFlashObject():InvokeASCallback("_root", "GetFormationPos", GameObjectDraggerItem.formationIdx)
  end
  DebugOut("DragFleetRelease: QJ", GameObjectDraggerItem.formationIdx, posInfo)
  if GameObjectDraggerItem.dragOnFormation then
    local ptPos = LuaUtils:string_split(posInfo, "\001")
    GameObjectDraggerItem:SetDestPosition(tonumber(ptPos[1]), tonumber(ptPos[2]))
  end
  if not GameObjectDraggerItem.dragFleetItem and not GameObjectDraggerItem.dragOnFormation then
    posInfo = GameUIFairArenaFormation:GetFlashObject():InvokeASCallback("_root", "IsOnFleetPool")
    DebugOut("DragFleetRelease :", posInfo)
    if posInfo ~= "-1" then
      if GameObjectDraggerItem.fleet_vessels == 6 then
        GameTip:Show(GameLoader:GetGameText("LC_MENU_LOADING_HINT_9"), 3000)
        GameObjectDraggerItem.notChangeFormation = true
      else
        local param = LuaUtils:string_split(posInfo, "\001")
        GameObjectDraggerItem:SetDestPosition(tonumber(param[1]), tonumber(param[2]))
        GameObjectDraggerItem.unSelectFleet = true
      end
    else
      GameObjectDraggerItem.notChangeFormation = true
    end
  end
end
function GameStateFairArena:InitGameState()
end
function GameStateFairArena:ExitState()
  if GameStateFairArena.beforeState and GameStateFairArena.beforeState ~= GameStateManager.GameStateBattlePlay then
    GameStateManager:SetCurrentGameState(GameStateFairArena.beforeState)
    GameStateFairArena.beforeState = nil
  else
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateLab)
  end
end
function GameStateFairArena:OnFocusGain(previousState)
  DebugOut("GameStateFairArena:OnFocusGain")
  GameStateFairArena.beforeState = previousState
  self:AddObject(GameUIFairArenaMain)
  if GameStateFairArena.curState == GameStateFairArena.MainState then
    self:AddObject(GameUIFairArenaEnter)
  end
  GameObjectDraggerItem:Init()
  GameObjectDraggerItem:LoadFlashObject()
end
function GameStateFairArena:SwitchState(toState, param, param2)
  if GameStateFairArena.curState == GameStateFairArena.MainState then
    self:EraseObject(GameUIFairArenaEnter)
  elseif GameStateFairArena.curState == GameStateFairArena.RankState then
    self:EraseObject(GameUIFairArenaRank)
  elseif GameStateFairArena.curState == GameStateFairArena.SelectState then
    self:EraseObject(GameUIFairArenaFormation)
  end
  if toState == GameStateFairArena.MainState then
    self:AddObject(GameUIFairArenaEnter)
  elseif toState == GameStateFairArena.RankState then
    GameUIFairArenaRank:Show(param, param2)
  elseif toState == GameStateFairArena.SelectState then
    self:AddObject(GameUIFairArenaFormation)
  end
  GameStateFairArena.curState = toState
end
function GameStateFairArena:OnFocusLost(newState)
  GameStateFairArena.curState = GameStateFairArena.MainState
  self:EraseObject(GameUIFairArenaMain)
  self:EraseObject(GameUIFairArenaEnter)
  self:EraseObject(GameUIFairArenaRank)
  GameObjectDraggerItem.m_renderFx = nil
end
