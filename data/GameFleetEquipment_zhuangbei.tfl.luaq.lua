local GameFleetEquipment = LuaObjectManager:GetLuaObject("GameFleetEquipment")
GameFleetEquipment.zhuangbei = {}
local zhuangbei = GameFleetEquipment.zhuangbei
local QuestTutorialEquip = TutorialQuestManager.QuestTutorialEquip
local QuestTutorialEnhance = TutorialQuestManager.QuestTutorialEnhance
local QuestTutorialEnhance_second = TutorialQuestManager.QuestTutorialEnhance_second
local QuestTutorialEquipAllByOneKey = TutorialQuestManager.QuestTutorialEquipAllByOneKey
local QuestTutorialsChangeFleets1 = TutorialQuestManager.QuestTutorialsChangeFleets1
local QuestTutorialEnhance_third = TutorialQuestManager.QuestTutorialEnhance_third
local QuestTutorialFirstReform = TutorialQuestManager.QuestTutorialFirstReform
local QuestTutorialBuildAcademy = TutorialQuestManager.QuestTutorialBuildAcademy
local QuestTutorialBattleMap = TutorialQuestManager.QuestTutorialBattleMap
local QuestTutorialEquipmentEvolution = TutorialQuestManager.QuestTutorialEquipmentEvolution
local QuestTutorialBattleMapAfterRecruit = TutorialQuestManager.QuestTutorialBattleMapAfterRecruit
local GameItemBag = LuaObjectManager:GetLuaObject("GameItemBag")
local GameStateEquipEnhance = GameStateManager.GameStateEquipEnhance
local GameFleetEquipmentPop = LuaObjectManager:GetLuaObject("GameFleetEquipmentPop")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameVip = LuaObjectManager:GetLuaObject("GameVip")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUICollect = LuaObjectManager:GetLuaObject("GameUICollect")
local k_equipmentInfoTypeLoad = 1
local k_equipmentInfoTypeUnload = 2
local k_equipmentInfoTypeBuy = 3
local k_equipmentInfoTypeStrengthen = 4
local k_maxSlot = 5
zhuangbei.fleetid = nil
zhuangbei.LastStrengthenItem = nil
zhuangbei.all_need_equip = {}
local BAG_OWNER_USER = 1
local BAG_OWNER_FLEET = 2
function zhuangbei:Init(fleetInfo, fleetidx, needshow)
  zhuangbei.fleetid = fleetInfo.id
  zhuangbei.fleetidentity = fleetInfo.identity
  zhuangbei.m_currentSelectFleetIndex = fleetidx
  zhuangbei.lastid_equipment_detail = nil
  zhuangbei.curMatrixIndex = GameFleetEquipment:GetCurMatrixId()
  if not GameFleetEquipment.isTeamLeague then
    do
      local bag_req_content = {
        owner = zhuangbei.fleetid,
        bag_type = BAG_OWNER_FLEET,
        pos = -1
      }
      local function netFailedCallback()
        NetMessageMgr:SendMsg(NetAPIList.bag_req.Code, bag_req_content, self.DownloadCurrentFleetEquipment, false, netFailedCallback)
      end
      NetMessageMgr:SendMsg(NetAPIList.bag_req.Code, bag_req_content, self.DownloadCurrentFleetEquipment, false, netFailedCallback)
    end
  else
    do
      local tlc_bag_req_content = {
        owner = zhuangbei.fleetid,
        matrix = GameFleetEquipment:GetCurMatrixId(),
        bag_type = BAG_OWNER_FLEET,
        pos = -1
      }
      local function netFailedCallback()
        NetMessageMgr:SendMsg(NetAPIList.tlc_bag_req.Code, tlc_bag_req_content, self.tlc_DownloadCurrentFleetEquipment, false, netFailedCallback)
      end
      NetMessageMgr:SendMsg(NetAPIList.tlc_bag_req.Code, tlc_bag_req_content, self.tlc_DownloadCurrentFleetEquipment, false, netFailedCallback)
    end
  end
  if needshow then
    zhuangbei:Show()
  end
end
function zhuangbei:OnEraseFromGameState()
  GameGlobalData:RemoveDataChangeCallback("equipments", zhuangbei.OnGlobalEquipmentsChange)
  GameGlobalData:RemoveDataChangeCallback("fleetinfo", zhuangbei.OnGlobalEquipmentsChange)
  GameGlobalData:RemoveDataChangeCallback("fleetinfo", zhuangbei.OnEnhanceFleetinfoChange)
end
function zhuangbei:Show()
  if not self:GetFlashObject() then
    return
  end
  self:GetFlashObject():InvokeASCallback("_root", "InitZhuangbei")
  GameGlobalData:RemoveDataChangeCallback("equipments", zhuangbei.OnGlobalEquipmentsChange)
  GameGlobalData:RemoveDataChangeCallback("fleetinfo", zhuangbei.OnGlobalEquipmentsChange)
  GameGlobalData:RegisterDataChangeCallback("equipments", zhuangbei.OnGlobalEquipmentsChange)
  GameGlobalData:RegisterDataChangeCallback("fleetinfo", zhuangbei.OnEnhanceFleetinfoChange)
end
function zhuangbei:Hide()
end
function zhuangbei:OnFSCommand(cmd, arg)
  if cmd == "EquipmentSelect" then
    self:OnEquipSelect(tonumber(arg), true)
    return true
  elseif cmd == "showitemItembox" then
    self:showEnhanceItemItemBox(arg)
  elseif cmd == "gotoBtnClick" then
    DebugOut("currentShowBluePrint", currentShowBluePrint)
    DebugTable(GameData.Item.Keys[currentShowBluePrint])
    local level = GameData.Item.Keys[currentShowBluePrint].REQ_LEVEL
    local GameObjectAdventure = LuaObjectManager:GetLuaObject("GameObjectAdventure")
    if immanentversion170 == 4 or immanentversion170 == 5 then
      GameObjectAdventure:gotoBossAreaDirectly170(currentShowBluePrint)
    else
      GameObjectAdventure:gotoBossAreaDirectly(level)
    end
    return true
  end
  return false
end
function zhuangbei:GetFlashObject()
  return (...), GameFleetEquipment
end
function zhuangbei.refreshFleets()
  DebugOut("failed!!!!!!!!!!")
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameFleetEquipment) then
    zhuangbei:UpdateSelectedFleetInfo()
  end
end
function zhuangbei.OnGlobalEquipmentsChange()
  DebugOut("NTF On: OnGlobalEquipmentsChange")
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameFleetEquipment) then
    zhuangbei:FleetSelect(zhuangbei.m_currentSelectFleetIndex, true)
  end
end
function zhuangbei.DownloadCurrentFleetEquipment(msgType, content)
  if msgType == NetAPIList.bag_ack.Code then
    DebugOut("DownloadCurrentFleetEquipment:", zhuangbei.m_subState)
    DebugOutPutTable(content, "currentFleetEquipment")
    if not GameStateManager:GetCurrentGameState():IsObjectInState(GameFleetEquipment) then
      return true
    end
    zhuangbei.m_curFleetEquipments = zhuangbei:processFleetEquipments(content.grids)
    GameItemBag.equipedItem = GameItemBag:processEquipedItem(content.equipedItems)
    zhuangbei:RefreshCurFleetEquipment()
    zhuangbei:lockOrUnlockEvolutionBtnAccordUseable()
    zhuangbei:lockOrUnlockQuickEnhanceBtnAccordUseable()
    DebugOut("RefreshEquipmentInfo in DownloadCurrentFleetEquipment")
    zhuangbei:RefreshEquipmentInfo()
    zhuangbei:CheckFinishWanaAndSilvaTutorial()
    if GameFleetEquipmentPop:GetFlashObject() then
      local isvis = GameStateEquipEnhance:GetPop():zb_isPopListVisible()
      if isvis then
        zhuangbei:OnEquipSelect(zhuangbei.m_currentSelectEquipSlot, false)
      end
    end
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.bag_req.Code then
    if not GameStateManager:GetCurrentGameState():IsObjectInState(GameFleetEquipment) then
      return true
    end
    if content.api == NetAPIList.bag_req.Code and content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function zhuangbei.tlc_DownloadCurrentFleetEquipment(msgType, content)
  if msgType == NetAPIList.tlc_bag_ack.Code then
    DebugOut("tlc_DownloadCurrentFleetEquipment:", zhuangbei.m_subState)
    DebugOutPutTable(content, "currentFleetEquipment")
    if not GameStateManager:GetCurrentGameState():IsObjectInState(GameFleetEquipment) then
      return true
    end
    zhuangbei.m_curFleetEquipments = zhuangbei:processFleetEquipments(content.grids)
    GameItemBag.equipedItem = GameItemBag:processEquipedItem(content.equipedItems)
    zhuangbei:RefreshCurFleetEquipment()
    zhuangbei:lockOrUnlockEvolutionBtnAccordUseable()
    zhuangbei:lockOrUnlockQuickEnhanceBtnAccordUseable()
    zhuangbei:RefreshEquipmentInfo()
    zhuangbei:CheckFinishWanaAndSilvaTutorial()
    if GameFleetEquipmentPop:GetFlashObject() then
      local isvis = GameStateEquipEnhance:GetPop():zb_isPopListVisible()
      if isvis then
        zhuangbei:OnEquipSelect(zhuangbei.m_currentSelectEquipSlot, false)
      end
    end
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.tlc_bag_req.Code then
    if not GameStateManager:GetCurrentGameState():IsObjectInState(GameFleetEquipment) then
      return true
    end
    if content.api == NetAPIList.tlc_bag_req.Code and content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function zhuangbei:showEnhanceItemItemBox(arg, item)
  local param = LuaUtils:string_split(arg, "\001")
  local butText = ""
  local item_type = zhuangbei.recipe_equip_type
  ItemBox:showItemBox("Item", item, item_type, tonumber(param[1]), tonumber(param[2]), tonumber(param[3]), tonumber(param[4]), "", nil, "", nil)
  ItemBox:showGotoBtn()
  currentShowBluePrint = item_type
end
function zhuangbei:AllEquipAction()
  DebugOut("AllEquipAction", #self.all_need_equip)
  DebugTable(self.all_need_equip)
  if #self.all_need_equip > 1 then
    return
  end
  local currentFleetEquipMentTemp = {}
  local currentGameItemBagTemp = {}
  local equipments = GameGlobalData:GetData("equipments")
  for k, v in pairs(equipments) do
    for k1, v1 in pairs(self.m_curFleetEquipments) do
      if v1.item_id == v.equip_id then
        table.insert(currentFleetEquipMentTemp, v)
      end
    end
    for k2, v2 in pairs(self.onlyEquip) do
      if v2.item_id == v.equip_id then
        v.pos = v2.pos
        table.insert(currentGameItemBagTemp, v)
      end
    end
  end
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleet = fleets[self.m_currentSelectFleetIndex]
  local fleetVessels = GameDataAccessHelper:GetCommanderVessels(fleet.identity)
  local isEnFleet = false
  if tonumber(fleetVessels) == 4 then
    isEnFleet = true
  end
  self.all_need_equip = {}
  local tmp_table = {}
  for i = 1, 5 do
    if i == 1 then
      tmp_table = {}
      for k, v in pairs(currentGameItemBagTemp) do
        if v.equip_slot == i then
          if isEnFleet and tonumber(string.sub(v.equip_type, -1)) == 6 then
            table.insert(tmp_table, v)
          elseif not isEnFleet and tonumber(string.sub(v.equip_type, -1)) ~= 6 then
            table.insert(tmp_table, v)
          end
        end
      end
    else
      tmp_table = {}
      for k, v in pairs(currentGameItemBagTemp) do
        if v.equip_slot == i then
          table.insert(tmp_table, v)
        end
      end
    end
    DebugOut("-------------------------------------" .. i .. "   --------")
    DebugTable(tmp_table)
    if #tmp_table > 0 then
      table.sort(tmp_table, function(v1, v2)
        if v1.equip_type == v2.equip_type then
          return v1.equip_level > v2.equip_level
        else
          return v1.equip_type > v2.equip_type
        end
      end)
      local canReplaceEquip = true
      for k, v in pairs(currentFleetEquipMentTemp) do
        if v.equip_slot == i then
          if v.equip_type == tmp_table[1].equip_type and v.equip_level >= tmp_table[1].equip_level then
            canReplaceEquip = false
          elseif v.equip_type > tmp_table[1].equip_type then
            canReplaceEquip = false
          end
        end
      end
      if canReplaceEquip then
        table.insert(self.all_need_equip, tmp_table[1])
      end
    end
  end
  DebugOut("all_need_equip", #self.all_need_equip)
  if #self.all_need_equip > 0 then
    self:ReplaceAllEquipmentByOneKey()
  elseif QuestTutorialEquipAllByOneKey:IsActive() then
    self:GetFlashObject():InvokeASCallback("_root", "SetShowTipClose", true)
    QuestTutorialEquipAllByOneKey:SetFinish(true)
  end
end
function zhuangbei:ReplaceAllEquipmentByOneKey()
  DebugOut("ReplaceAllEquipmentByOneKey")
  DebugTable(self.all_need_equip)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local function netFailedCallback()
    zhuangbei.all_need_equip = {}
    GameItemBag:RequestBag()
  end
  waitForRefreshCount = 0
  startQuickEquipment = false
  while #zhuangbei.all_need_equip > 0 do
    local swap_bag_grid_req_content = {
      matrix_id = zhuangbei.curMatrixIndex,
      orig_grid_owner = GameGlobalData:GetData("userinfo").player_id,
      oirg_grid_type = BAG_OWNER_USER,
      oirg_grid_pos = zhuangbei.all_need_equip[1].pos,
      target_grid_owner = fleets[zhuangbei.m_currentSelectFleetIndex].id,
      target_grid_type = BAG_OWNER_FLEET,
      target_grid_pos = zhuangbei.all_need_equip[1].equip_slot
    }
    table.remove(zhuangbei.all_need_equip, 1)
    NetMessageMgr:SendMsg(NetAPIList.swap_bag_grid_req.Code, swap_bag_grid_req_content, zhuangbei.EquipCallback, true, netFailedCallback)
    waitForRefreshCount = waitForRefreshCount + 1
    startQuickEquipment = true
  end
end
function zhuangbei:IsInterveneEnable()
  local reqLevel = GameDataAccessHelper:GetRightMenuPlayerReqLevel("enchant")
  local levelinfo = GameGlobalData:GetData("levelinfo")
  return reqLevel <= levelinfo.level
end
function zhuangbei:RefreshEquipStatus(idx)
  local tparm = {}
  local equip = self:IsEquipmentInSlot(idx)
  local currentEquipInfo
  if equip then
    tparm.enable_enhance = true
    tparm.enable_evolution = true
    tparm.enable_intervene = true
    tparm.lock_enhance = false
    tparm.lock_intervene = false
    tparm.lock_evolution = false
    if not GameStateEquipEnhance.IsEnhanceEnable() then
      tparm.lock_enhance = true
    end
    if not self:IsInterveneEnable() then
      tparm.lock_intervene = true
    end
    if not self:isEvolutionUseable() then
      tparm.lock_evolution = true
    end
  else
    tparm.enable_enhance = false
    tparm.enable_evolution = false
    tparm.enable_intervene = false
    tparm.lock_enhance = false
    tparm.lock_intervene = false
    tparm.lock_evolution = false
    tparm.visible_new_enhance = false
    tparm.visible_new_evolution = false
    tparm.visible_new_intervene = false
  end
  tparm.str_enhance = GameLoader:GetGameText("LC_MENU_ENHANCE_BUTTON")
  tparm.str_evolution = GameLoader:GetGameText("LC_MENU_EVOLUTION_BUTTON")
  tparm.str_intervene = GameLoader:GetGameText("LC_MENU_INTERFERENCE_INTERFERE_BUTTON")
  local pop = GameStateEquipEnhance:GetPop()
  pop:zb_setEquipStatus(tparm)
end
function zhuangbei:prepareEquipInfoForFlash(item_id, tobuy)
  local t = {}
  local showInfo
  if tobuy then
    showInfo = zhuangbei:GetShowEquipmentInfoObject(nil, k_equipmentInfoTypeBuy, nil)
  else
    local equipments = GameGlobalData:GetData("equipments")
    DebugOut("prepareEquipInfoForFlash")
    DebugTable(equipments)
    local itemInBag
    for k, v in pairs(equipments) do
      if item_id == v.equip_id then
        itemInBag = v
        break
      end
    end
    showInfo = zhuangbei:GetShowEquipmentInfoObject(itemInBag, k_equipmentInfoTypeLoad, nil)
  end
  t.is_not_equip = false
  t.icon_frame = showInfo.Frame
  t.icon_strcnt = GameLoader:GetGameText("LC_MENU_Level") .. showInfo.level
  t.icon_nstrenghlv = showInfo.StrengthenLv
  t.equipName = showInfo.itemName
  t.strid = "" .. item_id
  t.tpropinfo = {}
  local allvalue = 0
  for k, v in pairs(showInfo.params) do
    local tt = {}
    tt.paramColor = v.COLOR
    tt.paramType = v.TYPETEXT
    tt.paramValue = v.VALUETEXT
    if v.NVALUE then
      allvalue = allvalue + v.NVALUE
    end
    tt.isvis = true
    table.insert(t.tpropinfo, tt)
  end
  t.allvalue = allvalue
  if #t.tpropinfo == 3 then
    t.tpropinfo[4] = t.tpropinfo[3]
    t.tpropinfo[3] = t.tpropinfo[2]
    t.tpropinfo[2] = {isvis = false}
  end
  return t
end
function zhuangbei:OnEquipSelect(idx, showpop)
  if zhuangbei.m_currentSelectEquipSlot ~= idx then
    zhuangbei.m_currentSelectEquipSlot = idx
  end
  local equip = self:IsEquipmentInSlot(idx)
  if equip then
    local equipment_detail_req_param = {
      id = equip.item_id
    }
    NetMessageMgr:SendMsg(NetAPIList.equipment_detail_req.Code, equipment_detail_req_param, self.SetUpgradeMenu, false, nil)
    zhuangbei.lastid_equipment_detail = equip.item_id
  end
  local tparm = {
    listparam = {}
  }
  local equip = self:IsEquipmentInSlot(idx)
  local equipments = GameGlobalData:GetData("equipments")
  if equip then
    local t = self:prepareEquipInfoForFlash(equip.item_id, false)
    t.strBtnName = GameLoader:GetGameText("LC_MENU_UNLOAD_BUTTON")
    t.strid = t.strid .. "|" .. "unload"
    t.equipName = t.equipName .. " [" .. GameLoader:GetGameText("LC_MENU_NEW_FLEET_EQUIPED") .. "]"
    table.insert(tparm.listparam, t)
  else
    local tinfo = {}
    tinfo.is_not_equip = true
    tinfo.equipName = GameLoader:GetGameText("LC_MENU_NEW_FLEET_NO_EQUIPMENT")
    table.insert(tparm.listparam, tinfo)
  end
  if self.filterEquip[idx] and #self.filterEquip[idx] > 0 then
    local t_hasequip = {}
    for k, v in pairs(self.filterEquip[idx]) do
      if not GameDataAccessHelper:IsItemUsedStrengthen(v.item_type) and self:CanEquip(v.item_type) then
        local t = self:prepareEquipInfoForFlash(v.item_id, false)
        t.strBtnName = GameLoader:GetGameText("LC_MENU_EQUIPMENT_BUTTON")
        t.strid = t.strid .. "|" .. "equip"
        table.insert(t_hasequip, t)
      end
    end
    if #t_hasequip > 0 then
      table.sort(t_hasequip, function(a, b)
        return a.allvalue > b.allvalue
      end)
      for _, v in ipairs(t_hasequip) do
        table.insert(tparm.listparam, v)
      end
    end
  end
  if #tparm.listparam == 1 and tparm.listparam[1].is_not_equip == true then
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local fleetIdentity = fleets[self.m_currentSelectFleetIndex].identity
    local fleetLevel = fleets[self.m_currentSelectFleetIndex].level
    zhuangbei.curBuyEquipType = GameDataAccessHelper:GetEquipSlotShopType(self.m_currentSelectEquipSlot, fleetIdentity, fleetLevel)
    local t = self:prepareEquipInfoForFlash(0, true)
    t.strBtnName = GameLoader:GetGameText("LC_MENU_BUY_CHAR")
    t.strid = t.strid .. "|" .. "buy"
    table.insert(tparm.listparam, t)
  end
  local t_hasequip = {}
  for k, v in pairs(GameItemBag.equipedItem) do
    if GameDataAccessHelper:GetEquipSlot(v.item_type) == idx and tostring(zhuangbei.fleetid) ~= tostring(v.fleet_id) and self:CanEquip(v.item_type) then
      local t = self:prepareEquipInfoForFlash(v.item_id, false)
      t.strBtnName = GameLoader:GetGameText("LC_MENU_EQUIPMENT_BUTTON")
      local fleets = GameGlobalData:GetData("fleetinfo").fleets
      local Level = 0
      local identity = 0
      for ki, vi in pairs(fleets or {}) do
        DebugTable(vi)
        if vi.id == tostring(v.fleet_id) then
          Level = vi.level
          identity = vi.identity
          break
        end
      end
      DebugOut("zhuangbei_OnEquipSelect_1 " .. tostring(v.fleet_id) .. " " .. tostring(Level) .. " " .. tostring(identity))
      local fleetName = GameDataAccessHelper:GetFleetLevelDisplayName(identity, Level)
      t.equipName = t.equipName .. " [" .. fleetName .. "]"
      t.strid = t.strid .. "|" .. "swap" .. "|" .. tostring(v.fleet_id) .. "|" .. tostring(idx)
      table.insert(t_hasequip, t)
    end
  end
  if #t_hasequip > 0 then
    table.sort(t_hasequip, function(a, b)
      return a.allvalue > b.allvalue
    end)
    for _, v in ipairs(t_hasequip) do
      table.insert(tparm.listparam, v)
    end
  end
  self:RefreshEquipStatus(idx)
  local pop = GameStateEquipEnhance:GetPop()
  tparm.showpop = showpop
  pop:zb_InitZhuangbeiList(tparm)
  self:RefreshEnhanceBarRedPoint()
end
function zhuangbei.SetUpgradeMenu(msgType, content)
  if msgType == NetAPIList.equipment_detail_ack.Code then
    DebugOut("SetUpgradeMenu")
    DebugTable(content)
    local StrengthenLv = content.enchant_level
    local enableEvolution = content.enable_evolution == 0
    local enhance = content.enhance
    local evolution = content.evolution
    local recipe = content.recipe
    local recipe_total = content.recipe.recipe_total
    local enhanceIcon, evolutionIcon, recipeIcon = "", "", ""
    enhanceIcon = "item_" .. zhuangbei:GetDynamic(enhance.equip_type)
    if enableEvolution then
      evolutionIcon = "item_" .. zhuangbei:GetDynamic(evolution.equip_type)
      recipeIcon = "item_" .. zhuangbei:GetDynamic(recipe.equip_type) .. "\001"
    else
      local equip = zhuangbei:IsEquipmentInSlot(zhuangbei.m_currentSelectEquipSlot)
      local equipments = GameGlobalData:GetData("equipments")
      if equip then
        do
          local currentEquipId = equip.item_id
          local currentEquipInfo = LuaUtils:table_values(LuaUtils:table_filter(equipments, function(k, v)
            return v.equip_id == currentEquipId
          end))[1]
          evolutionIcon = "item_" .. zhuangbei:GetDynamic(currentEquipInfo.equip_type)
        end
      end
    end
    zhuangbei.enhance_equip_type = enhance.equip_type
    zhuangbei.enhance_equip = enhance
    zhuangbei.evolution_equip_type = evolution.equip_type
    zhuangbei.evolution_equip = evolution
    zhuangbei.recipe_equip_type = recipe.equip_type
    if not enableEvolution then
      local reqText = string.format(GameLoader:GetGameText("LC_MENU_Evolution_requirement"), content.enable_evolution)
    end
    local levelPermit = GameGlobalData:GetData("levelinfo").level >= content.enhance.equip_next_level
    GameStateEquipEnhance:GetPop():zb_SetUpgradeMenu(enhanceIcon, evolutionIcon, recipeIcon, enableEvolution, StrengthenLv, reqText, levelPermit)
    local enhanceSrcLevel = enhance.equip_level ~= 0 and enhance.equip_level or GameLoader:GetGameText("LC_MENU_MAX_CHAR")
    local enhanceDestLevel = enhance.equip_next_level ~= 0 and enhance.equip_next_level or GameLoader:GetGameText("LC_MENU_MAX_CHAR")
    local paramText, enhanceSrcParam, enhanceDestParam, evolutionDestParam = "", "", "", ""
    for k, v in pairs(enhance.equip_params) do
      paramText = paramText .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. v.key) .. "\001"
      enhanceSrcParam = enhanceSrcParam .. v.value .. "\001"
    end
    for k, v in pairs(enhance.equip_next_params) do
      enhanceDestParam = enhanceDestParam .. v.value .. "\001"
    end
    for k, v in pairs(evolution.equip_params) do
      evolutionDestParam = evolutionDestParam .. v.value .. "\001"
    end
    local enhanceCost = enhance.cost
    local oneKeyEnhanceCost = zhuangbei.numberConversionForQuickEnhanceBtnText(content.one_enhance)
    local evolutionCost = evolution.cost
    local evolutionLevel, recipeText, enhanceNameText = "", "", ""
    enhanceNameText = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. enhance.equip_type)
    local canUpgrade, showWoring = false, false
    local needPlayerLevel = ""
    local levelinfo = GameGlobalData:GetData("levelinfo")
    if enableEvolution then
      evolutionLevel = evolution.equip_level
      recipeText = tostring(recipe.existed) .. "/" .. tostring(recipe_total)
      canUpgrade = levelinfo.level >= recipe.player_level_limit
    else
      evolutionReq = string.format(GameLoader:GetGameText("LC_MENU_Evolution_requirement"), content.enable_evolution)
      canUpgrade = false
    end
    if levelinfo.level < recipe.player_level_limit then
      showWoring = true
    end
    needPlayerLevel = string.format(GameLoader:GetGameText("LC_MENU_Evolution_requirement"), recipe.player_level_limit)
    local commanderAcademy = GameGlobalData:GetBuildingInfo("engineering_bay")
    local disableEvolution = 0 >= GameGlobalData:GetBuildingInfo("commander_academy").level
    local resource = GameGlobalData:GetData("resource")
    local enableEnhance = false
    local enhanceTip = ""
    local maxbuild = commanderAcademy.level * 5
    if maxbuild > levelinfo.level then
      maxbuild = levelinfo.level or maxbuild
    end
    if enhanceDestLevel <= maxbuild then
      enableEnhance = true
    else
      enableEnhance = false
      local needLevel = math.ceil(enhanceDestLevel / 5)
      DebugOut("ajsdhajsdj:", commanderAcademy.level * 5, levelinfo.level)
      if enhanceDestLevel > commanderAcademy.level * 5 then
        enhanceTip = GameLoader:GetGameText("LC_MENU_TECH_UPGRADE_LEVEL_REQUIRE") .. needLevel
      elseif enhanceDestLevel > levelinfo.level then
        enhanceTip = string.format(GameLoader:GetGameText("LC_MENU_Evolution_requirement"), enhanceDestLevel)
      elseif enhanceCost > resource.money then
        enhanceTip = ""
      end
    end
    if immanentversion170 == nil then
      enableEnhance = true
    end
    local tparam = {
      enhanceNameText = enhanceNameText,
      enhanceSrcLevel = enhanceSrcLevel,
      enhanceDestLevel = enhanceDestLevel,
      evolutionLevel = evolutionLevel,
      paramText = paramText,
      enhanceSrcParam = enhanceSrcParam,
      enhanceDestParam = enhanceDestParam,
      evolutionDestParam = evolutionDestParam,
      enhanceCost = enhanceCost,
      recipeText = recipeText,
      evolutionCost = evolutionCost,
      strCastOfQuickEnhance = oneKeyEnhanceCost,
      quickBtnText = quickBtnText
    }
    GameStateEquipEnhance:GetPop():zb_SetUpgradeMenuText(tparam)
    GameStateEquipEnhance:GetPop():zb_enableUpgrad(canUpgrade, needPlayerLevel, showWoring)
    GameStateEquipEnhance:GetPop():zb_SetEnableEnhance(enableEnhance, enhanceTip)
    zhuangbei:UpdateInterveneData(StrengthenLv)
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.equipment_detail_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
local million = 1000000
local billion = million * 1000
function zhuangbei.numberConversionForQuickEnhanceBtnText(number)
  if number < million then
    return "" .. number
  elseif number < billion then
    number = number / million
    return ...
  else
    number = number / billion
    return ...
  end
end
function zhuangbei:UpdateInterveneData(strengthLv)
  local equip = zhuangbei:IsEquipmentInSlot(zhuangbei.m_currentSelectEquipSlot)
  local equipments = GameGlobalData:GetData("equipments")
  if equip then
    do
      local currentEquipId = equip.item_id
      equip = LuaUtils:table_values(LuaUtils:table_filter(equipments, function(k, v)
        return v.equip_id == currentEquipId
      end))[1]
    end
  else
    return
  end
  local item = {}
  local data = {}
  item.item_type = 3101
  local showInfo = zhuangbei:GetShowEquipmentInfoObject(item, k_equipmentInfoTypeStrengthen, equip)
  local levelinfo = GameGlobalData:GetData("levelinfo")
  local reqLevel = GameDataAccessHelper:GetRightMenuPlayerReqLevel("enchant")
  data.canReq = reqLevel <= levelinfo.level
  data.InterveneBatchText = GameLoader:GetGameText("LC_MENU_FLEETS_ENTRANCE_INTERFERENCE_BATCH")
  data.InterveneText = GameLoader:GetGameText("LC_MENU_INTERFERENCE_INTERFERE_BUTTON")
  data.canText = ""
  if not data.canReq then
    data.canText = GameLoader:GetGameText("LC_MENU_ALIIANCE_REQUIRMENT") .. ": " .. reqLevel
  end
  data.strengthenLv = strengthLv
  data.useBtnText = GameLoader:GetGameText("LC_MENU_INTERFERENCE_ADDITIVE_CHECK")
  zhuangbei.StrengthenExtParms = showInfo.StrengthenExtParms
  local StrengthenExtParms_info = {}
  if showInfo.StrengthenExtParms then
    StrengthenExtParms_info.CanPrim = showInfo.StrengthenExtParms.CanPrim
    StrengthenExtParms_info.AdditionItem = showInfo.StrengthenExtParms.AdditionItem
    StrengthenExtParms_info.AdditionItemNum = showInfo.StrengthenExtParms.AdditionItemNum
    StrengthenExtParms_info.AdditionItemCount = showInfo.StrengthenExtParms.AdditionItemCount
    StrengthenExtParms_info.AdditionProbability = showInfo.StrengthenExtParms.AdditionProbability
    StrengthenExtParms_info.AdditionUseText = showInfo.StrengthenExtParms.AdditionUseText
    StrengthenExtParms_info.DoPrimeText = showInfo.StrengthenExtParms.DoPrimeText
    StrengthenExtParms_info.IsResetCheckbox = showInfo.StrengthenExtParms.IsResetCheckbox
  end
  data.StrengthenExtParms_info = StrengthenExtParms_info
  data.paramList = {}
  for k, v in ipairs(showInfo.params) do
    local it = {}
    it.color = v.COLOR
    it.typeName = v.TYPETEXT
    it.valueText = v.VALUETEXT
    data.paramList[#data.paramList + 1] = it
  end
  DebugOut("zhuangbei:UpdateInterveneData")
  DebugTable(data.paramList)
  data.equip = {}
  data.equip.frame = "item_" .. zhuangbei:GetDynamic(equip.equip_type)
  data.equip.level = equip.equip_level
  data.equip.name = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. equip.equip_type)
  local list = self.filterEquip[zhuangbei.m_currentSelectEquipSlot]
  local zItem
  for k, v in pairs(list or {}) do
    if GameDataAccessHelper:IsItemUsedStrengthen(v.item_type) then
      zItem = v
      break
    end
  end
  data.Zcount = 0
  if zItem then
    data.Zcount = zItem.cnt
  end
  if data.Zcount <= 0 and data.canReq then
    data.canReq = false
    data.canText = GameLoader:GetGameText("LC_MENU_INTERFERE_LACK_OF_BOSON")
  end
  zhuangbei.inventedData = data
  GameStateEquipEnhance:GetPop():zb_setIntervalData(data, GameLoader:GetGameText("LC_MENU_Level"))
end
function zhuangbei:UpdateIconAndRate(item)
  if GameFleetEquipmentPop:GetFlashObject() then
    local param = zhuangbei.StrenthenWeight
    local percentnum = math.ceil((param.SucessWeight + param.AdditionWeight + item.weight) / (param.TotleWeight + param.AdditionWeight + item.weight) * 10000) / 100
    if percentnum > 100 then
      percentnum = 100
    end
    if item.weight >= 1000 then
      percentnum = 100
    end
    local percent = percentnum .. "%"
    DebugOut("percentnum", percent)
    DebugTable(param)
    DebugTable(item)
    local zCount = zhuangbei.inventedData.Zcount
    local itemCount = item.no
    GameStateEquipEnhance:GetPop():zb_changePrimeIcon(item.number, item.needcnt, percent, zCount, itemCount, zhuangbei.inventedData)
  end
end
function zhuangbei:GenarateData(item)
  local param = {}
  param.CanPrim = true
  param.AdditionItem = item.number
  param.AdditionItemNum = item.needcnt
  param.AdditionItemCount = item.no
  param.AdditionProbability = zhuangbei.StrengthenExtParms.AdditionProbability
  param.AdditionUseText = zhuangbei.StrengthenExtParms.AdditionUseText
  param.DoPrimeText = zhuangbei.StrengthenExtParms.DoPrimeText
  param.IsResetCheckbox = zhuangbei.StrengthenExtParms.IsResetCheckbox
  param.LimitLv = zhuangbei.StrengthenExtParms.LimitLv
  return param
end
function zhuangbei:popChangePrime(arg)
  local pt = LuaUtils:string_split(arg, "!")
  local cur_item, itemInBag, itemInFleet = zhuangbei:GetCurrentSelectItem(self.m_currentSelectEquipSlot, self.m_currentSelectBagSlot)
  local items = zhuangbei.StrenthenList
  if items ~= nil and #items > 0 then
    local _items = items
    local function callback(item)
      zhuangbei.StrengthenExtParms = zhuangbei:GenarateData(item)
      zhuangbei.inventedData.StrengthenExtParms_info.AdditionItem = zhuangbei.StrengthenExtParms.AdditionItem
      zhuangbei.inventedData.StrengthenExtParms_info.AdditionItemNum = zhuangbei.StrengthenExtParms.AdditionItemNum
      zhuangbei.inventedData.StrengthenExtParms_info.AdditionItemCount = zhuangbei.StrengthenExtParms.AdditionItemCount
      zhuangbei:UpdateIconAndRate(item)
    end
    ItemBox:ShowUseChoiceBox(pt[1], pt[2], _items, callback)
  end
end
function zhuangbei:popDoPrimeNew(arg)
  local list = self.filterEquip[zhuangbei.m_currentSelectEquipSlot]
  local item
  for k, v in pairs(list or {}) do
    if GameDataAccessHelper:IsItemUsedStrengthen(v.item_type) then
      item = v
      break
    end
  end
  local itemInFleet = self:IsEquipmentInSlot(zhuangbei.m_currentSelectEquipSlot)
  if itemInFleet then
    local equipments = GameGlobalData:GetData("equipments")
    for k, v in pairs(equipments) do
      if itemInFleet.item_id == v.equip_id then
        itemInFleet = v
        break
      end
    end
  end
  if item and itemInFleet then
    DebugOut("itemInFleet.enchant_item_id = ", itemInFleet.enchant_item_id)
    DebugOut("item.item_type = ", item.item_type)
    if item.item_type ~= itemInFleet.enchant_item_id and itemInFleet.enchant_item_id ~= 0 and itemInFleet.enchant_effect_value ~= 0 then
      zhuangbei:ShowPrimeRecoverConfirmBox2(arg, 1)
    else
      zhuangbei:DoPrimeNew(arg == "true", 1)
    end
  end
end
function zhuangbei:DoPrimeNew(isUseAdditionItem, count)
  local list = self.filterEquip[zhuangbei.m_currentSelectEquipSlot]
  local item
  for k, v in pairs(list or {}) do
    if GameDataAccessHelper:IsItemUsedStrengthen(v.item_type) then
      item = v
      break
    end
  end
  local itemInFleet = self:IsEquipmentInSlot(zhuangbei.m_currentSelectEquipSlot)
  if itemInFleet then
    local equipments = GameGlobalData:GetData("equipments")
    for k, v in pairs(equipments) do
      if itemInFleet.item_id == v.equip_id then
        itemInFleet = v
        break
      end
    end
  end
  DebugOut("zhuangbei.StrengthenExtParms")
  DebugTable(zhuangbei.StrengthenExtParms)
  local enchant_params = {
    equipment_id = itemInFleet.equip_id,
    item_id = item.item_type,
    addition_item_id = 0,
    addition_item_num = 0,
    addition_credits = 0,
    uplevel_count = count
  }
  if isUseAdditionItem then
    if zhuangbei.StrengthenExtParms.AdditionItem == 1 then
      enchant_params.addition_credits = zhuangbei.StrengthenExtParms.AdditionItemNum
    else
      enchant_params.addition_item_id = zhuangbei.StrengthenExtParms.AdditionItem
      enchant_params.addition_item_num = zhuangbei.StrengthenExtParms.AdditionItemNum
    end
  end
  DebugOut("enchant_params")
  DebugTable(enchant_params)
  NetMessageMgr:SendMsg(NetAPIList.enchant_req.Code, enchant_params, zhuangbei.PrimeCallBack, true, nil)
end
function zhuangbei.PrimeCallBack(msgType, content)
  if msgType == NetAPIList.enchant_ack.Code then
    DebugOut("zhuangbei.PrimeCallBack")
    DebugTable(content)
    zhuangbei:GetOnlyEquip()
    zhuangbei:UpdateInterveneData(content.new_level)
    local diff_level = content.new_level - content.origin_level
    local diff_effect_value = content.new_effect_value - content.origin_effect_value
    local ArrowType
    if diff_level > 0 then
      if diff_level > 1 then
        ArrowType = "up_b"
      else
        ArrowType = "up_s"
      end
      GameTip:Show(GameLoader:GetGameText("LC_ALERT_interference_success"))
    elseif diff_level < 0 then
      ArrowType = "down_s"
      GameTip:Show(GameLoader:GetGameText("LC_ALERT_interference_faild"))
    else
      GameTip:Show(GameLoader:GetGameText("LC_ALERT_interference_faild"))
    end
    if ArrowType and GameFleetEquipmentPop:GetFlashObject() then
      GameStateEquipEnhance:GetPop():zb_ShowEquipStrengthenChangeArrow(ArrowType, diff_level, diff_effect_value, content.new_level)
    end
    return true
  end
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.enchant_req.Code then
    DebugOut("zhuangbei.PrimeCallBack common_ack")
    DebugTable(content)
    GameTip:Show(AlertDataList:GetTextFromErrorCode(content.code))
    return true
  end
  return false
end
function zhuangbei:openQuickInterveneUI()
  local titleText = GameLoader:GetGameText("LC_MENU_FLEETS_ENTRANCE_INTERFERENCE_BATCH")
  local descText = GameLoader:GetGameText("LC_MENU_FLEETS_ENTRANCE_INTERFERENCE_BATCH_HINT")
  GameStateEquipEnhance:GetPop():zb_openQuickInterveneUI(titleText, descText, zhuangbei.inventedData)
end
function zhuangbei:processFleetEquipments(curFleetEquipments)
  local dest = {}
  for k, v in pairs(curFleetEquipments) do
    dest[v.pos] = v
  end
  return dest
end
function zhuangbei:TheFirstHaveEquipmentInSlotId(...)
  local index = -1
  for i = 1, 5 do
    local equip = self:IsEquipmentInSlot(i)
    if equip then
      index = i
      break
    end
  end
  return index
end
function zhuangbei:IsEquipmentInSlot(slot)
  DebugOut("m_curFleetEquipments = ")
  DebugTable(self.m_curFleetEquipments)
  return self.m_curFleetEquipments[slot]
end
function zhuangbei:IsEquipmentInFleet(item_id)
  for k, v in pairs(self.m_curFleetEquipments) do
    if v.item_id == item_id then
      return true, k
    end
  end
  return false
end
function zhuangbei:IsEquipmentInList(itemId)
  for k, v in pairs(self.onlyEquip) do
    if v.item_id == itemId then
      return true, k
    end
  end
  return false
end
function zhuangbei:IsEquipmentInFilterList(itemId)
  for k1, v1 in pairs(self.filterEquip) do
    for k2, v2 in pairs(v1) do
      if v2.item_id == itemId then
        return true, k1, k2
      end
    end
  end
  return false
end
function zhuangbei:IsHasEquipmentToLoad(slot)
  if zhuangbei:GetCurFirstCanEquipBagSlotIdx(self.m_currentSelectFleetIndex, slot) > 0 then
    return true
  else
    return false
  end
end
function zhuangbei:GetCurFirstCanEquipBagSlotIdx(fleetIdx, slot)
  local idx = -1
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleet = fleets[fleetIdx]
  local fleetVessels = GameDataAccessHelper:GetCommanderVessels(fleet.identity, fleet.level)
  local isEnFleet = false
  if tonumber(fleetVessels) == 4 then
    isEnFleet = true
  end
  if self.filterEquip[slot] and #self.filterEquip[slot] > 0 then
    for k = 1, #self.filterEquip[slot] do
      local v = self.filterEquip[slot][k]
      local item, itemInBag, itemInFleet = zhuangbei:GetCurrentSelectItem(slot, k)
      if nil == item and itemInBag then
        if slot == 1 then
          if isEnFleet and tonumber(string.sub(v.item_type, -1)) == 6 then
            idx = k
            break
          elseif not isEnFleet and tonumber(string.sub(v.item_type, -1)) ~= 6 then
            idx = k
            break
          end
        else
          idx = k
          break
        end
      end
    end
  end
  return idx
end
function zhuangbei:IsContainEquipment(filterEquip)
  for k, v in pairs(filterEquip) do
    if not GameDataAccessHelper:IsItemUsedStrengthen(v.item_type) then
      return true
    end
  end
  return false
end
function zhuangbei:EquipmentUnselectInBag()
  DebugOut("self.m_currentSelectBagSlot in  un EquipmentUnselectInBag = ", self.m_currentSelectBagSlot)
  self.m_currentSelectBagSlot = -1
  zhuangbei.LastStrengthenItem = nil
end
function zhuangbei:getUpgradeEquipmentTutorialPos(...)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "getUpgradeEquipmentTutorialPos")
  end
end
function zhuangbei:getHelpTutorialPos(...)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "getHelpTutorialPos")
  end
end
function zhuangbei:CheckFinishWanaAndSilvaTutorial()
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetId = fleets[zhuangbei.m_currentSelectFleetIndex].identity
  if TutorialQuestManager.QuestTutorialFirstGetWanaArmor:IsActive() and not TutorialQuestManager.QuestTutorialFirstGetWanaArmor:IsFinished() and 1 == fleetId and zhuangbei:IsEquipmentInSlot(2) then
    DebugOut("set finish QuestTutorialFirstGetWanaArmor")
    TutorialQuestManager.QuestTutorialFirstGetWanaArmor:SetActive(false, true)
    TutorialQuestManager.QuestTutorialFirstGetWanaArmor:SetFinish(true)
    self:GetFlashObject():InvokeASCallback("_root", "SetEquipBtnTipVisible", false)
  elseif TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:IsActive() and not TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:IsFinished() and 4 == fleetId and zhuangbei:IsEquipmentInSlot(1) and zhuangbei:IsEquipmentInSlot(2) then
    TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:SetActive(false, true)
    TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:SetFinish(true)
    self:GetFlashObject():InvokeASCallback("_root", "SetEquipBtnTipVisible", false)
  end
end
function zhuangbei:RefreshEquipmentInfo()
  if zhuangbei:GetFlashObject() then
    DebugOut("RefreshEquipmentInfo: ", self.m_subState)
  end
end
function zhuangbei:SelectEquipment(item_id)
  if tonumber(item_id) < 0 then
    return
  end
  local inlist, slot1, slot2 = self:IsEquipmentInFilterList(item_id)
  if inlist then
    self:EquipmentSelectInBag(slot2)
    return
  end
  DebugOut("select item id = ", item_id)
  inlist, slot = self:IsEquipmentInFleet(item_id)
  if inlist then
    self:EquipmentUnselectInBag()
    return
  end
end
function zhuangbei:GetShowEquipmentInfoObject(item, showType, itemInFleet)
  local showInfo = {}
  showInfo.Frame = nil
  showInfo.itemType = nil
  showInfo.itemName = nil
  showInfo.reqLevel = nil
  showInfo.level = nil
  showInfo.StrengthenLv = 0
  showInfo.itemPrice = 0
  showInfo.itemNeedLevel = nil
  showInfo.DescText = nil
  showInfo.params = {}
  showInfo.StrengthenExtParms = nil
  if showType == k_equipmentInfoTypeStrengthen then
    showInfo.itemType = item.item_type
    showInfo.reqLevel = GameDataAccessHelper:GetItemReqLevel(showInfo.itemType)
    local playerInfo = GameGlobalData:GetData("levelinfo")
    showInfo.level = 0
    showInfo.StrengthenLv = 0
    showInfo.itemPrice = GameDataAccessHelper:GetItemPrice(showInfo.itemType)
    showInfo.DescText = GameLoader:GetGameText("LC_ITEM_ITEM_DESCSIMPLE_" .. showInfo.itemType)
    local StrengthenParms = zhuangbei:GetStrengthenParmasInfo(itemInFleet, showInfo.itemType, GameGlobalData:GetData("levelinfo").level)
    if immanentversion170 == 4 or immanentversion170 == 5 then
      DebugOut("hjashdgagsaj:", showInfo.itemType)
      if itemInFleet and showInfo.itemType == 3101 then
        local enchoData = GameDataAccessHelper:GetStrengthenMaterialInfo(3101, itemInFleet.enchant_level, playerInfo.level)
        showInfo.reqLevel = enchoData.player_level
        DebugOut("hjashdgagsaj:", showInfo.reqLevel)
      end
    end
    showInfo.itemNeedLevel = GameLoader:GetGameText("LC_MENU_UPGRADE_PLAYER_LVL") .. ": " .. showInfo.reqLevel
    DebugOut("========StrengthenParms:")
    DebugTable(StrengthenParms)
    table.insert(showInfo.params, {
      COLOR = "B",
      TYPETEXT = StrengthenParms.itemStrengthenType.K,
      VALUETEXT = GameLoader:GetGameText("LC_MENU_Level") .. StrengthenParms.itemStrengthenType.V
    })
    table.insert(showInfo.params, {
      COLOR = "B",
      TYPETEXT = StrengthenParms.itemStrengthenDetail.K,
      VALUETEXT = StrengthenParms.itemStrengthenDetail.V
    })
    table.insert(showInfo.params, {
      COLOR = "B",
      TYPETEXT = StrengthenParms.itemStrengthenProbability.K,
      VALUETEXT = StrengthenParms.itemStrengthenProbability.V
    })
    showInfo.StrengthenExtParms = StrengthenParms.Ext
  else
    if showType == k_equipmentInfoTypeBuy then
      showInfo.itemType = zhuangbei.curBuyEquipType
      showInfo.reqLevel = 1
      showInfo.level = 1
      showInfo.StrengthenLv = 0
      local native_equipParams = GameDataAccessHelper:GetEquipParams(showInfo.itemType)
      for k, v in ipairs(native_equipParams) do
        table.insert(showInfo.params, {
          COLOR = "Y",
          TYPETEXT = GameLoader:GetGameText("LC_MENU_Equip_param_" .. v.T),
          VALUETEXT = GameUtils.numberConversion(v.V)
        })
      end
    else
      showInfo.itemType = item.equip_type
      showInfo.reqLevel = item.equip_level_req
      showInfo.level = item.equip_level
      showInfo.StrengthenLv = item.enchant_level
      for k, v in ipairs(item.equip_params) do
        table.insert(showInfo.params, {
          COLOR = "Y",
          TYPETEXT = GameLoader:GetGameText("LC_MENU_Equip_param_" .. v.key),
          NVALUE = v.value,
          VALUETEXT = GameUtils.numberConversion(v.value)
        })
      end
      if item.enchant_level ~= 0 then
        table.insert(showInfo.params, {
          COLOR = "B",
          TYPETEXT = GameLoader:GetGameText("LC_ITEM_ITEM_EFFECT_" .. item.enchant_item_id),
          VALUETEXT = GameLoader:GetGameText("LC_MENU_Level") .. GameUtils.numberConversion(item.enchant_level)
        })
        table.insert(showInfo.params, {
          COLOR = "B",
          TYPETEXT = GameLoader:GetGameText("LC_MENU_Equip_param_" .. item.enchant_effect),
          NVALUE = item.enchant_effect_value,
          VALUETEXT = GameUtils.numberConversion(item.enchant_effect_value)
        })
      end
    end
    showInfo.itemName = GameDataAccessHelper:GetItemNameText(showInfo.itemType)
    showInfo.itemPrice = GameDataAccessHelper:GetEquipPrice(showInfo.itemType)
    showInfo.itemNeedLevel = GameLoader:GetGameText("LC_MENU_UPGRADE_PLAYER_LVL") .. ": " .. showInfo.reqLevel
    showInfo.DescText = GameDataAccessHelper:GetItemDescText(showInfo.itemType)
  end
  showInfo.Frame = "item_" .. showInfo.itemType
  showInfo.itemName = GameDataAccessHelper:GetItemNameText(showInfo.itemType)
  return showInfo
end
function zhuangbei:GetStrengthenParmasInfo(src_Equip, StrengthenItemType, PlayerLevel)
  DebugOut("StrengthenParms:GetStrengthenParmasInfo")
  local StrengthenParms = {}
  StrengthenParms.itemStrengthenType = {}
  StrengthenParms.itemStrengthenDetail = {}
  StrengthenParms.itemStrengthenProbability = {}
  StrengthenParms.Ext = {}
  StrengthenParms.Ext.CanPrim = false
  StrengthenParms.Ext.AdditionItem = 1
  StrengthenParms.Ext.AdditionItemNum = 0
  StrengthenParms.Ext.AdditionItemCount = 0
  StrengthenParms.Ext.AdditionProbability = nil
  StrengthenParms.Ext.AdditionUseText = GameLoader:GetGameText("LC_MENU_INTERFERENCE_ADDITIVE_CHECK")
  StrengthenParms.Ext.DoPrimeText = GameLoader:GetGameText("LC_MENU_INTERFERENCE_INTERFERE_BUTTON")
  StrengthenParms.Ext.LimitLv = 0
  StrengthenParms.Ext.IsResetCheckbox = false
  local AdditionWeight = 0
  if not src_Equip then
    DebugOut("not src_Equip")
    src_Equip = {
      equip_type = 0,
      enchant_level = 0,
      enchant_item_id = 0,
      enchant_effect_value = 0,
      invented = true
    }
    StrengthenParms.Ext.CanPrim = false
  end
  local EquipStrengthenLv = 0
  if StrengthenItemType == src_Equip.enchant_item_id then
    EquipStrengthenLv = src_Equip.enchant_level
  end
  DebugOut("adhssahsgadsj:", StrengthenItemType, EquipStrengthenLv, PlayerLevel)
  local sDetail, sAddition = GameDataAccessHelper:GetStrengthenMaterialInfo(StrengthenItemType, EquipStrengthenLv, PlayerLevel)
  DebugOut("sDetail:")
  DebugTable(sDetail)
  DebugOut("=======>GameItemBag")
  DebugTable(GameItemBag.itemInBag)
  local matchResult = self:MatchStrengthenItem(sDetail, StrengthenItemType, src_Equip)
  zhuangbei.StrenthenList, zhuangbei.StrenthenWeight = self:GetStrenthenList(sDetail, sAddition, StrengthenItemType, src_Equip)
  DebugTable(matchResult)
  StrengthenParms.Ext.CanPrim = matchResult.CanPrim
  if matchResult.result then
    StrengthenParms.Ext.AdditionItem = matchResult.type
    StrengthenParms.Ext.AdditionItemNum = matchResult.num
    StrengthenParms.Ext.AdditionItemCount = matchResult.count
    zhuangbei.PreCheckbox = false
    AdditionWeight = matchResult.weight
  end
  if StrengthenParms.Ext.AdditionItem == 1 then
    StrengthenParms.Ext.AdditionItemNum = sDetail.add_item[#sDetail.add_item][1]
    local resource = GameGlobalData:GetData("resource")
    StrengthenParms.Ext.AdditionItemCount = resource.credit
    if zhuangbei.PreCheckbox == nil or not zhuangbei.PreCheckbox then
      StrengthenParms.Ext.IsResetCheckbox = true
    else
      StrengthenParms.Ext.IsResetCheckbox = false
    end
    zhuangbei.PreCheckbox = true
    AdditionWeight = sDetail.add_item[#sDetail.add_item][2]
  end
  local equipType = src_Equip.equip_type % 10
  StrengthenParms.itemStrengthenType = {
    K = GameLoader:GetGameText("LC_ITEM_ITEM_EFFECT_" .. StrengthenItemType),
    V = 0
  }
  if src_Equip and StrengthenItemType == src_Equip.enchant_item_id then
    StrengthenParms.itemStrengthenType.V = src_Equip.enchant_level
  end
  if src_Equip.invented or sDetail["equip_" .. equipType] and 1 < #sDetail["equip_" .. equipType] then
    if StrengthenItemType ~= src_Equip.enchant_item_id then
      StrengthenParms.itemStrengthenDetail = {
        K = GameLoader:GetGameText("LC_MENU_INTERFERENCE_RANDOM_ATTRIBUTE"),
        V = 0
      }
    else
      StrengthenParms.itemStrengthenDetail = {
        K = GameLoader:GetGameText("LC_MENU_Equip_param_" .. src_Equip.enchant_effect),
        V = src_Equip.enchant_effect_value
      }
    end
  else
    local effect_id = 0
    for k, v in pairs(sDetail.effect) do
      if v[1] == sDetail["equip_" .. equipType][1][1] then
        effect_id = v[2]
      end
    end
    if StrengthenItemType == src_Equip.enchant_item_id then
      StrengthenParms.itemStrengthenDetail = {
        K = GameLoader:GetGameText("LC_MENU_Equip_param_" .. effect_id),
        V = src_Equip.enchant_effect_value
      }
    else
      StrengthenParms.itemStrengthenDetail = {
        K = GameLoader:GetGameText("LC_MENU_Equip_param_" .. effect_id),
        V = 0
      }
    end
  end
  local TotleWeight = 0
  local SucessWeight = 0
  for k, v in pairs(sDetail.probability) do
    TotleWeight = TotleWeight + v[2]
    if v[1] > 0 then
      SucessWeight = SucessWeight + v[2]
    end
  end
  TotleWeight = TotleWeight + sAddition
  SucessWeight = SucessWeight + sAddition
  local probNum = math.ceil(SucessWeight / TotleWeight * 10000) / 100
  if probNum > 100 then
    probNum = 100
  end
  local Probability = probNum .. "%"
  StrengthenParms.itemStrengthenProbability = {
    K = GameLoader:GetGameText("LC_MENU_INTERFERENCE_SUCCESS_RATE"),
    V = Probability
  }
  local additionProbNum = math.ceil((SucessWeight + AdditionWeight) / (TotleWeight + AdditionWeight) * 10000) / 100
  if additionProbNum > 100 or AdditionWeight >= 1000 then
    additionProbNum = 100
  end
  StrengthenParms.Ext.AdditionProbability = additionProbNum .. "%"
  StrengthenParms.Ext.LimitLv = sDetail.player_level
  return StrengthenParms
end
function zhuangbei:MatchStrengthenItem(sDetail, StrengthenItemType, src_Equip)
  local result = {}
  local addIndex = 1
  result.result = false
  result.CanPrim = false
  DebugOut("MatchStrengthenItem:")
  DebugOut(StrengthenItemType)
  DebugTable(src_Equip)
  for kItem, vItem in pairs(GameItemBag.itemInBag) do
    if vItem.item_type == StrengthenItemType and vItem.cnt > 0 and not src_Equip.invented then
      result.CanPrim = true
    end
  end
  if zhuangbei.StrengthenExtParms ~= nil and zhuangbei.StrengthenExtParms.AdditionItem ~= nil then
    for k, v in pairs(sDetail.add_item) do
      if addIndex ~= #sDetail.add_item and v[1] == zhuangbei.StrengthenExtParms.AdditionItem then
        for kItem, vItem in pairs(GameItemBag.itemInBag) do
          if vItem.item_type == v[1] and vItem.cnt >= v[2] then
            result.result = true
            result.type = v[1]
            result.num = v[2]
            result.weight = v[3]
            result.count = vItem.cnt
            return result
          end
        end
      end
      addIndex = addIndex + 1
    end
  end
  addIndex = 1
  for k, v in pairs(sDetail.add_item) do
    if addIndex ~= #sDetail.add_item then
      for kItem, vItem in pairs(GameItemBag.itemInBag) do
        if vItem.item_type == v[1] and vItem.cnt >= v[2] then
          result.result = true
          result.type = v[1]
          result.num = v[2]
          result.weight = v[3]
          result.count = vItem.cnt
          return result
        end
      end
    end
    addIndex = addIndex + 1
  end
  return result
end
function zhuangbei:GetStrenthenList(sDetail, sAddition, StrengthenItemType, src_Equip)
  local addIndex = 1
  local result = {}
  for k, v in ipairs(sDetail.add_item) do
    if addIndex ~= #sDetail.add_item then
      for kItem, vItem in pairs(GameItemBag.itemInBag) do
        if vItem.item_type == v[1] and vItem.cnt >= v[2] then
          local param = {}
          param.item_type = "item"
          param.number = vItem.item_type
          param.no = vItem.cnt
          param.needcnt = v[2]
          param.level = 0
          param.weight = v[3]
          table.insert(result, param)
        end
      end
    end
    addIndex = addIndex + 1
  end
  local weight = {}
  weight.TotleWeight = 0
  weight.SucessWeight = 0
  weight.AdditionWeight = sAddition
  for k, v in pairs(sDetail.probability) do
    weight.TotleWeight = weight.TotleWeight + v[2]
    if v[1] > 0 then
      weight.SucessWeight = weight.SucessWeight + v[2]
    end
  end
  return result, weight
end
function zhuangbei:GetStrengthenParmasInfo(src_Equip, StrengthenItemType, PlayerLevel)
  DebugOut("StrengthenParms:GetStrengthenParmasInfo")
  local StrengthenParms = {}
  StrengthenParms.itemStrengthenType = {}
  StrengthenParms.itemStrengthenDetail = {}
  StrengthenParms.itemStrengthenProbability = {}
  StrengthenParms.Ext = {}
  StrengthenParms.Ext.CanPrim = false
  StrengthenParms.Ext.AdditionItem = 1
  StrengthenParms.Ext.AdditionItemNum = 0
  StrengthenParms.Ext.AdditionItemCount = 0
  StrengthenParms.Ext.AdditionProbability = nil
  StrengthenParms.Ext.AdditionUseText = GameLoader:GetGameText("LC_MENU_INTERFERENCE_ADDITIVE_CHECK")
  StrengthenParms.Ext.DoPrimeText = GameLoader:GetGameText("LC_MENU_INTERFERENCE_INTERFERE_BUTTON")
  StrengthenParms.Ext.LimitLv = 0
  StrengthenParms.Ext.IsResetCheckbox = false
  local AdditionWeight = 0
  if not src_Equip then
    DebugOut("not src_Equip")
    src_Equip = {
      equip_type = 0,
      enchant_level = 0,
      enchant_item_id = 0,
      enchant_effect_value = 0,
      invented = true
    }
    StrengthenParms.Ext.CanPrim = false
  end
  local EquipStrengthenLv = 0
  if StrengthenItemType == src_Equip.enchant_item_id then
    EquipStrengthenLv = src_Equip.enchant_level
  end
  DebugOut("adhssahsgadsj:", StrengthenItemType, EquipStrengthenLv, PlayerLevel)
  local sDetail, sAddition = GameDataAccessHelper:GetStrengthenMaterialInfo(StrengthenItemType, EquipStrengthenLv, PlayerLevel)
  DebugOut("sDetail:")
  DebugTable(sDetail)
  DebugOut("=======>GameItemBag")
  DebugTable(GameItemBag.itemInBag)
  local matchResult = self:MatchStrengthenItem(sDetail, StrengthenItemType, src_Equip)
  zhuangbei.StrenthenList, zhuangbei.StrenthenWeight = self:GetStrenthenList(sDetail, sAddition, StrengthenItemType, src_Equip)
  DebugTable(matchResult)
  StrengthenParms.Ext.CanPrim = matchResult.CanPrim
  if matchResult.result then
    StrengthenParms.Ext.AdditionItem = matchResult.type
    StrengthenParms.Ext.AdditionItemNum = matchResult.num
    StrengthenParms.Ext.AdditionItemCount = matchResult.count
    zhuangbei.PreCheckbox = false
    AdditionWeight = matchResult.weight
  end
  if StrengthenParms.Ext.AdditionItem == 1 then
    StrengthenParms.Ext.AdditionItemNum = sDetail.add_item[#sDetail.add_item][1]
    local resource = GameGlobalData:GetData("resource")
    StrengthenParms.Ext.AdditionItemCount = resource.credit
    if zhuangbei.PreCheckbox == nil or not zhuangbei.PreCheckbox then
      StrengthenParms.Ext.IsResetCheckbox = true
    else
      StrengthenParms.Ext.IsResetCheckbox = false
    end
    zhuangbei.PreCheckbox = true
    AdditionWeight = sDetail.add_item[#sDetail.add_item][2]
  end
  local equipType = src_Equip.equip_type % 10
  StrengthenParms.itemStrengthenType = {
    K = GameLoader:GetGameText("LC_ITEM_ITEM_EFFECT_" .. StrengthenItemType),
    V = 0
  }
  if src_Equip and StrengthenItemType == src_Equip.enchant_item_id then
    StrengthenParms.itemStrengthenType.V = src_Equip.enchant_level
  end
  if src_Equip.invented or sDetail["equip_" .. equipType] and 1 < #sDetail["equip_" .. equipType] then
    if StrengthenItemType ~= src_Equip.enchant_item_id then
      StrengthenParms.itemStrengthenDetail = {
        K = GameLoader:GetGameText("LC_MENU_INTERFERENCE_RANDOM_ATTRIBUTE"),
        V = 0
      }
    else
      StrengthenParms.itemStrengthenDetail = {
        K = GameLoader:GetGameText("LC_MENU_Equip_param_" .. src_Equip.enchant_effect),
        V = src_Equip.enchant_effect_value
      }
    end
  else
    local effect_id = 0
    for k, v in pairs(sDetail.effect) do
      if v[1] == sDetail["equip_" .. equipType][1][1] then
        effect_id = v[2]
      end
    end
    if StrengthenItemType == src_Equip.enchant_item_id then
      StrengthenParms.itemStrengthenDetail = {
        K = GameLoader:GetGameText("LC_MENU_Equip_param_" .. effect_id),
        V = src_Equip.enchant_effect_value
      }
    else
      StrengthenParms.itemStrengthenDetail = {
        K = GameLoader:GetGameText("LC_MENU_Equip_param_" .. effect_id),
        V = 0
      }
    end
  end
  local TotleWeight = 0
  local SucessWeight = 0
  for k, v in pairs(sDetail.probability) do
    TotleWeight = TotleWeight + v[2]
    if v[1] > 0 then
      SucessWeight = SucessWeight + v[2]
    end
  end
  TotleWeight = TotleWeight + sAddition
  SucessWeight = SucessWeight + sAddition
  local probNum = math.ceil(SucessWeight / TotleWeight * 10000) / 100
  if probNum > 100 then
    probNum = 100
  end
  local Probability = probNum .. "%"
  StrengthenParms.itemStrengthenProbability = {
    K = GameLoader:GetGameText("LC_MENU_INTERFERENCE_SUCCESS_RATE"),
    V = Probability
  }
  local additionProbNum = math.ceil((SucessWeight + AdditionWeight) / (TotleWeight + AdditionWeight) * 10000) / 100
  if additionProbNum > 100 or AdditionWeight >= 1000 then
    additionProbNum = 100
  end
  StrengthenParms.Ext.AdditionProbability = additionProbNum .. "%"
  StrengthenParms.Ext.LimitLv = sDetail.player_level
  return StrengthenParms
end
function zhuangbei:GetCurrentSelectItem(FeetSlot, BagSlot)
  local item, itemInBag, itemInFleet
  DebugOut("FeetSlot = ", FeetSlot)
  DebugOut("BagSlot = ", BagSlot)
  DebugTable(self.filterEquip)
  if BagSlot ~= -1 and self.filterEquip[FeetSlot][BagSlot] ~= nil then
    itemInBag = self.filterEquip[FeetSlot][BagSlot]
    if GameDataAccessHelper:IsItemUsedStrengthen(itemInBag.item_type) then
      item = itemInBag
    else
      local equipments = GameGlobalData:GetData("equipments")
      for k, v in pairs(equipments) do
        if itemInBag.item_id == v.equip_id then
          itemInBag = v
          break
        end
      end
    end
  end
  if FeetSlot ~= -1 then
    itemInFleet = self:IsEquipmentInSlot(FeetSlot)
    if itemInFleet then
      local equipments = GameGlobalData:GetData("equipments")
      for k, v in pairs(equipments) do
        if itemInFleet.item_id == v.equip_id then
          itemInFleet = v
          break
        end
      end
    end
  end
  return item, itemInBag, itemInFleet
end
function zhuangbei:GetOnlyEquip()
  DebugOut("GetOnlyEquip:")
  DebugTable(GameItemBag.itemInBag)
  self.onlyEquip = LuaUtils:table_values(LuaUtils:table_filter(GameItemBag.itemInBag or {}, function(k, v)
    return tonumber(v.item_type) > 100000 and tonumber(v.item_type) < 110000 or GameDataAccessHelper:IsItemUsedStrengthen(v.item_type)
  end))
  self.filterEquip = {}
  for i = 1, k_maxSlot do
    self.filterEquip[i] = {}
  end
  for i, value in pairs(self.onlyEquip) do
    local itemType = value.item_type
    if GameDataAccessHelper:IsItemUsedStrengthen(itemType) then
      for i = 1, k_maxSlot do
        self.filterEquip[i][#self.filterEquip[i] + 1] = value
      end
    else
      local slot = GameDataAccessHelper:GetEquipSlot(itemType)
      table.insert(self.filterEquip[slot], 1, value)
    end
  end
  DebugOut("onlyEquip:")
  DebugTable(self.onlyEquip)
  DebugOut("filterEquip:")
  DebugTable(self.filterEquip)
end
function zhuangbei:GetDynamic(frame, itemKey, index)
  local frame_temp = frame
  if DynamicResDownloader:IsDynamicStuff(frame_temp, DynamicResDownloader.resType.PIC) then
    local fullFileName = DynamicResDownloader:GetFullName(frame_temp .. ".png", DynamicResDownloader.resType.PIC)
    local localPath = "data2/" .. fullFileName
    if DynamicResDownloader:IfResExsit(localPath) then
      ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
    else
      frame_temp = "temp"
      self:AddDownloadPath(frame_temp, itemKey, index)
    end
  end
  return frame_temp
end
function zhuangbei:AddDownloadPath(itemID, itemKey, index)
  DebugStore("Add download path", itemID, itemKey, index)
  local resName = itemID .. ".png"
  local extInfo = {}
  extInfo.itemKey = itemKey
  extInfo.index = index
  extInfo.item_id = itemID
  extInfo.resPath = "data2/LAZY_LOAD_dynamic_" .. resName
  DynamicResDownloader:AddDynamicRes(resName, DynamicResDownloader.resType.PIC, extInfo, nil)
end
function zhuangbei:RefreshCurFleetEquipment()
  DebugOut("zhuangbei:RefreshCurFleetEquipment")
  self:GetOnlyEquip()
  GameUtils:printTable(self.m_curFleetEquipments)
  local frame, level, StrengthenLv, showText = "", "", "", ""
  for i = 1, 5 do
    local equip = self:IsEquipmentInSlot(i)
    local equipments = GameGlobalData:GetData("equipments")
    if equip then
      do
        local currentEquipId = equip.item_id
        local currentEquipInfo = LuaUtils:table_values(LuaUtils:table_filter(equipments, function(k, v)
          return v.equip_id == currentEquipId
        end))[1]
        if currentEquipInfo then
          frame = frame .. "item_" .. zhuangbei:GetDynamic(currentEquipInfo.equip_type) .. "\001"
          level = level .. GameLoader:GetGameText("LC_MENU_Level") .. currentEquipInfo.equip_level .. "\001"
          StrengthenLv = StrengthenLv .. currentEquipInfo.enchant_level .. "\001"
          showText = showText .. "" .. "\001"
        end
      end
    else
      frame = frame .. "empty_e" .. i .. "\001"
      level = level .. "no" .. "\001"
      StrengthenLv = StrengthenLv .. 0 .. "\001"
      DebugOut("filter:" .. tostring(i))
      if self.filterEquip and self.filterEquip[i] and self:IsContainEquipment(self.filterEquip[i]) and zhuangbei:IsHasEquipmentToLoad(i) then
        showText = showText .. GameLoader:GetGameText("LC_MENU_UNEQUIPPED_BUTTON") .. "\001"
        DebugOut("showText:1", showText)
      else
        showText = showText .. GameLoader:GetGameText("LC_MENU_BUY_CHAR") .. "\001"
        DebugOut("showText:2", showText)
      end
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "SetFleetEquipment", frame, level, StrengthenLv, true, showText)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetId = fleets[self.m_currentSelectFleetIndex].identity
  if (not TutorialQuestManager.QuestTutorialEquip:IsActive() or TutorialQuestManager.QuestTutorialEquip:IsFinished()) and TutorialQuestManager.QuestTutorialFirstGetWanaArmor:IsActive() then
    if 1 == fleetId and self.m_subState == k_subStateFleetView and nil == self:IsEquipmentInSlot(2) then
      self:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 2, true)
    else
      self:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 2, false)
    end
  elseif (not TutorialQuestManager.QuestTutorialEquip:IsActive() or TutorialQuestManager.QuestTutorialEquip:IsFinished()) and TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:IsActive() then
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local fleetId = fleets[zhuangbei.m_currentSelectFleetIndex].identity
    self:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 1, false)
    self:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 2, false)
    if 4 == fleetId then
      if nil == self:IsEquipmentInSlot(1) then
        self:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 1, true)
      elseif nil == self:IsEquipmentInSlot(2) then
        self:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 2, true)
      end
    end
  end
  local equipment_action_list_req_param = {
    ids = {}
  }
  for i = 1, 5 do
    local equip = zhuangbei:IsEquipmentInSlot(i)
    if equip then
      table.insert(equipment_action_list_req_param.ids, tonumber(equip.item_id))
    else
      table.insert(equipment_action_list_req_param.ids, -1)
    end
    self:GetFlashObject():InvokeASCallback("_root", "setEquipmentRedPoint", i, false)
  end
  NetMessageMgr:SendMsg(NetAPIList.equipment_action_list_req.Code, equipment_action_list_req_param, zhuangbei.EquipActionListCallback, true, nil)
end
function zhuangbei.EquipActionListCallback(msgType, content)
  if msgType == NetAPIList.equipment_action_list_ack.Code then
    DebugOut("EquipActionListCallback", msgType)
    DebugTable(content)
    zhuangbei.equipStatus = content.equipments
    local canEvolution = ""
    local firstCanEvolution = false
    for i = 1, 5 do
      local equip = zhuangbei:IsEquipmentInSlot(i)
      if equip then
        if zhuangbei.equipStatus[i] and zhuangbei.equipStatus[i] ~= -1 and zhuangbei.equipStatus[i].can_evolute == 1 then
          canEvolution = canEvolution .. "true" .. "\001"
          if i == 1 then
            firstCanEvolution = true
          end
        else
          canEvolution = canEvolution .. "false" .. "\001"
        end
      else
        canEvolution = canEvolution .. "false" .. "\001"
      end
    end
    local flashObj = zhuangbei:GetFlashObject()
    if flashObj then
      flashObj:InvokeASCallback("_root", "SetEquipArrows", canEvolution)
    end
    zhuangbei:RefreshRedPoint()
    return true
  elseif msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.equipment_action_list_req.Code and content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function zhuangbei:RefreshRedPoint()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "HideAllRedPoint")
    if GameGlobalData.redPointStates then
      local fleets = GameGlobalData:GetData("fleetinfo").fleets
      local fleetId = fleets[self.m_currentSelectFleetIndex].identity
      local maxFleetIndex = LuaUtils:table_size(fleets)
      self:RefreshEnhanceRedPoint()
      self:RefreshWearRedPoint()
      self:RefreshEnhanceBarRedPoint()
    end
  end
end
function zhuangbei:RefreshEnhanceBarRedPoint()
  if not self.m_currentSelectEquipSlot then
    self.m_currentSelectEquipSlot = 1
  end
  local equip = zhuangbei:IsEquipmentInSlot(self.m_currentSelectEquipSlot)
  if equip and self.equipStatus then
    local stat = zhuangbei.equipStatus[self.m_currentSelectEquipSlot]
    local levelinfo = GameGlobalData:GetData("levelinfo")
    local reqLevel = GameDataAccessHelper:GetRightMenuPlayerReqLevel("enchant")
    DebugOut("levelinfo.level", levelinfo.level, "reqLevel", reqLevel)
    DebugTable(stat)
    if stat and stat ~= -1 and (stat.can_evolute == 1 or stat.can_enhance == 1 or stat.can_enchant > 0 and reqLevel <= levelinfo.level) then
      if reqLevel <= levelinfo.level then
        GameStateEquipEnhance:GetPop():zb_refreshEnhanceBarRedPoint(true, stat.can_enhance, stat.can_evolute, stat.can_enchant)
      else
        GameStateEquipEnhance:GetPop():zb_refreshEnhanceBarRedPoint(true, stat.can_enhance, stat.can_evolute, 0)
      end
    else
      GameStateEquipEnhance:GetPop():zb_refreshEnhanceBarRedPoint(false, nil, nil, nil)
    end
  else
    GameStateEquipEnhance:GetPop():zb_refreshEnhanceBarRedPoint(false, nil, nil, nil)
  end
end
function zhuangbei:RefreshEnhanceRedPoint()
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetId = fleets[self.m_currentSelectFleetIndex].identity
  local levelinfo = GameGlobalData:GetData("levelinfo")
  local reqLevel = GameDataAccessHelper:GetRightMenuPlayerReqLevel("enchant")
  DebugOut("zhuangbei:RefreshEnhanceRedPoint")
  DebugTable(zhuangbei.equipStatus)
  local func_hasredpoint = false
  for i = 1, 5 do
    local equip = zhuangbei:IsEquipmentInSlot(i)
    if equip then
      if zhuangbei.equipStatus and zhuangbei.equipStatus[i] and zhuangbei.equipStatus[i] ~= -1 and (zhuangbei.equipStatus[i].can_enhance == 1 or zhuangbei.equipStatus[i].can_enchant > 0 and reqLevel <= levelinfo.level) then
        self:GetFlashObject():InvokeASCallback("_root", "setEquipmentRedPoint", i, true)
        func_hasredpoint = true
      elseif zhuangbei.equipStatus and zhuangbei.equipStatus[i] and zhuangbei.equipStatus[i] ~= -1 and zhuangbei.equipStatus[i].can_evolute == 1 then
        self:GetFlashObject():InvokeASCallback("_root", "setEquipmentRedPoint", i, true)
        func_hasredpoint = true
      else
        self:GetFlashObject():InvokeASCallback("_root", "setEquipmentRedPoint", i, false)
      end
    else
      local hassomeToEquip = false
      if self.filterEquip[i] and 0 < #self.filterEquip[i] then
        for k, v in pairs(self.filterEquip[i]) do
          if not GameDataAccessHelper:IsItemUsedStrengthen(v.item_type) and self:CanEquip(v.item_type) then
            hassomeToEquip = true
            func_hasredpoint = true
            break
          end
        end
      end
      self:GetFlashObject():InvokeASCallback("_root", "setEquipmentRedPoint", i, hassomeToEquip)
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "setFunctions_news", "zhuangbei", func_hasredpoint)
end
function zhuangbei:RefreshWearRedPoint()
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetId = fleets[self.m_currentSelectFleetIndex].identity
  local equipments = GameGlobalData:GetData("equipments")
  for i = 1, 5 do
    local equip = self:IsEquipmentInSlot(i)
    if not equip and self.filterEquip and self.filterEquip[i] and self:IsContainEquipment(self.filterEquip[i]) and zhuangbei:IsHasEquipmentToLoad(i) then
      self:GetFlashObject():InvokeASCallback("_root", "setWearRedPoint", true)
      return
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "setWearRedPoint", false)
end
function zhuangbei.UnloadEquipCallback(msgType, content)
  if msgType == NetAPIList.swap_bag_grid_ack.Code then
    DebugOut("zhuangbei.UnloadEquipCallback")
    if content.result == 0 then
      if waitForRefreshCount and waitForRefreshCount > 1 then
        waitForRefreshCount = waitForRefreshCount - 1
        local equip = zhuangbei:IsEquipmentInSlot(waitForRefreshCount)
        if equip then
          zhuangbei:UnloadEquipBySlot(equip.item_id, waitForRefreshCount)
        end
        DebugOut("waitForRefreshCount in unload = ", waitForRefreshCount)
      else
        DebugOut("whats happen")
        waitForRefreshCount = 0
        startQuickUnload = false
        zhuangbei.OnEnhanceFleetinfoChange()
      end
      return true
    else
      waitForRefreshCount = 0
      zhuangbei.OnEnhanceFleetinfoChange()
      GameTip:Show(AlertDataList:GetTextFromErrorCode(content.result), 3000)
      return true
    end
  end
  return false
end
function zhuangbei:UnloadEquip(itemId)
  DebugOut("zhuangbei:UnloadEquip(itemId): ", itemId)
  local inlist, slot = self:IsEquipmentInList(itemId)
  slot = slot or self.m_currentSelectEquipSlot
  self.unloadSlot = slot
  local equip = self:IsEquipmentInSlot(slot)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  if equip then
    self.m_optionEquipItemId = itemId
    local swap_bag_grid_req_content = {
      matrix_id = zhuangbei.curMatrixIndex,
      orig_grid_owner = fleets[self.m_currentSelectFleetIndex].id,
      oirg_grid_type = BAG_OWNER_FLEET,
      oirg_grid_pos = slot,
      target_grid_owner = GameGlobalData:GetData("userinfo").player_id,
      target_grid_type = BAG_OWNER_USER,
      target_grid_pos = -1
    }
    local function netFailedCallback()
      self.refreshFleets()
    end
    print("dddd index", zhuangbei.curMatrixIndex)
    NetMessageMgr:SendMsg(NetAPIList.swap_bag_grid_req.Code, swap_bag_grid_req_content, self.UnloadEquipCallback, true, netFailedCallback)
  end
end
function zhuangbei:UnloadEquipBySlot(itemId, slot)
  DebugOut("zhuangbei:UnloadEquip(slot): ", slot)
  self.unloadSlot = slot
  local equip = self:IsEquipmentInSlot(slot)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  if equip then
    self.m_optionEquipItemId = itemId
    local swap_bag_grid_req_content = {
      matrix_id = zhuangbei.curMatrixIndex,
      orig_grid_owner = fleets[self.m_currentSelectFleetIndex].id,
      oirg_grid_type = BAG_OWNER_FLEET,
      oirg_grid_pos = slot,
      target_grid_owner = GameGlobalData:GetData("userinfo").player_id,
      target_grid_type = BAG_OWNER_USER,
      target_grid_pos = -1
    }
    local function netFailedCallback()
      zhuangbei:onEndDragItem()
      self.refreshFleets()
    end
    NetMessageMgr:SendMsg(NetAPIList.swap_bag_grid_req.Code, swap_bag_grid_req_content, self.UnloadEquipCallback, true, netFailedCallback)
  end
end
function zhuangbei:UnloadAllEquip()
  waitForRefreshCount = 0
  DebugOut("unload equip in slot i = ", i)
  local equip = zhuangbei:IsEquipmentInSlot(5)
  if equip then
    DebugTable(equip)
    waitForRefreshCount = 5
    zhuangbei:UnloadEquipBySlot(equip.item_id, waitForRefreshCount)
  end
end
function zhuangbei.CheckTutorialBattleMapAfterRecruit()
  local fleet_info = GameGlobalData:GetData("fleetinfo")
  local resource_info = GameGlobalData:GetData("resource")
  DebugTable(fleet_info)
  if fleet_info and fleet_info.fleets and resource_info and resource_info.prestige then
    DebugOut(#fleet_info.fleets, resource_info.prestige, QuestTutorialBattleMapAfterRecruit:IsActive(), QuestTutorialBattleMapAfterRecruit:IsFinished())
    if #fleet_info.fleets > 2 and resource_info.prestige >= 500 and not QuestTutorialBattleMapAfterRecruit:IsActive() and not QuestTutorialBattleMapAfterRecruit:IsFinished() then
      DebugOut("tutorial BattleMapAfterRecruit open")
      QuestTutorialBattleMapAfterRecruit:SetActive(true)
    end
  end
end
function zhuangbei.OnEnhanceFleetinfoChange()
  DebugOut("NTF On: OnEnhanceFleetinfoChange")
  zhuangbei.CheckTutorialBattleMapAfterRecruit()
  DebugOut(GameStateManager:GetCurrentGameState())
  DebugOut("whats happen", zhuangbei.m_subState)
  DebugOut("In!")
  zhuangbei:RefreshFleetBagAndSlot()
end
function zhuangbei:RefreshFleetBagAndSlot()
  DebugOut("RefreshFleetBagAndSlot")
  self:FleetSelect(zhuangbei.m_currentSelectFleetIndex, true)
end
function zhuangbei:UpdateSelectedFleetInfo()
  if zhuangbei.needUpdateFleet == false then
    zhuangbei.needUpdateFleet = true
    return false
  end
  local fleetIndex = self.m_currentSelectFleetIndex
  local forceRefresh = false
  DebugOut("UpdateSelectedFleetInfo:", fleetIndex, ",", forceRefresh, ",")
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  if fleets ~= nil then
    local maxFleetIndex = LuaUtils:table_size(fleets)
    if fleetIndex > 0 and fleetIndex <= maxFleetIndex then
      if self:GetFlashObject() ~= nil then
        self:GetFlashObject():InvokeASCallback("_root", "SetSelectedFleetId", fleetIndex, maxFleetIndex)
      end
      local content = fleets[fleetIndex]
      self.m_currentFleetsId = content.id
      self.m_curreetFleetsForce = content.force
      DebugOut("content.force")
      DebugOut(self.m_curreetFleetsForce)
      if self.m_curreetFleetsForce and self:GetFlashObject() then
        self:GetFlashObject():InvokeASCallback("_root", "setForceText", GameUtils.numberConversion(self.m_curreetFleetsForce))
      end
      self:FleetSelect(fleetIndex, forceRefresh)
      DebugTable(content)
      DebugOut("fleet_id:", self.m_currentFleetsId, ",", content.identity)
      zhuangbei.art_canupgrade = false
      if self:GetFlashObject() then
        self:GetFlashObject():InvokeASCallback("_root", "setArtifactButtonState", currentState)
      end
      if self:GetFlashObject() then
        self:GetFlashObject():InvokeASCallback("_root", "setShipBtnShow", true)
      end
      GameFleetEquipment.kejin.RefreshKrypton()
      GameFleetEquipment:RefreshKryptonRedPoint(content.identity)
      DebugOut("xixixixixii")
      return true
    end
  end
  return false
end
function zhuangbei:FleetSelect(index, forceRefresh)
  DebugOut("FleetSelect: ", zhuangbei.doFleetSelectFrame, GameStateManager.m_frameCounter, forceRefresh)
  if not zhuangbei.doFleetSelectFrame or GameStateManager.m_frameCounter - zhuangbei.doFleetSelectFrame > 10 or forceRefresh then
  else
    return
  end
  zhuangbei.doFleetSelectFrame = GameStateManager.m_frameCounter
  if self.m_currentSelectFleetIndex ~= index or forceRefresh then
    if QuestTutorialsChangeFleets1:IsActive() or QuestTutorialEnhance_third:IsActive() and not QuestTutorialEquip:IsActive() then
      if index == 2 then
        if QuestTutorialsChangeFleets1:IsActive() then
          QuestTutorialsChangeFleets1:SetFinish(true)
        end
        if QuestTutorialEnhance_third:IsActive() then
          QuestTutorialEnhance_third:SetFinish(true)
        end
        self:GetFlashObject():InvokeASCallback("_root", "SetShowNextTip", false)
      end
    elseif TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:IsActive() and not QuestTutorialEquip:IsActive() then
      local fleets = GameGlobalData:GetData("fleetinfo").fleets
      if 4 == fleets[index].identity then
        self:GetFlashObject():InvokeASCallback("_root", "SetShowNextTip", false)
      end
    end
    if self.m_currentSelectFleetIndex ~= index then
      DebugOut(" self.m_currentSelectEquipSlot: ", self.m_currentSelectEquipSlot)
    end
    self.m_currentSelectFleetIndex = index
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local fleetId = fleets[self.m_currentSelectFleetIndex].id
    if not GameFleetEquipment.isTeamLeague then
      do
        local bag_req_content = {
          owner = fleetId,
          bag_type = BAG_OWNER_FLEET,
          pos = -1
        }
        local function netFailedCallback()
          NetMessageMgr:SendMsg(NetAPIList.bag_req.Code, bag_req_content, self.DownloadCurrentFleetEquipment, false, netFailedCallback)
        end
        NetMessageMgr:SendMsg(NetAPIList.bag_req.Code, bag_req_content, self.DownloadCurrentFleetEquipment, false, netFailedCallback)
      end
    else
      do
        local tlc_bag_req_content = {
          owner = fleetId,
          matrix = GameFleetEquipment:GetCurMatrixId(),
          bag_type = BAG_OWNER_FLEET,
          pos = -1
        }
        local function netFailedCallback()
          NetMessageMgr:SendMsg(NetAPIList.tlc_bag_req.Code, tlc_bag_req_content, self.tlc_DownloadCurrentFleetEquipment, false, netFailedCallback)
        end
        NetMessageMgr:SendMsg(NetAPIList.tlc_bag_req.Code, tlc_bag_req_content, self.tlc_DownloadCurrentFleetEquipment, false, netFailedCallback)
      end
    end
  end
end
function zhuangbei:CanEquip(itemType)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  if not fleets then
    return
  end
  if not fleets[self.m_currentSelectFleetIndex] then
    return
  end
  local fleetIdentity = fleets[self.m_currentSelectFleetIndex].identity
  local fleetLevel = fleets[self.m_currentSelectFleetIndex].level
  if GameDataAccessHelper:GetEquipReqFleet(itemType) == GameDataAccessHelper.ATK_TYPE_energy and GameDataAccessHelper:IsEnergyAttack(fleetIdentity, fleetLevel) then
    return true
  elseif GameDataAccessHelper:GetEquipReqFleet(itemType) == GameDataAccessHelper.ATK_TYPE_physics and not GameDataAccessHelper:IsEnergyAttack(fleetIdentity, fleetLevel) then
    return true
  elseif GameDataAccessHelper:GetEquipReqFleet(itemType) == GameDataAccessHelper.ATK_TYPE_common then
    return true
  end
  return false
end
function zhuangbei:Swap(itemId, fleetId, srcPos)
  local function swapCallback()
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local swap_bag_grid_req_content = {
      matrix_id = GameFleetEquipment:GetCurMatrixId(),
      orig_grid_owner = fleetId,
      oirg_grid_type = BAG_OWNER_FLEET,
      oirg_grid_pos = srcPos,
      target_grid_owner = fleets[zhuangbei.m_currentSelectFleetIndex].id,
      target_grid_type = BAG_OWNER_FLEET,
      target_grid_pos = srcPos
    }
    local function netFailedCallback()
      GameItemBag:RequestBag()
    end
    waitForRefreshCount = 1
    DebugOut("zhuangbei:Equip yes")
    NetMessageMgr:SendMsg(NetAPIList.swap_bag_grid_req.Code, swap_bag_grid_req_content, self.EquipCallback, true, netFailedCallback)
  end
  local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local Level = 0
  local identity = 0
  for k, v in pairs(fleets or {}) do
    if tostring(v.id) == tostring(fleetId) then
      Level = v.level
      identity = v.identity
    end
  end
  local fleetName = GameDataAccessHelper:GetFleetLevelDisplayName(identity, Level)
  local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
  local info = string.gsub(GameLoader:GetGameText("LC_MENU_EQUIP_CHANGE_CHAR"), "<name1>", fleetName)
  GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
  GameUIMessageDialog:SetYesButton(swapCallback)
  GameUIMessageDialog:SetNoButton(nil)
  GameUIMessageDialog:Display(text_title, info)
end
function zhuangbei:Equip(itemId)
  DebugOut("zhuangbei:Equip", itemId)
  local _, index = self:IsEquipmentInList(itemId)
  print("equip", index, LuaUtils:serializeTable(self.onlyEquip))
  index = index or self.lastReleasedIndex
  if not self.onlyEquip[index] then
    return
  end
  local itemType = self.onlyEquip[index].item_type
  local itemId = self.onlyEquip[index].item_id
  local srcPos = self.onlyEquip[index].pos
  self.equipIndex = index
  DebugOut("zhuangbei:Equip equipIndex is : " .. self.equipIndex)
  local slot = GameDataAccessHelper:GetEquipSlot(itemType) or -1
  if slot == -1 then
    return
  end
  self.equipSlot = slot
  if self:CanEquip(itemType) then
    self.m_optionEquipItemId = itemId
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local swap_bag_grid_req_content = {
      matrix_id = zhuangbei.curMatrixIndex,
      orig_grid_owner = GameGlobalData:GetData("userinfo").player_id,
      oirg_grid_type = BAG_OWNER_USER,
      oirg_grid_pos = srcPos,
      target_grid_owner = fleets[self.m_currentSelectFleetIndex].id,
      target_grid_type = BAG_OWNER_FLEET,
      target_grid_pos = slot
    }
    local function netFailedCallback()
      GameItemBag:RequestBag()
    end
    waitForRefreshCount = 1
    DebugOut("zhuangbei:Equip yes")
    NetMessageMgr:SendMsg(NetAPIList.swap_bag_grid_req.Code, swap_bag_grid_req_content, self.EquipCallback, true, netFailedCallback)
  end
end
function zhuangbei.EquipCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.swap_bag_grid_req.Code then
    return true
  end
  if msgType == NetAPIList.swap_bag_grid_ack.Code then
    DebugOut("\230\137\147\229\141\176--zhuangbei.EquipCallback----")
    DebugTable(content)
    if content.result == 0 then
      if waitForRefreshCount and 0 < waitForRefreshCount then
        waitForRefreshCount = waitForRefreshCount - 1
        DebugOut("waitForRefreshCount in equip = ", waitForRefreshCount)
      end
      if waitForRefreshCount == 0 then
        DebugOut("receive all")
        if QuestTutorialEquipAllByOneKey:IsActive() then
          zhuangbei:GetFlashObject():InvokeASCallback("_root", "SetShowTipClose", true)
          QuestTutorialEquipAllByOneKey:SetFinish(true)
        end
      end
      if QuestTutorialEquip:IsActive() then
      end
      DebugOut("lalalalal2222")
      if TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:IsActive() and not TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:IsFinished() then
        DebugOut("lalalalal333333")
        local fleets = GameGlobalData:GetData("fleetinfo").fleets
        local fleetId = fleets[zhuangbei.m_currentSelectFleetIndex].identity
        zhuangbei:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 1, false)
        zhuangbei:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 2, false)
        if 4 == fleetId then
          DebugOut("equip here ")
          if zhuangbei:IsEquipmentInSlot(1) and zhuangbei.m_currentSelectEquipSlot == 1 then
            DebugOut("equip here set visible false 11")
            zhuangbei:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 1, false)
            zhuangbei:GetFlashObject():InvokeASCallback("_root", "SetEquipBtnTipVisible", false)
          elseif nil == zhuangbei:IsEquipmentInSlot(2) and zhuangbei.m_currentSelectEquipSlot == 2 then
            DebugOut("equip here set visible false 22")
            zhuangbei:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 2, false)
            zhuangbei:GetFlashObject():InvokeASCallback("_root", "SetEquipBtnTipVisible", false)
          end
        end
      end
      zhuangbei._forceFinishTutorialFirstGetSilvaEquipIfExecptionOperation()
      zhuangbei._forceFinishTutorialEquipAndTutorialFirstGetWanaArmorIfTutorialFirstGetSilvaEquipIsActive()
      zhuangbei:CheckFinishWanaAndSilvaTutorial()
      return true
    else
      waitForRefreshCount = 0
      zhuangbei.all_need_equip = {}
      GameUIGlobalScreen:ShowAlert("error", content.result, nil)
      return true
    end
  end
  return false
end
function zhuangbei._forceFinishTutorialFirstGetSilvaEquipIfExecptionOperation()
  DebugOut("zhuangbei._forceFinishTutorialFirstGetSilvaEquipIfExecptionOperation")
  if not TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:IsActive() then
    return
  end
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetId = fleets[zhuangbei.m_currentSelectFleetIndex].identity
  DebugOut("fleetId is : " .. fleetId)
  if fleetId ~= 4 then
    zhuangbei._forceFinishTutorialFirstGetSilvaEquip()
    return
  end
  DebugOut("m_currentSelectEquipSlot is : " .. zhuangbei.m_currentSelectEquipSlot)
  local isSelectSlotOne = 1 == zhuangbei.m_currentSelectEquipSlot
  local isSelectSlotTwo = 2 == zhuangbei.m_currentSelectEquipSlot
  if not isSelectSlotOne and not isSelectSlotTwo then
    zhuangbei._forceFinishTutorialFirstGetSilvaEquip()
    return
  end
  if isSelectSlotTwo and nil == zhuangbei:IsEquipmentInSlot(1) then
    zhuangbei._forceFinishTutorialFirstGetSilvaEquip()
    return
  end
  DebugOut("end of zhuangbei._forceFinishTutorialFirstGetSilvaEquipIfExecptionOperation")
end
function zhuangbei._forceFinishTutorialFirstGetSilvaEquip()
  if TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:IsActive() then
    DebugOut("zhuangbei._forceFinishTutorialFirstGetSilvaEquip")
    TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:SetFinish(true)
  end
end
function zhuangbei._forceFinishTutorialEquipAndTutorialFirstGetWanaArmorIfTutorialFirstGetSilvaEquipIsActive()
  if TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:IsActive() then
    zhuangbei._forceFinishTutorialEquip()
    TutorialQuestManager.QuestTutorialFirstGetWanaArmor:IsActive()
  end
end
function zhuangbei._forceFinishTutorialEquip()
  if TutorialQuestManager.QuestTutorialEquip:IsActive() then
    DebugOut("zhuangbei._forceFinishTutorialEquip")
    TutorialQuestManager.QuestTutorialEquip:SetFinish(true)
  end
end
function zhuangbei._forceTutorialFirstGetWanaArmor()
  if TutorialQuestManager.QuestTutorialFirstGetWanaArmor:IsActive() then
    DebugOut("zhuangbei._forceFinishTutorialFirstGetWanaArmor")
    TutorialQuestManager.QuestTutorialFirstGetWanaArmor:SetFinish(true)
  end
end
function zhuangbei:sendQuickEnhanceRequire(levelUp)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetId = fleets[zhuangbei.m_currentSelectFleetIndex].id
  local equip = zhuangbei:IsEquipmentInSlot(zhuangbei.m_currentSelectEquipSlot)
  if equip then
    local equipment_enhance_req_param = {
      equip_id = equip.item_id,
      fleet_id = fleetId,
      once_more_enhance = 1,
      en_level = levelUp
    }
    NetMessageMgr:SendMsg(NetAPIList.equipment_enhance_req.Code, equipment_enhance_req_param, zhuangbei.equipmentEnhanceCallback, true, nil)
    GameUtils:RecordForTongdui(NetAPIList.equipment_enhance_req.Code, equipment_enhance_req_param, zhuangbei.equipmentEnhanceCallback, true, nil)
  end
end
function zhuangbei:sendQuickIntervene(arg)
  local isUseAdditionItem, count = unpack(LuaUtils:string_split(arg, "|"))
  local list = self.filterEquip[zhuangbei.m_currentSelectEquipSlot]
  local item
  for k, v in pairs(list or {}) do
    if GameDataAccessHelper:IsItemUsedStrengthen(v.item_type) then
      item = v
      break
    end
  end
  local itemInFleet = self:IsEquipmentInSlot(zhuangbei.m_currentSelectEquipSlot)
  if itemInFleet then
    local equipments = GameGlobalData:GetData("equipments")
    for k, v in pairs(equipments) do
      if itemInFleet.item_id == v.equip_id then
        itemInFleet = v
        break
      end
    end
  end
  if item and itemInFleet then
    DebugOut("itemInFleet.enchant_item_id = ", itemInFleet.enchant_item_id)
    DebugOut("item.item_type = ", item.item_type)
    if item.item_type ~= itemInFleet.enchant_item_id and itemInFleet.enchant_item_id ~= 0 and itemInFleet.enchant_effect_value ~= 0 then
      zhuangbei:ShowPrimeRecoverConfirmBox2(isUseAdditionItem, tonumber(count))
    else
      zhuangbei:DoPrimeNew(isUseAdditionItem == "true", tonumber(count))
    end
  end
end
function zhuangbei:onClickEquip(strid)
  local itemid, str, fleetId, pos = unpack(LuaUtils:string_split(strid, "|"))
  local equip = self:IsEquipmentInSlot(zhuangbei.m_currentSelectEquipSlot)
  if str == "unload" then
    self:UnloadEquip(itemid)
  elseif str == "equip" then
    self:Equip(itemid)
  elseif str == "swap" then
    self:Swap(itemid, fleetId, pos)
  elseif str == "buy" then
    self.EquipmentOperatorBuyCallback()
  else
    assert("bad str " .. str)
  end
end
function zhuangbei.equipmentEnhanceCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.equipment_enhance_req.Code then
    if content.code ~= 0 then
      if not GameUICollect:CheckTutorialCollect(content.code) and content.code == 8604 then
        local text = AlertDataList:GetTextFromErrorCode(130)
        GameFleetEquipment.NeedMoreMoney(text)
      else
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
    else
      if GameFleetEquipmentPop:GetFlashObject() then
        GameStateEquipEnhance:GetPop():zb_showEnhanceAnim()
      end
      if QuestTutorialEnhance:IsActive() then
        QuestTutorialEnhance:SetFinish(true)
        if (immanentversion170 == 4 or immanentversion170 == 5) and not TutorialQuestManager.QuestTutorialPveMapPoint:IsFinished() then
          TutorialQuestManager.QuestTutorialPveMapPoint:SetActive(true, false)
        end
        if GameFleetEquipmentPop:GetFlashObject() then
          GameStateEquipEnhance:GetPop():zb_SetShowTutEnhance()
        end
        zhuangbei:CheckEnableQuckEnhance()
      end
    end
    return true
  end
  return false
end
function zhuangbei:HaveEnhanceCD()
  local viplimit = GameDataAccessHelper:GetVIPLimit("no_enhance_cd")
  local curLevel = GameVipDetailInfoPanel:GetVipLevel()
  return viplimit > curLevel
end
function zhuangbei:CheckCDCanEnhance()
  do return true end
  if not self:HaveEnhanceCD() then
    return true
  end
  return (...), self
end
function zhuangbei.clearCD()
  local param = {
    cdtype = GameUIGlobalScreen.equipmentCdType
  }
  NetMessageMgr:SendMsg(NetAPIList.cdtimes_clear_req.Code, param, zhuangbei.clearCDCallback, false)
end
function zhuangbei.clearCDCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.cdtimes_clear_req.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    end
    zhuangbei:OnEquipmentSelect()
    return true
  end
  return false
end
function zhuangbei:onClickEnhance(arg)
  if GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "upgradeEquip" then
    GameUtils:HideTutorialHelp()
  end
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetId = fleets[self.m_currentSelectFleetIndex].id
  local equip = self:IsEquipmentInSlot(self.m_currentSelectEquipSlot)
  if equip then
    if self:CheckCDCanEnhance() then
      local equipment_enhance_req_param = {
        equip_id = equip.item_id,
        fleet_id = fleetId,
        once_more_enhance = 0,
        en_level = 1
      }
      NetMessageMgr:SendMsg(NetAPIList.equipment_enhance_req.Code, equipment_enhance_req_param, self.equipmentEnhanceCallback, true, nil)
      GameUtils:RecordForTongdui(NetAPIList.equipment_enhance_req.Code, equipment_enhance_req_param, self.equipmentEnhanceCallback, true, nil)
    else
      if not GameSettingData.EnablePayRemind then
        GameSettingData.EnablePayRemind = true
        GameUtils:SaveSettingData()
      end
      if GameSettingData.EnablePayRemind then
        local callbackFunc = self.clearCD
        GameUIGlobalScreen:ShowClearCD("clear_equipment_enhance_cd", callbackFunc)
      else
        self.clearCD()
      end
    end
  else
    DebugOut("get quip failed")
  end
end
function zhuangbei:tipVipLevelNotEnoughForQuickEquipEnhance()
  local playerlevellimit = GameDataAccessHelper:GetLevelLimit("one_key_equip_enhance")
  local txt = GameUtils:TryGetText("LC_MENU_DEXTER_INFO_2", "")
  txt = string.format(txt, playerlevellimit)
  GameTip:Show(txt)
end
function zhuangbei.sendEquipQuickEnhanceInfoRequire()
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetId = fleets[zhuangbei.m_currentSelectFleetIndex].id
  local equip = zhuangbei:IsEquipmentInSlot(zhuangbei.m_currentSelectEquipSlot)
  local req_param = {
    equip_id = equip.item_id
  }
  NetMessageMgr:SendMsg(NetAPIList.enhance_info_req.Code, req_param, zhuangbei.equipQuickEnhanceInfoRequireCallback, true, nil)
end
function zhuangbei:GetCanUpCountCd()
  if immanentversion170 == 4 or immanentversion170 == 5 then
    zhuangbei.enhanceOneCD = 10
  end
  local left_time = 0
  local level = self.maxEnhanceLevel
  if self.enhanceCDTime then
    left_time = self.enhanceCDTime - os.time()
    level = self.maxEnhanceLevel - math.ceil(left_time / self.enhanceOneCD)
  end
  return level
end
function zhuangbei.getEquipInfoByEquipId(itemId)
  local equipments = GameGlobalData:GetData("equipments")
  local equipInfo = LuaUtils:table_values(LuaUtils:table_filter(equipments, function(k, v)
    return v.equip_id == itemId
  end))[1]
  DebugTable(equipInfo)
  return equipInfo
end
function zhuangbei.getEquipLevelByEquipId(itemId)
  local equipInfo = zhuangbei.getEquipInfoByEquipId(itemId)
  return equipInfo.equip_level
end
function zhuangbei.getEquipTypeByEquipId(itemId)
  local equipInfo = zhuangbei.getEquipInfoByEquipId(itemId)
  return equipInfo.equip_type
end
function zhuangbei.getQuickEnhanceCanUpLevelCount()
  DebugOut("zhuangbei.getQuickEnhanceCanUpLevelCount")
  DebugTable(zhuangbei.mlastReceivedEnhanceInfo)
  local equipSelectNow = zhuangbei:IsEquipmentInSlot(zhuangbei.m_currentSelectEquipSlot)
  DebugTable(equipSelectNow)
  return zhuangbei.mlastReceivedEnhanceInfo.max_level - zhuangbei.getEquipLevelByEquipId(equipSelectNow.item_id)
end
function zhuangbei.sendCommonEnhanceRequire()
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetId = fleets[zhuangbei.m_currentSelectFleetIndex].id
  local equip = zhuangbei:IsEquipmentInSlot(zhuangbei.m_currentSelectEquipSlot)
  if equip then
    local equipment_enhance_req_param = {
      equip_id = equip.item_id,
      fleet_id = fleetId,
      once_more_enhance = 0,
      en_level = 1
    }
    NetMessageMgr:SendMsg(NetAPIList.equipment_enhance_req.Code, equipment_enhance_req_param, zhuangbei.equipmentEnhanceCallback, true, nil)
    GameUtils:RecordForTongdui(NetAPIList.equipment_enhance_req.Code, equipment_enhance_req_param, zhuangbei.equipmentEnhanceCallback, true, nil)
  end
end
function zhuangbei.openQuickEnhanceUI()
  local lastReceivedEnhanceInfo = zhuangbei.mlastReceivedEnhanceInfo
  local equipSelectNow = zhuangbei:IsEquipmentInSlot(zhuangbei.m_currentSelectEquipSlot)
  local equipNameText = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. zhuangbei.getEquipTypeByEquipId(equipSelectNow.item_id))
  local equipLevelNow = zhuangbei.getEquipLevelByEquipId(equipSelectNow.item_id)
  local levelCanUp = zhuangbei.levelUp
  local baseLevelUpConsume = lastReceivedEnhanceInfo.consume
  local equipIcon = "item_" .. zhuangbei.getEquipTypeByEquipId(equipSelectNow.item_id)
  local equipmentAttrParams = {}
  for i, attrInfo in ipairs(lastReceivedEnhanceInfo.values) do
    local attrNameText = GameLoader:GetGameText("LC_MENU_Equip_param_" .. attrInfo.type)
    table.insert(equipmentAttrParams, attrNameText)
    table.insert(equipmentAttrParams, ",")
    table.insert(equipmentAttrParams, attrInfo.bassic)
    table.insert(equipmentAttrParams, ",")
    table.insert(equipmentAttrParams, attrInfo.mature)
    table.insert(equipmentAttrParams, "|")
  end
  local resource = GameGlobalData:GetData("resource")
  local money = resource.money
  local titleText = GameLoader:GetGameText("LC_MENU_AUTO_ENHANCE_BUTTON")
  local creditCost = GameGlobalData.GetCostCredit_a_b({item_type = "money"})
  local minCreditCost = GameGlobalData.GetMinCostCredit()
  GameStateEquipEnhance:GetPop():zb_openQuickEnhanceUI(titleText, equipIcon, equipNameText, equipLevelNow, levelCanUp, baseLevelUpConsume, table.concat(equipmentAttrParams), money, creditCost, minCreditCost)
end
function zhuangbei.useQuickEnhanceFunction()
  DebugOut("zhuangbei.useQuickEnhanceFunction")
  local cd_level = zhuangbei:GetCanUpCountCd()
  local levelCanUp = zhuangbei.getQuickEnhanceCanUpLevelCount()
  zhuangbei.levelUp = levelCanUp
  if cd_level and cd_level > 0 and cd_level < zhuangbei.levelUp and zhuangbei:HaveEnhanceCD() then
    DebugOut("-----ecIndex")
    zhuangbei.levelUp = cd_level
  end
  DebugOut("zhuangbei.levelUp = " .. zhuangbei.levelUp)
  if zhuangbei.levelUp <= 0 then
    zhuangbei.sendCommonEnhanceRequire()
  else
    zhuangbei.openQuickEnhanceUI()
  end
end
function zhuangbei.equipQuickEnhanceInfoRequireCallback(msgType, content)
  DebugOut("zhuangbei.equipQuickEnhanceInfoRequireCallback")
  DebugTable(content)
  if msgType == NetAPIList.enhance_info_ack.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    zhuangbei.mlastReceivedEnhanceInfo = content
    zhuangbei.useQuickEnhanceFunction()
    if QuestTutorialEnhance:IsActive() then
      zhuangbei:CheckEnableQuckEnhance()
    end
    return true
  elseif msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.bag_req.Code and content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function zhuangbei:CheckEnableQuckEnhance()
end
function zhuangbei:onClickQuickEnhance(arg)
  if GameVip:IsFastEnhanceUnlocked() then
    zhuangbei.sendEquipQuickEnhanceInfoRequire()
  else
    self:tipVipLevelNotEnoughForQuickEquipEnhance()
  end
end
function zhuangbei:tipVipLevelNotEnoughForQuickEquipEnhance()
  local playerlevellimit = GameDataAccessHelper:GetLevelLimit("one_key_equip_enhance")
  local txt = GameUtils:TryGetText("LC_MENU_DEXTER_INFO_2", "")
  txt = string.format(txt, playerlevellimit)
  GameTip:Show(txt)
end
function zhuangbei:getEvolutionUseablePlayerLevel()
  local limitLevel = GameDataAccessHelper:GetRightMenuPlayerReqLevel("equip_evolution")
  DebugOut("equip evolution level limit is:" .. limitLevel)
  return limitLevel
end
function zhuangbei:isEvolutionUseable()
  local playerLevel = GameGlobalData:GetData("levelinfo").level
  local evolutionUseablePlayerLevel = self:getEvolutionUseablePlayerLevel()
  DebugOut("playerLevel is:" .. playerLevel .. " evolutionUseableLevel is:" .. evolutionUseablePlayerLevel)
  return playerLevel >= evolutionUseablePlayerLevel
end
function zhuangbei:tipEvolllutionUnuseable()
  local tipStr = string.format(self:getEvolutionUnuseableTipStr(), self:getEvolutionUseablePlayerLevel())
  GameUtils:ShowTips(tipStr)
end
function zhuangbei:CheckFinishEvolutionTutorial()
  DebugOut("CheckFinishEvolutionTutorial:", self.m_currentSelectFleetIndex, ",", QuestTutorialEquipmentEvolution:IsActive(), ",", QuestTutorialEquipmentEvolution:IsFinished())
  if self.m_currentSelectFleetIndex == 1 and QuestTutorialEquipmentEvolution:IsActive() and not QuestTutorialEquipmentEvolution:IsFinished() then
    if GameFleetEquipmentPop:GetFlashObject() then
      GameStateEquipEnhance:GetPop():zb_SetEvolutionTipVisible(false)
    end
    QuestTutorialEquipmentEvolution:SetFinish(true)
  end
end
function zhuangbei:onClickEvolution(arg)
  if not self:isEvolutionUseable() then
    self:tipEvolllutionUnuseable()
    return
  end
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetId = fleets[self.m_currentSelectFleetIndex].id
  local equip = self:IsEquipmentInSlot(self.m_currentSelectEquipSlot)
  if equip then
    self:CheckFinishEvolutionTutorial()
    local equipment_evolution_req_param = {
      equip_id = equip.item_id,
      fleet_id = fleetId
    }
    DebugOut("Evolution")
    DebugTable(equipment_evolution_req_param)
    NetMessageMgr:SendMsg(NetAPIList.equipment_evolution_req.Code, equipment_evolution_req_param, self.equipmentEvolutionCallback, true, nil)
    GameUtils:RecordForTongdui(NetAPIList.equipment_evolution_req.Code, equipment_evolution_req_param, self.equipmentEvolutionCallback, true, nil)
  end
end
function zhuangbei:lockOrUnlockEvolutionBtnAccordUseable()
  if GameFleetEquipmentPop:GetFlashObject() then
    if self:isEvolutionUseable() then
      GameStateEquipEnhance:GetPop():zb_SetEvolutionLock(false)
    else
      GameStateEquipEnhance:GetPop():zb_SetEvolutionLock(true)
    end
  end
end
function zhuangbei:lockOrUnlockQuickEnhanceBtnAccordUseable()
  if GameFleetEquipmentPop:GetFlashObject() then
    if GameVip:IsFastEnhanceUnlocked() then
      GameStateEquipEnhance:GetPop():zb_SetQuickEnhanceLock(false)
    else
      GameStateEquipEnhance:GetPop():zb_SetQuickEnhanceLock(true)
    end
  end
end
function zhuangbei.equipmentEvolutionCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.equipment_evolution_req.Code then
    if content.code ~= 0 then
      if not GameUICollect:CheckTutorialCollect(content.code) and content.code == 130 then
        local text = AlertDataList:GetTextFromErrorCode(content.code)
        GameFleetEquipment.NeedMoreMoney(text)
      else
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
    end
    GameItemBag:RequestBag(false)
    local fleetinfo = GameGlobalData.GlobalData.fleetinfo.fleets
    for k, v in pairs(fleetinfo) do
      if v.id == zhuangbei.m_currentFleetsId then
        local enhanceCount = v.force - zhuangbei.m_curreetFleetsForce
        zhuangbei.m_curreetFleetsForce = v.force
        DebugOut("showEnhanceAnimation", enhanceCount, GameUtils.numberConversion(v.force))
      end
    end
    zhuangbei:RefreshFleetBagAndSlot()
    return true
  end
  return false
end
function zhuangbei.EquipmentOperatorBuyCallback()
  DebugOut("EquipmentOperatorBuyCallback", equip, zhuangbei.m_currentSelectEquipSlot)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetId = fleets[zhuangbei.m_currentSelectFleetIndex].id
  local shop_buy_and_wear_req_param = {
    formation_id = GameFleetEquipment:GetCurMatrixId(),
    fleet_id = fleetId,
    equip_type = zhuangbei.curBuyEquipType
  }
  NetMessageMgr:SendMsg(NetAPIList.shop_buy_and_wear_req.Code, shop_buy_and_wear_req_param, zhuangbei.BuyAndEquipCallback, true, nil)
  GameUtils:RecordForTongdui(NetAPIList.shop_buy_and_wear_req.Code, shop_buy_and_wear_req_param, zhuangbei.BuyAndEquipCallback, true, nil)
end
function zhuangbei.BuyAndEquipCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.shop_buy_and_wear_req.Code then
    if content.code ~= 0 then
      if not GameUICollect:CheckTutorialCollect(content.code) and content.code == 709 then
        local text = AlertDataList:GetTextFromErrorCode(130)
        GameFleetEquipment.NeedMoreMoney(text)
      else
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
    else
    end
    return true
  end
  return false
end
function zhuangbei:CheckTutorialEquip()
  if immanentversion == 2 then
    if QuestTutorialEquip:IsActive() and not QuestTutorialEquip:IsFinished() then
      AddFlurryEvent("FirstEquipSuccess", {}, 1)
      QuestTutorialEquip:SetFinish(true)
      AddFlurryEvent("TutorialEquip", {}, 2)
      if not QuestTutorialSkill:IsFinished() and not QuestTutorialSkill:IsActive() then
        local function callback()
          QuestTutorialSkill:SetActive(true)
          GameStateBattleMap:SetStoryWhenFocusGain({1100014})
          zhuangbei:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 1, false)
          zhuangbei:GetFlashObject():InvokeASCallback("_root", "SetShowEqipTip", false)
          zhuangbei:ShowCloseTip()
        end
        GameUICommonDialog:PlayStory({11000131}, callback)
      else
        zhuangbei:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 1, false)
        zhuangbei:GetFlashObject():InvokeASCallback("_root", "SetShowEqipTip", false)
        if QuestTutorialEnhance:IsActive() then
          zhuangbei:CheckTutorialEnhance(true)
        else
          zhuangbei:ShowCloseTip()
        end
      end
    elseif QuestTutorialEquipAllByOneKey:IsActive() and not QuestTutorialEquipAllByOneKey:IsFinished() then
      AddFlurryEvent("TutorialItem_Equip", {}, 2)
      QuestTutorialEquipAllByOneKey:SetFinish(true)
    end
  elseif immanentversion == 1 and QuestTutorialEquip:IsActive() then
    AddFlurryEvent("FirstEquipSuccess", {}, 1)
    QuestTutorialEquip:SetFinish(true)
    if immanentversion170 == 4 or immanentversion170 == 5 then
      TutorialQuestManager.QuestTutorialPveMapPoint:SetActive(true, false)
    end
    zhuangbei:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 1, false)
    zhuangbei:GetFlashObject():InvokeASCallback("_root", "SetShowEqipTip", false)
    if QuestTutorialEnhance:IsActive() then
      zhuangbei:CheckTutorialEnhance(true)
    elseif TutorialQuestManager.QuestTutorialFirstGetWanaArmor:IsActive() then
    else
      zhuangbei:ShowCloseTip()
    end
    GameStateBattleMap:SetStoryWhenFocusGain({1108})
  end
end
function zhuangbei:CheckTutorialEvolution(firstCanEvolution)
  DebugOut("Check QuestTutorialEquipmentEvolution:", QuestTutorialEquipmentEvolution:IsActive(), ",", QuestTutorialEquipmentEvolution:IsFinished(), ",", zhuangbei.m_currentSelectFleetIndex, ",", firstCanEvolution)
  if self.m_subState == k_subStateFleetEquipment or self.m_subState == k_subStateFleetEvolution or self.m_subState == k_subStateFleetStrengthen then
    return
  end
  if not QuestTutorialEquipmentEvolution:IsActive() or QuestTutorialEquipmentEvolution:IsFinished() then
    return
  end
  if 1 == zhuangbei.m_currentSelectFleetIndex then
    if firstCanEvolution then
      local flashObj = zhuangbei:GetFlashObject()
      if flashObj then
        flashObj:InvokeASCallback("_root", "SetShowEquipSlotTip", 1, true)
      end
    elseif self:IsEquipmentInSlot(1) then
      local flashObj = zhuangbei:GetFlashObject()
      if flashObj then
        flashObj:InvokeASCallback("_root", "SetShowEquipSlotTip", 1, false)
      end
      QuestTutorialEquipmentEvolution:SetFinish(true)
    else
      local flashObj = zhuangbei:GetFlashObject()
      if flashObj then
        flashObj:InvokeASCallback("_root", "SetShowEquipSlotTip", 1, true)
      end
    end
  else
    local flashObj = zhuangbei:GetFlashObject()
    if flashObj then
      flashObj:InvokeASCallback("_root", "SetShowEquipSlotTip", 1, false)
    end
  end
end
