local boss_info = GameData.wve_boss.boss_info
boss_info[6000000] = {
  ID = 6000000,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[6000001] = {
  ID = 6000001,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[6000002] = {
  ID = 6000002,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[6000003] = {
  ID = 6000003,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[6000004] = {
  ID = 6000004,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[6000005] = {
  ID = 6000005,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5600100] = {
  ID = 5600100,
  Avatar = "head13",
  Ship = "ship16"
}
boss_info[5600101] = {
  ID = 5600101,
  Avatar = "head12",
  Ship = "ship52"
}
boss_info[5600102] = {
  ID = 5600102,
  Avatar = "head13",
  Ship = "ship60"
}
boss_info[5600103] = {
  ID = 5600103,
  Avatar = "head25",
  Ship = "ship54"
}
boss_info[5600104] = {
  ID = 5600104,
  Avatar = "head24",
  Ship = "ship61"
}
boss_info[5600105] = {
  ID = 5600105,
  Avatar = "head5",
  Ship = "ship16"
}
boss_info[5600106] = {
  ID = 5600106,
  Avatar = "head13",
  Ship = "ship16"
}
boss_info[5600201] = {
  ID = 5600201,
  Avatar = "head12",
  Ship = "ship52"
}
boss_info[5600202] = {
  ID = 5600202,
  Avatar = "head13",
  Ship = "ship60"
}
boss_info[5600203] = {
  ID = 5600203,
  Avatar = "head25",
  Ship = "ship54"
}
boss_info[5600204] = {
  ID = 5600204,
  Avatar = "head24",
  Ship = "ship61"
}
boss_info[5600205] = {
  ID = 5600205,
  Avatar = "head5",
  Ship = "ship16"
}
boss_info[5600206] = {
  ID = 5600206,
  Avatar = "head13",
  Ship = "ship16"
}
boss_info[5600301] = {
  ID = 5600301,
  Avatar = "head12",
  Ship = "ship52"
}
boss_info[5600302] = {
  ID = 5600302,
  Avatar = "head13",
  Ship = "ship60"
}
boss_info[5600303] = {
  ID = 5600303,
  Avatar = "head25",
  Ship = "ship54"
}
boss_info[5600304] = {
  ID = 5600304,
  Avatar = "head24",
  Ship = "ship61"
}
boss_info[5600305] = {
  ID = 5600305,
  Avatar = "head5",
  Ship = "ship16"
}
boss_info[5600306] = {
  ID = 5600306,
  Avatar = "head13",
  Ship = "ship16"
}
boss_info[5600401] = {
  ID = 5600401,
  Avatar = "head12",
  Ship = "ship52"
}
boss_info[5600402] = {
  ID = 5600402,
  Avatar = "head13",
  Ship = "ship60"
}
boss_info[5600403] = {
  ID = 5600403,
  Avatar = "head25",
  Ship = "ship54"
}
boss_info[5600404] = {
  ID = 5600404,
  Avatar = "head24",
  Ship = "ship61"
}
boss_info[5600405] = {
  ID = 5600405,
  Avatar = "head5",
  Ship = "ship16"
}
boss_info[5600406] = {
  ID = 5600406,
  Avatar = "head13",
  Ship = "ship16"
}
boss_info[5600501] = {
  ID = 5600501,
  Avatar = "head13",
  Ship = "ship52"
}
boss_info[5600502] = {
  ID = 5600502,
  Avatar = "head13",
  Ship = "ship60"
}
boss_info[5600503] = {
  ID = 5600503,
  Avatar = "head13",
  Ship = "ship54"
}
boss_info[5600504] = {
  ID = 5600504,
  Avatar = "head13",
  Ship = "ship61"
}
boss_info[5600505] = {
  ID = 5600505,
  Avatar = "head13",
  Ship = "ship16"
}
boss_info[5600506] = {
  ID = 5600506,
  Avatar = "head77",
  Ship = "ship80"
}
boss_info[5600601] = {
  ID = 5600601,
  Avatar = "head13",
  Ship = "ship16"
}
boss_info[5600602] = {
  ID = 5600602,
  Avatar = "head13",
  Ship = "ship37"
}
boss_info[5600603] = {
  ID = 5600603,
  Avatar = "head13",
  Ship = "ship37"
}
boss_info[5600604] = {
  ID = 5600604,
  Avatar = "head13",
  Ship = "ship16"
}
boss_info[5600605] = {
  ID = 5600605,
  Avatar = "head13",
  Ship = "ship37"
}
boss_info[5600606] = {
  ID = 5600606,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5600701] = {
  ID = 5600701,
  Avatar = "head13",
  Ship = "ship16"
}
boss_info[5600702] = {
  ID = 5600702,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5600703] = {
  ID = 5600703,
  Avatar = "head13",
  Ship = "ship16"
}
boss_info[5600704] = {
  ID = 5600704,
  Avatar = "head13",
  Ship = "ship16"
}
boss_info[5600705] = {
  ID = 5600705,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5600706] = {
  ID = 5600706,
  Avatar = "head13",
  Ship = "ship16"
}
boss_info[5600801] = {
  ID = 5600801,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5600802] = {
  ID = 5600802,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5600803] = {
  ID = 5600803,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5600804] = {
  ID = 5600804,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5600805] = {
  ID = 5600805,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5600806] = {
  ID = 5600806,
  Avatar = "head13",
  Ship = "ship16"
}
boss_info[5600901] = {
  ID = 5600901,
  Avatar = "head13",
  Ship = "ship16"
}
boss_info[5600902] = {
  ID = 5600902,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5600903] = {
  ID = 5600903,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5600904] = {
  ID = 5600904,
  Avatar = "head13",
  Ship = "ship16"
}
boss_info[5600905] = {
  ID = 5600905,
  Avatar = "head13",
  Ship = "ship16"
}
boss_info[5600906] = {
  ID = 5600906,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601001] = {
  ID = 5601001,
  Avatar = "head13",
  Ship = "ship16"
}
boss_info[5601002] = {
  ID = 5601002,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601003] = {
  ID = 5601003,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601004] = {
  ID = 5601004,
  Avatar = "head13",
  Ship = "ship16"
}
boss_info[5601005] = {
  ID = 5601005,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601006] = {
  ID = 5601006,
  Avatar = "head77",
  Ship = "ship80"
}
boss_info[5601101] = {
  ID = 5601101,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601102] = {
  ID = 5601102,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601103] = {
  ID = 5601103,
  Avatar = "head13",
  Ship = "ship16"
}
boss_info[5601104] = {
  ID = 5601104,
  Avatar = "head8",
  Ship = "ship5"
}
boss_info[5601105] = {
  ID = 5601105,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601106] = {
  ID = 5601106,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601201] = {
  ID = 5601201,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601202] = {
  ID = 5601202,
  Avatar = "head13",
  Ship = "ship16"
}
boss_info[5601203] = {
  ID = 5601203,
  Avatar = "head13",
  Ship = "ship16"
}
boss_info[5601204] = {
  ID = 5601204,
  Avatar = "head13",
  Ship = "ship16"
}
boss_info[5601205] = {
  ID = 5601205,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601206] = {
  ID = 5601206,
  Avatar = "head13",
  Ship = "ship16"
}
boss_info[5601301] = {
  ID = 5601301,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601302] = {
  ID = 5601302,
  Avatar = "head13",
  Ship = "ship16"
}
boss_info[5601303] = {
  ID = 5601303,
  Avatar = "head13",
  Ship = "ship16"
}
boss_info[5601304] = {
  ID = 5601304,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601305] = {
  ID = 5601305,
  Avatar = "head13",
  Ship = "ship16"
}
boss_info[5601306] = {
  ID = 5601306,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601401] = {
  ID = 5601401,
  Avatar = "head13",
  Ship = "ship16"
}
boss_info[5601402] = {
  ID = 5601402,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601403] = {
  ID = 5601403,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601404] = {
  ID = 5601404,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601405] = {
  ID = 5601405,
  Avatar = "head13",
  Ship = "ship16"
}
boss_info[5601406] = {
  ID = 5601406,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5601501] = {
  ID = 5601501,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601502] = {
  ID = 5601502,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601503] = {
  ID = 5601503,
  Avatar = "head13",
  Ship = "ship16"
}
boss_info[5601504] = {
  ID = 5601504,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601505] = {
  ID = 5601505,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601506] = {
  ID = 5601506,
  Avatar = "head77",
  Ship = "ship80"
}
boss_info[5601601] = {
  ID = 5601601,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601602] = {
  ID = 5601602,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601603] = {
  ID = 5601603,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601604] = {
  ID = 5601604,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601605] = {
  ID = 5601605,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601606] = {
  ID = 5601606,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601701] = {
  ID = 5601701,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601702] = {
  ID = 5601702,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601703] = {
  ID = 5601703,
  Avatar = "head13",
  Ship = "ship16"
}
boss_info[5601704] = {
  ID = 5601704,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601705] = {
  ID = 5601705,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601706] = {
  ID = 5601706,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601801] = {
  ID = 5601801,
  Avatar = "head13",
  Ship = "ship16"
}
boss_info[5601802] = {
  ID = 5601802,
  Avatar = "head13",
  Ship = "ship47"
}
boss_info[5601803] = {
  ID = 5601803,
  Avatar = "head24",
  Ship = "ship5"
}
boss_info[5601804] = {
  ID = 5601804,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5601805] = {
  ID = 5601805,
  Avatar = "head12",
  Ship = "ship16"
}
boss_info[5601806] = {
  ID = 5601806,
  Avatar = "head5",
  Ship = "ship16"
}
boss_info[5601901] = {
  ID = 5601901,
  Avatar = "head12",
  Ship = "ship16"
}
boss_info[5601902] = {
  ID = 5601902,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5601903] = {
  ID = 5601903,
  Avatar = "head12",
  Ship = "ship5"
}
boss_info[5601904] = {
  ID = 5601904,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5601905] = {
  ID = 5601905,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5601906] = {
  ID = 5601906,
  Avatar = "head12",
  Ship = "ship16"
}
boss_info[5602001] = {
  ID = 5602001,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5602002] = {
  ID = 5602002,
  Avatar = "head12",
  Ship = "ship5"
}
boss_info[5602003] = {
  ID = 5602003,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5602004] = {
  ID = 5602004,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5602005] = {
  ID = 5602005,
  Avatar = "head12",
  Ship = "ship16"
}
boss_info[5602006] = {
  ID = 5602006,
  Avatar = "head77",
  Ship = "ship80"
}
boss_info[5602101] = {
  ID = 5602101,
  Avatar = "head12",
  Ship = "ship5"
}
boss_info[5602102] = {
  ID = 5602102,
  Avatar = "head25",
  Ship = "ship5"
}
boss_info[5602103] = {
  ID = 5602103,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5602104] = {
  ID = 5602104,
  Avatar = "head12",
  Ship = "ship16"
}
boss_info[5602105] = {
  ID = 5602105,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5602106] = {
  ID = 5602106,
  Avatar = "head12",
  Ship = "ship5"
}
boss_info[5602201] = {
  ID = 5602201,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5602202] = {
  ID = 5602202,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5602203] = {
  ID = 5602203,
  Avatar = "head12",
  Ship = "ship16"
}
boss_info[5602204] = {
  ID = 5602204,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5602205] = {
  ID = 5602205,
  Avatar = "head12",
  Ship = "ship5"
}
boss_info[5602206] = {
  ID = 5602206,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5602301] = {
  ID = 5602301,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5602302] = {
  ID = 5602302,
  Avatar = "head12",
  Ship = "ship16"
}
boss_info[5602303] = {
  ID = 5602303,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5602304] = {
  ID = 5602304,
  Avatar = "head12",
  Ship = "ship5"
}
boss_info[5602305] = {
  ID = 5602305,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5602306] = {
  ID = 5602306,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5602401] = {
  ID = 5602401,
  Avatar = "head12",
  Ship = "ship16"
}
boss_info[5602402] = {
  ID = 5602402,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5602403] = {
  ID = 5602403,
  Avatar = "head12",
  Ship = "ship5"
}
boss_info[5602404] = {
  ID = 5602404,
  Avatar = "head24",
  Ship = "ship5"
}
boss_info[5602405] = {
  ID = 5602405,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5602406] = {
  ID = 5602406,
  Avatar = "head12",
  Ship = "ship16"
}
boss_info[5602501] = {
  ID = 5602501,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5602502] = {
  ID = 5602502,
  Avatar = "head12",
  Ship = "ship5"
}
boss_info[5602503] = {
  ID = 5602503,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5602504] = {
  ID = 5602504,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5602505] = {
  ID = 5602505,
  Avatar = "head12",
  Ship = "ship16"
}
boss_info[5602506] = {
  ID = 5602506,
  Avatar = "head77",
  Ship = "ship80"
}
boss_info[5602601] = {
  ID = 5602601,
  Avatar = "head12",
  Ship = "ship5"
}
boss_info[5602602] = {
  ID = 5602602,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5602603] = {
  ID = 5602603,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5602604] = {
  ID = 5602604,
  Avatar = "head12",
  Ship = "ship16"
}
boss_info[5602605] = {
  ID = 5602605,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5602606] = {
  ID = 5602606,
  Avatar = "head12",
  Ship = "ship5"
}
boss_info[5602701] = {
  ID = 5602701,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5602702] = {
  ID = 5602702,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5602703] = {
  ID = 5602703,
  Avatar = "head12",
  Ship = "ship16"
}
boss_info[5602704] = {
  ID = 5602704,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5602705] = {
  ID = 5602705,
  Avatar = "head12",
  Ship = "ship5"
}
boss_info[5602706] = {
  ID = 5602706,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5602801] = {
  ID = 5602801,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5602802] = {
  ID = 5602802,
  Avatar = "head12",
  Ship = "ship16"
}
boss_info[5602803] = {
  ID = 5602803,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5602804] = {
  ID = 5602804,
  Avatar = "head12",
  Ship = "ship5"
}
boss_info[5602805] = {
  ID = 5602805,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5602806] = {
  ID = 5602806,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5602901] = {
  ID = 5602901,
  Avatar = "head12",
  Ship = "ship16"
}
boss_info[5602902] = {
  ID = 5602902,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5602903] = {
  ID = 5602903,
  Avatar = "head12",
  Ship = "ship5"
}
boss_info[5602904] = {
  ID = 5602904,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5602905] = {
  ID = 5602905,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5602906] = {
  ID = 5602906,
  Avatar = "head12",
  Ship = "ship16"
}
boss_info[5603001] = {
  ID = 5603001,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5603002] = {
  ID = 5603002,
  Avatar = "head12",
  Ship = "ship5"
}
boss_info[5603003] = {
  ID = 5603003,
  Avatar = "head24",
  Ship = "ship47"
}
boss_info[5603004] = {
  ID = 5603004,
  Avatar = "head12",
  Ship = "ship5"
}
boss_info[5603005] = {
  ID = 5603005,
  Avatar = "head12",
  Ship = "ship16"
}
boss_info[5603006] = {
  ID = 5603006,
  Avatar = "head77",
  Ship = "ship80"
}
boss_info[5603101] = {
  ID = 5603101,
  Avatar = "head12",
  Ship = "ship5"
}
boss_info[5603102] = {
  ID = 5603102,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5603103] = {
  ID = 5603103,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5603104] = {
  ID = 5603104,
  Avatar = "head12",
  Ship = "ship16"
}
boss_info[5603105] = {
  ID = 5603105,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5603106] = {
  ID = 5603106,
  Avatar = "head12",
  Ship = "ship5"
}
boss_info[5603201] = {
  ID = 5603201,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5603202] = {
  ID = 5603202,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5603203] = {
  ID = 5603203,
  Avatar = "head12",
  Ship = "ship16"
}
boss_info[5603204] = {
  ID = 5603204,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5603205] = {
  ID = 5603205,
  Avatar = "head12",
  Ship = "ship5"
}
boss_info[5603206] = {
  ID = 5603206,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5603301] = {
  ID = 5603301,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5603302] = {
  ID = 5603302,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5603303] = {
  ID = 5603303,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5603304] = {
  ID = 5603304,
  Avatar = "head12",
  Ship = "ship5"
}
boss_info[5603305] = {
  ID = 5603305,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5603306] = {
  ID = 5603306,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5603401] = {
  ID = 5603401,
  Avatar = "head12",
  Ship = "ship16"
}
boss_info[5603402] = {
  ID = 5603402,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5603403] = {
  ID = 5603403,
  Avatar = "head12",
  Ship = "ship5"
}
boss_info[5603404] = {
  ID = 5603404,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5603405] = {
  ID = 5603405,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5603406] = {
  ID = 5603406,
  Avatar = "head12",
  Ship = "ship16"
}
boss_info[5603501] = {
  ID = 5603501,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5603502] = {
  ID = 5603502,
  Avatar = "head12",
  Ship = "ship5"
}
boss_info[5603503] = {
  ID = 5603503,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5603504] = {
  ID = 5603504,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5603505] = {
  ID = 5603505,
  Avatar = "head16",
  Ship = "ship5"
}
boss_info[5603506] = {
  ID = 5603506,
  Avatar = "head77",
  Ship = "ship80"
}
boss_info[5603601] = {
  ID = 5603601,
  Avatar = "head12",
  Ship = "ship5"
}
boss_info[5603602] = {
  ID = 5603602,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5603603] = {
  ID = 5603603,
  Avatar = "head12",
  Ship = "ship47"
}
boss_info[5603604] = {
  ID = 5603604,
  Avatar = "head25",
  Ship = "ship16"
}
boss_info[5603605] = {
  ID = 5603605,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5603606] = {
  ID = 5603606,
  Avatar = "head25",
  Ship = "ship5"
}
boss_info[5603701] = {
  ID = 5603701,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5603702] = {
  ID = 5603702,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5603703] = {
  ID = 5603703,
  Avatar = "head25",
  Ship = "ship16"
}
boss_info[5603704] = {
  ID = 5603704,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5603705] = {
  ID = 5603705,
  Avatar = "head25",
  Ship = "ship5"
}
boss_info[5603706] = {
  ID = 5603706,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5603801] = {
  ID = 5603801,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5603802] = {
  ID = 5603802,
  Avatar = "head25",
  Ship = "ship16"
}
boss_info[5603803] = {
  ID = 5603803,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5603804] = {
  ID = 5603804,
  Avatar = "head25",
  Ship = "ship5"
}
boss_info[5603805] = {
  ID = 5603805,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5603806] = {
  ID = 5603806,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5603901] = {
  ID = 5603901,
  Avatar = "head12",
  Ship = "ship5"
}
boss_info[5603902] = {
  ID = 5603902,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5603903] = {
  ID = 5603903,
  Avatar = "head25",
  Ship = "ship5"
}
boss_info[5603904] = {
  ID = 5603904,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5603905] = {
  ID = 5603905,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5603906] = {
  ID = 5603906,
  Avatar = "head25",
  Ship = "ship16"
}
boss_info[5604001] = {
  ID = 5604001,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604002] = {
  ID = 5604002,
  Avatar = "head25",
  Ship = "ship5"
}
boss_info[5604003] = {
  ID = 5604003,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604004] = {
  ID = 5604004,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604005] = {
  ID = 5604005,
  Avatar = "head25",
  Ship = "ship16"
}
boss_info[5604006] = {
  ID = 5604006,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604101] = {
  ID = 5604101,
  Avatar = "head25",
  Ship = "ship5"
}
boss_info[5604102] = {
  ID = 5604102,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604103] = {
  ID = 5604103,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604104] = {
  ID = 5604104,
  Avatar = "head25",
  Ship = "ship16"
}
boss_info[5604105] = {
  ID = 5604105,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604106] = {
  ID = 5604106,
  Avatar = "head25",
  Ship = "ship5"
}
boss_info[5604201] = {
  ID = 5604201,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604202] = {
  ID = 5604202,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604203] = {
  ID = 5604203,
  Avatar = "head24",
  Ship = "ship42"
}
boss_info[5604204] = {
  ID = 5604204,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604205] = {
  ID = 5604205,
  Avatar = "head25",
  Ship = "ship5"
}
boss_info[5604206] = {
  ID = 5604206,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604301] = {
  ID = 5604301,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604302] = {
  ID = 5604302,
  Avatar = "head25",
  Ship = "ship16"
}
boss_info[5604303] = {
  ID = 5604303,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604304] = {
  ID = 5604304,
  Avatar = "head25",
  Ship = "ship5"
}
boss_info[5604305] = {
  ID = 5604305,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604306] = {
  ID = 5604306,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604401] = {
  ID = 5604401,
  Avatar = "head25",
  Ship = "ship16"
}
boss_info[5604402] = {
  ID = 5604402,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604403] = {
  ID = 5604403,
  Avatar = "head25",
  Ship = "ship5"
}
boss_info[5604404] = {
  ID = 5604404,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604405] = {
  ID = 5604405,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604406] = {
  ID = 5604406,
  Avatar = "head24",
  Ship = "ship5"
}
boss_info[5604501] = {
  ID = 5604501,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604502] = {
  ID = 5604502,
  Avatar = "head25",
  Ship = "ship5"
}
boss_info[5604503] = {
  ID = 5604503,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604504] = {
  ID = 5604504,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604505] = {
  ID = 5604505,
  Avatar = "head25",
  Ship = "ship16"
}
boss_info[5604506] = {
  ID = 5604506,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604601] = {
  ID = 5604601,
  Avatar = "head25",
  Ship = "ship5"
}
boss_info[5604602] = {
  ID = 5604602,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604603] = {
  ID = 5604603,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604604] = {
  ID = 5604604,
  Avatar = "head25",
  Ship = "ship16"
}
boss_info[5604605] = {
  ID = 5604605,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604606] = {
  ID = 5604606,
  Avatar = "head25",
  Ship = "ship5"
}
boss_info[5604701] = {
  ID = 5604701,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604702] = {
  ID = 5604702,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604703] = {
  ID = 5604703,
  Avatar = "head25",
  Ship = "ship16"
}
boss_info[5604704] = {
  ID = 5604704,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604705] = {
  ID = 5604705,
  Avatar = "head25",
  Ship = "ship5"
}
boss_info[5604706] = {
  ID = 5604706,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604801] = {
  ID = 5604801,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604802] = {
  ID = 5604802,
  Avatar = "head7",
  Ship = "ship42"
}
boss_info[5604803] = {
  ID = 5604803,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604804] = {
  ID = 5604804,
  Avatar = "head25",
  Ship = "ship5"
}
boss_info[5604805] = {
  ID = 5604805,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604806] = {
  ID = 5604806,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604901] = {
  ID = 5604901,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604902] = {
  ID = 5604902,
  Avatar = "head7",
  Ship = "ship42"
}
boss_info[5604903] = {
  ID = 5604903,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604904] = {
  ID = 5604904,
  Avatar = "head25",
  Ship = "ship5"
}
boss_info[5604905] = {
  ID = 5604905,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5604906] = {
  ID = 5604906,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5605001] = {
  ID = 5605001,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5605002] = {
  ID = 5605002,
  Avatar = "head7",
  Ship = "ship42"
}
boss_info[5605003] = {
  ID = 5605003,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5605004] = {
  ID = 5605004,
  Avatar = "head25",
  Ship = "ship5"
}
boss_info[5605005] = {
  ID = 5605005,
  Avatar = "head25",
  Ship = "ship47"
}
boss_info[5605006] = {
  ID = 5605006,
  Avatar = "head25",
  Ship = "ship47"
}
