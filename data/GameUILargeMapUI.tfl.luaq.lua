require("GameTextEdit.tfl")
local GameUILargeMapUI = LuaObjectManager:GetLuaObject("GameUILargeMapUI")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameUILargeMap = LuaObjectManager:GetLuaObject("GameUILargeMap")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIPlayerView = LuaObjectManager:GetLuaObject("GameUIPlayerView")
local GameStateLargeMapAlliance = GameStateManager.GameStateLargeMapAlliance
GameUILargeMapUI.TYPE_BLOCK_DETAIL = 1
GameUILargeMapUI.TYPE_MASS_FLEETS = 2
GameUILargeMapUI.TYPE_MASS_MEMBERS = 3
GameUILargeMapUI.TYPE_MASS_INVITE = 4
GameUILargeMapUI.TYPE_SHIP_REPAIR = 5
GameUILargeMapUI.baseData = nil
GameUILargeMapUI.curShowType = 1
GameUILargeMapUI.paramData = nil
GameUILargeMapUI.blockDetailInfo = nil
GameUILargeMapUI.massListData = nil
GameUILargeMapUI.massTroopInfoData = nil
GameUILargeMapUI.massInviteListData = nil
GameUILargeMapUI.repairData = nil
GameUILargeMapUI.lastSearchName = ""
function GameUILargeMapUI:OnInitGame()
end
function GameUILargeMapUI:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  GameUILargeMapUI:ReqMainData()
end
function GameUILargeMapUI:OnEraseFromGameState()
  DebugOut("GameUILargeMapUI:OnEraseFromGameState")
  self:UnloadFlashObject()
  GameUILargeMapUI.paramData = nil
  GameUILargeMapUI.curShowType = 1
  GameUILargeMapUI.blockDetailInfo = nil
  GameUILargeMapUI.massTroopInfoData = nil
  GameUILargeMapUI.massInviteListData = nil
  GameUILargeMapUI.repairData = nil
  GameUILargeMapUI.massListData = nil
  GameUILargeMapUI.lastSearchName = ""
end
function GameUILargeMapUI:OnFSCommand(cmd, arg)
  GameTextEdit:fscommand(self:GetFlashObject(), cmd, arg)
  if cmd == "close" then
    GameStateManager:GetCurrentGameState():EraseObject(GameUILargeMapUI)
  elseif cmd == "GotoBlock" then
    GameUILargeMap:ReqRouteData(1, tonumber(arg))
    GameStateManager:GetCurrentGameState():EraseObject(GameUILargeMapUI)
  elseif cmd == "OccupyBlock" then
    GameUILargeMap:ReqRouteData(2, tonumber(arg))
    GameStateManager:GetCurrentGameState():EraseObject(GameUILargeMapUI)
  elseif cmd == "update_mass_troop_item" then
    local itemData = GameUILargeMapUI.massListData.troopList[tonumber(arg)]
    if GameUILargeMapUI:GetFlashObject() then
      GameUILargeMapUI:GetFlashObject():InvokeASCallback("_root", "UpdateMassTroopItem", tonumber(arg), itemData)
    end
  elseif cmd == "MassRefress" then
    GameUILargeMapUI:ReqMassList()
  elseif cmd == "CloseMassList" then
    GameStateManager:GetCurrentGameState():EraseObject(GameUILargeMapUI)
  elseif cmd == "CloseMassMember" then
    GameUILargeMapUI.massTroopInfoData = nil
    GameStateManager:GetCurrentGameState():EraseObject(GameUILargeMapUI)
  elseif cmd == "ApplyJoinTroop" then
    GameUILargeMapUI:ReqJoinTroop(arg)
  elseif cmd == "CreateMassPop" then
    local id, needEnergy = unpack(LuaUtils:string_split(arg, "|"))
    GameUILargeMapUI:SetCreateMassPopUI(id, tonumber(needEnergy))
    GameUILargeMapUI:ShowCreateMassPop()
  elseif cmd == "CreateMass" then
    local blockid, force, count = unpack(LuaUtils:string_split(arg, "|"))
    GameUILargeMapUI:CreateMassTroop(tonumber(blockid), tonumber(force), tonumber(count))
  elseif cmd == "Dissolve" then
    GameUILargeMapUI:ReqDissolveMassTroop()
  elseif cmd == "CloseMemberPop" then
    GameStateManager:GetCurrentGameState():EraseObject(GameUILargeMapUI)
  elseif cmd == "InvitePlayerPop" then
    GameUILargeMapUI:ReqMassInviteList("")
    GameUILargeMapUI:ShowMassInviteListUI()
  elseif cmd == "StartWar" then
    GameUILargeMapUI:StartMassWar()
  elseif cmd == "SearchInvitePlayer" then
    GameUILargeMapUI:ReqMassInviteList(arg)
  elseif cmd == "update_mass_invite_item" then
    local itemData = GameUILargeMapUI.massInviteListData.playerList[tonumber(arg)]
    if GameUILargeMapUI:GetFlashObject() then
      GameUILargeMapUI:GetFlashObject():InvokeASCallback("_root", "UpdateMassInviteItem", tonumber(arg), itemData)
    end
  elseif cmd == "CloseInvitePop" then
    GameUILargeMapUI.massInviteListData = nil
    GameUILargeMapUI.lastSearchName = ""
    GameUILargeMapUI:ReqMassMemberList()
  elseif cmd == "InvitePlayer" then
    GameUILargeMapUI:ReqInvitePlayer(arg, GameUILargeMapUI.lastSearchName)
  elseif cmd == "OnekeyInvite" then
    GameUILargeMapUI:ReqInvitePlayer("", GameUILargeMapUI.lastSearchName)
  elseif cmd == "ExitMass" then
    GameUILargeMapUI:ReqExitMassTroop(arg)
  elseif cmd == "ViewPlayer" then
    GameUILargeMapUI:ReqSimplePlayerInfo(arg)
  elseif cmd == "RemoveMassPlayer" then
    GameUILargeMapUI:ReqRemoveMassPlayer(arg)
  elseif cmd == "ViewDetailInfo" then
    GameUILargeMapUI:ViewPlayerDetailInfo(arg)
  elseif cmd == "CloseRepaire" then
    GameStateManager:GetCurrentGameState():EraseObject(GameUILargeMapUI)
  elseif cmd == "RepaireShip" then
    GameUILargeMapUI:ReqRepairShip(tonumber(arg))
  elseif cmd == "update_repair_item" then
    local itemData = GameUILargeMapUI.repairData.avaterList[tonumber(arg)]
    if GameUILargeMapUI:GetFlashObject() then
      GameUILargeMapUI:GetFlashObject():InvokeASCallback("_root", "UpdateRepairItem", tonumber(arg), itemData)
    end
  elseif cmd == "SelectedRepairIndex" then
    local itemData = GameUILargeMapUI.repairData.avaterList[GameUILargeMapUI.repairData.selectedIndex]
    itemData.selectStatus = "idle"
    GameUILargeMapUI:OnFSCommand("update_repair_item", GameUILargeMapUI.repairData.selectedIndex)
    GameUILargeMapUI.repairData.selectedIndex = tonumber(arg)
    itemData = GameUILargeMapUI.repairData.avaterList[GameUILargeMapUI.repairData.selectedIndex]
    itemData.selectStatus = "selected"
    GameUILargeMapUI:OnFSCommand("update_repair_item", GameUILargeMapUI.repairData.selectedIndex)
    GameUILargeMapUI:SetSelectedRepairShipData(GameUILargeMapUI.repairData.selectedIndex, itemData)
  end
end
function GameUILargeMapUI:Show(baseData, param, showType)
  GameUILargeMapUI.curShowType = showType
  GameUILargeMapUI.baseData = baseData
  GameUILargeMapUI.paramData = param
  if GameUILargeMapUI:GetFlashObject() then
    GameUILargeMapUI:ReqMainData()
  else
    GameStateManager:GetCurrentGameState():AddObject(GameUILargeMapUI)
  end
end
function GameUILargeMapUI:Hide()
  if GameUILargeMapUI.curShowType == GameUILargeMapUI.TYPE_MASS_FLEETS then
    GameUILargeMapUI:OnFSCommand("CloseMassList")
  elseif GameUILargeMapUI.curShowType == GameUILargeMapUI.TYPE_MASS_MEMBERS then
    GameUILargeMapUI:OnFSCommand("CloseMassMember")
  else
    GameStateManager:GetCurrentGameState():EraseObject(GameUILargeMapUI)
  end
end
function GameUILargeMapUI:HideAll()
  if GameUILargeMapUI:GetFlashObject() then
    GameUILargeMapUI:GetFlashObject():InvokeASCallback("_root", "HideAllUI")
  end
end
function GameUILargeMapUI:SetShowType()
  if GameUILargeMapUI:GetFlashObject() then
    GameUILargeMapUI:GetFlashObject():InvokeASCallback("_root", "SetShowType", GameUILargeMapUI.curShowType)
  end
end
function GameUILargeMapUI:ReqMainData()
  if GameUILargeMapUI.curShowType == GameUILargeMapUI.TYPE_BLOCK_DETAIL then
    GameUILargeMapUI:ReqBlockInfo(GameUILargeMapUI.paramData.blockId)
  elseif GameUILargeMapUI.curShowType == GameUILargeMapUI.TYPE_MASS_FLEETS then
    GameUILargeMapUI:ReqMassList()
    GameUILargeMapUI:ShowMassListUI()
  elseif GameUILargeMapUI.curShowType == GameUILargeMapUI.TYPE_MASS_MEMBERS then
    GameUILargeMapUI:ReqMassMemberList()
    GameUILargeMapUI:ShowMassTroopMemberUI()
  elseif GameUILargeMapUI.curShowType == GameUILargeMapUI.TYPE_MASS_INVITE then
    GameUILargeMapUI:ReqMassInviteList("")
  elseif GameUILargeMapUI.curShowType == GameUILargeMapUI.TYPE_SHIP_REPAIR then
    GameUILargeMapUI:ReqRepairData()
    GameUILargeMapUI:ShowRepairUI()
  end
end
function GameUILargeMapUI:ReqBlockInfo(blockId)
  local param = {}
  param.id = blockId
  NetMessageMgr:SendMsg(NetAPIList.large_map_block_detail_req.Code, param, GameUILargeMapUI.LargeMapBlockInfoCallBack, true, nil)
end
function GameUILargeMapUI.LargeMapBlockInfoCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_block_detail_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    GameStateManager:GetCurrentGameState():EraseObject(GameUILargeMapUI)
    return true
  elseif msgType == NetAPIList.large_map_block_detail_ack.Code then
    GameUILargeMapUI:GenerateBlockInfoData(content)
    GameUILargeMapUI:SetBlockInfoUI(GameUILargeMapUI.blockDetailInfo)
    GameUILargeMapUI:ShowBlockInfoUI()
    return true
  end
  return false
end
function GameUILargeMapUI:GenerateBlockInfoData(content)
  local data = {}
  data.id = content.id
  data.blockName = GameLoader:GetGameText(content.block_name_key) .. "(" .. data.id .. ")"
  data.belongSelf = GameUILargeMapUI.baseData.owner_code == content.owner_code and GameUILargeMapUI.baseData.owner_code ~= 0
  data.isOn = GameUILargeMapUI.baseData.pos == content.id
  data.status = GameUILargeMapUI.baseData.status
  data.enableWatch = GameUILargeMapUI.paramData.enabldShow
  data.unknowText = GameLoader:GetGameText("LC_MENU_MAP_DISPLAY_UNKOWN_AREA")
  data.allianceName = GameLoader:GetGameText("LC_MENU_MAP_FIGHT_ATTRIBUTION") .. GameLoader:GetGameText("LC_MENU_MAP_FIGHT_NO_COUNTRY")
  data.rate = GameLoader:GetGameText("LC_MENU_MAP_FIGHT_TAX_RATE") .. "0%"
  data.outputText = GameLoader:GetGameText("LC_MENU_MAP_FIGHT_OUTPUT_REMAINING")
  data.gotoText = GameLoader:GetGameText("LC_MENU_MAP_FIGHT_GO")
  data.fightText = GameLoader:GetGameText("LC_MENU_MAP_FIGHT_OCCUPIED")
  data.selfArmyText = GameLoader:GetGameText("LC_MENU_MAP_DISPLAY_STATIONED_FLEET")
  data.selfArmyCount = content.self_player_count
  data.otherArmyText = GameLoader:GetGameText("LC_MENU_MAP_DISPLAY_STAY_FLEET")
  data.otherArmyCount = content.other_player_count
  data.warningCount = #content.warning_items
  data.warningText = ""
  if 0 < data.warningCount then
    local warningText = GameLoader:GetGameText("LC_MENU_MAP_DISPLAY_EARLY_WARNING_TIPS")
    warningText = string.gsub(warningText, "<number1>", content.warning_items[1].alliance_name)
    data.warningText = string.gsub(warningText, "<number2>", tostring(content.warning_items[1].player_count))
  end
  data.isBorder = content.is_border
  data.troopsID = GameUILargeMapUI.baseData.troop_id
  if content.alliance_info.id ~= "" then
    data.allianceName = GameLoader:GetGameText("LC_MENU_MAP_FIGHT_ATTRIBUTION") .. content.alliance_info.name
    data.rate = GameLoader:GetGameText("LC_MENU_MAP_FIGHT_TAX_RATE") .. content.alliance_info.rate
  end
  data.consumeEnergy = content.consume_energy
  data.needEnergy = GameLoader:GetGameText("LC_MENU_MAP_FIGHT_POWER_CONSUMPTION") .. content.consume_energy
  data.resList = {}
  for k, v in ipairs(content.res_output) do
    local item = {}
    item.count = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
    item.displayCount = GameUtils.numberConversion(math.floor(item.count))
    item.maxDisplayCount = GameUtils.numberConversion(math.floor(v.max))
    item.frame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v)
    item.model = v
    item.disPlayText = GameLoader:GetGameText("LC_MENU_MAP_OUTPUT_NUMBER")
    item.disPlayText = string.gsub(item.disPlayText, "<number1>", tostring(item.displayCount))
    if item.maxDisplayCount == 0 then
      item.disPlayText = string.gsub(item.disPlayText, "<number2>", GameLoader:GetGameText("LC_MENU_MAP_DISPLAY_UNKOWN_AREA"))
    else
      item.disPlayText = string.gsub(item.disPlayText, "<number2>", tostring(item.maxDisplayCount))
    end
    data.resList[#data.resList + 1] = item
  end
  data.getResText = GameLoader:GetGameText("LC_MENU_MAP_FIGHT_HAS_BEEN_COLLECTED") .. ":"
  data.getList = {}
  for k, v in pairs(content.get_res) do
    local item = {}
    item.count = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
    item.displayCount = GameUtils.numberConversion(math.floor(item.count))
    item.frame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v)
    item.model = v
    data.getList[#data.getList + 1] = item
  end
  if not data.enableWatch then
    data.allianceName = GameLoader:GetGameText("LC_MENU_MAP_FIGHT_ATTRIBUTION") .. data.unknowText
    data.rate = GameLoader:GetGameText("LC_MENU_MAP_FIGHT_TAX_RATE") .. data.unknowText
  end
  GameUILargeMapUI.blockDetailInfo = data
end
function GameUILargeMapUI:SetBlockInfoUI(data)
  if GameUILargeMapUI:GetFlashObject() then
    GameUILargeMapUI:GetFlashObject():InvokeASCallback("_root", "SetBlockInfoUI", data)
  end
end
function GameUILargeMapUI:ShowBlockInfoUI()
  if GameUILargeMapUI:GetFlashObject() then
    GameUILargeMapUI:GetFlashObject():InvokeASCallback("_root", "ShowBlockInfoUI")
  end
end
function GameUILargeMapUI:HideBlockInfoUI()
  if GameUILargeMapUI:GetFlashObject() then
    GameUILargeMapUI:GetFlashObject():InvokeASCallback("_root", "HideBlockInfoUI")
  end
end
function GameUILargeMapUI:ReqMassList()
  DebugOut("GameUILargeMapUI:ReqMassList")
  NetMessageMgr:SendMsg(NetAPIList.large_map_mass_troops_req.Code, nil, GameUILargeMapUI.LargeMapMassTroopsCallBack, true, nil)
end
function GameUILargeMapUI.LargeMapMassTroopsCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_mass_troops_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_mass_troops_ack.Code then
    GameUILargeMapUI:GenerateMassTroopsData(content)
    GameUILargeMapUI:SetMassListUI(GameUILargeMapUI.massListData)
    return true
  end
  return false
end
function GameUILargeMapUI:GenerateMassTroopsData(content)
  local data = {}
  data.troopList = {}
  for k, v in ipairs(content.mass_troops) do
    local item = {}
    item.leaderName = v.leader_name
    item.id = v.id
    item.massName = GameLoader:GetGameText(v.mass_name_key)
    item.targeName = GameLoader:GetGameText(v.target_name_key) .. "(" .. v.alliance_name .. ")"
    item.agreeFleetCount = v.arrived_fleet
    item.totalFleetCount = v.mass_total_fleet
    item.status = v.status
    if item.agreeFleetCount >= item.totalFleetCount then
      item.status = 0
    end
    item.leftTime = v.time
    item.baseTime = os.time()
    item.leftTimeText = GameUtils:formatTimeString(v.time)
    item.applyText = GameLoader:GetGameText("LC_MENU_MAP_FIGHT_JOIN_THE_ASSEMBLY")
    data.troopList[#data.troopList + 1] = item
  end
  GameUILargeMapUI.massListData = data
end
function GameUILargeMapUI:SetMassListUI(data)
  if GameUILargeMapUI:GetFlashObject() then
    GameUILargeMapUI:GetFlashObject():InvokeASCallback("_root", "SetMassListUI", data)
  end
end
function GameUILargeMapUI:ShowMassListUI()
  if GameUILargeMapUI:GetFlashObject() then
    GameUILargeMapUI:GetFlashObject():InvokeASCallback("_root", "ShowMassListUI")
  end
end
function GameUILargeMapUI:ReqMassMemberList()
  NetMessageMgr:SendMsg(NetAPIList.large_map_mass_troops_info_req.Code, nil, GameUILargeMapUI.LargeMapMassTroopsMemberCallBack, true, nil)
end
function GameUILargeMapUI.LargeMapMassTroopsMemberCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_mass_troops_info_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_mass_troops_info_ack.Code then
    GameUILargeMapUI:GenerateMassTroopsMemberData(content)
    GameUILargeMapUI:SetMassTroopMemberUI(GameUILargeMapUI.massTroopInfoData)
    return true
  end
  return false
end
function GameUILargeMapUI:GenerateMassTroopsMemberData(content)
  local data = {}
  data.leftTime = content.time + os.time()
  data.leftTimeText = GameUtils:formatTimeString(content.time)
  data.isLeader = content.leader_id == GameUILargeMapUI.baseData.player_id
  data.troopId = content.troop_id
  data.status = content.status
  data.maxCount = content.max_count
  data.memberList = {}
  for k, v in ipairs(content.members) do
    local item = {}
    item.avatarFrame = FleetDataAccessHelper:GetFleetAvatarFrameByAvatar(v.avatar, 0, 0)
    item.name = v.name
    item.playerID = v.player_id
    item.isLeader = content.leader_id == v.player_id
    data.memberList[#data.memberList + 1] = item
  end
  GameUILargeMapUI.massTroopInfoData = data
end
function GameUILargeMapUI:SetMassTroopMemberUI(data)
  if GameUILargeMapUI:GetFlashObject() then
    GameUILargeMapUI:GetFlashObject():InvokeASCallback("_root", "SetMassTroopMemberUI", data)
  end
end
function GameUILargeMapUI:ShowMassTroopMemberUI()
  if GameUILargeMapUI:GetFlashObject() then
    GameUILargeMapUI:GetFlashObject():InvokeASCallback("_root", "ShowMassTroopMemberUI")
  end
end
function GameUILargeMapUI:HideMassTroopMemberUI()
  if GameUILargeMapUI:GetFlashObject() then
    GameUILargeMapUI:GetFlashObject():InvokeASCallback("_root", "HideMassTroopMemberUI")
  end
end
function GameUILargeMapUI:UpdateMassTroopTime()
  if GameUILargeMapUI.massTroopInfoData then
    local leftTime = GameUILargeMapUI.massTroopInfoData.leftTime - os.time()
    if leftTime < 0 then
      leftTime = 0
    end
    GameUILargeMapUI:GetFlashObject():InvokeASCallback("_root", "UpdateMassTroopInfoTime", GameUtils:formatTimeString(leftTime))
    if leftTime == 0 then
      GameUILargeMapUI:OnFSCommand("CloseMassMember")
    end
  end
  if GameUILargeMapUI.massListData then
    for k, v in ipairs(GameUILargeMapUI.massListData.troopList) do
      local leftTime = v.leftTime - (os.time() - v.baseTime)
      if leftTime < 0 then
        leftTime = 0
      end
      v.leftTimeText = GameUtils:formatTimeString(leftTime)
      GameUILargeMapUI:OnFSCommand("update_mass_troop_item", k)
    end
  end
end
function GameUILargeMapUI:ReqJoinTroop(troopId)
  local param = {}
  param.id = troopId
  NetMessageMgr:SendMsg(NetAPIList.large_map_join_mass_req.Code, param, GameUILargeMapUI.LargeMapJoinMassTroopsCallBack, true, nil)
end
function GameUILargeMapUI.LargeMapJoinMassTroopsCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_join_mass_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_join_mass_ack.Code then
    GameUILargeMapUI.curShowType = GameUILargeMapUI.TYPE_MASS_MEMBERS
    GameUILargeMapUI:GenerateMassTroopsMemberData(content)
    GameUILargeMapUI:ShowMassTroopMemberUI()
    GameUILargeMapUI:SetMassTroopMemberUI(GameUILargeMapUI.massTroopInfoData)
    return true
  end
  return false
end
function GameUILargeMapUI:SetCreateMassPopUI(blockID, needEnergy)
  local data = {}
  data.id = blockID
  data.min = 2
  data.max = 6
  data.defaultFroce = 0
  data.defaultCount = 6
  data.creditCount = 20
  data.energyCount = needEnergy
  data.desText = GameLoader:GetGameText("")
  data.powerText = GameLoader:GetGameText("LC_MENU_MAP_FIGHT_FIGHTING_REQUIREMENTS")
  if GameUILargeMapUI:GetFlashObject() then
    GameUILargeMapUI:GetFlashObject():InvokeASCallback("_root", "SetCreateMassPopUI", data)
  end
end
function GameUILargeMapUI:ShowCreateMassPop()
  if GameUILargeMapUI:GetFlashObject() then
    GameUILargeMapUI:GetFlashObject():InvokeASCallback("_root", "ShowCreateMassPop")
  end
end
function GameUILargeMapUI:HideCreateMassPop()
  if GameUILargeMapUI:GetFlashObject() then
    GameUILargeMapUI:GetFlashObject():InvokeASCallback("_root", "HideCreateMassPop")
  end
end
function GameUILargeMapUI:CreateMassTroop(blockID, force, count)
  local param = {}
  param.id = blockID
  param.min_force = force
  param.mass_count = count
  NetMessageMgr:SendMsg(NetAPIList.large_map_create_mass_troop_req.Code, param, GameUILargeMapUI.LargeMapCreateMassTroopCallBack, true, nil)
end
function GameUILargeMapUI.LargeMapCreateMassTroopCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_create_mass_troop_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    else
      GameUILargeMapUI.curShowType = GameUILargeMapUI.TYPE_MASS_MEMBERS
      GameUILargeMapUI:HideAll()
      GameUILargeMapUI:ReqMainData()
    end
    return true
  end
  return false
end
function GameUILargeMapUI:ReqDissolveMassTroop()
  NetMessageMgr:SendMsg(NetAPIList.large_map_dissolve_mass_troop_req.Code, nil, GameUILargeMapUI.LargeMapDissolveMassTroopCallBack, true, nil)
end
function GameUILargeMapUI.LargeMapDissolveMassTroopCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_dissolve_mass_troop_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    else
      GameUILargeMapUI:OnFSCommand("close", "")
    end
    return true
  end
  return false
end
function GameUILargeMapUI:ReqMassInviteList(searchStr)
  local param = {}
  param.search_name = searchStr
  NetMessageMgr:SendMsg(NetAPIList.large_map_mass_near_invite_req.Code, param, GameUILargeMapUI.LargeMapMassInviteListCallBack, true, nil)
  GameUILargeMapUI.lastSearchName = searchStr
end
function GameUILargeMapUI.LargeMapMassInviteListCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_mass_near_invite_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_mass_near_invite_ack.Code then
    GameUILargeMapUI:GenerateMassInviteListData(content)
    GameUILargeMapUI:SetMassInviteListUI(GameUILargeMapUI.massInviteListData)
    return true
  end
  return false
end
function GameUILargeMapUI:GenerateMassInviteListData(content)
  local data = {}
  data.onekeyTime = GameUILargeMapUI.massInviteListData and GameUILargeMapUI.massInviteListData.onekeyTime or content.onekey_time + os.time()
  data.onekeyTimeStr = GameUILargeMapUI.massInviteListData and GameUILargeMapUI.massInviteListData.onekeyTimeStr or GameUtils:formatTimeString(content.onekey_time)
  data.onekeyLeftTime = GameUILargeMapUI.massInviteListData and GameUILargeMapUI.massInviteListData.onekeyLeftTime or content.onekey_time
  data.playerList = {}
  for k, v in ipairs(content.invite_list) do
    local item = {}
    local oldItem = GameUILargeMapUI:FindInvitePlayer(v.player_id)
    item.id = v.id
    item.playerID = v.player_id
    item.name = v.name
    item.level = v.level
    item.forceText = GameUtils.numberConversion(v.force)
    if not oldItem or not oldItem.statusTime then
    end
    item.statusTime = v.status_time + os.time()
    if not oldItem or not oldItem.statusTimeStr then
    end
    item.statusTimeStr = GameUtils:formatTimeString(v.status_time)
    item.leftTime = oldItem and oldItem.leftTime or v.status_time
    item.status = oldItem and oldItem.status or v.status
    item.inviteText = GameLoader:GetGameText("LC_MENU_MAP_FIGHT_INVITE")
    data.playerList[#data.playerList + 1] = item
  end
  GameUILargeMapUI.massInviteListData = data
end
function GameUILargeMapUI:SetMassInviteListUI(data)
  if GameUILargeMapUI:GetFlashObject() then
    GameUILargeMapUI:GetFlashObject():InvokeASCallback("_root", "SetMassInviteListUI", data)
  end
end
function GameUILargeMapUI:ShowMassInviteListUI()
  if GameUILargeMapUI:GetFlashObject() then
    GameUILargeMapUI:GetFlashObject():InvokeASCallback("_root", "ShowMassInviteListUI")
  end
end
function GameUILargeMapUI:UpdateInviteBtnTime()
  if GameUILargeMapUI.massInviteListData then
    local leftTime = GameUILargeMapUI.massInviteListData.onekeyTime - os.time()
    if leftTime < 0 then
      leftTime = 0
    end
    GameUILargeMapUI.massInviteListData.onekeyTimeStr = GameUtils:formatTimeString(leftTime)
    GameUILargeMapUI:GetFlashObject():InvokeASCallback("_root", "UpdateMassInviteOnekeyTime", GameUILargeMapUI.massInviteListData.onekeyTimeStr, leftTime)
    for k, v in pairs(GameUILargeMapUI.massInviteListData.playerList) do
      if v.status == 2 then
        local leftTime2 = v.statusTime - os.time()
        if leftTime2 <= 0 then
          v.status = 1
        end
        v.statusTimeStr = GameUtils:formatTimeString(leftTime2)
        GameUILargeMapUI:GetFlashObject():InvokeASCallback("_root", "UpdateMassInviteItem", k, v)
      end
    end
  end
end
function GameUILargeMapUI:FindInvitePlayer(playerId)
  if GameUILargeMapUI.massInviteListData == nil then
    return nil
  end
  for k, v in pairs(GameUILargeMapUI.massInviteListData.playerList) do
    if v.playerID == playerId then
      return v
    end
  end
  return nil
end
function GameUILargeMapUI:ReqInvitePlayer(playerId, searchName)
  local strList = {}
  if playerId == "" then
    for k, v in pairs(GameUILargeMapUI.massInviteListData.playerList) do
      if v.status == 1 then
        v.leftTime = 5
        v.statusTime = 5 + os.time()
        v.statusTimeStr = GameUtils:formatTimeString(v.leftTime)
        v.status = 2
        table.insert(strList, v.playerID)
      end
    end
    GameUILargeMapUI.massInviteListData.onekeyTime = 5 + os.time()
    GameUILargeMapUI.massInviteListData.onekeyTimeStr = GameUtils:formatTimeString(5)
    GameUILargeMapUI.massInviteListData.onekeyLeftTime = 5
  else
    strList[1] = playerId
    local item = GameUILargeMapUI:FindInvitePlayer(playerId)
    if item then
      item.leftTime = 5
      item.statusTime = 5 + os.time()
      item.statusTimeStr = GameUtils:formatTimeString(item.leftTime)
      item.status = 2
    end
  end
  local param = {}
  param.search_name = ""
  param.player_id = strList
  if #strList == 0 then
    return
  end
  NetMessageMgr:SendMsg(NetAPIList.large_map_mass_invite_req.Code, param, GameUILargeMapUI.LargeMapMassInvitePlayerCallBack, true, nil)
end
function GameUILargeMapUI.LargeMapMassInvitePlayerCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_mass_invite_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_mass_invite_ack.Code then
    local lastCount = #GameUILargeMapUI.massInviteListData.playerList
    GameUILargeMapUI:GenerateMassInviteListData(content)
    local newCount = #GameUILargeMapUI.massInviteListData.playerList
    if lastCount ~= newCount then
      GameUILargeMapUI:SetMassInviteListUI(GameUILargeMapUI.massInviteListData)
    else
      GameUILargeMapUI:UpdateMassInviteListUI(GameUILargeMapUI.massInviteListData)
    end
    return true
  end
  return false
end
function GameUILargeMapUI:UpdateMassInviteListUI(data)
  if GameUILargeMapUI:GetFlashObject() then
    GameUILargeMapUI:GetFlashObject():InvokeASCallback("_root", "UpdateMassInviteListUI", data)
  end
end
function GameUILargeMapUI:ReqExitMassTroop(troopid)
  local param = {}
  param.troop_id = troopid
  NetMessageMgr:SendMsg(NetAPIList.large_map_exit_mass_troop_req.Code, param, GameUILargeMapUI.LargeMapExitMassTroopCallBack, true, nil)
end
function GameUILargeMapUI.LargeMapExitMassTroopCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_exit_mass_troop_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    else
      GameUILargeMapUI:OnFSCommand("close", "")
    end
    return true
  end
  return false
end
function GameUILargeMapUI:StartMassWar()
  NetMessageMgr:SendMsg(NetAPIList.large_map_start_mass_war_req.Code, nil, GameUILargeMapUI.LargeMapStartMassWarCallBack, true, nil)
end
function GameUILargeMapUI.LargeMapStartMassWarCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_start_mass_war_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    else
      GameUILargeMapUI:OnFSCommand("close", "")
    end
    return true
  end
  return false
end
function GameUILargeMapUI:ReqSimplePlayerInfo(playerID)
  local param = {}
  param.player_id = playerID
  NetMessageMgr:SendMsg(NetAPIList.large_map_view_mass_player_req.Code, param, GameUILargeMapUI.LargeMapSimpleMassPlayerInfoCallBack, true, nil)
end
function GameUILargeMapUI.LargeMapSimpleMassPlayerInfoCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_view_mass_player_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_view_mass_player_ack.Code then
    GameUILargeMapUI:GenerateSimpleMassPlayerData(content)
    return true
  end
  return false
end
function GameUILargeMapUI:GenerateSimpleMassPlayerData(content)
  local data = {}
  data.playerId = content.player_id
  data.isSelf = content.player_id == GameUILargeMapUI.baseData.player_id
  data.avatarFrame = FleetDataAccessHelper:GetFleetAvatarFrameByAvatar(content.avatar, 0, 0)
  data.name = content.name
  data.level = "LV." .. content.level
  data.forceText = GameUtils.numberConversion(content.force)
  data.rankText = GameLoader:GetGameText("LC_MENU_MAP_ARMY_RANK_" .. content.rank)
  data.exploit = content.exploit
  if GameUILargeMapUI:GetFlashObject() then
    GameUILargeMapUI:GetFlashObject():InvokeASCallback("_root", "SetPlayerViewUI", data)
    GameUILargeMapUI:GetFlashObject():InvokeASCallback("_root", "ShowPlayerViewUI")
  end
end
function GameUILargeMapUI:ReqRemoveMassPlayer(playerID)
  local param = {}
  param.player_id = playerID
  NetMessageMgr:SendMsg(NetAPIList.large_map_remove_mass_player_req.Code, param, GameUILargeMapUI.LargeMapRemoveMassPlayerCallBack, true, nil)
end
function GameUILargeMapUI.LargeMapRemoveMassPlayerCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_remove_mass_player_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUILargeMapUI:ViewPlayerDetailInfo(playerid)
  GameUIPlayerView:UpdateWithPlayerID("LM" .. playerid)
end
function GameUILargeMapUI.RemoveMassPlayerNtf(content)
  if GameUILargeMapUI:GetFlashObject() and GameUILargeMapUI.massTroopInfoData then
    for k, v in pairs(GameUILargeMapUI.massTroopInfoData.memberList) do
      if v.playerID == content.player_id then
        table.remove(GameUILargeMapUI.massTroopInfoData.memberList, k)
        break
      end
    end
    if content.player_id ~= GameUILargeMapUI.baseData.player_id then
      GameUILargeMapUI:SetMassTroopMemberUI(GameUILargeMapUI.massTroopInfoData)
    else
      GameUILargeMapUI:OnFSCommand("close")
    end
  end
end
function GameUILargeMapUI.AddMassPlayerNtf(content)
  if GameUILargeMapUI:GetFlashObject() and GameUILargeMapUI.massTroopInfoData then
    if GameUILargeMapUI.massTroopInfoData.troopId == content.troop_id then
      local item = {}
      item.avatarFrame = FleetDataAccessHelper:GetFleetAvatarFrameByAvatar(content.new_member.avatar, 0, 0)
      item.name = content.new_member.name
      item.playerID = content.new_member.player_id
      GameUILargeMapUI.massTroopInfoData.memberList[#GameUILargeMapUI.massTroopInfoData.memberList + 1] = item
    end
    GameUILargeMapUI:SetMassTroopMemberUI(GameUILargeMapUI.massTroopInfoData)
  end
end
function GameUILargeMapUI:ReqRepairData()
  NetMessageMgr:SendMsg(NetAPIList.large_map_repair_req.Code, nil, GameUILargeMapUI.LargeMapRepairDataCallBack, true, nil)
end
function GameUILargeMapUI.LargeMapRepairDataCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_repair_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_repair_ack.Code then
    GameUILargeMapUI:GenerateRepairData(content)
    GameUILargeMapUI:SetRepaireUI(GameUILargeMapUI.repairData)
    return true
  end
  return false
end
function GameUILargeMapUI:GenerateRepairData(content)
  local data = {}
  data.avaterList = {}
  data.selectedIndex = 1
  for k, v in ipairs(content.avatar_list) do
    local item = GameUILargeMapUI:GenerateRepairItemData(v)
    item.selectStatus = k == data.selectedIndex and "selected" or "idle"
    data.avaterList[#data.avaterList + 1] = item
  end
  data.selectedIndex = 1
  GameUILargeMapUI.repairData = data
end
function GameUILargeMapUI:GenerateRepairItemData(v)
  local item = {}
  item.identity = v.identity
  item.avatarName = FleetDataAccessHelper:GetFleetDisplayNameByName(v.name, v.identity)
  if v.level > 0 then
    item.avatarName = FleetDataAccessHelper:GetFleetDisplayNameByName(v.name, v.identity) .. "[+" .. tostring(v.level) .. "]"
  end
  item.avatarFrame = FleetDataAccessHelper:GetFleetAvatarFrameByAvatar(v.avatar, 0, 0)
  item.avatarLevel = ""
  item.avatarColor = FleetDataAccessHelper:GetFleetColorFrameByColor(v.color)
  item.avatarSex = FleetDataAccessHelper:GetFleetSexFrame(v.sex)
  item.vesselsType = FleetDataAccessHelper:GetFleetVesselsFrameByVessel(v.vessels)
  item.avatarForceText = GameUtils.numberConversion(v.force)
  item.avatarRankType = GameUtils:GetFleetRankFrame(v.rank, nil)
  item.avatarShipFrame = GameDataAccessHelper:GetCommanderShipByDownloadState(v.ship)
  item.avatarCurHP = v.cur_hp
  item.avatarTotalHP = v.total_hp
  item.iconInfo = GameHelper:GetItemInfo(v.need_res)
  item.count = GameHelper:GetAwardCount(v.need_res.item_type, v.need_res.number, v.need_res.no)
  item.avatarResNum = GameUtils.numberConversion(math.floor(item.count))
  item.avatarHPPercent = math.floor(item.avatarCurHP / item.avatarTotalHP * 1000) / 10
  return item
end
function GameUILargeMapUI:SetRepaireUI(data)
  if GameUILargeMapUI:GetFlashObject() then
    GameUILargeMapUI:GetFlashObject():InvokeASCallback("_root", "SetRepairUI", data)
  end
end
function GameUILargeMapUI:ShowRepairUI()
  if GameUILargeMapUI:GetFlashObject() then
    GameUILargeMapUI:GetFlashObject():InvokeASCallback("_root", "ShowRepairUI", data)
  end
end
function GameUILargeMapUI:ReqRepairShip(identity)
  local param = {}
  param.identity = identity
  NetMessageMgr:SendMsg(NetAPIList.large_map_repair_ship_req.Code, param, GameUILargeMapUI.LargeMapRepairShipCallBack, true, nil)
end
function GameUILargeMapUI.LargeMapRepairShipCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_repair_ship_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_repair_ship_ack.Code then
    GameUILargeMapUI:GenerateRepairShipData(content)
    return true
  end
  return false
end
function GameUILargeMapUI:GenerateRepairShipData(content)
  local item = GameUILargeMapUI:GenerateRepairItemData(content)
  for k, v in pairs(GameUILargeMapUI.repairData.avaterList) do
    if v.identity == item.identity then
      item.selectStatus = v.selectStatus
      GameUILargeMapUI.repairData.avaterList[k] = item
      break
    end
  end
  GameUILargeMapUI:SetSelectedRepairShipData(GameUILargeMapUI.repairData.selectedIndex, GameUILargeMapUI.repairData.avaterList[GameUILargeMapUI.repairData.selectedIndex])
end
function GameUILargeMapUI:SetSelectedRepairShipData(index, data)
  if GameUILargeMapUI:GetFlashObject() then
    GameUILargeMapUI:GetFlashObject():InvokeASCallback("_root", "SetReapirDetailAvatar", index, data)
  end
end
function GameUILargeMapUI:Update(dt)
  local flashObj = GameUILargeMapUI:GetFlashObject()
  if flashObj then
    flashObj:Update(dt)
    flashObj:InvokeASCallback("_root", "OnUpdate", dt)
    GameUILargeMapUI:UpdateMassTroopTime()
    GameUILargeMapUI:UpdateInviteBtnTime()
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUILargeMapUI.OnAndroidBack()
    GameUILargeMapUI:OnFSCommand("close")
  end
end
