local GameStateInterstellar = GameStateManager.GameStateInterstellar
local InterstellarAdventure = LuaObjectManager:GetLuaObject("InterstellarAdventure")
function GameStateInterstellar:InitGameState()
end
function GameStateInterstellar:OnFocusGain(previousState)
  self.lastState = previousState
  self:AddObject(InterstellarAdventure)
  DebugOut("self:AddObject(InterstellarAdventure)")
end
function GameStateInterstellar:OnFocusLost(newState)
  self.nextState = newState
  self:EraseObject(InterstellarAdventure)
end
