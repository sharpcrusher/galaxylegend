WVEDamageInfoPanel = luaClass(nil)
local GameStateWVE = GameStateManager.GameStateWVE
local WVEGameManeger = LuaObjectManager:GetLuaObject("WVEGameManeger")
function WVEDamageInfoPanel:ctor(wveGameManager)
  self.mGameManger = wveGameManager
  self.isShowDamagePanel = false
end
function WVEDamageInfoPanel:OnFSCommand(cmd, arg)
  if cmd == "show_damage_panel" then
    self.isShowDamagePanel = arg == "true"
    if self.isShowDamagePanel then
      self:UpdateDamageKillInfoPanel()
    end
  else
    return false
  end
  return true
end
function WVEDamageInfoPanel.PrimeRankNtf()
  if not WVEGameManeger.mWVEDamageInfoPanel.isShowDamagePanel then
    DebugOut("PrimeRankNtf____")
    return
  end
  DebugOut("PrimeRankNtf____UpdateDamageKill")
  WVEGameManeger.mWVEDamageInfoPanel:UpdateDamageKillInfoPanel()
end
function WVEDamageInfoPanel:UpdateDamageKillInfoPanel()
  local flashObj = self.mGameManger:GetFlashObject()
  if not flashObj then
    return
  end
  local content = GameGlobalData:GetData("prime_rank_info")
  if not content then
    DebugOut("WVEDamageInfoPanel:PrimeRankNtf:content data is wrong")
    for i = 1, 5 do
      flashObj:InvokeASCallback("_root", "SetDamageItemInfo", i, "none", "none")
      flashObj:InvokeASCallback("_root", "SetKillItemInfo", i, "none", "none")
    end
    flashObj:InvokeASCallback("_root", "SetMySelfDamageItemInfo", "-", "none", "none")
    flashObj:InvokeASCallback("_root", "SetMySelfKillItemInfo", "-", "none", "none")
    return
  end
  DebugOut("PrimeRankNtf:")
  DebugTable(content)
  local rankNum, userName
  for k, v in pairs(content) do
    if v.type and tonumber(v.type) == 2 then
      self:UpdateDamageInfoPanel(flashObj, v.players)
      rankNum = GameUtils.numberConversionEnglish(tonumber(v.self_rank.point))
      userName = GameUtils:GetUserDisplayName(v.self_rank.user_name)
      flashObj:InvokeASCallback("_root", "SetMySelfDamageItemInfo", v.self_rank.rank, userName, rankNum)
    elseif v.type and tonumber(v.type) == 1 then
      self:UpdateKillInfoPanel(flashObj, v.players)
      rankNum = GameUtils.numberConversionEnglish(tonumber(v.self_rank.point))
      userName = GameUtils:GetUserDisplayName(v.self_rank.user_name)
      flashObj:InvokeASCallback("_root", "SetMySelfKillItemInfo", v.self_rank.rank, userName, rankNum)
    end
  end
end
function WVEDamageInfoPanel:UpdateDamageInfoPanel(flashObj, content)
  local damageItemNum = 0
  local rankNum
  for k, v in pairs(content) do
    rankNum = GameUtils.numberConversionEnglish(tonumber(v.point))
    flashObj:InvokeASCallback("_root", "SetDamageItemInfo", v.rank, v.user_name, rankNum)
    damageItemNum = damageItemNum + 1
  end
  if damageItemNum < 5 then
    for i = damageItemNum + 1, 5 do
      flashObj:InvokeASCallback("_root", "SetDamageItemInfo", i, "none", "none")
    end
  end
end
function WVEDamageInfoPanel:UpdateKillInfoPanel(flashObj, content)
  local rankNum
  local killItemNum = 0
  for k, v in pairs(content) do
    rankNum = GameUtils.numberConversionEnglish(tonumber(v.point))
    flashObj:InvokeASCallback("_root", "SetKillItemInfo", v.rank, v.user_name, rankNum)
    killItemNum = killItemNum + 1
    if killItemNum == 5 then
      break
    end
  end
  if killItemNum < 5 then
    for i = killItemNum + 1, 5 do
      flashObj:InvokeASCallback("_root", "SetKillItemInfo", i, "none", "none")
    end
  end
end
