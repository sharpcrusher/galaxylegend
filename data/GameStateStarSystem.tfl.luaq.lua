local GameUIStarSystemPort = LuaObjectManager:GetLuaObject("GameUIStarSystemPort")
local GameStateStarSystem = GameStateManager.GameStateStarSystem
local GameUIStarSystemFuben = LuaObjectManager:GetLuaObject("GameUIStarSystemFuben")
function GameStateStarSystem:InitGameState()
end
function GameStateStarSystem:OnFocusGain(previousState)
  self.m_preState = previousState
  local showfuben = false
  if (previousState == GameStateManager.GameStateFormation or previousState == GameStateManager.GameStateBattlePlay) and GameStateStarSystem.leaveStateReson == "fuben" then
    GameUIStarSystemFuben:Show()
    showfuben = true
  end
  GameStateStarSystem.leaveStateReson = nil
  if not showfuben then
    self:AddObject(GameUIStarSystemPort)
  end
  if GameStateStarSystem.cb_onFocusGain then
    GameStateStarSystem.cb_onFocusGain()
    GameStateStarSystem.cb_onFocusGain = nil
  end
end
function GameStateStarSystem:OnFocusLost(newState)
  self.m_preState = nil
  self:EraseObject(GameUIStarSystemPort)
  self:EraseObject(GameUIStarSystemFuben)
end
function GameStateStarSystem:GetPreState()
  return self.m_preState
end
