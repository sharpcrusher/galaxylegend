local GameFleetEnhance = LuaObjectManager:GetLuaObject("GameFleetEnhance")
local GameFleetInfoUI = LuaObjectManager:GetLuaObject("GameFleetInfoUI")
local GameFleetInfoBag = LuaObjectManager:GetLuaObject("GameFleetInfoBag")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameFleetInfoBackground = LuaObjectManager:GetLuaObject("GameFleetInfoBackground")
local GameStateEquipEnhance = GameStateManager.GameStateEquipEnhance
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameItemBag = LuaObjectManager:GetLuaObject("GameItemBag")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local QuestTutorialEnhance = TutorialQuestManager.QuestTutorialEnhance
local QuestTutorialBattleMap = TutorialQuestManager.QuestTutorialBattleMap
local QuestTutorialEnhance_second = TutorialQuestManager.QuestTutorialEnhance_second
local QuestTutorialEnhance_third = TutorialQuestManager.QuestTutorialEnhance_third
local QuestTutorialBuildAcademy = TutorialQuestManager.QuestTutorialBuildAcademy
local GameUIBarRight = LuaObjectManager:GetLuaObject("GameUIBarRight")
local GameUICollect = LuaObjectManager:GetLuaObject("GameUICollect")
local QuestTutorialBuildAcademy = TutorialQuestManager.QuestTutorialBuildAcademy
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameVip = LuaObjectManager:GetLuaObject("GameVip")
local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
GameFleetEnhance.TAB_ENHANCE = 1
GameFleetEnhance.TAB_EVOLUTION = 2
function GameFleetEnhance:OnInitGame()
  GameGlobalData:RegisterDataChangeCallback("equipments", GameFleetEnhance.OnGlobalEquipmentsChange)
  GameGlobalData:RegisterDataChangeCallback("levelinfo", GameFleetEnhance.OnGlobalLevelChange)
  GameGlobalData:RegisterDataChangeCallback("cdtimes", GameFleetEnhance.OnEnhanceCDTimeChange)
  GameGlobalData:RegisterDataChangeCallback("fleetinfo", GameFleetEnhance.OnEnhanceFleetinfoChange)
  GameGlobalData:RegisterDataChangeCallback("resource", GameFleetEnhance.OnGlobalResourceChange)
  self.currentSelectEquipSlot = -1
  self.currentSlectedIndex = -1
  self.initEnhanceTab = -1
  GameTimer:Add(GameFleetEnhance._EnhanceCDTimeTimer, 1000)
end
function GameFleetEnhance.OnGlobalResourceChange()
  if not GameFleetEnhance:GetFlashObject() then
    return
  end
  local resource = GameGlobalData:GetData("resource")
  GameFleetEnhance:GetFlashObject():InvokeASCallback("_root", "setMoneyCredit", GameUtils.numberConversion(resource.money), GameUtils.numberConversion(resource.credit))
end
function GameFleetEnhance.OnEnhanceCDTimeChange()
  local cd_time = GameGlobalData:GetEnhanceCDTime()
  if not cd_time then
    GameFleetEnhance.enhanceCDTime = nil
    return
  end
  if cd_time.left_time > 0 then
    GameFleetEnhance.enhanceCDTime = os.time() + cd_time.left_time
  else
    GameFleetEnhance.enhanceCDTime = os.time()
  end
  GameFleetEnhance:CheckEnhanceCDTime()
end
function GameFleetEnhance.OnEnhanceFleetinfoChange()
  DebugOut("GameFleetEnhance.OnEnhanceFleetinfoChange")
  if GameStateManager:GetCurrentGameState() == GameStateEquipEnhance and GameStateEquipEnhance:IsObjectInState(GameFleetEnhance) then
    local fleetinfo = GameGlobalData.GlobalData.fleetinfo.fleets
    for k, v in pairs(fleetinfo) do
      if v.id == GameFleetEnhance.m_currentFleetsId then
        local enhanceCount = v.force - GameFleetEnhance.m_curreetFleetsForce
        GameFleetEnhance.m_curreetFleetsForce = v.force
        GameFleetEnhance:GetFlashObject():InvokeASCallback("_root", "showEnhanceAnimation", enhanceCount, GameUtils.numberConversion(v.force))
      end
    end
  end
end
function GameFleetEnhance:CheckEnhanceCDTime()
  if self.enhanceCDTime and os.time() > self.enhanceCDTime then
    self.enhanceCDTime = nil
  end
  self:RefreshCDTimer()
end
GameFleetEnhance.enhanceOneCD = 120
GameFleetEnhance.maxEnhanceLevel = 6
GameFleetEnhance.enhanceVIP = 4
function GameFleetEnhance:RefreshCDTimer()
  if not self:GetFlashObject() then
    return
  end
  local left_time = 0
  local level = self.maxEnhanceLevel
  if self.enhanceCDTime then
    local cd_time = GameGlobalData:GetEnhanceCDTime()
    left_time = self.enhanceCDTime - os.time()
    local show_time = left_time % GameFleetEnhance.enhanceOneCD
    level = self.maxEnhanceLevel - math.ceil(left_time / self.enhanceOneCD)
    if level < 0 then
      show_time = show_time + -level * self.enhanceOneCD
    end
    GameFleetEnhance:GetFlashObject():InvokeASCallback("_root", "RefreshCDTimer", self:HaveEnhanceCD(), cd_time.status == 2, GameUtils:formatTimeString(show_time), level)
  else
    GameFleetEnhance:GetFlashObject():InvokeASCallback("_root", "RefreshCDTimer", self:HaveEnhanceCD(), true, GameUtils:formatTimeString(left_time), level)
  end
end
function GameFleetEnhance:CanEnhance()
  if self.enhanceCDTime == nil then
    return true
  end
  local left_time = self.enhanceCDTime - os.time()
  local level = self.maxEnhanceLevel - math.ceil(left_time / self.enhanceOneCD)
  return level > 0
end
function GameFleetEnhance._EnhanceCDTimeTimer()
  if GameStateManager:GetCurrentGameState() == GameStateEquipEnhance then
    GameFleetEnhance:CheckEnhanceCDTime()
  end
  return 1000
end
function GameFleetEnhance.OnGlobalEquipmentsChange()
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameFleetEnhance) then
    GameFleetEnhance:FleetSelect(GameFleetEnhance.currentSlectedIndex, true)
  end
end
function GameFleetEnhance.OnGlobalLevelChange()
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameFleetEnhance) then
    GameFleetEnhance:RefreshLevelInfo()
  end
end
function GameFleetEnhance:RefreshLevelInfo()
  local levelinfo = GameGlobalData:GetData("levelinfo")
  local levelText = GameLoader:GetGameText("LC_MENU_AVATAR_LEVEL") .. " " .. tostring(levelinfo.level)
  self:GetFlashObject():InvokeASCallback("_root", "RefreshLevelInfo", levelText)
end
function GameFleetEnhance:SetInitEnhanceTab(tabIndex)
  self.initEnhanceTab = tabIndex
end
function GameFleetEnhance:Init()
  DebugOut("GameFleetEnhance:Init")
  self.currentSelectEquipSlot = math.max(self.currentSelectEquipSlot, 1)
  self.enhanceTab = math.max(self.initEnhanceTab, self.TAB_ENHANCE)
  self:GetFlashObject():InvokeASCallback("_root", "Init")
  if immanentversion == 1 and not self:UnlockQuickEnhance() then
    self:GetFlashObject():InvokeASCallback("_root", "HideQuickEnhanceFunction")
  end
  self:UpdateSelectedFleetInfo(self.currentSlectedIndex)
  self:AvatarAppear()
  self:RefreshLevelInfo()
  GameFleetEnhance.OnGlobalResourceChange()
  if not GameVip:IsFastEnhanceUnlocked() then
    self:GetFlashObject():InvokeASCallback("_root", "DisableQuickEnhance")
  end
end
function GameFleetEnhance:OnAddToGameState()
  self:Init()
end
function GameFleetEnhance:CheckBuildAcademyTutorial()
  if immanentversion == 2 then
    if not QuestTutorialBattleMap:IsFinished() then
      QuestTutorialBattleMap:SetActive(true)
    end
  elseif immanentversion == 1 and not QuestTutorialBuildAcademy:IsFinished() then
    QuestTutorialBuildAcademy:SetActive(true)
  end
end
function GameFleetEnhance:CheckTutorialEnhance(equip)
  if not self:GetFlashObject() then
    return
  end
  if QuestTutorialEnhance:IsActive() or GameFleetInfoBackground.finishEnhance_third or QuestTutorialEnhance_second:IsActive() then
    if QuestTutorialEnhance_second:IsActive() then
      if self.currentSlectedIndex ~= 2 then
        GameFleetInfoBackground:GetFlashObject():InvokeASCallback("_root", "ShowTutorial", true)
        self:GetFlashObject():InvokeASCallback("_root", "HideTutorialEnhance")
        self:GetFlashObject():InvokeASCallback("_root", "HideTutorialQuickEnhance")
        self:GetFlashObject():InvokeASCallback("_root", "EnableEnhance")
        if GameVip:IsFastEnhanceUnlocked() then
          self:GetFlashObject():InvokeASCallback("_root", "EnableQuickEnhance")
        end
      elseif self.currentSlectedIndex == 2 then
        GameFleetInfoBackground:GetFlashObject():InvokeASCallback("_root", "ShowTutorial", false)
        self:GetFlashObject():InvokeASCallback("_root", "DisableEnhance")
        self:GetFlashObject():InvokeASCallback("_root", "HideTutorialEnhance")
        self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialQuickEnhance")
        self.enhanceTab = 1
      end
    elseif equip then
      self:GetFlashObject():InvokeASCallback("_root", "DisableQuickEnhance")
      self:GetFlashObject():InvokeASCallback("_root", "HideTutorialQuickEnhance")
      self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialEnhance")
    else
      self:GetFlashObject():InvokeASCallback("_root", "HideTutorialEnhance")
      self:GetFlashObject():InvokeASCallback("_root", "HideTutorialQuickEnhance")
      if QuestTutorialEnhance:IsActive() then
        QuestTutorialEnhance:SetFinish(true)
        if (immanentversion170 == 4 or immanentversion170 == 5) and not TutorialQuestManager.QuestTutorialPveMapPoint:IsFinished() then
          TutorialQuestManager.QuestTutorialPveMapPoint:SetActive(true, false)
        end
        AddFlurryEvent("TutoriaEngineUse_EnhanceEqip", {}, 2)
        GameUICommonDialog:PlayStory({100044}, nil)
        self:CheckBuildAcademyTutorial()
      end
    end
    if immanentversion == 2 and GameFleetInfoBackground.finishEnhance_third then
      GameFleetInfoBackground.finishEnhance_third = nil
      GameUICommonDialog:PlayStory({512053}, nil)
    end
  else
    self:GetFlashObject():InvokeASCallback("_root", "HideTutorialEnhance")
    self:GetFlashObject():InvokeASCallback("_root", "HideTutorialQuickEnhance")
    self:GetFlashObject():InvokeASCallback("_root", "EnableEnhance")
    if GameVip:IsFastEnhanceUnlocked() then
      self:GetFlashObject():InvokeASCallback("_root", "EnableQuickEnhance")
    end
  end
end
function GameFleetEnhance:OnEraseFromGameState()
  self:Clear()
  if QuestTutorialEnhance:IsActive() and self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "HideTutorialEnhance")
  end
  if not QuestTutorialEnhance:IsActive() and QuestTutorialEnhance:IsFinished() then
    self:CheckBuildAcademyTutorial()
  end
end
function GameFleetEnhance:Clear()
  self.curFleetEquipments = {}
  self.currentSlectedIndex = -1
  self.currentSelectEquipSlot = -1
  self.initEnhanceTab = -1
  self.equipmentShown = false
end
function GameFleetEnhance:SetInitEquip(fleetIndex, equipSlot)
  self.currentSlectedIndex = fleetIndex
  self.currentSelectEquipSlot = equipSlot
end
function GameFleetEnhance:ShowEquipMent()
  if not self.equipmentShown then
    self.equipmentShown = true
    GameFleetEnhance:GetFlashObject():InvokeASCallback("_root", "ShowFleetEquipment")
  end
end
function GameFleetEnhance.BuyEquip()
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetId = fleets[GameFleetEnhance.currentSlectedIndex].id
  local shop_buy_and_wear_req_param = {
    formation_id = GameGlobalData:GetData("matrix").id,
    fleet_id = fleetId,
    equip_type = GameFleetEnhance.curBuyEquipType
  }
  NetMessageMgr:SendMsg(NetAPIList.shop_buy_and_wear_req.Code, shop_buy_and_wear_req_param, GameFleetEnhance.BuyAndEquipCallback, true, nil)
end
function GameFleetEnhance:HideEquipMent()
  if self.equipmentShown then
    self.equipmentShown = false
    GameFleetEnhance:GetFlashObject():InvokeASCallback("_root", "HideFleetEquipment")
  end
end
function GameFleetEnhance:UpdateSelectedFleetInfo(fleetIndex)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  if fleets ~= nil then
    local maxFleetIndex = LuaUtils:table_size(fleets)
    fleetIndex = math.max(1, fleetIndex)
    if fleetIndex > 0 and maxFleetIndex >= fleetIndex then
      if self:GetFlashObject() ~= nil then
        self:GetFlashObject():InvokeASCallback("_root", "UpdateSelectedFleetId", fleetIndex, maxFleetIndex)
      end
      local content = fleets[fleetIndex]
      self.m_currentFleetsId = content.id
      self.m_curreetFleetsForce = content.force
      if self.m_curreetFleetsForce then
        self:GetFlashObject():InvokeASCallback("_root", "setForceText", GameUtils.numberConversion(self.m_curreetFleetsForce))
      end
      GameFleetEnhance:UpdateFleetInfoWithContent(content, fleetIndex)
      self:FleetSelect(fleetIndex, true)
      self.currentSlectedIndex = fleetIndex
    end
  end
  self:AvatarAppear()
end
function GameFleetEnhance:UpdateFleetInfoWithContent(content, newfleetIndex)
  GameFleetInfoBackground:FleetAppear()
end
function GameFleetEnhance:AvatarAppear()
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local matrix = GameGlobalData:GetData("matrix").cells
  local identity = fleets[self.currentSlectedIndex].identity
  local avaters, nameText, statusText = "", "", GameLoader:GetGameText("LC_MENU_COMBAT_CAPABILITY")
  for k, v in ipairs(fleets) do
    avaters = avaters .. GameDataAccessHelper:GetFleetAvatar(v.identity) .. "\001"
  end
  for k, v in pairs(matrix) do
    if v.fleet_identity == identity then
      statusText = GameLoader:GetGameText("LC_MENU_COMBAT_CAPABILITY")
    end
  end
  if identity == 1 then
    local userinfo = GameGlobalData:GetUserInfo()
    nameText = GameUtils:GetUserDisplayName(userinfo.name)
  else
    nameText = GameLoader:GetGameText("LC_NPC_" .. GameDataAccessHelper:GetCommanderName(identity))
  end
  DebugOut("AvaterAppear: ", avaters, self.currentSlectedIndex, nameText, statusText)
  self:GetFlashObject():InvokeASCallback("_root", "SetAvatarContent", avaters, self.currentSlectedIndex, nameText, statusText)
end
function GameFleetEnhance:AvatarAppearByServerData()
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local matrix = GameGlobalData:GetData("matrix").cells
  local identity = fleets[self.currentSlectedIndex].identity
  local fleetDisplayInfo = GameGlobalData:GetFleetDisplayerInfo(identity)
  local avaters, nameText, statusText = "", "", GameLoader:GetGameText("LC_MENU_COMBAT_CAPABILITY")
  for k, v in ipairs(fleets) do
    local vDisplayInfo = GameGlobalData:GetFleetDisplayerInfo(v.identity)
    avaters = avaters .. FleetDataAccessHelper:GetFleetAvatarFrameByAvatar(vDisplayInfo.avatar, vDisplayInfo.fleet_id, vDisplayInfo.level) .. "\001"
  end
  for k, v in pairs(matrix) do
    if v.fleet_identity == identity then
      statusText = GameLoader:GetGameText("LC_MENU_COMBAT_CAPABILITY")
    end
  end
  nameText = FleetDataAccessHelper:GetFleetLevelDisplayName(fleetDisplayInfo.name, fleetDisplayInfo.fleet_id, fleetDisplayInfo.color, fleetDisplayInfo.level)
  DebugOut("AvatarAppearByServerData: ", avaters, self.currentSlectedIndex, nameText, statusText)
  self:GetFlashObject():InvokeASCallback("_root", "SetAvatarContent", avaters, self.currentSlectedIndex, nameText, statusText)
end
function GameFleetEnhance:Update(dt)
  self:GetFlashObject():InvokeASCallback("_root", "OnUpdate", dt)
  self:GetFlashObject():Update(dt)
end
function GameFleetEnhance:OnFSCommand(cmd, arg)
  if cmd == "closePress" then
    if GameFleetEnhance.tutorialClose then
      AddFlurryEvent("CloseFleetEnhanceUI", {}, 1)
      AddFlurryEvent("TutoriaEngineUse_Close", {}, 2)
      GameFleetEnhance.tutorialClose = false
    end
    if QuestTutorialEnhance_second:IsActive() and not QuestTutorialEnhance_second:IsFinished() then
      GameUICommonDialog:ForcePlayStory({11000315})
      return
    end
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateEquipEnhance.previousState)
  end
  if cmd == "enhanceTabSelect" then
    self.enhanceTab = tonumber(arg)
    local equip = GameFleetEnhance:IsEquipmentInSlot(GameFleetEnhance.currentSelectEquipSlot)
    GameFleetEnhance:GetFlashObject():InvokeASCallback("_root", "showEquipEnhance", equip ~= nil, self.enhanceTab)
  end
  if cmd == "showSrcEquip" then
    self:showEnhanceEquipItemBox(arg, GameFleetEnhance.enhance_equip, GameFleetEnhance.enhance_equip_type)
  end
  if cmd == "showenhanceEquip" then
    self:showEnhanceEquipItemBox(arg, GameFleetEnhance.enhance_equip, GameFleetEnhance.enhance_equip_type)
  end
  if cmd == "showitemItembox" then
    self:showEnhanceItemItemBox(arg, nil, GameFleetEnhance.recipe_equip_type)
  end
  if cmd == "showdestEquip" then
    self:showEnhanceEquipItemBox(arg, GameFleetEnhance.evolution_equip, GameFleetEnhance.evolution_equip_type)
  end
  if cmd == "equipmentPress" then
    local slot, x, y = unpack(LuaUtils:string_split(arg, "\001"))
    GameFleetInfoBackground:SetMousePos(tonumber(x), tonumber(y))
    GameFleetInfoBackground:onPressedFleetRect()
  elseif cmd == "equipmentReleaseOutside" then
    local slot, x, y = unpack(LuaUtils:string_split(arg, "\001"))
    GameFleetInfoBackground:onReleasedFleetRect()
    GameFleetInfoBackground:SetMousePos(0, tonumber(y))
  elseif cmd == "equipmentRelease" then
    local slot, x, y = unpack(LuaUtils:string_split(arg, "\001"))
    self:EquipmentSelect(tonumber(slot))
    GameFleetInfoBackground:onReleasedFleetRect()
    GameFleetInfoBackground:SetMousePos(0, tonumber(y))
  end
  if cmd == "SyncMousePos" then
    local x, y = unpack(LuaUtils:string_split(arg, "\001"))
    GameFleetInfoBackground:SetMousePos(tonumber(x), tonumber(y))
  end
  if cmd == "showbuyEquip" then
    GameFleetEnhance:showBuyEquipItenBox(arg)
  end
  if cmd == "buyEquip" then
    self.BuyEquip()
  elseif cmd == "enhance" then
    AddFlurryEvent("TutoriaEngineUse_EnhanceEqip", {}, 2)
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local fleetId = fleets[self.currentSlectedIndex].id
    local equip = self:IsEquipmentInSlot(self.currentSelectEquipSlot)
    if equip then
      if self:CheckCDCanEnhance() then
        local equipment_enhance_req_param = {
          equip_id = equip.item_id,
          fleet_id = fleetId,
          once_more_enhance = 0
        }
        NetMessageMgr:SendMsg(NetAPIList.equipment_enhance_req.Code, equipment_enhance_req_param, self.equipmentEnhanceCallback, true, nil)
      else
        if not GameSettingData.EnablePayRemind then
          GameSettingData.EnablePayRemind = true
          GameUtils:SaveSettingData()
        end
        if GameSettingData.EnablePayRemind then
          local callbackFunc = GameFleetEnhance.clearCD
          GameUIGlobalScreen:ShowClearCD("clear_equipment_enhance_cd", callbackFunc)
        else
          GameFleetEnhance.clearCD()
        end
      end
      self:GetFlashObject():InvokeASCallback("_root", "HideTutorialEnhance")
      if QuestTutorialEnhance:IsActive() then
        local equip = self:IsEquipmentInSlot(1)
        local equipments = GameGlobalData:GetData("equipments")
        AddFlurryEvent("ClickEnhance", {}, 1)
        if equip then
          do
            local currentEquipId = equip.item_id
            local currentEquipInfo = LuaUtils:table_values(LuaUtils:table_filter(equipments, function(k, v)
              return v.equip_id == currentEquipId
            end))[1]
            if 1 <= currentEquipInfo.equip_level then
              QuestTutorialEnhance:SetFinish(true)
              if (immanentversion170 == 4 or immanentversion170 == 5) and not TutorialQuestManager.QuestTutorialPveMapPoint:IsFinished() then
                TutorialQuestManager.QuestTutorialPveMapPoint:SetActive(true, false)
              end
              GameFleetEnhance:CheckBuildAcademyTutorial()
              if immanentversion == 1 then
                GameFleetEnhance:GetFlashObject():InvokeASCallback("_root", "HideTutorialEnhance")
              end
              if immanentversion == 1 then
                GameFleetEnhance:GetFlashObject():InvokeASCallback("_root", "ShowTutorialClose")
              end
              GameFleetEnhance.tutorialClose = true
            end
          end
        end
      end
    end
  elseif cmd == "quickenhance" then
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local fleetId = fleets[self.currentSlectedIndex].id
    local equip = self:IsEquipmentInSlot(self.currentSelectEquipSlot)
    local equipment_enhance_req_param = {
      equip_id = equip.item_id,
      fleet_id = fleetId,
      once_more_enhance = 1
    }
    NetMessageMgr:SendMsg(NetAPIList.equipment_enhance_req.Code, equipment_enhance_req_param, self.equipmentEnhanceCallback, true, nil)
    if QuestTutorialEnhance_second:IsActive() and not QuestTutorialEnhance_second:IsFinished() and self.currentSlectedIndex == 2 then
      self:GetFlashObject():InvokeASCallback("_root", "HideTutorialQuickEnhance")
      self:GetFlashObject():InvokeASCallback("_root", "EnableEnhance")
      if immanentversion == 1 then
        self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialClose")
      end
      QuestTutorialEnhance_second:SetFinish(true)
      GameStateMainPlanet:SetStoryWhenFocusGain({51134})
    end
  elseif cmd == "evolution" then
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local fleetId = fleets[self.currentSlectedIndex].id
    local equip = self:IsEquipmentInSlot(self.currentSelectEquipSlot)
    if equip then
      local equipment_evolution_req_param = {
        equip_id = equip.item_id,
        fleet_id = fleetId
      }
      NetMessageMgr:SendMsg(NetAPIList.equipment_evolution_req.Code, equipment_evolution_req_param, self.equipmentEvolutionCallback, true, nil)
    end
  end
end
function GameFleetEnhance.clearCD()
  local param = {
    cdtype = GameUIGlobalScreen.equipmentCdType
  }
  NetMessageMgr:SendMsg(NetAPIList.cdtimes_clear_req.Code, param, GameFleetEnhance.clearCDCallback, false)
end
function GameFleetEnhance.clearCDCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.cdtimes_clear_req.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    end
    return true
  end
  return false
end
function GameFleetEnhance:HaveEnhanceCD()
  local curLevel = GameVipDetailInfoPanel:GetVipLevel()
  local viplimit = GameDataAccessHelper:GetVIPLimit("no_enhance_cd")
  return curLevel < viplimit
end
function GameFleetEnhance:UnlockQuickEnhance()
  local viplimit = 1
  local curLevel = GameVipDetailInfoPanel:GetVipLevel()
  return viplimit <= curLevel
end
function GameFleetEnhance:CheckCDCanEnhance()
  if not self:HaveEnhanceCD() then
    return true
  end
  return (...), self
end
function GameFleetEnhance:FleetSelect(index, forceRefresh)
  DebugOut("FleetSelect: ", self.currentSlectedIndex, index)
  if self.currentSlectedIndex ~= index or forceRefresh then
    if self.currentSlectedIndex ~= index then
      DebugOut(" self.currentSelectEquipSlot: ", self.currentSelectEquipSlot)
      self.currentSelectEquipSlot = -1
    end
    self.currentSlectedIndex = index
    do
      local fleets = GameGlobalData:GetData("fleetinfo").fleets
      local fleetId = fleets[self.currentSlectedIndex].id
      local bag_req_content = {
        owner = fleetId,
        bag_type = GameFleetInfoBag.BAG_OWNER_FLEET,
        pos = -1
      }
      local function netFailedCallback()
        NetMessageMgr:SendMsg(NetAPIList.bag_req.Code, bag_req_content, self.DownloadCurrentFleetEquipment, false, netFailedCallback)
      end
      NetMessageMgr:SendMsg(NetAPIList.bag_req.Code, bag_req_content, self.DownloadCurrentFleetEquipment, false, netFailedCallback)
    end
  end
end
function GameFleetEnhance.DownloadCurrentFleetEquipment(msgType, content)
  if msgType == NetAPIList.bag_ack.Code then
    GameFleetEnhance.curFleetEquipments = content.grids
    GameFleetEnhance.curFleetEquipments = GameFleetEnhance:processFleetEquipments()
    GameItemBag.equipedItem = GameItemBag:processEquipedItem(content.equipedItems)
    GameFleetEnhance:RefreshCurFleetEquipment()
    local slot = math.max(1, GameFleetEnhance.currentSelectEquipSlot)
    GameFleetEnhance:EquipmentSelect(slot, true)
    GameFleetEnhance:ShowEquipMent()
    return true
  elseif msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.bag_req.Code and content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameFleetEnhance:processFleetEquipments()
  local dest = {}
  for k, v in pairs(self.curFleetEquipments) do
    dest[v.pos] = v
  end
  return dest
end
function GameFleetEnhance:RefreshCurFleetEquipment()
  DebugOut("GameFleetEnhance:RefreshCurFleetEquipment")
  if not self:GetFlashObject() then
    GameFleetEnhance:LoadFlashObject()
  end
  GameUtils:printTable(self.curFleetEquipments)
  GameFleetEnhance:CheckTutorialEnhance(GameFleetEnhance:IsEquipmentInSlot(1))
  local frame, level = "", ""
  for i = 1, 5 do
    local equip = self:IsEquipmentInSlot(i)
    local equipments = GameGlobalData:GetData("equipments")
    if equip then
      do
        local currentEquipId = equip.item_id
        local currentEquipInfo = LuaUtils:table_values(LuaUtils:table_filter(equipments, function(k, v)
          return v.equip_id == currentEquipId
        end))[1]
        frame = frame .. "item_" .. GameFleetEnhance:GetDynamic(currentEquipInfo.equip_type) .. "\001"
        level = level .. GameLoader:GetGameText("LC_MENU_Level") .. currentEquipInfo.equip_level .. "\001"
      end
    else
      frame = frame .. "empty_e" .. i .. "\001"
      level = level .. "no" .. "\001"
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "SetFleetEquipment", frame, level, TutorialQuestManager.QuestTutorialCityHall:IsFinished())
  local equipment_action_list_req_param = {
    ids = {}
  }
  for i = 1, 5 do
    local equip = GameFleetEnhance:IsEquipmentInSlot(i)
    if equip then
      table.insert(equipment_action_list_req_param.ids, tonumber(equip.item_id))
    else
      table.insert(equipment_action_list_req_param.ids, -1)
    end
  end
  NetMessageMgr:SendMsg(NetAPIList.equipment_action_list_req.Code, equipment_action_list_req_param, GameFleetEnhance.EquipActionListCallback, true, nil)
end
function GameFleetEnhance.EquipActionListCallback(msgType, content)
  if msgType == NetAPIList.equipment_action_list_ack.Code then
    GameFleetEnhance.equipStatus = content.equipments
    local canEvolution = ""
    for i = 1, 5 do
      local equip = GameFleetEnhance:IsEquipmentInSlot(i)
      if equip then
        if GameFleetEnhance.equipStatus[i] and GameFleetEnhance.equipStatus[i] ~= -1 and GameFleetEnhance.equipStatus[i].can_evolute == 1 then
          canEvolution = canEvolution .. "true" .. "\001"
        else
          canEvolution = canEvolution .. "false" .. "\001"
        end
      else
        canEvolution = canEvolution .. "false" .. "\001"
      end
    end
    GameFleetEnhance:GetFlashObject():InvokeASCallback("_root", "SetEquipArrows", canEvolution)
    return true
  elseif msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.equipment_action_list_req.Code and content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameFleetEnhance:GetDynamic(frame, itemKey, index)
  local frame_temp = frame
  if DynamicResDownloader:IsDynamicStuff(frame_temp, DynamicResDownloader.resType.PIC) then
    local fullFileName = DynamicResDownloader:GetFullName(frame_temp .. ".png", DynamicResDownloader.resType.PIC)
    local localPath = "data2/" .. fullFileName
    if DynamicResDownloader:IfResExsit(localPath) then
      ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
    else
      frame_temp = "temp"
      self:AddDownloadPath(frame_temp, itemKey, index)
    end
  end
  return frame_temp
end
function GameFleetEnhance:AddDownloadPath(itemID, itemKey, index)
  DebugStore("Add download path", itemID, itemKey, index)
  local resName = itemID .. ".png"
  local extInfo = {}
  extInfo.itemKey = itemKey
  extInfo.index = index
  extInfo.item_id = itemID
  extInfo.resPath = "data2/LAZY_LOAD_dynamic_" .. resName
  DynamicResDownloader:AddDynamicRes(resName, DynamicResDownloader.resType.PIC, extInfo, nil)
end
function GameFleetEnhance:IsEquipmentInSlot(slot)
  return self.curFleetEquipments[slot]
end
function GameFleetEnhance:EquipmentSelect(slot, forceRefresh)
  if self.currentSelectEquipSlot ~= slot or forceRefresh then
    local can_evolute = self.equipStatus and self.equipStatus[slot] and self.equipStatus[slot] ~= -1 and self.equipStatus[slot].can_evolute == 1
    self:GetFlashObject():InvokeASCallback("_root", "setSelectEquip", slot, can_evolute)
    self.currentSelectEquipSlot = slot
    self:OnEquipmentSelect()
  end
end
function GameFleetEnhance:OnEquipmentSelect()
  local equip = self:IsEquipmentInSlot(self.currentSelectEquipSlot)
  if equip then
    local equipment_detail_req_param = {
      id = equip.item_id
    }
    NetMessageMgr:SendMsg(NetAPIList.equipment_detail_req.Code, equipment_detail_req_param, self.SetUpgradeMenu, true, nil)
  else
    GameFleetEnhance:SetBuyMenu()
    self:GetFlashObject():InvokeASCallback("_root", "showEquipEnhance", false)
  end
end
function GameFleetEnhance.SetUpgradeMenu(msgType, content)
  if msgType == NetAPIList.equipment_detail_ack.Code then
    DebugOut("\230\137\147\229\141\176content")
    DebugTable(content)
    local enableEvolution = content.enable_evolution == 0
    local enhance = content.enhance
    local evolution = content.evolution
    local recipe = content.recipe
    local recipe_total = content.recipe.recipe_total
    local enhanceIcon, evolutionIcon, recipeIcon = "", "", ""
    enhanceIcon = "item_" .. GameFleetEnhance:GetDynamic(enhance.equip_type)
    if enableEvolution then
      evolutionIcon = "item_" .. GameFleetEnhance:GetDynamic(evolution.equip_type)
      recipeIcon = "item_" .. GameFleetEnhance:GetDynamic(recipe.equip_type) .. "\001"
    end
    GameFleetEnhance.enhance_equip_type = enhance.equip_type
    GameFleetEnhance.enhance_equip = enhance
    GameFleetEnhance.evolution_equip_type = evolution.equip_type
    GameFleetEnhance.evolution_equip = evolution
    GameFleetEnhance.recipe_equip_type = recipe.equip_type
    GameFleetEnhance:GetFlashObject():InvokeASCallback("_root", "SetUpgradeMenu", enhanceIcon, evolutionIcon, recipeIcon, enableEvolution)
    local enhanceSrcLevel = enhance.equip_level ~= 0 and enhance.equip_level or GameLoader:GetGameText("LC_MENU_MAX_CHAR")
    local enhanceDestLevel = enhance.equip_next_level ~= 0 and enhance.equip_next_level or GameLoader:GetGameText("LC_MENU_MAX_CHAR")
    local paramText, enhanceSrcParam, enhanceDestParam, evolutionDestParam = "", "", "", ""
    for k, v in pairs(enhance.equip_params) do
      paramText = paramText .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. v.key) .. "\001"
      enhanceSrcParam = enhanceSrcParam .. v.value .. "\001"
    end
    for k, v in pairs(enhance.equip_next_params) do
      enhanceDestParam = enhanceDestParam .. v.value .. "\001"
    end
    for k, v in pairs(evolution.equip_params) do
      evolutionDestParam = evolutionDestParam .. v.value .. "\001"
    end
    local enhanceCost = enhance.cost
    local evolutionCost = evolution.cost
    local evolutionLevel, recipeText, enhanceNameText = "", "", ""
    enhanceNameText = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. enhance.equip_type)
    local canUpgrade, showWoring = false, false
    local needPlayerLevel = ""
    local levelinfo = GameGlobalData:GetData("levelinfo")
    if enableEvolution then
      evolutionLevel = evolution.equip_level
      recipeText = tostring(recipe.existed) .. "/" .. tostring(recipe_total)
      canUpgrade = levelinfo.level >= recipe.player_level_limit
    else
      evolutionReq = string.format(GameLoader:GetGameText("LC_MENU_Evolution_requirement"), content.enable_evolution)
      canUpgrade = false
    end
    if levelinfo.level < recipe.player_level_limit then
      showWoring = true
    end
    needPlayerLevel = string.format(GameLoader:GetGameText("LC_MENU_Evolution_requirement"), recipe.player_level_limit)
    DebugOut("paramText: ", paramText, " enhanceSrcParam: ", enhanceSrcParam, " enhanceDestParam: ", enhanceDestParam, " evolutionDestParam: ", evolutionDestParam)
    DebugOut("recipeText: ", recipeText, canUpgrade)
    GameFleetEnhance:GetFlashObject():InvokeASCallback("_root", "SetUpgradeMenuText", enhanceNameText, enhanceSrcLevel, enhanceDestLevel, evolutionLevel, paramText, enhanceSrcParam, enhanceDestParam, evolutionDestParam, enhanceCost, recipeText, evolutionCost)
    GameFleetEnhance:GetFlashObject():InvokeASCallback("_root", "enableUpgrad", canUpgrade, needPlayerLevel, showWoring)
    local equip = GameFleetEnhance:IsEquipmentInSlot(GameFleetEnhance.currentSelectEquipSlot)
    GameFleetEnhance:GetFlashObject():InvokeASCallback("_root", "showEquipEnhance", equip ~= nil, GameFleetEnhance.enhanceTab)
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.equipment_detail_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameFleetEnhance:SetBuyMenu()
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetIdentity = fleets[self.currentSlectedIndex].identity
  local slot = self.currentSelectEquipSlot
  local shopItemType = GameDataAccessHelper:GetEquipSlotShopType(slot, fleetIdentity)
  self.curBuyEquipType = shopItemType
  if shopItemType ~= -1 then
    local itemFrame = "item_" .. GameFleetEnhance:GetDynamic(shopItemType)
    local desText = GameLoader:GetGameText("LC_ITEM_ITEM_DESC_" .. shopItemType)
    local nameText = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. shopItemType)
    local costText = GameDataAccessHelper:GetEquipPrice(shopItemType)
    local lvText = GameLoader:GetGameText("LC_MENU_Level") .. GameDataAccessHelper:GetEquipReqLevel(shopItemType)
    DebugOut("SetBuyMenu: ", itemFrame, nameText, costText, desText, lvText)
    self:GetFlashObject():InvokeASCallback("_root", "SetBuyMenu", itemFrame, nameText, costText, desText, lvText)
  end
end
function GameFleetEnhance:showBuyEquipItenBox(arg)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local param = LuaUtils:string_split(arg, "\001")
  local butText = ""
  ItemBox:showItemBox("Equip", fleets[self.currentSlectedIndex][self.currentSelectEquipSlot], self.curBuyEquipType, tonumber(param[1]), tonumber(param[2]), tonumber(param[3]), tonumber(param[4]), butText)
end
function GameFleetEnhance:showEnhanceEquipItemBox(arg, item, item_type)
  local param = LuaUtils:string_split(arg, "\001")
  local butText = ""
  ItemBox:showItemBox("Equip", item, item_type, tonumber(param[1]), tonumber(param[2]), tonumber(param[3]), tonumber(param[4]), butText)
end
function GameFleetEnhance:showEnhanceItemItemBox(arg, item, item_type)
  local param = LuaUtils:string_split(arg, "\001")
  local butText = ""
  ItemBox:showItemBox("Item", item, item_type, tonumber(param[1]), tonumber(param[2]), tonumber(param[3]), tonumber(param[4]), "", nil, "", nil)
end
function GameFleetEnhance.equipmentEnhanceCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.equipment_enhance_req.Code then
    if content.code ~= 0 then
      if not GameUICollect:CheckTutorialCollect(content.code) and content.code == 8604 then
        local text = AlertDataList:GetTextFromErrorCode(130)
        local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
        GameUIKrypton.NeedMoreMoney(text)
      else
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
    else
      GameFleetEnhance:GetFlashObject():InvokeASCallback("_root", "showEnhanceAnim")
    end
    return true
  end
  return false
end
function GameFleetEnhance.equipmentEvolutionCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.equipment_evolution_req.Code then
    if content.code ~= 0 then
      if not GameUICollect:CheckTutorialCollect(content.code) and content.code == 130 then
        local text = AlertDataList:GetTextFromErrorCode(content.code)
        local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
        GameUIKrypton.NeedMoreMoney(text)
      else
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
    end
    GameItemBag:RequestBag(false)
    return true
  end
  return false
end
function GameFleetEnhance.BuyAndEquipCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.shop_buy_and_wear_req.Code then
    if content.code ~= 0 then
      if not GameUICollect:CheckTutorialCollect(content.code) and content.code == 709 then
        local text = AlertDataList:GetTextFromErrorCode(130)
        local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
        GameUIKrypton.NeedMoreMoney(text)
      else
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
    end
    return true
  end
  return false
end
if AutoUpdate.isAndroidDevice then
  function GameFleetEnhance.OnAndroidBack()
    GameFleetEnhance:OnFSCommand("closePress")
  end
end
