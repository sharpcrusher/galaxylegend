local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
local GameStateWD = GameStateManager.GameStateWD
local DynamicResDownloader = DynamicResDownloader
GameUIWDStuff.leaderboardTab = 1
GameUIWDStuff.playerLeaderboard = nil
GameUIWDStuff.playerInstarAlliance = nil
GameUIWDStuff.starAllianceLeaderboard = nil
GameUIWDStuff.targetPoint = {
  0,
  0,
  0
}
GameUIWDStuff.targetRank = {
  0,
  0,
  0
}
GameUIWDStuff.myRankList = {}
GameUIWDStuff.awardsCopyOfServerDataPlayer = nil
GameUIWDStuff.awardsCopyOfServerDataAlliance = nil
GameUIWDStuff.awardTab = 1
local ParsedAwardItem = {
  topInfo = nil,
  awardItemNum = 0,
  awardItemNames = "",
  awardItemCounts = "",
  awardItemFrames = "",
  awrdSubItem = {}
}
GameUIWDStuff.ParsedAwardItemList = {}
GameUIWDStuff.resultCopyOfServerData = nil
GameUIWDStuff.parsedResultInfo = {
  myAllianceName,
  playerPoint,
  alliancePoint,
  playerRank,
  allianceRank,
  playerAwardItem,
  allianceAwardItem,
  toponeAllianceNam,
  toponeAllianceLeader,
  toponeAllianceServer,
  toponeAllianceFlag
}
GameUIWDStuff.currentMenu = 1
GameUIWDStuff.menuType = {
  menu_type_rank = 1,
  menu_type_reword = 2,
  menu_type_result = 3,
  menu_type_help = 4
}
GameUIWDStuff.currentMenu = GameUIWDStuff.menuType.menu_type_rank
function GameUIWDStuff:SetFleetContent()
  self:InitFleetInfo()
  self:SetFleetInfoIcon()
  self:SetFleetInfoBanner()
  self:SetFleetInfoText()
end
function GameUIWDStuff:Init()
  local flash_obj = self:GetFlashObject()
  if flash_obj ~= nil then
    if self.currentMenu == GameUIWDStuff.menuType.menu_type_rank then
      GameUIWDStuff.leaderboardTab = 1
      GameUIWDStuff.playerLeaderboard = nil
      GameUIWDStuff.playerInstarAlliance = nil
      GameUIWDStuff.starAllianceLeaderboard = nil
      GameUIWDStuff.targetPoint = {
        0,
        0,
        0
      }
      GameUIWDStuff.targetRank = {
        0,
        0,
        0
      }
      GameUIWDStuff.myRankList = {}
    elseif self.currentMenu == GameUIWDStuff.menuType.menu_type_reword then
      GameUIWDStuff.awardsCopyOfServerDataPlayer = nil
      GameUIWDStuff.awardsCopyOfServerDataAlliance = nil
      GameUIWDStuff.awardTab = 1
      GameUIWDStuff.ParsedAwardItemList = {}
    elseif self.currentMenu == GameUIWDStuff.menuType.menu_type_result then
      GameUIWDStuff.resultCopyOfServerData = nil
    end
  end
end
function GameUIWDStuff:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  GameUIWDStuff:CheckDownloadImage()
  DebugOut(GameUIWDStuff.currentMenu)
  self:Init()
  self:MoveIn()
  self:GetFlashObject():InvokeASCallback("_root", "clearMenuListItem", self.currentMenu)
  if self.currentMenu == GameUIWDStuff.menuType.menu_type_rank then
    self:RequestLeaderboard(GameUIWDStuff.leaderboardTab)
  elseif self.currentMenu == GameUIWDStuff.menuType.menu_type_reword then
    self:RequestAwards(GameUIWDStuff.awardTab)
  elseif self.currentMenu == GameUIWDStuff.menuType.menu_type_result then
    self:RequestResultInfo()
  elseif self.currentMenu == GameUIWDStuff.menuType.menu_type_help then
  end
end
function GameUIWDStuff:CheckDownloadImage()
  if not AutoUpdateInBackground:IsHeroFileUpdatedToServer("data2/LAZY_LOAD_dom_result_bg.png") then
    self:GetFlashObject():ReplaceTexture("LAZY_LOAD_dom_result_bg.png", "territorial_map_bg.png")
  end
end
function GameUIWDStuff:Show(menuType)
  if not GameStateManager:GetCurrentGameState():IsObjectInState(self) then
    self.currentMenu = menuType
    GameStateManager:GetCurrentGameState():AddObject(self)
    if not self:GetFlashObject() then
      self:LoadFlashObject()
    end
  end
end
function GameUIWDStuff:IsDownloadPNGExsit(filename)
  return ...
end
function GameUIWDStuff:OnEraseFromGameState()
  self:UnloadFlashObject()
end
function GameUIWDStuff:MoveIn()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "animationMoveIn", self.currentMenu)
end
function GameUIWDStuff:Update(dt)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "OnUpdate")
    flash_obj:Update(dt)
    flash_obj:InvokeASCallback("_root", "onUpdateFrame", dt)
  end
end
function GameUIWDStuff:OnFSCommand(cmd, arg)
  if cmd == "leaderboardTabSelect" then
    self.leaderboardTab = tonumber(arg)
    self:RefreshLeaderboard()
  elseif cmd == "needUpdateRankItem" then
    self:UpdateLeaderboardItem(arg)
  elseif cmd == "needUpdateAwardItem" then
    self:UpdateAwardItem(arg)
  elseif cmd == "close_reward" then
    self.currentMenu = GameUIWDStuff.menuType.menu_type_rank
    GameUIWDStuff.IsShowRankWindow = false
    GameStateManager:GetCurrentGameState():EraseObject(self)
  elseif cmd == "awardItemReleased" then
    local awardItemIndex, awardItemSubIndex = arg:match("(%d+)\001(%d+)")
    DebugOut("item index = awardItemIndex, awardItemSubItemIndex", awardItemIndex, awardItemSubIndex)
    local clickedItem = GameUIWDStuff.ParsedAwardItemList[GameUIWDStuff.awardTab][tonumber(awardItemIndex)]
    local clickedSubItem = clickedItem.awrdSubItem[tonumber(awardItemSubIndex)]
    self:showItemDetil(clickedSubItem, clickedSubItem.item_type)
  elseif cmd == "awardTabSelect" then
    self.awardTab = tonumber(arg)
    self:RefreshAwards()
  elseif cmd == "allianceItemReleased" then
    local clickedSubItem = GameUIWDStuff.parsedResultInfo.allianceAwardItem.awrdSubItem[tonumber(arg)]
    self:showItemDetil(clickedSubItem, clickedSubItem.item_type)
  elseif cmd == "playerItemReleased" then
    local clickedSubItem = GameUIWDStuff.parsedResultInfo.playerAwardItem.awrdSubItem[tonumber(arg)]
    self:showItemDetil(clickedSubItem, clickedSubItem.item_type)
  end
end
function GameUIWDStuff:UpdateLeaderboardItem(id)
  local itemKey = tonumber(id)
  DebugOut("updateleaderitem")
  local curLeaderboard
  if GameUIWDStuff.leaderboardTab == 1 then
    curLeaderboard = GameUIWDStuff.playerLeaderboard
  elseif GameUIWDStuff.leaderboardTab == 2 then
    curLeaderboard = GameUIWDStuff.playerInstarAlliance
  else
    curLeaderboard = GameUIWDStuff.starAllianceLeaderboard
  end
  if curLeaderboard == nil then
    return
  end
  if itemKey > #curLeaderboard then
    return
  end
  local rank = 0
  local name, turkey = "", ""
  local itemInfo = curLeaderboard[itemKey]
  local allianceName = ""
  name = GameUtils:GetUserDisplayName(itemInfo.name)
  turkey = itemInfo.point
  rank = tonumber(itemInfo.rank)
  allianceName = itemInfo.alliance_name
  flagFrame = GameUtils:GetFlagFrameName(itemInfo.flag)
  if allianceName == "" then
    allianceName = "-"
  end
  DebugOut("rank item", name, turkey, rank)
  self:GetFlashObject():InvokeASCallback("_root", "setRankItem", itemKey, rank, name, turkey, allianceName, GameUIWDStuff.leaderboardTab, flagFrame)
end
function GameUIWDStuff:UpdateAwardItem(id)
  local itemKey = tonumber(id)
  if itemKey > #GameUIWDStuff.ParsedAwardItemList[GameUIWDStuff.awardTab] then
    return
  end
  local itemInfo = GameUIWDStuff.ParsedAwardItemList[GameUIWDStuff.awardTab][itemKey]
  self:GetFlashObject():InvokeASCallback("_root", "setAwardItem", itemKey, itemInfo.topInfo, itemInfo.awardItemNum, itemInfo.awardItemNames, itemInfo.awardItemCounts, itemInfo.awardItemFrames, GameUtils:IsChinese() and GameUtils:IsSimplifiedChinese())
end
function GameUIWDStuff:RefreshLeaderboard()
  GameUIWDStuff:GetFlashObject():InvokeASCallback("_root", "setLeaderboardTab", GameUIWDStuff.leaderboardTab)
  GameUIWDStuff:GetFlashObject():InvokeASCallback("_root", "clearRankListItem")
  local curLeaderboard
  if GameUIWDStuff.leaderboardTab == 1 then
    DebugOut("rank in player")
    curLeaderboard = GameUIWDStuff.playerLeaderboard
  elseif GameUIWDStuff.leaderboardTab == 2 then
    DebugOut("rank  in alliance")
    curLeaderboard = GameUIWDStuff.playerInstarAlliance
  else
    DebugOut("alliance rank")
    curLeaderboard = GameUIWDStuff.starAllianceLeaderboard
  end
  if curLeaderboard ~= nil then
    local itemCount = #curLeaderboard
    DebugOut("rank item count ", itemCount)
    GameUIWDStuff:GetFlashObject():InvokeASCallback("_root", "initRankListItem")
    for i = 1, itemCount do
      GameUIWDStuff:GetFlashObject():InvokeASCallback("_root", "addRankListItem", i)
    end
    GameUIWDStuff:GetFlashObject():InvokeASCallback("_root", "SetArrowInitVisibleRank")
    GameUIWDStuff:RefreshMyRank()
  else
    GameUIWDStuff:RequestLeaderboard(GameUIWDStuff.leaderboardTab)
  end
end
function GameUIWDStuff:RefreshMyRank()
  if GameUIWDStuff:GetFlashObject() ~= nil then
    if GameUIWDStuff.targetPoint[GameUIWDStuff.leaderboardTab] <= 0 then
      if GameUIWDStuff.myRankList[self.leaderboardTab] == nil then
        GameUIWDStuff.myRankList[self.leaderboardTab] = {}
        GameUIWDStuff.myRankList[self.leaderboardTab].rank = 0
        local userInfo = GameGlobalData:GetData("userinfo")
        local name_display = GameUtils:GetUserDisplayName(userInfo.name)
        local myAllianceStr = "-"
        local myFlag = 0
        if GameGlobalData:GetData("alliance").alliance_name == "" or GameGlobalData:GetData("alliance").alliance_name == nil then
          myAllianceStr = "-"
        else
          myAllianceStr = GameGlobalData:GetData("alliance").alliance_name
        end
        myFlag = GameUtils:GetFlagFrameName(GameGlobalData:GetData("alliance").flag)
        GameUIWDStuff.myRankList[self.leaderboardTab].name = name_display
        GameUIWDStuff.myRankList[self.leaderboardTab].alliance_name = myAllianceStr
        GameUIWDStuff.myRankList[self.leaderboardTab].point = 0
        GameUIWDStuff.myRankList[self.leaderboardTab].flag = myFlag
      end
      local rank = GameUIWDStuff.myRankList[GameUIWDStuff.leaderboardTab].rank
      if rank > 9999 then
        rank = 9999
      end
      local aFlagFrame = GameUtils:GetFlagFrameName(GameUIWDStuff.myRankList[self.leaderboardTab].flag)
      GameUIWDStuff:GetFlashObject():InvokeASCallback("_root", "displayMyRank", GameUtils:GetUserDisplayName(GameUIWDStuff.myRankList[GameUIWDStuff.leaderboardTab].name), GameUIWDStuff.myRankList[self.leaderboardTab].alliance_name, "" .. GameUIWDStuff.myRankList[GameUIWDStuff.leaderboardTab].point, rank, GameUIWDStuff.leaderboardTab, aFlagFrame)
    else
      local topInfo
      if GameUIWDStuff.leaderboardTab == 3 then
        topInfo = GameLoader:GetGameText("LC_MENU_WD_RANKING_ALLIANCE_NO_RANK_TEXT")
      else
        topInfo = GameLoader:GetGameText("LC_MENU_WD_RANKING_PERSONAL_NO_RANK_TEXT")
      end
      topInfo = string.gsub(topInfo, "<points>", "" .. GameUIWDStuff.targetPoint[GameUIWDStuff.leaderboardTab])
      topInfo = string.format(topInfo, GameUIWDStuff.targetRank[GameUIWDStuff.leaderboardTab])
      GameUIWDStuff:GetFlashObject():InvokeASCallback("_root", "SetTop200Info", topInfo)
    end
  end
end
function GameUIWDStuff:RefreshAwards()
  GameUIWDStuff:GetFlashObject():InvokeASCallback("_root", "setAwardTab", GameUIWDStuff.awardTab)
  self:GetFlashObject():InvokeASCallback("_root", "clearAwardListItem")
  local curAward, curAwardTitle
  local curWD = GameStateWD.activeWD
  if curWD == nil then
    return
  end
  local battleName = GameLoader:GetGameText(string.upper("LC_MENU_WD_BATTLE_NAME_" .. curWD.fight_name))
  local curAwardTitle = GameLoader:GetGameText("LC_MENU_WD_REWARD_DESC")
  DebugOut("curAwardTitle = ", curAwardTitle)
  curAwardTitle = string.format(curAwardTitle, battleName)
  DebugOut("curAwardTitle = ", curAwardTitle)
  self:GetFlashObject():InvokeASCallback("_root", "setAwardTitle", curAwardTitle)
  if GameUIWDStuff.awardTab == 1 then
    curAward = GameUIWDStuff.awardsCopyOfServerDataPlayer
  elseif GameUIWDStuff.awardTab == 2 then
    curAward = GameUIWDStuff.awardsCopyOfServerDataAlliance
  else
    return
  end
  GameUIWDStuff.ParsedAwardItemList[GameUIWDStuff.awardTab] = {}
  if curAward ~= nil then
    local itemCount = 0
    for _, v in ipairs(curAward) do
      itemCount = itemCount + 1
      table.insert(GameUIWDStuff.ParsedAwardItemList[GameUIWDStuff.awardTab], self:ParseAwardsItem(v))
    end
    self:GetFlashObject():InvokeASCallback("_root", "initAwardListItem")
    for i = 1, itemCount do
      self:GetFlashObject():InvokeASCallback("_root", "addAwardListItem", i)
    end
    self:GetFlashObject():InvokeASCallback("_root", "SetArrowInitVisibleAward")
  else
    GameUIWDStuff:RequestAwards(GameUIWDStuff.awardTab)
  end
end
function GameUIWDStuff:RefreshResults()
  local allianceStr, playerStr
  if GameUIWDStuff.parsedResultInfo.myAllianceID == 0 then
    local noAllianceStr = GameLoader:GetGameText("LC_MENU_WD_FINAL_RESULT_ALLIANCE_NO_ALLIANCE_TEXT")
    if self:GetFlashObject() then
      self:GetFlashObject():InvokeASCallback("_root", "setNoAllianceResult", noAllianceStr)
    end
  elseif GameUIWDStuff.resultCopyOfServerData ~= nil then
    local resultNumbers = #GameUIWDStuff.resultCopyOfServerData
    local playerInRank = true
    local allianceInRank = true
    if resultNumbers == 0 then
      allianceInRank = false
      playerInRank = false
    elseif resultNumbers == 1 then
      allianceInRank = true
      playerInRank = false
    end
    if allianceInRank then
      local allianceInfo = GameUIWDStuff.resultCopyOfServerData[1]
      GameUIWDStuff.parsedResultInfo.alliancePoint = allianceInfo.point
      GameUIWDStuff.parsedResultInfo.allianceRank = allianceInfo.rank
      GameUIWDStuff.parsedResultInfo.allianceAwardItem = self:ParseResultAwards(allianceInfo, true)
      allianceStr = GameLoader:GetGameText("LC_MENU_WD_FINAL_RESULT_ALLIANCE_RESULT_TEXT")
      allianceStr = string.format(allianceStr, GameUIWDStuff.parsedResultInfo.myAllianceName, GameUIWDStuff.parsedResultInfo.alliancePoint)
      if self:GetFlashObject() then
        self:GetFlashObject():InvokeASCallback("_root", "setAllianceResultInfo", allianceStr, GameUIWDStuff.parsedResultInfo.allianceRank, GameUIWDStuff.parsedResultInfo.allianceAwardItem.awardItemNum, GameUIWDStuff.parsedResultInfo.allianceAwardItem.awardItemNames, GameUIWDStuff.parsedResultInfo.allianceAwardItem.awardItemCounts, GameUIWDStuff.parsedResultInfo.allianceAwardItem.awardItemFrames)
      end
    else
      allianceStr = GameLoader:GetGameText("LC_MENU_WD_FINAL_RESULT_ALLIANCE_NO_RESULT_TEXT")
      if self:GetFlashObject() then
        self:GetFlashObject():InvokeASCallback("_root", "setAllianceNoResult", allianceStr)
      end
    end
    if playerInRank then
      local playerInfo = GameUIWDStuff.resultCopyOfServerData[2]
      GameUIWDStuff.parsedResultInfo.playerPoint = playerInfo.point
      GameUIWDStuff.parsedResultInfo.playerRank = playerInfo.rank
      GameUIWDStuff.parsedResultInfo.playerAwardItem = self:ParseResultAwards(playerInfo, false)
      playerStr = GameLoader:GetGameText("LC_MENU_WD_FINAL_RESULT_PERSONAL_RESULT_TEXT")
      playerStr = string.format(playerStr, GameUIWDStuff.parsedResultInfo.playerPoint)
      if self:GetFlashObject() then
        self:GetFlashObject():InvokeASCallback("_root", "setPlayerResultInfo", playerStr, GameUIWDStuff.parsedResultInfo.playerRank, GameUIWDStuff.parsedResultInfo.playerAwardItem.awardItemNum, GameUIWDStuff.parsedResultInfo.playerAwardItem.awardItemNames, GameUIWDStuff.parsedResultInfo.playerAwardItem.awardItemCounts, GameUIWDStuff.parsedResultInfo.playerAwardItem.awardItemFrames)
      end
    elseif self:GetFlashObject() then
      playerStr = GameLoader:GetGameText("LC_MENU_WD_FINAL_RESULT_PERSONAL_NO_RESULT_TEXT")
      self:GetFlashObject():InvokeASCallback("_root", "setPlayerNoResult", playerStr)
    end
  end
  local numberOneStr = GameLoader:GetGameText("LC_MENU_WD_FINAL_RESULT_CHAMPOIN_TEXT")
  numberOneStr = string.format(numberOneStr, GameUIWDStuff.parsedResultInfo.toponeAllianceNam)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "setCongratulationInfo", GameUIWDStuff.parsedResultInfo.toponeAllianceNam, GameUIWDStuff.parsedResultInfo.toponeAllianceLeader, GameUIWDStuff.parsedResultInfo.toponeAllianceServer, GameUIWDStuff.parsedResultInfo.toponeAllianceFlag + 1, numberOneStr)
  end
end
function GameUIWDStuff:ParseAwardsItem(awardItem)
  if awardItem == nil then
    return nil
  end
  local parsedItem = {}
  parsedItem.awrdSubItem = {}
  parsedItem.awardItemNames = ""
  parsedItem.awardItemCounts = ""
  parsedItem.awardItemFrames = ""
  parsedItem.topInfo = "Top " .. awardItem.begin_rank .. "-" .. awardItem.end_rank
  if awardItem.begin_rank == awardItem.end_rank then
    parsedItem.topInfo = "Top " .. awardItem.begin_rank
  end
  parsedItem.awardItemNum = #awardItem.awards
  local subIndex = 1
  for k, v in ipairs(awardItem.awards) do
    parsedItem.awrdSubItem[subIndex] = v
    DebugOut("Debug item info", v.item_type, "  " .. v.number)
    parsedItem.awardItemNames = parsedItem.awardItemNames .. GameHelper:GetAwardTypeText(v.item_type, v.number) .. "\001"
    parsedItem.awardItemCounts = parsedItem.awardItemCounts .. "x " .. GameUtils.numberConversion(GameHelper:GetAwardCount(v.item_type, v.number, v.no)) .. "\001"
    local frame = ""
    if DynamicResDownloader:IsDynamicStuffByGameItem(v) then
      local fullFileName = DynamicResDownloader:GetFullName(v.number .. ".png", DynamicResDownloader.resType.PIC)
      local localPath = "data2/" .. fullFileName
      DebugWD("localPath = ", localPath)
      if DynamicResDownloader:IfResExsit(localPath) then
        ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
        frame = "item_" .. v.number
      else
        frame = "temp"
        self:AddDownloadPathForReward(v.number)
      end
    elseif v.item_type == "fleet" then
      frame = GameDataAccessHelper:GetFleetAvatar(v.number, 0)
    else
      frame = GameHelper:GetAwardTypeIconFrameName(v.item_type, v.number, v.no)
    end
    parsedItem.awardItemFrames = parsedItem.awardItemFrames .. frame .. "\001"
    subIndex = subIndex + 1
  end
  return parsedItem
end
function GameUIWDStuff:ParseResultAwards(resultAwardItem, isAlliance)
  if resultAwardItem == nil then
    return nil
  end
  local parsedItem = {}
  parsedItem.awrdSubItem = {}
  parsedItem.awardItemNames = ""
  parsedItem.awardItemCounts = ""
  parsedItem.awardItemFrames = ""
  parsedItem.awardItemNum = #resultAwardItem.award
  local subIndex = 1
  for k, v in ipairs(resultAwardItem.award) do
    parsedItem.awrdSubItem[subIndex] = v
    local rewardText = GameHelper:GetAwardTypeText(v.item_type, v.number)
    if v.item_type == "item" then
      rewardText = GameLoader:GetGameText("LC_MENU_ITEM_CHAR")
    elseif v.item_type == "equip" then
      rewardText = GameLoader:GetGameText("LC_MENU_SLAC_EQUIPMENT_BUTTON")
    end
    parsedItem.awardItemNames = parsedItem.awardItemNames .. rewardText .. "\001"
    parsedItem.awardItemCounts = parsedItem.awardItemCounts .. "x " .. GameUtils.numberConversion(GameHelper:GetAwardCount(v.item_type, v.number, v.no)) .. "\001"
    local frame = ""
    if DynamicResDownloader:IsDynamicStuffByGameItem(v) then
      local fullFileName = DynamicResDownloader:GetFullName(v.number .. ".png", DynamicResDownloader.resType.PIC)
      local localPath = "data2/" .. fullFileName
      DebugWD("localPath = ", localPath)
      if DynamicResDownloader:IfResExsit(localPath) then
        ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
        frame = "item_" .. v.number
      else
        frame = "temp"
        self:AddDownloadPath(v.number, isAlliance, k)
      end
    elseif v.item_type == "fleet" then
      frame = GameDataAccessHelper:GetFleetAvatar(v.number, 0)
    else
      frame = GameHelper:GetAwardTypeIconFrameName(v.item_type, v.number, v.no)
    end
    parsedItem.awardItemFrames = parsedItem.awardItemFrames .. frame .. "\001"
    subIndex = subIndex + 1
  end
  DebugWD("ParseResultAwards")
  DebugWDTable(parsedItem)
  return parsedItem
end
function GameUIWDStuff:AddDownloadPath(itemID, isAlliance, index)
  local resName = itemID .. ".png"
  local extInfo = {}
  extInfo.isAlliance = isAlliance
  extInfo.index = index
  extInfo.item_id = itemID
  extInfo.localPath = "data2/LAZY_LOAD_dynamic_" .. resName
  DynamicResDownloader:AddDynamicRes(resName, DynamicResDownloader.resType.PIC, extInfo, GameUIWDStuff.donamicDownloadFinishCallback)
end
function GameUIWDStuff:AddDownloadPathForReward(itemID)
  local resName = itemID .. ".png"
  local extInfo = {}
  extInfo.item_id = itemID
  extInfo.localPath = "data2/LAZY_LOAD_dynamic_" .. resName
  DynamicResDownloader:AddDynamicRes(resName, DynamicResDownloader.resType.PIC, extInfo, nil)
end
function GameUIWDStuff.donamicDownloadFinishCallback(extInfo)
  DebugWD("donamicDownloadFinishCallback")
  GameUIWDStuff:RefreshResults()
  return true
end
function GameUIWDStuff:ParseAllAwards()
end
function GameUIWDStuff:showItemDetil(item_, item_type)
  if GameHelper:IsResource(item_type) then
    return
  end
  local item = item_
  if item_type == "krypton" then
    if item.level == 0 then
      item.level = 1
    end
    local detail = GameUIKrypton:TryQueryKryptonDetail(item.number, item.level, function(msgType, content)
      local ret = GameUIKrypton.QueryKryptonDetailCallBack(msgType, content)
      if ret then
        local tmp = GameGlobalData:GetKryptonDetail(item.number, item.level)
        if tmp == nil then
          return false
        end
        ItemBox:SetKryptonBox(tmp, item.number)
        local operationTable = {}
        operationTable.btnUnloadVisible = false
        operationTable.btnUpgradeVisible = false
        operationTable.btnUseVisible = false
        operationTable.btnDecomposeVisible = false
        ItemBox:ShowKryptonBox(320, 240, operationTable)
      end
      return ret
    end)
    if detail == nil then
    else
      ItemBox:SetKryptonBox(detail, item.number)
      local operationTable = {}
      operationTable.btnUnloadVisible = false
      operationTable.btnUpgradeVisible = false
      operationTable.btnUseVisible = false
      operationTable.btnDecomposeVisible = false
      ItemBox:ShowKryptonBox(320, 240, operationTable)
    end
  end
  if item_type == "item" then
    item.cnt = 1
    ItemBox:showItemBox("Item", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
  end
  if item_type == "fleet" then
    ItemBox:ShowCommanderDetail2(tonumber(item.number))
  end
end
function GameUIWDStuff:RequestLeaderboard(leaderboardType)
  local request_content = {rank_type = leaderboardType}
  NetMessageMgr:SendMsg(NetAPIList.domination_ranks_req.Code, request_content, GameUIWDStuff.FetchLeaderboardCallback, true, nil)
end
function GameUIWDStuff:RequestAwards(awardType)
  local request_content = {flag = awardType}
  NetMessageMgr:SendMsg(NetAPIList.wd_award_req.Code, request_content, GameUIWDStuff.FetchAwardsCallback, true, nil)
end
function GameUIWDStuff:RequestResultInfo()
  NetMessageMgr:SendMsg(NetAPIList.domination_awards_list_req.Code, nil, GameUIWDStuff.FetchResultInfoCallback, false, nil)
end
function GameUIWDStuff.FetchLeaderboardCallback(msgType, content)
  if msgType == NetAPIList.domination_ranks_ack.Code then
    GameUIWDStuff.targetPoint[GameUIWDStuff.leaderboardTab] = 0
    GameUIWDStuff.targetRank[GameUIWDStuff.leaderboardTab] = 0
    if #content.params >= 2 then
      GameUIWDStuff.targetPoint[GameUIWDStuff.leaderboardTab] = content.params[1]
      GameUIWDStuff.targetRank[GameUIWDStuff.leaderboardTab] = content.params[2]
    end
    local myRankInRankList = false
    local rankID = -1
    if GameUIWDStuff.targetPoint[GameUIWDStuff.leaderboardTab] <= 0 then
      myRankInRankList = true
    end
    if content.ranks ~= nil then
      if GameUIWDStuff.leaderboardTab == 1 then
        GameUIWDStuff.playerLeaderboard = content.ranks
        rankID = GameGlobalData:GetData("userinfo").player_id
      elseif GameUIWDStuff.leaderboardTab == 2 then
        GameUIWDStuff.playerInstarAlliance = content.ranks
        rankID = GameGlobalData:GetData("userinfo").player_id
      else
        GameUIWDStuff.starAllianceLeaderboard = content.ranks
        rankID = GameGlobalData:GetData("alliance").id
      end
      DebugOut("user id = ", rankID)
      DebugOut("user id is a ", type(rankID))
      DebugOut("GameUtils:GetActiveServerInfo().logic_id = ", GameUtils:GetActiveServerInfo().logic_id)
      DebugTable(GameGlobalData:GetData("userinfo"))
      local server_id = GameGlobalData:GetData("userinfo").server
      if myRankInRankList then
        DebugOut("in the ranks, begin to search")
        for i = 1, #content.ranks do
          DebugOut("search in", i)
          DebugOut("search in id = ", content.ranks[i].id, ",", content.ranks[i].server)
          DebugOut(type(content.ranks[i].id))
          if content.ranks[i].id == tonumber(rankID) and content.ranks[i].server == tonumber(server_id) then
            GameUIWDStuff.myRankList[GameUIWDStuff.leaderboardTab] = content.ranks[i]
          end
        end
      end
    end
    DebugOut("GameUIWDStuff.targetPoint = ", GameUIWDStuff.targetPoint[GameUIWDStuff.leaderboardTab])
    DebugOut("GameUIWDStuff.targetRank = ", GameUIWDStuff.targetRank[GameUIWDStuff.leaderboardTab])
    GameUIWDStuff:RefreshLeaderboard()
    return true
  end
  return false
end
function GameUIWDStuff.FetchAwardsCallback(msgType, content)
  if msgType == NetAPIList.wd_award_ack.Code then
    if content.infoes ~= nil then
      if GameUIWDStuff.awardTab == 1 then
        GameUIWDStuff.awardsCopyOfServerDataPlayer = content.infoes
      elseif GameUIWDStuff.awardTab == 2 then
        GameUIWDStuff.awardsCopyOfServerDataAlliance = content.infoes
      end
      DebugWD("FetchAwardsCallback")
    end
    GameUIWDStuff:RefreshAwards()
    return true
  end
  return false
end
function GameUIWDStuff.FetchResultInfoCallback(msgType, content)
  if msgType == NetAPIList.domination_awards_list_ack.Code and content ~= nil then
    GameUIWDStuff.resultCopyOfServerData = nil
    if content.awards ~= nil then
      GameUIWDStuff.resultCopyOfServerData = content.awards
    end
    GameUIWDStuff.parsedResultInfo.myAllianceID = content.alliance_id
    GameUIWDStuff.parsedResultInfo.myAllianceName = content.alliance_name
    NetMessageMgr:SendMsg(NetAPIList.wd_last_champion_req.Code, nil, GameUIWDStuff.FetchResultChampionCallback, true, nil)
    return true
  end
  return false
end
function GameUIWDStuff.FetchResultChampionCallback(msgType, content)
  if msgType == NetAPIList.wd_last_champion_ack.Code and content ~= nil then
    GameUIWDStuff.parsedResultInfo.toponeAllianceNam = content.alliance_name
    GameUIWDStuff.parsedResultInfo.toponeAllianceLeader = content.alliance_leader
    GameUIWDStuff.parsedResultInfo.toponeAllianceServer = content.server_name
    GameUIWDStuff.parsedResultInfo.toponeAllianceFlag = content.flag
    if (GameUtils:GetSaveLang() == "cn" or GameUtils:GetIsCNBundleType() == true) and (content.flag == 18 or content.flag == 34 or content.flag == 39) then
      GameUIWDStuff.parsedResultInfo.toponeAllianceFlag = 38
    end
    GameUIWDStuff:RefreshResults()
    return true
  end
  return false
end
function GameUIWDStuff:SetHelpText(textStr)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "setHelpText", textStr)
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUIWDStuff.OnAndroidBack()
    if GameUIWDStuff.currentMenu == GameUIWDStuff.menuType.menu_type_help then
      local flash_obj = GameUIWDStuff:GetFlashObject()
      if flash_obj then
        flash_obj:InvokeASCallback("_root", "hideHelp")
      end
      GameUIWDStuff.currentMenu = GameUIWDStuff.menuType.menu_type_rank
    elseif GameUIWDStuff.currentMenu == GameUIWDStuff.menuType.menu_type_reword then
      local flash_obj = GameUIWDStuff:GetFlashObject()
      if flash_obj then
        flash_obj:InvokeASCallback("_root", "HideRewardWindow")
      end
      GameUIWDStuff.currentMenu = GameUIWDStuff.menuType.menu_type_rank
    elseif GameUIWDStuff.currentMenu == GameUIWDStuff.menuType.menu_type_result then
      local flash_obj = GameUIWDStuff:GetFlashObject()
      if flash_obj then
        flash_obj:InvokeASCallback("_root", "HideResultWindow")
      end
      GameUIWDStuff.currentMenu = GameUIWDStuff.menuType.menu_type_rank
    elseif GameUIWDStuff.IsShowRankWindow then
      local flash_obj = GameUIWDStuff:GetFlashObject()
      if flash_obj then
        flash_obj:InvokeASCallback("_root", "animationMoveOut")
      end
      GameUIWDStuff.IsShowRankWindow = false
    else
      GameUIWDStuff:OnFSCommand("close_reward")
    end
  end
end
