local GameUIChat = LuaObjectManager:GetLuaObject("GameUIChat")
GameUIGlobalChatBar = {}
function GameUIGlobalChatBar:GetLuaObj()
  return self.mLuaObj
end
function GameUIGlobalChatBar:Release()
  self.mLuaObj = nil
  self.mFlashName = nil
  self.mFlashObj = nil
  self.mDefaultChanel = nil
end
function GameUIGlobalChatBar:Init(luaObj, flashObj, flashName)
  self.mLuaObj = luaObj
  self.mFlashName = flashName
  self.mFlashObj = flashObj
  self.mFlashObj:InvokeASCallback(self.mFlashName, "ChatBar_updateText", "")
  self.mDefaultChanel = nil
  local chatCount = GameGlobalData:GetData("newChatTotalCount")
  if chatCount and chatCount > 0 then
    self.mFlashObj:InvokeASCallback(self.mFlashName, "ChatBar_ShowOrangeChatBtn", true)
  end
end
function GameUIGlobalChatBar:UpdateChatBarText(htmlText)
  if self.mFlashObj then
    self.mFlashObj:InvokeASCallback(self.mFlashName, "ChatBar_updateText", htmlText)
  end
end
function GameUIGlobalChatBar:SetDefaultChanel(chanel)
  self.mDefaultChanel = chanel
end
function GameUIGlobalChatBar:MoveIn()
  self.mFlashObj:InvokeASCallback(self.mFlashName, "ChatBar_MoveIn")
end
function GameUIGlobalChatBar:MoveOut()
  self.mFlashObj:InvokeASCallback(self.mFlashName, "ChatBar_MoveOut")
end
function GameUIGlobalChatBar:RegisterMsgHandler()
  GameGlobalData:RegisterDataChangeCallback("newChatTotalCount", GameUIGlobalChatBar.UpdateChatButtonStatus)
end
function GameUIGlobalChatBar:RemoveMsgHandler()
  GameGlobalData:RemoveDataChangeCallback("newChatTotalCount", GameUIGlobalChatBar.UpdateChatButtonStatus)
end
function GameUIGlobalChatBar.UpdateChatButtonStatus()
  DebugOut("UpdateChatButtonStatus")
  local chatCount = GameGlobalData:GetData("newChatTotalCount")
  local flashObj = self.mFlashObj
  if flashObj == nil then
    return
  end
  if chatCount and chatCount > 0 then
    flashObj:InvokeASCallback(self.mFlashName, "ChatBar_ShowOrangeChatBtn", true)
  else
    flashObj:InvokeASCallback(self.mFlashName, "ChatBar_ShowOrangeChatBtn", false)
  end
end
function GameUIGlobalChatBar:OnFSCommand(cmd, arg)
  if cmd == "PopupChatWindow" then
    self:MoveOut()
    GameStateManager:GetCurrentGameState():AddObject(GameUIChat)
    if self.mDefaultChanel then
      GameUIChat:SelectChannel(self.mDefaultChanel)
    else
      GameUIChat:SelectChannel(GameUIChat.activeChannel)
    end
  end
end
