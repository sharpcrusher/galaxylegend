local GameStateAlliance = GameStateManager.GameStateAlliance
local GameUIPlayerView = LuaObjectManager:GetLuaObject("GameUIPlayerView")
local GameMail = LuaObjectManager:GetLuaObject("GameMail")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameFleetInfoBackground = LuaObjectManager:GetLuaObject("GameFleetInfoBackground")
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
GameUIPlayerView._PlayerID = nil
GameUIPlayerView._PlayerData = {}
GameUIPlayerView._ItemBoxRect = {}
GameUIPlayerView.mCurSelectedIndex = 1
function GameUIPlayerView:SetPlayerData(data_player)
  self._PlayerData = data_player
  self:updateLeftDate()
  if self._PlayerData.cached_fleets[1] then
    self:updatePlayerDate(1)
    self:updateFleetDate(1)
  end
  self.m_current_fleet = 1
end
function GameUIPlayerView:UpdateWithPlayerID(player_id)
  local content = {user_id = player_id}
  local function netCallProcess()
    NetMessageMgr:SendMsg(NetAPIList.user_brief_info_req.Code, content, self.NetCallbackPlayerInfo, true, netCallProcess)
  end
  netCallProcess()
end
function GameUIPlayerView:OnAddToGameState(gamestate)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  GameUIPlayerView:MoveIn()
end
function GameUIPlayerView:OnEraseFromGameState(game_state)
  self._PlayerData = nil
  self:UnloadFlashObject()
end
function GameUIPlayerView:MoveIn()
  local forceText = GameLoader:GetGameText("LC_MENU_FRIEND_BATTLE")
  local kryptonText = GameLoader:GetGameText("LC_MENU_KRYPTON_TAB")
  self:GetFlashObject():InvokeASCallback("_root", "lua2fs_MoveIn", forceText, kryptonText)
end
function GameUIPlayerView.RefreshIcon()
  DebugOut("GameUIPlayerView.RefreshIcon")
  GameUIPlayerView:updateFleetDate(GameUIPlayerView.mCurSelectedIndex)
end
function GameUIPlayerView:updateFleetDate(fleet_index)
  local flashObj = self:GetFlashObject()
  if flashObj then
    GameUIPlayerView.mCurSelectedIndex = fleet_index
    local fleet_data = self._PlayerData.cached_fleets[fleet_index]
    local avatar_pic, avatar_pos, level = "", "", ""
    local StrengthenLv = ""
    local fleetID = fleet_data.fleet_identity
    if fleetID == 1 and self._PlayerData.icon ~= 0 and self._PlayerData.icon ~= 1 then
      fleetID = self._PlayerData.icon
    end
    local frame = GameDataAccessHelper:GetCommanderVesselsImage(fleetID, fleet_data.level)
    for i = 1, 5 do
      if fleet_data and fleet_data.equipments[i] and fleet_data.equipments[i].equip_type then
        avatar_pic = avatar_pic .. "item_" .. fleet_data.equipments[i].equip_type .. "\001"
        avatar_pos = avatar_pos .. fleet_data.equipments[i].equip_slot .. "\001"
        level = level .. fleet_data.equipments[i].equip_level .. "\001"
        StrengthenLv = StrengthenLv .. fleet_data.equipments[i].enchant_level .. "\001"
      else
        avatar_pic = avatar_pic .. "empty_e" .. i .. "\001"
        avatar_pos = avatar_pos .. "no" .. "\001"
        level = level .. "no" .. "\001"
        StrengthenLv = StrengthenLv .. "0" .. "\001"
      end
    end
    local kryptonCnt = 0
    local kryptonFr = ""
    local kryptonLv = ""
    if fleet_data and 0 < #fleet_data.kryptons then
      for k = 1, #fleet_data.kryptons do
        local item = fleet_data.kryptons[k]
        local typeId = GameUIKrypton:GetKryptonType(item)
        local fr = "item_" .. tostring(typeId)
        if GameHelper:DownloadResIfNeed(typeId, nil) then
          fr = "temp"
        end
        kryptonFr = kryptonFr .. fr .. "\001"
        kryptonLv = kryptonLv .. GameLoader:GetGameText("LC_MENU_Level") .. tostring(item.level) .. "\001"
        kryptonCnt = kryptonCnt + 1
      end
    end
    DebugOut("kryptonFr: " .. kryptonFr)
    flashObj:InvokeASCallback("_root", "updateFleetDate", avatar_pic, avatar_pos, frame, level, StrengthenLv, GameLoader:GetGameText("LC_MENU_Level"))
    flashObj:InvokeASCallback("_root", "ReflashLeftInfo", fleet_index)
    flashObj:InvokeASCallback("_root", "SetKryptonList", kryptonCnt, kryptonFr, kryptonLv)
    local avatarFr = ""
    if 0 < fleet_data.adjutant_id then
      avatarFr = GameDataAccessHelper:GetFleetAvatar(fleet_data.adjutant_id)
    end
    flashObj:InvokeASCallback("_root", "SetAdjutantFlg", avatarFr)
  end
end
function GameUIPlayerView:updateLeftDate()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  local fleetTotal = #self._PlayerData.cached_fleets
  local avatar_pic, name = "", ""
  for i = 1, 5 do
    if self._PlayerData.cached_fleets[i] and self._PlayerData.cached_fleets[i].fleet_identity then
      local identity = self._PlayerData.cached_fleets[i].fleet_identity
      local iconFrame = GameDataAccessHelper:GetFleetAvatar(identity, self._PlayerData.cached_fleets[i].level)
      if identity == 1 then
        iconFrame = GameDataAccessHelper:GetFleetAvatar(identity, self._PlayerData.cached_fleets[i].level, self._PlayerData.icon)
        if self._PlayerData.icon ~= 0 and self._PlayerData.icon ~= 1 then
          iconFrame = GameDataAccessHelper:GetFleetAvatar(self._PlayerData.icon, self._PlayerData.cached_fleets[i].level)
        end
      end
      avatar_pic = avatar_pic .. iconFrame .. "\001"
    else
      avatar_pic = avatar_pic .. "no" .. "\001"
    end
    if self._PlayerData.cached_fleets[i] and self._PlayerData.cached_fleets[i].fleet_identity then
      local identity = self._PlayerData.cached_fleets[i].fleet_identity
      local fleet_name = GameDataAccessHelper:GetFleetLevelDisplayName(self._PlayerData.cached_fleets[i].fleet_identity, self._PlayerData.cached_fleets[i].level)
      if identity == 1 then
        fleet_name = GameUtils:GetUserDisplayName(self._PlayerData.name)
        if self._PlayerData.cached_fleets[i].level > 0 then
          fleet_name = fleet_name .. " [+" .. self._PlayerData.cached_fleets[i].level .. "]"
        end
      end
      name = name .. fleet_name .. "\001"
    else
      name = name .. "no" .. "\001"
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "updateLeftDate", fleetTotal, avatar_pic, name)
end
function GameUIPlayerView:updatePlayerDate(_index)
  local name, icon
  local identity = self._PlayerData.cached_fleets[_index].fleet_identity
  local fleetId = identity
  if identity == 1 then
    icon = GameDataAccessHelper:GetFleetAvatar(identity, self._PlayerData.cached_fleets[_index].level, self._PlayerData.icon)
    if self._PlayerData.icon ~= 0 and self._PlayerData.icon ~= 1 then
      icon = GameDataAccessHelper:GetFleetAvatar(self._PlayerData.icon, self._PlayerData.cached_fleets[_index].level)
      fleetId = self._PlayerData.icon
    end
  else
    icon = GameDataAccessHelper:GetFleetAvatar(identity, self._PlayerData.cached_fleets[_index].level)
  end
  name = GameDataAccessHelper:GetFleetLevelDisplayName(identity, self._PlayerData.cached_fleets[_index].level)
  if identity == 1 then
    name = GameUtils:GetUserDisplayName(self._PlayerData.name)
    if self._PlayerData.cached_fleets[_index].level > 0 then
      name = name .. " [+" .. self._PlayerData.cached_fleets[_index].level .. "]"
    end
  end
  local level = GameLoader:GetGameText("LC_MENU_Level") .. self._PlayerData.level
  local force = GameUtils.numberConversion(self._PlayerData.cached_fleets[_index].force)
  local krypton = GameUtils.numberConversion(self._PlayerData.cached_fleets[_index].krypton)
  local color = GameDataAccessHelper:GetCommanderColorFrame(fleetId, self._PlayerData.cached_fleets[_index].level)
  local CommanderAbility = GameDataAccessHelper:GetCommanderAbility(fleetId, self._PlayerData.cached_fleets[_index].level)
  DebugOut("CommanderAbility = ")
  DebugTable(CommanderAbility)
  DebugOut(self._PlayerData.level)
  local rankFrame = GameUtils:GetFleetRankFrame(CommanderAbility.Rank, GameUIPlayerView.DownloadRankimageCallback)
  local vessleType = GameDataAccessHelper:GetCommanderVesselsType(fleetId, self._PlayerData.cached_fleets[_index].level)
  local sex = GameDataAccessHelper:GetCommanderSex(fleetId)
  if identity == 1 then
    sex = self._PlayerData.icon
  end
  if sex == 1 then
    sex = "man"
  elseif sex == 0 then
    sex = "woman"
  else
    sex = "unknown"
  end
  DebugOut("asdkjasdhkaj:", sex)
  self:GetFlashObject():InvokeASCallback("_root", "updatePlayerDate", name, level, force, krypton, icon, identity, vessleType, color, rankFrame, sex)
end
function GameUIPlayerView.DownloadRankimageCallback(extInfo)
  if GameUIPlayerView:GetFlashObject() then
    GameUIPlayerView:GetFlashObject():InvokeASCallback("_root", "updateRankImage", extInfo.rank_id)
  end
end
function GameUIPlayerView:GetEquipItem(avatar_pos)
  for i = 1, 5 do
    if self._PlayerData.cached_fleets[self.m_current_fleet] and self._PlayerData.cached_fleets[self.m_current_fleet].equipments[i] and self._PlayerData.cached_fleets[self.m_current_fleet].equipments[i].equip_slot == tonumber(avatar_pos) then
      return self._PlayerData.cached_fleets[self.m_current_fleet].equipments[i]
    end
  end
end
function GameUIPlayerView:GetPlayerIcon(icon)
  if icon == 0 then
    return "female"
  else
    return "male"
  end
end
function GameUIPlayerView:ShowPlayerView()
  self:LoadFlashObject()
  GameStateManager:GetCurrentGameState():AddObject(self)
end
function GameUIPlayerView:OnFSCommand(cmd, arg)
  if cmd == "move_out_over" then
    GameStateManager:GetCurrentGameState():EraseObject(self)
  end
  if cmd == "avatarClicked" then
    self:updateFleetDate(tonumber(arg))
    self:updatePlayerDate(tonumber(arg))
    self.m_current_fleet = tonumber(arg)
  end
  if cmd == "showEquip" then
    local item = self:GetEquipItem(arg)
    if item and item.equip_type then
      ItemBox:showItemBox("Equip", item, item.equip_type, 120, 251, 127, 127)
      ItemBox:hideEquipButton()
    end
  elseif cmd == "showKrypton" then
    GameUIPlayerView:ShowKryptonDetail(tonumber(arg))
  end
end
function GameUIPlayerView:ShowKryptonDetail(idx)
  local fleet_data = self._PlayerData.cached_fleets[self.m_current_fleet]
  if fleet_data and fleet_data.kryptons[idx] then
    local item = fleet_data.kryptons[idx]
    local typeId = GameUIKrypton:GetKryptonType(item)
    ItemBox:SetKryptonBox(item, typeId, #item.formation)
    local operationTable = {}
    operationTable.btnUnloadVisible = false
    operationTable.btnUpgradeVisible = false
    operationTable.btnUseVisible = false
    operationTable.btnDecomposeVisible = false
    ItemBox:ShowKryptonBox(320, 240, operationTable)
  end
end
function GameUIPlayerView.NetCallbackPlayerInfo(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.user_brief_info_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.user_brief_info_ack.Code then
    GameUIPlayerView:SetPlayerData(content)
    GameUIPlayerView:ShowPlayerView()
    return true
  end
  return false
end
if AutoUpdate.isAndroidDevice then
  function GameUIPlayerView.OnAndroidBack()
    GameUIPlayerView:GetFlashObject():InvokeASCallback("_root", "viewMoveOut")
  end
end
