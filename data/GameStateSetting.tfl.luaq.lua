local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
local GameStateSetting = GameStateManager.GameStateSetting
function GameStateSetting:InitGameState()
end
function GameStateSetting:OnFocusGain(previousState)
  DebugOut("GameStateSetting:OnFocusGain", previousState)
  self.m_preState = previousState
  self:AddObject(GameSetting)
end
function GameStateSetting:OnFocusLost(newState)
  self.m_preState = nil
  self:EraseObject(GameSetting)
end
function GameStateSetting:GetPreState()
  return self.m_preState
end
