local GamePushNotification = LuaObjectManager:GetLuaObject("GamePushNotification")
local GameObjectMineMap = LuaObjectManager:GetLuaObject("GameObjectMineMap")
local GameUIArena = LuaObjectManager:GetLuaObject("GameUIArena")
local GameUICollect = LuaObjectManager:GetLuaObject("GameUICollect")
local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
local NotificationID = {
  mine = 150323001,
  arena = 150323002,
  battle_supply = 150323003,
  building = 150323004,
  equipment = 150323005,
  collection = 150323006,
  worldboss = 150323007,
  offline = 150323008
}
function GamePushNotification:OnInitGame()
  DebugOut("GamePushNotification:OnInitGame")
  GameGlobalData:RegisterDataChangeCallback("push_button_info", self.OnPushButtonChange)
  if ext.GetPlatform() == "Android" then
    GamePushNotification.InitNotification()
  elseif ext.RegisterNotification ~= nil then
    DebugOut("RegisterNotification ios ")
    ext.RegisterNotification()
    GamePushNotification.InitNotification()
  end
end
function GamePushNotification.ChangePushSetting(arg, isTurnOn)
  local push_info = GameGlobalData:GetData("push_button_info")
  local isTurn = 0
  if isTurnOn then
    isTurn = 1
  end
  if push_info == nil then
    DebugOut("GamePushNotification.ChangePushSetting:push_info is nil")
    return
  end
  if arg == "all" then
    for k, v in pairs(push_info) do
      push_info[k] = isTurn
    end
  else
    if arg == "activity" then
      push_info.activity = isTurn
    elseif arg == "champion" then
      push_info.champion = isTurn
    elseif arg == "colony" then
      push_info.colony = isTurn
    elseif arg == "mine" then
      push_info.mine = isTurn
    elseif arg == "building" then
      push_info.building = isTurn
    elseif arg == "equipment" then
      push_info.equipment = isTurn
    elseif arg == "revenue" then
      push_info.revenue = isTurn
    elseif arg == "alliance" then
      push_info.alliance = isTurn
    end
    if 0 >= push_info.all and isTurn > 0 then
      push_info.all = isTurn
    end
  end
  local content = {}
  content.req = push_info
  NetMessageMgr:SendMsg(NetAPIList.push_button_req.Code, content, GamePushNotification.ChangePushSettingCallback, false, nil)
end
function GamePushNotification.ChangePushSettingCallback(msgType, content)
  DebugOut("ChangePushSettingCallback:")
  DebugOut(msgType)
  DebugTable(content)
  DebugTable(GameGlobalData:GetData("push_button_info"))
  if msgType == NetAPIList.common_ack.Code and content and content.api == NetAPIList.push_button_req.Code then
    DebugOut("successful")
    GamePushNotification.OnPushButtonChange()
    return true
  else
    NetMessageMgr:SendMsg(NetAPIList.push_button_info_req.Code, nil, GamePushNotification.RequestPushButtonInfoCallback, false, nil)
    return false
  end
end
function GamePushNotification.RequestPushButtonInfoCallback(msgType, content)
  DebugOut("RequestPushButtonInfoCallback")
  DebugOut(msgType)
  DebugTable(content)
  if msgType == NetAPIList.push_button_info_ack.Code and content then
    GameGlobalData:UpdateGameData("push_button_info", content.ack)
    GameGlobalData:RefreshData("push_button_info")
    return true
  else
    DebugOut("RequestPushButtonInfoCallback:msgType is wrong")
    return false
  end
end
function GamePushNotification.OnPushButtonChange()
  local push_info = GameGlobalData:GetData("push_button_info")
  DebugOut("OnPushButtonChange:")
  DebugTable(push_info)
  if push_info then
    if push_info.all <= 0 then
      ext.localpush.cancelAll()
    else
      if 0 >= push_info.activity then
      end
      if 0 >= push_info.champion then
        ext.localpush.clearEventWithIdentity(NotificationID.arena)
      end
      if 0 >= push_info.colony then
      end
      if 0 >= push_info.mine then
        ext.localpush.clearEventWithIdentity(NotificationID.mine)
      end
      if 0 >= push_info.building then
        ext.localpush.clearEventWithIdentity(NotificationID.building)
      end
      if 0 >= push_info.equipment then
        ext.localpush.clearEventWithIdentity(NotificationID.equipment)
      end
      if 0 >= push_info.revenue then
        ext.localpush.clearEventWithIdentity(NotificationID.collection)
      end
      if 0 >= push_info.alliance then
      end
    end
    GameSetting.SetPushSwitchStatus(push_info)
  end
end
function GamePushNotification.InitNotification()
  GamePushNotification.CheckExtLocalPushFunc()
  DebugOut("InitNotification begin")
  ext.localpush.cancelAll()
  GameGlobalData:RegisterDataChangeCallback("battle_supply", GamePushNotification.AddBattleSupplyNotification)
  GameGlobalData:RegisterDataChangeCallback("cdtimes", GamePushNotification.BaseSystemNotification)
  GamePushNotification.OfflineNotification()
  GameObjectMineMap.AddMineNotification = GamePushNotification.AddMineNotification
  GameUICollect.AddCollectionNotification = GamePushNotification.AddCollectionNotification
  GamePushNotification.RewardNotification()
  DebugOut("InitNotification end")
end
function GamePushNotification.CheckExtLocalPushFunc()
  if ext.localpush.add == nil then
    function ext.localpush.add(...)
      return
    end
  end
  if ext.localpush.clearEventWithIdentity == nil then
    function ext.localpush.clearEventWithIdentity(...)
      return
    end
  end
  if ext.localpush.cancelAll == nil then
    function ext.localpush.cancelAll(...)
      return
    end
  end
  if ext.localpush.setSwitch == nil then
    function ext.localpush.setSwitch(...)
      return
    end
  end
end
function GamePushNotification.AddMineNotification(endTime)
  local push_info = GameGlobalData:GetData("push_button_info")
  ext.localpush.clearEventWithIdentity(NotificationID.mine)
  if endTime > 0 and push_info and 0 < push_info.all and push_info.mine > 0 then
    DebugOut("AddMineNotification")
    ext.localpush.add("type1", endTime + os.time(), GameLoader:GetGameText("LC_MENU_gl_event_6_push"), NotificationID.mine)
  end
end
function GamePushNotification.AddArenaNotification(atkPoints)
  local push_info = GameGlobalData:GetData("push_button_info")
  ext.localpush.clearEventWithIdentity(NotificationID.arena)
  if atkPoints > 0 and push_info and 0 < push_info.all and 0 < push_info.champion then
    DebugOut("AddArenaNotification")
    ext.localpush.add("type1", atkPoints * 12 * 60 + os.time(), GameLoader:GetGameText("LC_MENU_gl_event_11_push"), NotificationID.arena)
  end
end
function GamePushNotification.AddBattleSupplyNotification()
  local battle_supply = GameGlobalData:GetData("battle_supply")
  ext.localpush.clearEventWithIdentity(NotificationID.battle_supply)
  if battle_supply and tonumber(battle_supply.current) < tonumber(battle_supply.max) then
    local tempAllTime_4 = os.time() + (tonumber(battle_supply.max) - tonumber(battle_supply.current)) * 30 * 60
    DebugOut("AddBattleSupplyNotification")
    ext.localpush.add("type1", tempAllTime_4, GameLoader:GetGameText("LC_HELP_ANDROID_LOCAL_PUSH_2"), NotificationID.battle_supply)
  end
end
function GamePushNotification.BaseSystemNotification()
  local buildingCDTimes = GameGlobalData:GetBuildingCDTimes()
  local buildingShortestTime = 0
  local push_info = GameGlobalData:GetData("push_button_info")
  if buildingCDTimes ~= nil then
    ext.localpush.clearEventWithIdentity(NotificationID.building)
    for key, value in pairs(buildingCDTimes) do
      buildingShortestTime = math.max(buildingShortestTime, value.left_time)
    end
  end
  if buildingShortestTime ~= 0 and push_info and 0 < push_info.all and 0 < push_info.building then
    DebugOut("buildingShortestTime")
    ext.localpush.add("type1", buildingShortestTime + os.time(), GameLoader:GetGameText("LC_MENU_gl_event_12_push"), NotificationID.building)
  end
  local enhanceCDTime = GameGlobalData:GetEnhanceCDTime()
  if enhanceCDTime ~= nil then
    ext.localpush.clearEventWithIdentity(NotificationID.equipment)
    if enhanceCDTime.left_time ~= 0 and push_info and 0 < push_info.all and 0 < push_info.equipment then
      DebugOut("enhanceCDTime")
      ext.localpush.add("type1", enhanceCDTime.left_time + os.time(), GameLoader:GetGameText("LC_MENU_gl_event_13_push"), NotificationID.equipment)
    end
  end
end
function GamePushNotification.AddCollectionNotification(allCDTimes)
  local collectionCDTime = GameGlobalData:GetCollectionCDTime()
  local push_info = GameGlobalData:GetData("push_button_info")
  ext.localpush.clearEventWithIdentity(NotificationID.collection)
  if allCDTimes > 0 and push_info and 0 < push_info.all and 0 < push_info.revenue then
    DebugOut("collectionCDTime")
    DebugOut(allCDTimes)
    ext.localpush.add("type1", allCDTimes + os.time(), GameLoader:GetGameText("LC_MENU_gl_event_14_push"), NotificationID.collection)
  end
end
function GamePushNotification.RewardNotification()
  local levelInfo = GameGlobalData:GetData("levelinfo")
  if levelInfo and levelInfo.level > 1 then
    return
  end
  local localtimeHour = tonumber(os.date("%H", os.time()))
  local localtimeMinute = tonumber(os.date("%M", os.time()))
  local loginRewardHour = 0
  local tempAllTime_3 = 0
  if GamePushNotification:IsChinese() then
    loginRewardHour = 24 - localtimeHour + 2
  else
    loginRewardHour = 24 - localtimeHour + 10
  end
  local tempLoginRewardTime = (loginRewardHour * 60 - localtimeMinute) * 60 + os.time()
  tempAllTime_3 = 0 + tempLoginRewardTime
  ext.localpush.add("type1", tempAllTime_3, GameLoader:GetGameText("LC_MENU_gl_event_2_push"))
  tempAllTime_3 = 86400 + tempLoginRewardTime
  ext.localpush.add("type1", tempAllTime_3, GameLoader:GetGameText("LC_MENU_gl_event_2_push"))
  tempAllTime_3 = 172800 + tempLoginRewardTime
  ext.localpush.add("type1", tempAllTime_3, GameLoader:GetGameText("LC_MENU_gl_event_2_push"))
  tempAllTime_3 = 259200 + tempLoginRewardTime
  ext.localpush.add("type1", tempAllTime_3, GameLoader:GetGameText("LC_MENU_gl_event_2_push"))
  tempAllTime_3 = 345600 + tempLoginRewardTime
  ext.localpush.add("type1", tempAllTime_3, GameLoader:GetGameText("LC_MENU_gl_event_2_push"))
  tempAllTime_3 = 432000 + tempLoginRewardTime
  ext.localpush.add("type1", tempAllTime_3, GameLoader:GetGameText("LC_MENU_gl_event_2_push"))
end
function GamePushNotification.WorldBossNotification()
end
function GamePushNotification:IsChinese()
  local isChinese = false
  if ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2.chinese" then
    isChinese = true
  elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android.platform.BD" then
    isChinese = true
  elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android.platform.qihoo360" then
    isChinese = true
  elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android.platform.uc" then
    isChinese = true
  elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android_local" then
    isChinese = true
  end
  return isChinese
end
function GamePushNotification.OfflineNotification()
  local pushSupport = IPlatformExt.getConfigValue("ExtDestIpAddrPool")
  if pushSupport and pushSupport == "False" then
    return
  end
  local levelInfo = GameGlobalData:GetData("levelinfo")
  if levelInfo and levelInfo.level > 1 then
    return
  end
  local localtimeHour = tonumber(os.date("%H", os.time()))
  local localtimeMinute = tonumber(os.date("%M", os.time()))
  local tempHour_2 = 0
  local tempAllTime_2 = 0
  if GamePushNotification:IsChinese() then
    DebugOut("GamePushNotification:IsChinese")
    tempHour_2 = 24 - localtimeHour + 4
  else
    DebugOut("GamePushNotification:Is not Chinese")
    tempHour_2 = 24 - localtimeHour + 24
  end
  tempAllTime_2 = (tempHour_2 * 60 - localtimeMinute) * 60 + os.time()
  ext.localpush.clearEventWithIdentity(NotificationID.offline)
  local tempAllTime_3 = 0
  tempAllTime_3 = 0 + tempAllTime_2
  ext.localpush.add("type1", tempAllTime_3, GameLoader:GetGameText("LC_MENU_gl_event_1_push"), NotificationID.offline)
  local tempAllTime_3 = 0
  tempAllTime_3 = 86400 + tempAllTime_2
  ext.localpush.add("type1", tempAllTime_3, GameLoader:GetGameText("LC_MENU_gl_event_1_push"), NotificationID.offline)
  local tempAllTime_3 = 0
  tempAllTime_3 = 172800 + tempAllTime_2
  ext.localpush.add("type1", tempAllTime_3, GameLoader:GetGameText("LC_MENU_gl_event_1_push"), NotificationID.offline)
  local tempAllTime_3 = 0
  tempAllTime_3 = 259200 + tempAllTime_2
  ext.localpush.add("type1", tempAllTime_3, GameLoader:GetGameText("LC_MENU_gl_event_1_push"), NotificationID.offline)
  local tempAllTime_3 = 0
  tempAllTime_3 = 345600 + tempAllTime_2
  ext.localpush.add("type1", tempAllTime_3, GameLoader:GetGameText("LC_MENU_gl_event_1_push"), NotificationID.offline)
  local tempAllTime_3 = 0
  tempAllTime_3 = 432000 + tempAllTime_2
  ext.localpush.add("type1", tempAllTime_3, GameLoader:GetGameText("LC_MENU_gl_event_1_push"), NotificationID.offline)
  if GamePushNotification:IsChinese() then
    tempHour_2 = 24 - localtimeHour + 10
    tempAllTime_2 = (tempHour_2 * 60 - localtimeMinute) * 60 + os.time()
    tempAllTime_3 = 0 + tempAllTime_2
    ext.localpush.add("type1", tempAllTime_3, GameLoader:GetGameText("LC_MENU_gl_event_1_push"), NotificationID.offline)
    tempAllTime_3 = 86400 + tempAllTime_2
    ext.localpush.add("type1", tempAllTime_3, GameLoader:GetGameText("LC_MENU_gl_event_1_push"), NotificationID.offline)
    tempAllTime_3 = 172800 + tempAllTime_2
    ext.localpush.add("type1", tempAllTime_3, GameLoader:GetGameText("LC_MENU_gl_event_1_push"), NotificationID.offline)
    tempAllTime_3 = 259200 + tempAllTime_2
    ext.localpush.add("type1", tempAllTime_3, GameLoader:GetGameText("LC_MENU_gl_event_1_push"), NotificationID.offline)
    tempAllTime_3 = 345600 + tempAllTime_2
    ext.localpush.add("type1", tempAllTime_3, GameLoader:GetGameText("LC_MENU_gl_event_1_push"), NotificationID.offline)
    tempAllTime_3 = 432000 + tempAllTime_2
    ext.localpush.add("type1", tempAllTime_3, GameLoader:GetGameText("LC_MENU_gl_event_1_push"), NotificationID.offline)
  end
end
