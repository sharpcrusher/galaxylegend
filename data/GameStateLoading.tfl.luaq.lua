local GameObjectTutorialCutscene = LuaObjectManager:GetLuaObject("GameObjectTutorialCutscene")
local GameObjectLevelUp = LuaObjectManager:GetLuaObject("GameObjectLevelUp")
local GameStateManager = GameStateManager
local GameStateLoading = GameStateManager.GameStateLoading
local NetMessageMgr = NetMessageMgr
local NetAPIList = NetAPIList
local GameGlobalData = GameGlobalData
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local AlertDataList = AlertDataList
local LuaObjectBase = LuaObjectManager.LuaObjectBase
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameMail = LuaObjectManager:GetLuaObject("GameMail")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameVip = LuaObjectManager:GetLuaObject("GameVip")
local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
local GameItemBag = LuaObjectManager:GetLuaObject("GameItemBag")
local TutorialQuestManager = TutorialQuestManager
local GameStateBattleMap = GameStateManager.GameStateBattleMap
local QuestTutorialCityHall = TutorialQuestManager.QuestTutorialCityHall
GameLoader = {}
realLuaLoader = LuaLoader:new()
function GameLoader:LoadGameText(lang, sheets)
  return ...
end
function GameLoader:GetGameText(string_id)
  local lang = string.upper(GameSetting:GetSavedLanguage())
  if Extra_text ~= nil and Extra_text[lang] ~= nil and Extra_text[lang][string_id] ~= nil then
    return Extra_text[lang][string_id]
  else
    return ...
  end
end
GameStateLoading.fetchDefaultChatInfo = false
ext.RegisteFlashClass("FlashLoading_t")
function FlashLoading_t:fscommand(cmd, arg)
  if cmd == "EndAgreement" then
    GameStateLoading.isShowAgreement = false
    if GameStateLoading.isNeedPrivacy and GameGlobalData.isNeedPrivacy ~= false then
      GameStateLoading.isShowAgreement = true
      GameStateLoading.isNeedPrivacy = false
      GameStateLoading.m_loadingBG:InvokeASCallback("_root", "showPrivacytWithBtn")
    end
  elseif cmd == "Continue" then
    DebugOut("nothing to do ")
  end
end
GameStateLoading.loginCallbackDataForQuickStartGame = nil
function GameStateLoading.loginCallbackForQuickStartGame(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.user_login_req.Code or msgType == NetAPIList.user_login_ack.Code then
    GameStateLoading.loginCallbackDataForQuickStartGame = {msgType = msgType, content = content}
    return true
  end
  return false
end
function GameStateLoading.loginCallback(msgType, content)
  DebugOut("GameStateLoading.loginCallback " .. msgType)
  DebugTable(content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.user_login_req.Code then
    DebugOut("GameStateLoading.loginCallback receive" .. msgType)
    assert(content.code ~= 0)
    local alertText = AlertDataList:GetTextFromErrorCode(content.code)
    local function callback()
      if IPlatformExt.getConfigValue("UseExtLogin") == "True" and content.code == 11 then
        GameUtils.PlatfromExtLogout_Standard()
      elseif AutoUpdate.isQuickStartGame then
        GameUtils:RestartGame(200, GameStateLoading.m_loadingBG)
      else
        local fbTokenFailed = false
        if 101016 == tonumber(content.code) then
          local LoginInfo = GameUtils:GetLoginInfo()
          if LoginInfo.AccountInfo and GameUtils:IsFacebookAccount(LoginInfo.AccountInfo) then
            fbTokenFailed = true
            LoginInfo.AccountInfo.password = ""
            GameUtils:AddAccountLocalData(LoginInfo.AccountInfo)
            GameUtils:RestartGame(200, GameStateLoading.m_loadingBG)
          end
        end
        if not fbTokenFailed then
          GameStateLoading.m_nextLoginTimer = 10000
        end
      end
    end
    GameUIGlobalScreen:ShowMessageBox(1, GameLoader:GetGameText("LC_MENU_NOTIFICATION_CHAR"), alertText, callback)
    return true
  end
  if msgType == NetAPIList.user_login_ack.Code then
    DebugOut("NetAPIList.user_login_ack.Code")
    if content.server_code and content.server_code == 4 then
      immanentversion170 = 4
    elseif content.server_code and content.server_code == 175 then
      immanentversion170 = 4
      immanentversion175 = 6
    end
    GameUtils:SavePhoneInfo()
    GameGlobalData:UpdateGameData("", content)
    GameGlobalData:RefreshData(nil)
    DebugOut("GameGlobalData.isLogin__=" .. debug.traceback())
    GameGlobalData.isLogin = true
    GameUtils.IPlatformExtLoginInfo_json = nil
    if g_SaveNewFteData then
      ext.sendNewFteData("FTE_LoginGSResultSuccessed")
    end
    NetMessageMgr:SendLoginLog()
    if IPlatformExt.isLeaderBoard_support() then
      local force_value = 0
      for _, fleet_cell in ipairs(GameGlobalData:GetData("matrix").cells) do
        if 0 < fleet_cell.fleet_identity then
          local fleet = GameGlobalData:GetFleetInfo(fleet_cell.fleet_identity)
          if fleet then
            force_value = force_value + fleet.force
          end
        end
      end
      local force_leaderboad_object = LeaderBoardPlatformExtMap.Force or {}
      local force_leaderboad_id = force_leaderboad_object[IPlatformExt.getConfigValue("PlatformName")] or "fuck_leaderboard_id_Force"
      DebugOut("force_value:" .. force_value)
      IPlatformExt.submit_LeaderBoardScore(force_leaderboad_id, force_value)
    end
    DebugOut("print GameGlobalData")
    DebugTable(GameGlobalData)
    GameUtils:ReqReDownloadRes()
    local intoBattle = false
    if GameStateManager:GetCurrentGameState() == GameStateManager.GameStateLoading then
      GameStateLoading.m_loadingBG = nil
      local progress = GameGlobalData:GetData("progress")
      DebugOut("\230\137\147\229\141\176progress ")
      DebugTable(progress)
      if immanentversion == 2 then
        if DebugConfig.isSkipFirstScene then
          if not QuestTutorialCityHall:IsFinished() and not QuestTutorialCityHall:IsActive() then
            QuestTutorialCityHall:SetActive(true)
          end
          GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
          GameStateManager.GameStateMainPlanet:CheckShowTutorial()
        elseif GameUtils:IsTutorialScene() then
          if DebugConfig.isDebugBattle then
            GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
          else
            GameStateBattleMap:EnterSection(60, 1, nil, true)
          end
        else
          GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
          GameStateManager.GameStateMainPlanet:CheckShowTutorial()
        end
      elseif immanentversion == 1 then
        if not QuestTutorialCityHall:IsFinished() and not QuestTutorialCityHall:IsActive() then
          QuestTutorialCityHall:SetActive(true)
        end
        local LoginInfo = GameUtils:GetLoginInfo()
        DebugOut("check fake battle index :", GameGlobalData:GetFakeBattleIndex(), LoginInfo.isCreateActor)
        if LoginInfo.isCreateActor and LoginInfo.isCreateActor == 0 or GameObjectTutorialCutscene:IsNeedShowNewFakeBattle() and GameGlobalData:GetFakeBattleIndex() and GameGlobalData:GetFakeBattleIndex() ~= 6 then
          intoBattle = true
        else
          GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
          GameStateManager.GameStateMainPlanet:CheckShowTutorial()
        end
      end
    end
    DeviceAutoConfig.SendDeviceInfo()
    NetMessageMgr:OnLoginFinish()
    GameVip:RequestCarrierProductList()
    GameVip:RequestProductIdentifierList()
    GameItemBag:RequestBag()
    TutorialQuestManager:Init()
    TutorialQuestManager:TutorialQuestsCheck()
    AutoUpdateInBackground:CheckUpdateFileList()
    local LoginInfo = GameUtils:GetLoginInfo()
    GameStateMainPlanet:CheckIfSendNormalStartCode()
    if intoBattle then
      GameStateLoading.m_loadingBG = nil
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateEmptyState)
      GameObjectTutorialCutscene:StartBattlePlay()
      AddFlurryEvent("StartCutScene", {}, 1)
      intoBattle = false
    else
      GameSettingData.LastLogin = LoginInfo
      GameSettingData.LastLoginSuccess = true
      GameSettingData.gameGateway = AutoUpdate.gameGateway
      GameUtils:SaveSettingData()
      Facebook:CheckAccountBind()
    end
    GameUtils:SafeRecordLoginInfo()
    DebugOut("hello world")
    for _, v in pairs(GameGlobalData.Purchase_tobeVerify) do
      StoreObject:verifyPurchase(v.tobeVerify_signedData, v.tobeVerify_signature, v.tobeVerify_pay_type)
    end
    GameGlobalData.Purchase_tobeVerify = {}
    if GL2_IS_ASK_FOR_ALL_STORE_LIST then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip.ForceUpdateTransactionStatus()
    end
    if IPlatformExt.getConfigValue("PlatformExtCharge") == "true" then
      IPlatformExt.RegCallBackFunc(2, GameVip.VerifyPurchase_forPlatformExt)
    end
    DebugOut("hello world22222")
    if IPlatformExt.getConfigValue("PlatformExtCharge.Finish") == "true" then
      IPlatformExt.RequestUnfinishedCharge()
    end
    local addtion_content = {
      locale = GameSettingData.Save_Lang
    }
    NetMessageMgr:SendMsg(NetAPIList.game_addtion_req.Code, addtion_content, nil, false, nil)
    return true
  end
  return false
end
function GameStateLoading.VersionCheckCallback(msgType, content)
  DebugOut("VersionCheckCallback", msgType, content.api, content.code)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.ver_check_req.Code then
    if content.code == 0 then
      DebugOut("GameStateLoading:VersionCheck is updated to data")
    else
      local LoginInfo = GameUtils:GetLoginInfo()
      local serverName = " " .. LoginInfo.ServerInfo.name or LoginInfo.ServerInfo.logic_id or ""
      local text = GameLoader:GetGameText("LC_ALERT_client_version_up") .. serverName
      GameUIGlobalScreen:ShowMessageBox(1, GameLoader:GetGameText("LC_MENU_NOTIFICATION_CHAR"), text, function()
        GameUtils:RestartGame(200)
      end)
    end
    return true
  end
  return false
end
function GameStateLoading:CheckNewVersion()
  DebugOut("CheckNewVersion")
  if not AutoUpdate.isDeveloper then
    local payload = {
      market = ext.GetBundleIdentifier(),
      version = AutoUpdate.localVersion
    }
    DebugTable(payload)
    NetMessageMgr:SendMsg(NetAPIList.ver_check_req.Code, payload, GameStateLoading.VersionCheckCallback, nil)
  end
end
function GameStateLoading.LoadMail()
  ext.dofile("MAIL.tfl")
  if GameUserMailData == nil then
    GameUserMailData = {RECORD_VERSION = "1.0"}
  end
  GameUserMailData = GameMail.parseSaveData()
  GameMail.executeSave()
end
function GameStateLoading.LoadSetting()
  GameUtils:CheckUpdateSettingData()
end
function GameStateLoading:InitGameState()
end
function GameStateLoading:IsShowText()
  GameUtils:printByAndroid("---IsShowText ---- GetBundleIdentifier----" .. ext.GetBundleIdentifier())
  DebugOut("GameStateLoading:---IsShowText ---- GetBundleIdentifier--" .. ext.GetBundleIdentifier())
  self.m_loadingBG:InvokeASCallback("_root", "showtext", false)
  if GameUtils:IsChinese() then
    self.m_loadingBG:InvokeASCallback("_root", "showtext", true)
  end
end
function GameStateLoading:OnFocusGain(previousState)
  GameStateLoading:_InitLoadActions()
  GameStateLoading:Reset()
  GameStateLoading.m_nextLoginTimer = 0
  DebugOut("GameStateLoading:OnFocusGain")
  self.m_loadingBG = FlashLoading_t:new()
  if ext.is2x1 and ext.is2x1() and not AutoUpdate.isAndroidDevice then
    local width = ext.SCREEN_WIDTH
    local height = ext.SCREEN_HEIGHT
    local x_start = math.floor(width * 92 / 2436)
    local usewidth = math.floor(width * 2252 / 2436)
    self.m_loadingBG:Load("data2/opening.tfs", ext.isLangFR(), x_start, 0, usewidth, height)
  else
    self.m_loadingBG:Load("data2/opening.tfs")
  end
  self.m_loadingBG:InvokeASCallback("_root", "stopLoadingTextAnim")
  GameStateLoading:IsShowText()
  math.randomseed(os.time())
  local randomIndex = math.random(1, 10)
  local textID = "LC_MENU_LOADING_HINT_" .. randomIndex
  local tip = GameLoader:GetGameText(textID)
  local lang = "en"
  if GameSettingData and GameSettingData.Save_Lang then
    lang = GameSettingData.Save_Lang
    if string.find(lang, "ru") == 1 then
      lang = "ru"
    end
  end
  self.m_loadingBG:InvokeASCallback("_root", "setTip", tip)
  self.m_loadingBG:InvokeASCallback("_root", "setPrototext", GameLoader:GetGameText("LC_MENU_USER_PROTOCOL_CHAR"))
  self.m_loadingBG:InvokeASCallback("_root", "setPrivacytext", GameLoader:GetGameText("LC_MENU_PRIVACY_POLICY_CHAR"))
  self.m_loadingBG:InvokeASCallback("_root", "setBgFrame", lang)
  self.m_loadingBG:InvokeASCallback("_root", "setShowPrivacyBtn", false)
  local GameAccountSelector = LuaObjectManager:GetLuaObject("GameAccountSelector")
  if GameAccountSelector.LoginTable and #GameAccountSelector.LoginTable == 0 then
    self.m_loadingBG:InvokeASCallback("_root", "showAgreementWithBtn")
    GameStateLoading.isShowAgreement = true
    GameStateLoading.isNeedPrivacy = true
  end
  if not AutoUpdate.isWin32 then
    Facebook:LoginWithNoUI()
  end
end
function GameStateLoading:OnFocusLost(newState)
  DebugOut("GameStateLoading:FocusLost")
  self.m_loadingBG = nil
  collectgarbage("collect")
end
function GameStateLoading:Update(dt)
  if self.m_loadingBG then
    self.m_loadingBG:Update(dt)
    self.m_loadingBG:InvokeASCallback("_root", "AgreementUpdate")
  end
  self:_UpdateLoadAction()
  if self:IsLoadFinished() and not GameStateLoading.isShowAgreement then
    local LoginIngo = GameUtils:GetLoginInfo()
    if LoginIngo.PlayerInfo then
      if self.m_nextLoginTimer ~= -1 then
        self.m_nextLoginTimer = self.m_nextLoginTimer - dt
        if self.m_nextLoginTimer <= 0 then
          self:CheckNewVersion()
          self:Login()
          self.m_nextLoginTimer = -1
        end
      end
    else
      local login_info = GameUtils:GetLoginInfo()
      if login_info and not GameStateLoading.hasCreateRole then
        local server_info = login_info.ServerInfo
        if not server_info.is_in_maintain then
          DebugOut("[udid] Loading\229\174\140\230\136\144")
          NetMessageMgr:SendUdidStepReq(2)
          GameStateLoading.hasCreateRole = true
          if immanentversion == 1 then
            self:CreateRole()
            if GameUtils:IsAppVersionSmallerthan(1, 9, 6) then
              GameStateManager:SetCurrentGameState(GameStateManager.GameStateEmptyState)
              GameObjectTutorialCutscene:StartBattlePlay()
              self.m_loadingBG = nil
              return nil
            else
              GameTimer:Add(function()
                if GameGlobalData.needFPSFte ~= nil then
                  GameStateManager:SetCurrentGameState(GameStateManager.GameStateEmptyState)
                  GameObjectTutorialCutscene:StartBattlePlay()
                  self.m_loadingBG = nil
                  return nil
                else
                  return 10
                end
              end, 10)
            end
          elseif immanentversion == 2 then
            GameObjectTutorialCutscene:ShowTutorial("part1")
            self.m_loadingBG = nil
          end
          ext.audio.stopBackgroundMusic()
          AddFlurryEvent("StartCutScene", {}, 1)
        end
      end
    end
    if AutoUpdate.isQuickStartGame and GameStateLoading.loginCallbackDataForQuickStartGame then
      local loginData = GameStateLoading.loginCallbackDataForQuickStartGame
      GameStateLoading.loginCallback(loginData.msgType, loginData.content)
      GameStateLoading.loginCallbackDataForQuickStartGame = nil
    end
  end
  GameStateBase.Update(self, dt)
end
function GameStateLoading.CreateRoleSuccess(msgType, content)
  DebugOut("CreateRoleSuccess NetAPIList.common_ack.Code=" .. tostring(NetAPIList.common_ack.Code))
  DebugTable(content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.user_register_req.Code then
    GameUIGlobalScreen:ShowAlert2(content)
    return true
  elseif msgType == NetAPIList.user_login_ack.Code then
    GameUtils:SavePhoneInfo()
    GameGlobalData.isLogin = true
    DebugOut("GameGlobalData.isLogin__=" .. debug.traceback())
    NetMessageMgr:SendLoginLog()
    if content.server_code and content.server_code == 4 then
      immanentversion170 = 4
    elseif content.server_code and content.server_code == 175 then
      immanentversion170 = 4
      immanentversion175 = 6
    end
    GameGlobalData:UpdateGameData("", content)
    local LoginInfo = GameUtils:GetLoginInfo()
    LoginInfo.PlayerInfo = {}
    LoginInfo.PlayerInfo.name = content.userinfo.name
    GameSettingData = GameSettingData or {}
    GameSettingData.RECORD_VERSION = GameGlobalData.RECORD_VERSION
    GameSettingData.LastLogin = LoginInfo
    GameSettingData.LastLoginSuccess = true
    GameSettingData.gameGateway = AutoUpdate.gameGateway
    GameUtils:SaveSettingData()
    GameStateMainPlanet:CheckIfSendNormalStartCode()
    GameUtils.IPlatformExtLoginInfo_json = nil
    GameUtils:ReqReDownloadRes()
    GameUtils:SafeRecordLoginInfo()
    local createPlayerFinish = {}
    createPlayerFinish.Type = "CreatePlayerSuccess"
    createPlayerFinish.Description = "CreateNewPlayer"
    createPlayerFinish.PlayerSex = content.userinfo.sex == 1 and "male" or "female"
    createPlayerFinish.PlayerName = content.userinfo.name or "unknow"
    createPlayerFinish.PlayerServer = LoginInfo and LoginInfo.ServerInfo and LoginInfo.ServerInfo.name or "unknow"
    GameUtils:SafeRecordGameInfo(createPlayerFinish)
    return true
  end
  return false
end
function GameStateLoading:CreateRole()
  local LoginInfo = GameUtils:GetLoginInfo()
  local passport_info = {}
  passport_info.passport = ""
  passport_info.password = ""
  if LoginInfo.AccountInfo then
    passport_info.passport = LoginInfo.AccountInfo.passport or ""
    passport_info.password = LoginInfo.AccountInfo.password or ""
  end
  if IPlatformExt.getConfigValue("UseExtLogin") == "True" then
    passport_info.acc_type = tonumber(IPlatformExt.getConfigValue("AccType"))
    passport_info.password = GameUtils.IPlatformExtLoginInfo_json
  elseif GameUtils:IsFacebookAccount(LoginInfo.AccountInfo) then
    passport_info.acc_type = GameUtils.AccountType.facebook
  elseif GameUtils:IsLoginTangoAccount() then
    passport_info.acc_type = GameUtils.AccountType.tango
  elseif GameUtils:IsMailAccount(LoginInfo.AccountInfo) then
    passport_info.acc_type = GameUtils.AccountType.mail
  elseif GameUtils:IsQihooAccount(LoginInfo.AccountInfo) then
    passport_info.acc_type = GameUtils.AccountType.qihoo
  else
    passport_info.acc_type = GameUtils.AccountType.guest
  end
  passport_info.udid = ext.GetIOSOpenUdid()
  passport_info.mac_addr = ext.GetIOSMacAddress() or ""
  passport_info.open_udid = ext.GetIOSOpenUdid()
  DebugTutorial("udid", ext.GetIOSOpenUdid())
  passport_info.locale = ext.GetDeviceLanguage()
  passport_info.game_id = GameUtils:GetLogSeverLogicID()
  passport_info.region_name = LoginInfo.ServerInfo.name
  passport_info.idfa = GameUtils:GetDeviceIDFA()
  if not passport_info.idfa then
    passport_info.idfa = ""
  end
  local clientInfo = {}
  clientInfo.market = ext.GetChannel()
  clientInfo.terminal = ext.GetDeviceName()
  DebugOut("clientInfo.terminal = ", clientInfo.terminal)
  clientInfo.app_id = ext.GetBundleIdentifier()
  clientInfo.os_version = ext.GetOSVersion()
  clientInfo.device_name = ext.GetDeviceFullName()
  clientInfo.client_version_svn = tostring(AutoUpdate.localVersion) or ""
  clientInfo.client_version_main = ext.GetAppVersion()
  local ret1 = 0
  local ret2 = ""
  if ext.GetDeviceInfo then
    ret1, ret2 = ext.GetDeviceInfo()
  end
  clientInfo.google_aid = ret2
  clientInfo.device_id = ret2
  clientInfo.device_id_type = ret1
  local defaultName = "Player" .. math.random(10000, 99999)
  DebugOut("defaultName = ", defaultName)
  local registerInfo = {
    passport_info = passport_info,
    client_info = clientInfo,
    name = defaultName,
    sex = 1,
    invitecode = "fuckInviteCode"
  }
  NetMessageMgr:SendMsg(NetAPIList.user_register_req.Code, registerInfo, self.CreateRoleSuccess, false, nil)
end
function GameStateLoading:Render()
  if self.m_loadingBG ~= nil then
    self.m_loadingBG:Render()
  end
  GameStateBase.Render(self)
end
function GameStateLoading:OnTouchPressed(x, y)
  GameStateBase.OnTouchPressed(self, x, y)
  if self.m_loadingBG ~= nil then
    self.m_loadingBG:OnTouchPressed(x, y)
  end
end
function GameStateLoading:OnTouchMoved(x, y)
  GameStateBase.OnTouchMoved(self, x, y)
  if self.m_loadingBG ~= nil then
    self.m_loadingBG:OnTouchMoved(x, y)
  end
end
function GameStateLoading:OnTouchReleased(x, y)
  GameStateBase.OnTouchReleased(self, x, y)
  if self.m_loadingBG ~= nil then
    self.m_loadingBG:OnTouchReleased(x, y)
  end
end
function GameStateLoading:_InitLoadActions()
  self:RegisterLoadAction(self.LoadMail, 0)
  if not GameUtils:GetLoginInfo().PlayerInfo or GameUtils:GetLoginInfo().PlayerInfo and GameUtils:GetLoginInfo().isCreateActor and GameUtils:GetLoginInfo().isCreateActor == 0 then
    self:RegisterLoadAction(function()
      GameObjectTutorialCutscene:LoadFlashObject()
    end, 2)
  else
    for _, v in pairs(LuaObjectManager) do
      if type(v) == "table" and v.m_alwaysload and v.m_flashFileName then
        DebugOut("LuaObjectManager m_alwaysload m_objectName m_flashFileName " .. v.m_objectName .. " " .. v.m_flashFileName)
        self:RegisterLoadAction(function()
          v:LoadFlashObject()
        end, 1)
      end
    end
  end
end
function GameStateLoading:RegisterLoadAction(loadFunc, weight)
  local obj_load = {}
  obj_load.loadFunc = loadFunc
  obj_load.weight = weight
  if not self.allLoadActions then
    self.allLoadActions = {}
  end
  table.insert(self.allLoadActions, obj_load)
end
function GameStateLoading:Login(isQuickStartGame)
  DebugOut("GameStateLoading:Login--")
  if not isQuickStartGame and not self:IsLoadFinished() then
    DebugOut("not load finish , ingore login")
    return
  end
  local LoginInfo = GameUtils:GetLoginInfo()
  local last_login_user = LoginInfo.PlayerInfo
  if last_login_user then
    local passport_info = {}
    DebugOut("last_login_user")
    DebugTable(LoginInfo)
    if LoginInfo.AccountInfo and LoginInfo.AccountInfo.accType ~= GameUtils.AccountType.guest then
      passport_info.passport = LoginInfo.AccountInfo.passport
      if GameUtils:IsFacebookAccount(LoginInfo.AccountInfo) or GameUtils:IsLoginTangoAccount() or GameUtils:IsQihooAccount(LoginInfo.AccountInfo) then
        passport_info.password = LoginInfo.AccountInfo.password
      else
        passport_info.password = ext.ENC1(LoginInfo.AccountInfo.password)
      end
    else
      passport_info.passport = ""
      passport_info.password = ""
    end
    if IPlatformExt.getConfigValue("UseExtLogin") == "True" then
      passport_info.acc_type = tonumber(IPlatformExt.getConfigValue("AccType"))
      passport_info.password = GameUtils.IPlatformExtLoginInfo_json or GameUtils.IPlatformExtLastLoginInfo
    elseif GameUtils:IsFacebookAccount(LoginInfo.AccountInfo) then
      passport_info.acc_type = GameUtils.AccountType.facebook
    elseif GameUtils:IsMailAccount(LoginInfo.AccountInfo) then
      passport_info.acc_type = GameUtils.AccountType.mail
    elseif GameUtils:IsLoginTangoAccount() then
      passport_info.acc_type = GameUtils.AccountType.tango
    elseif GameUtils:IsQihooAccount(LoginInfo.AccountInfo) then
      passport_info.acc_type = GameUtils.AccountType.qihoo
    else
      passport_info.acc_type = GameUtils.AccountType.guest
    end
    passport_info.udid = ext.GetIOSOpenUdid()
    passport_info.mac_addr = ext.GetIOSMacAddress() or ""
    passport_info.open_udid = ext.GetIOSOpenUdid()
    passport_info.locale = ext.GetDeviceLanguage()
    passport_info.game_id = GameUtils:GetLogSeverLogicID()
    passport_info.region_name = LoginInfo.ServerInfo.name
    passport_info.idfa = GameUtils:GetDeviceIDFA()
    if not passport_info.idfa then
      passport_info.idfa = ""
    end
    local clientInfo = {}
    clientInfo.market = ext.GetChannel()
    clientInfo.terminal = ext.GetDeviceName()
    clientInfo.app_id = ext.GetBundleIdentifier()
    clientInfo.os_version = ext.GetOSVersion()
    clientInfo.device_name = ext.GetDeviceFullName()
    clientInfo.client_version_svn = tostring(AutoUpdate.localVersion) or ""
    clientInfo.client_version_main = ext.GetAppVersion()
    local ret1 = 0
    local ret2 = ""
    if ext.GetDeviceInfo then
      ret1, ret2 = ext.GetDeviceInfo()
    end
    clientInfo.google_aid = ret2
    clientInfo.device_id = ret2
    clientInfo.device_id_type = ret1
    local loginInfo = {
      passport_info = passport_info,
      client_info = clientInfo,
      name = last_login_user.name
    }
    DebugOutPutTable(loginInfo, "loginInfo")
    ext.trackLogin(last_login_user.name, LoginInfo.ServerInfo.name)
    GameStateLoading.loginStartTime = ext.getSysTime()
    local callback = self.loginCallback
    if isQuickStartGame then
      GameStateLoading.loginCallbackDataForQuickStartGame = nil
      callback = GameStateLoading.loginCallbackForQuickStartGame
    end
    if g_SaveNewFteData then
      ext.sendNewFteData("FTE_LoginGSStarted")
    end
    NetMessageMgr:SendMsg(NetAPIList.user_login_req.Code, loginInfo, callback, GameStateManager:GetCurrentGameState() ~= GameStateManager.GameStateLoading, nil)
    self.m_nextLoginTimer = -1
    local function CurmatrixIndexReqCallback(msgType, content)
      if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.mulmatrix_get_req.Code then
        if content.code ~= 0 then
          GameUIGlobalScreen:ShowAlert("error", content.code, nil)
        end
        return true
      elseif msgType == NetAPIList.mulmatrix_get_ack.Code then
        GameGlobalData.GlobalData.curMatrixIndex = content.cur_index
        return true
      end
      return false
    end
    NetMessageMgr:SendMsg(NetAPIList.mulmatrix_get_req.Code, {type = 1}, CurmatrixIndexReqCallback, false, nil)
    DebugOut("GameSetting.GameSettingData.Save_Lang", GameSettingData.Save_Lang)
    if GameSettingData ~= nil then
      if GameSettingData.Save_Lang == nil then
        GameSettingData.Save_Lang = "en"
      end
      NetMessageMgr:SendMsg(NetAPIList.set_game_local_req.Code, {
        game_local = GameSettingData.Save_Lang
      }, nil, false, nil)
    end
  end
  if GameStateLoading.fetchDefaultChatInfo == false then
    DebugOut("fetchDefaultChatInfo")
    NetMessageMgr:SendMsg(NetAPIList.chat_history_async_req.Code, nil, nil, false, nil)
    GameStateLoading.fetchDefaultChatInfo = true
  end
  NetMessageMgr:SendMsg(NetAPIList.event_ntf_req.Code, nil, nil, false)
end
function GameStateLoading:IsLoadFinished()
  if not self.allLoadActions then
    return false
  else
    return self.step_current > #self.allLoadActions
  end
end
function GameStateLoading:Reset()
  self.step_current = 0
  self.weight_current = 0
  self.weight_total = 0
  for i, v in ipairs(self.allLoadActions) do
    self.weight_total = self.weight_total + v.weight
  end
end
function GameStateLoading:_UpdateLoadAction()
  self.step_current = self.step_current + 1
  local curload = self.allLoadActions[self.step_current]
  if curload ~= nil then
    curload.loadFunc()
    self.weight_current = self.weight_current + curload.weight
    self.m_loadingBG:InvokeASCallback("_root", "luafs_SetLoadingPercent", self.weight_current / self.weight_total)
  end
end
