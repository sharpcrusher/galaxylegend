local growing = GameData.medal_enhance.growing
table.insert(growing, {
  id = 1,
  type = 100001,
  level = 1,
  exp_origin = 0,
  exp_target = 80
})
table.insert(growing, {
  id = 2,
  type = 100001,
  level = 2,
  exp_origin = 80,
  exp_target = 200
})
table.insert(growing, {
  id = 3,
  type = 100001,
  level = 3,
  exp_origin = 200,
  exp_target = 360
})
table.insert(growing, {
  id = 4,
  type = 100001,
  level = 4,
  exp_origin = 360,
  exp_target = 550
})
table.insert(growing, {
  id = 5,
  type = 100001,
  level = 5,
  exp_origin = 550,
  exp_target = 780
})
table.insert(growing, {
  id = 6,
  type = 100001,
  level = 6,
  exp_origin = 780,
  exp_target = 1050
})
table.insert(growing, {
  id = 7,
  type = 100001,
  level = 7,
  exp_origin = 1050,
  exp_target = 1360
})
table.insert(growing, {
  id = 8,
  type = 100001,
  level = 8,
  exp_origin = 1360,
  exp_target = 1710
})
table.insert(growing, {
  id = 9,
  type = 100001,
  level = 9,
  exp_origin = 1710,
  exp_target = 2100
})
table.insert(growing, {
  id = 10,
  type = 100001,
  level = 10,
  exp_origin = 2100,
  exp_target = 2530
})
table.insert(growing, {
  id = 11,
  type = 100001,
  level = 11,
  exp_origin = 2530,
  exp_target = 3000
})
table.insert(growing, {
  id = 12,
  type = 100001,
  level = 12,
  exp_origin = 3000,
  exp_target = 3500
})
table.insert(growing, {
  id = 13,
  type = 100001,
  level = 13,
  exp_origin = 3500,
  exp_target = 4040
})
table.insert(growing, {
  id = 14,
  type = 100001,
  level = 14,
  exp_origin = 4040,
  exp_target = 4620
})
table.insert(growing, {
  id = 15,
  type = 100001,
  level = 15,
  exp_origin = 4620,
  exp_target = 5240
})
table.insert(growing, {
  id = 16,
  type = 100001,
  level = 16,
  exp_origin = 5240,
  exp_target = 5900
})
table.insert(growing, {
  id = 17,
  type = 100001,
  level = 17,
  exp_origin = 5900,
  exp_target = 6600
})
table.insert(growing, {
  id = 18,
  type = 100001,
  level = 18,
  exp_origin = 6600,
  exp_target = 7340
})
table.insert(growing, {
  id = 19,
  type = 100001,
  level = 19,
  exp_origin = 7340,
  exp_target = 8120
})
table.insert(growing, {
  id = 20,
  type = 100001,
  level = 20,
  exp_origin = 8120,
  exp_target = 8930
})
table.insert(growing, {
  id = 21,
  type = 100001,
  level = 21,
  exp_origin = 8930,
  exp_target = 9780
})
table.insert(growing, {
  id = 22,
  type = 100001,
  level = 22,
  exp_origin = 9780,
  exp_target = 10670
})
table.insert(growing, {
  id = 23,
  type = 100001,
  level = 23,
  exp_origin = 10670,
  exp_target = 11600
})
table.insert(growing, {
  id = 24,
  type = 100001,
  level = 24,
  exp_origin = 11600,
  exp_target = 12570
})
table.insert(growing, {
  id = 25,
  type = 100001,
  level = 25,
  exp_origin = 12570,
  exp_target = 13580
})
table.insert(growing, {
  id = 26,
  type = 100001,
  level = 26,
  exp_origin = 13580,
  exp_target = 14630
})
table.insert(growing, {
  id = 27,
  type = 100001,
  level = 27,
  exp_origin = 14630,
  exp_target = 15720
})
table.insert(growing, {
  id = 28,
  type = 100001,
  level = 28,
  exp_origin = 15720,
  exp_target = 16850
})
table.insert(growing, {
  id = 29,
  type = 100001,
  level = 29,
  exp_origin = 16850,
  exp_target = 18010
})
table.insert(growing, {
  id = 30,
  type = 100001,
  level = 30,
  exp_origin = 18010,
  exp_target = 18010
})
table.insert(growing, {
  id = 31,
  type = 100002,
  level = 1,
  exp_origin = 0,
  exp_target = 80
})
table.insert(growing, {
  id = 32,
  type = 100002,
  level = 2,
  exp_origin = 80,
  exp_target = 200
})
table.insert(growing, {
  id = 33,
  type = 100002,
  level = 3,
  exp_origin = 200,
  exp_target = 360
})
table.insert(growing, {
  id = 34,
  type = 100002,
  level = 4,
  exp_origin = 360,
  exp_target = 550
})
table.insert(growing, {
  id = 35,
  type = 100002,
  level = 5,
  exp_origin = 550,
  exp_target = 780
})
table.insert(growing, {
  id = 36,
  type = 100002,
  level = 6,
  exp_origin = 780,
  exp_target = 1050
})
table.insert(growing, {
  id = 37,
  type = 100002,
  level = 7,
  exp_origin = 1050,
  exp_target = 1360
})
table.insert(growing, {
  id = 38,
  type = 100002,
  level = 8,
  exp_origin = 1360,
  exp_target = 1710
})
table.insert(growing, {
  id = 39,
  type = 100002,
  level = 9,
  exp_origin = 1710,
  exp_target = 2100
})
table.insert(growing, {
  id = 40,
  type = 100002,
  level = 10,
  exp_origin = 2100,
  exp_target = 2530
})
table.insert(growing, {
  id = 41,
  type = 100002,
  level = 11,
  exp_origin = 2530,
  exp_target = 3000
})
table.insert(growing, {
  id = 42,
  type = 100002,
  level = 12,
  exp_origin = 3000,
  exp_target = 3500
})
table.insert(growing, {
  id = 43,
  type = 100002,
  level = 13,
  exp_origin = 3500,
  exp_target = 4040
})
table.insert(growing, {
  id = 44,
  type = 100002,
  level = 14,
  exp_origin = 4040,
  exp_target = 4620
})
table.insert(growing, {
  id = 45,
  type = 100002,
  level = 15,
  exp_origin = 4620,
  exp_target = 5240
})
table.insert(growing, {
  id = 46,
  type = 100002,
  level = 16,
  exp_origin = 5240,
  exp_target = 5900
})
table.insert(growing, {
  id = 47,
  type = 100002,
  level = 17,
  exp_origin = 5900,
  exp_target = 6600
})
table.insert(growing, {
  id = 48,
  type = 100002,
  level = 18,
  exp_origin = 6600,
  exp_target = 7340
})
table.insert(growing, {
  id = 49,
  type = 100002,
  level = 19,
  exp_origin = 7340,
  exp_target = 8120
})
table.insert(growing, {
  id = 50,
  type = 100002,
  level = 20,
  exp_origin = 8120,
  exp_target = 8930
})
table.insert(growing, {
  id = 51,
  type = 100002,
  level = 21,
  exp_origin = 8930,
  exp_target = 9780
})
table.insert(growing, {
  id = 52,
  type = 100002,
  level = 22,
  exp_origin = 9780,
  exp_target = 10670
})
table.insert(growing, {
  id = 53,
  type = 100002,
  level = 23,
  exp_origin = 10670,
  exp_target = 11600
})
table.insert(growing, {
  id = 54,
  type = 100002,
  level = 24,
  exp_origin = 11600,
  exp_target = 12570
})
table.insert(growing, {
  id = 55,
  type = 100002,
  level = 25,
  exp_origin = 12570,
  exp_target = 13580
})
table.insert(growing, {
  id = 56,
  type = 100002,
  level = 26,
  exp_origin = 13580,
  exp_target = 14630
})
table.insert(growing, {
  id = 57,
  type = 100002,
  level = 27,
  exp_origin = 14630,
  exp_target = 15720
})
table.insert(growing, {
  id = 58,
  type = 100002,
  level = 28,
  exp_origin = 15720,
  exp_target = 16850
})
table.insert(growing, {
  id = 59,
  type = 100002,
  level = 29,
  exp_origin = 16850,
  exp_target = 18010
})
table.insert(growing, {
  id = 60,
  type = 100002,
  level = 30,
  exp_origin = 18010,
  exp_target = 18010
})
table.insert(growing, {
  id = 61,
  type = 100003,
  level = 1,
  exp_origin = 0,
  exp_target = 80
})
table.insert(growing, {
  id = 62,
  type = 100003,
  level = 2,
  exp_origin = 80,
  exp_target = 200
})
table.insert(growing, {
  id = 63,
  type = 100003,
  level = 3,
  exp_origin = 200,
  exp_target = 360
})
table.insert(growing, {
  id = 64,
  type = 100003,
  level = 4,
  exp_origin = 360,
  exp_target = 550
})
table.insert(growing, {
  id = 65,
  type = 100003,
  level = 5,
  exp_origin = 550,
  exp_target = 780
})
table.insert(growing, {
  id = 66,
  type = 100003,
  level = 6,
  exp_origin = 780,
  exp_target = 1050
})
table.insert(growing, {
  id = 67,
  type = 100003,
  level = 7,
  exp_origin = 1050,
  exp_target = 1360
})
table.insert(growing, {
  id = 68,
  type = 100003,
  level = 8,
  exp_origin = 1360,
  exp_target = 1710
})
table.insert(growing, {
  id = 69,
  type = 100003,
  level = 9,
  exp_origin = 1710,
  exp_target = 2100
})
table.insert(growing, {
  id = 70,
  type = 100003,
  level = 10,
  exp_origin = 2100,
  exp_target = 2530
})
table.insert(growing, {
  id = 71,
  type = 100003,
  level = 11,
  exp_origin = 2530,
  exp_target = 3000
})
table.insert(growing, {
  id = 72,
  type = 100003,
  level = 12,
  exp_origin = 3000,
  exp_target = 3500
})
table.insert(growing, {
  id = 73,
  type = 100003,
  level = 13,
  exp_origin = 3500,
  exp_target = 4040
})
table.insert(growing, {
  id = 74,
  type = 100003,
  level = 14,
  exp_origin = 4040,
  exp_target = 4620
})
table.insert(growing, {
  id = 75,
  type = 100003,
  level = 15,
  exp_origin = 4620,
  exp_target = 5240
})
table.insert(growing, {
  id = 76,
  type = 100003,
  level = 16,
  exp_origin = 5240,
  exp_target = 5900
})
table.insert(growing, {
  id = 77,
  type = 100003,
  level = 17,
  exp_origin = 5900,
  exp_target = 6600
})
table.insert(growing, {
  id = 78,
  type = 100003,
  level = 18,
  exp_origin = 6600,
  exp_target = 7340
})
table.insert(growing, {
  id = 79,
  type = 100003,
  level = 19,
  exp_origin = 7340,
  exp_target = 8120
})
table.insert(growing, {
  id = 80,
  type = 100003,
  level = 20,
  exp_origin = 8120,
  exp_target = 8930
})
table.insert(growing, {
  id = 81,
  type = 100003,
  level = 21,
  exp_origin = 8930,
  exp_target = 9780
})
table.insert(growing, {
  id = 82,
  type = 100003,
  level = 22,
  exp_origin = 9780,
  exp_target = 10670
})
table.insert(growing, {
  id = 83,
  type = 100003,
  level = 23,
  exp_origin = 10670,
  exp_target = 11600
})
table.insert(growing, {
  id = 84,
  type = 100003,
  level = 24,
  exp_origin = 11600,
  exp_target = 12570
})
table.insert(growing, {
  id = 85,
  type = 100003,
  level = 25,
  exp_origin = 12570,
  exp_target = 13580
})
table.insert(growing, {
  id = 86,
  type = 100003,
  level = 26,
  exp_origin = 13580,
  exp_target = 14630
})
table.insert(growing, {
  id = 87,
  type = 100003,
  level = 27,
  exp_origin = 14630,
  exp_target = 15720
})
table.insert(growing, {
  id = 88,
  type = 100003,
  level = 28,
  exp_origin = 15720,
  exp_target = 16850
})
table.insert(growing, {
  id = 89,
  type = 100003,
  level = 29,
  exp_origin = 16850,
  exp_target = 18010
})
table.insert(growing, {
  id = 90,
  type = 100003,
  level = 30,
  exp_origin = 18010,
  exp_target = 18010
})
table.insert(growing, {
  id = 91,
  type = 100004,
  level = 1,
  exp_origin = 0,
  exp_target = 80
})
table.insert(growing, {
  id = 92,
  type = 100004,
  level = 2,
  exp_origin = 80,
  exp_target = 200
})
table.insert(growing, {
  id = 93,
  type = 100004,
  level = 3,
  exp_origin = 200,
  exp_target = 360
})
table.insert(growing, {
  id = 94,
  type = 100004,
  level = 4,
  exp_origin = 360,
  exp_target = 550
})
table.insert(growing, {
  id = 95,
  type = 100004,
  level = 5,
  exp_origin = 550,
  exp_target = 780
})
table.insert(growing, {
  id = 96,
  type = 100004,
  level = 6,
  exp_origin = 780,
  exp_target = 1050
})
table.insert(growing, {
  id = 97,
  type = 100004,
  level = 7,
  exp_origin = 1050,
  exp_target = 1360
})
table.insert(growing, {
  id = 98,
  type = 100004,
  level = 8,
  exp_origin = 1360,
  exp_target = 1710
})
table.insert(growing, {
  id = 99,
  type = 100004,
  level = 9,
  exp_origin = 1710,
  exp_target = 2100
})
table.insert(growing, {
  id = 100,
  type = 100004,
  level = 10,
  exp_origin = 2100,
  exp_target = 2530
})
table.insert(growing, {
  id = 101,
  type = 100004,
  level = 11,
  exp_origin = 2530,
  exp_target = 3000
})
table.insert(growing, {
  id = 102,
  type = 100004,
  level = 12,
  exp_origin = 3000,
  exp_target = 3500
})
table.insert(growing, {
  id = 103,
  type = 100004,
  level = 13,
  exp_origin = 3500,
  exp_target = 4040
})
table.insert(growing, {
  id = 104,
  type = 100004,
  level = 14,
  exp_origin = 4040,
  exp_target = 4620
})
table.insert(growing, {
  id = 105,
  type = 100004,
  level = 15,
  exp_origin = 4620,
  exp_target = 5240
})
table.insert(growing, {
  id = 106,
  type = 100004,
  level = 16,
  exp_origin = 5240,
  exp_target = 5900
})
table.insert(growing, {
  id = 107,
  type = 100004,
  level = 17,
  exp_origin = 5900,
  exp_target = 6600
})
table.insert(growing, {
  id = 108,
  type = 100004,
  level = 18,
  exp_origin = 6600,
  exp_target = 7340
})
table.insert(growing, {
  id = 109,
  type = 100004,
  level = 19,
  exp_origin = 7340,
  exp_target = 8120
})
table.insert(growing, {
  id = 110,
  type = 100004,
  level = 20,
  exp_origin = 8120,
  exp_target = 8930
})
table.insert(growing, {
  id = 111,
  type = 100004,
  level = 21,
  exp_origin = 8930,
  exp_target = 9780
})
table.insert(growing, {
  id = 112,
  type = 100004,
  level = 22,
  exp_origin = 9780,
  exp_target = 10670
})
table.insert(growing, {
  id = 113,
  type = 100004,
  level = 23,
  exp_origin = 10670,
  exp_target = 11600
})
table.insert(growing, {
  id = 114,
  type = 100004,
  level = 24,
  exp_origin = 11600,
  exp_target = 12570
})
table.insert(growing, {
  id = 115,
  type = 100004,
  level = 25,
  exp_origin = 12570,
  exp_target = 13580
})
table.insert(growing, {
  id = 116,
  type = 100004,
  level = 26,
  exp_origin = 13580,
  exp_target = 14630
})
table.insert(growing, {
  id = 117,
  type = 100004,
  level = 27,
  exp_origin = 14630,
  exp_target = 15720
})
table.insert(growing, {
  id = 118,
  type = 100004,
  level = 28,
  exp_origin = 15720,
  exp_target = 16850
})
table.insert(growing, {
  id = 119,
  type = 100004,
  level = 29,
  exp_origin = 16850,
  exp_target = 18010
})
table.insert(growing, {
  id = 120,
  type = 100004,
  level = 30,
  exp_origin = 18010,
  exp_target = 18010
})
table.insert(growing, {
  id = 121,
  type = 100005,
  level = 1,
  exp_origin = 0,
  exp_target = 80
})
table.insert(growing, {
  id = 122,
  type = 100005,
  level = 2,
  exp_origin = 80,
  exp_target = 200
})
table.insert(growing, {
  id = 123,
  type = 100005,
  level = 3,
  exp_origin = 200,
  exp_target = 360
})
table.insert(growing, {
  id = 124,
  type = 100005,
  level = 4,
  exp_origin = 360,
  exp_target = 550
})
table.insert(growing, {
  id = 125,
  type = 100005,
  level = 5,
  exp_origin = 550,
  exp_target = 780
})
table.insert(growing, {
  id = 126,
  type = 100005,
  level = 6,
  exp_origin = 780,
  exp_target = 1050
})
table.insert(growing, {
  id = 127,
  type = 100005,
  level = 7,
  exp_origin = 1050,
  exp_target = 1360
})
table.insert(growing, {
  id = 128,
  type = 100005,
  level = 8,
  exp_origin = 1360,
  exp_target = 1710
})
table.insert(growing, {
  id = 129,
  type = 100005,
  level = 9,
  exp_origin = 1710,
  exp_target = 2100
})
table.insert(growing, {
  id = 130,
  type = 100005,
  level = 10,
  exp_origin = 2100,
  exp_target = 2530
})
table.insert(growing, {
  id = 131,
  type = 100005,
  level = 11,
  exp_origin = 2530,
  exp_target = 3000
})
table.insert(growing, {
  id = 132,
  type = 100005,
  level = 12,
  exp_origin = 3000,
  exp_target = 3500
})
table.insert(growing, {
  id = 133,
  type = 100005,
  level = 13,
  exp_origin = 3500,
  exp_target = 4040
})
table.insert(growing, {
  id = 134,
  type = 100005,
  level = 14,
  exp_origin = 4040,
  exp_target = 4620
})
table.insert(growing, {
  id = 135,
  type = 100005,
  level = 15,
  exp_origin = 4620,
  exp_target = 5240
})
table.insert(growing, {
  id = 136,
  type = 100005,
  level = 16,
  exp_origin = 5240,
  exp_target = 5900
})
table.insert(growing, {
  id = 137,
  type = 100005,
  level = 17,
  exp_origin = 5900,
  exp_target = 6600
})
table.insert(growing, {
  id = 138,
  type = 100005,
  level = 18,
  exp_origin = 6600,
  exp_target = 7340
})
table.insert(growing, {
  id = 139,
  type = 100005,
  level = 19,
  exp_origin = 7340,
  exp_target = 8120
})
table.insert(growing, {
  id = 140,
  type = 100005,
  level = 20,
  exp_origin = 8120,
  exp_target = 8930
})
table.insert(growing, {
  id = 141,
  type = 100005,
  level = 21,
  exp_origin = 8930,
  exp_target = 9780
})
table.insert(growing, {
  id = 142,
  type = 100005,
  level = 22,
  exp_origin = 9780,
  exp_target = 10670
})
table.insert(growing, {
  id = 143,
  type = 100005,
  level = 23,
  exp_origin = 10670,
  exp_target = 11600
})
table.insert(growing, {
  id = 144,
  type = 100005,
  level = 24,
  exp_origin = 11600,
  exp_target = 12570
})
table.insert(growing, {
  id = 145,
  type = 100005,
  level = 25,
  exp_origin = 12570,
  exp_target = 13580
})
table.insert(growing, {
  id = 146,
  type = 100005,
  level = 26,
  exp_origin = 13580,
  exp_target = 14630
})
table.insert(growing, {
  id = 147,
  type = 100005,
  level = 27,
  exp_origin = 14630,
  exp_target = 15720
})
table.insert(growing, {
  id = 148,
  type = 100005,
  level = 28,
  exp_origin = 15720,
  exp_target = 16850
})
table.insert(growing, {
  id = 149,
  type = 100005,
  level = 29,
  exp_origin = 16850,
  exp_target = 18010
})
table.insert(growing, {
  id = 150,
  type = 100005,
  level = 30,
  exp_origin = 18010,
  exp_target = 18010
})
table.insert(growing, {
  id = 151,
  type = 200001,
  level = 1,
  exp_origin = 0,
  exp_target = 290
})
table.insert(growing, {
  id = 152,
  type = 200001,
  level = 2,
  exp_origin = 290,
  exp_target = 730
})
table.insert(growing, {
  id = 153,
  type = 200001,
  level = 3,
  exp_origin = 730,
  exp_target = 1320
})
table.insert(growing, {
  id = 154,
  type = 200001,
  level = 4,
  exp_origin = 1320,
  exp_target = 2050
})
table.insert(growing, {
  id = 155,
  type = 200001,
  level = 5,
  exp_origin = 2050,
  exp_target = 2930
})
table.insert(growing, {
  id = 156,
  type = 200001,
  level = 6,
  exp_origin = 2930,
  exp_target = 3960
})
table.insert(growing, {
  id = 157,
  type = 200001,
  level = 7,
  exp_origin = 3960,
  exp_target = 5130
})
table.insert(growing, {
  id = 158,
  type = 200001,
  level = 8,
  exp_origin = 5130,
  exp_target = 6450
})
table.insert(growing, {
  id = 159,
  type = 200001,
  level = 9,
  exp_origin = 6450,
  exp_target = 7920
})
table.insert(growing, {
  id = 160,
  type = 200001,
  level = 10,
  exp_origin = 7920,
  exp_target = 9530
})
table.insert(growing, {
  id = 161,
  type = 200001,
  level = 11,
  exp_origin = 9530,
  exp_target = 11290
})
table.insert(growing, {
  id = 162,
  type = 200001,
  level = 12,
  exp_origin = 11290,
  exp_target = 13200
})
table.insert(growing, {
  id = 163,
  type = 200001,
  level = 13,
  exp_origin = 13200,
  exp_target = 15250
})
table.insert(growing, {
  id = 164,
  type = 200001,
  level = 14,
  exp_origin = 15250,
  exp_target = 17450
})
table.insert(growing, {
  id = 165,
  type = 200001,
  level = 15,
  exp_origin = 17450,
  exp_target = 19800
})
table.insert(growing, {
  id = 166,
  type = 200001,
  level = 16,
  exp_origin = 19800,
  exp_target = 22300
})
table.insert(growing, {
  id = 167,
  type = 200001,
  level = 17,
  exp_origin = 22300,
  exp_target = 24940
})
table.insert(growing, {
  id = 168,
  type = 200001,
  level = 18,
  exp_origin = 24940,
  exp_target = 27730
})
table.insert(growing, {
  id = 169,
  type = 200001,
  level = 19,
  exp_origin = 27730,
  exp_target = 30670
})
table.insert(growing, {
  id = 170,
  type = 200001,
  level = 20,
  exp_origin = 30670,
  exp_target = 33750
})
table.insert(growing, {
  id = 171,
  type = 200001,
  level = 21,
  exp_origin = 33750,
  exp_target = 36980
})
table.insert(growing, {
  id = 172,
  type = 200001,
  level = 22,
  exp_origin = 36980,
  exp_target = 40360
})
table.insert(growing, {
  id = 173,
  type = 200001,
  level = 23,
  exp_origin = 40360,
  exp_target = 43880
})
table.insert(growing, {
  id = 174,
  type = 200001,
  level = 24,
  exp_origin = 43880,
  exp_target = 47550
})
table.insert(growing, {
  id = 175,
  type = 200001,
  level = 25,
  exp_origin = 47550,
  exp_target = 51370
})
table.insert(growing, {
  id = 176,
  type = 200001,
  level = 26,
  exp_origin = 51370,
  exp_target = 55330
})
table.insert(growing, {
  id = 177,
  type = 200001,
  level = 27,
  exp_origin = 55330,
  exp_target = 59440
})
table.insert(growing, {
  id = 178,
  type = 200001,
  level = 28,
  exp_origin = 59440,
  exp_target = 63700
})
table.insert(growing, {
  id = 179,
  type = 200001,
  level = 29,
  exp_origin = 63700,
  exp_target = 68100
})
table.insert(growing, {
  id = 180,
  type = 200001,
  level = 30,
  exp_origin = 68100,
  exp_target = 68100
})
table.insert(growing, {
  id = 181,
  type = 200002,
  level = 1,
  exp_origin = 0,
  exp_target = 290
})
table.insert(growing, {
  id = 182,
  type = 200002,
  level = 2,
  exp_origin = 290,
  exp_target = 730
})
table.insert(growing, {
  id = 183,
  type = 200002,
  level = 3,
  exp_origin = 730,
  exp_target = 1320
})
table.insert(growing, {
  id = 184,
  type = 200002,
  level = 4,
  exp_origin = 1320,
  exp_target = 2050
})
table.insert(growing, {
  id = 185,
  type = 200002,
  level = 5,
  exp_origin = 2050,
  exp_target = 2930
})
table.insert(growing, {
  id = 186,
  type = 200002,
  level = 6,
  exp_origin = 2930,
  exp_target = 3960
})
table.insert(growing, {
  id = 187,
  type = 200002,
  level = 7,
  exp_origin = 3960,
  exp_target = 5130
})
table.insert(growing, {
  id = 188,
  type = 200002,
  level = 8,
  exp_origin = 5130,
  exp_target = 6450
})
table.insert(growing, {
  id = 189,
  type = 200002,
  level = 9,
  exp_origin = 6450,
  exp_target = 7920
})
table.insert(growing, {
  id = 190,
  type = 200002,
  level = 10,
  exp_origin = 7920,
  exp_target = 9530
})
table.insert(growing, {
  id = 191,
  type = 200002,
  level = 11,
  exp_origin = 9530,
  exp_target = 11290
})
table.insert(growing, {
  id = 192,
  type = 200002,
  level = 12,
  exp_origin = 11290,
  exp_target = 13200
})
table.insert(growing, {
  id = 193,
  type = 200002,
  level = 13,
  exp_origin = 13200,
  exp_target = 15250
})
table.insert(growing, {
  id = 194,
  type = 200002,
  level = 14,
  exp_origin = 15250,
  exp_target = 17450
})
table.insert(growing, {
  id = 195,
  type = 200002,
  level = 15,
  exp_origin = 17450,
  exp_target = 19800
})
table.insert(growing, {
  id = 196,
  type = 200002,
  level = 16,
  exp_origin = 19800,
  exp_target = 22300
})
table.insert(growing, {
  id = 197,
  type = 200002,
  level = 17,
  exp_origin = 22300,
  exp_target = 24940
})
table.insert(growing, {
  id = 198,
  type = 200002,
  level = 18,
  exp_origin = 24940,
  exp_target = 27730
})
table.insert(growing, {
  id = 199,
  type = 200002,
  level = 19,
  exp_origin = 27730,
  exp_target = 30670
})
table.insert(growing, {
  id = 200,
  type = 200002,
  level = 20,
  exp_origin = 30670,
  exp_target = 33750
})
table.insert(growing, {
  id = 201,
  type = 200002,
  level = 21,
  exp_origin = 33750,
  exp_target = 36980
})
table.insert(growing, {
  id = 202,
  type = 200002,
  level = 22,
  exp_origin = 36980,
  exp_target = 40360
})
table.insert(growing, {
  id = 203,
  type = 200002,
  level = 23,
  exp_origin = 40360,
  exp_target = 43880
})
table.insert(growing, {
  id = 204,
  type = 200002,
  level = 24,
  exp_origin = 43880,
  exp_target = 47550
})
table.insert(growing, {
  id = 205,
  type = 200002,
  level = 25,
  exp_origin = 47550,
  exp_target = 51370
})
table.insert(growing, {
  id = 206,
  type = 200002,
  level = 26,
  exp_origin = 51370,
  exp_target = 55330
})
table.insert(growing, {
  id = 207,
  type = 200002,
  level = 27,
  exp_origin = 55330,
  exp_target = 59440
})
table.insert(growing, {
  id = 208,
  type = 200002,
  level = 28,
  exp_origin = 59440,
  exp_target = 63700
})
table.insert(growing, {
  id = 209,
  type = 200002,
  level = 29,
  exp_origin = 63700,
  exp_target = 68100
})
table.insert(growing, {
  id = 210,
  type = 200002,
  level = 30,
  exp_origin = 68100,
  exp_target = 68100
})
table.insert(growing, {
  id = 211,
  type = 200003,
  level = 1,
  exp_origin = 0,
  exp_target = 290
})
table.insert(growing, {
  id = 212,
  type = 200003,
  level = 2,
  exp_origin = 290,
  exp_target = 730
})
table.insert(growing, {
  id = 213,
  type = 200003,
  level = 3,
  exp_origin = 730,
  exp_target = 1320
})
table.insert(growing, {
  id = 214,
  type = 200003,
  level = 4,
  exp_origin = 1320,
  exp_target = 2050
})
table.insert(growing, {
  id = 215,
  type = 200003,
  level = 5,
  exp_origin = 2050,
  exp_target = 2930
})
table.insert(growing, {
  id = 216,
  type = 200003,
  level = 6,
  exp_origin = 2930,
  exp_target = 3960
})
table.insert(growing, {
  id = 217,
  type = 200003,
  level = 7,
  exp_origin = 3960,
  exp_target = 5130
})
table.insert(growing, {
  id = 218,
  type = 200003,
  level = 8,
  exp_origin = 5130,
  exp_target = 6450
})
table.insert(growing, {
  id = 219,
  type = 200003,
  level = 9,
  exp_origin = 6450,
  exp_target = 7920
})
table.insert(growing, {
  id = 220,
  type = 200003,
  level = 10,
  exp_origin = 7920,
  exp_target = 9530
})
table.insert(growing, {
  id = 221,
  type = 200003,
  level = 11,
  exp_origin = 9530,
  exp_target = 11290
})
table.insert(growing, {
  id = 222,
  type = 200003,
  level = 12,
  exp_origin = 11290,
  exp_target = 13200
})
table.insert(growing, {
  id = 223,
  type = 200003,
  level = 13,
  exp_origin = 13200,
  exp_target = 15250
})
table.insert(growing, {
  id = 224,
  type = 200003,
  level = 14,
  exp_origin = 15250,
  exp_target = 17450
})
table.insert(growing, {
  id = 225,
  type = 200003,
  level = 15,
  exp_origin = 17450,
  exp_target = 19800
})
table.insert(growing, {
  id = 226,
  type = 200003,
  level = 16,
  exp_origin = 19800,
  exp_target = 22300
})
table.insert(growing, {
  id = 227,
  type = 200003,
  level = 17,
  exp_origin = 22300,
  exp_target = 24940
})
table.insert(growing, {
  id = 228,
  type = 200003,
  level = 18,
  exp_origin = 24940,
  exp_target = 27730
})
table.insert(growing, {
  id = 229,
  type = 200003,
  level = 19,
  exp_origin = 27730,
  exp_target = 30670
})
table.insert(growing, {
  id = 230,
  type = 200003,
  level = 20,
  exp_origin = 30670,
  exp_target = 33750
})
table.insert(growing, {
  id = 231,
  type = 200003,
  level = 21,
  exp_origin = 33750,
  exp_target = 36980
})
table.insert(growing, {
  id = 232,
  type = 200003,
  level = 22,
  exp_origin = 36980,
  exp_target = 40360
})
table.insert(growing, {
  id = 233,
  type = 200003,
  level = 23,
  exp_origin = 40360,
  exp_target = 43880
})
table.insert(growing, {
  id = 234,
  type = 200003,
  level = 24,
  exp_origin = 43880,
  exp_target = 47550
})
table.insert(growing, {
  id = 235,
  type = 200003,
  level = 25,
  exp_origin = 47550,
  exp_target = 51370
})
table.insert(growing, {
  id = 236,
  type = 200003,
  level = 26,
  exp_origin = 51370,
  exp_target = 55330
})
table.insert(growing, {
  id = 237,
  type = 200003,
  level = 27,
  exp_origin = 55330,
  exp_target = 59440
})
table.insert(growing, {
  id = 238,
  type = 200003,
  level = 28,
  exp_origin = 59440,
  exp_target = 63700
})
table.insert(growing, {
  id = 239,
  type = 200003,
  level = 29,
  exp_origin = 63700,
  exp_target = 68100
})
table.insert(growing, {
  id = 240,
  type = 200003,
  level = 30,
  exp_origin = 68100,
  exp_target = 68100
})
table.insert(growing, {
  id = 241,
  type = 200004,
  level = 1,
  exp_origin = 0,
  exp_target = 290
})
table.insert(growing, {
  id = 242,
  type = 200004,
  level = 2,
  exp_origin = 290,
  exp_target = 730
})
table.insert(growing, {
  id = 243,
  type = 200004,
  level = 3,
  exp_origin = 730,
  exp_target = 1320
})
table.insert(growing, {
  id = 244,
  type = 200004,
  level = 4,
  exp_origin = 1320,
  exp_target = 2050
})
table.insert(growing, {
  id = 245,
  type = 200004,
  level = 5,
  exp_origin = 2050,
  exp_target = 2930
})
table.insert(growing, {
  id = 246,
  type = 200004,
  level = 6,
  exp_origin = 2930,
  exp_target = 3960
})
table.insert(growing, {
  id = 247,
  type = 200004,
  level = 7,
  exp_origin = 3960,
  exp_target = 5130
})
table.insert(growing, {
  id = 248,
  type = 200004,
  level = 8,
  exp_origin = 5130,
  exp_target = 6450
})
table.insert(growing, {
  id = 249,
  type = 200004,
  level = 9,
  exp_origin = 6450,
  exp_target = 7920
})
table.insert(growing, {
  id = 250,
  type = 200004,
  level = 10,
  exp_origin = 7920,
  exp_target = 9530
})
table.insert(growing, {
  id = 251,
  type = 200004,
  level = 11,
  exp_origin = 9530,
  exp_target = 11290
})
table.insert(growing, {
  id = 252,
  type = 200004,
  level = 12,
  exp_origin = 11290,
  exp_target = 13200
})
table.insert(growing, {
  id = 253,
  type = 200004,
  level = 13,
  exp_origin = 13200,
  exp_target = 15250
})
table.insert(growing, {
  id = 254,
  type = 200004,
  level = 14,
  exp_origin = 15250,
  exp_target = 17450
})
table.insert(growing, {
  id = 255,
  type = 200004,
  level = 15,
  exp_origin = 17450,
  exp_target = 19800
})
table.insert(growing, {
  id = 256,
  type = 200004,
  level = 16,
  exp_origin = 19800,
  exp_target = 22300
})
table.insert(growing, {
  id = 257,
  type = 200004,
  level = 17,
  exp_origin = 22300,
  exp_target = 24940
})
table.insert(growing, {
  id = 258,
  type = 200004,
  level = 18,
  exp_origin = 24940,
  exp_target = 27730
})
table.insert(growing, {
  id = 259,
  type = 200004,
  level = 19,
  exp_origin = 27730,
  exp_target = 30670
})
table.insert(growing, {
  id = 260,
  type = 200004,
  level = 20,
  exp_origin = 30670,
  exp_target = 33750
})
table.insert(growing, {
  id = 261,
  type = 200004,
  level = 21,
  exp_origin = 33750,
  exp_target = 36980
})
table.insert(growing, {
  id = 262,
  type = 200004,
  level = 22,
  exp_origin = 36980,
  exp_target = 40360
})
table.insert(growing, {
  id = 263,
  type = 200004,
  level = 23,
  exp_origin = 40360,
  exp_target = 43880
})
table.insert(growing, {
  id = 264,
  type = 200004,
  level = 24,
  exp_origin = 43880,
  exp_target = 47550
})
table.insert(growing, {
  id = 265,
  type = 200004,
  level = 25,
  exp_origin = 47550,
  exp_target = 51370
})
table.insert(growing, {
  id = 266,
  type = 200004,
  level = 26,
  exp_origin = 51370,
  exp_target = 55330
})
table.insert(growing, {
  id = 267,
  type = 200004,
  level = 27,
  exp_origin = 55330,
  exp_target = 59440
})
table.insert(growing, {
  id = 268,
  type = 200004,
  level = 28,
  exp_origin = 59440,
  exp_target = 63700
})
table.insert(growing, {
  id = 269,
  type = 200004,
  level = 29,
  exp_origin = 63700,
  exp_target = 68100
})
table.insert(growing, {
  id = 270,
  type = 200004,
  level = 30,
  exp_origin = 68100,
  exp_target = 68100
})
table.insert(growing, {
  id = 271,
  type = 200005,
  level = 1,
  exp_origin = 0,
  exp_target = 290
})
table.insert(growing, {
  id = 272,
  type = 200005,
  level = 2,
  exp_origin = 290,
  exp_target = 730
})
table.insert(growing, {
  id = 273,
  type = 200005,
  level = 3,
  exp_origin = 730,
  exp_target = 1320
})
table.insert(growing, {
  id = 274,
  type = 200005,
  level = 4,
  exp_origin = 1320,
  exp_target = 2050
})
table.insert(growing, {
  id = 275,
  type = 200005,
  level = 5,
  exp_origin = 2050,
  exp_target = 2930
})
table.insert(growing, {
  id = 276,
  type = 200005,
  level = 6,
  exp_origin = 2930,
  exp_target = 3960
})
table.insert(growing, {
  id = 277,
  type = 200005,
  level = 7,
  exp_origin = 3960,
  exp_target = 5130
})
table.insert(growing, {
  id = 278,
  type = 200005,
  level = 8,
  exp_origin = 5130,
  exp_target = 6450
})
table.insert(growing, {
  id = 279,
  type = 200005,
  level = 9,
  exp_origin = 6450,
  exp_target = 7920
})
table.insert(growing, {
  id = 280,
  type = 200005,
  level = 10,
  exp_origin = 7920,
  exp_target = 9530
})
table.insert(growing, {
  id = 281,
  type = 200005,
  level = 11,
  exp_origin = 9530,
  exp_target = 11290
})
table.insert(growing, {
  id = 282,
  type = 200005,
  level = 12,
  exp_origin = 11290,
  exp_target = 13200
})
table.insert(growing, {
  id = 283,
  type = 200005,
  level = 13,
  exp_origin = 13200,
  exp_target = 15250
})
table.insert(growing, {
  id = 284,
  type = 200005,
  level = 14,
  exp_origin = 15250,
  exp_target = 17450
})
table.insert(growing, {
  id = 285,
  type = 200005,
  level = 15,
  exp_origin = 17450,
  exp_target = 19800
})
table.insert(growing, {
  id = 286,
  type = 200005,
  level = 16,
  exp_origin = 19800,
  exp_target = 22300
})
table.insert(growing, {
  id = 287,
  type = 200005,
  level = 17,
  exp_origin = 22300,
  exp_target = 24940
})
table.insert(growing, {
  id = 288,
  type = 200005,
  level = 18,
  exp_origin = 24940,
  exp_target = 27730
})
table.insert(growing, {
  id = 289,
  type = 200005,
  level = 19,
  exp_origin = 27730,
  exp_target = 30670
})
table.insert(growing, {
  id = 290,
  type = 200005,
  level = 20,
  exp_origin = 30670,
  exp_target = 33750
})
table.insert(growing, {
  id = 291,
  type = 200005,
  level = 21,
  exp_origin = 33750,
  exp_target = 36980
})
table.insert(growing, {
  id = 292,
  type = 200005,
  level = 22,
  exp_origin = 36980,
  exp_target = 40360
})
table.insert(growing, {
  id = 293,
  type = 200005,
  level = 23,
  exp_origin = 40360,
  exp_target = 43880
})
table.insert(growing, {
  id = 294,
  type = 200005,
  level = 24,
  exp_origin = 43880,
  exp_target = 47550
})
table.insert(growing, {
  id = 295,
  type = 200005,
  level = 25,
  exp_origin = 47550,
  exp_target = 51370
})
table.insert(growing, {
  id = 296,
  type = 200005,
  level = 26,
  exp_origin = 51370,
  exp_target = 55330
})
table.insert(growing, {
  id = 297,
  type = 200005,
  level = 27,
  exp_origin = 55330,
  exp_target = 59440
})
table.insert(growing, {
  id = 298,
  type = 200005,
  level = 28,
  exp_origin = 59440,
  exp_target = 63700
})
table.insert(growing, {
  id = 299,
  type = 200005,
  level = 29,
  exp_origin = 63700,
  exp_target = 68100
})
table.insert(growing, {
  id = 300,
  type = 200005,
  level = 30,
  exp_origin = 68100,
  exp_target = 68100
})
table.insert(growing, {
  id = 301,
  type = 300001,
  level = 1,
  exp_origin = 0,
  exp_target = 510
})
table.insert(growing, {
  id = 302,
  type = 300001,
  level = 2,
  exp_origin = 510,
  exp_target = 1280
})
table.insert(growing, {
  id = 303,
  type = 300001,
  level = 3,
  exp_origin = 1280,
  exp_target = 2300
})
table.insert(growing, {
  id = 304,
  type = 300001,
  level = 4,
  exp_origin = 2300,
  exp_target = 3580
})
table.insert(growing, {
  id = 305,
  type = 300001,
  level = 5,
  exp_origin = 3580,
  exp_target = 5110
})
table.insert(growing, {
  id = 306,
  type = 300001,
  level = 6,
  exp_origin = 5110,
  exp_target = 6900
})
table.insert(growing, {
  id = 307,
  type = 300001,
  level = 7,
  exp_origin = 6900,
  exp_target = 8940
})
table.insert(growing, {
  id = 308,
  type = 300001,
  level = 8,
  exp_origin = 8940,
  exp_target = 11240
})
table.insert(growing, {
  id = 309,
  type = 300001,
  level = 9,
  exp_origin = 11240,
  exp_target = 13790
})
table.insert(growing, {
  id = 310,
  type = 300001,
  level = 10,
  exp_origin = 13790,
  exp_target = 16600
})
table.insert(growing, {
  id = 311,
  type = 300001,
  level = 11,
  exp_origin = 16600,
  exp_target = 19660
})
table.insert(growing, {
  id = 312,
  type = 300001,
  level = 12,
  exp_origin = 19660,
  exp_target = 22980
})
table.insert(growing, {
  id = 313,
  type = 300001,
  level = 13,
  exp_origin = 22980,
  exp_target = 26550
})
table.insert(growing, {
  id = 314,
  type = 300001,
  level = 14,
  exp_origin = 26550,
  exp_target = 30380
})
table.insert(growing, {
  id = 315,
  type = 300001,
  level = 15,
  exp_origin = 30380,
  exp_target = 34460
})
table.insert(growing, {
  id = 316,
  type = 300001,
  level = 16,
  exp_origin = 34460,
  exp_target = 38800
})
table.insert(growing, {
  id = 317,
  type = 300001,
  level = 17,
  exp_origin = 38800,
  exp_target = 43390
})
table.insert(growing, {
  id = 318,
  type = 300001,
  level = 18,
  exp_origin = 43390,
  exp_target = 48240
})
table.insert(growing, {
  id = 319,
  type = 300001,
  level = 19,
  exp_origin = 48240,
  exp_target = 53340
})
table.insert(growing, {
  id = 320,
  type = 300001,
  level = 20,
  exp_origin = 53340,
  exp_target = 58700
})
table.insert(growing, {
  id = 321,
  type = 300001,
  level = 21,
  exp_origin = 58700,
  exp_target = 64310
})
table.insert(growing, {
  id = 322,
  type = 300001,
  level = 22,
  exp_origin = 64310,
  exp_target = 70180
})
table.insert(growing, {
  id = 323,
  type = 300001,
  level = 23,
  exp_origin = 70180,
  exp_target = 76300
})
table.insert(growing, {
  id = 324,
  type = 300001,
  level = 24,
  exp_origin = 76300,
  exp_target = 82680
})
table.insert(growing, {
  id = 325,
  type = 300001,
  level = 25,
  exp_origin = 82680,
  exp_target = 89310
})
table.insert(growing, {
  id = 326,
  type = 300001,
  level = 26,
  exp_origin = 89310,
  exp_target = 96200
})
table.insert(growing, {
  id = 327,
  type = 300001,
  level = 27,
  exp_origin = 96200,
  exp_target = 103340
})
table.insert(growing, {
  id = 328,
  type = 300001,
  level = 28,
  exp_origin = 103340,
  exp_target = 110740
})
table.insert(growing, {
  id = 329,
  type = 300001,
  level = 29,
  exp_origin = 110740,
  exp_target = 118390
})
table.insert(growing, {
  id = 330,
  type = 300001,
  level = 30,
  exp_origin = 118390,
  exp_target = 118390
})
table.insert(growing, {
  id = 331,
  type = 300002,
  level = 1,
  exp_origin = 0,
  exp_target = 510
})
table.insert(growing, {
  id = 332,
  type = 300002,
  level = 2,
  exp_origin = 510,
  exp_target = 1280
})
table.insert(growing, {
  id = 333,
  type = 300002,
  level = 3,
  exp_origin = 1280,
  exp_target = 2300
})
table.insert(growing, {
  id = 334,
  type = 300002,
  level = 4,
  exp_origin = 2300,
  exp_target = 3580
})
table.insert(growing, {
  id = 335,
  type = 300002,
  level = 5,
  exp_origin = 3580,
  exp_target = 5110
})
table.insert(growing, {
  id = 336,
  type = 300002,
  level = 6,
  exp_origin = 5110,
  exp_target = 6900
})
table.insert(growing, {
  id = 337,
  type = 300002,
  level = 7,
  exp_origin = 6900,
  exp_target = 8940
})
table.insert(growing, {
  id = 338,
  type = 300002,
  level = 8,
  exp_origin = 8940,
  exp_target = 11240
})
table.insert(growing, {
  id = 339,
  type = 300002,
  level = 9,
  exp_origin = 11240,
  exp_target = 13790
})
table.insert(growing, {
  id = 340,
  type = 300002,
  level = 10,
  exp_origin = 13790,
  exp_target = 16600
})
table.insert(growing, {
  id = 341,
  type = 300002,
  level = 11,
  exp_origin = 16600,
  exp_target = 19660
})
table.insert(growing, {
  id = 342,
  type = 300002,
  level = 12,
  exp_origin = 19660,
  exp_target = 22980
})
table.insert(growing, {
  id = 343,
  type = 300002,
  level = 13,
  exp_origin = 22980,
  exp_target = 26550
})
table.insert(growing, {
  id = 344,
  type = 300002,
  level = 14,
  exp_origin = 26550,
  exp_target = 30380
})
table.insert(growing, {
  id = 345,
  type = 300002,
  level = 15,
  exp_origin = 30380,
  exp_target = 34460
})
table.insert(growing, {
  id = 346,
  type = 300002,
  level = 16,
  exp_origin = 34460,
  exp_target = 38800
})
table.insert(growing, {
  id = 347,
  type = 300002,
  level = 17,
  exp_origin = 38800,
  exp_target = 43390
})
table.insert(growing, {
  id = 348,
  type = 300002,
  level = 18,
  exp_origin = 43390,
  exp_target = 48240
})
table.insert(growing, {
  id = 349,
  type = 300002,
  level = 19,
  exp_origin = 48240,
  exp_target = 53340
})
table.insert(growing, {
  id = 350,
  type = 300002,
  level = 20,
  exp_origin = 53340,
  exp_target = 58700
})
table.insert(growing, {
  id = 351,
  type = 300002,
  level = 21,
  exp_origin = 58700,
  exp_target = 64310
})
table.insert(growing, {
  id = 352,
  type = 300002,
  level = 22,
  exp_origin = 64310,
  exp_target = 70180
})
table.insert(growing, {
  id = 353,
  type = 300002,
  level = 23,
  exp_origin = 70180,
  exp_target = 76300
})
table.insert(growing, {
  id = 354,
  type = 300002,
  level = 24,
  exp_origin = 76300,
  exp_target = 82680
})
table.insert(growing, {
  id = 355,
  type = 300002,
  level = 25,
  exp_origin = 82680,
  exp_target = 89310
})
table.insert(growing, {
  id = 356,
  type = 300002,
  level = 26,
  exp_origin = 89310,
  exp_target = 96200
})
table.insert(growing, {
  id = 357,
  type = 300002,
  level = 27,
  exp_origin = 96200,
  exp_target = 103340
})
table.insert(growing, {
  id = 358,
  type = 300002,
  level = 28,
  exp_origin = 103340,
  exp_target = 110740
})
table.insert(growing, {
  id = 359,
  type = 300002,
  level = 29,
  exp_origin = 110740,
  exp_target = 118390
})
table.insert(growing, {
  id = 360,
  type = 300002,
  level = 30,
  exp_origin = 118390,
  exp_target = 118390
})
table.insert(growing, {
  id = 361,
  type = 300003,
  level = 1,
  exp_origin = 0,
  exp_target = 510
})
table.insert(growing, {
  id = 362,
  type = 300003,
  level = 2,
  exp_origin = 510,
  exp_target = 1280
})
table.insert(growing, {
  id = 363,
  type = 300003,
  level = 3,
  exp_origin = 1280,
  exp_target = 2300
})
table.insert(growing, {
  id = 364,
  type = 300003,
  level = 4,
  exp_origin = 2300,
  exp_target = 3580
})
table.insert(growing, {
  id = 365,
  type = 300003,
  level = 5,
  exp_origin = 3580,
  exp_target = 5110
})
table.insert(growing, {
  id = 366,
  type = 300003,
  level = 6,
  exp_origin = 5110,
  exp_target = 6900
})
table.insert(growing, {
  id = 367,
  type = 300003,
  level = 7,
  exp_origin = 6900,
  exp_target = 8940
})
table.insert(growing, {
  id = 368,
  type = 300003,
  level = 8,
  exp_origin = 8940,
  exp_target = 11240
})
table.insert(growing, {
  id = 369,
  type = 300003,
  level = 9,
  exp_origin = 11240,
  exp_target = 13790
})
table.insert(growing, {
  id = 370,
  type = 300003,
  level = 10,
  exp_origin = 13790,
  exp_target = 16600
})
table.insert(growing, {
  id = 371,
  type = 300003,
  level = 11,
  exp_origin = 16600,
  exp_target = 19660
})
table.insert(growing, {
  id = 372,
  type = 300003,
  level = 12,
  exp_origin = 19660,
  exp_target = 22980
})
table.insert(growing, {
  id = 373,
  type = 300003,
  level = 13,
  exp_origin = 22980,
  exp_target = 26550
})
table.insert(growing, {
  id = 374,
  type = 300003,
  level = 14,
  exp_origin = 26550,
  exp_target = 30380
})
table.insert(growing, {
  id = 375,
  type = 300003,
  level = 15,
  exp_origin = 30380,
  exp_target = 34460
})
table.insert(growing, {
  id = 376,
  type = 300003,
  level = 16,
  exp_origin = 34460,
  exp_target = 38800
})
table.insert(growing, {
  id = 377,
  type = 300003,
  level = 17,
  exp_origin = 38800,
  exp_target = 43390
})
table.insert(growing, {
  id = 378,
  type = 300003,
  level = 18,
  exp_origin = 43390,
  exp_target = 48240
})
table.insert(growing, {
  id = 379,
  type = 300003,
  level = 19,
  exp_origin = 48240,
  exp_target = 53340
})
table.insert(growing, {
  id = 380,
  type = 300003,
  level = 20,
  exp_origin = 53340,
  exp_target = 58700
})
table.insert(growing, {
  id = 381,
  type = 300003,
  level = 21,
  exp_origin = 58700,
  exp_target = 64310
})
table.insert(growing, {
  id = 382,
  type = 300003,
  level = 22,
  exp_origin = 64310,
  exp_target = 70180
})
table.insert(growing, {
  id = 383,
  type = 300003,
  level = 23,
  exp_origin = 70180,
  exp_target = 76300
})
table.insert(growing, {
  id = 384,
  type = 300003,
  level = 24,
  exp_origin = 76300,
  exp_target = 82680
})
table.insert(growing, {
  id = 385,
  type = 300003,
  level = 25,
  exp_origin = 82680,
  exp_target = 89310
})
table.insert(growing, {
  id = 386,
  type = 300003,
  level = 26,
  exp_origin = 89310,
  exp_target = 96200
})
table.insert(growing, {
  id = 387,
  type = 300003,
  level = 27,
  exp_origin = 96200,
  exp_target = 103340
})
table.insert(growing, {
  id = 388,
  type = 300003,
  level = 28,
  exp_origin = 103340,
  exp_target = 110740
})
table.insert(growing, {
  id = 389,
  type = 300003,
  level = 29,
  exp_origin = 110740,
  exp_target = 118390
})
table.insert(growing, {
  id = 390,
  type = 300003,
  level = 30,
  exp_origin = 118390,
  exp_target = 118390
})
table.insert(growing, {
  id = 391,
  type = 300004,
  level = 1,
  exp_origin = 0,
  exp_target = 510
})
table.insert(growing, {
  id = 392,
  type = 300004,
  level = 2,
  exp_origin = 510,
  exp_target = 1280
})
table.insert(growing, {
  id = 393,
  type = 300004,
  level = 3,
  exp_origin = 1280,
  exp_target = 2300
})
table.insert(growing, {
  id = 394,
  type = 300004,
  level = 4,
  exp_origin = 2300,
  exp_target = 3580
})
table.insert(growing, {
  id = 395,
  type = 300004,
  level = 5,
  exp_origin = 3580,
  exp_target = 5110
})
table.insert(growing, {
  id = 396,
  type = 300004,
  level = 6,
  exp_origin = 5110,
  exp_target = 6900
})
table.insert(growing, {
  id = 397,
  type = 300004,
  level = 7,
  exp_origin = 6900,
  exp_target = 8940
})
table.insert(growing, {
  id = 398,
  type = 300004,
  level = 8,
  exp_origin = 8940,
  exp_target = 11240
})
table.insert(growing, {
  id = 399,
  type = 300004,
  level = 9,
  exp_origin = 11240,
  exp_target = 13790
})
table.insert(growing, {
  id = 400,
  type = 300004,
  level = 10,
  exp_origin = 13790,
  exp_target = 16600
})
table.insert(growing, {
  id = 401,
  type = 300004,
  level = 11,
  exp_origin = 16600,
  exp_target = 19660
})
table.insert(growing, {
  id = 402,
  type = 300004,
  level = 12,
  exp_origin = 19660,
  exp_target = 22980
})
table.insert(growing, {
  id = 403,
  type = 300004,
  level = 13,
  exp_origin = 22980,
  exp_target = 26550
})
table.insert(growing, {
  id = 404,
  type = 300004,
  level = 14,
  exp_origin = 26550,
  exp_target = 30380
})
table.insert(growing, {
  id = 405,
  type = 300004,
  level = 15,
  exp_origin = 30380,
  exp_target = 34460
})
table.insert(growing, {
  id = 406,
  type = 300004,
  level = 16,
  exp_origin = 34460,
  exp_target = 38800
})
table.insert(growing, {
  id = 407,
  type = 300004,
  level = 17,
  exp_origin = 38800,
  exp_target = 43390
})
table.insert(growing, {
  id = 408,
  type = 300004,
  level = 18,
  exp_origin = 43390,
  exp_target = 48240
})
table.insert(growing, {
  id = 409,
  type = 300004,
  level = 19,
  exp_origin = 48240,
  exp_target = 53340
})
table.insert(growing, {
  id = 410,
  type = 300004,
  level = 20,
  exp_origin = 53340,
  exp_target = 58700
})
table.insert(growing, {
  id = 411,
  type = 300004,
  level = 21,
  exp_origin = 58700,
  exp_target = 64310
})
table.insert(growing, {
  id = 412,
  type = 300004,
  level = 22,
  exp_origin = 64310,
  exp_target = 70180
})
table.insert(growing, {
  id = 413,
  type = 300004,
  level = 23,
  exp_origin = 70180,
  exp_target = 76300
})
table.insert(growing, {
  id = 414,
  type = 300004,
  level = 24,
  exp_origin = 76300,
  exp_target = 82680
})
table.insert(growing, {
  id = 415,
  type = 300004,
  level = 25,
  exp_origin = 82680,
  exp_target = 89310
})
table.insert(growing, {
  id = 416,
  type = 300004,
  level = 26,
  exp_origin = 89310,
  exp_target = 96200
})
table.insert(growing, {
  id = 417,
  type = 300004,
  level = 27,
  exp_origin = 96200,
  exp_target = 103340
})
table.insert(growing, {
  id = 418,
  type = 300004,
  level = 28,
  exp_origin = 103340,
  exp_target = 110740
})
table.insert(growing, {
  id = 419,
  type = 300004,
  level = 29,
  exp_origin = 110740,
  exp_target = 118390
})
table.insert(growing, {
  id = 420,
  type = 300004,
  level = 30,
  exp_origin = 118390,
  exp_target = 118390
})
table.insert(growing, {
  id = 421,
  type = 300005,
  level = 1,
  exp_origin = 0,
  exp_target = 510
})
table.insert(growing, {
  id = 422,
  type = 300005,
  level = 2,
  exp_origin = 510,
  exp_target = 1280
})
table.insert(growing, {
  id = 423,
  type = 300005,
  level = 3,
  exp_origin = 1280,
  exp_target = 2300
})
table.insert(growing, {
  id = 424,
  type = 300005,
  level = 4,
  exp_origin = 2300,
  exp_target = 3580
})
table.insert(growing, {
  id = 425,
  type = 300005,
  level = 5,
  exp_origin = 3580,
  exp_target = 5110
})
table.insert(growing, {
  id = 426,
  type = 300005,
  level = 6,
  exp_origin = 5110,
  exp_target = 6900
})
table.insert(growing, {
  id = 427,
  type = 300005,
  level = 7,
  exp_origin = 6900,
  exp_target = 8940
})
table.insert(growing, {
  id = 428,
  type = 300005,
  level = 8,
  exp_origin = 8940,
  exp_target = 11240
})
table.insert(growing, {
  id = 429,
  type = 300005,
  level = 9,
  exp_origin = 11240,
  exp_target = 13790
})
table.insert(growing, {
  id = 430,
  type = 300005,
  level = 10,
  exp_origin = 13790,
  exp_target = 16600
})
table.insert(growing, {
  id = 431,
  type = 300005,
  level = 11,
  exp_origin = 16600,
  exp_target = 19660
})
table.insert(growing, {
  id = 432,
  type = 300005,
  level = 12,
  exp_origin = 19660,
  exp_target = 22980
})
table.insert(growing, {
  id = 433,
  type = 300005,
  level = 13,
  exp_origin = 22980,
  exp_target = 26550
})
table.insert(growing, {
  id = 434,
  type = 300005,
  level = 14,
  exp_origin = 26550,
  exp_target = 30380
})
table.insert(growing, {
  id = 435,
  type = 300005,
  level = 15,
  exp_origin = 30380,
  exp_target = 34460
})
table.insert(growing, {
  id = 436,
  type = 300005,
  level = 16,
  exp_origin = 34460,
  exp_target = 38800
})
table.insert(growing, {
  id = 437,
  type = 300005,
  level = 17,
  exp_origin = 38800,
  exp_target = 43390
})
table.insert(growing, {
  id = 438,
  type = 300005,
  level = 18,
  exp_origin = 43390,
  exp_target = 48240
})
table.insert(growing, {
  id = 439,
  type = 300005,
  level = 19,
  exp_origin = 48240,
  exp_target = 53340
})
table.insert(growing, {
  id = 440,
  type = 300005,
  level = 20,
  exp_origin = 53340,
  exp_target = 58700
})
table.insert(growing, {
  id = 441,
  type = 300005,
  level = 21,
  exp_origin = 58700,
  exp_target = 64310
})
table.insert(growing, {
  id = 442,
  type = 300005,
  level = 22,
  exp_origin = 64310,
  exp_target = 70180
})
table.insert(growing, {
  id = 443,
  type = 300005,
  level = 23,
  exp_origin = 70180,
  exp_target = 76300
})
table.insert(growing, {
  id = 444,
  type = 300005,
  level = 24,
  exp_origin = 76300,
  exp_target = 82680
})
table.insert(growing, {
  id = 445,
  type = 300005,
  level = 25,
  exp_origin = 82680,
  exp_target = 89310
})
table.insert(growing, {
  id = 446,
  type = 300005,
  level = 26,
  exp_origin = 89310,
  exp_target = 96200
})
table.insert(growing, {
  id = 447,
  type = 300005,
  level = 27,
  exp_origin = 96200,
  exp_target = 103340
})
table.insert(growing, {
  id = 448,
  type = 300005,
  level = 28,
  exp_origin = 103340,
  exp_target = 110740
})
table.insert(growing, {
  id = 449,
  type = 300005,
  level = 29,
  exp_origin = 110740,
  exp_target = 118390
})
table.insert(growing, {
  id = 450,
  type = 300005,
  level = 30,
  exp_origin = 118390,
  exp_target = 118390
})
table.insert(growing, {
  id = 451,
  type = 400001,
  level = 1,
  exp_origin = 0,
  exp_target = 740
})
table.insert(growing, {
  id = 452,
  type = 400001,
  level = 2,
  exp_origin = 740,
  exp_target = 1840
})
table.insert(growing, {
  id = 453,
  type = 400001,
  level = 3,
  exp_origin = 1840,
  exp_target = 3310
})
table.insert(growing, {
  id = 454,
  type = 400001,
  level = 4,
  exp_origin = 3310,
  exp_target = 5150
})
table.insert(growing, {
  id = 455,
  type = 400001,
  level = 5,
  exp_origin = 5150,
  exp_target = 7360
})
table.insert(growing, {
  id = 456,
  type = 400001,
  level = 6,
  exp_origin = 7360,
  exp_target = 9940
})
table.insert(growing, {
  id = 457,
  type = 400001,
  level = 7,
  exp_origin = 9940,
  exp_target = 12880
})
table.insert(growing, {
  id = 458,
  type = 400001,
  level = 8,
  exp_origin = 12880,
  exp_target = 16190
})
table.insert(growing, {
  id = 459,
  type = 400001,
  level = 9,
  exp_origin = 16190,
  exp_target = 19870
})
table.insert(growing, {
  id = 460,
  type = 400001,
  level = 10,
  exp_origin = 19870,
  exp_target = 23920
})
table.insert(growing, {
  id = 461,
  type = 400001,
  level = 11,
  exp_origin = 23920,
  exp_target = 28330
})
table.insert(growing, {
  id = 462,
  type = 400001,
  level = 12,
  exp_origin = 28330,
  exp_target = 33110
})
table.insert(growing, {
  id = 463,
  type = 400001,
  level = 13,
  exp_origin = 33110,
  exp_target = 38260
})
table.insert(growing, {
  id = 464,
  type = 400001,
  level = 14,
  exp_origin = 38260,
  exp_target = 43780
})
table.insert(growing, {
  id = 465,
  type = 400001,
  level = 15,
  exp_origin = 43780,
  exp_target = 49670
})
table.insert(growing, {
  id = 466,
  type = 400001,
  level = 16,
  exp_origin = 49670,
  exp_target = 55920
})
table.insert(growing, {
  id = 467,
  type = 400001,
  level = 17,
  exp_origin = 55920,
  exp_target = 62540
})
table.insert(growing, {
  id = 468,
  type = 400001,
  level = 18,
  exp_origin = 62540,
  exp_target = 69530
})
table.insert(growing, {
  id = 469,
  type = 400001,
  level = 19,
  exp_origin = 69530,
  exp_target = 76890
})
table.insert(growing, {
  id = 470,
  type = 400001,
  level = 20,
  exp_origin = 76890,
  exp_target = 84620
})
table.insert(growing, {
  id = 471,
  type = 400001,
  level = 21,
  exp_origin = 84620,
  exp_target = 92710
})
table.insert(growing, {
  id = 472,
  type = 400001,
  level = 22,
  exp_origin = 92710,
  exp_target = 101170
})
table.insert(growing, {
  id = 473,
  type = 400001,
  level = 23,
  exp_origin = 101170,
  exp_target = 110000
})
table.insert(growing, {
  id = 474,
  type = 400001,
  level = 24,
  exp_origin = 110000,
  exp_target = 119200
})
table.insert(growing, {
  id = 475,
  type = 400001,
  level = 25,
  exp_origin = 119200,
  exp_target = 128770
})
table.insert(growing, {
  id = 476,
  type = 400001,
  level = 26,
  exp_origin = 128770,
  exp_target = 138700
})
table.insert(growing, {
  id = 477,
  type = 400001,
  level = 27,
  exp_origin = 138700,
  exp_target = 149000
})
table.insert(growing, {
  id = 478,
  type = 400001,
  level = 28,
  exp_origin = 149000,
  exp_target = 159670
})
table.insert(growing, {
  id = 479,
  type = 400001,
  level = 29,
  exp_origin = 159670,
  exp_target = 170710
})
table.insert(growing, {
  id = 480,
  type = 400001,
  level = 30,
  exp_origin = 170710,
  exp_target = 170710
})
table.insert(growing, {
  id = 481,
  type = 400002,
  level = 1,
  exp_origin = 0,
  exp_target = 740
})
table.insert(growing, {
  id = 482,
  type = 400002,
  level = 2,
  exp_origin = 740,
  exp_target = 1840
})
table.insert(growing, {
  id = 483,
  type = 400002,
  level = 3,
  exp_origin = 1840,
  exp_target = 3310
})
table.insert(growing, {
  id = 484,
  type = 400002,
  level = 4,
  exp_origin = 3310,
  exp_target = 5150
})
table.insert(growing, {
  id = 485,
  type = 400002,
  level = 5,
  exp_origin = 5150,
  exp_target = 7360
})
table.insert(growing, {
  id = 486,
  type = 400002,
  level = 6,
  exp_origin = 7360,
  exp_target = 9940
})
table.insert(growing, {
  id = 487,
  type = 400002,
  level = 7,
  exp_origin = 9940,
  exp_target = 12880
})
table.insert(growing, {
  id = 488,
  type = 400002,
  level = 8,
  exp_origin = 12880,
  exp_target = 16190
})
table.insert(growing, {
  id = 489,
  type = 400002,
  level = 9,
  exp_origin = 16190,
  exp_target = 19870
})
table.insert(growing, {
  id = 490,
  type = 400002,
  level = 10,
  exp_origin = 19870,
  exp_target = 23920
})
table.insert(growing, {
  id = 491,
  type = 400002,
  level = 11,
  exp_origin = 23920,
  exp_target = 28330
})
table.insert(growing, {
  id = 492,
  type = 400002,
  level = 12,
  exp_origin = 28330,
  exp_target = 33110
})
table.insert(growing, {
  id = 493,
  type = 400002,
  level = 13,
  exp_origin = 33110,
  exp_target = 38260
})
table.insert(growing, {
  id = 494,
  type = 400002,
  level = 14,
  exp_origin = 38260,
  exp_target = 43780
})
table.insert(growing, {
  id = 495,
  type = 400002,
  level = 15,
  exp_origin = 43780,
  exp_target = 49670
})
table.insert(growing, {
  id = 496,
  type = 400002,
  level = 16,
  exp_origin = 49670,
  exp_target = 55920
})
table.insert(growing, {
  id = 497,
  type = 400002,
  level = 17,
  exp_origin = 55920,
  exp_target = 62540
})
table.insert(growing, {
  id = 498,
  type = 400002,
  level = 18,
  exp_origin = 62540,
  exp_target = 69530
})
table.insert(growing, {
  id = 499,
  type = 400002,
  level = 19,
  exp_origin = 69530,
  exp_target = 76890
})
table.insert(growing, {
  id = 500,
  type = 400002,
  level = 20,
  exp_origin = 76890,
  exp_target = 84620
})
table.insert(growing, {
  id = 501,
  type = 400002,
  level = 21,
  exp_origin = 84620,
  exp_target = 92710
})
table.insert(growing, {
  id = 502,
  type = 400002,
  level = 22,
  exp_origin = 92710,
  exp_target = 101170
})
table.insert(growing, {
  id = 503,
  type = 400002,
  level = 23,
  exp_origin = 101170,
  exp_target = 110000
})
table.insert(growing, {
  id = 504,
  type = 400002,
  level = 24,
  exp_origin = 110000,
  exp_target = 119200
})
table.insert(growing, {
  id = 505,
  type = 400002,
  level = 25,
  exp_origin = 119200,
  exp_target = 128770
})
table.insert(growing, {
  id = 506,
  type = 400002,
  level = 26,
  exp_origin = 128770,
  exp_target = 138700
})
table.insert(growing, {
  id = 507,
  type = 400002,
  level = 27,
  exp_origin = 138700,
  exp_target = 149000
})
table.insert(growing, {
  id = 508,
  type = 400002,
  level = 28,
  exp_origin = 149000,
  exp_target = 159670
})
table.insert(growing, {
  id = 509,
  type = 400002,
  level = 29,
  exp_origin = 159670,
  exp_target = 170710
})
table.insert(growing, {
  id = 510,
  type = 400002,
  level = 30,
  exp_origin = 170710,
  exp_target = 170710
})
table.insert(growing, {
  id = 511,
  type = 400003,
  level = 1,
  exp_origin = 0,
  exp_target = 740
})
table.insert(growing, {
  id = 512,
  type = 400003,
  level = 2,
  exp_origin = 740,
  exp_target = 1840
})
table.insert(growing, {
  id = 513,
  type = 400003,
  level = 3,
  exp_origin = 1840,
  exp_target = 3310
})
table.insert(growing, {
  id = 514,
  type = 400003,
  level = 4,
  exp_origin = 3310,
  exp_target = 5150
})
table.insert(growing, {
  id = 515,
  type = 400003,
  level = 5,
  exp_origin = 5150,
  exp_target = 7360
})
table.insert(growing, {
  id = 516,
  type = 400003,
  level = 6,
  exp_origin = 7360,
  exp_target = 9940
})
table.insert(growing, {
  id = 517,
  type = 400003,
  level = 7,
  exp_origin = 9940,
  exp_target = 12880
})
table.insert(growing, {
  id = 518,
  type = 400003,
  level = 8,
  exp_origin = 12880,
  exp_target = 16190
})
table.insert(growing, {
  id = 519,
  type = 400003,
  level = 9,
  exp_origin = 16190,
  exp_target = 19870
})
table.insert(growing, {
  id = 520,
  type = 400003,
  level = 10,
  exp_origin = 19870,
  exp_target = 23920
})
table.insert(growing, {
  id = 521,
  type = 400003,
  level = 11,
  exp_origin = 23920,
  exp_target = 28330
})
table.insert(growing, {
  id = 522,
  type = 400003,
  level = 12,
  exp_origin = 28330,
  exp_target = 33110
})
table.insert(growing, {
  id = 523,
  type = 400003,
  level = 13,
  exp_origin = 33110,
  exp_target = 38260
})
table.insert(growing, {
  id = 524,
  type = 400003,
  level = 14,
  exp_origin = 38260,
  exp_target = 43780
})
table.insert(growing, {
  id = 525,
  type = 400003,
  level = 15,
  exp_origin = 43780,
  exp_target = 49670
})
table.insert(growing, {
  id = 526,
  type = 400003,
  level = 16,
  exp_origin = 49670,
  exp_target = 55920
})
table.insert(growing, {
  id = 527,
  type = 400003,
  level = 17,
  exp_origin = 55920,
  exp_target = 62540
})
table.insert(growing, {
  id = 528,
  type = 400003,
  level = 18,
  exp_origin = 62540,
  exp_target = 69530
})
table.insert(growing, {
  id = 529,
  type = 400003,
  level = 19,
  exp_origin = 69530,
  exp_target = 76890
})
table.insert(growing, {
  id = 530,
  type = 400003,
  level = 20,
  exp_origin = 76890,
  exp_target = 84620
})
table.insert(growing, {
  id = 531,
  type = 400003,
  level = 21,
  exp_origin = 84620,
  exp_target = 92710
})
table.insert(growing, {
  id = 532,
  type = 400003,
  level = 22,
  exp_origin = 92710,
  exp_target = 101170
})
table.insert(growing, {
  id = 533,
  type = 400003,
  level = 23,
  exp_origin = 101170,
  exp_target = 110000
})
table.insert(growing, {
  id = 534,
  type = 400003,
  level = 24,
  exp_origin = 110000,
  exp_target = 119200
})
table.insert(growing, {
  id = 535,
  type = 400003,
  level = 25,
  exp_origin = 119200,
  exp_target = 128770
})
table.insert(growing, {
  id = 536,
  type = 400003,
  level = 26,
  exp_origin = 128770,
  exp_target = 138700
})
table.insert(growing, {
  id = 537,
  type = 400003,
  level = 27,
  exp_origin = 138700,
  exp_target = 149000
})
table.insert(growing, {
  id = 538,
  type = 400003,
  level = 28,
  exp_origin = 149000,
  exp_target = 159670
})
table.insert(growing, {
  id = 539,
  type = 400003,
  level = 29,
  exp_origin = 159670,
  exp_target = 170710
})
table.insert(growing, {
  id = 540,
  type = 400003,
  level = 30,
  exp_origin = 170710,
  exp_target = 170710
})
table.insert(growing, {
  id = 541,
  type = 400004,
  level = 1,
  exp_origin = 0,
  exp_target = 740
})
table.insert(growing, {
  id = 542,
  type = 400004,
  level = 2,
  exp_origin = 740,
  exp_target = 1840
})
table.insert(growing, {
  id = 543,
  type = 400004,
  level = 3,
  exp_origin = 1840,
  exp_target = 3310
})
table.insert(growing, {
  id = 544,
  type = 400004,
  level = 4,
  exp_origin = 3310,
  exp_target = 5150
})
table.insert(growing, {
  id = 545,
  type = 400004,
  level = 5,
  exp_origin = 5150,
  exp_target = 7360
})
table.insert(growing, {
  id = 546,
  type = 400004,
  level = 6,
  exp_origin = 7360,
  exp_target = 9940
})
table.insert(growing, {
  id = 547,
  type = 400004,
  level = 7,
  exp_origin = 9940,
  exp_target = 12880
})
table.insert(growing, {
  id = 548,
  type = 400004,
  level = 8,
  exp_origin = 12880,
  exp_target = 16190
})
table.insert(growing, {
  id = 549,
  type = 400004,
  level = 9,
  exp_origin = 16190,
  exp_target = 19870
})
table.insert(growing, {
  id = 550,
  type = 400004,
  level = 10,
  exp_origin = 19870,
  exp_target = 23920
})
table.insert(growing, {
  id = 551,
  type = 400004,
  level = 11,
  exp_origin = 23920,
  exp_target = 28330
})
table.insert(growing, {
  id = 552,
  type = 400004,
  level = 12,
  exp_origin = 28330,
  exp_target = 33110
})
table.insert(growing, {
  id = 553,
  type = 400004,
  level = 13,
  exp_origin = 33110,
  exp_target = 38260
})
table.insert(growing, {
  id = 554,
  type = 400004,
  level = 14,
  exp_origin = 38260,
  exp_target = 43780
})
table.insert(growing, {
  id = 555,
  type = 400004,
  level = 15,
  exp_origin = 43780,
  exp_target = 49670
})
table.insert(growing, {
  id = 556,
  type = 400004,
  level = 16,
  exp_origin = 49670,
  exp_target = 55920
})
table.insert(growing, {
  id = 557,
  type = 400004,
  level = 17,
  exp_origin = 55920,
  exp_target = 62540
})
table.insert(growing, {
  id = 558,
  type = 400004,
  level = 18,
  exp_origin = 62540,
  exp_target = 69530
})
table.insert(growing, {
  id = 559,
  type = 400004,
  level = 19,
  exp_origin = 69530,
  exp_target = 76890
})
table.insert(growing, {
  id = 560,
  type = 400004,
  level = 20,
  exp_origin = 76890,
  exp_target = 84620
})
table.insert(growing, {
  id = 561,
  type = 400004,
  level = 21,
  exp_origin = 84620,
  exp_target = 92710
})
table.insert(growing, {
  id = 562,
  type = 400004,
  level = 22,
  exp_origin = 92710,
  exp_target = 101170
})
table.insert(growing, {
  id = 563,
  type = 400004,
  level = 23,
  exp_origin = 101170,
  exp_target = 110000
})
table.insert(growing, {
  id = 564,
  type = 400004,
  level = 24,
  exp_origin = 110000,
  exp_target = 119200
})
table.insert(growing, {
  id = 565,
  type = 400004,
  level = 25,
  exp_origin = 119200,
  exp_target = 128770
})
table.insert(growing, {
  id = 566,
  type = 400004,
  level = 26,
  exp_origin = 128770,
  exp_target = 138700
})
table.insert(growing, {
  id = 567,
  type = 400004,
  level = 27,
  exp_origin = 138700,
  exp_target = 149000
})
table.insert(growing, {
  id = 568,
  type = 400004,
  level = 28,
  exp_origin = 149000,
  exp_target = 159670
})
table.insert(growing, {
  id = 569,
  type = 400004,
  level = 29,
  exp_origin = 159670,
  exp_target = 170710
})
table.insert(growing, {
  id = 570,
  type = 400004,
  level = 30,
  exp_origin = 170710,
  exp_target = 170710
})
table.insert(growing, {
  id = 571,
  type = 400005,
  level = 1,
  exp_origin = 0,
  exp_target = 740
})
table.insert(growing, {
  id = 572,
  type = 400005,
  level = 2,
  exp_origin = 740,
  exp_target = 1840
})
table.insert(growing, {
  id = 573,
  type = 400005,
  level = 3,
  exp_origin = 1840,
  exp_target = 3310
})
table.insert(growing, {
  id = 574,
  type = 400005,
  level = 4,
  exp_origin = 3310,
  exp_target = 5150
})
table.insert(growing, {
  id = 575,
  type = 400005,
  level = 5,
  exp_origin = 5150,
  exp_target = 7360
})
table.insert(growing, {
  id = 576,
  type = 400005,
  level = 6,
  exp_origin = 7360,
  exp_target = 9940
})
table.insert(growing, {
  id = 577,
  type = 400005,
  level = 7,
  exp_origin = 9940,
  exp_target = 12880
})
table.insert(growing, {
  id = 578,
  type = 400005,
  level = 8,
  exp_origin = 12880,
  exp_target = 16190
})
table.insert(growing, {
  id = 579,
  type = 400005,
  level = 9,
  exp_origin = 16190,
  exp_target = 19870
})
table.insert(growing, {
  id = 580,
  type = 400005,
  level = 10,
  exp_origin = 19870,
  exp_target = 23920
})
table.insert(growing, {
  id = 581,
  type = 400005,
  level = 11,
  exp_origin = 23920,
  exp_target = 28330
})
table.insert(growing, {
  id = 582,
  type = 400005,
  level = 12,
  exp_origin = 28330,
  exp_target = 33110
})
table.insert(growing, {
  id = 583,
  type = 400005,
  level = 13,
  exp_origin = 33110,
  exp_target = 38260
})
table.insert(growing, {
  id = 584,
  type = 400005,
  level = 14,
  exp_origin = 38260,
  exp_target = 43780
})
table.insert(growing, {
  id = 585,
  type = 400005,
  level = 15,
  exp_origin = 43780,
  exp_target = 49670
})
table.insert(growing, {
  id = 586,
  type = 400005,
  level = 16,
  exp_origin = 49670,
  exp_target = 55920
})
table.insert(growing, {
  id = 587,
  type = 400005,
  level = 17,
  exp_origin = 55920,
  exp_target = 62540
})
table.insert(growing, {
  id = 588,
  type = 400005,
  level = 18,
  exp_origin = 62540,
  exp_target = 69530
})
table.insert(growing, {
  id = 589,
  type = 400005,
  level = 19,
  exp_origin = 69530,
  exp_target = 76890
})
table.insert(growing, {
  id = 590,
  type = 400005,
  level = 20,
  exp_origin = 76890,
  exp_target = 84620
})
table.insert(growing, {
  id = 591,
  type = 400005,
  level = 21,
  exp_origin = 84620,
  exp_target = 92710
})
table.insert(growing, {
  id = 592,
  type = 400005,
  level = 22,
  exp_origin = 92710,
  exp_target = 101170
})
table.insert(growing, {
  id = 593,
  type = 400005,
  level = 23,
  exp_origin = 101170,
  exp_target = 110000
})
table.insert(growing, {
  id = 594,
  type = 400005,
  level = 24,
  exp_origin = 110000,
  exp_target = 119200
})
table.insert(growing, {
  id = 595,
  type = 400005,
  level = 25,
  exp_origin = 119200,
  exp_target = 128770
})
table.insert(growing, {
  id = 596,
  type = 400005,
  level = 26,
  exp_origin = 128770,
  exp_target = 138700
})
table.insert(growing, {
  id = 597,
  type = 400005,
  level = 27,
  exp_origin = 138700,
  exp_target = 149000
})
table.insert(growing, {
  id = 598,
  type = 400005,
  level = 28,
  exp_origin = 149000,
  exp_target = 159670
})
table.insert(growing, {
  id = 599,
  type = 400005,
  level = 29,
  exp_origin = 159670,
  exp_target = 170710
})
table.insert(growing, {
  id = 600,
  type = 400005,
  level = 30,
  exp_origin = 170710,
  exp_target = 170710
})
table.insert(growing, {
  id = 601,
  type = 500001,
  level = 1,
  exp_origin = 0,
  exp_target = 1050
})
table.insert(growing, {
  id = 602,
  type = 500001,
  level = 2,
  exp_origin = 1050,
  exp_target = 2620
})
table.insert(growing, {
  id = 603,
  type = 500001,
  level = 3,
  exp_origin = 2620,
  exp_target = 4710
})
table.insert(growing, {
  id = 604,
  type = 500001,
  level = 4,
  exp_origin = 4710,
  exp_target = 7330
})
table.insert(growing, {
  id = 605,
  type = 500001,
  level = 5,
  exp_origin = 7330,
  exp_target = 10470
})
table.insert(growing, {
  id = 606,
  type = 500001,
  level = 6,
  exp_origin = 10470,
  exp_target = 14140
})
table.insert(growing, {
  id = 607,
  type = 500001,
  level = 7,
  exp_origin = 14140,
  exp_target = 18330
})
table.insert(growing, {
  id = 608,
  type = 500001,
  level = 8,
  exp_origin = 18330,
  exp_target = 23040
})
table.insert(growing, {
  id = 609,
  type = 500001,
  level = 9,
  exp_origin = 23040,
  exp_target = 28280
})
table.insert(growing, {
  id = 610,
  type = 500001,
  level = 10,
  exp_origin = 28280,
  exp_target = 34040
})
table.insert(growing, {
  id = 611,
  type = 500001,
  level = 11,
  exp_origin = 34040,
  exp_target = 40320
})
table.insert(growing, {
  id = 612,
  type = 500001,
  level = 12,
  exp_origin = 40320,
  exp_target = 47130
})
table.insert(growing, {
  id = 613,
  type = 500001,
  level = 13,
  exp_origin = 47130,
  exp_target = 54460
})
table.insert(growing, {
  id = 614,
  type = 500001,
  level = 14,
  exp_origin = 54460,
  exp_target = 62320
})
table.insert(growing, {
  id = 615,
  type = 500001,
  level = 15,
  exp_origin = 62320,
  exp_target = 70700
})
table.insert(growing, {
  id = 616,
  type = 500001,
  level = 16,
  exp_origin = 70700,
  exp_target = 79600
})
table.insert(growing, {
  id = 617,
  type = 500001,
  level = 17,
  exp_origin = 79600,
  exp_target = 89030
})
table.insert(growing, {
  id = 618,
  type = 500001,
  level = 18,
  exp_origin = 89030,
  exp_target = 98980
})
table.insert(growing, {
  id = 619,
  type = 500001,
  level = 19,
  exp_origin = 98980,
  exp_target = 109450
})
table.insert(growing, {
  id = 620,
  type = 500001,
  level = 20,
  exp_origin = 109450,
  exp_target = 120450
})
table.insert(growing, {
  id = 621,
  type = 500001,
  level = 21,
  exp_origin = 120450,
  exp_target = 131970
})
table.insert(growing, {
  id = 622,
  type = 500001,
  level = 22,
  exp_origin = 131970,
  exp_target = 144020
})
table.insert(growing, {
  id = 623,
  type = 500001,
  level = 23,
  exp_origin = 144020,
  exp_target = 156590
})
table.insert(growing, {
  id = 624,
  type = 500001,
  level = 24,
  exp_origin = 156590,
  exp_target = 169680
})
table.insert(growing, {
  id = 625,
  type = 500001,
  level = 25,
  exp_origin = 169680,
  exp_target = 183300
})
table.insert(growing, {
  id = 626,
  type = 500001,
  level = 26,
  exp_origin = 183300,
  exp_target = 197440
})
table.insert(growing, {
  id = 627,
  type = 500001,
  level = 27,
  exp_origin = 197440,
  exp_target = 212100
})
table.insert(growing, {
  id = 628,
  type = 500001,
  level = 28,
  exp_origin = 212100,
  exp_target = 227290
})
table.insert(growing, {
  id = 629,
  type = 500001,
  level = 29,
  exp_origin = 227290,
  exp_target = 243000
})
table.insert(growing, {
  id = 630,
  type = 500001,
  level = 30,
  exp_origin = 243000,
  exp_target = 243000
})
table.insert(growing, {
  id = 631,
  type = 500002,
  level = 1,
  exp_origin = 0,
  exp_target = 1050
})
table.insert(growing, {
  id = 632,
  type = 500002,
  level = 2,
  exp_origin = 1050,
  exp_target = 2620
})
table.insert(growing, {
  id = 633,
  type = 500002,
  level = 3,
  exp_origin = 2620,
  exp_target = 4710
})
table.insert(growing, {
  id = 634,
  type = 500002,
  level = 4,
  exp_origin = 4710,
  exp_target = 7330
})
table.insert(growing, {
  id = 635,
  type = 500002,
  level = 5,
  exp_origin = 7330,
  exp_target = 10470
})
table.insert(growing, {
  id = 636,
  type = 500002,
  level = 6,
  exp_origin = 10470,
  exp_target = 14140
})
table.insert(growing, {
  id = 637,
  type = 500002,
  level = 7,
  exp_origin = 14140,
  exp_target = 18330
})
table.insert(growing, {
  id = 638,
  type = 500002,
  level = 8,
  exp_origin = 18330,
  exp_target = 23040
})
table.insert(growing, {
  id = 639,
  type = 500002,
  level = 9,
  exp_origin = 23040,
  exp_target = 28280
})
table.insert(growing, {
  id = 640,
  type = 500002,
  level = 10,
  exp_origin = 28280,
  exp_target = 34040
})
table.insert(growing, {
  id = 641,
  type = 500002,
  level = 11,
  exp_origin = 34040,
  exp_target = 40320
})
table.insert(growing, {
  id = 642,
  type = 500002,
  level = 12,
  exp_origin = 40320,
  exp_target = 47130
})
table.insert(growing, {
  id = 643,
  type = 500002,
  level = 13,
  exp_origin = 47130,
  exp_target = 54460
})
table.insert(growing, {
  id = 644,
  type = 500002,
  level = 14,
  exp_origin = 54460,
  exp_target = 62320
})
table.insert(growing, {
  id = 645,
  type = 500002,
  level = 15,
  exp_origin = 62320,
  exp_target = 70700
})
table.insert(growing, {
  id = 646,
  type = 500002,
  level = 16,
  exp_origin = 70700,
  exp_target = 79600
})
table.insert(growing, {
  id = 647,
  type = 500002,
  level = 17,
  exp_origin = 79600,
  exp_target = 89030
})
table.insert(growing, {
  id = 648,
  type = 500002,
  level = 18,
  exp_origin = 89030,
  exp_target = 98980
})
table.insert(growing, {
  id = 649,
  type = 500002,
  level = 19,
  exp_origin = 98980,
  exp_target = 109450
})
table.insert(growing, {
  id = 650,
  type = 500002,
  level = 20,
  exp_origin = 109450,
  exp_target = 120450
})
table.insert(growing, {
  id = 651,
  type = 500002,
  level = 21,
  exp_origin = 120450,
  exp_target = 131970
})
table.insert(growing, {
  id = 652,
  type = 500002,
  level = 22,
  exp_origin = 131970,
  exp_target = 144020
})
table.insert(growing, {
  id = 653,
  type = 500002,
  level = 23,
  exp_origin = 144020,
  exp_target = 156590
})
table.insert(growing, {
  id = 654,
  type = 500002,
  level = 24,
  exp_origin = 156590,
  exp_target = 169680
})
table.insert(growing, {
  id = 655,
  type = 500002,
  level = 25,
  exp_origin = 169680,
  exp_target = 183300
})
table.insert(growing, {
  id = 656,
  type = 500002,
  level = 26,
  exp_origin = 183300,
  exp_target = 197440
})
table.insert(growing, {
  id = 657,
  type = 500002,
  level = 27,
  exp_origin = 197440,
  exp_target = 212100
})
table.insert(growing, {
  id = 658,
  type = 500002,
  level = 28,
  exp_origin = 212100,
  exp_target = 227290
})
table.insert(growing, {
  id = 659,
  type = 500002,
  level = 29,
  exp_origin = 227290,
  exp_target = 243000
})
table.insert(growing, {
  id = 660,
  type = 500002,
  level = 30,
  exp_origin = 243000,
  exp_target = 243000
})
table.insert(growing, {
  id = 661,
  type = 500003,
  level = 1,
  exp_origin = 0,
  exp_target = 1050
})
table.insert(growing, {
  id = 662,
  type = 500003,
  level = 2,
  exp_origin = 1050,
  exp_target = 2620
})
table.insert(growing, {
  id = 663,
  type = 500003,
  level = 3,
  exp_origin = 2620,
  exp_target = 4710
})
table.insert(growing, {
  id = 664,
  type = 500003,
  level = 4,
  exp_origin = 4710,
  exp_target = 7330
})
table.insert(growing, {
  id = 665,
  type = 500003,
  level = 5,
  exp_origin = 7330,
  exp_target = 10470
})
table.insert(growing, {
  id = 666,
  type = 500003,
  level = 6,
  exp_origin = 10470,
  exp_target = 14140
})
table.insert(growing, {
  id = 667,
  type = 500003,
  level = 7,
  exp_origin = 14140,
  exp_target = 18330
})
table.insert(growing, {
  id = 668,
  type = 500003,
  level = 8,
  exp_origin = 18330,
  exp_target = 23040
})
table.insert(growing, {
  id = 669,
  type = 500003,
  level = 9,
  exp_origin = 23040,
  exp_target = 28280
})
table.insert(growing, {
  id = 670,
  type = 500003,
  level = 10,
  exp_origin = 28280,
  exp_target = 34040
})
table.insert(growing, {
  id = 671,
  type = 500003,
  level = 11,
  exp_origin = 34040,
  exp_target = 40320
})
table.insert(growing, {
  id = 672,
  type = 500003,
  level = 12,
  exp_origin = 40320,
  exp_target = 47130
})
table.insert(growing, {
  id = 673,
  type = 500003,
  level = 13,
  exp_origin = 47130,
  exp_target = 54460
})
table.insert(growing, {
  id = 674,
  type = 500003,
  level = 14,
  exp_origin = 54460,
  exp_target = 62320
})
table.insert(growing, {
  id = 675,
  type = 500003,
  level = 15,
  exp_origin = 62320,
  exp_target = 70700
})
table.insert(growing, {
  id = 676,
  type = 500003,
  level = 16,
  exp_origin = 70700,
  exp_target = 79600
})
table.insert(growing, {
  id = 677,
  type = 500003,
  level = 17,
  exp_origin = 79600,
  exp_target = 89030
})
table.insert(growing, {
  id = 678,
  type = 500003,
  level = 18,
  exp_origin = 89030,
  exp_target = 98980
})
table.insert(growing, {
  id = 679,
  type = 500003,
  level = 19,
  exp_origin = 98980,
  exp_target = 109450
})
table.insert(growing, {
  id = 680,
  type = 500003,
  level = 20,
  exp_origin = 109450,
  exp_target = 120450
})
table.insert(growing, {
  id = 681,
  type = 500003,
  level = 21,
  exp_origin = 120450,
  exp_target = 131970
})
table.insert(growing, {
  id = 682,
  type = 500003,
  level = 22,
  exp_origin = 131970,
  exp_target = 144020
})
table.insert(growing, {
  id = 683,
  type = 500003,
  level = 23,
  exp_origin = 144020,
  exp_target = 156590
})
table.insert(growing, {
  id = 684,
  type = 500003,
  level = 24,
  exp_origin = 156590,
  exp_target = 169680
})
table.insert(growing, {
  id = 685,
  type = 500003,
  level = 25,
  exp_origin = 169680,
  exp_target = 183300
})
table.insert(growing, {
  id = 686,
  type = 500003,
  level = 26,
  exp_origin = 183300,
  exp_target = 197440
})
table.insert(growing, {
  id = 687,
  type = 500003,
  level = 27,
  exp_origin = 197440,
  exp_target = 212100
})
table.insert(growing, {
  id = 688,
  type = 500003,
  level = 28,
  exp_origin = 212100,
  exp_target = 227290
})
table.insert(growing, {
  id = 689,
  type = 500003,
  level = 29,
  exp_origin = 227290,
  exp_target = 243000
})
table.insert(growing, {
  id = 690,
  type = 500003,
  level = 30,
  exp_origin = 243000,
  exp_target = 243000
})
table.insert(growing, {
  id = 691,
  type = 500004,
  level = 1,
  exp_origin = 0,
  exp_target = 1050
})
table.insert(growing, {
  id = 692,
  type = 500004,
  level = 2,
  exp_origin = 1050,
  exp_target = 2620
})
table.insert(growing, {
  id = 693,
  type = 500004,
  level = 3,
  exp_origin = 2620,
  exp_target = 4710
})
table.insert(growing, {
  id = 694,
  type = 500004,
  level = 4,
  exp_origin = 4710,
  exp_target = 7330
})
table.insert(growing, {
  id = 695,
  type = 500004,
  level = 5,
  exp_origin = 7330,
  exp_target = 10470
})
table.insert(growing, {
  id = 696,
  type = 500004,
  level = 6,
  exp_origin = 10470,
  exp_target = 14140
})
table.insert(growing, {
  id = 697,
  type = 500004,
  level = 7,
  exp_origin = 14140,
  exp_target = 18330
})
table.insert(growing, {
  id = 698,
  type = 500004,
  level = 8,
  exp_origin = 18330,
  exp_target = 23040
})
table.insert(growing, {
  id = 699,
  type = 500004,
  level = 9,
  exp_origin = 23040,
  exp_target = 28280
})
table.insert(growing, {
  id = 700,
  type = 500004,
  level = 10,
  exp_origin = 28280,
  exp_target = 34040
})
table.insert(growing, {
  id = 701,
  type = 500004,
  level = 11,
  exp_origin = 34040,
  exp_target = 40320
})
table.insert(growing, {
  id = 702,
  type = 500004,
  level = 12,
  exp_origin = 40320,
  exp_target = 47130
})
table.insert(growing, {
  id = 703,
  type = 500004,
  level = 13,
  exp_origin = 47130,
  exp_target = 54460
})
table.insert(growing, {
  id = 704,
  type = 500004,
  level = 14,
  exp_origin = 54460,
  exp_target = 62320
})
table.insert(growing, {
  id = 705,
  type = 500004,
  level = 15,
  exp_origin = 62320,
  exp_target = 70700
})
table.insert(growing, {
  id = 706,
  type = 500004,
  level = 16,
  exp_origin = 70700,
  exp_target = 79600
})
table.insert(growing, {
  id = 707,
  type = 500004,
  level = 17,
  exp_origin = 79600,
  exp_target = 89030
})
table.insert(growing, {
  id = 708,
  type = 500004,
  level = 18,
  exp_origin = 89030,
  exp_target = 98980
})
table.insert(growing, {
  id = 709,
  type = 500004,
  level = 19,
  exp_origin = 98980,
  exp_target = 109450
})
table.insert(growing, {
  id = 710,
  type = 500004,
  level = 20,
  exp_origin = 109450,
  exp_target = 120450
})
table.insert(growing, {
  id = 711,
  type = 500004,
  level = 21,
  exp_origin = 120450,
  exp_target = 131970
})
table.insert(growing, {
  id = 712,
  type = 500004,
  level = 22,
  exp_origin = 131970,
  exp_target = 144020
})
table.insert(growing, {
  id = 713,
  type = 500004,
  level = 23,
  exp_origin = 144020,
  exp_target = 156590
})
table.insert(growing, {
  id = 714,
  type = 500004,
  level = 24,
  exp_origin = 156590,
  exp_target = 169680
})
table.insert(growing, {
  id = 715,
  type = 500004,
  level = 25,
  exp_origin = 169680,
  exp_target = 183300
})
table.insert(growing, {
  id = 716,
  type = 500004,
  level = 26,
  exp_origin = 183300,
  exp_target = 197440
})
table.insert(growing, {
  id = 717,
  type = 500004,
  level = 27,
  exp_origin = 197440,
  exp_target = 212100
})
table.insert(growing, {
  id = 718,
  type = 500004,
  level = 28,
  exp_origin = 212100,
  exp_target = 227290
})
table.insert(growing, {
  id = 719,
  type = 500004,
  level = 29,
  exp_origin = 227290,
  exp_target = 243000
})
table.insert(growing, {
  id = 720,
  type = 500004,
  level = 30,
  exp_origin = 243000,
  exp_target = 243000
})
table.insert(growing, {
  id = 721,
  type = 500005,
  level = 1,
  exp_origin = 0,
  exp_target = 1050
})
table.insert(growing, {
  id = 722,
  type = 500005,
  level = 2,
  exp_origin = 1050,
  exp_target = 2620
})
table.insert(growing, {
  id = 723,
  type = 500005,
  level = 3,
  exp_origin = 2620,
  exp_target = 4710
})
table.insert(growing, {
  id = 724,
  type = 500005,
  level = 4,
  exp_origin = 4710,
  exp_target = 7330
})
table.insert(growing, {
  id = 725,
  type = 500005,
  level = 5,
  exp_origin = 7330,
  exp_target = 10470
})
table.insert(growing, {
  id = 726,
  type = 500005,
  level = 6,
  exp_origin = 10470,
  exp_target = 14140
})
table.insert(growing, {
  id = 727,
  type = 500005,
  level = 7,
  exp_origin = 14140,
  exp_target = 18330
})
table.insert(growing, {
  id = 728,
  type = 500005,
  level = 8,
  exp_origin = 18330,
  exp_target = 23040
})
table.insert(growing, {
  id = 729,
  type = 500005,
  level = 9,
  exp_origin = 23040,
  exp_target = 28280
})
table.insert(growing, {
  id = 730,
  type = 500005,
  level = 10,
  exp_origin = 28280,
  exp_target = 34040
})
table.insert(growing, {
  id = 731,
  type = 500005,
  level = 11,
  exp_origin = 34040,
  exp_target = 40320
})
table.insert(growing, {
  id = 732,
  type = 500005,
  level = 12,
  exp_origin = 40320,
  exp_target = 47130
})
table.insert(growing, {
  id = 733,
  type = 500005,
  level = 13,
  exp_origin = 47130,
  exp_target = 54460
})
table.insert(growing, {
  id = 734,
  type = 500005,
  level = 14,
  exp_origin = 54460,
  exp_target = 62320
})
table.insert(growing, {
  id = 735,
  type = 500005,
  level = 15,
  exp_origin = 62320,
  exp_target = 70700
})
table.insert(growing, {
  id = 736,
  type = 500005,
  level = 16,
  exp_origin = 70700,
  exp_target = 79600
})
table.insert(growing, {
  id = 737,
  type = 500005,
  level = 17,
  exp_origin = 79600,
  exp_target = 89030
})
table.insert(growing, {
  id = 738,
  type = 500005,
  level = 18,
  exp_origin = 89030,
  exp_target = 98980
})
table.insert(growing, {
  id = 739,
  type = 500005,
  level = 19,
  exp_origin = 98980,
  exp_target = 109450
})
table.insert(growing, {
  id = 740,
  type = 500005,
  level = 20,
  exp_origin = 109450,
  exp_target = 120450
})
table.insert(growing, {
  id = 741,
  type = 500005,
  level = 21,
  exp_origin = 120450,
  exp_target = 131970
})
table.insert(growing, {
  id = 742,
  type = 500005,
  level = 22,
  exp_origin = 131970,
  exp_target = 144020
})
table.insert(growing, {
  id = 743,
  type = 500005,
  level = 23,
  exp_origin = 144020,
  exp_target = 156590
})
table.insert(growing, {
  id = 744,
  type = 500005,
  level = 24,
  exp_origin = 156590,
  exp_target = 169680
})
table.insert(growing, {
  id = 745,
  type = 500005,
  level = 25,
  exp_origin = 169680,
  exp_target = 183300
})
table.insert(growing, {
  id = 746,
  type = 500005,
  level = 26,
  exp_origin = 183300,
  exp_target = 197440
})
table.insert(growing, {
  id = 747,
  type = 500005,
  level = 27,
  exp_origin = 197440,
  exp_target = 212100
})
table.insert(growing, {
  id = 748,
  type = 500005,
  level = 28,
  exp_origin = 212100,
  exp_target = 227290
})
table.insert(growing, {
  id = 749,
  type = 500005,
  level = 29,
  exp_origin = 227290,
  exp_target = 243000
})
table.insert(growing, {
  id = 750,
  type = 500005,
  level = 30,
  exp_origin = 243000,
  exp_target = 243000
})
