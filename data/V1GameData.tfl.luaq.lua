GameData = {}
GameData.vip_exp = {
  version = "001",
  vip = {},
  on = {},
  limit = {},
  chat = {},
  gift = {}
}
ext.dofile("V1vip_expvip.tfl")
ext.dofile("V1vip_expon.tfl")
ext.dofile("V1vip_explimit.tfl")
ext.dofile("V1vip_expchat.tfl")
ext.dofile("V1vip_expgift.tfl")
GameData.BuffEffect = {
  version = "001",
  Keys = {}
}
ext.dofile("V1BuffEffectKeys.tfl")
GameData.AllDialogues = {
  version = "001",
  Dialogues = {}
}
ext.dofile("V1AllDialoguesDialogues.tfl")
GameData.section = {
  version = "001",
  section = {}
}
ext.dofile("V1sectionsection.tfl")
GameData.chapterdata_act1 = {
  version = "001",
  ChapterInfos = {},
  Battles = {},
  ChapterInfos_170 = {},
  Battles_170 = {}
}
ext.dofile("V1chapterdata_act1ChapterInfos.tfl")
ext.dofile("V1chapterdata_act1Battles.tfl")
ext.dofile("V1chapterdata_act1ChapterInfos_170.tfl")
ext.dofile("V1chapterdata_act1Battles_170.tfl")
GameData.chapterdata_act2 = {
  version = "001",
  ChapterInfos = {},
  Battles = {},
  ChapterInfos_170 = {},
  Battles_170 = {}
}
ext.dofile("V1chapterdata_act2ChapterInfos.tfl")
ext.dofile("V1chapterdata_act2Battles.tfl")
ext.dofile("V1chapterdata_act2ChapterInfos_170.tfl")
ext.dofile("V1chapterdata_act2Battles_170.tfl")
GameData.chapterdata_act3 = {
  version = "001",
  ChapterInfos = {},
  Battles = {},
  ChapterInfos_170 = {},
  Battles_170 = {}
}
ext.dofile("V1chapterdata_act3ChapterInfos.tfl")
ext.dofile("V1chapterdata_act3Battles.tfl")
ext.dofile("V1chapterdata_act3ChapterInfos_170.tfl")
ext.dofile("V1chapterdata_act3Battles_170.tfl")
GameData.chapterdata_act4 = {
  version = "001",
  ChapterInfos = {},
  Battles = {},
  ChapterInfos_170 = {},
  Battles_170 = {}
}
ext.dofile("V1chapterdata_act4ChapterInfos.tfl")
ext.dofile("V1chapterdata_act4Battles.tfl")
ext.dofile("V1chapterdata_act4ChapterInfos_170.tfl")
ext.dofile("V1chapterdata_act4Battles_170.tfl")
GameData.chapterdata_act5 = {
  version = "001",
  ChapterInfos = {},
  Battles = {},
  ChapterInfos_170 = {},
  Battles_170 = {}
}
ext.dofile("V1chapterdata_act5ChapterInfos.tfl")
ext.dofile("V1chapterdata_act5Battles.tfl")
ext.dofile("V1chapterdata_act5ChapterInfos_170.tfl")
ext.dofile("V1chapterdata_act5Battles_170.tfl")
GameData.chapterdata_act6 = {
  version = "001",
  ChapterInfos = {},
  Battles = {},
  ChapterInfos_170 = {},
  Battles_170 = {}
}
ext.dofile("V1chapterdata_act6ChapterInfos.tfl")
ext.dofile("V1chapterdata_act6Battles.tfl")
ext.dofile("V1chapterdata_act6ChapterInfos_170.tfl")
ext.dofile("V1chapterdata_act6Battles_170.tfl")
GameData.chapterdata_act7 = {
  version = "001",
  ChapterInfos = {},
  Battles = {},
  ChapterInfos_170 = {},
  Battles_170 = {}
}
ext.dofile("V1chapterdata_act7ChapterInfos.tfl")
ext.dofile("V1chapterdata_act7Battles.tfl")
ext.dofile("V1chapterdata_act7ChapterInfos_170.tfl")
ext.dofile("V1chapterdata_act7Battles_170.tfl")
GameData.chapterdata_act8 = {
  version = "001",
  ChapterInfos = {},
  Battles = {},
  ChapterInfos_170 = {},
  Battles_170 = {}
}
ext.dofile("V1chapterdata_act8ChapterInfos.tfl")
ext.dofile("V1chapterdata_act8Battles.tfl")
ext.dofile("V1chapterdata_act8ChapterInfos_170.tfl")
ext.dofile("V1chapterdata_act8Battles_170.tfl")
GameData.chapterdata_act9 = {
  version = "001",
  ChapterInfos = {},
  Battles = {},
  ChapterInfos_170 = {},
  Battles_170 = {}
}
ext.dofile("V1chapterdata_act9ChapterInfos.tfl")
ext.dofile("V1chapterdata_act9Battles.tfl")
ext.dofile("V1chapterdata_act9ChapterInfos_170.tfl")
ext.dofile("V1chapterdata_act9Battles_170.tfl")
GameData.chapterdata_act10 = {
  version = "001",
  ChapterInfos = {},
  Battles = {},
  ChapterInfos_170 = {},
  Battles_170 = {}
}
ext.dofile("V1chapterdata_act10ChapterInfos.tfl")
ext.dofile("V1chapterdata_act10Battles.tfl")
ext.dofile("V1chapterdata_act10ChapterInfos_170.tfl")
ext.dofile("V1chapterdata_act10Battles_170.tfl")
GameData.chapterdata_act60 = {
  version = "001",
  ChapterInfos = {},
  Battles = {},
  ChapterInfos_170 = {},
  Battles_170 = {}
}
ext.dofile("V1chapterdata_act60ChapterInfos.tfl")
ext.dofile("V1chapterdata_act60Battles.tfl")
ext.dofile("V1chapterdata_act60ChapterInfos_170.tfl")
ext.dofile("V1chapterdata_act60Battles_170.tfl")
GameData.FleetBaseInfo = {
  version = "001",
  Fleets = {}
}
ext.dofile("V1FleetBaseInfoFleets.tfl")
GameData.Technologies = {
  version = "001",
  Technologies = {},
  consume = {}
}
ext.dofile("V1TechnologiesTechnologies.tfl")
ext.dofile("V1Technologiesconsume.tfl")
GameData.Equip = {
  version = "001",
  Keys = {}
}
ext.dofile("V1EquipKeys.tfl")
GameData.Item = {
  version = "001",
  Keys = {}
}
ext.dofile("V1ItemKeys.tfl")
GameData.Spell = {
  version = "001",
  Keys = {},
  Spell = {}
}
ext.dofile("V1SpellKeys.tfl")
ext.dofile("V1SpellSpell.tfl")
GameData.ChapterMonsterAct1 = {
  version = "001",
  Monsters = {}
}
ext.dofile("V1ChapterMonsterAct1Monsters.tfl")
GameData.ChapterMonsterAct2 = {
  version = "001",
  Monsters = {}
}
ext.dofile("V1ChapterMonsterAct2Monsters.tfl")
GameData.ChapterMonsterAct3 = {
  version = "001",
  Monsters = {}
}
ext.dofile("V1ChapterMonsterAct3Monsters.tfl")
GameData.ChapterMonsterAct4 = {
  version = "001",
  Monsters = {}
}
ext.dofile("V1ChapterMonsterAct4Monsters.tfl")
GameData.ChapterMonsterAct5 = {
  version = "001",
  Monsters = {}
}
ext.dofile("V1ChapterMonsterAct5Monsters.tfl")
GameData.ChapterMonsterAct6 = {
  version = "001",
  Monsters = {}
}
ext.dofile("V1ChapterMonsterAct6Monsters.tfl")
GameData.ChapterMonsterAct7 = {
  version = "001",
  Monsters = {}
}
ext.dofile("V1ChapterMonsterAct7Monsters.tfl")
GameData.ChapterMonsterAct8 = {
  version = "001",
  Monsters = {}
}
ext.dofile("V1ChapterMonsterAct8Monsters.tfl")
GameData.ChapterMonsterAct9 = {
  version = "001",
  Monsters = {}
}
ext.dofile("V1ChapterMonsterAct9Monsters.tfl")
GameData.ChapterMonsterAct10 = {
  version = "001",
  Monsters = {},
  Monsters_170 = {}
}
ext.dofile("V1ChapterMonsterAct10Monsters.tfl")
ext.dofile("V1ChapterMonsterAct10Monsters_170.tfl")
GameData.ChapterMonsterAct60 = {
  version = "001",
  Monsters = {}
}
ext.dofile("V1ChapterMonsterAct60Monsters.tfl")
GameData.adventure = {
  version = "001",
  adventure = {},
  adventure_170 = {},
  search_170 = {},
  rest_170 = {}
}
ext.dofile("V1adventureadventure.tfl")
ext.dofile("V1adventureadventure_170.tfl")
ext.dofile("V1adventuresearch_170.tfl")
ext.dofile("V1adventurerest_170.tfl")
GameData.champion_award = {
  version = "001",
  reward = {},
  basic_reward = {},
  rank_extra_reward = {}
}
ext.dofile("V1champion_awardreward.tfl")
ext.dofile("V1champion_awardbasic_reward.tfl")
ext.dofile("V1champion_awardrank_extra_reward.tfl")
GameData.adventure_monster = {
  version = "001",
  info = {},
  info_170 = {}
}
ext.dofile("V1adventure_monsterinfo.tfl")
ext.dofile("V1adventure_monsterinfo_170.tfl")
GameData.Prestige = {
  version = "001",
  Data = {}
}
ext.dofile("V1PrestigeData.tfl")
GameData.achievement = {
  version = "001",
  Data = {},
  Classify = {}
}
ext.dofile("V1achievementData.tfl")
ext.dofile("V1achievementClassify.tfl")
GameData.krypton_slot = {
  version = "001",
  slotLevel = {}
}
ext.dofile("V1krypton_slotslotLevel.tfl")
GameData.Krypton = {
  version = "001",
  KRYPTON = {},
  KRYPTONADD = {}
}
ext.dofile("V1KryptonKRYPTON.tfl")
ext.dofile("V1KryptonKRYPTONADD.tfl")
GameData.RightMenu = {
  version = "001",
  rightMenu = {},
  rightMenu_170 = {}
}
ext.dofile("V1RightMenurightMenu.tfl")
ext.dofile("V1RightMenurightMenu_170.tfl")
GameData.ChapterMonsterTutorial = {
  version = "001",
  Monsters = {}
}
ext.dofile("V1ChapterMonsterTutorialMonsters.tfl")
GameData.equip_enhance = {
  version = "001",
  basic = {},
  consume = {},
  evolution = {}
}
ext.dofile("V1equip_enhancebasic.tfl")
ext.dofile("V1equip_enhanceconsume.tfl")
ext.dofile("V1equip_enhanceevolution.tfl")
GameData.heros = {
  version = "001",
  basic = {},
  growing = {},
  Ability = {},
  enhance = {}
}
ext.dofile("V1herosbasic.tfl")
ext.dofile("V1herosgrowing.tfl")
ext.dofile("V1herosAbility.tfl")
ext.dofile("V1herosenhance.tfl")
GameData.showhero = {
  version = "001",
  heros = {},
  Monsters = {}
}
ext.dofile("V1showheroheros.tfl")
ext.dofile("V1showheroMonsters.tfl")
GameData.help_info = {
  version = "001",
  Data = {}
}
ext.dofile("V1help_infoData.tfl")
GameData.censor_words = {
  version = "001",
  Censor = {}
}
ext.dofile("V1censor_wordsCensor.tfl")
GameData.Dexter_laba = {
  version = "001",
  LABA_TYPE = {},
  LABA_REWARD = {},
  LABA_RATE = {},
  LABA_LEVEL = {}
}
ext.dofile("V1Dexter_labaLABA_TYPE.tfl")
ext.dofile("V1Dexter_labaLABA_REWARD.tfl")
ext.dofile("V1Dexter_labaLABA_RATE.tfl")
ext.dofile("V1Dexter_labaLABA_LEVEL.tfl")
GameData.Dexter_ac = {
  version = "001",
  enhance = {},
  battlesInfo = {},
  monsters = {}
}
ext.dofile("V1Dexter_acenhance.tfl")
ext.dofile("V1Dexter_acbattlesInfo.tfl")
ext.dofile("V1Dexter_acmonsters.tfl")
GameData.alliance = {
  version = "001",
  exp = {},
  exp = {},
  cost = {},
  activity = {},
  award = {},
  award = {},
  base_award = {},
  percent_award = {},
  repair = {},
  flag = {},
  id = {}
}
ext.dofile("V1allianceexp.tfl")
ext.dofile("V1allianceexp.tfl")
ext.dofile("V1alliancecost.tfl")
ext.dofile("V1allianceactivity.tfl")
ext.dofile("V1allianceaward.tfl")
ext.dofile("V1allianceaward.tfl")
ext.dofile("V1alliancebase_award.tfl")
ext.dofile("V1alliancepercent_award.tfl")
ext.dofile("V1alliancerepair.tfl")
ext.dofile("V1allianceflag.tfl")
ext.dofile("V1allianceid.tfl")
GameData.world_boss = {
  version = "001",
  id = {},
  boss_info = {},
  reward = {},
  boost = {}
}
ext.dofile("V1world_bossid.tfl")
ext.dofile("V1world_bossboss_info.tfl")
ext.dofile("V1world_bossreward.tfl")
ext.dofile("V1world_bossboost.tfl")
GameData.effectImage = {
  version = "001",
  effectImageList = {},
  spell = {},
  buff = {},
  avtar = {},
  ship = {}
}
ext.dofile("V1effectImageeffectImageList.tfl")
ext.dofile("V1effectImagespell.tfl")
ext.dofile("V1effectImagebuff.tfl")
ext.dofile("V1effectImageavtar.tfl")
ext.dofile("V1effectImageship.tfl")
GameData.Ladder = {
  version = "001",
  Step = {},
  normalStep = {},
  Monster = {},
  ISO = {},
  raids = {}
}
ext.dofile("V1LadderStep.tfl")
ext.dofile("V1LaddernormalStep.tfl")
ext.dofile("V1LadderMonster.tfl")
ext.dofile("V1LadderISO.tfl")
ext.dofile("V1Ladderraids.tfl")
GameData.wormhole = {
  version = "001",
  Step = {},
  Monster = {},
  ISO = {}
}
ext.dofile("V1wormholeStep.tfl")
ext.dofile("V1wormholeMonster.tfl")
ext.dofile("V1wormholeISO.tfl")
GameData.enchant = {
  version = "001",
  enchant = {},
  addition = {},
  description = {},
  enchant170 = {}
}
ext.dofile("V1enchantenchant.tfl")
ext.dofile("V1enchantaddition.tfl")
ext.dofile("V1enchantenchant170.tfl")
GameData.adjutant_skill = {
  version = "001",
  adjutant_skill = {}
}
ext.dofile("V1adjutant_skilladjutant_skill.tfl")
GameData.wve_boss = {
  version = "001",
  boss_info = {}
}
ext.dofile("V1wve_bossboss_info.tfl")
GameData.medal_boss = {
  version = "001",
  config = {},
  monster = {},
  price = {},
  Step = {},
  Monster = {},
  ISO = {}
}
ext.dofile("V1medal_bossconfig.tfl")
ext.dofile("V1medal_bossmonster.tfl")
ext.dofile("V1medal_bossprice.tfl")
ext.dofile("V1medal_bossStep.tfl")
ext.dofile("V1medal_bossMonster.tfl")
ext.dofile("V1medal_bossISO.tfl")
GameData.medal_enhance = {
  version = "001",
  basic = {},
  growing = {},
  enhance = {},
  material = {}
}
ext.dofile("V1medal_enhancebasic.tfl")
ext.dofile("V1medal_enhancegrowing.tfl")
ext.dofile("V1medal_enhanceenhance.tfl")
ext.dofile("V1medal_enhancematerial.tfl")
