local GameUIDice = LuaObjectManager:GetLuaObject("GameUIDice")
local GameStateDice = GameStateManager.GameStateDice
function GameStateDice:InitGameState()
end
function GameStateDice:OnFocusGain(previousState)
  self.m_preState = previousState
  self:AddObject(GameUIDice)
end
function GameStateDice:OnFocusLost(newState)
  self.m_preState = nil
  self:EraseObject(GameUIDice)
end
function GameStateDice:GetPreState()
  return self.m_preState
end
