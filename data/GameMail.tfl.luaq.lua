local GameMail = LuaObjectManager:GetLuaObject("GameMail")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
local GameUIUserAlliance = LuaObjectManager:GetLuaObject("GameUIUserAlliance")
GameMail.MAILBOX_ONLINE_TAB = 1
GameMail.MAILBOX_LOCAL_TAB = 2
GameMail.MAILBOX_WRITE_TAB = 3
GameMail.countPrePage = 21
GameMail.ICON_HAD_READED = 1
GameMail.ICON_NOT_READ = 2
GameMail.ICON_NOTIFACTION = 3
GameMail.ICON_LOCAL = 5
GameMail.MAX_LOCAL_MAIL = 100
GameMail.ICON_ADMIN_READED = 6
GameMail.ICON_ADMIN_READ = 7
GameMail.ICON_ADMIN_LOCAL = 8
GameMail.m_currentSaveVer = "1.0"
GameMail.menuCount = 0
GameMail.unread = 0
GameMail.titleText = ""
GameMail.contentText = ""
GameMail.currentSelectMail = {}
GameMail.goodsIcon = {}
GameMail.goodsNameText = {}
GameMail.curEquip = {}
GameMail.mIsTranslateOpen = false
GameMail.sendSuffix = ""
GameMail.mShareStoryChecked = {}
GameMail.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_MAIL] = true
local sendServerPrice = 0
local currentMailType = "player"
function GameMail:OnAddToGameState(game_state)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  local allianceInfo = GameGlobalData:GetData("alliance")
  DebugOutPutTable(allianceInfo, "allianceInfo")
  local out = string.format("%q", "File:data2/136_E703lansiluote_.jpg")
  DebugOut("out", out)
  if allianceInfo ~= nil and allianceInfo.id ~= "" then
    DebugOut("allianceInfo is ok")
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setNoAlliance", false)
  else
    DebugOut("no allianceInfo")
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setNoAlliance", true)
  end
  if sendServerPrice ~= 0 then
    GameMail:GetFlashObject():InvokeASCallback("_root", "lua2fs_setSendBtnPrice", sendServerPrice)
  end
  local flashObj = GameMail:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetTranslateBtnVisible", GameMail.mIsTranslateOpen)
  end
end
function GameMail:OnEraseFromGameState(game_state)
  self:UnloadFlashObject()
  GameMail.isOnReadMail = nil
  currentMailType = "player"
end
function GameMail.parseSaveData()
  local r = {}
  if GameUserMailData.RECORD_VERSION == GameMail.m_currentSaveVer then
    r = GameUserMailData
    r.RECORD_VERSION = nil
  else
    assert(false, "GameUserMailData version is not support!")
  end
  for k, v in pairs(r) do
    if type(v) == "table" then
      v = LuaUtils:table_values(v)
      table.sort(v, GameMail.compMail)
      r[k] = v
    else
      r[k] = nil
    end
  end
  return r
end
function GameMail.compMail(v1, v2)
  if v1 and v2 and tonumber(v1.time) <= tonumber(v2.time) then
    return false
  end
  return true
end
function GameMail.executeSave()
  local r = {}
  local save = LuaUtils:table_copy(GameUserMailData)
  for k, v in pairs(save) do
    if type(v) ~= "table" then
      save[k] = nil
    end
  end
  save.RECORD_VERSION = GameMail.m_currentSaveVer
  ext.SaveGameData("MAIL.tfl", GameUtils:formatSaveString(save, "GameUserMailData"))
end
function GameMail.insertSaveMail()
  local mail = {}
  mail.status = 0
  mail.sender_role = GameMail.currentSelectMail.sender_role
  mail.id = GameMail.currentMailContent.id
  mail.time = GameMail.currentMailContent.time
  mail.title = GameMail.currentMailContent.title
  mail.get_goods = GameMail:FindMailByID(GameMail.currentMailContent.id).get_goods
  mail.sender_name = GameMail.currentMailContent.sender_name
  mail.content = GameMail.currentMailContent.content
  mail.goods = GameMail:FindMailByID(GameMail.currentMailContent.id).goods
  DebugOut("insertSaveMail")
  DebugTable(mail)
  local curSave = GameMail.getCurrentUserSaveMail()
  if #curSave > GameMail.MAX_LOCAL_MAIL then
    table.remove(curSave)
  end
  table.insert(curSave, mail)
  table.sort(curSave, GameMail.compMail)
  GameMail.executeSave()
end
function GameMail.getCurrentUserSaveMail()
  local player_id = GameGlobalData:GetUserInfo().player_id
  GameUserMailData[player_id] = GameUserMailData[player_id] or {}
  return GameUserMailData[player_id]
end
function GameMail.setCurrentUserSaveMail(t)
  local player_id = GameGlobalData:GetUserInfo().player_id
  if not player_id then
    return
  end
  GameUserMailData[player_id] = t
end
function GameMail:Update(dt)
  self:GetFlashObject():InvokeASCallback("_root.contents", "onUpdateList")
  self:GetFlashObject():InvokeASCallback("_root", "UpdateRead")
  self:GetFlashObject():InvokeASCallback("_root", "UpdateReadAttach")
  self:GetFlashObject():Update(dt)
  self:GetFlashObject():InvokeASCallback("_root", "OnUpdate")
end
function GameMail:ShowBox(tabIndex)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self.menuCount = self.menuCount + 1
  self:GetFlashObject():InvokeASCallback("_root", "ShowBox", tabIndex)
end
function GameMail:HideBox()
  self.menuCount = self.menuCount - 1
  self:GetFlashObject():InvokeASCallback("_root", "HideBox")
end
function GameMail:ShowRead(canReply, canSave, canDel, isAttach, isGet)
  DebugOut("canReply, canSave, canDel, isAttach, isGet", canReply, canSave, canDel, isAttach, isGet)
  GameMail.isOnReadMail = true
  self.menuCount = self.menuCount + 1
  local shareStoryEnabled = false
  local shareSupport = Facebook:CanShareStory()
  local curMail = GameMail:FindMailByID(GameMail.currentMailContent.id)
  if FacebookGraphStorySharer:IsStoryEnabled(FacebookGraphStorySharer.StoryType.STORY_TYPE_MAIL) then
    if curMail.can_send_story then
      shareStoryEnabled = true
    end
  else
    shareSupport = false
  end
  self:GetFlashObject():InvokeASCallback("_root", "ShowReadMail", canReply, canSave, canDel, isAttach, isGet, shareStoryEnabled, shareSupport)
end
function GameMail:SetRead(from, title, content, time, mail_type, hasGoods)
  local mailDisplayTitle = title ~= "" and title or GameLoader:GetGameText("LC_MENU_MAIL_NO_SUBJECT_CHAR")
  self:GetFlashObject():InvokeASCallback("_root", "SetReadMail", from, mailDisplayTitle, content, time, mail_type, hasGoods)
end
function GameMail:HideRead()
  self.isOnReadMail = nil
  self.menuCount = self.menuCount - 1
  self:GetFlashObject():InvokeASCallback("_root", "HideReadMail")
end
function GameMail:ShowWrite()
  self:GetFlashObject():InvokeASCallback("_root", "ShowWriteMail")
end
function GameMail:SetWrite(to, title, content)
  to = to or ""
  title = title or ""
  content = content or ""
  DebugOut("GameMail:SetWrite: ", to, title, content)
  self:GetFlashObject():InvokeASCallback("_root", "SetWriteMail", to, title, content)
end
function GameMail:HideWrite()
  self:GetFlashObject():InvokeASCallback("_root", "HideWriteMail")
  self.titleText = ""
  self.contentText = ""
end
function GameMail:deleteLocalMail()
  local index = (self.currentPage - 1) * (self.countPrePage - 1) + 1
  local localmails = GameMail.getCurrentUserSaveMail()
  DebugOutPutTable(self.deleteIds, "deleteIds")
  for k, v in pairs(self.deleteIds) do
    if localmails[index + tonumber(v) - 1] then
      localmails[index + tonumber(v) - 1] = nil
    end
    for kMail, vMail in pairs(localmails) do
      if vMail.id and vMail.id == v then
        localmails[kMail] = nil
      end
    end
  end
  GameMail.setCurrentUserSaveMail({})
  local remaindata = {}
  DebugOutPutTable(localmails, "localmails")
  for _, v in pairs(localmails) do
    table.insert(remaindata, v)
  end
  DebugOutPutTable(remaindata, "remaindata")
  GameMail.setCurrentUserSaveMail(remaindata)
  GameMail.executeSave()
  self.maxMail = #remaindata
  GameMail:CalcPageAfterDelete()
  self.GetLocalMail(self.currentPage)
end
function GameMail:CalcPageAfterDelete()
  index = (self.currentPage - 1) * (self.countPrePage - 1) + 1
  if index > self.maxMail then
    self.currentPage = self.currentPage - 1
    self.currentPage = math.max(1, self.currentPage)
  end
end
function GameMail:UpdateAttachItem(id)
  local itemKey = tonumber(id)
  DebugTable(GameMail.goodsIcon)
  DebugTable(GameMail.goodsNameText)
  GameMail:GetFlashObject():InvokeASCallback("_root", "setAttachListItem", itemKey, GameMail.goodsIcon[itemKey], GameMail.goodsNameText[itemKey])
end
function GameMail:ShowItemDetil(item_, item_type)
  if GameHelper:IsResource(item_type) then
    return
  end
  local item = item_
  if item_type == "krypton" then
    if item.level == 0 then
      item.level = 1
    end
    local detail = GameUIKrypton:TryQueryKryptonDetail(item.number, item.level, function(msgType, content)
      local ret = GameUIKrypton.QueryKryptonDetailCallBack(msgType, content)
      if ret then
        local tmp = GameGlobalData:GetKryptonDetail(item.number, item.level)
        if tmp == nil then
          return false
        end
        ItemBox:SetKryptonBox(tmp, item.number)
        local operationTable = {}
        operationTable.btnUnloadVisible = false
        operationTable.btnUpgradeVisible = false
        operationTable.btnUseVisible = false
        operationTable.btnDecomposeVisible = false
        ItemBox:ShowKryptonBox(320, 240, operationTable)
      end
      return ret
    end)
    if detail == nil then
    else
      ItemBox:SetKryptonBox(detail, item.number)
      local operationTable = {}
      operationTable.btnUnloadVisible = false
      operationTable.btnUpgradeVisible = false
      operationTable.btnUseVisible = false
      operationTable.btnDecomposeVisible = false
      ItemBox:ShowKryptonBox(320, 240, operationTable)
    end
  end
  if item_type == "item" then
    item.cnt = GameHelper:GetAwardCount(item.item_type, item.number, item.no)
    ItemBox:showItemBox("Item", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
  end
  if item_type == "fleet" then
    ItemBox:ShowCommanderDetail2(tonumber(item.number))
  end
  if item_type == "equip" then
    GameMail.curEquip = item
    local equipID = {
      id = item.number
    }
    DebugTable(equipID)
    NetMessageMgr:SendMsg(NetAPIList.equip_info_req.Code, equipID, self.ShowEquip, true, nil)
  end
end
function GameMail.ShowEquip(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.equip_info_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.equip_info_ack.Code then
    table.insert(GameGlobalData.GlobalData.equipments, content.equip)
    ItemBox:showItemBox("Equip", content.equip, tonumber(content.equip.equip_type), 320, 480, 200, 200, "", nil, "", nil)
    return true
  end
  return false
end
function GameMail.ReceiveMailGoods(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.mail_goods_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.mail_goods_ack.Code then
    for k, v in pairs(GameMail.mails or {}) do
      if tonumber(v.id) == tonumber(content.new_info.id) then
        GameMail.mails[k] = content.new_info
      end
    end
    GameMail:GetFlashObject():InvokeASCallback("_root", "ShowReceived")
    GameMail.unread = GameMail.unread - 1
    GameMail:RefreshMail()
    DebugTable(GameMail.mails)
    GameTip:Show(GameLoader:GetGameText("LC_MENU_MAIL_RECIEVE_SUCCESS_ALERT"))
    return true
  end
  return false
end
function GameMail:HaveAttach()
  for k, v in pairs(GameMail.mails) do
    if not LuaUtils:table_empty(v.goods) then
      return true
    end
  end
  return false
end
function GameMail:OnFSCommand(cmd, arg)
  GameTextEdit:fscommand(self:GetFlashObject(), cmd, arg)
  if cmd == "Receive" then
    local mailID = {
      id = tostring(GameMail.currentMailContent.id)
    }
    DebugTable(mailID)
    NetMessageMgr:SendMsg(NetAPIList.mail_goods_req.Code, mailID, self.ReceiveMailGoods, true, nil)
  end
  if cmd == "Received" then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_MAIL_RECIEVED_ALERT"))
  end
  if cmd == "ShareMail" then
    local countryStr = ext.GetDeviceCountry and ext.GetDeviceCountry() or "unknow"
    if self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_MAIL] then
      local extraInfo = {}
      if GameMail.currentMailContent.content and GameMail.currentMailContent.content then
        extraInfo.title = GameUtils:TrimHtmlStr(GameMail.currentMailContent.title)
        extraInfo.mailText = GameUtils:TrimHtmlStr(GameMail.currentMailContent.content)
        extraInfo.mailID = GameMail.currentMailContent.id
      end
      FacebookGraphStorySharer:ShareStory(FacebookGraphStorySharer.StoryType.STORY_TYPE_MAIL, extraInfo)
      local curMail = GameMail:FindMailByID(GameMail.currentMailContent.id)
      curMail.can_send_story = false
      if self:GetFlashObject() then
        self:GetFlashObject():InvokeASCallback("_root", "SetReadMailFacebookState", false)
      end
    else
    end
    return
  end
  if cmd == "AttachItemReleased" then
    local itemKey = tonumber(arg)
    local curMail = GameMail:FindMailByID(GameMail.currentMailContent.id)
    local item = {}
    DebugTable(curMail.goods)
    for k, v in pairs(curMail.goods) do
      if k == itemKey then
        item = v
        break
      end
    end
    DebugTable(item)
    GameMail:ShowItemDetil(item, item.item_type)
  end
  if cmd == "NeedUpdateAttachItem" then
    GameMail:UpdateAttachItem(tonumber(arg))
  end
  if cmd == "closeBoxReleased" then
    self:HideBox()
  elseif cmd == "closeReadReleased" then
    self:RefreshMail()
    self:HideRead()
  elseif cmd == "closeWriteReleased" then
    self:HideWrite()
  end
  if cmd == "closedMenu" then
    DebugOut("self.menuCount:", self.menuCount)
    if self.menuCount == 0 then
      self.currentBox = -1
      self.currentPage = -1
      GameStateManager:GetCurrentGameState():EraseObject(self)
    end
  end
  if cmd == "markReleased" then
    self.markIds = {}
    for k, v in pairs(self.mails) do
      if v.status ~= 0 then
        table.insert(self.markIds, v.id)
      end
    end
    local mail_set_read_req_param = {
      ids = self.markIds
    }
    NetMessageMgr:SendMsg(NetAPIList.mail_set_read_req.Code, mail_set_read_req_param, self.markMailCallback, true, nil)
  end
  if cmd == "deleteAllMail" then
    self.deleteIds = {}
    self.deleteNoAttachID = {}
    DebugTable(self.mails)
    for k, v in pairs(self.mails or {}) do
      local key = v.id or k
      DebugOut("deleteAllMail key is :", key)
      if LuaUtils:table_empty(v.goods) then
        table.insert(self.deleteNoAttachID, key)
      end
      table.insert(self.deleteIds, key)
    end
    if LuaUtils:table_size(self.deleteIds) == 0 then
      return
    end
    DebugTable(self.deleteIds)
    DebugTable(self.deleteNoAttachID)
    if GameMail:HaveAttach() == false then
      GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
      GameUIMessageDialog:SetYesButton(GameMail.deleteAllMail)
      GameUIMessageDialog:SetNoButton(GameMail.HideCheckBox)
      GameUIMessageDialog:Display(GameLoader:GetGameText("LC_MENU_MAIL_DELETE_TITLE"), GameLoader:GetGameText("LC_MENU_MAIL_DELETE"))
    else
      GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
      GameUIMessageDialog:SetYesButton(GameMail.deleteAllMail)
      GameUIMessageDialog:SetNoButton(GameMail.HideCheckBox)
      GameUIMessageDialog:Display(GameLoader:GetGameText("LC_MENU_MAIL_DELETE_TITLE"), GameLoader:GetGameText("LC_MENU_MAIL_DELETE"))
      GameUIMessageDialog:ShowCheckBox()
    end
  end
  if cmd == "MailItemReleased" then
    do
      local key = arg
      self.deleteIds = {}
      table.insert(self.deleteIds, key)
      if self.currentBox == self.MAILBOX_ONLINE_TAB then
        curMail = LuaUtils:table_values(LuaUtils:table_filter(self.mails, function(k, v)
          return v.id == key
        end))[1]
        if curMail and curMail.status ~= 0 and (not curMail.get_goods or #curMail.goods == 0) then
          self.unread = self.unread - 1
          curMail.status = 0
          self:RefreshMail()
        end
        if curMail then
          GameMail.currentSelectMail.sender_role = curMail.sender_role
          local mail_content_req_param = {id = key}
          NetMessageMgr:SendMsg(NetAPIList.mail_content_req.Code, mail_content_req_param, self.readMailCallback, true, nil)
        end
      elseif self.currentBox == self.MAILBOX_LOCAL_TAB then
        local content = GameMail:GetLocalMailByIdOrKey(key)
        local time = GameUtils:formatTimeStringAsAgoStyle(content.time)
        local displayName = GameUtils:GetUserDisplayName(content.sender_name)
        GameMail.currentMailContent = content
        GameMail.currentSelectMail.sender_role = content.sender_role
        local mail_type = content.sender_role
        mail_type = mail_type or 0
        GameMail:GetFlashObject():InvokeASCallback("_root", "ClearAttachListItem")
        GameMail:GetFlashObject():InvokeASCallback("_root", "InitAttachListItem")
        DebugOut("curMail")
        DebugTable(content)
        GameMail.goodsIcon = {}
        GameMail.goodsNameText = {}
        for k, v in pairs(content.goods) do
          local icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v, nil, GameMail.DynamicResDowloadFinished)
          local nameText = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
          nameText = GameUtils.numberConversion(tonumber(nameText))
          nameText = "x" .. nameText
          table.insert(GameMail.goodsIcon, icon)
          table.insert(GameMail.goodsNameText, nameText)
        end
        for k, v in pairs(content.goods) do
          GameMail:GetFlashObject():InvokeASCallback("_root", "AddAttachListItem", k)
        end
        GameMail:GetFlashObject():InvokeASCallback("_root", "SetAttachArrowVisible")
        if LuaUtils:table_empty(GameMail.goodsIcon) then
          if content.get_goods == 0 then
            GameMail:ShowRead(true, true, true, false, true)
          else
            GameMail:ShowRead(true, true, true, false, false)
          end
        elseif content.get_goods == 0 then
          GameMail:ShowRead(true, true, true, true, true)
        else
          GameMail:ShowRead(true, true, true, true, false)
        end
        GameMail:SetRead(displayName, content.title, content.content, time, mail_type, not LuaUtils:table_empty(GameMail.goodsIcon))
      end
    end
  end
  if cmd == "needUpdateItem" then
    do
      local key = arg
      if tonumber(key) == -1 then
        local pageCount = math.ceil((self.maxMail + 1) / self.countPrePage)
        local pageText = self.currentPage .. "/" .. pageCount
        self:GetFlashObject():InvokeASCallback("_root.contents", "setPageControl", pageText)
      else
        local curMail = {}
        if self.currentBox == self.MAILBOX_ONLINE_TAB then
          curMail = LuaUtils:table_values(LuaUtils:table_filter(self.mails, function(k, v)
            return v.id == key
          end))[1]
        elseif self.currentBox == self.MAILBOX_LOCAL_TAB then
          curMail = GameMail:GetLocalMailByIdOrKey(key)
        end
        assert(curMail)
        local isAttach = LuaUtils:table_empty(curMail.goods)
        local isGet = curMail.get_goods
        local frame = self:GetMailIconFrame(curMail)
        local time = GameUtils:formatTimeStringAsAgoStyle(curMail.time)
        local mailDisplayTitle = curMail.title ~= "" and curMail.title or GameLoader:GetGameText("LC_MENU_MAIL_NO_SUBJECT_CHAR")
        self:GetFlashObject():InvokeASCallback("_root.contents", "setItem", key, curMail.status == 0, frame, mailDisplayTitle, time, not isAttach, isGet)
      end
    end
  end
  if cmd == "selectTab" then
    local tabIndex = tonumber(arg)
    self:SelectTab(tabIndex)
    if tabIndex == GameMail.MAILBOX_WRITE_TAB then
      self:ShowWrite()
      self:SetWrite("", "", "")
    else
      self:HideWrite()
    end
  end
  if cmd == "replyMail" then
    self:HideRead()
    self:ShowWrite()
    self:setCurrentMailType("player")
    local displayName = GameUtils:GetUserDisplayName(self.currentMailContent.sender_name)
    self:SetWrite(displayName)
  elseif cmd == "saveMail" then
    if GameMail.currentBox == GameMail.MAILBOX_LOCAL_TAB then
      return
    end
    if not LuaUtils:table_empty(GameMail:FindMailByID(GameMail.currentMailContent.id).goods) and GameMail:FindMailByID(GameMail.currentMailContent.id).get_goods == 1 then
      local tipText = GameLoader:GetGameText("LC_MENU_KEEP_FAILED_ATTACHMENT_MAIL_ALERT")
      GameTip:Show(tipText)
      return
    end
    self:HideRead()
    self.insertSaveMail()
    local mail_delete_req_param = {
      ids = self.deleteIds
    }
    NetMessageMgr:SendMsg(NetAPIList.mail_delete_req.Code, mail_delete_req_param, self.deleteMailCallback, true, nil)
  elseif cmd == "deleteMail" then
    if self.currentBox == self.MAILBOX_ONLINE_TAB then
      if not LuaUtils:table_empty(GameMail:FindMailByID(GameMail.currentMailContent.id).goods) and GameMail:FindMailByID(GameMail.currentMailContent.id).get_goods == 1 then
        GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
        GameUIMessageDialog:SetYesButton(GameMail.DeleteMail)
        GameUIMessageDialog:Display(GameLoader:GetGameText("LC_MENU_MAIL_DELETE_TITLE"), GameLoader:GetGameText("LC_MENU_ATTACHMENT_MAIL_DELETE_CONFIRM_ALERT"))
        return
      end
      self:HideRead()
      local mail_delete_req_param = {
        ids = self.deleteIds
      }
      NetMessageMgr:SendMsg(NetAPIList.mail_delete_req.Code, mail_delete_req_param, self.deleteMailCallback, true, nil)
    elseif self.currentBox == self.MAILBOX_LOCAL_TAB then
      self:HideRead()
      self:deleteLocalMail()
    end
  end
  if cmd == "WriteMailTitle" then
    self.titleText = arg
  elseif cmd == "WriteMailContent" then
    self.contentText = arg
  end
  if cmd == "sendMail" then
    local receviers = {}
    DebugOut("sendMail " .. currentMailType)
    if currentMailType == "alliance" then
      local mail_send_req_param = {
        title = self.titleText,
        content = self.contentText,
        my_send = 0,
        receiver_type = 0
      }
      NetMessageMgr:SendMsg(NetAPIList.mail_to_alliance_req.Code, mail_send_req_param, self.sendGroupMailCallback, true, nil)
    elseif currentMailType == "server" then
      local mail_send_req_param = {
        title = self.titleText,
        content = self.contentText
      }
      NetMessageMgr:SendMsg(NetAPIList.mail_to_all_req.Code, mail_send_req_param, self.sendGroupMailCallback, true, nil)
    else
      local nameText = self:GetFlashObject():GetText("contents.Write_email.toText")
      if 0 >= string.len(nameText) then
        local tipText = GameLoader:GetGameText("LC_MENU_RECIPIENTS_CANT_EMPTY_CHAR")
        GameTip:Show(tipText)
        return
      end
      local fullName = nameText .. GameMail.sendSuffix
      GameMail.sendSuffix = ""
      table.insert(receviers, fullName)
      DebugOut(receviers[1], self.titleText, self.contentText)
      local mail_send_req_param = {
        recevier_names = receviers,
        title = self.titleText,
        content = self.contentText
      }
      NetMessageMgr:SendMsg(NetAPIList.mail_send_req.Code, mail_send_req_param, self.sendMailCallback, true, nil)
    end
  end
  if cmd == "btnFirst" or cmd == "btnPrev" or cmd == "btnNext" or cmd == "btnLast" then
    if cmd == "btnFirst" then
      self.currentPage = 1
    elseif cmd == "btnPrev" then
      self.currentPage = self.currentPage - 1
      self.currentPage = math.max(1, self.currentPage)
    elseif cmd == "btnNext" then
      local maxPage = math.ceil((self.maxMail + 1) / self.countPrePage)
      self.currentPage = self.currentPage + 1
      self.currentPage = math.min(self.currentPage, maxPage)
    elseif cmd == "btnLast" then
      local maxPage = math.ceil((self.maxMail + 1) / self.countPrePage)
      self.currentPage = maxPage
    end
    if self.currentBox == self.MAILBOX_ONLINE_TAB then
      GameMail:SelectTab(self.currentBox, true)
    elseif self.currentBox == self.MAILBOX_LOCAL_TAB then
      GameMail:GetLocalMail(self.currentPage)
    end
  end
  if cmd == "shieldMailSender" then
    local friend_ban_req_param = {
      id = self.currentMailContent.sender_id,
      name = self.currentMailContent.sender_name
    }
    NetMessageMgr:SendMsg(NetAPIList.friend_ban_req.Code, friend_ban_req_param, GameMail.friendBanCallback, true, nil)
  end
  if cmd == "changeMailType" then
    self:setCurrentMailType(arg)
  end
  if cmd == "translate" then
    GameMail:TranslateMail(arg)
  end
end
function GameMail:GetLocalMailByIdOrKey(idOrKey)
  DebugOut("GameMail:GetLocalMailByIdOrKey idOrKey " .. idOrKey)
  DebugTable(self.mails)
  local mailItem
  local find = false
  for k, v in pairs(self.mails) do
    DebugTable(v)
    if v.id and tostring(idOrKey) == tostring(v.id) then
      mailItem = v
      find = true
      DebugOut("find.")
      break
    end
  end
  if not find then
    mailItem = self.mails[tonumber(idOrKey)]
  end
  return mailItem
end
function GameMail:GetMailIconFrame(mailItem)
  if self.currentBox == self.MAILBOX_LOCAL_TAB then
    if mailItem.sender_role and (mailItem.sender_role == 1 or mailItem.sender_role == 3) then
      return self.ICON_ADMIN_LOCAL
    else
      return self.ICON_LOCAL
    end
  elseif mailItem.status == 0 then
    if mailItem.sender_role and (mailItem.sender_role == 1 or mailItem.sender_role == 3) then
      return self.ICON_ADMIN_READED
    else
      return self.ICON_HAD_READED
    end
  elseif mailItem.sender_role and (mailItem.sender_role == 1 or mailItem.sender_role == 3) then
    return self.ICON_ADMIN_READED
  else
    return self.ICON_HAD_READED
  end
end
function GameMail:SelectTab(tabId, forceGet)
  self:RequestMail(tabId, forceGet)
  if tonumber(tabId) == tonumber(GameMail.MAILBOX_WRITE_TAB) then
    self:GetFlashObject():InvokeASCallback("_root.contents", "selectTab", tabId)
  end
end
function GameMail:RequestMail(tabId, forceGet)
  if forceGet or self.currentBox ~= tabId then
    if self.currentBox ~= tabId then
      self.currentPage = 1
    end
    self.currentBox = tabId
    if self.currentBox == GameMail.MAILBOX_ONLINE_TAB then
      local mail_page_req_param = {
        page_size = self.countPrePage,
        page_no = self.currentPage
      }
      self:GetFlashObject():InvokeASCallback("_root.contents", "ClearList")
      self:GetFlashObject():InvokeASCallback("_root.contents", "HideNoEmail")
      GameMail.mails = {}
      NetMessageMgr:SendMsg(NetAPIList.mail_page_req.Code, mail_page_req_param, GameMail.GetOnlineMail, true, nil)
    elseif self.currentBox == GameMail.MAILBOX_LOCAL_TAB then
      self.GetLocalMail()
    end
  end
end
function GameMail.GetOnlineMail(msgType, content)
  if msgType == NetAPIList.mail_page_ack.Code then
    GameMail.maxMail = content.max
    GameMail.currentPage = content.page_no
    GameMail.mails = content.mails
    GameMail.unread = content.unread
    GameMail:RefreshMail()
    return true
  end
  return false
end
function GameMail.GetLocalMail(page_no)
  local localmails = GameMail.getCurrentUserSaveMail()
  DebugOut("GetLocalMail", localmails)
  DebugTable(localmails)
  GameMail.maxMail = #localmails
  GameMail.currentPage = page_no or 1
  GameMail.mails = {}
  local startIndex = (GameMail.currentPage - 1) * (GameMail.countPrePage - 1) + 1
  local endIndex = math.min(GameMail.currentPage * (GameMail.countPrePage - 1), GameMail.maxMail)
  DebugOut("startIndex: ", startIndex, " endIndex: ", endIndex)
  for i = startIndex, endIndex do
    if localmails[i].get_goods == nil then
      localmails[i].get_goods = 1
    end
    if localmails[i].goods == nil then
      localmails[i].goods = {}
    end
    table.insert(GameMail.mails, localmails[i])
  end
  GameMail:RefreshMail()
end
function GameMail:RefreshMail()
  if self:GetFlashObject() == nil then
    return
  end
  self:GetFlashObject():InvokeASCallback("_root.contents", "ClearList")
  local noMailText = ""
  if self.currentBox == self.MAILBOX_ONLINE_TAB then
    noMailText = GameLoader:GetGameText("LC_MENU_NO_ONLINE_MAIL")
  elseif self.currentBox == self.MAILBOX_LOCAL_TAB then
    noMailText = GameLoader:GetGameText("LC_MENU_NO_LOCAL_MAIL")
  end
  self:GetFlashObject():InvokeASCallback("_root.contents", "InitList", #self.mails, noMailText)
  for k, v in pairs(self.mails or {}) do
    local key = v.id or k
    self:GetFlashObject():InvokeASCallback("_root.contents", "AddItemId", key)
  end
  if self.maxMail >= self.countPrePage then
    self:GetFlashObject():InvokeASCallback("_root.contents", "AddItemId", -1)
    local tipText = GameLoader:GetGameText("LC_MENU_MAILBOX_FULL_ALERT")
    GameTip:Show(tipText)
  end
  self:GetFlashObject():InvokeASCallback("_root.contents", "SetNewOnLineMailCount", self.unread)
  self:GetFlashObject():InvokeASCallback("_root.contents", "selectTab", self.currentBox)
end
function GameMail.readMailCallback(msgType, content)
  if msgType == NetAPIList.mail_content_ack.Code then
    DebugOut("readMailCallback ------> msgType= " .. msgType)
    DebugTable(content)
    GameMail.currentMailContent = content
    local time = GameUtils:formatTimeStringAsAgoStyle(content.time)
    local displayName = GameUtils:GetUserDisplayName(content.sender_name)
    local mail_type = tonumber(content.sender_role)
    mail_type = mail_type or 0
    local curMail = GameMail:FindMailByID(content.id)
    GameMail:GetFlashObject():InvokeASCallback("_root", "ClearAttachListItem")
    GameMail:GetFlashObject():InvokeASCallback("_root", "InitAttachListItem")
    DebugOut("curMail")
    DebugTable(curMail)
    GameMail.goodsIcon = {}
    GameMail.goodsNameText = {}
    for k, v in pairs(curMail.goods) do
      local icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v, nil, GameMail.DynamicResDowloadFinished)
      local nameText = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
      nameText = GameUtils.numberConversion(tonumber(nameText))
      nameText = "x" .. nameText
      table.insert(GameMail.goodsIcon, icon)
      table.insert(GameMail.goodsNameText, nameText)
    end
    for k, v in pairs(curMail.goods) do
      GameMail:GetFlashObject():InvokeASCallback("_root", "AddAttachListItem", k)
    end
    GameMail:GetFlashObject():InvokeASCallback("_root", "SetAttachArrowVisible")
    local canReply = true
    if mail_type == 0 then
      canReply = false
    end
    DebugOut("mail_type == ", mail_type)
    if LuaUtils:table_empty(GameMail.goodsIcon) then
      if curMail.get_goods == 0 then
        GameMail:ShowRead(canReply, true, true, false, true)
      else
        GameMail:ShowRead(canReply, true, true, false, false)
      end
    elseif curMail.get_goods == 0 then
      GameMail:ShowRead(canReply, true, true, true, true)
    else
      GameMail:ShowRead(canReply, true, true, true, false)
    end
    GameMail:SetRead(displayName, content.title, content.content, time, mail_type, not LuaUtils:table_empty(GameMail.goodsIcon))
    return true
  end
  return false
end
function GameMail.DynamicResDowloadFinished()
  local curMail = GameMail:FindMailByID(GameMail.currentMailContent.id)
  DebugOut("DynamicResDowloadFinished curMail")
  DebugTable(curMail)
  GameMail.goodsIcon = {}
  GameMail.goodsNameText = {}
  for k, v in pairs(curMail.goods) do
    local icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v, nil, GameMail.DynamicResDowloadFinished)
    local nameText = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
    nameText = GameUtils.numberConversion(tonumber(nameText))
    nameText = "x" .. nameText
    table.insert(GameMail.goodsIcon, icon)
    table.insert(GameMail.goodsNameText, nameText)
  end
  GameMail:GetFlashObject():InvokeASCallback("_root", "UpdateAttachList")
  DebugTable(GameMail.goodsIcon)
end
function GameMail:FindMailByID(id)
  for k, v in pairs(self.mails or {}) do
    if tonumber(v.id) == tonumber(id) then
      return v
    end
  end
  return nil
end
function GameMail.deleteMailCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.mail_delete_req.Code then
      if content.code == 0 then
        GameMail.maxMail = GameMail.maxMail - #GameMail.deleteIds
        GameMail:deleteMailCallback()
        GameMail:SelectTab(GameMail.currentBox, true)
      else
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    end
    return false
  end
  return false
end
function GameMail.sendMailCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.mail_send_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      else
        GameMail:HideWrite()
        GameMail:SelectTab(GameMail.MAILBOX_ONLINE_TAB)
      end
      return true
    end
    return false
  end
  return false
end
function GameMail.sendGroupMailCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.mail_to_alliance_req.Code then
      DebugOut("function GameMail.sendGroupMailCallback(msgType, content)")
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      else
        GameMail:HideWrite()
        GameMail:SelectTab(GameMail.MAILBOX_ONLINE_TAB)
      end
      return true
    elseif content.api == NetAPIList.mail_to_all_req.Code then
      DebugOut("mail_to_all_req callback")
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      else
        GameMail:HideWrite()
        GameMail:SelectTab(GameMail.MAILBOX_ONLINE_TAB)
      end
      return true
    end
    return false
  end
  return false
end
function GameMail.markMailCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.mail_set_read_req.Code then
      if content.code == 0 then
        GameMail:SelectTab(GameMail.currentBox, true)
      else
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    end
    return false
  end
  return false
end
function GameMail.friendBanCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.friend_ban_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    end
    return false
  end
  return false
end
function GameMail.DeleteMail()
  GameMail:HideRead()
  local mail_delete_req_param = {
    ids = GameMail.deleteIds
  }
  NetMessageMgr:SendMsg(NetAPIList.mail_delete_req.Code, mail_delete_req_param, GameMail.deleteMailCallback, true, nil)
end
function GameMail.deleteAllMail()
  GameMail.HideCheckBox()
  if GameMail.currentBox == GameMail.MAILBOX_ONLINE_TAB then
    local mail_delete_req_param = {}
    if GameUIMessageDialog.isCheck == 1 then
      mail_delete_req_param = {
        ids = GameMail.deleteIds
      }
    else
      mail_delete_req_param = {
        ids = GameMail.deleteNoAttachID
      }
    end
    NetMessageMgr:SendMsg(NetAPIList.mail_delete_req.Code, mail_delete_req_param, GameMail.deleteMailCallback, true, nil)
  elseif GameMail.currentBox == GameMail.MAILBOX_LOCAL_TAB then
    if GameUIMessageDialog.isCheck == 0 then
      GameMail.deleteIds = GameMail.deleteNoAttachID
    end
    GameMail:deleteLocalMail()
  end
end
function GameMail.HideCheckBox()
  GameUIMessageDialog:HideCheckBox()
end
if AutoUpdate.isAndroidDevice then
  function GameMail.OnAndroidBack()
    if GameMail.isOnReadMail then
      GameMail.isOnReadMail = nil
      GameMail:HideRead()
    else
      GameMail:HideBox()
    end
  end
end
function GameMail:GetCostCredit(content)
  DebugOut("GameMail:GetCostCredit", content)
  local requestParam = {price_type = content, type = 0}
  NetMessageMgr:SendMsg(NetAPIList.price_req.Code, requestParam, self.getCostCreditCallback, true, nil)
end
function GameMail.getCostCreditCallback(msgType, content)
  DebugOut("getCostCreditCallback ", content.price, content.price_type)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.price_req.Code then
    assert(content.code ~= 0)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  if msgType == NetAPIList.price_ack.Code then
    if content.price >= 0 then
      DebugOut("price", content.price)
      sendServerPrice = content.price
      GameMail:GetFlashObject():InvokeASCallback("_root", "lua2fs_setSendBtnPrice", sendServerPrice)
    end
    return true
  end
  return false
end
function GameMail:setCurrentMailType(typeStr)
  DebugOut("GameMail:setCurrentMailType", typeStr)
  currentMailType = typeStr
  self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setMailType", typeStr)
  if typeStr == "server" and sendServerPrice == 0 then
    GameMail:GetCostCredit("mail_to_all_cost")
  end
end
GameMail.mTranslateMail = nil
function GameMail:TranslateMail(transBtnStateFr)
  if "Idle" == transBtnStateFr then
    if GameMail.mTranslateMail then
      return
    end
    GameMail.mTranslateMail = GameMail.currentMailContent
    if GameMail.mTranslateMail.title == "" then
      GameMail.mTranslateMail.title = GameLoader:GetGameText("LC_MENU_MAIL_NO_SUBJECT_CHAR")
    end
    local tmpTitle = GameUtils:TrimHtmlStr(GameMail.mTranslateMail.title)
    local tmpContent = GameUtils:TrimHtmlStr(GameMail.mTranslateMail.content)
    if tmpTitle ~= "" then
      GameUtils:Translate(tmpTitle, GameMail.CallbackOfTranslateTitle)
    end
    if tmpContent ~= "" then
      GameUtils:Translate(tmpContent, GameMail.CallbackOfTranslateContent)
    end
  else
    local flashObj = GameMail:GetFlashObject()
    if flashObj and GameMail.currentMailContent then
      flashObj:InvokeASCallback("_root", "SetReadMailTitleAndContent", GameMail.currentMailContent.title, GameMail.currentMailContent.content)
      flashObj:InvokeASCallback("_root", "SetTranslateBtnState", "Idle")
    end
  end
end
function GameMail.CallbackOfTranslateTitle(success, result)
  DebugOut("GameMail.CallbackOfTranslateTitle ")
  if success then
    DebugOut("Translate result : ")
    DebugTable(result)
    if GameMail.mTranslateMail and GameMail.currentMailContent and GameMail.mTranslateMail.id == GameMail.currentMailContent.id then
      GameMail.mTranslateMail.titleTanslate = result.translatedText
      local flashObj = GameMail:GetFlashObject()
      if flashObj then
        flashObj:InvokeASCallback("_root", "SetReadMailTitleAndContent", GameMail.mTranslateMail.titleTanslate, GameMail.mTranslateMail.contentTanslate)
        flashObj:InvokeASCallback("_root", "SetTranslateBtnState", "highlight")
      end
    end
  else
    if GameMail.mTranslateMail == nil then
      return
    end
    GameMail.mTranslateMail.titleTanslate = GameMail.mTranslateMail.title
    DebugTable(GameMail.mTranslateMail)
    DebugOut("Translate failed, errText : ")
  end
  if GameMail.mTranslateMail and GameMail.mTranslateMail.contentTanslate then
    GameMail.mTranslateMail = nil
  end
end
function GameMail.CallbackOfTranslateContent(success, result)
  DebugOut("GameMail.CallbackOfTranslateContent ")
  if success then
    DebugOut("Translate result : ", GameMail.mTranslateMail, GameMail.currentMailContent)
    DebugTable(result)
    if GameMail.mTranslateMail and GameMail.currentMailContent and GameMail.mTranslateMail.id == GameMail.currentMailContent.id then
      GameMail.mTranslateMail.contentTanslate = result.translatedText
      DebugOut("lkasdalksdlak")
      local flashObj = GameMail:GetFlashObject()
      if flashObj then
        flashObj:InvokeASCallback("_root", "SetReadMailTitleAndContent", GameMail.mTranslateMail.titleTanslate, GameMail.mTranslateMail.contentTanslate)
        flashObj:InvokeASCallback("_root", "SetTranslateBtnState", "highlight")
      end
    end
  else
    if GameMail.mTranslateMail == nil then
      return
    end
    GameMail.mTranslateMail.contentTanslate = GameMail.mTranslateMail.content
    DebugTable(GameMail.mTranslateMail)
    DebugOut("Translate failed, errText : ")
  end
  if GameMail.mTranslateMail and GameMail.mTranslateMail.titleTanslate then
    GameMail.mTranslateMail = nil
  end
end
function GameMail:SetEnableChangeName(enable)
  if GameMail:GetFlashObject() then
    GameMail:GetFlashObject():InvokeASCallback("_root", "SetEnabledChangeName", enable)
  end
end
function GameMail:SetSendSuffix(str)
  GameMail.sendSuffix = str
end
