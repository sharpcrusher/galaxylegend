local GameUICommonEvent = LuaObjectManager:GetLuaObject("GameUICommonEvent")
local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameCommonGoodsList = LuaObjectManager:GetLuaObject("GameCommonGoodsList")
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
GameUICommonEvent.festivalDetailDataset = {}
GameUICommonEvent.DetailType = {
  UNKNOWN = 0,
  FESTIVAL = 1,
  WD_EVENT = 2,
  WDC = 3
}
GameUICommonEvent.curDetail = nil
GameUICommonEvent.curDetailType = GameUICommonEvent.DetailType.UNKNOWN
GameUICommonEvent.NetMegType = {FESTIVAL = 1, WD_EVENT = 3}
function GameUICommonEvent:ResetState()
  GameUICommonEvent.receiveFestivalInfoTime = -1
  GameUICommonEvent.festivalInfo = nil
  GameUICommonEvent.leaderboardTab = 1
  GameUICommonEvent.playerLeaderboard = nil
  GameUICommonEvent.starAllianceLeaderboard = nil
  GameUICommonEvent.myLeaderboard = nil
  GameUICommonEvent.receiveFestivalInfoTime = -1
  GameUICommonEvent.leftTime = 0
  GameUICommonEvent.endTime = 0
  GameUICommonEvent.goodsList = nil
  GameUICommonEvent.rewardsList = nil
  GameUICommonEvent.pointsInfo = {}
  GameUICommonEvent.pointsInfo.isText = true
  GameUICommonEvent.pointsInfo.id = -1
  GameUICommonEvent.pointsInfo.frame = "empty"
  GameUICommonEvent.fleetInfo = {}
  GameUICommonEvent.fleetInfo.isOn = false
  GameUICommonEvent.fleetInfo.fleetId = -1
  GameUICommonEvent.fleetInfo.fleetPrice = {}
  GameUICommonEvent.fleetInfo.fleetPrice.p1 = -1
  GameUICommonEvent.fleetInfo.fleetPrice.p2 = -1
  GameUICommonEvent.fleetInfo.fleetPrice.p3 = -1
  GameUICommonEvent.fleetInfo.id1 = -1
  GameUICommonEvent.fleetInfo.id2 = -1
  GameUICommonEvent.fleetInfo.id3 = -1
  GameUICommonEvent.fleetInfo.frame1 = "empty"
  GameUICommonEvent.fleetInfo.frame2 = "empty"
  GameUICommonEvent.fleetInfo.frame3 = "empty"
  GameUICommonEvent.fleetInfo.title = ""
  GameUICommonEvent.fleetInfo.desc = ""
  GameUICommonEvent.fleetInfo.buffUrl = ""
  GameUICommonEvent.fleetInfo.fleetUrl = ""
  GameUICommonEvent.fleetInfo.buffExist = false
  GameUICommonEvent.fleetInfo.fleetExist = false
  GameUICommonEvent.title = ""
  GameUICommonEvent.subTitle = ""
  GameUICommonEvent.desc = ""
  GameUICommonEvent.bannerPicUrl = ""
  GameUICommonEvent.bannerExist = false
  GameUICommonEvent.rulesInfo = {}
  GameUICommonEvent.rulesInfo.rulesPicUrl = ""
  GameUICommonEvent.rulesInfo.rulesExist = false
  GameUICommonEvent.rulesInfo.descs = {}
  GameUICommonEvent.rewardInfo = {}
  GameUICommonEvent.rewardInfo.rewardPicUrl = ""
  GameUICommonEvent.rewardInfo.rewardExist = false
  GameUICommonEvent.rewardInfo.rewardDesc = ""
  GameUICommonEvent.rewardInfo.rewards = {}
  GameUICommonEvent.isExchangeOn = false
  GameUICommonEvent.isLeaderboardOn = false
end
function GameUICommonEvent:SetExchangeInfo(exchangeInfo)
  DebugOut("setExchangeInfo")
  DebugTable(exchangeInfo)
  GameUICommonEvent.isExchangeOn = exchangeInfo.isExchangeOn
  if GameUICommonEvent.isExchangeOn then
    GameCommonGoodsList:SetID(exchangeInfo)
  end
end
function GameUICommonEvent:SetRewardInfo(rewardInfo)
  DebugOut("setRewardInfo")
  DebugTable(rewardInfo)
  GameUICommonEvent.rewardInfo.rewardPicUrl = rewardInfo.rewardPicUrl
  GameUICommonEvent.rewardInfo.rewardDesc = rewardInfo.rewardDesc
  for i = 1, #rewardInfo.rewards do
    DebugTable(rewardInfo.rewards[i])
    GameUICommonEvent.rewardInfo.rewards[i] = rewardInfo.rewards[i]
  end
end
function GameUICommonEvent:RefreshReward()
  GameUICommonEvent:GetFlashObject():InvokeASCallback("_root", "clearRewardListItem")
  local itemCount = #GameUICommonEvent.rewardInfo.rewards + 2
  local rewardsCount = 0
  if GameUICommonEvent.rewardsList == nil then
    rewardsCount = 0
  else
    rewardsCount = #GameUICommonEvent.rewardsList
  end
  if itemCount > rewardsCount + 2 then
    itemCount = rewardsCount + 2
  end
  GameUICommonEvent:GetFlashObject():InvokeASCallback("_root", "initRewardListItem")
  for i = 1, itemCount do
    GameUICommonEvent:GetFlashObject():InvokeASCallback("_root", "addRewardListItem", i)
  end
  GameUICommonEvent:GetFlashObject():InvokeASCallback("_root", "SetRawardArrowInitVisible")
end
function GameUICommonEvent:SetRewardContent(rewardInfo)
  GameUICommonEvent:InitRewardInfo()
  GameUICommonEvent:RefreshReward()
end
function GameUICommonEvent:InitRewardInfo()
  if not GameUICommonEvent.rewardInfo.rewardExist then
    local pngName = string.match(GameUICommonEvent.rewardInfo.rewardPicUrl, "([^/]*)$")
    local localPath = "data2/" .. pngName
    local replaceName = "reward_banner.png"
    if DynamicResDownloader:IfResExsit(localPath) then
      if GameUICommonEvent:GetFlashObject() then
        ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
        GameUICommonEvent:GetFlashObject():ReplaceTexture(replaceName, pngName)
      end
    else
      local extInfo = {}
      extInfo.pngName = pngName
      extInfo.localPath = localPath
      extInfo.replaceName = replaceName
      DynamicResDownloader:AddDynamicRes(pngName, DynamicResDownloader.resType.FESTIVAL_BANNER, extInfo, GameUICommonEvent.bannerDownloaderCallback)
    end
  end
end
function GameUICommonEvent:UpdateRewardItem(id)
  local itemKey = tonumber(id)
  if itemKey > #GameUICommonEvent.rewardInfo.rewards + 2 then
    return
  end
  if GameUICommonEvent.rewardsList == nil then
    if itemKey > 2 then
      return
    end
  elseif itemKey > #GameUICommonEvent.rewardsList + 2 then
    return
  end
  if itemKey == 1 then
    self:GetFlashObject():InvokeASCallback("_root", "setRewardItem1")
  elseif itemKey == 2 then
    self:GetFlashObject():InvokeASCallback("_root", "setRewardItem2", GameUICommonEvent.rewardInfo.rewardDesc)
  else
    local reward = GameUICommonEvent.rewardInfo.rewards[itemKey - 2]
    local items = GameUICommonEvent.rewardsList[itemKey - 2]
    local names = ""
    local frames = ""
    local nums = ""
    local levels = ""
    for i = 1, #items do
      if items[i].item_type == "item" or items[i].item_type == "krypton" then
        if DynamicResDownloader:IsDynamicStuff(items[i].number, DynamicResDownloader.resType.PIC) then
          local fullFileName = DynamicResDownloader:GetFullName(items[i].number .. ".png", DynamicResDownloader.resType.PIC)
          local localPath = "data2/" .. fullFileName
          if DynamicResDownloader:IfResExsit(localPath) then
            ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
            frames = frames .. "item_" .. items[i].number .. "\001"
          else
            frames = frames .. "temp" .. "\001"
            self:AddDownloadPath(items[i].number, itemKey, -1, donamicDownloadFinishCallback3)
          end
        else
          frames = frames .. "item_" .. items[i].number .. "\001"
        end
        names = names .. GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. items[i].number) .. "\001"
        nums = nums .. "X" .. items[i].no .. "\001"
        if items[i].item_type == "item" then
          levels = levels .. "-1\001"
        else
          levels = levels .. GameLoader:GetGameText("LC_MENU_Level") .. items[i].level .. "\001"
        end
      else
        local f = ""
        local n = ""
        local u = 0
        n, u, f = GameUIBattleResult:GetItemTextAndNum3(items[i].item_type, items[i].no, items[i].number)
        frames = frames .. f .. "\001"
        names = names .. n .. "\001"
        nums = nums .. "X" .. u .. "\001"
        levels = levels .. "-1\001"
      end
    end
    for i = #items + 1, 5 do
      frames = frames .. "empty\001"
      names = names .. "..\001"
      nums = nums .. "0\001"
      levels = levels .. "-1\001"
    end
    DebugOut("setRewardItem", frames, names, nums, levels)
    self:GetFlashObject():InvokeASCallback("_root", "setRewardItem3", itemKey, reward.desc, frames, nums, names, levels)
  end
end
function GameUICommonEvent:SetRulesInfo(rulesInfo)
  DebugOut("setRulesInfo")
  DebugTable(rulesInfo)
  GameUICommonEvent.rulesInfo.rulesPicUrl = rulesInfo.rulesPicUrl
  for i = 1, #rulesInfo.rulesDesc do
    GameUICommonEvent.rulesInfo.descs[i] = rulesInfo.rulesDesc[i]
  end
end
function GameUICommonEvent:SetRulesContent()
  GameUICommonEvent:InitRulesInfo()
  GameUICommonEvent:GetFlashObject():InvokeASCallback("_root", "clearInfoListItem")
  local itemCount = #GameUICommonEvent.rulesInfo.descs + 1
  GameUICommonEvent:GetFlashObject():InvokeASCallback("_root", "initInfoListItem")
  for i = 1, itemCount do
    GameUICommonEvent:GetFlashObject():InvokeASCallback("_root", "addInfoListItem", i)
  end
  GameUICommonEvent:GetFlashObject():InvokeASCallback("_root", "SetInfoArrowInitVisible")
end
function GameUICommonEvent:UpdateInfoItem(id)
  local itemKey = tonumber(id)
  if itemKey > #GameUICommonEvent.rulesInfo.descs + 1 then
    return
  end
  if itemKey == 1 then
    self:GetFlashObject():InvokeASCallback("_root", "setInfoItem", 1)
  else
    self:GetFlashObject():InvokeASCallback("_root", "setInfoItem", itemKey, GameUICommonEvent.rulesInfo.descs[id - 1])
  end
end
function GameUICommonEvent:InitRulesInfo()
  if not GameUICommonEvent.rulesInfo.rulesExist then
    local pngName = string.match(GameUICommonEvent.rulesInfo.rulesPicUrl, "([^/]*)$")
    local localPath = "data2/" .. pngName
    local replaceName = "rules_banner.png"
    if DynamicResDownloader:IfResExsit(localPath) then
      if GameUICommonEvent:GetFlashObject() then
        ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
        GameUICommonEvent:GetFlashObject():ReplaceTexture(replaceName, pngName)
      end
    else
      local extInfo = {}
      extInfo.pngName = pngName
      extInfo.localPath = localPath
      extInfo.replaceName = replaceName
      DynamicResDownloader:AddDynamicRes(pngName, DynamicResDownloader.resType.FESTIVAL_BANNER, extInfo, GameUICommonEvent.bannerDownloaderCallback)
    end
  end
end
function GameUICommonEvent:SetEventInfo(eventInfo)
  DebugOut("setEventInfo")
  DebugTable(eventInfo)
  GameUICommonEvent.title = eventInfo.title
  GameUICommonEvent.subTitle = eventInfo.subTitle
  GameUICommonEvent.desc = eventInfo.desc
  GameUICommonEvent.bannerPicUrl = eventInfo.bannerPicUrl
end
function GameUICommonEvent:SetEventContent()
  GameUICommonEvent:InitEventInfo()
  GameUICommonEvent:GetFlashObject():InvokeASCallback("_root", "SetEventInfoText", GameUICommonEvent.title, GameUICommonEvent.subTitle, GameUICommonEvent.desc)
end
function GameUICommonEvent:InitEventInfo()
  if not GameUICommonEvent.bannerExist then
    local pngName = string.match(GameUICommonEvent.bannerPicUrl, "([^/]*)$")
    local localPath = "data2/" .. pngName
    local replaceName = "christmas_banner.png"
    if DynamicResDownloader:IfResExsit(localPath) then
      if GameUICommonEvent:GetFlashObject() then
        ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
        GameUICommonEvent:GetFlashObject():ReplaceTexture(replaceName, pngName)
      end
    else
      local extInfo = {}
      extInfo.pngName = pngName
      extInfo.localPath = localPath
      extInfo.replaceName = replaceName
      DynamicResDownloader:AddDynamicRes(pngName, DynamicResDownloader.resType.FESTIVAL_BANNER, extInfo, GameUICommonEvent.bannerDownloaderCallback)
    end
  end
end
function GameUICommonEvent:SetPointsInfo(pointsInfo)
  DebugOut("setPointsInfo")
  DebugTable(pointsInfo)
  GameUICommonEvent.pointsInfo.isText = pointsInfo.isPointsText
  if GameUICommonEvent.pointsInfo.isText then
    GameUICommonEvent.pointsInfo.id = pointsInfo.pointId
  end
end
function GameUICommonEvent:SetPointsContent()
  GameUICommonEvent:InitPointsInfo()
  GameUICommonEvent:GetFlashObject():InvokeASCallback("_root", "SetPointsIsText", GameUICommonEvent.pointsInfo.isText, GameUICommonEvent.pointsInfo.frame)
end
function GameUICommonEvent:InitPointsInfo()
  if GameUICommonEvent.pointsInfo.isText and GameUICommonEvent.pointsInfo.id ~= -1 and GameUICommonEvent.pointsInfo.frame == "empty" then
    if DynamicResDownloader:IsDynamicStuff(self.pointsInfo.id, DynamicResDownloader.resType.PIC) then
      local fullFileName = DynamicResDownloader:GetFullName(self.pointsInfo.id .. ".png", DynamicResDownloader.resType.PIC)
      local localPath = "data2/" .. fullFileName
      if DynamicResDownloader:IfResExsit(localPath) then
        ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
        self.pointsInfo.frame = "item_" .. self.pointsInfo.id
      else
        self.pointsInfo.frame = "temp"
        self:AddDownloadPath(self.pointsInfo.id, -1, -1, donamicDownloadFinishCallback2)
      end
    else
      self.pointsInfo.frame = "item_" .. self.pointsInfo.id
    end
  end
end
function GameUICommonEvent:SetFleetInfo(fleetInfo)
  DebugOut("setFleetInfo")
  DebugTable(fleetInfo)
  GameUICommonEvent.fleetInfo.isOn = fleetInfo.isCallOn
  if GameUICommonEvent.fleetInfo.isOn then
    for i = 1, #fleetInfo.items do
      GameUICommonEvent.fleetInfo["id" .. i] = fleetInfo.items[i]
    end
    GameUICommonEvent.fleetInfo.title = fleetInfo.callTitle
    GameUICommonEvent.fleetInfo.desc = fleetInfo.callDesc
    GameUICommonEvent.fleetInfo.buffUrl = fleetInfo.buffPicUrl
    GameUICommonEvent.fleetInfo.fleetUrl = fleetInfo.fleetPicUrl
  end
end
function GameUICommonEvent:SetFleetContent()
  self:InitFleetInfo()
  self:SetFleetInfoIcon()
  self:SetFleetInfoBanner()
  self:SetFleetInfoText()
end
function GameUICommonEvent:SetFleetInfoIcon()
  if GameUICommonEvent.fleetInfo.isOn then
    self:GetFlashObject():InvokeASCallback("_root", "SetFleetIcon", self.fleetInfo.frame1, self.fleetInfo.frame2, self.fleetInfo.frame3)
  end
end
function GameUICommonEvent:SetFleetInfoBanner()
end
function GameUICommonEvent:SetFleetInfoText()
  if GameUICommonEvent.fleetInfo.isOn then
    GameUICommonEvent:GetFlashObject():InvokeASCallback("_root", "SetFleetInfoText", GameUICommonEvent.fleetInfo.title, GameUICommonEvent.fleetInfo.desc)
  end
end
function GameUICommonEvent:InitFleetInfo()
  if GameUICommonEvent.fleetInfo.isOn then
    if not GameUICommonEvent.fleetInfo.buffExist then
      local pngName = string.match(GameUICommonEvent.fleetInfo.buffUrl, "([^/]*)$")
      local localPath = "data2/" .. pngName
      local replaceName = "buff.png"
      if DynamicResDownloader:IfResExsit(localPath) then
        if GameUICommonEvent:GetFlashObject() then
          ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
          GameUICommonEvent:GetFlashObject():ReplaceTexture(replaceName, pngName)
        end
      else
        local extInfo = {}
        extInfo.pngName = pngName
        extInfo.localPath = localPath
        extInfo.replaceName = replaceName
        DynamicResDownloader:AddDynamicRes(pngName, DynamicResDownloader.resType.FESTIVAL_BANNER, extInfo, GameUICommonEvent.bannerDownloaderCallback)
      end
    end
    if not GameUICommonEvent.fleetInfo.fleetExist then
      local pngName = string.match(GameUICommonEvent.fleetInfo.fleetUrl, "([^/]*)$")
      local localPath = "data2/" .. pngName
      local replaceName = "christmas_card.png"
      if DynamicResDownloader:IfResExsit(localPath) then
        if GameUICommonEvent:GetFlashObject() then
          ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
          GameUICommonEvent:GetFlashObject():ReplaceTexture(replaceName, pngName)
        end
        GameUICommonEvent:GetFlashObject():InvokeASCallback("_root", "unShowCardText")
      else
        local extInfo = {}
        extInfo.pngName = pngName
        extInfo.localPath = localPath
        extInfo.replaceName = replaceName
        function extInfo.callback()
          GameUICommonEvent:GetFlashObject():InvokeASCallback("_root", "unShowCardText")
        end
        DynamicResDownloader:AddDynamicRes(pngName, DynamicResDownloader.resType.FESTIVAL_BANNER, extInfo, GameUICommonEvent.bannerDownloaderCallback)
      end
    end
    for i = 1, 3 do
      local id = "id" .. i
      local frame = "frame" .. i
      if self.fleetInfo[id] ~= -1 and self.fleetInfo[frame] == "empty" then
        if DynamicResDownloader:IsDynamicStuff(self.fleetInfo[id], DynamicResDownloader.resType.PIC) then
          local fullFileName = DynamicResDownloader:GetFullName(self.fleetInfo[id] .. ".png", DynamicResDownloader.resType.PIC)
          local localPath = "data2/" .. fullFileName
          if DynamicResDownloader:IfResExsit(localPath) then
            ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
            self.fleetInfo[frame] = "item_" .. self.fleetInfo[id]
          else
            self.fleetInfo[frame] = "temp"
            self:AddDownloadPath(self.fleetInfo[id], id, frame, donamicDownloadFinishCallback1)
          end
        else
          self.fleetInfo[frame] = "item_" .. self.fleetInfo[id]
        end
      end
    end
  end
end
function GameUICommonEvent:OnInitGame()
end
function GameUICommonEvent:SetBntContent()
  DebugOut("setBntContent", self.fleetInfo.isOn, self.isExchangeOn, self.isLeaderboardOn)
  self:GetFlashObject():InvokeASCallback("_root", "setBntPos", self.fleetInfo.isOn, self.isExchangeOn, self.isLeaderboardOn)
end
function GameUICommonEvent:Init()
  local flash_obj = self:GetFlashObject()
  if flash_obj ~= nil then
    if GameUICommonEvent.leftTime < 0 then
      GameUICommonEvent.leftTime = 0
    end
    GameUICommonEvent:SetPointsContent()
    GameUICommonEvent:SetFleetContent()
    GameUICommonEvent:SetEventContent()
    GameUICommonEvent:SetRulesContent()
    GameUICommonEvent:SetBntContent()
    GameUICommonEvent:SetRewardContent()
    GameUICommonEvent:TrySendGoodsListReq()
    GameUICommonEvent:TrySendRewardReq()
    GameUICommonEvent:RefreshMyRank()
    GameGlobalData:RegisterDataChangeCallback("fleetinfo", self.RefreshCanCall)
    GameUICommonEvent:RefreshResource()
    GameGlobalData:RegisterDataChangeCallback("item_count", self.RefreshResource)
  end
end
function GameUICommonEvent:TrySendRewardReq()
  if GameUICommonEvent.rewardsList == nil then
    DebugOut("send rewards list req")
    local idStr = "festival_lantern"
    if GameUICommonEvent.curDetailType == GameUICommonEvent.DetailType.WD_EVENT then
      idStr = "wd_ranking"
    end
    local content = {id = idStr, index = 0}
    NetMessageMgr:SendMsg(NetAPIList.promotion_detail_req.Code, content, GameUICommonEvent.RewardCallback, true, nil)
  end
end
function GameUICommonEvent.RewardCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.promotion_detail_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.promotion_detail_ack.Code then
    DebugOut("GameUICommonEvent.PromotionsDetailCallbac ", msgType)
    DebugTable(content)
    GameUICommonEvent.rewardsList = {}
    for i = 1, #content.sections do
      GameUICommonEvent.rewardsList[i] = content.sections[i].awards
    end
    DebugTable(GameUICommonEvent.rewardsList)
    GameUICommonEvent:RefreshReward()
    return true
  end
  return false
end
function GameUICommonEvent:TrySendGoodsListReq()
  if GameUICommonEvent.goodsList == nil and GameUICommonEvent.fleetInfo.isOn then
    local reqType = GameUICommonEvent.NetMegType.FESTIVAL
    if GameUICommonEvent.curDetailType == GameUICommonEvent.DetailType.WD_EVENT then
      reqType = GameUICommonEvent.NetMegType.WD_EVENT
    end
    DebugOut("---------------zm test-------------request goodsList")
    local stores_req_content = {
      store_type = reqType,
      locale = GameSettingData.Save_Lang
    }
    NetMessageMgr:SendMsg(NetAPIList.activity_stores_req.Code, stores_req_content, GameUICommonEvent.GoodsListCallback, true, nil)
  end
end
function GameUICommonEvent.GoodsListCallback(msgType, content)
  DebugOut("receive fleet list res")
  if msgType == NetAPIList.activity_stores_ack.Code then
    local initList = GameUICommonEvent.goodsList == nil
    DebugOut("zmtest:")
    DebugTable(content)
    for i = #content.items, 1, -1 do
      if content.items[i].item_type ~= "fleet" then
        DebugOut("remove item", i)
        table.remove(content.items, i)
      end
    end
    DebugOut("after remove")
    DebugTable(content)
    GameUICommonEvent.goodsList = content
    if initList and #content.items > 0 then
      local price = {}
      local p1, p2, p3 = 0, 0, 0
      for k1, v1 in ipairs(content.items[1].items) do
        DebugOut("=====item id", v1.item_id)
        DebugOut("=====item count", v1.item_no)
        if v1.item_id == GameUICommonEvent.fleetInfo.id1 then
          p1 = v1.item_no
        elseif v1.item_id == GameUICommonEvent.fleetInfo.id2 then
          p2 = v1.item_no
        elseif v1.item_id == GameUICommonEvent.fleetInfo.id3 then
          p3 = v1.item_no
        end
      end
      GameUICommonEvent.fleetInfo.fleetPrice.p1 = p1
      GameUICommonEvent.fleetInfo.fleetPrice.p2 = p2
      GameUICommonEvent.fleetInfo.fleetPrice.p3 = p3
      GameUICommonEvent.fleetInfo.buyId = content.items[1].id
      GameUICommonEvent.fleetInfo.fleetId = content.items[1].item_id
      GameUICommonEvent:RefreshResource()
      GameUICommonEvent:RefreshCanCall()
    end
    return true
  end
  return false
end
function GameUICommonEvent:RefreshResource()
  if not GameUICommonEvent.fleetInfo.isOn then
    return
  end
  local resource = GameGlobalData:GetData("item_count")
  local flash_obj = GameUICommonEvent:GetFlashObject()
  if flash_obj and GameUICommonEvent.fleetInfo.fleetPrice.p1 ~= -1 then
    local item_tree_count = 0
    local item_suger_count = 0
    local item_jew_count = 0
    for i, v in ipairs(resource.items) do
      if v.item_id == GameUICommonEvent.fleetInfo.id1 then
        item_tree_count = item_tree_count + v.item_no
      elseif v.item_id == GameUICommonEvent.fleetInfo.id2 then
        item_suger_count = item_suger_count + v.item_no
      elseif v.item_id == GameUICommonEvent.fleetInfo.id3 then
        item_jew_count = item_jew_count + v.item_no
      end
    end
    local price1 = ""
    local price2 = ""
    local price3 = ""
    price1 = "<font color='#FFCC00'>" .. item_tree_count .. "</font>/" .. GameUICommonEvent.fleetInfo.fleetPrice.p1
    price2 = "<font color='#FFCC00'>" .. item_suger_count .. "</font>/" .. GameUICommonEvent.fleetInfo.fleetPrice.p2
    price3 = "<font color='#FFCC00'>" .. item_jew_count .. "</font>/" .. GameUICommonEvent.fleetInfo.fleetPrice.p3
    DebugOut("set fleet price", price1, price2, price3)
    flash_obj:InvokeASCallback("_root", "RefreshResource", price1, price2, price3)
  end
end
function GameUICommonEvent:RefreshCanCall()
  DebugOut("GameUICommonEvent:RefreshCanCall")
  if not GameUICommonEvent.fleetInfo.isOn then
    return
  end
  local flash_obj = GameUICommonEvent:GetFlashObject()
  if flash_obj then
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local isExist = false
    DebugTable(fleets)
    for k, v in ipairs(fleets) do
      if v.identity == GameUICommonEvent.fleetInfo.fleetId then
        isExist = true
        break
      end
    end
    if not isExist then
      DebugOut("call bnt visible")
      flash_obj:InvokeASCallback("_root", "setCallBntVisible", true)
    else
      DebugOut("call bnt un visible")
      flash_obj:InvokeASCallback("_root", "setCallBntVisible", false)
    end
  end
end
function GameUICommonEvent:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self:Init()
  self:MoveIn()
  self:GetFlashObject():InvokeASCallback("_root", "clearListItem")
  local code = NetAPIList.thanksgiven_rank_req.Code
  if GameUICommonEvent.curDetailType == GameUICommonEvent.DetailType.WD_EVENT then
    code = NetAPIList.wd_ranking_rank_req.Code
  end
  NetMessageMgr:SendMsg(code, nil, GameUICommonEvent.FetchLeaderboardCallback, false, nil)
end
function GameUICommonEvent:IsEventOn(eventType)
  for i, v in ipairs(self.festivalDetailDataset) do
    if eventType == v.detailType then
      return true
    end
  end
  return false
end
function GameUICommonEvent:ShowEvent(type)
  self.ResetState()
  self.curDetailType = type
  for i, v in ipairs(self.festivalDetailDataset) do
    if self.curDetailType == v.detailType then
      self.curDetail = v
      self.isLeaderboardOn = v.details.isLeaderboardOn
      break
    end
  end
  if self.curDetail == nil then
    DebugOut("GameUICommonEvent:ShowEvent: self.curDetail == nil")
    return
  end
  if not GameStateManager:GetCurrentGameState():IsObjectInState(self) then
    GameStateManager:GetCurrentGameState():AddObject(self)
    if not self:GetFlashObject() then
      self:LoadFlashObject()
    end
    self.receiveFestivalInfoTime = self.curDetail.receiveInfoTime
    self.leftTime = self.curDetail.timeInfo.end_time - self.curDetail.timeInfo.current_time
    self.endTime = self.curDetail.timeInfo.end_time
    self:SetExchangeInfo(self.curDetail.details.exchangeInfo)
    self:SetRulesInfo(self.curDetail.details.rulesInfo)
    self:SetEventInfo(self.curDetail.details)
    self:SetRewardInfo(self.curDetail.details.rewardInfo)
    self:SetPointsInfo(self.curDetail.details.pointsInfo)
    self:SetFleetInfo(self.curDetail.details.callInfo)
  else
    DebugOut("GameUICommonEvent:ShowEvent: error")
  end
end
function GameUICommonEvent:IsDownloadPNGExsit(filename)
  return ...
end
function GameUICommonEvent:OnEraseFromGameState()
  self:UnloadFlashObject()
end
function GameUICommonEvent:MoveIn()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "animationMoveIn")
end
function GameUICommonEvent:Update(dt)
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "OnUpdate")
  flash_obj:Update(dt)
  GameUICommonEvent:UpdateItemCDTime()
end
function GameUICommonEvent:UpdateItemCDTime()
  local timeString = ""
  local flash_obj = self:GetFlashObject()
  if flash_obj == nil then
    return
  end
  if GameUICommonEvent.receiveFestivalInfoTime ~= -1 then
    local _lefttime = GameUICommonEvent.leftTime - (os.time() - GameUICommonEvent.receiveFestivalInfoTime)
    if _lefttime < 0 then
      _lefttime = 0
      timeString = GameLoader:GetGameText("LC_MENU_EVENT_OVER_BUTTON")
    else
      timeString = GameUtils:formatTimeStringAsTwitterStyle(_lefttime)
    end
    flash_obj:InvokeASCallback("_root", "setItemCDTime", timeString)
  end
end
function GameUICommonEvent:RequestCall()
  DebugOut("request call ", GameUICommonEvent.fleetInfo.buyId)
  local reqType = GameUICommonEvent.NetMegType.FESTIVAL
  if GameUICommonEvent.curDetailType == GameUICommonEvent.DetailType.WD_EVENT then
    reqType = GameUICommonEvent.NetMegType.WD_EVENT
  end
  local shop_purchase_req_param = {
    store_type = reqType,
    id = GameUICommonEvent.fleetInfo.buyId,
    buy_times = 1
  }
  NetMessageMgr:SendMsg(NetAPIList.activity_stores_buy_req.Code, shop_purchase_req_param, GameUICommonEvent.BuyItemCallback, true, nil)
end
function GameUICommonEvent.BuyItemCallback(msgType, content)
  DebugOut("call ack")
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.activity_stores_buy_req.Code then
    GameWaiting:HideLoadingScreen()
    if content.api == NetAPIList.activity_stores_buy_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      else
        GameUtils:ShowTips(GameLoader:GetGameText("LC_MENU_EVENT_XMARS_GIFT_SUC_CHAR"))
      end
    end
    return true
  end
  return false
end
function GameUICommonEvent:OnFSCommand(cmd, arg)
  if cmd == "chris_store" then
    GameStateManager:GetCurrentGameState():AddObject(GameCommonGoodsList)
  elseif cmd == "item_released" then
    local id = GameUICommonEvent.fleetInfo["id" .. arg]
    local item = {}
    item.number = id
    item.cnt = 1
    item.no = 1
    item.item_type = "item"
    ItemBox:showItemBox("Item", item, item.number, 320, 480, 200, 200, "", nil, "", nil)
  elseif cmd == "rewardItem" then
    do
      local param = LuaUtils:string_split(arg, ",")
      DebugTable(param)
      local tmp = GameUICommonEvent.rewardsList[tonumber(param[1]) - 2][tonumber(param[2])]
      if tmp == nil then
        return
      end
      if tmp.item_type ~= "item" and tmp.item_type ~= "krypton" then
        return
      end
      local item = {}
      item.number = tmp.number
      if tmp.item_type == "item" then
        item.cnt = 1
        item.no = 1
        item.item_type = "item"
        ItemBox:showItemBox("Item", item, item.number, 320, 480, 200, 200, "", nil, "", nil)
      else
        local detail = GameUIKrypton:TryQueryKryptonDetail(item.number, tmp.level, function(msgType, content)
          local ret = GameUIKrypton.QueryKryptonDetailCallBack(msgType, content)
          if ret then
            local tmp = GameGlobalData:GetKryptonDetail(item.number, tmp.level)
            ItemBox:SetKryptonBox(tmp, item.number)
            local operationTable = {}
            operationTable.btnUnloadVisible = false
            operationTable.btnUpgradeVisible = false
            operationTable.btnUseVisible = false
            operationTable.btnDecomposeVisible = false
            ItemBox:ShowKryptonBox(320, 240, operationTable)
          end
          return ret
        end)
        if detail == nil then
        else
          ItemBox:SetKryptonBox(detail, item.number)
          local operationTable = {}
          operationTable.btnUnloadVisible = false
          operationTable.btnUpgradeVisible = false
          operationTable.btnUseVisible = false
          operationTable.btnDecomposeVisible = false
          ItemBox:ShowKryptonBox(320, 240, operationTable)
        end
      end
    end
  elseif cmd == "fleet_released" then
    local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
    ItemBox:ShowCommanderDetail2(GameUICommonEvent.fleetInfo.fleetId)
  elseif cmd == "CallRelease" then
    self:RequestCall()
  elseif cmd == "close_reward" then
    GameStateManager:GetCurrentGameState():EraseObject(self)
  elseif cmd == "needUpdateShopItem" then
    self:UpdateLeaderboardItem(arg)
  elseif cmd == "needUpdateInfoItem" then
    self:UpdateInfoItem(arg)
  elseif cmd == "needUpdateRewardItem" then
    self:UpdateRewardItem(arg)
  elseif cmd == "leaderboardTabSelect" then
    self.leaderboardTab = tonumber(arg)
    self:RefreshLeaderboard()
  end
end
function GameUICommonEvent:UpdateLeaderboardItem(id)
  local itemKey = tonumber(id)
  DebugOut("updateleaderitem")
  local curLeaderboard
  if GameUICommonEvent.leaderboardTab == 1 then
    curLeaderboard = GameUICommonEvent.playerLeaderboard
  else
    curLeaderboard = GameUICommonEvent.starAllianceLeaderboard
  end
  if curLeaderboard == nil then
    return
  end
  if itemKey > #curLeaderboard then
    return
  end
  local rank = 0
  local name, turkey = "", ""
  local v1, v2
  local itemInfo = curLeaderboard[itemKey]
  local allianceName = ""
  if GameUICommonEvent.leaderboardTab == 1 then
    name = GameUtils:GetUserDisplayName(itemInfo.name)
    turkey = "" .. itemInfo.box_cnt
    rank = tonumber(itemInfo.rank)
    allianceName = itemInfo.alliance_name
  else
    name = itemInfo.alliance.name
    turkey = "" .. itemInfo.box_cnt
    rank = tonumber(itemInfo.rank)
  end
  if allianceName == "" then
    allianceName = "-"
  end
  DebugOut("rank item", name, turkey, rank)
  self:GetFlashObject():InvokeASCallback("_root", "setItem", itemKey, rank, name, turkey, allianceName, GameUICommonEvent.leaderboardTab)
end
function GameUICommonEvent:RefreshLeaderboard()
  GameUICommonEvent:GetFlashObject():InvokeASCallback("_root", "setLeaderboardTab", GameUICommonEvent.leaderboardTab)
  GameUICommonEvent:GetFlashObject():InvokeASCallback("_root", "clearListItem")
  local curLeaderboard
  if GameUICommonEvent.leaderboardTab == 1 then
    DebugOut("rank player")
    curLeaderboard = GameUICommonEvent.playerLeaderboard
  else
    DebugOut("rank alliance")
    curLeaderboard = GameUICommonEvent.starAllianceLeaderboard
  end
  if curLeaderboard ~= nil then
    local itemCount = #curLeaderboard
    DebugOut("rank item count ", itemCount)
    GameUICommonEvent:GetFlashObject():InvokeASCallback("_root", "initListItem")
    for i = 1, itemCount do
      GameUICommonEvent:GetFlashObject():InvokeASCallback("_root", "addListItem", i)
    end
    GameUICommonEvent:GetFlashObject():InvokeASCallback("_root", "SetArrowInitVisible")
  end
end
function GameUICommonEvent:RefreshMyRank()
  if not self.isLeaderboardOn then
    return
  end
  if GameUICommonEvent.myLeaderboard ~= nil and GameUICommonEvent:GetFlashObject() ~= nil then
    local rank = GameUICommonEvent.myLeaderboard.rank
    if rank > 9999 then
      rank = 9999
    end
    GameUICommonEvent:GetFlashObject():InvokeASCallback("_root", "displayMyRank", GameUtils:GetUserDisplayName(GameUICommonEvent.myLeaderboard.name), "" .. GameUICommonEvent.myLeaderboard.box_cnt, rank)
  else
    local player_info = GameGlobalData:GetUserInfo()
    local myName = GameUtils:GetUserDisplayName(player_info.name)
    GameUICommonEvent:GetFlashObject():InvokeASCallback("_root", "displayMyRank", myName, "-", 0)
  end
end
function GameUICommonEvent.FetchLeaderboardCallback(msgType, content)
  if msgType == NetAPIList.thanksgiven_rank_ack.Code or msgType == NetAPIList.wd_ranking_rank_ack.Code then
    GameUICommonEvent.playerLeaderboard = nil
    GameUICommonEvent.starAllianceLeaderboard = nil
    GameUICommonEvent.myLeaderboard = nil
    DebugOut("event rank")
    DebugTable(content)
    if #content.players > 0 then
      GameUICommonEvent.playerLeaderboard = content.players
      local SortCallback = function(a, b)
        return a.rank < b.rank
      end
      table.sort(GameUICommonEvent.playerLeaderboard, SortCallback)
    end
    DebugTable(GameUICommonEvent.playerLeaderboard)
    if 0 < #content.alliances then
      GameUICommonEvent.starAllianceLeaderboard = content.alliances
      local SortCallback = function(a, b)
        return a.rank < b.rank
      end
      table.sort(GameUICommonEvent.starAllianceLeaderboard, SortCallback)
    end
    GameUICommonEvent.myLeaderboard = content.self_info
    GameUICommonEvent:RefreshLeaderboard()
    GameUICommonEvent:RefreshMyRank()
    return true
  end
  return false
end
function GameUICommonEvent.bannerDownloaderCallback(extInfo)
  if DynamicResDownloader:IfResExsit(extInfo.localPath) and GameUICommonEvent:GetFlashObject() then
    GameUICommonEvent:GetFlashObject():ReplaceTexture(extInfo.replaceName, extInfo.pngName)
    if extInfo.callback then
      extInfo.callback()
    end
  end
end
function GameUICommonEvent.donamicDownloadFinishCallback1(extInfo)
  if DynamicResDownloader:IfResExsit(extInfo.localPath) and GameUICommonEvent:GetFlashObject() then
    GameUICommonEvent.fleetInfo[extInfo.frame] = "item_" .. GameUICommonEvent.fleetInfo[extInfo.id]
    GameUICommonEvent:SetFleetInfoIcon()
  end
end
function GameUICommonEvent.donamicDownloadFinishCallback3(extInfo)
  if DynamicResDownloader:IfResExsit(extInfo.localPath) and GameUICommonEvent:GetFlashObject() then
    GameUICommonEvent.UpdateRewardItem(extInfo.id)
  end
end
function GameUICommonEvent:AddDownloadPath(itemID, id, frame, callBack)
  local resName = itemID .. ".png"
  local extInfo = {}
  extInfo.item_id = itemID
  extInfo.localPath = "data2/LAZY_LOAD_dynamic_" .. resName
  extInfo.id = id
  extInfo.frame = frame
  DynamicResDownloader:AddDynamicRes(resName, DynamicResDownloader.resType.PIC, extInfo, callBack)
end
