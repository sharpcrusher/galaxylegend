local addition = GameData.enchant.addition
table.insert(addition, {
  player_level = 55,
  enchant_level = 9,
  addition = 0
})
table.insert(addition, {
  player_level = 55,
  enchant_level = 19,
  addition = 0
})
table.insert(addition, {
  player_level = 55,
  enchant_level = 29,
  addition = 0
})
table.insert(addition, {
  player_level = 55,
  enchant_level = 39,
  addition = 0
})
table.insert(addition, {
  player_level = 55,
  enchant_level = 49,
  addition = 0
})
table.insert(addition, {
  player_level = 55,
  enchant_level = 59,
  addition = 0
})
table.insert(addition, {
  player_level = 55,
  enchant_level = 69,
  addition = 0
})
table.insert(addition, {
  player_level = 55,
  enchant_level = 79,
  addition = 0
})
table.insert(addition, {
  player_level = 55,
  enchant_level = 89,
  addition = 0
})
table.insert(addition, {
  player_level = 55,
  enchant_level = 99,
  addition = 0
})
table.insert(addition, {
  player_level = 65,
  enchant_level = 9,
  addition = 25
})
table.insert(addition, {
  player_level = 65,
  enchant_level = 19,
  addition = 25
})
table.insert(addition, {
  player_level = 65,
  enchant_level = 29,
  addition = 0
})
table.insert(addition, {
  player_level = 65,
  enchant_level = 39,
  addition = 0
})
table.insert(addition, {
  player_level = 65,
  enchant_level = 49,
  addition = 0
})
table.insert(addition, {
  player_level = 65,
  enchant_level = 59,
  addition = 0
})
table.insert(addition, {
  player_level = 65,
  enchant_level = 69,
  addition = 0
})
table.insert(addition, {
  player_level = 65,
  enchant_level = 79,
  addition = 0
})
table.insert(addition, {
  player_level = 65,
  enchant_level = 89,
  addition = 0
})
table.insert(addition, {
  player_level = 65,
  enchant_level = 99,
  addition = 0
})
table.insert(addition, {
  player_level = 75,
  enchant_level = 9,
  addition = 50
})
table.insert(addition, {
  player_level = 75,
  enchant_level = 19,
  addition = 50
})
table.insert(addition, {
  player_level = 75,
  enchant_level = 29,
  addition = 20
})
table.insert(addition, {
  player_level = 75,
  enchant_level = 39,
  addition = 20
})
table.insert(addition, {
  player_level = 75,
  enchant_level = 49,
  addition = 0
})
table.insert(addition, {
  player_level = 75,
  enchant_level = 59,
  addition = 0
})
table.insert(addition, {
  player_level = 75,
  enchant_level = 69,
  addition = 0
})
table.insert(addition, {
  player_level = 75,
  enchant_level = 79,
  addition = 0
})
table.insert(addition, {
  player_level = 75,
  enchant_level = 89,
  addition = 0
})
table.insert(addition, {
  player_level = 75,
  enchant_level = 99,
  addition = 0
})
table.insert(addition, {
  player_level = 85,
  enchant_level = 9,
  addition = 75
})
table.insert(addition, {
  player_level = 85,
  enchant_level = 19,
  addition = 75
})
table.insert(addition, {
  player_level = 85,
  enchant_level = 29,
  addition = 30
})
table.insert(addition, {
  player_level = 85,
  enchant_level = 39,
  addition = 30
})
table.insert(addition, {
  player_level = 85,
  enchant_level = 49,
  addition = 20
})
table.insert(addition, {
  player_level = 85,
  enchant_level = 59,
  addition = 20
})
table.insert(addition, {
  player_level = 85,
  enchant_level = 69,
  addition = 0
})
table.insert(addition, {
  player_level = 85,
  enchant_level = 79,
  addition = 0
})
table.insert(addition, {
  player_level = 85,
  enchant_level = 89,
  addition = 0
})
table.insert(addition, {
  player_level = 85,
  enchant_level = 99,
  addition = 0
})
table.insert(addition, {
  player_level = 90,
  enchant_level = 9,
  addition = 100
})
table.insert(addition, {
  player_level = 90,
  enchant_level = 19,
  addition = 100
})
table.insert(addition, {
  player_level = 90,
  enchant_level = 29,
  addition = 50
})
table.insert(addition, {
  player_level = 90,
  enchant_level = 39,
  addition = 50
})
table.insert(addition, {
  player_level = 90,
  enchant_level = 49,
  addition = 30
})
table.insert(addition, {
  player_level = 90,
  enchant_level = 59,
  addition = 30
})
table.insert(addition, {
  player_level = 90,
  enchant_level = 69,
  addition = 20
})
table.insert(addition, {
  player_level = 90,
  enchant_level = 79,
  addition = 20
})
table.insert(addition, {
  player_level = 90,
  enchant_level = 89,
  addition = 0
})
table.insert(addition, {
  player_level = 90,
  enchant_level = 99,
  addition = 0
})
