local GameUIStarSystemPort = LuaObjectManager:GetLuaObject("GameUIStarSystemPort")
local GameUIStarSystemPort_SUB = LuaObjectManager:GetLuaObject("GameUIStarSystemPort_SUB")
GameUIStarSystemPort.gacha = {}
local GameUIStarSystemGacha = GameUIStarSystemPort.gacha
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameVip = LuaObjectManager:GetLuaObject("GameVip")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
local GameStateStore = GameStateManager.GameStateStore
local DynamicResDownloader = DynamicResDownloader
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIStarSystemRecycle = GameUIStarSystemPort.uiRecycle
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local GameUIStarSystemHandbook = LuaObjectManager:GetLuaObject("GameUIStarSystemHandbook")
local isProgressPlayOver = false
local isBuyOver = false
function GameUIStarSystemGacha:GetFlashObject()
  return (...), GameUIStarSystemPort_SUB
end
function GameUIStarSystemGacha:Hide()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "HideGacha")
  self.flashResdata = nil
  if self.close_callback then
    self.close_callback()
    self.close_callback = nil
  end
end
function GameUIStarSystemGacha:OnFSCommand(cmd, arg)
  if cmd == "close_Gacha" then
    GameUIStarSystemPort.curSelect = nil
    self:Hide()
    if GameUIStarSystemHandbook.gotoPrev then
      GameUIStarSystemHandbook.gotoPrev = false
      GameUIStarSystemHandbook:ComeBack()
    end
  elseif cmd == "EnableSkipStarSystemAnim" then
    local b = false
    if arg == "1" then
      b = true
    end
    if TutorialQuestManager.QuestTutorialStarSystemSkipAnim:IsActive() then
      DebugOut("EnableSkipStarSystemAnim_IsActive")
      TutorialQuestManager.QuestTutorialStarSystemSkipAnim:SetFinish(true)
    end
    GameUtils:SetEnableSkipStarSystemAnim(b)
  elseif cmd == "look_reward" then
    self:MedelLookReq(tonumber(arg))
  elseif cmd == "update_list_item" then
    self:updateListItem(tonumber(arg))
  elseif cmd == "buy_one" then
    local GameUICardAnim = LuaObjectManager:GetLuaObject("GameUICardAnim")
    if TutorialQuestManager.QuestTutorialEnterStarSystem:IsActive() and GameGlobalData:GetModuleStatus("medal") then
      self:GetFlashObject():InvokeASCallback("_root", "showGachaOneTip", false)
      self:GetFlashObject():InvokeASCallback("_root", "showBattleCardCloseTip", true)
      GameUICardAnim:SetIsNeedShowTip(true)
      TutorialQuestManager.QuestTutorialEnterStarSystem:SetFinish(true, true)
      TutorialQuestManager.QuestTutorialEquipStarSystemItem:SetActive(true, true)
      TutorialQuestManager.QuestTutorialStarSystemTujian:SetActive(true, true)
      TutorialQuestManager:Save()
      GameUIStarSystemPort.isPlayGachaTutorial = false
    end
    self:MedelReq(tonumber(arg), 1)
  elseif cmd == "buy_ten" then
    self:MedelReq(tonumber(arg), 10)
  elseif cmd == "close_buy" then
    GameUIStarSystemGacha.curShowUI = "main"
  elseif cmd == "select_tab" then
    self:SetLookReward(tonumber(arg))
  elseif cmd == "update_look_list_item" then
    self:updateLookItemList(tonumber(arg))
  elseif cmd == "ShowDetail" then
    local itemType, index = unpack(LuaUtils:string_split(arg, ":"))
    local consume = {}
    local type = ""
    local e_quality = 0
    if itemType == "medal_item" then
      type = GameUIStarSystemGacha.itemList[tonumber(index)].item_reward.number
      if type > 999 then
        e_quality = 2
      else
        e_quality = type % 10
      end
      local item = {
        item_type = itemType,
        number = type,
        no = 1,
        quality = e_quality
      }
      ItemBox:showItemBox("Medal", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
    elseif itemType == "medal" then
      GameUIStarSystemRecycle:showMedalDetail(GameUIStarSystemGacha.medalList[tonumber(index)].medal)
    end
  elseif cmd == "ShowResultDetail" then
    local itemType, index = unpack(LuaUtils:string_split(arg, ":"))
    local type = ""
    local e_quality = 0
    local reward = GameUIStarSystemGacha.curResultList[tonumber(index)]
    type = reward.item_reward.number
    if reward.type == 0 then
      if type > 999 then
        e_quality = 2
      else
        e_quality = type % 10
      end
      local item = {
        item_type = itemType,
        number = type,
        no = 1,
        quality = e_quality
      }
      ItemBox:showItemBox("Medal", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
    else
      GameUIStarSystemRecycle:showMedalDetail(reward.medal)
    end
  elseif cmd == "ShowHelpInfo" then
    local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_HELP_MEDAL_GLORY_ROAD"))
  elseif cmd == "TuFinish" then
    if TutorialQuestManager.QuestTutorialEnterStarSystem:IsActive() and GameGlobalData:GetModuleStatus("medal") then
      TutorialQuestManager.QuestTutorialEnterStarSystem:SetFinish(true, true)
      TutorialQuestManager.QuestTutorialStarSystemTujian:SetActive(true, true)
      TutorialQuestManager.QuestTutorialEquipStarSystemItem:SetActive(true, true)
      TutorialQuestManager:Save()
    end
  elseif cmd == "loading_over" then
    isProgressPlayOver = true
    if isBuyOver == true then
      self:ShowBuyResult()
      isProgressPlayOver = false
      isBuyOver = false
    end
  elseif cmd == "shouldShowWhich" then
    isProgressPlayOver = true
    if isBuyOver == true then
      self:ShowBuyResult()
      isProgressPlayOver = false
      isBuyOver = false
    end
  elseif cmd == "on_erase" then
    isProgressPlayOver = false
    isBuyOver = false
  end
end
function GameUIStarSystemGacha:ShowBuyResult()
  local GameUICardAnim = LuaObjectManager:GetLuaObject("GameUICardAnim")
  if not GameStateManager.GameStateStarSystem:IsObjectInState(GameUICardAnim) then
    GameStateManager.GameStateStarSystem:AddObject(GameUICardAnim)
  end
  GameUICardAnim:ShowAnim(self.curShowUI, self.flashResdata)
end
function GameUIStarSystemGacha:Show(close_callback)
  GameUIStarSystemGacha:GetFlashObject():InvokeASCallback("_root", "initEnableSkipStarSystemAnim", GameSettingData.EnableSkipStarSystemAnim)
  if not TutorialQuestManager.QuestTutorialStarSystemSkipAnim:IsFinished() then
    DebugOut("GameUIStarSystemGacha_IsFinished")
    TutorialQuestManager.QuestTutorialStarSystemSkipAnim:SetActive(true)
    GameUIStarSystemGacha:GetFlashObject():InvokeASCallback("_root", "showGachaSkipTip", true)
  else
    GameUIStarSystemGacha:GetFlashObject():InvokeASCallback("_root", "showGachaSkipTip", false)
  end
  if not self._MedalDataList then
    local param = {type = "medal"}
    NetMessageMgr:SendMsg(NetAPIList.recruit_list_new_req.Code, param, self._NetMessageCallback, true, nil)
  else
    self:UpdateMedalList()
  end
  GameTimer:Add(self._OnTimerTick, 1000)
  self.close_callback = close_callback
end
function GameUIStarSystemGacha._NetMessageCallback(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.recruit_list_new_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  if msgtype == NetAPIList.recruit_list_new_ack.Code then
    DebugOutPutTable(content, "GameUIStarSystemGacha._NetMessageCallback")
    GameUIStarSystemGacha:GenerateMedelList(content)
    return true
  end
  return false
end
function GameUIStarSystemGacha:GenerateMedelList(content)
  local data = {}
  for k, v in ipairs(content.recruit_list or {}) do
    local item = {}
    item.buyItemFrame = GameHelper:GetAwardTypeIconFrameName(v.buy_item.item_type, v.buy_item.number, v.buy_item.no)
    item.buyNumber = GameHelper:GetAwardCount(v.buy_item.item_type, v.buy_item.number, v.buy_item.no)
    item.priceOneFrame = GameHelper:GetAwardTypeIconFrameName(v.buy_price_one.item_type, v.buy_price_one.number, v.buy_price_one.no)
    item.priceOneNumber = GameHelper:GetAwardCount(v.buy_price_one.item_type, v.buy_price_one.number, v.buy_price_one.no)
    item.priceTenFrame = GameHelper:GetAwardTypeIconFrameName(v.buy_price_ten.item_type, v.buy_price_ten.number, v.buy_price_ten.no)
    item.priceTenNumber = GameHelper:GetAwardCount(v.buy_price_ten.item_type, v.buy_price_ten.number, v.buy_price_ten.no)
    item.baseTime = os.time()
    item.nextFreeTime = v.next_free_time
    item.leftFreeCount = v.free_count
    item.max_free_count = v.max_free_count
    item.headImage = v.image
    item.detailContent = GameLoader:GetGameText("LC_MENU_DESC_MEDAL_RECRUIT_" .. v.desc_key)
    item.desc = GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_CARD_TIP_" .. v.desc_key)
    item.nextDesc = GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_GET_COMMANDER_" .. v.desc_key)
    item.buyMustText = string.gsub(GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_MUST_GET"), "<number1>", "<font color='#FFCC00'>" .. v.must_count .. "</font>")
    item.activityTime = v.activity_time
    item.isActivity = v.is_activity
    item.id = v.id
    item.buyText = GameLoader:GetGameText("LC_MENU_WELFAREE_PURCHASE_BUTTON")
    item.buyOneText = string.gsub(GameLoader:GetGameText("LC_MENU_MEDAL_RECRUIT_PURCHASE"), "<number>", "1")
    item.buyTenText = string.gsub(GameLoader:GetGameText("LC_MENU_MEDAL_RECRUIT_PURCHASE"), "<number>", "10")
    item.freeText = GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_FREE")
    item.loadText = GameLoader:GetGameText("LC_MENU_NEWS_DOWNLOAD_CHAR")
    item.bgImg = v.img
    item.baseData = v
    item.item_title = GameLoader:GetGameText("LC_MENU_MEDAL_CARD_NAME_" .. v.desc_key)
    item.item_desc = GameLoader:GetGameText("LC_MENU_MEDAL_CARD_DESC_" .. v.desc_key)
    item.imgExist = true
    data[#data + 1] = item
  end
  self._MedalDataList = data
  self:UpdateMedalList()
end
function GameUIStarSystemGacha:updateListItem(itemIndex)
  local itemdata = self._MedalDataList[itemIndex]
  if itemdata then
    local localPath = "data2/" .. DynamicResDownloader:GetFullName(itemdata.bgImg, DynamicResDownloader.resType.WELCOME_PIC)
    DebugOut("GameUIStarSystemGacha:updateListItem:", localPath, DynamicResDownloader:IsPngCrcCorrect(localPath), ext.crc32.crc32(localPath), type(ext.crc32.crc32(localPath)))
    if ext.crc32.crc32(localPath) ~= "" then
      itemdata.imgExist = true
    else
      local extendInfo = {}
      extendInfo.img = itemdata.bgImg
      extendInfo.itemIndex = itemIndex
      DynamicResDownloader:AddDynamicRes(itemdata.bgImg, DynamicResDownloader.resType.WELCOME_PIC, extendInfo, GameUIStarSystemGacha.DynamicBackgroundCallback)
      itemdata.imgExist = false
    end
    local extInfo_one = {}
    extInfo_one.index = itemIndex
    extInfo_one.frame = GameHelper:GetAwardTypeIconFrameName(itemdata.baseData.buy_price_one.item_type, itemdata.baseData.buy_price_one.number, itemdata.baseData.buy_price_one.no)
    itemdata.priceOneFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(itemdata.baseData.buy_price_one, extInfo_one, GameUIStarSystemGacha.priceOneCallBack)
    local extInfo_ten = {}
    extInfo_ten.index = k
    extInfo_ten.frame = GameHelper:GetAwardTypeIconFrameName(itemdata.baseData.buy_price_ten.item_type, itemdata.baseData.buy_price_ten.number, itemdata.baseData.buy_price_ten.no)
    itemdata.priceTenFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(itemdata.baseData.buy_price_ten, extInfo_ten, GameUIStarSystemGacha.priceTenCallBack)
    DebugOutPutTable(itemdata, "updateListItem :" .. itemIndex)
    self:GetFlashObject():InvokeASCallback("_root", "updateListItem", itemIndex, itemdata)
    GameUIStarSystemGacha._OnTimerTick()
  end
end
function GameUIStarSystemGacha.DynamicBackgroundCallback(extinfo)
  DebugTable(extinfo)
  DebugOut("ameUIStarSystemGacha.DynamicBackgroundCallback")
  if GameUIStarSystemGacha:GetFlashObject() then
    GameUIStarSystemGacha:GetFlashObject():OpenTextureForLua(extinfo.img)
    GameUIStarSystemGacha:GetFlashObject():InvokeASCallback("_root", "setItemBg", extinfo.itemIndex, extinfo.img)
  end
end
function GameUIStarSystemGacha:MedelReq(id, count)
  local function callback()
    local param = {}
    param.id = id
    param.count = count
    NetMessageMgr:SendMsg(NetAPIList.recruit_req.Code, param, self.MedelCallBack, true, nil)
    GameUtils:RecordForTongdui(NetAPIList.recruit_req.Code, param, self.MedelCallBack, true, nil)
  end
  local price = 0
  local item = GameUIStarSystemGacha:GetItemByID(id)
  if count == 1 and item and item.priceOneFrame == "credit" then
    price = item.priceOneNumber
  elseif count == 10 and item and item.priceTenFrame == "credit" then
    price = item.priceTenNumber
  end
  if GameSettingData.EnablePayRemind and price > 0 then
    local text_content = GameLoader:GetGameText("LC_MENU_MEDAL_CARD_PAY_WARNING")
    text_content = string.gsub(text_content, "<number1>", tostring(price))
    text_content = string.gsub(text_content, "<number2>", tostring(count))
    local cancelcallback = function()
      local GameUICardAnim = LuaObjectManager:GetLuaObject("GameUICardAnim")
      if GameStateManager.GameStateStarSystem:IsObjectInState(GameUICardAnim) then
        GameUICardAnim:OnFSCommand("ConfirmQuit")
      end
    end
    GameUtils:CreditCostConfirm(text_content, callback, nil, cancelcallback)
  else
    callback()
  end
end
function GameUIStarSystemGacha.MedelCallBack(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.recruit_req.Code then
    if content.code ~= 0 then
      if content.code == 8604 then
        local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
        local text = AlertDataList:GetTextFromErrorCode(130)
        GameUIKrypton.NeedMoreMoney(text)
      else
        local GameVip = LuaObjectManager:GetLuaObject("GameVip")
        GameVip:CheckIsNeedShowVip(content.code)
      end
      local GameUICardAnim = LuaObjectManager:GetLuaObject("GameUICardAnim")
      if GameStateManager.GameStateStarSystem:IsObjectInState(GameUICardAnim) then
        GameUICardAnim:OnFSCommand("ConfirmQuit")
      end
    end
    return true
  elseif msgtype == NetAPIList.recruit_ack.Code then
    DebugOut("MedelCallBack")
    DebugTable(content)
    local id = content.recruit_info.id
    local itemData, index = GameUIStarSystemGacha:GetItemDataById(id)
    local item = GameUIStarSystemGacha:GenerateItemData(content.recruit_info)
    if itemData then
      DebugTable(item)
      GameUIStarSystemGacha._MedalDataList[index] = item
    end
    if #content.rewards > 1 then
      GameUIStarSystemGacha:SetTenReward(item, content.rewards)
    else
      GameUIStarSystemGacha:SetOneReward(item, content.rewards)
    end
    GameUIStarSystemGacha:updateListItem(index)
    GameUIStarSystemGacha:ShowBuyResult()
    return true
  end
  return false
end
function GameUIStarSystemGacha:GenerateItemData(itemdata)
  local v = itemdata
  local item = {}
  item.buyItemFrame = GameHelper:GetAwardTypeIconFrameName(v.buy_item.item_type, v.buy_item.number, v.buy_item.no)
  item.buyNumber = GameHelper:GetAwardCount(v.buy_item.item_type, v.buy_item.number, v.buy_item.no)
  item.priceOneFrame = GameHelper:GetAwardTypeIconFrameName(v.buy_price_one.item_type, v.buy_price_one.number, v.buy_price_one.no)
  item.priceOneNumber = GameHelper:GetAwardCount(v.buy_price_one.item_type, v.buy_price_one.number, v.buy_price_one.no)
  item.priceTenFrame = GameHelper:GetAwardTypeIconFrameName(v.buy_price_ten.item_type, v.buy_price_ten.number, v.buy_price_ten.no)
  item.priceTenNumber = GameHelper:GetAwardCount(v.buy_price_ten.item_type, v.buy_price_ten.number, v.buy_price_ten.no)
  item.baseTime = os.time()
  item.nextFreeTime = v.next_free_time
  item.leftFreeCount = v.free_count
  item.desc = GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_CARD_TIP_" .. v.desc_key)
  item.nextDesc = GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_GET_COMMANDER_" .. v.desc_key)
  item.buyMustText = string.gsub(GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_MUST_GET"), "<number1>", "<font color='#FFCC00'>" .. v.must_count .. "</font>")
  item.activityTime = v.activity_time
  item.isActivity = v.is_activity
  item.id = v.id
  item.buyText = GameLoader:GetGameText("LC_MENU_WELFAREE_PURCHASE_BUTTON")
  item.buyOneText = string.gsub(GameLoader:GetGameText("LC_MENU_MEDAL_RECRUIT_PURCHASE"), "<number>", "1")
  item.buyTenText = string.gsub(GameLoader:GetGameText("LC_MENU_MEDAL_RECRUIT_PURCHASE"), "<number>", "10")
  item.freeText = GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_FREE")
  item.loadText = GameLoader:GetGameText("LC_MENU_NEWS_DOWNLOAD_CHAR")
  item.bgImg = v.img
  item.baseData = v
  item.imgExist = true
  item.item_title = GameLoader:GetGameText("LC_MENU_MEDAL_CARD_NAME_" .. v.desc_key)
  item.item_desc = GameLoader:GetGameText("LC_MENU_MEDAL_CARD_DESC_" .. v.desc_key)
  item.max_free_count = v.max_free_count
  item.headImage = v.image
  item.detailContent = GameLoader:GetGameText("LC_MENU_DESC_MEDAL_RECRUIT_" .. v.desc_key)
  return item
end
function GameUIStarSystemGacha:PopReplayItem(rewardInfo)
  if rewardInfo.is_replacement then
    local baseStr = GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_ALREADY_HAVE_COMMANDER")
    local ownName = GameHelper:GetAwardTypeText(rewardInfo.own_item.item_type, rewardInfo.own_item.number)
    local orinName = GameHelper:GetAwardTypeText(rewardInfo.origin_item.item_type, rewardInfo.origin_item.number)
    local desName = ""
    if rewardInfo.type == 0 or rewardInfo.type == 2 then
      desName = GameHelper:GetAwardText(rewardInfo.item_reward.item_type, rewardInfo.item_reward.number, rewardInfo.item_reward.no)
    else
      desName = FleetDataAccessHelper:GetFleetLevelDisplayName(rewardInfo.fleet_reward.name, rewardInfo.fleet_reward.fleet_id, rewardInfo.fleet_reward.color, 0)
    end
    baseStr = string.gsub(baseStr, "<owned_char>", ownName)
    baseStr = string.gsub(baseStr, "<reward_char>", orinName)
    baseStr = string.gsub(baseStr, "<sub_char>", desName)
    local GameTip = LuaObjectManager:GetLuaObject("GameTip")
    GameTip:Show(baseStr)
  end
end
function GameUIStarSystemGacha:SetOneReward(itemData, rewards)
  local v = rewards[1]
  local data = {}
  data.id = itemData.id
  data.price = itemData.priceOneNumber
  data.priceIcon = itemData.priceOneFrame
  data.buytip = string.gsub(GameLoader:GetGameText("LC_MENU_MEDAL_RECRUIT_PURCHASE_MORE"), "<number>", "1")
  local Fleft = itemData.nextFreeTime - (os.time() - itemData.baseTime)
  if Fleft <= 0 and 0 < itemData.leftFreeCount then
    data.isFree = true
    data.freeText = itemData.freeText .. " " .. itemData.leftFreeCount .. "/" .. itemData.max_free_count
  end
  data.reward = {}
  self:PopReplayItem(v)
  if v.type == 0 then
    ty = 1
    data.reward.iconFrame = "medal_item_" .. v.item_reward.number
    data.reward.picName = v.item_reward.number .. ".png"
    data.reward.nameText = GameHelper:GetAwardTypeText(v.item_reward.item_type, v.item_reward.number)
    data.reward.Count = GameHelper:GetAwardCount(v.item_reward.item_type, v.item_reward.number, v.item_reward.no)
    data.reward.itemType = "medal_item"
    data.reward.item_reward = v.item_reward
    if v.item_reward.number > 999 then
      data.reward.quality = 2
    else
      data.reward.quality = v.item_reward.number % 10
    end
  else
    ty = 3
    data.reward.iconFrame, data.reward.picName = GameHelper:GetMedalIconFrameAndPicNew(v.item_reward.number)
    data.reward.nameText = GameHelper:GetAwardTypeText("medal", v.item_reward.number)
    data.reward.Lv = v.medal.level
    data.reward.rank = v.medal.rank
    data.reward.quality = v.medal.quality
    data.reward.itemType = "medal"
  end
  data.reward.type = ty
  local redlist = {}
  redlist[1] = data.reward
  GameUIStarSystemGacha.curRewards = redlist
  GameUIStarSystemGacha.curResultList = rewards
  DebugOutPutTable(data, "SetOneRewardInfo")
  GameUIStarSystemGacha.curShowUI = "buyOne"
  GameUIStarSystemGacha.flashResdata = data
end
function GameUIStarSystemGacha:SetTenReward(itemData, rewards)
  local data = {}
  data.id = itemData.id
  data.price = itemData.priceTenNumber
  data.priceIcon = itemData.priceTenFrame
  data.buytip = string.gsub(GameLoader:GetGameText("LC_MENU_MEDAL_RECRUIT_PURCHASE_MORE"), "<number>", "10")
  data.rewards = {}
  for k, v in ipairs(rewards or {}) do
    local item = {}
    self:PopReplayItem(v)
    if v.type == 0 then
      ty = 1
      item.iconFrame = "medal_item_" .. v.item_reward.number
      item.picName = v.item_reward.number .. ".png"
      item.nameText = GameHelper:GetAwardTypeText(v.item_reward.item_type, v.item_reward.number)
      item.Count = GameHelper:GetAwardCount(v.item_reward.item_type, v.item_reward.number, v.item_reward.no)
      item.itemType = "medal_item"
      item.item_reward = v.item_reward
      if v.item_reward.number > 999 then
        item.quality = 2
      else
        item.quality = v.item_reward.number % 10
      end
    else
      ty = 3
      item.iconFrame, item.picName = GameHelper:GetMedalIconFrameAndPicNew(v.item_reward.number)
      item.nameText = GameHelper:GetAwardTypeText("medal", v.item_reward.number)
      item.Lv = v.medal.level
      item.rank = v.medal.rank
      item.quality = v.medal.quality
      item.itemType = "medal"
    end
    item.type = ty
    data.rewards[#data.rewards + 1] = item
  end
  GameUIStarSystemGacha.curRewards = data.rewards
  GameUIStarSystemGacha.curResultList = rewards
  GameUIStarSystemGacha.flashResdata = data
  GameUIStarSystemGacha.curShowUI = "buyTen"
end
function GameUIStarSystemGacha:PopGetFleet(fleetID)
  local item = {}
  item.item_type = "fleet"
  item.number = fleetID
  item.no = 0
  item.level = 1
  item.shared_rewards = {}
  local function callback()
    local GameUIPrestigeRankUp = LuaObjectManager:GetLuaObject("GameUIPrestigeRankUp")
    GameUIPrestigeRankUp:ShowGetCommander(item, false)
  end
  if GameStateManager:GetCurrentGameState():IsObjectInState(ItemBox) and ItemBox.currentBox == "rewardInfo" then
    ItemBox:SetDelayShowAfterReward(callback)
  else
    callback()
  end
end
function GameUIStarSystemGacha:GetItemByID(id)
  for k, v in pairs(self._MedalDataList or {}) do
    if v.id == id then
      return v
    end
  end
  return nil
end
function GameUIStarSystemGacha:GetItemDataById(id)
  for k, v in ipairs(self._MedalDataList or {}) do
    if v.id == id then
      return v, k
    end
  end
  return nil
end
function GameUIStarSystemGacha._OnTimerTick()
  for k, v in pairs(GameUIStarSystemGacha._MedalDataList or {}) do
    local data = {}
    data.isActivity = v.isActivity
    if v.isActivity then
      local Aleft = v.activityTime - (os.time() - v.baseTime)
      if Aleft < 0 then
        Aleft = 0
      end
      data.activityTime = GameLoader:GetGameText("LC_MENU_WELFAREE_END_IN_CHAR") .. GameUtils:formatTimeStringAsTwitterStyleWithOutSecond(Aleft)
    end
    local Fleft = v.nextFreeTime - (os.time() - v.baseTime)
    if Fleft < 0 then
      Fleft = 0
    end
    if Fleft == 0 then
      data.isFree = true
      data.freeTip = GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_CARD_FREE_TIME") .. v.leftFreeCount
    else
      data.isFree = false
      data.freeTip = GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_CARD_COLDDOWN") .. GameUtils:formatTimeStringAsPartion2(Fleft)
    end
    if 0 >= v.leftFreeCount then
      data.freeTip = ""
      data.isFree = false
    end
    data.freeText = GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_FREE")
    if GameUIStarSystemGacha:GetFlashObject() then
      GameUIStarSystemGacha:GetFlashObject():InvokeASCallback("_root", "updateItemTime", k, data)
    end
  end
  return 1000
end
function GameUIStarSystemGacha:MedelLookReq(id)
  local param = {}
  param.id = id
  NetMessageMgr:SendMsg(NetAPIList.recruit_look_req.Code, param, self.MedelLookCallBack, true, nil)
end
function GameUIStarSystemGacha.MedelLookCallBack(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.recruit_look_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgtype == NetAPIList.recruit_look_ack.Code then
    local id = content.id
    DebugOutPutTable(content, "GameUIStarSystemGacha:MedelLookReq")
    GameUIStarSystemGacha.itemList = {}
    GameUIStarSystemGacha.medalList = {}
    for k, v in ipairs(content.rewards) do
      if v.type == 0 then
        GameUIStarSystemGacha.itemList[#GameUIStarSystemGacha.itemList + 1] = v
      else
        GameUIStarSystemGacha.medalList[#GameUIStarSystemGacha.medalList + 1] = v
      end
    end
    local sorMedal = function(a, b)
      if a.medal.quality > b.medal.quality then
        return true
      elseif a.medal.quality < b.medal.quality then
        return false
      elseif a.medal.rank > b.medal.rank then
        return true
      elseif a.medal.rank < b.medal.rank then
        return false
      else
        return a.medal.level > b.medal.level
      end
    end
    table.sort(GameUIStarSystemGacha.medalList, sorMedal)
    GameUIStarSystemGacha:SetLookReward(1)
    GameUIStarSystemGacha:GetFlashObject():InvokeASCallback("_root", "ShowLookUI")
    GameUIStarSystemGacha.curShowUI = "look"
    return true
  end
  return false
end
function GameUIStarSystemGacha:SetLookReward(TabType)
  local data = {}
  local list
  if TabType == 1 then
    list = GameUIStarSystemGacha.medalList
  else
    list = GameUIStarSystemGacha.itemList
  end
  data.tabType = TabType
  data.rewards = {}
  for k, v in ipairs(list or {}) do
    local item = {}
    local ty = 0
    if v.type == 0 then
      ty = 1
      item.iconFrame = "medal_item_" .. v.item_reward.number
      item.picName = v.item_reward.number .. ".png"
      item.nameText = GameHelper:GetAwardTypeText(v.item_reward.item_type, v.item_reward.number)
      item.Count = GameHelper:GetAwardCount(v.item_reward.item_type, v.item_reward.number, v.item_reward.no)
      item.itemType = "medal_item"
      item.item_reward = v.item_reward
      if v.item_reward.number > 999 then
        item.quality = 2
      else
        item.quality = v.item_reward.number % 10
      end
    else
      ty = 3
      item.iconFrame, item.picName = GameHelper:GetMedalIconFrameAndPicNew(v.item_reward.number)
      item.nameText = GameHelper:GetAwardTypeText("medal", v.item_reward.number)
      item.Lv = v.medal.level
      item.rank = v.medal.rank
      item.quality = v.medal.quality
      item.itemType = "medal"
      DebugOutPutTable(item, "SetLookReward  ")
    end
    item.type = ty
    data.rewards[#data.rewards + 1] = item
  end
  GameUIStarSystemGacha.curTabData = data
  GameUIStarSystemGacha.curRewards = data.rewards
  self:GetFlashObject():InvokeASCallback("_root", "initLookList", math.ceil(#data.rewards / 4), TabType)
end
function GameUIStarSystemGacha:updateLookItemList(itemIndex)
  local startIndex = (itemIndex - 1) * 4 + 1
  local endIndex = itemIndex * 4
  local data = {}
  for i = startIndex, endIndex do
    data[#data + 1] = GameUIStarSystemGacha.curTabData.rewards[i]
  end
  self:GetFlashObject():InvokeASCallback("_root", "updateLookListItem", itemIndex, data)
end
function GameUIStarSystemGacha.priceOneCallBack(extInfo)
  DebugOut("priceOneCallBack ")
  DebugTable(extInfo)
  if GameUIStarSystemGacha:GetFlashObject() then
    GameUIStarSystemGacha:GetFlashObject():InvokeASCallback("_root", "setPriceOneFrame", extInfo)
  end
end
function GameUIStarSystemGacha.priceTenCallBack(extInfo)
  if GameUIStarSystemGacha:GetFlashObject() then
  end
end
function GameUIStarSystemGacha:UpdateMedalList()
  DebugOut("UpdateMedalList ff")
  self:GetFlashObject():InvokeASCallback("_root", "initListBox", #self._MedalDataList)
  self:GetFlashObject():InvokeASCallback("_root", "ShowGacha")
  if not TutorialQuestManager.QuestTutorialEnterStarSystem:IsFinished() and GameGlobalData:GetModuleStatus("medal") then
    self:GetFlashObject():InvokeASCallback("_root", "showCardTip", true)
  end
  GameUIStarSystemGacha.SetCreditAndMoney()
end
function GameUIStarSystemGacha.SetCreditAndMoney()
  if not GameUIStarSystemGacha:GetFlashObject() then
    return
  end
  DebugOut("GameUIStarSystemGacha.SetCreditAndMoney")
  local resource = GameGlobalData:GetData("resource")
  GameUIStarSystemGacha:GetFlashObject():InvokeASCallback("_root", "setCreditAndMoney", GameUtils.numberConversion(resource.credit), GameUtils.numberConversion(resource.money))
end
function GameUIStarSystemGacha:OnGachaUpdate(dt)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "OnGachaUpdate", dt)
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUIStarSystemGacha.OnAndroidBack()
    if GameStateStore.CurItemDetail then
      GameStateStore:HideBuyConfirmWin()
    else
      GameUIStarSystemGacha:OnFSCommand("close_Gacha")
    end
  end
end
