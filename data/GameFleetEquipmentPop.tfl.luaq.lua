local GameFleetEquipmentPop = LuaObjectManager:GetLuaObject("GameFleetEquipmentPop")
local GameFleetEquipment = LuaObjectManager:GetLuaObject("GameFleetEquipment")
local GameStateEquipEnhance = GameStateManager.GameStateEquipEnhance
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameVip = LuaObjectManager:GetLuaObject("GameVip")
local GameFleetEquipment = LuaObjectManager:GetLuaObject("GameFleetEquipment")
function GameFleetEquipmentPop:shenjie_showReset(tparam)
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  local resourceTxt = GameLoader:GetGameText("LC_MENU_REFORM_RECOVER_RESOURCE_LABEL")
  local fleetCard = GameFleetEquipment.shenjie.mCurrentFleetEnhanceData:GetRecoverCard()
  if fleetCard and fleetCard.mCurFleetCard and fleetCard.mCurFleetCard.level > 15 then
    resourceTxt = GameLoader:GetGameText("LC_MENU_REFORM_RECOVER_RESOURCE_LABEL_15")
  end
  local t = {}
  t[1] = {}
  t[1].path = "_root.pop_shengjieReset.get_resources.text_desc_get_resources"
  t[1].txt = resourceTxt
  flash:InvokeASCallback("_root", "SetText", t)
  flash:InvokeASCallback("_root", "setRecoverCurCard", tparam.curcard)
  flash:InvokeASCallback("_root", "setRecoverMenuNextCard", tparam.nextcard)
  flash:InvokeASCallback("_root", "InitshenjieReset", {
    listparam = tparam.listparam
  })
  flash:InvokeASCallback("_root", "showPop_shenjieReset")
end
function GameFleetEquipmentPop:shenjie_resetFinished()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "showRecoverFinished")
  local resourceTxt = GameLoader:GetGameText("LC_MENU_REFORM_RECOVER_SUCCESS_RECIEVE_ALERT")
  local t = {}
  t[1] = {}
  t[1].path = "_root.pop_shengjieReset.get_resources.text_desc_get_resources"
  t[1].txt = resourceTxt
  flash:InvokeASCallback("_root", "SetText", t)
end
function GameFleetEquipmentPop:shenjie_hideEvolutionPanel()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "hidePop_EvolvePanel")
end
function GameFleetEquipmentPop:shenjie_showEvolutionPanel(tparam)
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "setEvolveSuccessCurCard", tparam.curcard)
  flash:InvokeASCallback("_root", "setEvolveSuccessNextCard", tparam.nextcard)
  local lang = "en"
  if GameSettingData and GameSettingData.Save_Lang then
    lang = GameSettingData.Save_Lang
    if string.find(lang, "ru") == 1 then
      lang = "ru"
    end
  end
  local desc = GameLoader:GetGameText("LC_MENU_REFORMING_EVOLUTION_EFFECT_INFO")
  print("shenjie_showEvolutionPanel_" .. desc)
  flash:InvokeASCallback("_root", "showPop_EvolvePanel", {lang = lang, desc = desc})
end
function GameFleetEquipmentPop:shenjie_showGrowup(tparam)
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "setUpSuccessCurCard", tparam.curcard)
  flash:InvokeASCallback("_root", "setUpSuccessNextCard", tparam.nextcard)
  flash:InvokeASCallback("_root", "InitshenjieSuccess", {
    listparam = tparam.propparam
  })
  local lang = "en"
  if GameSettingData and GameSettingData.Save_Lang then
    lang = GameSettingData.Save_Lang
    if string.find(lang, "ru") == 1 then
      lang = "ru"
    end
  end
  flash:InvokeASCallback("_root", "showPop_shenjieSuccess", {lang = lang})
end
function GameFleetEquipmentPop:zb_isPopListVisible()
  if not GameFleetEquipmentPop:GetFlashObject() then
    return false
  end
  local ret = GameUtils:GetFlashValue(GameFleetEquipmentPop, "_root.equipment_pop", "_visible")
  return ret == "true"
end
function GameFleetEquipmentPop:zb_isEnhanceVisible()
  if not GameFleetEquipmentPop:GetFlashObject() then
    return false
  end
  local ret = GameUtils:GetFlashValue(GameFleetEquipmentPop, "_root.Pop_intensify", "_visible")
  return ret == "true"
end
function GameFleetEquipmentPop:zb_isEvolutionVisible()
  if not GameFleetEquipmentPop:GetFlashObject() then
    return false
  end
  local ret = GameUtils:GetFlashValue(GameFleetEquipmentPop, "_root.Pop_evolution_content", "_visible")
  return ret == "true"
end
function GameFleetEquipmentPop:zb_isInterveneVisible()
  if not GameFleetEquipmentPop:GetFlashObject() then
    return false
  end
  local ret = GameUtils:GetFlashValue(GameFleetEquipmentPop, "_root.Pop_intervene", "_visible")
  return ret == "true"
end
function GameFleetEquipmentPop:zb_refreshResource_enhance()
  if self:zb_isEnhanceVisible() then
    local resource = GameGlobalData:GetData("resource")
    local ncredit = resource.credit
    local ncoin = resource.money
    local tparam = {}
    tparam[1] = {}
    tparam[1].path = "_root.Pop_intensify.intensify_all.num_cubit"
    tparam[1].txt = GameUtils.numberConversion(ncoin)
    tparam[2] = {}
    tparam[2].path = "_root.Pop_intensify.intensify_all.num_credit"
    tparam[2].txt = GameUtils.numberConversion(ncredit)
    self:GetFlashObject():InvokeASCallback("_root", "SetText", tparam)
  end
end
function GameFleetEquipmentPop:zb_refreshResource_evolution()
  if self:zb_isEvolutionVisible() then
    local resource = GameGlobalData:GetData("resource")
    local ncredit = resource.credit
    local ncoin = resource.money
    local tparam = {}
    tparam[1] = {}
    tparam[1].path = "_root.Pop_evolution_content.evolution_all.num_cubit"
    tparam[1].txt = GameUtils.numberConversion(ncoin)
    tparam[2] = {}
    tparam[2].path = "_root.Pop_evolution_content.evolution_all.num_credit"
    tparam[2].txt = GameUtils.numberConversion(ncredit)
    self:GetFlashObject():InvokeASCallback("_root", "SetText", tparam)
  end
end
function GameFleetEquipmentPop:zb_refreshResource_intervene()
  if self:zb_isInterveneVisible() then
    local nleft = GameGlobalData:GetItemCount(3101)
    local nright = 0
    if self.zb_intervene_additem.item_type == "credit" then
      local resource = GameGlobalData:GetData("resource")
      nright = resource.credit
    else
      nright = GameGlobalData:GetItemCount(self.zb_intervene_additem.number)
    end
    local resource = GameGlobalData:GetData("resource")
    local ncredit = resource.credit
    local ncoin = resource.money
    local tparam = {}
    tparam[1] = {}
    tparam[1].path = "_root.Pop_intervene.intervene_all.num_cubit"
    tparam[1].txt = GameUtils.numberConversion(nleft)
    tparam[2] = {}
    tparam[2].path = "_root.Pop_intervene.intervene_all.num_credit"
    tparam[2].txt = GameUtils.numberConversion(nright)
    self:GetFlashObject():InvokeASCallback("_root", "SetText", tparam)
  end
end
function GameFleetEquipmentPop:zb_setEquipStatus(tparam)
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "SetZhuangbeiStatus", tparam)
end
function GameFleetEquipmentPop:zb_InitZhuangbeiList(tparam)
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "InitZhuangbeiList", tparam)
end
function GameFleetEquipmentPop:zb_changePrimeIcon(number, needcnt, percent, zCount, itemCount, inventedData)
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "changePrimeIcon", number, needcnt, percent, zCount, itemCount, inventedData)
end
function GameFleetEquipmentPop:zb_SetUpgradeMenu(enhanceIconFrame, evolutionIconFrame, materialIconFrame, canEvolution, equip_StrengthenLv, reqText, levelPermit)
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "SetUpgradeMenu", enhanceIconFrame, evolutionIconFrame, materialIconFrame, canEvolution, equip_StrengthenLv, reqText, levelPermit)
end
function GameFleetEquipmentPop:zb_setIntervalData(tparam, lvtxt)
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  local additem = tparam.StrengthenExtParms_info.AdditionItem
  local item = {}
  if additem == 1 then
    item.item_type = "credit"
  else
    item.item_type = "item"
    item.number = additem
    item.no = 1
  end
  self.zb_intervene_additem = item
  flash:InvokeASCallback("_root", "setIntervalData", tparam, lvtxt)
  self:zb_refreshResource_intervene()
end
function GameFleetEquipmentPop:zb_SetUpgradeMenuText(tparam)
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "SetUpgradeMenuText", tparam)
end
function GameFleetEquipmentPop:zb_showEnhance()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  if not GameStateEquipEnhance.IsEnhanceEnable() then
    local txt = GameUtils:TryGetText("LC_MENU_enhance_DESC", "")
    txt = string.format(txt, 4)
    GameTip:Show(txt)
    return
  end
  flash:InvokeASCallback("_root", "ShowEnhance", tparam)
  self:zb_refreshResource_enhance()
end
function GameFleetEquipmentPop:zb_showEvolution()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  if not GameFleetEquipment.zhuangbei:isEvolutionUseable() then
    local level = GameFleetEquipment.zhuangbei:getEvolutionUseablePlayerLevel()
    local txt = GameUtils:TryGetText("LC_MENU_elite_challenge_DESC", "")
    txt = string.format(txt, level)
    GameTip:Show(txt)
    return
  end
  flash:InvokeASCallback("_root", "ShowEvolution", tparam)
  self:zb_refreshResource_evolution()
end
function GameFleetEquipmentPop:zb_showIntervene()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  if not GameFleetEquipment.zhuangbei:IsInterveneEnable() then
    local level = GameDataAccessHelper:GetRightMenuPlayerReqLevel("enchant")
    local txt = GameUtils:TryGetText("LC_MENU_DEXTER_INFO_2", "")
    txt = string.format(txt, level)
    GameTip:Show(txt)
    return
  end
  flash:InvokeASCallback("_root", "ShowIntervene", tparam)
  self:zb_refreshResource_intervene()
end
function GameFleetEquipmentPop:zb_ShowEquipStrengthenChangeArrow(ArrowType, diff_level, diff_effect_value, new_level)
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "ShowEquipStrengthenChangeArrow", ArrowType, diff_level, diff_effect_value, new_level)
end
function GameFleetEquipmentPop:zb_openQuickInterveneUI(titleText, descText, inventedData)
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "openQuickInterveneUI", titleText, descText, inventedData)
end
function GameFleetEquipmentPop:zb_openQuickEnhanceUI(titleText, equipIcon, equipNameText, equipLevelNow, levelCanUp, baseLevelUpConsume, tequipmentAttrParams, money, creditCost, minCreditCost)
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "openQuickEnhanceUI", titleText, equipIcon, equipNameText, equipLevelNow, levelCanUp, baseLevelUpConsume, tequipmentAttrParams, money, creditCost, minCreditCost)
end
function GameFleetEquipmentPop:zb_SetEvolutionTipVisible(isv)
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "SetEvolutionTipVisible", isv)
end
function GameFleetEquipmentPop:zb_SetShowTutEnhance(isv)
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "SetShowTutEnhance", isv)
end
function GameFleetEquipmentPop:zb_SetEvolutionLock(isl)
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "SetEvolutionLock", isl)
end
function GameFleetEquipmentPop:zb_SetQuickEnhanceLock(isl)
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "SetQuickEnhanceLock", isl)
end
function GameFleetEquipmentPop:zb_showEnhanceAnim()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "showEnhanceAnim")
end
function GameFleetEquipmentPop:zb_refreshEnhanceBarRedPoint(isshow, can_enhance, can_evolve, can_enchant)
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "refreshEnhanceBarRedPoint", isshow, can_enhance, can_evolve, can_enchant)
end
function GameFleetEquipmentPop:zb_enableUpgrad(canUpgrade, needPlayerLevel, showWoring)
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "enableUpgrad", canUpgrade, needPlayerLevel, showWoring)
end
function GameFleetEquipmentPop:zb_SetEnableEnhance(enableEnhance, enhanceTip)
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "SetEnableEnhance", enableEnhance, enhanceTip)
end
function GameFleetEquipmentPop:Init()
end
function GameFleetEquipmentPop:OnAddToGameState()
  GameGlobalData:RegisterDataChangeCallback("resource", self.RefreshResource)
  GameGlobalData:RegisterDataChangeCallback("item_count", self.RefreshResource)
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "DEPLOY", GameLoader:GetGameText("LC_MENU_NEW_FLEET_IN_BATTLE"))
end
function GameFleetEquipmentPop.RefreshResource()
  GameFleetEquipmentPop:zb_refreshResource_enhance()
  GameFleetEquipmentPop:zb_refreshResource_evolution()
  GameFleetEquipmentPop:zb_refreshResource_intervene()
end
function GameFleetEquipmentPop:OnEraseFromGameState()
  GameGlobalData:RemoveDataChangeCallback("resource", self.RefreshResource)
  GameGlobalData:RemoveDataChangeCallback("item_count", self.RefreshResource)
end
function GameFleetEquipmentPop:Update(dt)
  if not self:GetFlashObject() then
    return
  end
  self:GetFlashObject():InvokeASCallback("_root", "DoUpdate", dt)
  self:GetFlashObject():Update(dt)
end
function GameFleetEquipmentPop:OnFSCommand(cmd, arg)
  if GameUtils:OnFSCommand(cmd, arg, self) then
    return
  end
  GameTextEdit:fscommand(self:GetFlashObject(), cmd, arg)
  if cmd == "ShowItemDetail" then
    local tinfo = LuaUtils:deserializeTable(arg)
    ItemBox:ShowGameItem(tinfo)
  elseif cmd == "onClick_ShenjieReset_Confirm" then
    GameFleetEquipment.shenjie:shenjie_onClickResetConfirm()
  elseif cmd == "onClick_ShenjieReset_ok" then
  elseif cmd == "onClick_ShenjieSuccess_Confirm" then
  elseif cmd == "onClickEvolve" then
    local function callback()
      GameFleetEquipmentPop:shenjie_hideEvolutionPanel()
      GameFleetEquipment.shenjie:OnFSCommand("onClickGrowupForce")
    end
    local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
    local info = GameLoader:GetGameText("LC_MENU_REFORMING_EVOLUTION_WARNING")
    local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
    GameUIMessageDialog:SetYesButton(callback)
    GameUIMessageDialog:SetNoButton()
    GameUIMessageDialog:Display(text_title, info)
  elseif cmd == "showitemItembox" then
    GameFleetEquipment.zhuangbei:OnFSCommand(cmd, arg)
  elseif cmd == "onClickEquip" then
    GameFleetEquipment.zhuangbei:onClickEquip(arg)
  elseif cmd == "changePrime" then
    GameFleetEquipment.zhuangbei:popChangePrime(arg)
  elseif cmd == "doPrimeNew" then
    GameFleetEquipment.zhuangbei:popDoPrimeNew(arg)
  elseif cmd == "doOneKeyPrimeNew" then
    GameFleetEquipment.zhuangbei:openQuickInterveneUI()
  elseif cmd == "onClick_enhance" then
    self:zb_showEnhance()
  elseif cmd == "onClick_evolution" then
    self:zb_showEvolution()
  elseif cmd == "onClick_intervene" then
    self:zb_showIntervene()
  elseif cmd == "onClick_enhanceClose" then
  elseif cmd == "onClick_evolutionClose" then
  elseif cmd == "onClick_interveneClose" then
  elseif cmd == "sendQuickEnhanceRequire" then
    GameFleetEquipment.zhuangbei:sendQuickEnhanceRequire(arg)
  elseif cmd == "sendQuickInterveneRequire" then
    GameFleetEquipment.zhuangbei:sendQuickIntervene(arg)
  elseif cmd == "Enhance" then
    GameFleetEquipment.zhuangbei:onClickEnhance(arg)
  elseif cmd == "QuickEnhance" then
    GameFleetEquipment.zhuangbei:onClickQuickEnhance(arg)
  elseif cmd == "Evolution" then
    GameFleetEquipment.zhuangbei:onClickEvolution(arg)
  elseif cmd == "update_changefleet_item" then
    self:UpdateChangeFleetsItem(tonumber(arg))
  elseif cmd == "selectFleet" then
    ZhenXinUI():OnChangeFleet(tonumber(arg))
  else
    GameFleetEquipment.chuancang:OnFSCommand(cmd, arg)
  end
end
function GameFleetEquipmentPop:chuancang_clickPoint(arg)
  local itemKey = tonumber(arg)
  self:GetFlashObject():InvokeASCallback("_root", "clickPoint", itemKey)
end
function GameFleetEquipmentPop:chuancang_showPointUpgrade(pointInfo)
  DebugOut("ddqeweqechuancang_showPointUpgrade")
  self:GetFlashObject():InvokeASCallback("_root", "showPointUpgrade", pointInfo)
end
function GameFleetEquipmentPop:chuancang_init()
  self:GetFlashObject():InvokeASCallback("_root", "init")
end
function GameFleetEquipmentPop:chuancang_updatePointUpgrade(pointInfo)
  self:GetFlashObject():InvokeASCallback("_root", "updatePointUpgrade", pointInfo)
end
function GameFleetEquipmentPop:chuancang_showCoreItems(arg1, arg2, arg3)
  self:GetFlashObject():InvokeASCallback("_root", "showCoreItems", arg1, arg2, arg3)
end
function GameFleetEquipmentPop:chuancang_setCoreItem(coreIndex, items)
  self:GetFlashObject():InvokeASCallback("_root", "setCoreItem", coreIndex, items)
end
function GameFleetEquipmentPop:chuancang_showCoreInfo(coreInfo, isNowCore, replaceText, arg)
  self:GetFlashObject():InvokeASCallback("_root", "showCoreInfo", coreInfo, isNowCore, replaceText, arg)
end
function GameFleetEquipmentPop:chuancang_setResetButton(arg1, arg2, arg3, arg4)
  self:GetFlashObject():InvokeASCallback("_root", "setResetButton", arg1, arg2, arg3, arg4)
end
function GameFleetEquipmentPop:chuancang_updateRollTextPos(dt)
  self:GetFlashObject():InvokeASCallback("_root", "updateRollTextPos", dt)
end
function GameFleetEquipmentPop:chuancang_setOneKeyUpBuff(curBufflen)
  self:GetFlashObject():InvokeASCallback("_root", "setOneKeyUpBuff", curBufflen)
end
function GameFleetEquipmentPop:chuancang_updateAllOneKeyUpBuffList()
  self:GetFlashObject():InvokeASCallback("_root", "updateAllOneKeyUpBuffList")
end
function GameFleetEquipmentPop:chuancang_setOneKeyBuffItemData(item, dstItem, itemIndex)
  self:GetFlashObject():InvokeASCallback("_root", "setOneKeyBuffItemData", item, dstItem, itemIndex)
end
function GameFleetEquipmentPop:chuancang_setOneKeyUpName(curName, dstName)
  self:GetFlashObject():InvokeASCallback("_root", "setOneKeyUpName", curName, dstName)
end
function GameFleetEquipmentPop:chuancang_setPopOneUpBar(per)
  self:GetFlashObject():InvokeASCallback("_root", "setPopOneUpBar", per)
end
function GameFleetEquipmentPop:chuancang_setPopOneUpLimit(minNum, maxNum)
  self:GetFlashObject():InvokeASCallback("_root", "setPopOneUpLimit", minNum, maxNum)
end
function GameFleetEquipmentPop:chuancang_setPopOneUpRes(costCount, displayCount, frame)
  self:GetFlashObject():InvokeASCallback("_root", "setPopOneUpRes", costCount, displayCount, frame)
end
function GameFleetEquipmentPop:chuancang_setInputText(str)
  self:GetFlashObject():InvokeASCallback("_root", "setInputText", str)
end
function GameFleetEquipmentPop:chuancang_ShowOneKeyPop()
  self:GetFlashObject():InvokeASCallback("_root", "ShowOneKeyPop")
end
function GameFleetEquipmentPop:chuancang_onUpdateAll(dt)
end
function GameFleetEquipmentPop:chuancang_showPointInfo(arg1, arg2, arg3, arg4, arg5)
  self:GetFlashObject():InvokeASCallback("_root", "showPointInfo", arg1, arg2, arg3, arg4, arg5)
end
function GameFleetEquipmentPop:chuancang_updateResAndResetCost(resetCost)
  self:GetFlashObject():InvokeASCallback("_root", "updateResAndResetCost", resetCost)
end
function GameFleetEquipmentPop:chuancang_showEquipedCoreItem(now_core, HasEquipedTitle, NotEquipedTitle)
  self:GetFlashObject():InvokeASCallback("_root", "showEquipedCoreItem", now_core, HasEquipedTitle, NotEquipedTitle)
end
function GameFleetEquipmentPop:chuancang_showPerfectReset(arg1, arg2)
  self:GetFlashObject():InvokeASCallback("_root", "showPerfectReset", arg1, arg2)
end
function SortSelectFleetPool(a, b)
  return a.sourceForce > b.sourceForce
end
function GameFleetEquipmentPop:zhenxin_showChangeFleetPop(content)
  DebugOutPutTable(content, "zhenxin_showChangeFleetPop")
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  local fleetDetail, index = ZhenXinUI():GetCurFleetContent()
  local fleetId = fleetDetail.identity
  local data = {}
  data.fleets = {}
  local fleetsContent = GameGlobalData:GetData("fleetinfo").fleets
  DebugOutPutTable(fleetsContent, "fleetsContent")
  for k, v in ipairs(content.fleets or {}) do
    local fleet = {}
    fleet = v
    if v.fleet_id == fleetId then
      fleet.isSelected = true
    end
    fleet.sourceForce = 0
    for _, v2 in ipairs(fleetsContent) do
      if v2.identity == v.fleet_id and v.vessels ~= 6 then
        fleet.sourceForce = v2.force
        fleet.Showforce = GameUtils.numberConversion(v2.force)
      end
    end
    if v.vessels == 6 then
      fleet.sourceForce = v.force
      fleet.Showforce = GameUtils.numberConversion(v.force)
    end
    fleet.CommanderType = GameDataAccessHelper:GetCommanderVesselsType(v.fleet_id, v.level)
    fleet.name = FleetDataAccessHelper:GetFleetLevelDisplayName_2(v.name, v.fleet_id, v.color, v.level, true)
    fleet.vesselsType = FleetDataAccessHelper:GetFleetVesselsFrameByVessel(v.vessels)
    fleet.vesselsName = FleetDataAccessHelper:GetFleetVesselsTypeStringByVessel(v.vessels)
    fleet.ship = GameDataAccessHelper:GetCommanderShipByDownloadState(v.ship)
    fleet.avatar = FleetDataAccessHelper:GetFleetAvatarFrameByAvatar(v.avatar, v.fleet_id)
    fleet.color = FleetDataAccessHelper:GetFleetColorFrameByColor(v.color)
    fleet.spellFrame = FleetDataAccessHelper:GetFleetSpellFrameBySpell(v.spell_id)
    fleet.rankFrame = GameUtils:GetFleetRankFrame(v.rank)
    fleet.fleet_id = v.fleet_id
    data.fleets[#data.fleets + 1] = fleet
  end
  table.sort(data.fleets, SortSelectFleetPool)
  self.changeFleetsData = data
  self:SetChangeFleetsData()
  self:ShowChangeFleetsPop()
end
function GameFleetEquipmentPop:SetChangeFleetsData()
  if self.changeFleetsData and self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "setChangeFleetsData", math.ceil(#self.changeFleetsData.fleets / 2))
  end
end
function GameFleetEquipmentPop:ShowChangeFleetsPop()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "ShowChangeFleetsPop")
  end
end
function GameFleetEquipmentPop:UpdateChangeFleetsItem(itemIndex)
  local list = {}
  local startIndex = (itemIndex - 1) * 2 + 1
  local item = self.changeFleetsData.fleets[startIndex]
  local item2 = self.changeFleetsData.fleets[startIndex + 1]
  list[#list + 1] = item
  list[#list + 1] = item2
  local flashObj = self:GetFlashObject()
  if flashObj then
    DebugOutPutTable(list, "UpdateChangeFleetsItem:" .. itemIndex)
    flashObj:InvokeASCallback("_root", "upChangeFleetsItem", list, itemIndex)
  end
end
