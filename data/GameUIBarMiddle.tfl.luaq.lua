local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameStateKrypton = GameStateManager.GameStateKrypton
local GameGlobalData = GameGlobalData
local GameUIBarMiddle = LuaObjectManager:GetLuaObject("GameUIBarMiddle")
local GameUIBuilding = LuaObjectManager:GetLuaObject("GameUIBuilding")
local GameUIBuildingInfo = LuaObjectManager:GetLuaObject("GameUIBuildingInfo")
local GameObjectMainPlanet = LuaObjectManager:GetLuaObject("GameObjectMainPlanet")
local GameUITechnology = LuaObjectManager:GetLuaObject("GameUITechnology")
local GameUICollect = LuaObjectManager:GetLuaObject("GameUICollect")
local QuestTutorialEngineer = TutorialQuestManager.QuestTutorialEngineer
local QuestTutorialEnhance = TutorialQuestManager.QuestTutorialEnhance
local QuestTutorialCityHall = TutorialQuestManager.QuestTutorialCityHall
local QuestTutorialTax = TutorialQuestManager.QuestTutorialTax
local QuestTutorialBuildStar = TutorialQuestManager.QuestTutorialBuildStar
local QuestTutorialStarCharge = TutorialQuestManager.QuestTutorialStarCharge
local QuestTutorialRecruit = TutorialQuestManager.QuestTutorialRecruit
local QuestTutorialBuildAcademy = TutorialQuestManager.QuestTutorialBuildAcademy
local QuestTutorialsFirstKillBoss = TutorialQuestManager.QuestTutorialsFirstKillBoss
local QuestTutorialBuildTechLab = TutorialQuestManager.QuestTutorialBuildTechLab
local QuestTutorialBuildFactory = TutorialQuestManager.QuestTutorialBuildFactory
local QuestTutorialFactoryRemodel = TutorialQuestManager.QuestTutorialFactoryRemodel
local QuestTutorialBuildKrypton = TutorialQuestManager.QuestTutorialBuildKrypton
local QuestTutorialBuildAffairs = TutorialQuestManager.QuestTutorialBuildAffairs
local QuestTutorialUseTechLab = TutorialQuestManager.QuestTutorialUseTechLab
local QuestTutorialUseFactory = TutorialQuestManager.QuestTutorialUseFactory
local QuestTutorialUseKrypton = TutorialQuestManager.QuestTutorialUseKrypton
local QuestTutorialUseAffairs = TutorialQuestManager.QuestTutorialUseAffairs
local QuestTutorialCollect = TutorialQuestManager.QuestTutorialCollect
local QuestTutorialEnhance_second = TutorialQuestManager.QuestTutorialEnhance_second
local GameUIMaskLayer = LuaObjectManager:GetLuaObject("GameUIMaskLayer")
local middleBtnType = {
  INFO_TYPE = 1,
  UPGRADE_TYPE = 2,
  FUNCTION_TYPE = 3
}
GameUIBarMiddle.FunctionCallbackList = {}
function GameUIBarMiddle.FunctionCallbackList.tech_lab()
  GameUITechnology:EnterTechnology()
end
function GameUIBarMiddle.FunctionCallbackList.planetary_fortress()
  GameUICollect:ShowCollect()
end
function GameUIBarMiddle.FunctionCallbackList.star_portal()
  local GameStateRecruit = GameStateManager.GameStateRecruit
  GameStateManager:SetCurrentGameState(GameStateRecruit)
end
function GameUIBarMiddle.FunctionCallbackList.factory()
  local GameUIFactory = LuaObjectManager:GetLuaObject("GameUIFactory")
  GameStateManager:GetCurrentGameState():AddObject(GameUIFactory)
end
function GameUIBarMiddle.FunctionCallbackList.commander_academy()
  local GameStateConfrontation = GameStateManager.GameStateConfrontation
  GameStateConfrontation:EnterConfrontation()
end
function GameUIBarMiddle.FunctionCallbackList.krypton_center()
  GameStateKrypton:EnterKrypton(GameStateKrypton.KRYPTON)
end
function GameUIBarMiddle.FunctionCallbackList.affairs_hall()
  GameStateManager:SetCurrentGameState(GameStateManager.GameStateAffairInfo)
end
function GameUIBarMiddle.FunctionCallbackList.trade_port()
end
function GameUIBarMiddle.FunctionCallbackList.engineering_bay()
  if QuestTutorialEngineer:IsActive() then
    return
  end
  local GameStateEquipEnhance = GameStateManager.GameStateEquipEnhance
  GameStateEquipEnhance.isNeedShowEnhanceUI = true
  GameStateEquipEnhance:EnterEquipEnhance()
end
function GameUIBarMiddle:OnInitGame()
  GameGlobalData:RegisterDataChangeCallback("buildings", self.buildingsListener)
end
function GameUIBarMiddle:OnEraseFromGameState()
  self.m_currentBuildingName = nil
end
function GameUIBarMiddle:UpdateFunctionButtonStatus()
  TutorialQuestManager.OnBuildingInfoChange()
  if self.m_currentBuildingName then
    local building_info = GameGlobalData:GetBuildingInfo(self.m_currentBuildingName)
    local frame_name = "function"
    local btnText = GameLoader:GetGameText("LC_MENU_BUILDING_BTN_FUNCTION")
    local enable_function = true
    if building_info.level <= 0 then
      enable_function = false
    end
    self:GetFlashObject():InvokeASCallback("_root", "UpdateButtonStatus", middleBtnType.FUNCTION_TYPE, frame_name, btnText, enable_function)
  end
end
function GameUIBarMiddle:ShowMiddleMenu(buildingName)
  DebugOut("ShowMiddleMenu" .. buildingName)
  if buildingName and self.m_currentBuildingName == buildingName then
    return
  end
  if self.m_currentBuildingName then
    self.m_nextBuildingName = buildingName
    self:HideMiddleMenu()
    return
  end
  self.m_currentBuildingName = buildingName
  local buildingInfo = GameGlobalData:GetBuildingInfo(buildingName)
  local btnIconLeft = "info"
  local btnIconMiddle = "upgrade"
  local btnIconRight = "function"
  local enable_function = true
  if buildingInfo.level == 0 then
    enable_function = false
  end
  local flash_obj = self:GetFlashObject()
  local btn1Text = GameLoader:GetGameText("LC_MENU_BUILDING_BTN_INFO")
  local btn2Text = GameLoader:GetGameText("LC_MENU_BUILDING_BTN_UPGRADE")
  local btn3Text = GameLoader:GetGameText("LC_MENU_BUILDING_BTN_FUNCTION")
  flash_obj:SetBtnEnable("_root.btn_function", enable_function)
  local name = "LC_MENU_BUILDING_NAME_" .. string.upper(buildingName)
  local building_name = GameLoader:GetGameText(name)
  flash_obj:InvokeASCallback("_root", "ShowMiddleMenu", building_name)
  GameStateMainPlanet:AddObject(self)
  if GameObjectMainPlanet:GetFlashObject() then
    GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "HideAllTutorial")
  end
  if QuestTutorialEngineer:IsActive() and buildingName == "engineering_bay" or QuestTutorialCityHall:IsActive() and buildingName == "planetary_fortress" or QuestTutorialBuildStar:IsActive() and buildingName == "star_portal" or QuestTutorialBuildAcademy:IsActive() and buildingName == "commander_academy" or QuestTutorialBuildFactory:IsActive() and buildingName == "factory" or QuestTutorialBuildKrypton:IsActive() and buildingName == "krypton_center" or QuestTutorialBuildAffairs:IsActive() and buildingName == "affairs_hall" or QuestTutorialBuildTechLab:IsActive() and buildingName == "tech_lab" then
    self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialUpdate")
    self:GetFlashObject():InvokeASCallback("_root", "HideTutorialFuncBtn")
  elseif QuestTutorialEnhance:IsActive() and buildingName == "engineering_bay" or QuestTutorialEnhance_second:IsActive() and buildingName == "engineering_bay" or QuestTutorialTax:IsActive() and buildingName == "planetary_fortress" or QuestTutorialFactoryRemodel:IsActive() and buildingName == "factory" or QuestTutorialCollect:IsActive() and buildingName == "planetary_fortress" or QuestTutorialUseTechLab:IsActive() and buildingName == "tech_lab" or QuestTutorialUseKrypton:IsActive() and buildingName == "krypton_center" or QuestTutorialsFirstKillBoss:IsActive() and buildingName == "commander_academy" or QuestTutorialUseFactory:IsActive() and buildingName == "factory" or QuestTutorialUseAffairs:IsActive() and buildingName == "affairs_hall" or (QuestTutorialStarCharge:IsActive() or QuestTutorialRecruit:IsActive()) and buildingName == "star_portal" then
    self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialFuncBtn")
    self:GetFlashObject():InvokeASCallback("_root", "HideTutorialUpdate")
  else
    self:GetFlashObject():InvokeASCallback("_root", "HideTutorialUpdate")
    self:GetFlashObject():InvokeASCallback("_root", "HideTutorialFuncBtn")
  end
  if GameUtils:GetTutorialHelp() and (GameUtils:GetTutorialName() == "planetary_fortress" or GameUtils:GetTutorialName() == "engineering_bay" or GameUtils:GetTutorialName() == "tech_lab" or GameUtils:GetTutorialName() == "krypton_center") and (buildingName == "planetary_fortress" or buildingName == "engineering_bay" or buildingName == "tech_lab" or buildingName == "krypton_center") then
    flash_obj:InvokeASCallback("_root", "GetHelpTutorialPosInMidBar")
  end
  if GameUtils:GetTutorialHelp() and (GameUtils:GetTutorialName() == "upgradeEquip" or GameUtils:GetTutorialName() == "upgradeTech" or GameUtils:GetTutorialName() == "collect" or GameUtils:GetTutorialName() == "composeKrypton") then
    flash_obj:InvokeASCallback("_root", "GetHelpTutorialPosInMidBarAction")
  end
end
function GameUIBarMiddle:HideMiddleMenu()
  if self.m_renderFx ~= nil then
    self.m_renderFx:InvokeASCallback("_root", "HideMiddleMenu")
  end
  self.m_currentBuildingName = nil
end
function GameUIBarMiddle:FindBuildingFromBuildings(buildingName)
  for i, v in ipairs(GameGlobalData.GetData("buildings").buildings) do
    if v and v.name == buildingName then
      return v
    end
  end
end
function GameUIBarMiddle:OnFSCommand(cmd, arg)
  if cmd == "on_info_clicked" then
    if self.m_currentBuildingName then
      GameUIBuildingInfo:DisplayInfoDialog(self.m_currentBuildingName)
      self:HideMiddleMenu()
    end
  elseif cmd == "on_upgrade_clicked" then
    if self.m_currentBuildingName then
      local buildingName = self.m_currentBuildingName
      if QuestTutorialCityHall:IsActive() and buildingName == "planetary_fortress" then
        AddFlurryEvent("ClickCityHallMidMenuUpgrade", {}, 1)
        AddFlurryEvent("TutorialCityHall_SelectUpMenu", {}, 2)
      elseif QuestTutorialEngineer:IsActive() and buildingName == "engineering_bay" then
        AddFlurryEvent("TutoriaEngine_SelectUpMenu", {}, 2)
      end
      GameUIBuilding:DisplayUpgradeDialog(self.m_currentBuildingName)
      self:HideMiddleMenu()
      if GameUtils:GetTutorialHelp() then
        GameUtils:HideTutorialHelp()
      end
    end
  elseif cmd == "on_function_clicked" then
    if self.m_currentBuildingName then
      local buildingInfo = GameGlobalData:GetBuildingInfo(self.m_currentBuildingName)
      if not buildingInfo then
        return
      end
      DebugOut(" functon  clicked: ", buildingInfo.name, buildingInfo.level)
      local buildingName = self.m_currentBuildingName
      if QuestTutorialEnhance:IsActive() and buildingName == "engineering_bay" then
        AddFlurryEvent("ClickEngineeringBayMidMenuFunction", {}, 1)
        AddFlurryEvent("TutoriaEngineUse_SelectFunMenu", {}, 2)
      elseif QuestTutorialsFirstKillBoss:IsActive() and buildingName == "commander_academy" then
        AddFlurryEvent("ClickAcademyMidMenuFunction", {}, 1)
      elseif QuestTutorialTax:IsActive() and buildingName == "planetary_fortress" then
        AddFlurryEvent("TutorialCityHallTax_SelectFunMenu", {}, 2)
      end
      if buildingInfo.level > 0 then
        local on_function = self.FunctionCallbackList[buildingInfo.name]
        assert(on_function)
        on_function(self)
      end
      self:HideMiddleMenu()
    end
  elseif cmd == "Remove_MiddleUI" then
    if self.m_nextBuildingName then
      self:ShowMiddleMenu(self.m_nextBuildingName)
      self.m_nextBuildingName = nil
    else
      GameStateMainPlanet:EraseObject(self)
      GameStateMainPlanet:ForceCompleteCammandList()
      if not GameStateManager:GetCurrentGameState():IsObjectInState(GameUIBuilding) and not GameStateManager:GetCurrentGameState():IsObjectInState(GameUIBuildingInfo) then
        GameStateMainPlanet:CheckShowTutorial()
      end
    end
  elseif cmd == "Hide_MiddleUI" then
    GameObjectMainPlanet:ReleasedInBG()
  elseif cmd == "upgrade_mid" then
    if GameUtils:GetTutorialHelp() and (GameUtils:GetTutorialName() == "planetary_fortress" or GameUtils:GetTutorialName() == "engineering_bay" or GameUtils:GetTutorialName() == "tech_lab" or GameUtils:GetTutorialName() == "krypton_center") then
      local param = LuaUtils:string_split(arg, "\001")
      GameUtils:ShowMaskLayer(tonumber(param[1]), tonumber(param[2]), GameUIMaskLayer.ButtonPos.right, "empty", GameUIMaskLayer.MaskStyle.small)
    end
  elseif cmd == "upgrade_mid_action" then
    if GameUtils:GetTutorialHelp() and (GameUtils:GetTutorialName() == "upgradeEquip" or GameUtils:GetTutorialName() == "upgradeTech" or GameUtils:GetTutorialName() == "collect" or GameUtils:GetTutorialName() == "composeKrypton") then
      local param = LuaUtils:string_split(arg, "\001")
      GameUtils:ShowMaskLayer(tonumber(param[1]), tonumber(param[2]), GameUIMaskLayer.ButtonPos.right, "empty", GameUIMaskLayer.MaskStyle.small)
    end
  else
    assert(false)
  end
end
function GameUIBarMiddle:GetHelpTutorialPosInMidBar(...)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "")
  end
end
function GameUIBarMiddle.buildingsListener()
  GameUIBarMiddle:UpdateFunctionButtonStatus()
end
if AutoUpdate.isAndroidDevice then
  function GameUIBarMiddle.OnAndroidBack()
    GameUIBarMiddle:HideMiddleMenu()
  end
end
