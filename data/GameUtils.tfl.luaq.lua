GameUtils = {
  DOUBLE_CLICK_TIMER = 200,
  lastRequestRecord = {url = "", statusCode = 0}
}
GameUtils.curFacebookInfo = {}
function GameUtils:RequestTranslateInfo()
  NetMessageMgr:SendMsg(NetAPIList.translate_info_req.Code, null, self.RequestTranslateInfoCallback, false, nil)
end
function GameUtils:isIDNumberCorrect(id)
  if id == nil then
    return false
  end
  local weight = {
    [1] = 7,
    [2] = 9,
    [3] = 10,
    [4] = 5,
    [5] = 8,
    [6] = 4,
    [7] = 2,
    [8] = 1,
    [9] = 6,
    [10] = 3,
    [11] = 7,
    [12] = 9,
    [13] = 10,
    [14] = 5,
    [15] = 8,
    [16] = 4,
    [17] = 2
  }
  local comp = {
    [1] = "1",
    [2] = "0",
    [3] = "X",
    [4] = "9",
    [5] = "8",
    [6] = "7",
    [7] = "6",
    [8] = "5",
    [9] = "4",
    [10] = "3",
    [11] = "2"
  }
  if string.len(id) ~= 18 then
    return false
  else
    local sum = 0
    local res = {}
    for i = 1, 18 do
      local txt = string.sub(id, i, i)
      if string.byte(txt) > 128 then
        return false
      end
      local num = tonumber(txt)
      if num == nil and i ~= 18 then
        return false
      end
      if i == 18 and num == nil and txt ~= "X" and txt ~= "x" then
        return false
      end
      table.insert(res, txt)
    end
    if tonumber(res[7] .. res[8]) < 19 then
      return false
    end
    if 12 < tonumber(res[11] .. res[12]) or tonumber(res[11] .. res[12]) == 0 then
      return false
    end
    if tonumber(res[13] .. res[14]) > 31 or tonumber(res[13] .. res[14]) == 0 then
      return false
    end
    for i = 1, 17 do
      sum = tonumber(res[i]) * weight[i] + sum
    end
    local mod = sum % 11
    if comp[mod + 1] == string.upper(res[18]) then
      return true
    else
      return false
    end
  end
end
function GameUtils:isOlderThan18(id)
  if id and id ~= "" then
    local currentYear = os.date("%Y")
    local currentMonth = os.date("%m")
    local currentDay = os.date("%d")
    local birthYear = string.sub(id, 7, 10)
    local birthMonth = string.sub(id, 11, 12)
    local birthDay = string.sub(id, 13, 14)
    DebugOut("id:", id, "currentYear", currentYear, "currentMonth", currentMonth, "currentDay", currentDay)
    if tonumber(currentYear) - tonumber(birthYear) < 18 then
      return false
    elseif tonumber(currentYear) - tonumber(birthYear) == 18 then
      if tonumber(currentMonth) < tonumber(birthMonth) then
        return false
      elseif tonumber(currentMonth) == tonumber(birthMonth) then
        if tonumber(currentDay) < tonumber(birthDay) then
          return false
        else
          return true
        end
      else
        return true
      end
    else
      return true
    end
  end
  return true
end
function GameUtils:isNameCorrect(name)
  if name == nil then
    return false
  end
  if string.len(name) < 6 then
    return false
  else
    local lenInByte = #name
    for i = 1, lenInByte, 3 do
      local firstb = string.byte(name, i)
      local secondb = string.byte(name, i + 1)
      local thirdb = string.byte(name, i + 2)
      if firstb < 228 or firstb > 233 then
        return false
      end
      if secondb < 128 or secondb > 191 then
        return false
      end
      if thirdb < 128 or thirdb > 191 then
        return false
      end
    end
    return true
  end
end
function GameUtils:isTelephoneCorrect(tel)
  if tel == nil then
    return false
  end
  if string.len(tel) ~= 11 then
    return false
  end
  for i = 1, 11 do
    local txt = string.sub(tel, i, i)
    local num = tonumber(txt)
    if num == nil then
      return false
    end
  end
  local first = string.sub(tel, 1, 1)
  if tonumber(first) ~= 1 then
    return false
  end
  return true
end
function GameUtils:isPlayerMaxLevel(nowLevel)
  if nowLevel < 110 then
    return false
  else
    return true
  end
end
function GameUtils.RequestTranslateInfoCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.translate_info_req.Code then
    assert(content.code ~= 0)
    local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  if msgType == NetAPIList.translate_info_ack.Code then
    local platformToken = content.token
    local platformUrl = content.url
    if platformUrl == nil then
      platformUrl = ""
    end
    DebugOut("RequestTranslateInfoCallback")
    DebugTable(content)
    GameUtils:InitTranstale(platformToken, platformUrl)
    return true
  end
  return false
end
function GameUtils:InitTranstale(platformToken, platformUrl)
  GameUtils.mTranslation = GetTranslation()
  if GameUtils.mTranslation == nil then
    return
  end
  local appId = "20151123000006216"
  local key = "FgGYFJU9e7oqd6lZH3Cg"
  local token = ""
  local statUrl = ""
  local ret1 = 0
  local ret2 = ""
  if ext.GetDeviceInfo then
    ret1, ret2 = ext.GetDeviceInfo()
  end
  local udid = ret2
  local abroadMode = true
  if self:IsChinese() then
    abroadMode = false
  end
  GameUtils.mTranslation:setConfig(appId, key, platformToken, platformUrl, token, statUrl, udid, abroadMode)
end
function GameUtils.EndSocialShare(success)
  DebugOut("EndSocialShare:")
  DebugOut(success)
end
function GameUtils.BeginSocialShare()
  DebugOut("BeginSocialShare")
end
function GameUtils:GetTimeChar(ti)
  local charMap = {
    en_Y = "Y",
    en_M = "M",
    en_d = "d",
    en_h = "h",
    en_m = "m",
    en_s = "s",
    en_ago = " ago",
    ru_Y = "\208\179",
    ru_M = "\208\188",
    ru_d = "\208\180",
    ru_h = "\209\135",
    ru_m = "\208\188",
    ru_s = "\209\129",
    ru_ago = " \208\189\208\176\208\183\208\176\208\180"
  }
  local lang = "en"
  if GameSettingData and GameSettingData.Save_Lang then
    lang = GameSettingData.Save_Lang
    if string.find(lang, "ru") == 1 then
      lang = "ru"
    else
      lang = "en"
    end
  end
  local key = lang .. "_" .. ti
  return charMap[key] or ti
end
function GameUtils:formatTimeStringAsPartion(time)
  local timeText = ""
  if time < 60 then
    return "<1" .. GameUtils:GetTimeChar("m")
  end
  if time >= 86400 then
    local count = math.floor(time / 86400)
    timeText = timeText .. string.format("%d" .. GameUtils:GetTimeChar("d"), tostring(count))
    time = time - count * 86400
    return timeText
  end
  hasHours = false
  if time >= 3600 then
    local count = math.floor(time / 3600)
    timeText = timeText .. string.format("%d" .. GameUtils:GetTimeChar("h"), tostring(count))
    time = time - count * 3600
    hasHours = true
  end
  if time >= 60 then
    local count = math.floor(time / 60)
    timeText = timeText .. string.format("%d" .. GameUtils:GetTimeChar("m"), tostring(count))
    time = time - count * 60
  end
  if hasHours then
    return timeText
  end
  if time > 0 then
    timeText = timeText .. string.format("%d" .. GameUtils:GetTimeChar("s"), tostring(time))
  end
  return timeText
end
function GameUtils:formatTimeStringAsPartion2(time)
  local timeText = ""
  if time < 60 then
    return time .. GameUtils:GetTimeChar("s")
  end
  local haveDay = false
  if time >= 86400 then
    local count = math.floor(time / 86400)
    timeText = timeText .. string.format("%d" .. GameUtils:GetTimeChar("d"), tostring(count))
    time = time - count * 86400
    haveDay = true
    return timeText
  end
  local hasHours = false
  if time >= 3600 then
    local count = math.floor(time / 3600)
    timeText = timeText .. string.format("%d" .. GameUtils:GetTimeChar("h"), tostring(count))
    time = time - count * 3600
    hasHours = true
  end
  if haveDay then
    return timeText
  end
  if time >= 60 then
    local count = math.floor(time / 60)
    timeText = timeText .. string.format("%d" .. GameUtils:GetTimeChar("m"), tostring(count))
    time = time - count * 60
  end
  if hasHours then
    return timeText
  end
  if time > 0 then
    timeText = timeText .. string.format("%d" .. GameUtils:GetTimeChar("s"), tostring(time))
  end
  return timeText
end
function GameUtils:formatTimeStringAsTwitterStyle(time)
  local timeText = ""
  if time < 60 then
    return "<1" .. GameUtils:GetTimeChar("m")
  end
  if time >= 86400 then
    local count = math.floor(time / 86400)
    timeText = timeText .. string.format("%d" .. GameUtils:GetTimeChar("d"), tostring(count))
    time = time - count * 86400
  end
  if time >= 3600 then
    local count = math.floor(time / 3600)
    timeText = timeText .. string.format("%d" .. GameUtils:GetTimeChar("h"), tostring(count))
    time = time - count * 3600
  end
  if time >= 60 then
    local count = math.floor(time / 60)
    timeText = timeText .. string.format("%d" .. GameUtils:GetTimeChar("m"), tostring(count))
    time = time - count * 60
  end
  if time > 0 then
    timeText = timeText .. string.format("%d" .. GameUtils:GetTimeChar("s"), tostring(time))
  end
  return timeText
end
function GameUtils:formatTimeStringAsTwitterStyle2(time)
  local timeText = ""
  if time < 60 then
    return time
  end
  if time >= 86400 then
    local count = math.floor(time / 86400)
    timeText = timeText .. string.format("%d" .. GameUtils:GetTimeChar("d"), tostring(count))
    time = time - count * 86400
  end
  if time >= 3600 then
    local count = math.floor(time / 3600)
    timeText = timeText .. string.format("%d" .. GameUtils:GetTimeChar("h"), tostring(count))
    time = time - count * 3600
  end
  if time >= 60 then
    local count = math.floor(time / 60)
    timeText = timeText .. string.format("%d" .. GameUtils:GetTimeChar("m"), tostring(count))
    time = time - count * 60
  end
  if time > 0 then
    timeText = timeText .. string.format("%d" .. GameUtils:GetTimeChar("s"), tostring(time))
  end
  return timeText
end
function GameUtils:formatTimeStringAsTwitterStyle3(time)
  local timeText = ""
  if time < 60 then
    return time .. GameUtils:GetTimeChar("s")
  end
  if time >= 86400 then
    local count = math.floor(time / 86400)
    timeText = timeText .. string.format("%d" .. GameUtils:GetTimeChar("d"), tostring(count))
    time = time - count * 86400
  end
  if time >= 3600 then
    local count = math.floor(time / 3600)
    timeText = timeText .. string.format("%d" .. GameUtils:GetTimeChar("h"), tostring(count))
    time = time - count * 3600
  end
  if time >= 60 then
    local count = math.floor(time / 60)
    timeText = timeText .. string.format("%d" .. GameUtils:GetTimeChar("m"), tostring(count))
    time = time - count * 60
  end
  if time > 0 then
    timeText = timeText .. string.format("%d" .. GameUtils:GetTimeChar("s"), tostring(time))
  end
  return timeText
end
function GameUtils:formatTimeStringAsTwitterStyleWithOutSecond(time)
  local timeText = ""
  if time < 60 then
    return "<1" .. GameUtils:GetTimeChar("m")
  end
  if time >= 86400 then
    local count = math.floor(time / 86400)
    timeText = timeText .. string.format("%d" .. GameUtils:GetTimeChar("d"), tostring(count))
    time = time - count * 86400
  end
  if time >= 3600 then
    local count = math.floor(time / 3600)
    timeText = timeText .. string.format("%d" .. GameUtils:GetTimeChar("h"), tostring(count))
    time = time - count * 3600
  end
  if time >= 60 then
    local count = math.floor(time / 60)
    timeText = timeText .. string.format("%d" .. GameUtils:GetTimeChar("m"), tostring(count))
    time = time - count * 60
  end
  return timeText
end
function GameUtils:formatTimeStringAsAgoStyle(time)
  local timeText = ""
  if time >= 31536000 then
    local count = math.floor(time / 31536000)
    timeText = string.format("%d" .. GameUtils:GetTimeChar("Y"), tostring(count))
  elseif time >= 2592000 then
    local count = math.floor(time / 2592000)
    timeText = string.format("%d" .. GameUtils:GetTimeChar("M"), tostring(count))
  elseif time >= 86400 then
    local count = math.floor(time / 86400)
    timeText = string.format("%d" .. GameUtils:GetTimeChar("d"), tostring(count))
  elseif time >= 3600 then
    local count = math.floor(time / 3600)
    timeText = string.format("%d" .. GameUtils:GetTimeChar("h"), tostring(count))
  elseif time >= 60 then
    local count = math.floor(time / 60)
    timeText = string.format("%d" .. GameUtils:GetTimeChar("m"), tostring(count))
  elseif time > 0 then
    timeText = string.format("%d" .. GameUtils:GetTimeChar("s"), tostring(time))
  else
    timeText = string.format("%d" .. GameUtils:GetTimeChar("s"), tostring(time))
  end
  return timeText .. GameUtils:GetTimeChar("ago")
end
function GameUtils:formatTimeStringWithoutSeconds(time)
  time = time / 60
  local minutes = time % 60
  time = time / 60
  local hours = time
  return ...
end
function GameUtils:formatTimeStringWithoutHour(time)
  local seconds = time % 60
  time = time / 60
  local minutes = time % 60
  time = time / 60
  local hours = time
  return ...
end
function GameUtils:formatTimeStringByMinutes(timeMinutes)
  local minutes = timeMinutes % 60
  local hours = timeMinutes / 60
  return ...
end
function GameUtils:formatTimeString(time)
  if time < 0 then
    return ""
  end
  local seconds = time % 60
  time = time / 60
  local minutes = time % 60
  time = time / 60
  local hours = time
  return ...
end
function GameUtils:formatTimeToDateStr(time)
  return ...
end
function GameUtils:formatTimeToDate(time)
  return ...
end
function GameUtils:SetHTTPPostData(data, dataType)
  self._httpData = data
  self._httpDataType = dataType
end
function GameUtils:GetShortActivityID(itemid)
  local itemId = tostring(itemid)
  if string.len(itemId) > 10 then
    itemId = string.sub(itemId, -10)
  end
  return (...), itemId, -10
end
function GameUtils:HTTP_SendRequest(requestType, requestParam, responseFunc, isNeedLoading, responseErrFunc, httpMethod, requestMode)
  local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
  DebugOut("-------------------HTTP_REQUEST-----------------", requestType)
  if isNeedLoading then
    local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
    GameWaiting:ShowLoadingScreen()
  end
  httpMethod = httpMethod or "GET"
  requestMode = requestMode or "normal"
  assert(requestMode ~= "notencrypt" or string.lower(httpMethod) ~= "post")
  local loginInfo = GameUtils:GetLoginInfo()
  DebugOut("ajksdjaksdkjjkaskd")
  DebugTable(loginInfo)
  DebugOut(requestType)
  local outList = {
    ["gamers/servers_v2"] = false,
    ["gamers/register"] = false,
    ["gamers/udid_v2"] = false,
    ["gamers/validate"] = false
  }
  local url = ""
  if GameUtils._httpServer ~= nil then
    url = GameUtils._httpServer .. requestType
  else
    url = AutoUpdate.gameGateway .. requestType
  end
  DebugOut("url ", url)
  local header = {}
  if GameUtils._httpHeader ~= nil then
    header = GameUtils._httpHeader
  end
  if GameUtils._httpServer == nil and loginInfo and loginInfo.ServerInfo and outList[requestType] == nil then
    header.logic_id = loginInfo.ServerInfo.logic_id
  end
  DebugOutPutTable(header, "header")
  if httpMethod == "POST" then
    if GameUtils._httpData == "" or GameUtils._httpData == nil then
      DebugOut("warning: no body data")
    end
    DebugOut("GameUtils._httpData", GameUtils._httpData)
    DebugOut("requestMode", requestMode)
    DebugOut("GameUtils._httpDataType", GameUtils._httpDataType)
  end
  local attpTime = os.time()
  ext.http.request({
    url,
    param = requestParam,
    mode = requestMode,
    header = header,
    method = httpMethod,
    body = GameUtils._httpData,
    contentType = GameUtils._httpDataType,
    callback = function(statusCode, responseContent, errstr)
      GameUtils.lastRequestRecord.url = url or GameUtils.lastRequestRecord.url
      GameUtils.lastRequestRecord.statusCode = statusCode or GameUtils.lastRequestRecord.statusCode
      DebugOut("---------------NOW CALL BACK HTTP------------")
      if type(responseContent) == "table" then
        DebugOutPutTable(responseContent, "responseContent")
      end
      if isNeedLoading then
        local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
        GameWaiting:HideLoadingScreen()
        DebugOut(string.format("%s msg send-receive used time:%d", url, os.time() - attpTime))
      end
      DebugOut("NOW STATUS CODE::" .. statusCode .. tostring(responseContent))
      if errstr ~= nil or statusCode ~= 200 then
        if responseErrFunc then
          responseErrFunc(statusCode, responseContent)
        end
        if errstr and errstr ~= "" then
          GameUtils:ShowTips("Error:" .. errstr .. [[

Code:]] .. statusCode .. [[

Url:]] .. GameUtils.lastRequestRecord.url)
        end
        return
      elseif responseFunc then
        responseFunc(responseContent)
      end
    end
  })
  GameUtils._httpData = nil
  GameUtils._httpDataType = nil
  GameUtils._httpServer = nil
  GameUtils._httpHeader = nil
end
function GameUtils:HTTP_SendBaseRequest(requestType, requestParam, responseFunc, isNeedLoading, responseErrFunc, httpMethod, requestMode)
  local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
  DebugOut("-------------------HTTP_REQUEST-----------------", requestType)
  if isNeedLoading then
    local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
    GameWaiting:ShowLoadingScreen()
  end
  httpMethod = httpMethod or "GET"
  requestMode = requestMode or "normal"
  local outList = {
    ["gamers/servers_v2"] = false,
    ["gamers/register"] = false,
    ["gamers/udid_v2"] = false,
    ["gamers/validate"] = false
  }
  local url = ""
  if GameUtils._httpServer ~= nil then
    url = GameUtils._httpServer .. requestType
  else
    url = AutoUpdate.gameGateway .. requestType
  end
  DebugOut("url ", url)
  local header = {}
  if GameUtils._httpHeader ~= nil then
    header = GameUtils._httpHeader
    DebugOutPutTable(header, "header")
  end
  if GameUtils._httpServer == nil and loginInfo and loginInfo.ServerInfo and outList[requestType] == nil then
    header.logic_id = loginInfo.ServerInfo.logic_id
  end
  if httpMethod == "POST" then
    if GameUtils._httpData == "" or GameUtils._httpData == nil then
      DebugOut("warning: no body data")
    end
    DebugOut("GameUtils._httpData", GameUtils._httpData)
    DebugOut("requestMode", requestMode)
    DebugOut("GameUtils._httpDataType", GameUtils._httpDataType)
  end
  ext.http.requestBasic({
    url,
    param = requestParam,
    mode = requestMode,
    header = header,
    method = httpMethod,
    body = GameUtils._httpData,
    contentType = GameUtils._httpDataType,
    callback = function(statusCode, responseContent, errstr)
      DebugOut("---------------NOW CALL BACK HTTP------------")
      if type(responseContent) == "table" then
        DebugOutPutTable(responseContent, "responseContent")
      end
      if isNeedLoading then
        local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
        GameWaiting:HideLoadingScreen()
      end
      DebugOut("NOW STATUS CODE::" .. statusCode)
      if errstr ~= nil or statusCode ~= 200 then
        responseErrFunc(statusCode, responseContent)
        return
      else
        responseFunc(responseContent)
      end
    end
  })
  GameUtils._httpData = nil
  GameUtils._httpDataType = nil
  GameUtils._httpServer = nil
  GameUtils._httpHeader = nil
end
function GameUtils:SetHTTPServer(server_ip)
  GameUtils._httpServer = server_ip
end
function GameUtils:SetHTTPHeader(header)
  GameUtils._httpHeader = header
end
function GameUtils.httpRequestFailedCallback(status_code, content)
  local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
  local serverName = " "
  serverName = not GameUtils:GetActiveServerInfo() or serverName .. GameUtils:GetActiveServerInfo().name or GameUtils:GetActiveServerInfo().id or ""
  local errorText = GameLoader:GetGameText("LC_MENU_CONNECT_ERROR") .. serverName
  local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
  GameSetting:showContactUsDialogForNetError(GameLoader:GetGameText("LC_MENU_ERROR_TITLE"), errorText)
end
function GameUtils:Tap4funAccountRegister(tap4fun_account, successCallback)
  local data = {
    passport = tap4fun_account.passport,
    password = ext.ENC1(tap4fun_account.password)
  }
  GameUtils:SetHTTPPostData(ext.json.generate(data), nil)
  GameUtils:HTTP_SendRequest("gamers/register", nil, successCallback, true, GameUtils.httpRequestFailedCallback, "POST")
end
function GameUtils:Tap4funModifyPassword(tap4fun_account, newPassword, successCallback)
  DebugOut("GameUtils:Tap4funModifyPassword")
  assert(tap4fun_account)
  assert(newPassword)
  local httpheader = {
    AccessToken = "A6E56CD1-B178-8B82-E6E0-02DCF13D7A25"
  }
  GameUtils._httpData = "passport=" .. tap4fun_account.passport .. "&old_password=" .. ext.ENC1(tap4fun_account.password) .. "&new_password=" .. ext.ENC1(newPassword)
  GameUtils:SetHTTPHeader(httpheader)
  GameUtils:SetHTTPServer("https://ucenter.pf.tap4fun.com/")
  GameUtils:HTTP_SendBaseRequest("users/outside_update_password?" .. GameUtils._httpData, nil, successCallback, true, GameUtils.httpRequestFailedCallback, "GET", "notencrypt")
end
function GameUtils:Tap4funAccountValidate(tap4fun_account, successCallback)
  local data = {
    passport = tap4fun_account.passport,
    password = ext.ENC1(tap4fun_account.password),
    acc_type = GameUtils.AccountType.mail
  }
  GameUtils:SetHTTPPostData(ext.json.generate(data), nil)
  GameUtils:HTTP_SendRequest("gamers/validate", nil, successCallback, true, GameUtils.httpRequestFailedCallback, "POST")
end
function GameUtils:QihooAccountValidate(qihoo_account, successCallback)
  local data = {
    passport = qihoo_account.passport,
    password = qihoo_account.password,
    acc_type = GameUtils.AccountType.qihoo
  }
  GameUtils:SetHTTPPostData(ext.json.generate(data), nil)
  GameUtils:HTTP_SendRequest("gamers/validate", nil, successCallback, true, GameUtils.httpRequestFailedCallback, "POST")
end
function GameUtils:FacebookAccountValidate(facebook_account, successCallback, failedCallback)
  local data = {
    passport = facebook_account.passport,
    password = facebook_account.password,
    acc_type = GameUtils.AccountType.facebook
  }
  if nil == failedCallback then
    failedCallback = GameUtils.httpRequestFailedCallback
  end
  GameUtils:SetHTTPPostData(ext.json.generate(data), nil)
  GameUtils:HTTP_SendRequest("gamers/validate", nil, successCallback, true, failedCallback, "POST")
end
function GameUtils:PlatformExtAccountValidate(Ext_account_info_json, successCallback)
  local data = ext.json.parse(Ext_account_info_json)
  data.acc_type = tonumber(IPlatformExt.getConfigValue("AccType"))
  GameUtils:SetHTTPPostData(ext.json.generate(data), nil)
  GameUtils:HTTP_SendRequest("gamers/validate", nil, successCallback, true, GameUtils.httpRequestFailedCallback, "POST")
end
function GameUtils:GetLogSeverLogicID()
  local LoginInfo = self:GetLoginInfo()
  return LoginInfo.ServerInfo.logic_id
end
function GameUtils:Tap4funAccountBind(tap4fun_account, BindPlayerName, successCallback, failedCallback)
  assert(tap4fun_account)
  local device_info = {}
  device_info.udid = ext.GetIOSOpenUdid()
  device_info.mac_addr = ext.GetIOSMacAddress()
  device_info.open_udid = ext.GetIOSOpenUdid()
  device_info.game_id = GameUtils:GetLogSeverLogicID()
  local bind_data = {
    gamer = {
      passport = tap4fun_account.passport,
      password = ext.ENC1(tap4fun_account.password),
      acc_type = GameUtils.AccountType.mail
    },
    device = device_info
  }
  bind_data.role = BindPlayerName
  DebugOutPutTable(bind_data, "bindData")
  bind_data = ext.json.generate(bind_data)
  GameUtils:SetHTTPPostData(bind_data, nil)
  if nil == failedCallback then
    failedCallback = GameUtils.httpRequestFailedCallback
  end
  GameUtils:HTTP_SendRequest("gamers/bind_role", nil, successCallback, true, failedCallback, "POST")
end
function GameUtils:FacebookAccountBind(loginType, facebook_account, BindPlayerName, successCallback)
  assert(facebook_account)
  local device_info = {}
  device_info.udid = ext.GetIOSOpenUdid()
  device_info.mac_addr = ext.GetIOSMacAddress()
  device_info.open_udid = ext.GetIOSOpenUdid()
  device_info.game_id = GameUtils:GetLogSeverLogicID()
  local AccountType
  if loginType == GameUtils.AccountType.facebook then
    AccountType = GameUtils.AccountType.facebook
  elseif loginType == GameUtils.AccountType.tango then
    AccountType = GameUtils.AccountType.tango
  end
  local bind_data = {
    gamer = {
      passport = facebook_account.passport,
      password = facebook_account.password,
      acc_type = AccountType
    },
    device = device_info
  }
  bind_data.role = BindPlayerName
  DebugOutPutTable(bind_data, "bindData")
  bind_data = ext.json.generate(bind_data)
  GameUtils:SetHTTPPostData(bind_data, nil)
  GameUtils:HTTP_SendRequest("gamers/bind_role", nil, successCallback, true, GameUtils.httpRequestFailedCallback, "POST")
end
function GameUtils:UpdateServerList(SuccessCallback, AccountTable)
  local function successCallback(content)
    if SuccessCallback then
      SuccessCallback(content)
    end
  end
  local httpRequestInfo = GameLoader:GetGameText("LC_ALERT_HTTP_FAILED")
  local function failedCallback(statusCode)
    local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
    local serverName = " "
    serverName = not GameUtils:GetActiveServerInfoHook() or serverName .. GameUtils:GetActiveServerInfoHook().name or GameUtils:GetActiveServerInfoHook().id or ""
    local extra_error = "Error:1_" .. 5 .. "_" .. statusCode
    local errorText = GameLoader:GetGameText("LC_ALERT_HTTP_FAILED") .. serverName .. extra_error
    local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
    GameSetting:showContactUsDialogForNetError("ERROR", errorText, function()
      GameUtils:UpdateServerList(SuccessCallback, tap4fun_account)
    end)
  end
  local httpdata = {}
  httpdata.passports = {}
  if AccountTable then
    httpdata.passports = AccountTable
  end
  DebugOutPutTable(httpdata, "httpdata")
  httpdata = ext.json.generate(httpdata)
  DebugOut(httpdata)
  GameUtils:SetHTTPPostData(httpdata, nil)
  GameUtils:HTTP_SendRequest("gamers/udid_v2", nil, successCallback, false, failedCallback, "POST")
end
function GameUtils:RequestAccountPlayerTable(AccountInfo, SuccessCallback, FailedCallback)
  if nil == AccountInfo then
    AccountInfo = {passort = "", password = ""}
  end
  assert(AccountInfo)
  local DataPacket = {}
  DataPacket.passport = AccountInfo.passport
  if IPlatformExt.getConfigValue("UseExtLogin") == "True" then
    local accInfoStr = GameUtils.IPlatformExtLoginInfo_json or GameUtils.IPlatformExtLastLoginInfo
    if accInfoStr then
      local accInfo = {}
      accInfo = ext.json.parse(accInfoStr)
      if accInfo then
        DataPacket.password = accInfo.password
        if IPlatformExt.getConfigValue("PlatformName") == "taptap" then
          DataPacket.passport = accInfo.passport
        end
      end
    end
    DataPacket.acc_type = tonumber(IPlatformExt.getConfigValue("AccType"))
  elseif GameUtils:IsFacebookAccount(AccountInfo) then
    DataPacket.password = AccountInfo.password
    DataPacket.acc_type = GameUtils.AccountType.facebook
  elseif GameUtils:IsLoginTangoAccount() then
    DataPacket.password = AccountInfo.password
    DataPacket.acc_type = GameUtils.AccountType.tango
  elseif AccountInfo.accType ~= GameUtils.AccountType.guest and GameUtils:IsMailAccount(AccountInfo) then
    DataPacket.password = ext.ENC1(AccountInfo.password)
    DataPacket.acc_type = GameUtils.AccountType.mail
  elseif GameUtils:IsQihooApp() then
    GameUtils:printByAndroid("GameUtils:IsQihooAccount(AccountInfo)")
    DataPacket.acc_type = GameUtils.AccountType.qihoo
    DataPacket.password = GameUtils.qihoo_token
    GameUtils:printByAndroid("DataPacket.passport: " .. DataPacket.passport .. "/ DataPacket.acc_type: " .. DataPacket.acc_type)
  else
    DataPacket.passport = ""
    DataPacket.password = ext.ENC1("")
    DataPacket.acc_type = GameUtils.AccountType.guest
  end
  DebugOutPutTable(DataPacket, "AccountData")
  DataPacket = ext.json.generate(DataPacket)
  GameUtils:SetHTTPPostData(DataPacket, nil)
  GameUtils:HTTP_SendRequest("gamers/passport_v2", nil, SuccessCallback, true, FailedCallback, "POST")
end
function GameUtils:GetAccountLocalData(AccountPassport, accType)
  if GameSettingData.PassportTable then
    local TargetAccount
    for _, AccountInfo in ipairs(GameSettingData.PassportTable) do
      if AccountInfo.passport == AccountPassport then
        if accType then
          local tmp = AccountInfo.accType or GameUtils.AccountType.mail
          if tmp == accType then
            TargetAccount = AccountInfo
            break
          end
        else
          TargetAccount = AccountInfo
          break
        end
      end
    end
    return TargetAccount
  end
  return nil
end
function GameUtils:AddAccountLocalData(AccountLocalData)
  if not GameSettingData.PassportTable then
    GameSettingData.PassportTable = {}
  end
  if IPlatformExt.getConfigValue("UseExtLogin") == "True" then
    GameSettingData.PassportTable = {}
    table.insert(GameSettingData.PassportTable, AccountLocalData)
    GameUtils:SaveSettingData()
    return
  end
  for _, AccountInfo in ipairs(GameSettingData.PassportTable) do
    if AccountInfo.passport == AccountLocalData.passport then
      AccountInfo.password = AccountLocalData.password
      GameUtils:SaveSettingData()
      return
    end
  end
  if GameUtils:IsQihooApp() then
    GameUtils:printByAndroid("\229\176\134qihoo\232\180\166\229\143\183\230\183\187\229\138\160\229\136\176GameSettingData.PassportTable[1]")
    GameSettingData.PassportTable[1] = AccountLocalData
    GameUtils:SaveSettingData()
  else
    table.insert(GameSettingData.PassportTable, AccountLocalData)
    DebugOutPutTable(GameSettingData.PassportTable, "AccountLocalTable")
    GameUtils:SaveSettingData()
  end
end
function GameUtils:RemoveAccountLocalData(passport)
  if GameSettingData.PassportTable then
    for IndexData, AccountInfo in ipairs(GameSettingData.PassportTable) do
      if AccountInfo.passport == passport then
        table.remove(GameSettingData.PassportTable, IndexData)
        break
      end
    end
    if GameSettingData.LastLogin and GameSettingData.LastLogin.AccountInfo and GameSettingData.LastLogin.AccountInfo.passport == passport then
      GameSettingData.LastLogin = nil
    end
    self:SaveSettingData()
  end
end
function GameUtils:SetServerListData(content)
  DebugTable(content)
  GameUtils.ServerAndAccountInfo = content
end
function GameUtils:GetServerInfoWithIP(server_ip)
  for _, v in ipairs(GameUtils.ServerAndAccountInfo.can_use_servers) do
    if v.ip == server_ip then
      return v
    end
  end
  return nil
end
function GameUtils:WriteUserName(text)
  if GameGlobalData:GetUserInfo() then
    text = string.gsub(text, "<player>", GameUtils:GetUserDisplayName(GameGlobalData:GetUserInfo().name))
    text = string.gsub(text, "<v_player>", GameUtils:GetUserDisplayName(GameUtils:GetAdjutantName()))
  end
  return text
end
function GameUtils:GetServerInfoWithID(server_id)
  for _, v in ipairs(GameUtils.ServerAndAccountInfo.server_list) do
    if v.logic_id == server_id then
      return v
    end
  end
  return nil
end
function GameUtils:GetActiveServerInfo()
  return self.LoginInfo.ServerInfo
end
function GameUtils:GetActiveServerInfoHook()
  if self.LoginInfo then
    return self.LoginInfo.ServerInfo
  end
  return nil
end
function GameUtils:SetLoginInfo(LoginInfo)
  if LoginInfo and LoginInfo.RegistAppId == nil then
    LoginInfo.RegistAppId = ""
  elseif LoginInfo and LoginInfo.isCreateActor == nil then
    LoginInfo.isCreateActor = 1
  end
  self.LoginInfo = LoginInfo
end
function GameUtils:GetLoginInfo()
  return self.LoginInfo
end
function GameUtils:CheckFirstEnter()
  local firstenter = true
  firstenter = firstenter and #self.ServerAndAccountInfo.passports_last == 0
  firstenter = firstenter and #self.ServerAndAccountInfo.passports == 0
  firstenter = firstenter and #self.ServerAndAccountInfo.udid_roles == 0
  return firstenter
end
function GameUtils.GetServerList()
  return GameUtils.ServerAndAccountInfo.can_use_servers
end
function GameUtils.numberAddCommaWithBackslash(numToConvert, backslash)
  local num = tonumber(numToConvert)
  if num and type(num) == "number" then
    local formatted = tostring(num)
    while true do
      if not backslash then
        formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", "%1,%2")
      else
        formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", "%1,%2")
      end
      if k == 0 then
        break
      end
    end
    return formatted
  else
    return numToConvert
  end
end
function GameUtils.numberAddComma(numToConvert)
  return ...
end
function GameUtils.numberConversion(num)
  local convertedNum
  if num >= 1000000000000 then
    convertedNum = string.format("%.1fT", num / 1000000000000)
  elseif num >= 100000000000 then
    convertedNum = string.format("%3dB", num / 1000000000)
  elseif num >= 1000000000 then
    convertedNum = string.format("%.1fB", num / 1000000000)
  elseif num >= 100000000 then
    convertedNum = string.format("%3dM", num / 1000000)
  elseif num >= 10000000 and num < 100000000 then
    convertedNum = string.format("%.1fM", num / 1000000)
  elseif num >= 1000000 and num < 10000000 then
    convertedNum = string.format("%.1fM", num / 1000000)
  elseif num >= 100000 and num < 1000000 then
    convertedNum = string.format("%3dK", num / 1000)
  elseif num >= 10000 and num < 100000 then
    convertedNum = string.format("%.1fk", num / 1000)
  else
    convertedNum = string.format("%d", num)
  end
  if num > 0 and num < 1 then
    convertedNum = string.format("%.2f", num)
  end
  return convertedNum
end
function GameUtils.numberConversionEnglish(num)
  local convertedNum
  if num >= 1000000000000 then
    convertedNum = string.format("%.1fT", num / 1000000000000)
  elseif num >= 100000000000 then
    convertedNum = string.format("%3dB", num / 1000000000)
  elseif num >= 1000000000 then
    convertedNum = string.format("%.1fB", num / 1000000000)
  elseif num >= 100000000 then
    convertedNum = string.format("%3dM", num / 1000000)
  elseif num >= 10000000 and num < 100000000 then
    convertedNum = string.format("%.1fM", num / 1000000)
  elseif num >= 1000000 and num < 10000000 then
    convertedNum = string.format("%.1fM", num / 1000000)
  elseif num >= 100000 and num < 1000000 then
    convertedNum = string.format("%3dK", num / 1000)
  elseif num >= 10000 and num < 100000 then
    convertedNum = string.format("%.1fk", num / 1000)
  else
    convertedNum = string.format("%d", num)
  end
  return convertedNum
end
function GameUtils.numberConversion2(num, isNoLimited)
  DebugOut("numberConversion2", num)
  if isNoLimited then
    convertedNum = string.format("%.0f", num)
  elseif num >= 1.0E20 then
    local countM = 0
    while num >= 1.0E20 do
      countM = countM + 1
      num = num / 1000000
    end
    convertedNum = string.format("%.0f", num)
    while countM > 0 do
      countM = countM - 1
      convertedNum = convertedNum .. "M"
    end
  else
    convertedNum = string.format("%.0f", num)
  end
  return convertedNum
end
function GameUtils.numberConversion3(num, isNoLimited)
  local convertedNum
  if num >= 1000000000000 then
    convertedNum = string.format("%3dMM", num / 1000000000000)
  elseif num >= 100000000 then
    convertedNum = string.format("%3dM", num / 1000000)
  elseif num >= 10000000 and num < 100000000 then
    convertedNum = string.format("%.1fM", num / 1000000)
  else
    convertedNum = string.format("%d", num)
  end
  return convertedNum
end
function GameUtils.numberConversionFloat(num)
  local convertedNum
  if num >= 100000000 then
    convertedNum = string.format("%3dM", num / 1000000)
  elseif num >= 10000000 and num < 100000000 then
    convertedNum = string.format("%.1fM", num / 1000000)
  elseif num >= 1000000 and num < 10000000 then
    convertedNum = string.format("%.1fM", num / 1000000)
  elseif num >= 100000 and num < 1000000 then
    convertedNum = string.format("%3dK", num / 1000)
  elseif num >= 10000 and num < 100000 then
    convertedNum = string.format("%.1fk", num / 1000)
  elseif num > math.floor(num) then
    convertedNum = string.format("%.1f", num)
  else
    convertedNum = string.format("%d", num)
  end
  return convertedNum
end
function GameUtils:Warning(str)
  print(" Warning: " .. str)
end
function GameUtils:Error(str)
  print(" Error: " .. str)
end
function GameUtils:printTab(n)
  for i = 1, n do
    io.write("\t")
  end
end
function GameUtils:printValue(v, depth)
  if type(v) == "string" then
    io.write(string.format("%q", v))
  elseif type(v) == "number" then
    io.write(v)
  elseif type(v) == "boolean" then
    io.write(v and "true" or "false")
  elseif type(v) == "table" then
    GameUtils:printTable(v, depth)
  elseif type(v) == "userdata" then
    io.write("userdata")
  elseif type(v) == "function" then
    io.write("function")
  else
    self:Warning("invalid type " .. type(v))
  end
end
function GameUtils:printTable(t, depth)
  if t == nil then
    print("printTable: nil table")
    return
  end
  local depth = depth or 1
  if type(t) ~= "table" or type(depth) ~= "number" then
    self:Warning("error depth; ignore")
    return
  end
  if depth > 9 then
    self:Warning("too many depth; ignore")
    return
  end
  io.write("{\n")
  for k, v in pairs(t) do
    GameUtils:printTab(depth)
    io.write("[")
    GameUtils:printValue(k, depth + 1)
    io.write("] = ")
    GameUtils:printValue(v, depth + 1)
    io.write(",\n")
  end
  GameUtils:printTab(depth - 1)
  io.write("}\n")
end
function GameUtils:SaveTable(file, t, depth)
  if t == nil then
    print("printTable: nil table")
    return
  end
  local depth = depth or 1
  if depth > 9 then
    self:Warning("too many depth; ignore")
    return
  end
  file:write("{\n")
  for k, v in pairs(t) do
    GameUtils:SaveTab(file, depth)
    file:write("[")
    GameUtils:SaveValue(file, k, depth + 1)
    file:write("] = ")
    GameUtils:SaveValue(file, v, depth + 1)
    file:write(",\n")
  end
  GameUtils:SaveTab(file, depth - 1)
  file:write("}\n")
end
function GameUtils:SaveValue(file, v, depth)
  if type(v) == "string" then
    file:write(string.format("%q", v))
  elseif type(v) == "number" then
    file:write(v)
  elseif type(v) == "boolean" then
    file:write(v and "true" or "false")
  elseif type(v) == "table" then
    GameUtils:SaveTable(file, v, depth)
  elseif type(v) == "userdata" then
    file:write("userdata")
  elseif type(v) == "function" then
    file:write("function")
  else
    self:Warning("invalid type " .. type(v))
  end
end
function GameUtils:SaveTab(file, n)
  for i = 1, n do
    file:write("\t")
  end
end
function GameUtils:outputTable(t, name)
  io.write(name .. " = ")
  GameUtils:printTable(t)
end
function GameUtils:formatValue(v, out)
  if type(v) == "string" then
    if pcall(function()
      string.format("%q", v)
    end) then
      if pcall(function()
        out = out .. string.format("%q", v)
      end) then
      else
        error("formatValue out connect Wrong ")
      end
    else
      error("formatValue format Wrong")
    end
  elseif type(v) == "number" then
    out = out .. v
  elseif type(v) == "boolean" then
    out = out .. (v and "true" or "false")
  elseif type(v) == "table" then
    out = GameUtils:formatTable(v, out)
  else
    error("Wrong value type for data table!")
  end
  return out
end
function GameUtils:ShowTips(content)
  local GameTip = LuaObjectManager:GetLuaObject("GameTip")
  GameTip:Show(content)
end
function GameUtils:formatTable(t, out)
  out = out .. "{\n"
  for k, v in pairs(t) do
    out = out .. "["
    out = GameUtils:formatValue(k, out)
    out = out .. "] = "
    out = GameUtils:formatValue(v, out)
    out = out .. ",\n"
  end
  out = out .. " }\n"
  return out
end
function GameUtils:formatSaveString(t, name)
  local out = name .. " = "
  out = GameUtils:formatTable(t, out)
  return out
end
function GameUtils:CombineBattleID(actId, battleID)
  return actId * 1000000 + battleID
end
function GameUtils:ResolveBattleID(combinedID)
  local actID = math.floor(combinedID / 1000000)
  local battleID = combinedID - actID * 1000000
  return actID, battleID
end
function GameUtils:CheckEventType(battleID)
  local event_type = battleID % 1000
  event_type = math.floor(event_type / 100)
  return event_type
end
function GameUtils.DeviceLangToText(str)
  if ext.SNSdkType == "kakao" then
    return "kr"
  end
  if string.find(str, "en") == 1 then
    return "en"
  elseif string.find(str, "zh_Hans") == 1 then
    return "cn"
  elseif string.find(str, "zh_Hant") == 1 then
    return "zh"
  elseif string.find(str, "ja") == 1 then
    return "jp"
  elseif string.find(str, "de") == 1 then
    return "ger"
  elseif string.find(str, "fr") == 1 then
    return "fr"
  elseif string.find(str, "ko") == 1 then
    return "kr"
  elseif string.find(str, "it") == 1 then
    return "it"
  elseif string.find(str, "pt") == 1 then
    return "po"
  elseif string.find(str, "es") == 1 then
    return "sp"
  elseif string.find(str, "ru") == 1 then
    return "ru"
  elseif string.find(str, "nl") == 1 then
    return "nl"
  else
    return "en"
  end
end
function GameUtils:formatNumber(num)
  if num > 1000000000 then
    num = math.floor(num / 1000000000)
    return "" .. num .. "B"
  elseif num > 1000000 then
    num = math.floor(num / 1000000)
    return "" .. num .. "M"
  elseif num > 10000 then
    num = math.floor(num / 1000)
    return "" .. num .. "K"
  else
    return "" .. num
  end
end
function GameUtils:formatTimeStringToNBit(timevalue)
  local checkarray = {
    [1] = {
      sc = 2592000,
      ss = GameUtils:GetTimeChar("M")
    },
    [2] = {
      sc = 86400,
      ss = GameUtils:GetTimeChar("d")
    },
    [3] = {
      sc = 3600,
      ss = GameUtils:GetTimeChar("h")
    },
    [4] = {
      sc = 60,
      ss = GameUtils:GetTimeChar("m")
    },
    [5] = {
      sc = 1,
      ss = GameUtils:GetTimeChar("s")
    }
  }
  local valuetable = {}
  local valueindex = -1
  local n = 0
  if timevalue >= checkarray[1].sc then
    n = 1
  elseif timevalue >= checkarray[2].sc then
    n = 2
  elseif timevalue >= checkarray[3].sc then
    n = 2
  elseif timevalue >= checkarray[4].sc then
    n = 2
  else
    n = 1
  end
  for i = 1, #checkarray do
    if timevalue > checkarray[i].sc then
      local count = math.floor(timevalue / checkarray[i].sc)
      local _str = tostring(count) .. checkarray[i].ss
      table.insert(valuetable, _str)
      if #valuetable == n then
        break
      end
      timevalue = timevalue % checkarray[i].sc
    end
  end
  local timeString = ""
  for i = 1, #valuetable do
    timeString = timeString .. valuetable[i]
  end
  return timeString
end
function GameUtils:GetUserDisplayName(name_full)
  local server_suffix = "." .. GameUtils:GetActiveServerInfo().tag
  local index_suffix = string.find(name_full, server_suffix)
  if index_suffix then
    local name_display = string.sub(name_full, 1, index_suffix - 1)
    return name_display
  else
    return name_full
  end
end
function GameUtils:CutOutUsernameByLastDot(name_full)
  local totoalName = ""
  startpos, endpos = string.find(name_full, "[%.]")
  while startpos do
    local name_preffix = string.sub(name_full, 1, startpos - 1)
    if totoalName ~= "" then
      totoalName = totoalName .. "."
    end
    totoalName = totoalName .. name_preffix
    name_full = string.sub(name_full, startpos + 1, string.len(name_full))
    startpos, endpos = string.find(name_full, "[%.]")
  end
  if totoalName == "" then
    totoalName = name_full
  end
  return totoalName
end
function GameUtils:GetUserServerName(name_full)
  local index = string.find(name_full, "%.")
  if index then
    local serverName = string.sub(name_full, index + 1, string.len(name_full))
    return serverName
  else
    return nil
  end
end
function GameUtils:GetUserFullName(name_user)
  local server_suffix = "." .. GameUtils:GetActiveServerInfo().tag
  local index_suffix = string.find(name_user, server_suffix)
  if index_suffix then
    return name_user
  else
    local name_full = name_user .. server_suffix
    return name_full
  end
end
function GameUtils:SaveSettingData()
  if ext.SaveRawData then
    ext.SaveRawData("SETTING.raw", GameUtils:formatSaveString(GameSettingData, "GameSettingData"))
  else
    ext.SaveGameData("SETTING.tfl", GameUtils:formatSaveString(GameSettingData, "GameSettingData"))
  end
end
function GameUtils:LoadSettingData()
  if ext.IsFileExist then
    if ext.IsFileExist("SETTING.raw") and ext.LoadRawData then
      loadstring(ext.LoadRawData("SETTING.raw"))()
    else
      ext.dofile("SETTING.tfl")
      if ext.SaveRawData then
        ext.SaveRawData("SETTING.raw", GameUtils:formatSaveString(GameSettingData, "GameSettingData"))
      end
    end
  else
    ext.dofile("SETTING.tfl")
  end
end
function GameUtils:CheckUpdateSettingData()
  local isUpdated = false
  if GameSettingData == nil then
    GameUtils:LoadSettingData()
    if GameSettingData == nil then
      GameSettingData = {}
      GameSettingData.EnableMusic = true
      GameSettingData.EnableSound = true
      GameSettingData.EnablePayRemind = true
      GameSettingData.FirstEnterGame = true
      GameSettingData.EnableBuildingName = true
      GameSettingData.EnableChatEffect = true
      GameSettingData.EnableSkipStarSystemAnim = false
      isUpdated = true
    end
  end
  if GameSettingData and GameSettingData.EnablePayRemind == nil then
    GameSettingData.EnablePayRemind = true
    isUpdated = true
  end
  if GameSettingData and GameSettingData.EnableSkipStarSystemAnim == nil then
    GameSettingData.EnableSkipStarSystemAnim = false
    isUpdated = true
  end
  if GameSettingData.LastLogin and GameSettingData.LastLogin.ServerInfo and GameSettingData.LastLogin.ServerInfo.server_version == nil then
    GameSettingData.LastLogin.ServerInfo.server_version = 1
    isUpdated = true
  end
  if GameSettingData.Save_Lang == nil then
    GameSettingData.Save_Lang = GameUtils.DeviceLangToText(ext.GetDeviceLanguage())
    GameSettingData.RECORD_VERSION = 1
    isUpdated = true
  end
  if GameSettingData.m_lastSectionShowEnterDialog == nil then
    GameSettingData.m_lastSectionShowEnterDialog = -10
    GameSettingData.RECORD_VERSION = 1
    isUpdated = true
  end
  if isUpdated then
    GameUtils:SaveSettingData()
  end
  if IPlatformExt.getConfigValue("PlatformName") == "taptap" then
    local sucfunc = function(jsoninfo)
      local newjson = {
        passport = jsoninfo.access_token,
        password = jsoninfo.openid
      }
      local strnewjson = ext.json.generate(newjson)
      GameUtils.IPlatformExtLoginInfo_json = strnewjson
      GameUtils.IPlatformExtLastLoginInfo = strnewjson
      GameUtils:PlatformExtAccountValidate(strnewjson, GameAccountSelector.PlatformExtAccountValidateCallback_Standard)
      GameSettingData.wechat_refrshtoken = jsoninfo.refresh_token
    end
    if GameSettingData.wechat_refrshtoken and #GameSettingData.wechat_refrshtoken > 0 then
      local sucfunc = function(jsoninfo)
        local newjson = {
          passport = jsoninfo.access_token,
          password = jsoninfo.openid
        }
        local strnewjson = ext.json.generate(newjson)
        GameUtils.IPlatformExtLoginInfo_json = strnewjson
        GameUtils.IPlatformExtLastLoginInfo = strnewjson
      end
      GameUtils:wechat_getToken_by_refresh(GameSettingData.wechat_refrshtoken, sucfunc, function()
      end)
    end
  end
end
function GameUtils._RestartGameCallback()
  if GameUtils.clearFontObj then
    GameUtils.clearFontObj:clearFonts()
  end
  ext.RestartGame()
  return nil
end
function GameUtils:SetEnableSkipStarSystemAnim(skip)
  DebugOut("SetEnableSkipStarSystemAnim" .. type(skip) .. " " .. tostring(skip))
  GameSettingData.EnableSkipStarSystemAnim = skip
  GameUtils:SaveSettingData()
end
function GameUtils:RestartGame(delayTime, flash_obj)
  GameSettingData.LastLoginSuccess = false
  GameUtils:SaveSettingData()
  GameUtils.clearFontObj = flash_obj
  delayTime = delayTime or 200
  GameTimer:Add(GameUtils._RestartGameCallback, delayTime)
end
function GameUtils:GetPlayerSexByAvatra(avatar)
  if avatar == "male" then
    return 1
  end
  return 0
end
function GameUtils:GetPlayerAvatar(playerSex, fleetLevel)
  local sex = playerSex or GameGlobalData:GetUserInfo().sex
  local userinfo = GameGlobalData:GetUserInfo()
  local avatar = ""
  if fleetLevel and fleetLevel >= 6 then
    if 1 == sex then
      avatar = "head300"
    else
      avatar = "head301"
    end
    if GameDataAccessHelper:IsHeadResNeedDownload(avatar) and GameDataAccessHelper:CheckFleetHasAvataImage(avatar) then
      DebugOut(avatar)
      return avatar
    end
  end
  if 1 == sex then
    return "male"
  else
    return "female"
  end
end
function GameUtils:GetPlayerAvatarWithSex(sex, fleetLevel)
  if fleetLevel and fleetLevel >= 6 then
    local avatar = ""
    if 1 == sex then
      avatar = "head300"
    else
      avatar = "head301"
    end
    if GameDataAccessHelper:IsHeadResNeedDownload(avatar) and GameDataAccessHelper:CheckFleetHasAvataImage(avatar) then
      return avatar
    end
  end
  if 1 == sex then
    return "male"
  else
    return "female"
  end
end
function GameUtils:GetAdjutantName()
  local sex = GameGlobalData:GetUserInfo().sex
  if 1 == sex then
    return ...
  else
    return ...
  end
end
function GameUtils:GetAdjutantAvatar()
  local sex = GameGlobalData:GetUserInfo().sex
  if 1 == sex then
    return "head10"
  else
    return "head11"
  end
end
function GameUtils:GetUserVoiceKeyAndSecret()
  local locale = ext.GetDeviceLanguage()
  local key = -1
  local secret = -1
  if locale == "zh_Hans" then
    key = "gIjCQ0Fgk9z7XilcW01qQ"
    secret = "oXCgnY9jmbgQSLoJrdfbx863SmHSxroYDVJa4zjE"
  elseif locale == "fr" then
    key = "Ol24IVOigSIfv3GshZZrVw"
    secret = "t4jbNH4pdn6amIcF27WayZM2iu93ghLQdukEVX2XguY"
  elseif locale == "zh_Hant" then
    key = "mxlvDhw7x5zKFWpZjArwXw"
    secret = "TPUTwnmwLO4dR4MzUNpDm7sRZcMPobnXphPqhXAO8"
  elseif locale == "ja" then
    key = "o8AI6vNKDzyf0LGjmELdnA"
    secret = "mHUh1ShTHilq9XPCA9v8CrMIOssUqQ4Zhrwqjq7Ac"
  elseif locale == "es" then
    key = "iBbE21qwCVN838q4qj1w"
    secret = "MHjZMwAQJeDvNUemOJ3jcuP3S7Iw094bPXtsHKoAY"
  elseif locale == "it" then
    key = "XbvkcxFrv2AKppJWISEc3A"
    secret = "DWau4uHgD9AVZFFmaBuiYufOqNCrlcXtli4UQOReGo"
  elseif locale == "nl" then
    key = "T4ANcCGGgDvdUV8lVo0NA"
    secret = "dIlF1xO4l3RXA96jg5I0YM52FtodaTtGuzrvZhr08KU"
  elseif locale == "de" then
    key = "VngwJI4ubxWKu4cv9GOG2w"
    secret = "cL1Wa3FGsFtnh7ODhqcA3K9bx9cCylCueP9tuABU"
  elseif locale == "pt" then
    key = "tm7ZP2cdSKuswt7CEd97jw"
    secret = "bTLcTSyE5YWoW2AZEoFA8wUp7J1T1Ys6Bi8DgYZpe8"
  elseif locale == "ko" then
    key = "wW22rANdoUU7VG4GVZ0Pg"
    secret = "FF79ZgIGaa1f9CzXxhPJTwrEk4CV2pKAuZ0YXlzXE"
  elseif locale == "ru" then
    key = "vpDUyjXQIbbmLXXph7Fkxg"
    secret = "aCZnFDqQOiKZ0QpEseWdEYILl1yub83NpzdsY7aTiI"
  else
    key = "sT7K93hMFj8cWrehCINRA"
    secret = "cEy7MweT4E4kdcGRiIhx4QBgpy5i3FtsLbVGEin4lhs"
  end
  return key, secret
end
function GameUtils:PlayMusic(music_path)
  DebugOut("play music = ", music_path)
  if GameSettingData.EnableMusic == nil then
    GameSettingData.EnableMusic = true
    GameUtils:SaveSettingData()
  end
  local indexDir = string.find(music_path, "sound/")
  if not indexDir then
    music_path = "sound/" .. music_path
  end
  if GameSettingData.EnableMusic and self.ActiveMusic ~= music_path then
    self.ActiveMusic = music_path
    ext.audio.playBackgroundMusic(music_path)
  end
end
function GameUtils:StopMusic()
  ext.audio.stopBackgroundMusic()
end
function GameUtils:PlaySound(sound_path)
  DebugOut("PlaySound = ", sound_path)
  if GameSettingData.EnableSound == nil then
    GameSettingData.EnableSound = true
    GameUtils:SaveSettingData()
  end
  if GameSettingData.EnableSound then
    local indexDir = string.find(sound_path, "sound/")
    if not indexDir then
      sound_path = "sound/" .. sound_path
    end
    local sid = ext.audio.playEffect(sound_path)
    return sid
  end
end
function GameUtils:StopSound(soundid)
  assert(type(soundid) == "number")
  ext.audio.stopEffect(soundid)
end
function GameUtils:PlayCG()
  if AutoUpdate.isAndroidDevice then
    GameUtils:PlayMovie("cut_scene.mp4", true)
  else
    GameUtils:PlayMovie("cut_scene.mov", true)
  end
end
function GameUtils:PlayMovie(movie_path, isCanSkip)
  if isCanSkip then
    isCanSkip = true
  else
    isCanSkip = false
  end
  local indexDir = string.find(movie_path, "video/")
  if not indexDir then
    movie_path = "video/" .. movie_path
  end
  ext.video.playMovie(movie_path, isCanSkip)
end
function GameUtils:IsMovieFinished()
