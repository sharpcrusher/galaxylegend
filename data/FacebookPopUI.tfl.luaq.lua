local FacebookPopUI = LuaObjectManager:GetLuaObject("FacebookPopUI")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
FACEBOOKMENUTYPE = {
  TYPE_MENU_LOGIN = 1,
  TYPE_MENU_CONGRATULATION = 2,
  TYPE_MENU_INVITE = 3,
  TYPE_MENU_BIND_TIPS = 4
}
FACEBOOKBINDAWARDTYPE = {
  AWARD_PVESUPPLY = 1,
  AWARD_PRESTIGE_CREDIT = 2,
  AWARD_VIP_CREDIT = 3,
  AWARD_UNLOCK_ENHANCEALL = 4,
  AWARD_UNLOCK_COLLECTALL = 5,
  AWARD_FIRSTLOGIN_CREDIT = 6,
  AWARD_NO_AWARD_VIP = 7,
  AWARD_NO_AWARD_FIRSTLOGIN = 8
}
FACEBOOKFEATURESWITCH = {
  SWITCH_PRESTIGE = 1,
  SWITCH_BIND = 2,
  SWITCH_PAY_BIND = 3,
  SWITCH_PAY_INVITE = 4,
  SWITCH_ENHANCE = 5,
  SWITCH_COLLECT = 6,
  SWITCH_SUPPLY = 7,
  SWITCH_INVITE_SUCC = 8,
  SWITCH_SELECT_ALL = 9,
  SWITCH_3_BIND_INVITE = 10,
  SWITCH_5_BIND_INVITE = 11,
  SWITCH_6_BIND_INVITE = 12,
  SWITCH_7_BIND_INVITE = 13,
  SWITCH_FIRSTLOGIN_BIND = 14,
  SWITCH_14_BIND_INVITE = 15,
  SWITCH_FBBALL_SHOW = 16
}
Friends_Per_Row = 3
function FacebookPopUI:OnInitGame()
  self:InitData()
  NetMessageMgr:RegisterMsgHandler(NetAPIList.facebook_switch_ntf.Code, FacebookPopUI.FeatureEnableNtf)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.first_login_ntf.Code, FacebookPopUI.shouldPopNtf)
end
function FacebookPopUI.FeatureEnableNtf(content)
  for i, v in ipairs(content.switches) do
    FacebookPopUI.mFeatureSwitch[v.key] = v.value
    FacebookGraphStorySharer.mFeatureSwitch[v.key] = v.value
  end
  if GameUtils:IsChinese() then
    FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_FBBALL_SHOW] = false
  end
  FacebookGraphStorySharer.mMaxImgCount = content.story_image_max
  FacebookGraphStorySharer.mMaxTextCount = content.story_title_max
  FacebookPopUI.mBindAwards = content.bind_awards
  FacebookPopUI.mBallFreSecs = FacebookPopUI.mBallFreSecs or content.ball_loop_seconds
  FacebookPopUI.mBallLoopTime = FacebookPopUI.mBallLoopTime or content.ball_loop_times
  FacebookPopUI:CheckPop()
  GameStateMainPlanet:CheckShowFBBall()
end
function FacebookPopUI:InitData()
  FacebookPopUI.mFeatureSwitch = {}
  FacebookPopUI.mBindAwards = {}
  FacebookPopUI.mInviteableFriends = nil
  FacebookPopUI.mGameFriendsList = {}
  FacebookPopUI.mInviteableFriendsRowList = {}
  FacebookPopUI.mInviteableWithFriendsExcluded = {}
  FacebookPopUI.mExcludeInviteableFriends = {}
  FacebookPopUI.mInviteTokenTableList = {}
  FacebookPopUI.mCurrentTokenTable = {}
  FacebookPopUI.mCurInviteAwards = {}
  FacebookPopUI.mCurInviteAnimIndex = 0
  FacebookPopUI.mCurInviteType = -1
  FacebookPopUI.mCurrentTokenTableIndex = -1
  FacebookPopUI.mCurLoginAwardType = nil
  FacebookPopUI.mLogInAwardCallback = nil
  FacebookPopUI.mManullyCloseCallback = nil
  FacebookPopUI.mTipsShowTimer = nil
  FacebookPopUI.TIPS_SHOW_TIME = 3000
  FacebookPopUI.mInviteTitleText = nil
  FacebookPopUI.mIsGettingInviteFriends = false
  FacebookPopUI.mCurInvireCritical = 0
end
function FacebookPopUI:Clear()
  FacebookPopUI.mCurInviteAwards = {}
  FacebookPopUI.mCurInviteAnimIndex = 0
  FacebookPopUI.mIsGettingInviteFriends = false
  FacebookPopUI.mCurrentMenuType = nil
end
function FacebookPopUI:OnAddToGameState()
  if not self:GetFlashObject() then
    FacebookPopUI:Clear()
    self:LoadFlashObject()
  end
end
function FacebookPopUI:ResetInviteStateAuto()
  local curServerUTCTime = GameUIBarLeft:GetServerTimeUTC()
  if FacebookPopUI.mInviteFinishTime and FacebookPopUI.mBallLoopTime > 0 and curServerUTCTime - FacebookPopUI.mInviteFinishTime >= FacebookPopUI.mBallFreSecs then
    FacebookPopUI.mExcludeInviteableFriends = {}
    FacebookPopUI:GenerateExcludedInviteableFrineds()
    FacebookPopUI.mInviteFinishTime = nil
    FacebookPopUI.mBallLoopTime = FacebookPopUI.mBallLoopTime - 1
  end
end
function FacebookPopUI:RecordInviteFinishTime()
  FacebookPopUI.mInviteFinishTime = GameUIBarLeft:GetServerTimeUTC()
end
function FacebookPopUI:OnEraseFromGameState()
  self:UnloadFlashObject()
end
function FacebookPopUI.shouldPopNtf(content)
  if not Facebook:IsFacebookEnabled() then
    FacebookPopUI.mCanPop = false
    return
  end
  DebugOut("what's here")
  local test = false
  if 1 == content.status or test then
    FacebookPopUI.mCanPop = true
  else
    FacebookPopUI.mCanPop = false
  end
end
function FacebookPopUI:CheckPop()
  local QuestTutorialPrestige = TutorialQuestManager.QuestTutorialPrestige
  if QuestTutorialPrestige:IsFinished() and FacebookPopUI.mCanPop then
    GameNotice:AddNotice("FacebookPop")
    if GameStateManager.GameStateMainPlanet == GameStateManager:GetCurrentGameState() then
      GameNotice:CheckShowNotice()
    end
  end
end
function FacebookPopUI:ShowFacebookMenu(menuType)
  if not Facebook:IsFacebookEnabled() then
    return
  end
  if not GameStateManager:GetCurrentGameState():IsObjectInState(self) then
    self.mCurrentMenu = menuType
    GameStateManager:GetCurrentGameState():AddObject(self)
    if not self:GetFlashObject() then
      self:LoadFlashObject()
    end
  end
  self:MoveInWithType(menuType)
end
function FacebookPopUI:MoveInWithType(menuType)
  local flash_obj = self:GetFlashObject()
  if not flash_obj then
    return
  end
  self.mCurrentMenuType = menuType
  flash_obj:InvokeASCallback("_root", "AnimationMoveInWithType", menuType)
end
function FacebookPopUI:MoveOutWithType(menuType)
  local flash_obj = self:GetFlashObject()
  if not flash_obj then
    return
  end
  flash_obj:InvokeASCallback("_root", "AnimationMoveOutWithType", menuType)
end
function FacebookPopUI:SetBindAwardType(awardType)
  self.mCurLoginAwardType = awardType
  self:SetLoginMenu()
end
function FacebookPopUI:SetLoginMenu()
  local buttonText = ""
  local titleText = ""
  local awards = {}
  local animType = "credit"
  if self.mCurLoginAwardType == FACEBOOKBINDAWARDTYPE.AWARD_PVESUPPLY then
    titleText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_LOG_IN_BUTTON_3")
    buttonText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_LOG_IN_BUTTON_3")
    animType = "lightning"
  elseif self.mCurLoginAwardType == FACEBOOKBINDAWARDTYPE.AWARD_PRESTIGE_CREDIT or self.mCurLoginAwardType == FACEBOOKBINDAWARDTYPE.AWARD_VIP_CREDIT or self.mCurLoginAwardType == FACEBOOKBINDAWARDTYPE.AWARD_FIRSTLOGIN_CREDIT then
    titleText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_LOG_IN_BUTTON_4")
    buttonText = GameLoader:GetGameText("LC_MENU_LOGIN_WITH_FACEBOOK")
  elseif self.mCurLoginAwardType == FACEBOOKBINDAWARDTYPE.AWARD_UNLOCK_ENHANCEALL then
    titleText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_LOG_IN_BUTTON_1")
    buttonText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_LOG_IN_BUTTON_1")
  elseif self.mCurLoginAwardType == FACEBOOKBINDAWARDTYPE.AWARD_UNLOCK_COLLECTALL then
    titleText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_LOG_IN_BUTTON_2")
    buttonText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_LOG_IN_BUTTON_2")
  end
  local flash_obj = self:GetFlashObject()
  if not flash_obj then
    return
  end
  if self.mBindAwards then
    for i, v in ipairs(self.mBindAwards) do
      local award = {}
      award.frameIcon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v, nil, nil)
      local nameText = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
      award.count = GameUtils.numberConversion3(tonumber(nameText))
      table.insert(awards, award)
    end
  end
  DebugOut("self.mBindAwards = ")
  DebugOut("titleText = ", titleText)
  DebugOut("buttonText = ", buttonText)
  DebugTable(self.mBindAwards)
  flash_obj:InvokeASCallback("_root", "InitLoginMenu", buttonText, titleText, awards, animType)
end
function FacebookPopUI:OnFSCommand(cmd, arg)
  if cmd == "FBLoginClicked" then
    Facebook:LoginV2(FacebookPopUI.FacebookLoginCallback, true, true)
  elseif cmd == "FBLoginCloseManully" then
    if FacebookPopUI.mManullyCloseCallback then
      FacebookPopUI.mManullyCloseCallback()
      FacebookPopUI.mManullyCloseCallback = nil
    end
    FacebookPopUI.mLogInAwardCallback = nil
    Facebook:SetStatus(FACEBOOK_ACTION_STATUS.IDLE)
    local flash_obj = self:GetFlashObject()
    if not flash_obj then
      return
    end
    FacebookPopUI.needRemoveFromState = true
    flash_obj:InvokeASCallback("_root", "AnimationMoveOutWithType", FACEBOOKMENUTYPE.TYPE_MENU_LOGIN)
  elseif cmd == "FBLoginClosed" then
    self.mCurrentMenuType = nil
    if FacebookPopUI.needRemoveFromState then
      FacebookPopUI.needRemoveFromState = false
      GameStateManager:GetCurrentGameState():EraseObject(self)
    elseif FacebookPopUI.needShowBindAwardTips then
      FacebookPopUI.needShowBindAwardTips = false
      FacebookPopUI:ShowBindAwardTips()
    end
  elseif cmd == "needUpdateInviteFriends" then
    local rowIndex, mcIndex = arg:match("(%d+)&&(%d+)")
    self:NeedUpdateInviteableItem(tonumber(rowIndex), tonumber(mcIndex))
  elseif cmd == "FBInviteFriendChoose" then
    local rowIndex, subIndex, mcIndex = arg:match("(%d+)&&(%d+)&&(%d+)")
    self:SetChooseState(tonumber(rowIndex), tonumber(subIndex), tonumber(mcIndex))
  elseif cmd == "FBInviteFriends" then
    self:GenerateInviteTokenTableList()
    self:DoInviteFrineds(self.mCurrentTokenTableIndex)
    FacebookPopUI.mInviteFriendViewPoped = true
  elseif cmd == "FBContinueInviteFriends" then
    local nextTokenTable = FacebookPopUI.mInviteTokenTableList[FacebookPopUI.mCurrentTokenTableIndex]
    if nextTokenTable and #nextTokenTable > 0 then
      self:DoInviteFrineds(self.mCurrentTokenTableIndex)
      FacebookPopUI.mInviteFriendViewPoped = true
    end
    local flash_obj = self:GetFlashObject()
    if not flash_obj then
      return
    end
    flash_obj:InvokeASCallback("_root", "AnimationMoveOutWithType", FACEBOOKMENUTYPE.TYPE_MENU_CONGRATULATION)
  elseif cmd == "FBInviteFriendsCloseManully" then
    local flash_obj = self:GetFlashObject()
    if not flash_obj then
      return
    end
    Facebook:SetStatus(FACEBOOK_ACTION_STATUS.IDLE)
    FacebookPopUI.needRemoveFromState = true
    flash_obj:InvokeASCallback("_root", "AnimationMoveOutWithType", FACEBOOKMENUTYPE.TYPE_MENU_INVITE)
  elseif cmd == "FBInviteFriendsClosed" then
    GameStateMainPlanet:CheckShowFBBall()
    GameStateManager:GetCurrentGameState():EraseObject(self)
  elseif cmd == "FBCongratulationsCloseManully" then
    self.mCurrentMenuType = nil
    local flash_obj = self:GetFlashObject()
    if not flash_obj then
      return
    end
    Facebook:SetStatus(FACEBOOK_ACTION_STATUS.IDLE)
    flash_obj:InvokeASCallback("_root", "AnimationMoveOutWithType", FACEBOOKMENUTYPE.TYPE_MENU_CONGRATULATION)
  elseif cmd == "selectOrUnselectAll" then
    FacebookPopUI:SelectOrUnselectAll()
  elseif cmd == "PlayNextAnim" then
    do return end
    local flash_obj = self:GetFlashObject()
    if not flash_obj then
      return
    end
    DebugOut("self.mCurInviteAnimIndex = ", self.mCurInviteAnimIndex)
    DebugTable(self.mCurInviteAwards)
    if self.mCurInviteAnimIndex < #self.mCurInviteAwards then
      DebugOut("PlayCongratulationsPopAnim arg = ", arg)
      self.mCurInviteAnimIndex = self.mCurInviteAnimIndex + 1
      local awardItem = {}
      local nameText, count = GameHelper:getAwardNameTextAndCount(self.mCurInviteAwards[self.mCurInviteAnimIndex].item_type, self.mCurInviteAwards[self.mCurInviteAnimIndex].number, self.mCurInviteAwards[self.mCurInviteAnimIndex].no)
      awardItem.nameText = nameText .. "x" .. GameUtils.numberConversion3(tonumber(count))
      flash_obj:InvokeASCallback("_root", "PlayCongratulationsPopAnim", tonumber(arg), awardItem)
    else
      self.mCurInviteAnimIndex = 0
      self.mCurInviteAwards = {}
      self.mCurInviteType = -1
      return
    end
  elseif cmd == "PlayPopAnim" then
    local flash_obj = self:GetFlashObject()
    if not flash_obj then
      return
    end
    flash_obj:InvokeASCallback("_root", "PlayCongratulationsAnim", tonumber(arg), awardItem)
  elseif cmd == "BindAwardTipsMoveOutOver" then
    self.mCurrentMenuType = nil
    self:CheckShowInviteFrineds()
  elseif cmd == "PlayAwardShakeAnim" then
    local flash_obj = self:GetFlashObject()
    if not flash_obj then
      return
    end
    DebugTable(self.mCurInviteAwards)
    local resource = GameGlobalData:GetData("resource")
    DebugOut("PlayCongratulationsShakeAnim arg = ", arg)
    if arg and tonumber(arg) <= #self.mCurInviteAwards then
      local award = {}
      if resource[self.mCurInviteAwards[tonumber(arg)].item_type] then
        award.nameText = resource[self.mCurInviteAwards[tonumber(arg)].item_type]
        award.nameText = GameUtils.numberConversion3(tonumber(award.nameText))
      end
      flash_obj:InvokeASCallback("_root", "PlayCongratulationsShakeAnim", tonumber(arg), award)
    end
  end
end
function FacebookPopUI:LoginDirectly()
  Facebook:LoginV2(FacebookPopUI.FacebookLoginCallback, true, true)
  DebugOut("FacebookPopUI:LoginDirectly(  )")
end
function FacebookPopUI:SelectOrUnselectAll()
  for i, v in ipairs(self.mInviteableWithFriendsExcluded) do
    if v.chooseState == "unchoose" then
      FacebookPopUI:SelectAll()
      return
    end
  end
  FacebookPopUI:UnSelectAll()
end
function FacebookPopUI:SelectAll()
  for i, v in ipairs(self.mInviteableWithFriendsExcluded) do
    v.chooseState = "choose"
  end
  local flash_obj = self:GetFlashObject()
  if not flash_obj then
    return
  end
  flash_obj:InvokeASCallback("_root", "UpdateVisibleListItemRow")
  FacebookPopUI:UpdateSelectButton()
end
function FacebookPopUI:UnSelectAll()
  for i, v in ipairs(self.mInviteableWithFriendsExcluded) do
    v.chooseState = "unchoose"
  end
  local flash_obj = self:GetFlashObject()
  if not flash_obj then
    return
  end
  flash_obj:InvokeASCallback("_root", "UpdateVisibleListItemRow")
  FacebookPopUI:UpdateSelectButton()
end
function FacebookPopUI:UpdateSelectButton()
  local flash_obj = self:GetFlashObject()
  if not flash_obj then
    return
  end
  local selectNone = true
  local hasSelect = false
  for i, v in ipairs(self.mInviteableWithFriendsExcluded) do
    if v.chooseState == "unchoose" then
      selectNone = false
    else
      hasSelect = true
    end
  end
  local selectText
  if selectNone then
    selectText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_SELECT_NONE_CHAR")
  else
    selectText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_SELECT_ALL_CHAR")
  end
  flash_obj:InvokeASCallback("_root", "UpdateSelectAllButton", hasSelect, selectText)
end
function FacebookPopUI.FacebookLoginCallback(success, idOrError, token, username, email, extarError)
  DebugOut(success)
  if success then
    Facebook:CheckAccountBind()
    Facebook.mIsBindFacebookAccount = true
    if FacebookPopUI.mLogInAwardCallback then
      DebugOut("FacebookPopUI.mLogInAwardCallback")
      FacebookPopUI.mLogInAwardCallback()
    end
    FacebookPopUI.mLogInAwardCallback = nil
    DebugOut("FacebookPopUI.mCurLoginAwardType = ", FacebookPopUI.mCurLoginAwardType)
    DebugOut("FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_3_BIND_INVITE] = ", FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_3_BIND_INVITE])
    if FacebookPopUI.mCurLoginAwardType == FACEBOOKBINDAWARDTYPE.AWARD_NO_AWARD_FIRSTLOGIN then
      FacebookPopUI.needShowBindAwardTips = false
      if not Facebook:GetCanFetchInvitableFriendList() then
        FacebookPopUI.needRemoveFromState = true
      elseif FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_FIRSTLOGIN_BIND] and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_FIRSTLOGIN_BIND] == 1 then
        FacebookPopUI.mInviteTitleText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_LOG_IN_BUTTON_4")
        FacebookPopUI:GetFacebookInvitableFriendList()
      end
    elseif FacebookPopUI.mCurLoginAwardType == FACEBOOKBINDAWARDTYPE.AWARD_NO_AWARD_VIP then
      FacebookPopUI.needShowBindAwardTips = false
      if not Facebook:GetCanFetchInvitableFriendList() then
        FacebookPopUI.needRemoveFromState = true
      elseif FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_PAY_INVITE] and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_PAY_INVITE] == 1 then
        FacebookPopUI.mInviteTitleText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_LOG_IN_BUTTON_4")
        DebugOut("FacebookPopUI.mInviteTitleText = ", FacebookPopUI.mInviteTitleText)
        FacebookPopUI:GetFacebookInvitableFriendList()
      end
    else
      FacebookPopUI.needShowBindAwardTips = true
    end
    local flash_obj = FacebookPopUI:GetFlashObject()
    if flash_obj then
      FacebookPopUI:MoveOutWithType(FACEBOOKMENUTYPE.TYPE_MENU_LOGIN)
    end
    return true
  else
    return true
  end
end
function FacebookPopUI:CheckShowInviteFrineds()
  if not Facebook:GetCanFetchInvitableFriendList() then
    GameStateManager:GetCurrentGameState():EraseObject(self)
  elseif FacebookPopUI.mCurLoginAwardType == FACEBOOKBINDAWARDTYPE.AWARD_PRESTIGE_CREDIT and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_BIND] and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_BIND] == 1 then
    FacebookPopUI.mInviteTitleText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_MORE_CREDITS")
    FacebookPopUI:GetFacebookInvitableFriendList()
    if not ext.GetDeviceCountry or not ext.GetDeviceCountry() then
      local countryStr = "unknow"
    end
  elseif FacebookPopUI.mCurLoginAwardType == FACEBOOKBINDAWARDTYPE.AWARD_FIRSTLOGIN_CREDIT and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_14_BIND_INVITE] and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_14_BIND_INVITE] == 1 then
    FacebookPopUI.mInviteTitleText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_MORE_CREDITS")
    FacebookPopUI:GetFacebookInvitableFriendList()
    if not ext.GetDeviceCountry or not ext.GetDeviceCountry() then
      local countryStr = "unknow"
    end
  elseif FacebookPopUI.mCurLoginAwardType == FACEBOOKBINDAWARDTYPE.AWARD_VIP_CREDIT and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_3_BIND_INVITE] and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_3_BIND_INVITE] == 1 then
    DebugOut("show something here")
    FacebookPopUI.mInviteTitleText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_MORE_CREDITS")
    FacebookPopUI:GetFacebookInvitableFriendList()
    if not ext.GetDeviceCountry or not ext.GetDeviceCountry() then
      local countryStr = "unknow"
    end
  elseif FacebookPopUI.mCurLoginAwardType == FACEBOOKBINDAWARDTYPE.AWARD_PVESUPPLY and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_7_BIND_INVITE] and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_7_BIND_INVITE] == 1 then
    FacebookPopUI.mInviteTitleText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_MORE_CREDITS")
    FacebookPopUI:GetFacebookInvitableFriendList()
    if not ext.GetDeviceCountry or not ext.GetDeviceCountry() then
      local countryStr = "unknow"
    end
  elseif FacebookPopUI.mCurLoginAwardType == FACEBOOKBINDAWARDTYPE.AWARD_UNLOCK_COLLECTALL and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_6_BIND_INVITE] and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_6_BIND_INVITE] == 1 then
    FacebookPopUI.mInviteTitleText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_MORE_CREDITS")
    FacebookPopUI:GetFacebookInvitableFriendList()
    if not ext.GetDeviceCountry or not ext.GetDeviceCountry() then
      local countryStr = "unknow"
    end
  elseif FacebookPopUI.mCurLoginAwardType == FACEBOOKBINDAWARDTYPE.AWARD_UNLOCK_ENHANCEALL and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_5_BIND_INVITE] and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_5_BIND_INVITE] == 1 then
    FacebookPopUI.mInviteTitleText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_MORE_CREDITS")
    FacebookPopUI:GetFacebookInvitableFriendList()
    if not ext.GetDeviceCountry or not ext.GetDeviceCountry() then
      local countryStr = "unknow"
    end
  else
    FacebookPopUI.mInviteTitleText = nil
    GameStateManager:GetCurrentGameState():EraseObject(self)
  end
end
function FacebookPopUI:ShowBindAwardTips()
  local flash_obj = self:GetFlashObject()
  if not flash_obj then
    return
  end
  FacebookPopUI:SetUpBindAwardTips()
  FacebookPopUI:MoveInWithType(FACEBOOKMENUTYPE.TYPE_MENU_BIND_TIPS)
end
function FacebookPopUI:SetUpBindAwardTips()
  FacebookPopUI.mTipsShowTimer = 0
  local flash_obj = self:GetFlashObject()
  if not flash_obj then
    return
  end
  local awards = {}
  if self.mBindAwards then
    for i, v in ipairs(self.mBindAwards) do
      local award = {}
      award.frameIcon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v, nil, nil)
      local nameText = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
      award.count = GameUtils.numberConversion3(tonumber(nameText))
      table.insert(awards, award)
    end
  end
  flash_obj:InvokeASCallback("_root", "SetUpBindAwardTips", awards)
end
function FacebookPopUI:BindAwardTipsShowOver()
  if FacebookPopUI.needRemoveFromState == true then
    if FacebookPopUI.needRemoveFromState then
      FacebookPopUI.needRemoveFromState = false
      GameStateManager:GetCurrentGameState():EraseObject(self)
    end
  else
    FacebookPopUI:CheckShowInviteFrineds()
  end
end
function FacebookPopUI:GetFacebookFriendList()
  DebugOut("FacebookPopUI:GetFacebookFriendList")
  local function callback(success, friends)
    local function delayCall()
      FacebookPopUI.GetFacebookFriendListCallback(success, friends)
    end
    if NetMessageMgr.ReConnect then
      DebugOut("FacebookPopUI Now is reconnect server.")
      NetMessageMgr:AddReconnectSuccessCall(delayCall)
    else
      FacebookPopUI.GetFacebookFriendListCallback(success, friends)
    end
  end
  Facebook:GetFriendList(callback)
end
function FacebookPopUI.GetFacebookFriendListCallback(success, friends)
  DebugOut("FacebookPopUI.GetFacebookFriendListCallback")
  if success then
    DebugTable(friends)
    FacebookPopUI.facebookFriendList = friends
  end
end
function FacebookPopUI:InvireProgressFinished()
  if not Facebook:GetCanFetchInvitableFriendList() then
    return true
  end
  FacebookPopUI:ResetInviteStateAuto()
  if FacebookPopUI.mInviteableFriends then
    if not forceShow and self.mInviteableWithFriendsExcluded then
      local excludeNum = #self.mInviteableFriends - #self.mInviteableWithFriendsExcluded
      local totalNumber = #self.mInviteableFriends
      if totalNumber and totalNumber > 0 and excludeNum and excludeNum >= totalNumber then
        return true
      end
    end
    return false
  else
    return false
  end
end
function FacebookPopUI:GetFacebookInvitableFriendList(forceShow)
  if not Facebook:GetCanFetchInvitableFriendList() then
    return
  end
  if Facebook:GetStatus() == FACEBOOK_ACTION_STATUS.DOING then
    return
  end
  DebugOut("FacebookPopUI:GetFacebookInvitableFriendList")
  FacebookPopUI:ResetInviteStateAuto()
  if FacebookPopUI.mInviteableFriends then
    if not forceShow and self.mInviteableWithFriendsExcluded then
      local excludeNum = #self.mInviteableFriends - #self.mInviteableWithFriendsExcluded
      local totalNumber = #self.mInviteableFriends
      if totalNumber and totalNumber > 0 and excludeNum and excludeNum >= totalNumber then
        return
      end
    end
    FacebookPopUI.mInviteTitleText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_LOG_IN_BUTTON_4")
    FacebookPopUI:ShowFacebookMenu(FACEBOOKMENUTYPE.TYPE_MENU_INVITE)
    FacebookPopUI:PrepareInviteableFriendsUI(true)
  else
    local function callback(success, friends)
      local function delayCall()
        FacebookPopUI.GetFacebookInviteableFriendListCallback(success, friends)
      end
      if NetMessageMgr.ReConnect then
        DebugOut("FacebookPopUI Now is reconnect server.")
        NetMessageMgr:AddReconnectSuccessCall(delayCall)
      else
        FacebookPopUI.GetFacebookInviteableFriendListCallback(success, friends)
      end
    end
    FacebookPopUI.mIsGettingInviteFriends = true
    Facebook:GetInvitableFriendList(nil, callback)
  end
end
function FacebookPopUI.GetFacebookInviteableFriendListCallback(success, friends)
  DebugOut("FacebookPopUI.GetFacebookInviteableFriendListCallback")
  FacebookPopUI.mIsGettingInviteFriends = false
  if success then
    DebugOut("FacebookPopUI.GetFacebookInviteableFriendListCallback success")
    FacebookPopUI:ShowFacebookMenu(FACEBOOKMENUTYPE.TYPE_MENU_INVITE)
    FacebookPopUI.mInviteableFriends = friends.friendList
    FacebookPopUI:PrepareInviteableFriendsUI(true)
  end
end
function FacebookPopUI:PrepareInviteableFriendsUI(forceChoose)
  tmpIndex = 1
  local friendsPerRow = Friends_Per_Row
  local friendsList = self:GenerateExcludedInviteableFrineds()
  self.mInviteableFriendsRowList = {}
  for it = 1, #friendsList, friendsPerRow do
    local friendsRowData = {}
    for i = 1, friendsPerRow do
      if it + (i - 1) <= #friendsList then
        if forceChoose then
          friendsList[it + (i - 1)].chooseState = "choose"
        elseif friendsList[it + (i - 1)].chooseState == nil then
          friendsList[it + (i - 1)].chooseState = "unchoose"
        end
        friendsRowData[i] = friendsList[it + (i - 1)]
      end
    end
    DebugOut("friendsRowData = ")
    DebugTable(friendsRowData)
    friendsRowData.rowIndex = tmpIndex
    tmpIndex = tmpIndex + 1
    table.insert(self.mInviteableFriendsRowList, friendsRowData)
  end
  local flash_obj = self:GetFlashObject()
  if not flash_obj then
    return
  end
  local excludeNum = #self.mInviteableFriends - #self.mInviteableWithFriendsExcluded
  local totalNumber = #self.mInviteableFriends
  local progress = math.floor(excludeNum / totalNumber * 100) + 1
  local canSelectAll = false
  if self.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_SELECT_ALL] and self.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_SELECT_ALL] == 1 then
    canSelectAll = true
  end
  local boxNumbers = 4
  if totalNumber <= 0 then
    boxNumbers = 0
  elseif totalNumber == 1 then
    boxNumbers = 1
  elseif totalNumber <= 3 then
    boxNumbers = 2
  end
  DebugOut("progress = ", progress)
  DebugOut("canSelectAll = ", canSelectAll)
  local noFriendsText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_MORE_FRIENDS_MORE_CREDITS")
  local noMoreFriendsText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_FREE_CREDITS_NEXT_TIME")
  flash_obj:InvokeASCallback("_root", "InitInviteFriendsList", #self.mInviteableFriendsRowList, excludeNum, totalNumber, progress, canSelectAll, boxNumbers, noFriendsText, noMoreFriendsText, FacebookPopUI.mInviteTitleText)
  FacebookPopUI:UpdateSelectButton()
end
function FacebookPopUI:NeedUpdateInviteableItem(rowIndex, mcIndex)
  local rowData = self.mInviteableFriendsRowList[rowIndex]
  if rowData then
    local flash_obj = self:GetFlashObject()
    if not flash_obj then
      return
    end
    for i, v in ipairs(rowData) do
      FacebookPopUI:GetFriendsHeadIcon(v.id, rowIndex, i, mcIndex, FacebookPopUI.DownloadHeadCallback)
      if v.icon then
        DebugOut("v.icon = ", v.icon)
        DebugOut("i = ", i)
        DebugOut("mcIndex = ", mcIndex)
        flash_obj:ReplaceTexture("fb160_avatar" .. (mcIndex - 1) * Friends_Per_Row + i .. ".png", v.icon)
      end
    end
    flash_obj:InvokeASCallback("_root", "SetInviteRowItem", rowIndex, rowData)
  end
end
function FacebookPopUI:SetInviteFriendIcon(rowIndex, subIndex, mcIndex)
  local flash_obj = self:GetFlashObject()
  if not flash_obj then
    return
  end
  local rowData = self.mInviteableFriendsRowList[rowIndex]
  if not rowData then
    return
  end
  local friendItem = rowData[subIndex]
  if not friendItem then
    return
  end
  if friendItem.icon then
    flash_obj:ReplaceTexture("fb160_avatar" .. (mcIndex - 1) * Friends_Per_Row + subIndex .. ".png", friendItem.icon)
  end
  flash_obj:InvokeASCallback("_root", "SetInviteFriendIcon", rowIndex, subIndex, friendItem)
end
function FacebookPopUI:GetFriendsHeadIcon(friendToken, rowIndex, subIndex, mcIndex, downloadCallback)
  local friendsIndex = (rowIndex - 1) * Friends_Per_Row + subIndex
  local fileName = "fb_" .. tostring(friendToken) .. ".png"
  local localPath = "data2/fb_" .. tostring(friendToken) .. ".png"
  local url = self.mInviteableWithFriendsExcluded[friendsIndex].picture.data.url
  DebugOut("GetFriendsHeadIcon = ", rowIndex, subIndex)
  if FacebookPopUI.mHeadMap and FacebookPopUI.mHeadMap[friendToken] then
    ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
    if downloadCallback then
      downloadCallback(rowIndex, subIndex, mcIndex, fileName)
    end
    return true
  end
  if ext.IsFileExist and ext.IsFileExist(localPath) then
    FacebookPopUI.mHeadMap = FacebookPopUI.mHeadMap or {}
    FacebookPopUI.mHeadMap[friendToken] = fileName
    ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
    if downloadCallback then
      downloadCallback(rowIndex, subIndex, mcIndex, fileName)
    end
    return true
  end
  ext.http.requestDownload({
    url,
    method = "GET",
    localpath = localPath,
    progressCallback = function(percent)
    end,
    callback = function(statusCode, filename, errstr)
      print("downloading filename ", filename)
      DebugOut("errstr = ", errstr)
      if 200 == statusCode and (nil == errstr or "" == errstr) then
        ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
        if not FacebookPopUI.mHeadMap then
          FacebookPopUI.mHeadMap = {}
        end
        FacebookPopUI.mHeadMap[friendToken] = fileName
        if downloadCallback then
          downloadCallback(rowIndex, subIndex, mcIndex, fileName)
        end
      else
        DebugOut("Download facebook head icon failed. ")
      end
    end
  })
end
function FacebookPopUI.DownloadHeadCallback(rowIndex, subIndex, mcIndex, filename)
  local friendItemRow = FacebookPopUI.mInviteableFriendsRowList[rowIndex]
  if not friendItemRow then
    return
  end
  local friendItem = friendItemRow[subIndex]
  if friendItem then
    friendItem.icon = filename
    local function timeCall()
      if FacebookPopUI.mInviteFriendViewPoped then
        return 15
      end
      FacebookPopUI:SetInviteFriendIcon(rowIndex, subIndex, mcIndex)
      return nil
    end
    if FacebookPopUI.mInviteFriendViewPoped then
      GameTimer:Add(timeCall, 15)
    else
      timeCall()
    end
  end
end
function FacebookPopUI:GenerateInviteTokenTableList()
  local inviteCount = 0
  local friendTokenTable = {}
  self.mInviteTokenTableList = {}
  self.mCurrentTokenTableIndex = 1
  local currentFriendsTable = self:GenerateExcludedInviteableFrineds()
  for i, v in ipairs(currentFriendsTable) do
    if v.chooseState == "choose" then
      table.insert(friendTokenTable, v.id)
      inviteCount = inviteCount + 1
      if inviteCount >= 50 then
        table.insert(self.mInviteTokenTableList, friendTokenTable)
        friendTokenTable = {}
        inviteCount = 0
      end
    end
  end
  if #friendTokenTable > 0 then
    table.insert(self.mInviteTokenTableList, friendTokenTable)
  end
  DebugOut("self.mInviteTokenTableList = ")
  DebugTable(self.mInviteTokenTableList)
end
function FacebookPopUI:DoInviteFrineds(tokenTableIndex)
  self.mCurrentTokenTable = self.mInviteTokenTableList[tokenTableIndex]
  DebugOut("self.mCurrentTokenTable = ")
  DebugTable(self.mCurrentTokenTable)
  if not self.mCurrentTokenTable then
    return
  end
  if #self.mCurrentTokenTable <= 0 then
    return
  end
  FacebookPopUI.mInviteFinishTime = nil
  local titleText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_APP_REQUESTS_INFO")
  local contentText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_SENT_CREDITS_INFO")
  Facebook:InviteFriends(titleText, contentText, self.mCurrentTokenTable, FacebookPopUI.InviteFriendsCallback)
end
function FacebookPopUI.InviteFriendsCallback(success)
  FacebookPopUI.mInviteFriendViewPoped = false
  DebugOut("FacebookPopUI.InviteFriendsCallback = ", success)
  if success then
    for i, v in ipairs(FacebookPopUI.mCurrentTokenTable) do
      FacebookPopUI.mExcludeInviteableFriends[v] = true
    end
    FacebookPopUI.mCurrentTokenTableIndex = FacebookPopUI.mCurrentTokenTableIndex + 1
    FacebookPopUI:PrepareInviteableFriendsUI(true)
    if FacebookPopUI:InvireProgressFinished() then
      FacebookPopUI:RecordInviteFinishTime()
    end
    FacebookPopUI:GetInviteRewards()
  else
  end
end
function FacebookPopUI:GetInviteRewards()
  DebugOut("self.mCurrentTokenTable count = ", #self.mCurrentTokenTable)
  local invitedNum = #self.mCurrentTokenTable
  local excludeNum = #self.mInviteableFriends - #self.mInviteableWithFriendsExcluded
  local totalNumber = #self.mInviteableFriends
  local curProgress = math.floor(excludeNum / totalNumber * 100)
  local prePercent = math.floor((excludeNum - invitedNum) / totalNumber * 100)
  if totalNumber == 0 then
    return
  end
  local friendsList = {}
  for i, v in ipairs(self.mInviteableFriends) do
    if FacebookPopUI.mExcludeInviteableFriends[v.id] then
      table.insert(friendsList, v.name or "NoName")
    end
  end
  DebugOut("friendsList = ")
  DebugTable(friendsList)
  curProgress = math.min(100, curProgress)
  curProgress = math.max(0, curProgress)
  prePercent = math.min(100, prePercent)
  prePercent = math.max(0, prePercent)
  local req = {
    number = invitedNum,
    total = totalNumber,
    pre_percent = prePercent,
    cur_percent = curProgress,
    friends = friendsList
  }
  DebugOut("get award req = ")
  DebugTable(req)
  NetMessageMgr:SendMsg(NetAPIList.get_invite_gift_req.Code, req, FacebookPopUI.GetInviteRewardsCallback, true, nil)
end
function FacebookPopUI.GetInviteRewardsCallback(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.get_invite_gift_req.Code then
    if content.code ~= 0 then
      local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgtype == NetAPIList.get_invite_gift_ack.Code then
    if FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_INVITE_SUCC] and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_INVITE_SUCC] == 0 then
      local nextTokenTable = FacebookPopUI.mInviteTokenTableList[FacebookPopUI.mCurrentTokenTableIndex]
      if nextTokenTable and #nextTokenTable > 0 then
        FacebookPopUI:DoInviteFrineds(FacebookPopUI.mCurrentTokenTableIndex)
        FacebookPopUI.mInviteFriendViewPoped = true
      end
    else
      FacebookPopUI.mCurInviteAwards = content.awards
      FacebookPopUI.mCurInvireCritical = content.strike
      FacebookPopUI.mCurInviteType = content.type
      FacebookPopUI.mCurInviteAnimIndex = 0
      FacebookPopUI:PlayCongratulationsAnim()
      FacebookPopUI:ShowFacebookMenu(FACEBOOKMENUTYPE.TYPE_MENU_CONGRATULATION)
    end
    return true
  end
  return false
end
function FacebookPopUI:PlayCongratulationsAnim()
  local flash_obj = self:GetFlashObject()
  if not flash_obj then
    return
  end
  local resource = GameGlobalData:GetData("resource")
  local orginalReses = {}
  local awardReses = {}
  if self.mCurInviteAwards then
    for i, v in ipairs(self.mCurInviteAwards) do
      local orginalRes = {}
      local awardRes = {}
      orginalRes.frameIcon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v, nil, nil)
      awardRes.frameIcon = orginalRes.frameIcon
      awardRes.count = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
      if resource[v.item_type] then
        DebugOut("resource[v.item_type] = ", v.item_type, resource[v.item_type])
        orginalRes.count = resource[v.item_type] - GameHelper:GetAwardCount(v.item_type, v.number, v.no)
        orginalRes.count = math.max(0, orginalRes.count)
      end
      orginalRes.count = GameUtils.numberConversion3(orginalRes.count)
      table.insert(orginalReses, orginalRes)
      table.insert(awardReses, awardRes)
    end
  end
  DebugOut("orginalReses = ")
  DebugTable(orginalReses)
  local sex = GameGlobalData:GetUserInfo().sex
  local player_main_fleet = GameGlobalData:GetFleetInfo(1)
  local avatarFrame = GameUtils:GetPlayerAvatar(nil, player_main_fleet.level)
  local playerName = GameUtils:GetUserDisplayName(GameGlobalData:GetUserInfo().name)
  flash_obj:InvokeASCallback("_root", "PlayCongratulationsAnim", orginalReses, awardReses, avatarFrame, playerName, self.mCurInviteType, self.mCurInvireCritical)
end
function FacebookPopUI:GenerateExcludedInviteableFrineds()
  local friendsList = {}
  for i, v in ipairs(self.mInviteableFriends) do
    if not FacebookPopUI.mExcludeInviteableFriends[v.id] then
      table.insert(friendsList, v)
    end
  end
  self.mInviteableWithFriendsExcluded = friendsList
  return friendsList
end
function FacebookPopUI:SetChooseState(rowIndex, subIndex, mcIndex)
  if rowIndex and subIndex then
    local rowData = self.mInviteableFriendsRowList[rowIndex]
    if rowData[subIndex].chooseState == "choose" then
      rowData[subIndex].chooseState = "unchoose"
      DebugOut("rowData[subIndex].chooseState = ", rowData[subIndex].chooseState)
    else
      rowData[subIndex].chooseState = "choose"
      DebugOut("rowData[subIndex].chooseState = ", rowData[subIndex].chooseState)
    end
    self:NeedUpdateInviteableItem(rowIndex, mcIndex)
    FacebookPopUI:UpdateSelectButton()
  end
end
function FacebookPopUI:Update(dt)
  if self.mTipsShowTimer then
    self.mTipsShowTimer = self.mTipsShowTimer + dt
    DebugOut("mTipsShowTimer = ", self.mTipsShowTimer)
    if self.mTipsShowTimer >= self.TIPS_SHOW_TIME then
      self.mTipsShowTimer = nil
      DebugOut("should move out!!!")
      self:MoveOutWithType(FACEBOOKMENUTYPE.TYPE_MENU_BIND_TIPS)
    end
  end
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "OnUpdate")
    self:GetFlashObject():Update(dt)
  end
end
if AutoUpdate.isAndroidDevice then
  function FacebookPopUI.OnAndroidBack()
    if FacebookPopUI.mCurrentMenuType == FACEBOOKMENUTYPE.TYPE_MENU_LOGIN then
      FacebookPopUI:OnFSCommand("FBLoginCloseManully")
    elseif FacebookPopUI.mCurrentMenuType == FACEBOOKMENUTYPE.TYPE_MENU_INVITE then
      FacebookPopUI:OnFSCommand("FBInviteFriendsCloseManully")
    elseif FacebookPopUI.mCurrentMenuType == FACEBOOKMENUTYPE.TYPE_MENU_CONGRATULATION then
      FacebookPopUI:OnFSCommand("FBCongratulationsCloseManully")
    end
  end
end
