local GameUIFpsFte = LuaObjectManager:GetLuaObject("GameUIFpsFte")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameObjectTutorialCutscene = LuaObjectManager:GetLuaObject("GameObjectTutorialCutscene")
local GameStateFpsFte = GameStateManager.GameStateFpsFte
local g_objType_stone = 1
local g_objType_coin = 2
local g_objType_count = 2
local function _randomInit(posData, index, scale, curhp)
  local temp = GameUIFpsFte:TableDeepCopy(posData)
  local direction = {
    temp.epos[1] - temp.bpos[1],
    temp.epos[2] - temp.bpos[2],
    temp.epos[3] - temp.bpos[3]
  }
  local len = math.sqrt(direction[1] * direction[1] + direction[2] * direction[2] + direction[3] * direction[3])
  direction[1] = direction[1] / len * temp.speed
  direction[2] = direction[2] / len * temp.speed
  direction[3] = direction[3] / len * temp.speed
  temp.direction = direction
  local data = {
    stoneData = temp,
    index = index,
    scale = scale,
    status = 1,
    curhp = curhp
  }
  DebugOut("create newstone ", GameStateManager.m_timer_ms, index)
  return data
end
local function newOneStoneFactory()
  local factory = {
    config = {},
    nid = 0,
    create_idx = 0,
    createInfo = {}
  }
  function factory:Init_random()
    local info = self.config.info
    local data = self.config.data
    self.createInfo = {}
    for k = info.start_time, info.end_time, info.interval_time do
      local allcnt = #data
      local allused = {}
      for i = 1, allcnt do
        allused[i] = false
      end
      for i = 1, info.random_num do
        local range = allcnt - i + 1
        local idx
        local idxcnt = math.random(1, range)
        for j = 1, allcnt do
          if allused[j] == false then
            idxcnt = idxcnt - 1
          end
          if idxcnt == 0 then
            idx = j
            break
          end
        end
        assert(idx, string.format("nid %d idxcnt %d allused %s", self.nid, idxcnt, LuaUtils:serializeTable(allused)))
        local idx2 = math.random(1, #data[idx].data)
        local one = {
          t = k,
          stoneinfo = data[idx].data[idx2],
          id1 = self.nid,
          id2 = idx,
          id3 = idx2,
          desc = ""
        }
        one.desc = string.format("random %d %d %d, time: %d", one.id1, one.id2, one.id3, one.t)
        table.insert(self.createInfo, one)
      end
    end
  end
  function factory:Init_notrandom()
    local info = self.config.info
    local data = self.config.data
    self.createInfo = {}
    for idx = 1, #data do
      local dataidx = data[idx].data
      local infoidx = data[idx].info
      if infoidx.isloop then
        for k = 1, infoidx.loop_num do
          local t = info.start_time + infoidx.timestart_rel + infoidx.interval_time * (k - 1)
          if t <= info.end_time then
            local idx2 = math.random(1, #dataidx)
            local one = {
              t = t,
              stoneinfo = dataidx[idx2],
              id1 = self.nid,
              id2 = idx,
              id3 = idx2,
              desc = ""
            }
            one.desc = string.format("norandom %d %d %d, time: %d", one.id1, one.id2, one.id3, one.t)
            table.insert(self.createInfo, one)
          end
        end
      else
        local t = info.start_time + infoidx.timestart_rel
        if t <= info.end_time then
          local idx2 = math.random(1, #dataidx)
          local one = {
            t = t,
            stoneinfo = dataidx[idx2],
            id1 = self.nid,
            id2 = idx,
            id3 = idx2,
            desc = ""
          }
          one.desc = string.format("norandom %d %d %d, time: %d", one.id1, one.id2, one.id3, one.t)
          table.insert(self.createInfo, one)
        end
      end
    end
    LuaUtils:array_sort(self.createInfo, 1, #self.createInfo, function(a, b)
      return a.t < b.t
    end)
  end
  function factory:Init(tconfig, nid)
    self.config = tconfig
    self.nid = nid
    self.create_idx = 0
    if self.config.info.is_random then
      factory:Init_random()
    else
      factory:Init_notrandom()
    end
  end
  function factory:Update(lasttime, allObjInfo, idx)
    if #self.createInfo == 0 then
      return 0
    end
    local create_cnt = 0
    for k = self.create_idx + 1, #self.createInfo do
      local cinfo = self.createInfo[k]
      if lasttime >= cinfo.t then
        local stoneData = self.createInfo[k].stoneinfo
        assert(1 < stoneData.bpos[3], "pos require")
        local firstScale = 50 / (stoneData.bpos[3] - 1)
        local param = {}
        local data = _randomInit(stoneData, idx, firstScale, stoneData.curhp)
        local curPlace = GameUIFpsFte.Camera.WorldToScreenPoint(data.stoneData.bpos)
        local stonetype = math.random(1, 3)
        param.pos = curPlace
        param.scale = firstScale
        param.index_s = data.index
        param.stonetype = stonetype
        param.ntype = stoneData.type
        local depth = GameUIFpsFte:GetFlashObject():InvokeASCallback("_root", "createNewObj", param)
        data.stoneData.depth = tonumber(depth)
        allObjInfo[stoneData.type].curObjList[data.index] = data
        self.create_idx = k
        idx = data.index + 1
        create_cnt = create_cnt + 1
      else
        break
      end
    end
    return create_cnt
  end
  return factory
end
local StoneFactory = {
  allFactory = {},
  idx = 1,
  lasttime = 0
}
function StoneFactory.Init(tconfig)
  StoneFactory.allFactory = {}
  for k, v in ipairs(tconfig) do
    local f = newOneStoneFactory()
    f:Init(v, k)
    table.insert(StoneFactory.allFactory, f)
  end
  StoneFactory.lasttime = 0
  StoneFactory.idx = 1
end
function StoneFactory.Update(dt, allObjInfo)
  StoneFactory.lasttime = StoneFactory.lasttime + dt
  for k, v in ipairs(StoneFactory.allFactory) do
    local n = v:Update(StoneFactory.lasttime, allObjInfo, StoneFactory.idx)
    StoneFactory.idx = StoneFactory.idx + n
  end
end
function StoneFactory.OnResume()
  local diff = os.time() - GameGlobalData.onPauseTime
  StoneFactory.lasttime = StoneFactory.lasttime + diff
end
GameUIFpsFte.TotalHp = 0
GameUIFpsFte.CurHp = 0
GameUIFpsFte.countTime = 0
GameUIFpsFte.curIndex = 1
GameUIFpsFte.xRate = 0
GameUIFpsFte.yRate = 0
GameUIFpsFte.skillcd = 0
GameUIFpsFte.ScreenParam = {}
GameUIFpsFte.ScopeInfo = {}
GameUIFpsFte.tutoStep = 0
GameUIFpsFte.tutoStep_init = 0
GameUIFpsFte.tutoStep_move = 1
GameUIFpsFte.tutoStep_afterMove = 2
GameUIFpsFte.tutoStep_shoot = 3
GameUIFpsFte.tutoStep_afterShoot = 4
GameUIFpsFte.tutoStep_skill = 5
GameUIFpsFte.tutoStep_afterSkill = 6
GameUIFpsFte.tutoStep_end = 7
GameUIFpsFte.dtLast = 0
GameUIFpsFte.Camera = require("Camera.tfl")
GameUIFpsFte.lookat = {theta = 0, phi = 0}
GameUIFpsFte.isShooting = false
GameUIFpsFte.startShootTime = 0
GameUIFpsFte.rect_commonShoot = {}
GameUIFpsFte.rect_skillShoot = {}
GameUIFpsFte.debug_timeStop = false
GameUIFpsFte.allMoveX = 0
GameUIFpsFte.allMoveY = 0
local matrix = require("Matrix.tfl")
function GameUIFpsFte:OnTouchMove(dt)
  if self.xRate == 0 and self.yRate == 0 then
    return
  end
  if not self.ScreenParam then
    return
  end
  local flash_obj = self:GetFlashObject()
  local fingerparam = GameUIFpsFte.config.fingerparam
  local deltax = self.xRate * fingerparam
  local deltay = self.yRate * fingerparam
  local diffx = deltax
  local diffy = deltay
  local config = GameUIFpsFte.config
  if diffx ~= 0 or diffy ~= 0 then
    GameUIFpsFte.lookat.theta = GameUIFpsFte.lookat.theta + diffx / self.ScreenParam[1]
    GameUIFpsFte.lookat.phi = GameUIFpsFte.lookat.phi + diffy / self.ScreenParam[2] * config.fingerScaleY
    if GameUIFpsFte.lookat.theta < config.angMin_x then
      GameUIFpsFte.lookat.theta = config.angMin_x
    end
    if GameUIFpsFte.lookat.theta > config.angMax_x then
      GameUIFpsFte.lookat.theta = config.angMax_x
    end
    if GameUIFpsFte.lookat.phi < config.angMin_y then
      GameUIFpsFte.lookat.phi = config.angMin_y
    end
    if GameUIFpsFte.lookat.phi > config.angMax_y then
      GameUIFpsFte.lookat.phi = config.angMax_y
    end
    local newLookat = {}
    newLookat[1] = math.cos(GameUIFpsFte.lookat.phi) * math.sin(GameUIFpsFte.lookat.theta)
    newLookat[3] = math.cos(GameUIFpsFte.lookat.phi) * math.cos(GameUIFpsFte.lookat.theta)
    newLookat[2] = math.sin(GameUIFpsFte.lookat.phi)
    local newPos = {
      [1] = self.Camera.eyepos[1][1],
      [2] = self.Camera.eyepos[2][1],
      [3] = 0
    }
    local bg_x = GameUIFpsFte.config.bg_h
    local before = GameUIFpsFte.bg_init_pos
    self.Camera.SetParam(newPos, newLookat, self.ScreenParam)
    local after = GameUIFpsFte.Camera.WorldToScreenPoint({
      0,
      0,
      bg_x
    })
    local xdisdiff = after[1] - before[1]
    local ydisdiff = after[2] - before[2]
    flash_obj:InvokeASCallback("_root", "UpdateBgPos", xdisdiff, ydisdiff)
  end
end
function GameUIFpsFte:UpdateObjects(dt, ntype)
  local objinfo = GameUIFpsFte.AllObjInfo[ntype]
  local flash_obj = self:GetFlashObject()
  if flash_obj == nil then
    return
  end
  local allCloseStones = {}
  local newObjList = {}
  for k, v in pairs(objinfo.curObjList) do
    if v.status == 1 then
      local last_pos = v.stoneData.bpos
      local direction = v.stoneData.direction
      local cur_pos = {
        last_pos[1] + direction[1] * dt / 1000,
        last_pos[2] + direction[2] * dt / 1000,
        last_pos[3] + direction[3] * dt / 1000
      }
      if math.abs(cur_pos[1] - self.Camera.eyepos[1][1]) <= 0.5 and math.abs(cur_pos[2] - self.Camera.eyepos[2][1]) <= 0.5 and 1 > math.abs(cur_pos[3]) then
        v.status = 0
        objinfo.fireObjList[v.index] = nil
        self.CurHp = self.CurHp - v.stoneData.damage
        flash_obj:InvokeASCallback("_root", "onHitted", self.CurHp, self.TotalHp)
        flash_obj:InvokeASCallback("_root", "RemoveObj", ntype, v.index)
        local GameObjectShakeScreen = LuaObjectManager:GetLuaObject("GameObjectShakeScreen")
        GameObjectShakeScreen:StartShaking(2)
        GameUtils:PlaySound("fps_hitship.mp3")
      elseif cur_pos[3] < 0 then
        v.status = 0
        flash_obj:InvokeASCallback("_root", "RemoveObj", ntype, v.index)
      else
        table.insert(allCloseStones, v)
        local curplace = GameUIFpsFte.Camera.WorldToScreenPoint(cur_pos)
        local curScale = self:CalculateScale(last_pos, cur_pos, v.scale)
        v.stoneData.bpos = cur_pos
        v.scale = curScale
        newObjList[v.index] = v
        if curplace[1] <= self.ScreenParam[1] and curplace[1] >= 0 and curplace[2] <= self.ScreenParam[2] and curplace[2] >= 0 then
          flash_obj:InvokeASCallback("_root", "StoneShow", ntype, v.index)
          flash_obj:InvokeASCallback("_root", "updateObjPosAndScale", ntype, v.index, curplace, curScale)
          if cur_pos[3] < GameUIFpsFte.config.canbehit_dist then
            if self:CheckCanBeHitted(ntype, curplace, curScale) then
              objinfo.fireObjList[v.index] = v
            else
              objinfo.fireObjList[v.index] = nil
            end
            flash_obj:InvokeASCallback("_root", "onObjCanBeHit", ntype, v.index)
          end
        else
          flash_obj:InvokeASCallback("_root", "ObjHide", ntype, v.index)
        end
      end
    end
  end
  LuaUtils:array_sort(allCloseStones, 1, #allCloseStones, function(a, b)
    return a.stoneData.bpos[3] < b.stoneData.bpos[3]
  end)
  do
    local tdepth = {}
    local tidx = {}
    for k, v in ipairs(allCloseStones) do
      table.insert(tidx, v.index)
      table.insert(tdepth, v.stoneData.depth)
    end
    LuaUtils:array_sort(tdepth, 1, #tdepth, function(a, b)
      return b < a
    end)
    flash_obj:InvokeASCallback("_root", "sortObjs", tidx, ntype, tdepth)
    for k, v in ipairs(allCloseStones) do
      v.stoneData.depth = tdepth[k]
    end
  end
  objinfo.curObjList = newObjList
end
function GameUIFpsFte:UpdateBullet(dt, ntype)
  local objinfo = GameUIFpsFte.AllObjInfo[ntype]
  if self.isFireSkill then
    for k, v in pairs(objinfo.curObjList) do
      if v.status == 1 and v.stoneData.bpos[3] < GameUIFpsFte.config.canbehit_dist then
        local stone = v
        stone.curhp = 0
        self:GetFlashObject():InvokeASCallback("_root", "onObjHited", ntype, stone.index, stone.curhp, stone.stoneData.curhp, stone.curhp)
        local ind = stone.index
        objinfo.fireObjList[ind] = nil
        objinfo.curObjList[ind] = nil
        GameUIFpsFte:OnObjDie(ntype, ind, v.stoneData.bpos[3])
      end
    end
    return
  elseif self.isShooting then
    if next(objinfo.fireObjList) == nil then
      return
    end
    local stonelist = {}
    for k, v in pairs(objinfo.fireObjList) do
      table.insert(stonelist, v)
    end
    LuaUtils:array_sort(stonelist, 1, #stonelist, function(a, b)
      return a.stoneData.depth > b.stoneData.depth
    end)
    stone = stonelist[1]
    stone.curhp = stone.curhp - GameUIFpsFte.config.atk_common
    self:GetFlashObject():InvokeASCallback("_root", "onObjHited", ntype, stone.index, stone.curhp, stone.stoneData.curhp, GameUIFpsFte.config.atk_common)
    if 0 >= stone.curhp then
      local ind = stone.index
      objinfo.fireObjList[ind] = nil
      objinfo.curObjList[ind] = nil
      GameUIFpsFte:OnObjDie(ntype, ind, stone.stoneData.bpos[3])
    end
  end
end
function GameUIFpsFte:updateSkill(dt)
  if self.skillcd > 0 then
    self.skillcd = self.skillcd - dt
    self:GetFlashObject():InvokeASCallback("_root", "SetShootCD", self.skillcd / 1000, GameUIFpsFte.config.skill_cd / 1000)
  end
  if self.skillcd <= 0 then
    local trets = GameUtils:GetFlashValues(GameUIFpsFte, "_root.skill_b.mc.cd", {
      "_currentframe"
    })
    if trets[1] ~= "1" then
      self:GetFlashObject():InvokeASCallback("_root", "SetShootCD", 0, GameUIFpsFte.config.skill_cd / 1000)
    end
    self.skillcd = 0
  end
end
function GameUIFpsFte:Update(dt)
  local flash_obj = self:GetFlashObject()
  if not flash_obj then
    return
  end
  flash_obj:Update(dt)
  if GameUIFpsFte.tutoStep == GameUIFpsFte.tutoStep_end then
    return
  end
  if GameUIFpsFte.tutoStep == GameUIFpsFte.tutoStep_init and self.countTime > GameUIFpsFte.config.tutoMove_time then
    GameUIFpsFte.tutoStep = GameUIFpsFte.tutoStep_move
    GameUIFpsFte:OnTutoPrepare(1)
  end
  if GameUIFpsFte.tutoStep == GameUIFpsFte.tutoStep_afterMove and self.countTime > GameUIFpsFte.config.tutoShoot_time then
    GameUIFpsFte.tutoStep = GameUIFpsFte.tutoStep_shoot
    GameUIFpsFte:OnTutoPrepare(2)
  end
  if GameUIFpsFte.tutoStep == GameUIFpsFte.tutoStep_afterShoot and self.countTime > GameUIFpsFte.config.tutoSkill_time then
    GameUIFpsFte.tutoStep = GameUIFpsFte.tutoStep_skill
    GameUIFpsFte:OnTutoPrepare(3)
  end
  if self.CurHp <= GameUIFpsFte.config.tutoEnd_HP then
    GameUIFpsFte.tutoStep = GameUIFpsFte.tutoStep_end
    GameUIFpsFte:OnTutoPrepare(4)
    return
  end
  local need_update_time = true
  local need_update_objs = true
  local need_update_other = true
  if GameUIFpsFte.tutoStep == GameUIFpsFte.tutoStep_move or GameUIFpsFte.tutoStep == GameUIFpsFte.tutoStep_skill then
    need_update_time = false
    need_update_objs = false
    need_update_other = false
  elseif GameUIFpsFte.tutoStep == GameUIFpsFte.tutoStep_shoot then
    need_update_time = false
    need_update_objs = true
    need_update_other = false
  end
  if need_update_time then
    flash_obj:InvokeASCallback("_root", "onUpdate", dt)
    self.countTime = self.countTime + dt
  end
  if need_update_objs then
    if self:GetFlashObject() == nil then
      return
    end
    local movedt = dt
    if GameUIFpsFte.tutoStep == GameUIFpsFte.tutoStep_shoot then
      movedt = 0
    end
    for i = 1, g_objType_count do
      self:UpdateObjects(movedt, i)
    end
    local showScope = false
    for i = 1, g_objType_count do
      if next(GameUIFpsFte.AllObjInfo[i].fireObjList) then
        showScope = true
        break
      end
    end
    if showScope then
      self:GetFlashObject():InvokeASCallback("_root", "SetScopeState", 2)
    else
      self:GetFlashObject():InvokeASCallback("_root", "SetScopeState", 1)
    end
  end
  if need_update_other then
    GameUIFpsFte:updateSkill(dt)
    StoneFactory.Update(dt, GameUIFpsFte.AllObjInfo)
    for i = 1, g_objType_count do
      self:UpdateBullet(dt, i)
    end
    local config = GameUIFpsFte.config
    if self.CurHp < config.hp_self * 0.6 and self.crack_idx == 0 then
      flash_obj:InvokeASCallback("_root", "SetVisible", "_root.crack.crack_1", true)
      self.crack_idx = 1
    elseif self.CurHp < config.hp_self * 0.5 and self.crack_idx == 1 then
      flash_obj:InvokeASCallback("_root", "SetVisible", "_root.crack.crack_2", true)
      self.crack_idx = 2
    elseif self.CurHp < config.hp_self * 0.4 and self.crack_idx == 2 then
      flash_obj:InvokeASCallback("_root", "SetVisible", "_root.crack.crack_3", true)
      self.crack_idx = 3
    elseif self.CurHp < config.hp_self * 0.3 and self.crack_idx == 3 then
      flash_obj:InvokeASCallback("_root", "SetVisible", "_root.crack.crack_4", true)
      self.crack_idx = 4
    elseif self.CurHp < config.hp_self * 0.2 and self.crack_idx == 4 then
      flash_obj:InvokeASCallback("_root", "SetVisible", "_root.crack.crack_5", true)
      self.crack_idx = 5
    elseif self.CurHp < config.hp_self * 0.1 and self.crack_idx == 5 then
      flash_obj:InvokeASCallback("_root", "SetVisible", "_root.crack.crack_6", true)
      self.crack_idx = 6
    end
    if self.CurHp <= 0 then
    end
  end
end
function GameUIFpsFte:CheckCanBeHitted(ntype, curPlace, scale)
  if not self.ScopeInfo then
    return false
  end
  local objinfo = GameUIFpsFte.AllObjInfo[ntype]
  local center_scope = {
    x = self.ScopeInfo.xpos + self.ScopeInfo.width * 0.5,
    y = self.ScopeInfo.ypos + self.ScopeInfo.height * 0.5
  }
  local center_stone = {
    x = curPlace[1],
    y = curPlace[2]
  }
  local len = self.ScopeInfo.width * 0.5 + objinfo.width * scale / 100 * 0.5 * 0.7
  local dist = math.sqrt((center_scope.x - center_stone.x) * (center_scope.x - center_stone.x) + (center_scope.y - center_stone.y) * (center_scope.y - center_stone.y))
  return len > dist
end
function GameUIFpsFte:CalculateScale(lastPos, curPos, lastScale)
  local curplace = GameUIFpsFte.Camera.WorldToScreenPoint(curPos)
  local anothercurplace = GameUIFpsFte.Camera.WorldToScreenPoint({
    curPos[1],
    curPos[2] + 1,
    curPos[3]
  })
  local lastplace = GameUIFpsFte.Camera.WorldToScreenPoint({
    lastPos[1],
    lastPos[2],
    lastPos[3]
  })
  local anotherlastplace = GameUIFpsFte.Camera.WorldToScreenPoint({
    lastPos[1],
    lastPos[2] + 1,
    lastPos[3]
  })
  local newscale = 50
  local curlen = math.sqrt((anothercurplace[1] - curplace[1]) * (anothercurplace[1] - curplace[1]) + (anothercurplace[2] - curplace[2]) * (anothercurplace[2] - curplace[2]))
  local lastlen = math.sqrt((anotherlastplace[1] - lastplace[1]) * (anotherlastplace[1] - lastplace[1]) + (anotherlastplace[2] - lastplace[2]) * (anotherlastplace[2] - lastplace[2]))
  newscale = curlen * lastScale / lastlen
  return newscale
end
function GameUIFpsFte:OnObjDie(ntype, idx, dist)
  if dist < GameUIFpsFte.config.sound_close_mid then
    GameUtils:PlaySound("fps_explo_close.mp3")
  elseif dist < GameUIFpsFte.config.sound_mid_far then
    GameUtils:PlaySound("fps_explo_mid.mp3")
  else
    GameUtils:PlaySound("fps_explo_far.mp3")
  end
  DebugOut("onobjdie", ntype, idx)
  if ntype == g_objType_stone then
    local objinfo = GameUIFpsFte.AllObjInfo[ntype]
    objinfo.killedObjCount = objinfo.killedObjCount + 1
    local t = {
      {
        path = "_root.integral.text_integral",
        txt = "" .. objinfo.killedObjCount
      }
    }
    self:GetFlashObject():InvokeASCallback("_root", "SetText", t)
  end
end
local function onResume()
  StoneFactory.OnResume()
  GameStateManager.GameStateFpsFte.bgTouchid = nil
  GameStateManager.GameStateFpsFte.commonShootTouchid = nil
  GameUIFpsFte:OnFSCommand("CommonFire", "end")
end
function GameUIFpsFte:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  GameStateManager:RegisterGameResumeNotification(onResume, nil)
  self:Init()
  GameUtils:PlayMusic("sound/GE2_Battle.mp3")
end
function GameUIFpsFte:OnEraseFromGameState()
  self:UnloadFlashObject()
  self.TotalHp = nil
  self.CurHp = nil
  self.xRate = 0
  self.yRate = 0
  self.isStopMove = false
  self.ScreenParam = nil
  self.ScopeInfo = nil
  self.skillcd = 0
  GameStateManager:RemoveGameResumeNotification(onResume)
end
function GameUIFpsFte:Show()
end
function GameUIFpsFte:TableDeepCopy(src)
  local res = {}
  for k, v in pairs(src) do
    if type(v) == "table" then
      res[k] = self:TableDeepCopy(v)
    else
      res[k] = v
    end
  end
  return res
end
function GameUIFpsFte:Init(allData)
  DebugOut("Init")
  GameUIFpsFte.AllObjInfo = {}
  GameUIFpsFte.TotalHp = GameUIFpsFte.config.hp_self
  GameUIFpsFte.CurHp = GameUIFpsFte.config.hp_self
  GameUIFpsFte.countTime = 0
  GameUIFpsFte.skillcd = 0
  GameUIFpsFte.ScreenParam = {}
  GameUIFpsFte.ScopeInfo = {}
  GameUIFpsFte.isFireSkill = false
  GameUIFpsFte.isShooting = false
  GameUIFpsFte.canshoot = true
  GameUIFpsFte.lookat = {theta = 0, phi = 0}
  GameUIFpsFte.crack_idx = 0
  local flash_obj = self:GetFlashObject()
  for i = 1, g_objType_count do
    GameUIFpsFte.AllObjInfo[i] = {
      curObjList = {},
      killedObjCount = 0,
      fireObjList = {},
      width = 0,
      height = 0
    }
  end
  for k, v in pairs(GameUIFpsFte.AllObjInfo) do
    local objinfo = GameUIFpsFte.AllObjInfo[k]
    local strinfo = flash_obj:InvokeASCallback("_root", "getDefaultObjInfo", k)
    local stoneInfo = LuaUtils:string_split(strinfo, ",")
    flash_obj:InvokeASCallback("_root", "delDefaultObj", k)
    objinfo.width = tonumber(stoneInfo[1])
    objinfo.height = tonumber(stoneInfo[2])
    assert(objinfo.width, string.format("objinfo %d %s end", k, strinfo))
    assert(objinfo.height, "objinfo " .. k)
  end
  local param = flash_obj:InvokeASCallback("_root", "getScreenWdAndHei")
  flash_obj:InvokeASCallback("_root", "SetScopeState", 1)
  local param1 = LuaUtils:string_split(param, ",")
  local postionAndAttr = LuaUtils:string_split(flash_obj:InvokeASCallback("_root", "getScopeInfo"), ",")
  flash_obj:InvokeASCallback("_root", "delDefaultStone")
  GameUIFpsFte.ScopeInfo = {
    xpos = tonumber(postionAndAttr[1]),
    ypos = tonumber(postionAndAttr[2]),
    width = tonumber(postionAndAttr[3]),
    height = tonumber(postionAndAttr[4])
  }
  local screenparam = {}
  for k, v in ipairs(param1) do
    table.insert(screenparam, tonumber(v))
  end
  GameUIFpsFte.ScreenParam = screenparam
  GameUIFpsFte.Camera.SetParam({
    0,
    0,
    0
  }, {
    0,
    0,
    1
  }, screenparam)
  local bg_x = GameUIFpsFte.config.bg_h
  GameUIFpsFte.bg_init_pos = GameUIFpsFte.Camera.WorldToScreenPoint({
    0,
    0,
    bg_x
  })
  local t = {
    {
      path = "_root.integral.text_integral",
      txt = "0"
    }
  }
  self:GetFlashObject():InvokeASCallback("_root", "SetText", t)
  do
    local function _getrect(trect, strname)
      local trets = GameUtils:GetFlashValues(GameUIFpsFte, strname, {
        "globalX",
        "globalY",
        "_width",
        "_height",
        "_xscale",
        "_yscale"
      })
      local flash_wh = GameUIFpsFte.ScreenParam
      if not tonumber(trets[1]) then
        print("bad rect", strname)
      end
      local xlen = tonumber(trets[3]) * tonumber(trets[5]) / 100
      local lt_x = tonumber(trets[1])
      local lt_y = tonumber(trets[2])
      if ext.is16x9 then
        xlen = xlen * 1.4
      end
      local rb_x = lt_x + xlen
      local rb_y = lt_y + xlen
      trect.lt_x, trect.lt_y = GameUtils:ConvertFlashToScreen(lt_x, lt_y, flash_wh[1], flash_wh[2])
      trect.rb_x, trect.rb_y = GameUtils:ConvertFlashToScreen(rb_x, rb_y, flash_wh[1], flash_wh[2])
    end
    _getrect(GameUIFpsFte.rect_commonShoot, "_root.shoot_btn.hit")
    _getrect(GameUIFpsFte.rect_skillShoot, "_root.skill_b.hit")
  end
  if GameUIFpsFte.config.istuto then
    GameUIFpsFte.tutoStep = GameUIFpsFte.tutoStep_init
    flash_obj:InvokeASCallback("_root", "SetScopeState", 0, 1)
    flash_obj:InvokeASCallback("_root", "SetShootStatus", false, 1)
    flash_obj:InvokeASCallback("_root", "SetSkillBtnStatus", 1)
    GameUIFpsFte.canshoot = false
  else
    GameUIFpsFte.tutoStep = -1
  end
  local timeout = GameUIFpsFte.config.time_sec
  self:GetFlashObject():InvokeASCallback("_root", "setTimer", timeout)
  StoneFactory.Init(GameUIFpsFte.config.stones, GameUIFpsFte.config.isLoop)
end
function GameUIFpsFte:HitTest(xpos, ypos)
  local trect
  trect = GameUIFpsFte.rect_commonShoot
  if (trect.lt_x - xpos) * (trect.rb_x - xpos) < 0 and 0 > (trect.lt_y - ypos) * (trect.rb_y - ypos) then
    return "commonShoot"
  end
  trect = GameUIFpsFte.rect_skillShoot
  if (trect.lt_x - xpos) * (trect.rb_x - xpos) < 0 and 0 > (trect.lt_y - ypos) * (trect.rb_y - ypos) then
    return "skillShoot"
  end
  if ypos < ext.SCREEN_HEIGHT * 0.9 and ypos > ext.SCREEN_HEIGHT * 0.1 then
    return "bg"
  end
  return "nil"
end
function GameUIFpsFte:StartMoveBg(xdiff, ydiff)
  self.allMoveX = self.allMoveX + xdiff
  self.allMoveY = self.allMoveY + ydiff
  self.xRate = xdiff
  self.yRate = ydiff
  GameUIFpsFte:OnTouchMove()
  if GameUIFpsFte.tutoStep == GameUIFpsFte.tutoStep_move and math.abs(self.allMoveX) + math.abs(self.allMoveY) > GameUIFpsFte.config.tutoMove_len then
    GameUIFpsFte.tutoStep = GameUIFpsFte.tutoStep_afterMove
    local flash_obj = self:GetFlashObject()
    flash_obj:InvokeASCallback("_root", "SetScopeState", 0, 0)
    flash_obj:InvokeASCallback("_root", "SetVisible", "_root.npc_tip_03", false)
    flash_obj:InvokeASCallback("_root", "SetVisible", "_root.tutotxt.txt1", false)
    flash_obj:InvokeASCallback("_root", "HideTalk")
  end
end
function GameUIFpsFte:StopMoveBg()
end
function GameUIFpsFte:StartPlayFireSound()
  local t = 760
  local function doplay()
    if not GameUIFpsFte.isShooting then
      return
    end
    if GameUIFpsFte.loopSoundID then
      GameUtils:StopSound(GameUIFpsFte.loopSoundID)
    end
    GameUIFpsFte.loopSoundID = GameUtils:PlaySound("fps_laser_loop.mp3")
    return t
  end
  doplay()
  GameTimer:Add(doplay, t)
end
function GameUIFpsFte:OnFinishFps()
  local GameStateFpsFte = GameStateManager.GameStateFpsFte
  if GameStateFpsFte.m_previousState == GameStateManager.GameStateMainPlanet then
    local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
    GameUICommonDialog:JudeIsLoadFlash()
    local dialogCallback = function()
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
    end
    GameUICommonDialog:PlayStory({1100066, 1100067}, dialogCallback)
  else
    TutorialQuestManager.QuestTutorialCityHall:SetActive(true)
    AddFlurryEvent("FinishBattle_60001013", {1}, 2)
    local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
    GameUICommonDialog:JudeIsLoadFlash()
    local function dialogCallback()
      AddFlurryEvent("ShamBattle_Dilaog_2", {state = 2}, 1)
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateEmptyState)
      GameObjectTutorialCutscene:ShowTutorial("1")
    end
    AddFlurryEvent("ShamBattle_Dilaog_2", {state = 1}, 1)
    GameUICommonDialog:PlayStory({1100067}, dialogCallback)
  end
  GameGlobalData.needFPSFte = nil
end
function GameUIFpsFte:OnTutoPrepare(ntype)
  local flash_obj = self:GetFlashObject()
  if ntype == 1 then
    local p = {}
    p.npcname = "male"
    p.strContent = GameUtils:TryGetText("LC_MENU_FPS_DIALOG_1", "tuto1")
    p.strPerson = ""
    flash_obj:InvokeASCallback("_root", "ShowTalk", p)
    flash_obj:InvokeASCallback("_root", "SetVisible", "_root.npc_tip_03", true)
    flash_obj:InvokeASCallback("_root", "SetVisible", "_root.tutotxt.txt1", true)
  elseif ntype == 2 then
    local p = {}
    p.npcname = "female"
    p.strContent = GameUtils:TryGetText("LC_MENU_FPS_DIALOG_2", "tuto2")
    p.strPerson = ""
    flash_obj:InvokeASCallback("_root", "ShowTalk", p)
    flash_obj:InvokeASCallback("_root", "SetShootStatus", false, 0)
    GameUIFpsFte.canshoot = true
    flash_obj:InvokeASCallback("_root", "SetVisible", "_root.npc_tip_02", true)
    flash_obj:InvokeASCallback("_root", "SetVisible", "_root.tutotxt.txt2", true)
  elseif ntype == 3 then
    local p = {}
    p.npcname = "male"
    p.strContent = GameUtils:TryGetText("LC_MENU_FPS_DIALOG_3", "tuto3")
    p.strPerson = ""
    flash_obj:InvokeASCallback("_root", "ShowTalk", p)
    flash_obj:InvokeASCallback("_root", "SetSkillBtnStatus", 0)
    flash_obj:InvokeASCallback("_root", "SetVisible", "_root.npc_tip_04", true)
    flash_obj:InvokeASCallback("_root", "SetVisible", "_root.tutotxt.txt3", true)
    if self.isShooting then
      GameStateFpsFte.commonShootTouchid = nil
      GameUIFpsFte:OnFSCommand("CommonFire", "end")
    end
  elseif ntype == 4 then
    if self.isShooting then
      GameStateFpsFte.commonShootTouchid = nil
      GameUIFpsFte:OnFSCommand("CommonFire", "end")
    end
    flash_obj:InvokeASCallback("_root", "setSelfHpPercent", 1)
    GameUIFpsFte:PlayEndAnim()
  end
end
function GameUIFpsFte:PlayEndAnim()
  local flash_obj = self:GetFlashObject()
  local p = {}
  p.npcname = "male"
  p.strContent = GameUtils:TryGetText("LC_MENU_FPS_DIALOG_4", "tuto4")
  p.strPerson = ""
  flash_obj:InvokeASCallback("_root", "ShowTalk", p)
  GameTimer:Add(function()
    flash_obj:InvokeASCallback("_root", "HideTalk")
    flash_obj:InvokeASCallback("_root", "ShowMaskAnim")
    GameTimer:Add(function()
      GameUIFpsFte:OnTimeup()
    end, 3800)
  end, 2000)
end
function GameUIFpsFte:OnTutoStart(ntype)
end
function GameUIFpsFte:OnTutoStop(ntype)
end
function GameUIFpsFte:OnTimeup()
  if GameUIFpsFte.config.istuto then
    GameUIFpsFte.isShooting = false
    GameStateManager:GetCurrentGameState():EraseObject(GameUIFpsFte)
    GameUIFpsFte:OnFinishFps()
  else
    local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
    GameUIBattleResult:LoadFlashObject()
    GameStateManager:GetCurrentGameState():AddObject(GameUIBattleResult)
    GameStateManager:GetCurrentGameState():EraseObject(GameUIFpsFte)
    function GameUIBattleResult.closeCallBack()
      GameUIFpsFte:OnFinishFps()
    end
    local tparam = {
      allrewards = {}
    }
    local resource = GameGlobalData:GetData("resource")
    local awards = {
      {
        item_type = "credit",
        number = resource.credit,
        no = 0,
        level = 0
      },
      {
        item_type = "money",
        number = resource.money,
        no = 0,
        level = 0
      }
    }
    for k, v in pairs(awards) do
      local t = {}
      local info = GameHelper:GetItemInfo(v)
      t.strname = info.name
      t.strframe = info.icon_frame
      t.id = LuaUtils:serializeTable(v)
      if not GameHelper:IsResource(v.item_type) then
        t.strpic = info.icon_pic
      end
      t.quality = info.quality
      t.isext = info.isExt
      t.cnt = info.cnt
      table.insert(tparam.allrewards, t)
    end
    GameUIBattleResult:ShowFPSWin(tparam)
    GameUIFpsFte.isShooting = false
  end
end
function GameUIFpsFte:OnFSCommand(cmd, arg)
  if not self:GetFlashObject() then
    return
  end
  if cmd == "timeup" then
    GameUIFpsFte:OnTimeup()
  elseif cmd == "onObjDie" then
    local param = LuaUtils:string_split(arg, ",")
    local ntype = tonumber(param[1])
    local nid = tonumber(param[2])
  elseif cmd == "CommonFire" then
    if arg == "start" then
      if GameUIFpsFte.tutoStep == GameUIFpsFte.tutoStep_shoot then
        self:GetFlashObject():InvokeASCallback("_root", "SetVisible", "_root.npc_tip_02", false)
        self:GetFlashObject():InvokeASCallback("_root", "HideTalk")
        self:GetFlashObject():InvokeASCallback("_root", "SetVisible", "_root.tutotxt.txt2", false)
        GameUIFpsFte.tutoStep = GameUIFpsFte.tutoStep_afterShoot
      end
      self.isShooting = true
      self.startShootTime = GameStateManager.m_timer_ms
      self:GetFlashObject():InvokeASCallback("_root", "SetVisible", "_root.cabin.mc.shoot.aim", true)
      self:GetFlashObject():InvokeASCallback("_root", "SetShootStatus", true)
      self:StartPlayFireSound()
    elseif arg == "end" then
      self.isShooting = false
      self:GetFlashObject():InvokeASCallback("_root", "SetVisible", "_root.cabin.mc.shoot.aim", false)
      self:GetFlashObject():InvokeASCallback("_root", "SetShootStatus", false)
      if GameUIFpsFte.loopSoundID ~= nil then
        GameUtils:StopSound(GameUIFpsFte.loopSoundID)
      end
    else
      assert(false)
    end
  elseif cmd == "onSkillOver" then
    self.isFireSkill = false
  elseif cmd == "SkillFire" then
    if GameUIFpsFte.tutoStep == GameUIFpsFte.tutoStep_skill then
      self:GetFlashObject():InvokeASCallback("_root", "HideTalk")
      self:GetFlashObject():InvokeASCallback("_root", "SetVisible", "_root.npc_tip_04", false)
      self:GetFlashObject():InvokeASCallback("_root", "SetVisible", "_root.tutotxt.txt3", false)
      GameUIFpsFte.tutoStep = GameUIFpsFte.tutoStep_afterSkill
    end
    if self.skillcd <= 0 then
      self.skillcd = GameUIFpsFte.config.skill_cd
      self.isFireSkill = true
      self:GetFlashObject():InvokeASCallback("_root", "playSkill")
      GameUIFpsFte:updateSkill(0)
      GameUtils:PlaySound("fps_laser_big.mp3")
    else
      local GameTip = LuaObjectManager:GetLuaObject("GameTip")
      GameTip:Show(GameUtils:TryGetText("LC_MENU_FPS_TIPS_ULTIMATE_CHARGING"), 1000, nil, nil, true)
    end
  elseif cmd == "FinishTutor" then
    local timeout = GameUIFpsFte.config.time_sec
    self:GetFlashObject():InvokeASCallback("_root", "setTimer", timeout)
    StoneFactory.Init(GameUIFpsFte.config.stones, GameUIFpsFte.config.isLoop)
  end
end
