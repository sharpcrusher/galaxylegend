AchievementPlatformMap = {
  [1] = {
    PlatformExtCommon = "achievement_lawsry_guardian",
    GooglePlayGame = "CgkIofXkg7cbEAIQAQ"
  },
  [2] = {
    PlatformExtCommon = "achievement_imperial_sorranto",
    GooglePlayGame = "CgkIofXkg7cbEAIQAg"
  },
  [3] = {
    PlatformExtCommon = "achievement_kalintor_savior",
    GooglePlayGame = "CgkIofXkg7cbEAIQAw"
  },
  [4] = {
    PlatformExtCommon = "achievement_megaman",
    GooglePlayGame = "CgkIofXkg7cbEAIQBA"
  },
  [5] = {
    PlatformExtCommon = "achievement_evergreen_land",
    GooglePlayGame = "CgkIofXkg7cbEAIQBQ"
  },
  [6] = {
    PlatformExtCommon = "achievement_spiral_zone",
    GooglePlayGame = "CgkIofXkg7cbEAIQBg"
  },
  [7] = {
    PlatformExtCommon = "achievement_farseer",
    GooglePlayGame = "CgkIofXkg7cbEAIQBw"
  },
  [8] = {
    PlatformExtCommon = "achievement_luckyfelix",
    GooglePlayGame = "CgkIofXkg7cbEAIQCA"
  },
  [9] = {
    PlatformExtCommon = "achievement_first_blood",
    GooglePlayGame = "CgkIofXkg7cbEAIQCQ"
  },
  [10] = {
    PlatformExtCommon = "achievement_double_kill",
    GooglePlayGame = "CgkIofXkg7cbEAIQCg"
  },
  [11] = {
    PlatformExtCommon = "achievement_triple_kill",
    GooglePlayGame = "CgkIofXkg7cbEAIQCw"
  },
  [12] = {
    PlatformExtCommon = "achievement_monster_kill",
    GooglePlayGame = "CgkIofXkg7cbEAIQDA"
  },
  [13] = {
    PlatformExtCommon = "achievement_unstoppable",
    GooglePlayGame = "CgkIofXkg7cbEAIQDQ"
  },
  [14] = {
    PlatformExtCommon = "achievement_dominate",
    GooglePlayGame = "CgkIofXkg7cbEAIQDg"
  },
  [15] = {
    PlatformExtCommon = "achievement_godlike",
    GooglePlayGame = "CgkIofXkg7cbEAIQDw"
  },
  [16] = {
    PlatformExtCommon = "achievement_all_about_the_credits",
    GooglePlayGame = "CgkIofXkg7cbEAIQEA"
  },
  [17] = {
    PlatformExtCommon = "achievement_credits_as_a_primary_force",
    GooglePlayGame = "CgkIofXkg7cbEAIQEQ"
  },
  [18] = {
    PlatformExtCommon = "achievement_untouchable",
    GooglePlayGame = "CgkIofXkg7cbEAIQEg"
  },
  [19] = {
    PlatformExtCommon = "achievement_rookie",
    GooglePlayGame = "CgkIofXkg7cbEAIQEw"
  },
  [20] = {
    PlatformExtCommon = "achievement_graduate",
    GooglePlayGame = "CgkIofXkg7cbEAIQFA"
  },
  [21] = {
    PlatformExtCommon = "achievement_ascending",
    GooglePlayGame = "CgkIofXkg7cbEAIQFQ"
  },
  [22] = {
    PlatformExtCommon = "achievement_commander",
    GooglePlayGame = "CgkIofXkg7cbEAIQFg"
  },
  [23] = {
    PlatformExtCommon = "achievement_for_the_empire",
    GooglePlayGame = "CgkIofXkg7cbEAIQFw"
  },
  [24] = {
    PlatformExtCommon = "achievement_iron_fisted",
    GooglePlayGame = "CgkIofXkg7cbEAIQGA"
  },
  [25] = {
    PlatformExtCommon = "achievement_dominator",
    GooglePlayGame = "CgkIofXkg7cbEAIQGQ"
  },
  [26] = {
    PlatformExtCommon = "achievement_lord_of_the_galactonite",
    GooglePlayGame = "CgkIofXkg7cbEAIQGg"
  },
  [27] = {
    PlatformExtCommon = "achievement_friendship",
    GooglePlayGame = "CgkIofXkg7cbEAIQGw"
  },
  [28] = {
    PlatformExtCommon = "achievement_planet_will",
    GooglePlayGame = "CgkIofXkg7cbEAIQHw"
  },
  [29] = {
    PlatformExtCommon = "achievement_planet_owner",
    GooglePlayGame = "CgkIofXkg7cbEAIQIA"
  },
  [30] = {
    PlatformExtCommon = "achievement_shining",
    GooglePlayGame = "CgkIofXkg7cbEAIQIQ"
  },
  [31] = {
    PlatformExtCommon = "achievement_radiant",
    GooglePlayGame = "CgkIofXkg7cbEAIQIg"
  },
  [32] = {
    PlatformExtCommon = "achievement_new_guest",
    GooglePlayGame = "CgkIofXkg7cbEAIQIw"
  },
  [33] = {
    PlatformExtCommon = "achievement_moneybags",
    GooglePlayGame = "CgkIofXkg7cbEAIQJQ"
  },
  [34] = {
    PlatformExtCommon = "achievement_on_your_marks",
    GooglePlayGame = "CgkIofXkg7cbEAIQJg"
  },
  [35] = {
    PlatformExtCommon = "achievement_iron_bench",
    GooglePlayGame = "CgkIofXkg7cbEAIQJw"
  },
  [36] = {
    PlatformExtCommon = "achievement_group_of_legends",
    GooglePlayGame = "CgkIofXkg7cbEAIQKA"
  },
  [37] = {
    PlatformExtCommon = "achievement_bachelor",
    GooglePlayGame = "CgkIofXkg7cbEAIQKQ"
  },
  [38] = {
    PlatformExtCommon = "achievement_master",
    GooglePlayGame = "CgkIofXkg7cbEAIQKg"
  },
  [39] = {
    PlatformExtCommon = "achievement_phd",
    GooglePlayGame = "CgkIofXkg7cbEAIQKw"
  },
  [40] = {
    PlatformExtCommon = "achievement_hyperspeed",
    GooglePlayGame = "CgkIofXkg7cbEAIQLA"
  },
  [41] = {
    PlatformExtCommon = "achievement_acceleration",
    GooglePlayGame = "CgkIofXkg7cbEAIQLQ"
  },
  [42] = {
    PlatformExtCommon = "achievement_speed_racer",
    GooglePlayGame = "CgkIofXkg7cbEAIQLg"
  },
  [43] = {
    PlatformExtCommon = "achievement_wind_rider",
    GooglePlayGame = "CgkIofXkg7cbEAIQLw"
  },
  [44] = {
    PlatformExtCommon = "achievement_bumper_harvest",
    GooglePlayGame = "CgkIofXkg7cbEAIQMA"
  },
  [45] = {
    PlatformExtCommon = "achievement_airstrike",
    GooglePlayGame = "CgkIofXkg7cbEAIQMQ"
  },
  [46] = {
    PlatformExtCommon = "achievement_my_territory",
    GooglePlayGame = "CgkIofXkg7cbEAIQMg"
  },
  [47] = {
    PlatformExtCommon = "achievement_poor_darling",
    GooglePlayGame = "CgkIofXkg7cbEAIQMw"
  },
  [48] = {
    PlatformExtCommon = "achievement_philanthropist",
    GooglePlayGame = "CgkIofXkg7cbEAIQNA"
  },
  [49] = {
    PlatformExtCommon = "achievement_im_a_pirate",
    GooglePlayGame = "CgkIofXkg7cbEAIQNQ"
  },
  [50] = {
    PlatformExtCommon = "achievement_myopia",
    GooglePlayGame = "CgkIofXkg7cbEAIQNg"
  },
  [51] = {
    PlatformExtCommon = "achievement_notorious",
    GooglePlayGame = "CgkIofXkg7cbEAIQNw"
  },
  [52] = {
    PlatformExtCommon = "achievement_king_pirate",
    GooglePlayGame = "CgkIofXkg7cbEAIQOA"
  },
  [53] = {
    PlatformExtCommon = "achievement_miser",
    GooglePlayGame = "CgkIofXkg7cbEAIQOQ"
  },
  [54] = {
    PlatformExtCommon = "achievement_little_miner",
    GooglePlayGame = "CgkIofXkg7cbEAIQOg"
  },
  [55] = {
    PlatformExtCommon = "achievement_rags_to_riches",
    GooglePlayGame = "CgkIofXkg7cbEAIQOw"
  },
  [56] = {
    PlatformExtCommon = "achievement_mining_giant",
    GooglePlayGame = "CgkIofXkg7cbEAIQPA"
  },
  [57] = {
    PlatformExtCommon = "achievement_trickster",
    GooglePlayGame = "CgkIofXkg7cbEAIQPQ"
  },
  [58] = {
    PlatformExtCommon = "achievement_taste_of_power",
    GooglePlayGame = "CgkIofXkg7cbEAIQPg"
  },
  [59] = {
    PlatformExtCommon = "achievement_lords",
    GooglePlayGame = "CgkIofXkg7cbEAIQPw"
  },
  [60] = {
    PlatformExtCommon = "achievement_long_live_the_allies",
    GooglePlayGame = "CgkIofXkg7cbEAIQQA"
  },
  [61] = {
    PlatformExtCommon = "achievement_wait_and_see",
    GooglePlayGame = "CgkIofXkg7cbEAIQQQ"
  },
  [62] = {
    PlatformExtCommon = "achievement_independence_day",
    GooglePlayGame = "CgkIofXkg7cbEAIQQg"
  },
  [63] = {
    PlatformExtCommon = "achievement_ropeadope",
    GooglePlayGame = "CgkIofXkg7cbEAIQQw"
  },
  [64] = {
    PlatformExtCommon = "achievement_strength_of_unity",
    GooglePlayGame = "CgkIofXkg7cbEAIQRA"
  },
  [65] = {
    PlatformExtCommon = "achievement_slayer",
    GooglePlayGame = "CgkIofXkg7cbEAIQRQ"
  },
  [66] = {
    PlatformExtCommon = "achievement_mainstay",
    GooglePlayGame = "CgkIofXkg7cbEAIQRg"
  },
  [67] = {
    PlatformExtCommon = "achievement_galaxy_ship",
    GooglePlayGame = "CgkIofXkg7cbEAIQRw"
  },
  [68] = {
    PlatformExtCommon = "achievement_musket",
    GooglePlayGame = "CgkIofXkg7cbEAIQSA"
  },
  [69] = {
    PlatformExtCommon = "achievement_bronze_armor",
    GooglePlayGame = "CgkIofXkg7cbEAIQUw"
  },
  [70] = {
    PlatformExtCommon = "achievement_fledgling",
    GooglePlayGame = "CgkIofXkg7cbEAIQSQ"
  },
  [71] = {
    PlatformExtCommon = "achievement_defensive_stance",
    GooglePlayGame = "CgkIofXkg7cbEAIQSg"
  },
  [72] = {
    PlatformExtCommon = "achievement_destroyer_apprentice",
    GooglePlayGame = "CgkIofXkg7cbEAIQSw"
  },
  [73] = {
    PlatformExtCommon = "achievement_not_afraid",
    GooglePlayGame = "CgkIofXkg7cbEAIQTA"
  },
  [74] = {
    PlatformExtCommon = "achievement_fly_to_space",
    GooglePlayGame = "CgkIofXkg7cbEAIQTQ"
  },
  [75] = {
    PlatformExtCommon = "achievement_galaxy_mothership",
    GooglePlayGame = "CgkIofXkg7cbEAIQTg"
  },
  [76] = {
    PlatformExtCommon = "achievement_weapon_mastery",
    GooglePlayGame = "CgkIofXkg7cbEAIQTw"
  },
  [77] = {
    PlatformExtCommon = "achievement_silver_armor",
    GooglePlayGame = "CgkIofXkg7cbEAIQUA"
  },
  [78] = {
    PlatformExtCommon = "achievement_in_the_house",
    GooglePlayGame = "CgkIofXkg7cbEAIQUQ"
  },
  [79] = {
    PlatformExtCommon = "achievement_guardian_force",
    GooglePlayGame = "CgkIofXkg7cbEAIQUg"
  },
  [80] = {
    PlatformExtCommon = "achievement_destroy_force",
    GooglePlayGame = "CgkIofXkg7cbEAIQVA"
  },
  [81] = {
    PlatformExtCommon = "achievement_defense",
    GooglePlayGame = "CgkIofXkg7cbEAIQVQ"
  },
  [82] = {
    PlatformExtCommon = "achievement_speed_of_light",
    GooglePlayGame = "CgkIofXkg7cbEAIQVg"
  },
  [83] = {
    PlatformExtCommon = "achievement_more_than_large",
    GooglePlayGame = "CgkIofXkg7cbEAIQVw"
  },
  [84] = {
    PlatformExtCommon = "achievement_nobody_beats",
    GooglePlayGame = "CgkIofXkg7cbEAIQWA"
  },
  [85] = {
    PlatformExtCommon = "achievement_golden_armour",
    GooglePlayGame = "CgkIofXkg7cbEAIQWQ"
  },
  [86] = {
    PlatformExtCommon = "achievement_mother_nature",
    GooglePlayGame = "CgkIofXkg7cbEAIQWg"
  },
  [87] = {
    PlatformExtCommon = "achievement_final_protect",
    GooglePlayGame = "CgkIofXkg7cbEAIQWw"
  },
  [88] = {
    PlatformExtCommon = "achievement_the_annihilation",
    GooglePlayGame = "CgkIofXkg7cbEAIQXA"
  },
  [89] = {
    PlatformExtCommon = "achievement_undying",
    GooglePlayGame = "CgkIofXkg7cbEAIQXQ"
  },
  [90] = {
    PlatformExtCommon = "achievement_unmatched",
    GooglePlayGame = "CgkIofXkg7cbEAIQXg"
  },
  [91] = {
    PlatformExtCommon = "achievement_brand_new_world",
    GooglePlayGame = "CgkIofXkg7cbEAIQXw"
  },
  [92] = {
    PlatformExtCommon = "achievement_ruler_of_the_cosmos",
    GooglePlayGame = "CgkIofXkg7cbEAIQYA"
  },
  [93] = {
    PlatformExtCommon = "achievement_postdoctorate",
    GooglePlayGame = "CgkIofXkg7cbEAIQYQ"
  },
  [94] = {
    PlatformExtCommon = "achievement_unprecedented",
    GooglePlayGame = "CgkIofXkg7cbEAIQYg"
  },
  [95] = {
    PlatformExtCommon = "achievement_power_of_creation",
    GooglePlayGame = "CgkIofXkg7cbEAIQYw"
  },
  [96] = {
    PlatformExtCommon = "achievement_invincibility",
    GooglePlayGame = "CgkIofXkg7cbEAIQZA"
  },
  [97] = {
    PlatformExtCommon = "achievement_galactic_law",
    GooglePlayGame = "CgkIofXkg7cbEAIQZQ"
  },
  [98] = {
    PlatformExtCommon = "achievement_invisibility",
    GooglePlayGame = "CgkIofXkg7cbEAIQZg"
  },
  [99] = {
    PlatformExtCommon = "achievement_power_of_doom",
    GooglePlayGame = "CgkIofXkg7cbEAIQZw"
  },
  [100] = {
    PlatformExtCommon = "achievement_eternal_legend",
    GooglePlayGame = "CgkIofXkg7cbEAIQaA"
  },
  [101] = {
    PlatformExtCommon = "achievement_chasing_the_past",
    GooglePlayGame = "CgkIofXkg7cbEAIQaQ"
  }
}
LeaderBoardPlatformExtMap = {
  Force = {
    PlatformExtCommon = "leaderboard_power_rank",
    GooglePlayGame = "CgkIofXkg7cbEAIQHA"
  }
}
