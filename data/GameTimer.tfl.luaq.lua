require("data1/Minheap.tfl")
GameTimer = {
  heap = Minheap.new(function(a, b)
    return a[1] < b[1]
  end)
}
function GameTimer:Init()
  self:Clear()
end
function GameTimer:Update(dt)
  local now = ext.getSysTime()
  while not self.heap:empty() do
    local t, f = unpack(self.heap:top())
    if now < t then
      break
    end
    local rv = self.heap:pop()
    local r = f()
    if r then
      rv[1] = now + r
      self.heap:push(rv)
    end
  end
end
function GameTimer:Add(timerFunc, interval)
  assert(timerFunc)
  self.heap:push({
    ext.getSysTime() + interval,
    timerFunc
  })
end
function GameTimer:Clear()
  self.heap:clear()
end
function GameTimer:ConvertTimeToHReadbleTime(time)
  local currTime = os.date("%c", time)
  return currTime
end
