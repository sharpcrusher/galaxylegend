local GameFleetEquipment = LuaObjectManager:GetLuaObject("GameFleetEquipment")
local GameStateEquipEnhance = GameStateManager.GameStateEquipEnhance
GameFleetEquipment.chuancang = {}
require("GameTextEdit.tfl")
local GameArtifact = GameFleetEquipment.chuancang
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUILab = LuaObjectManager:GetLuaObject("GameUILab")
function GameArtifact:Hide(...)
  GameGlobalData:RemoveDataChangeCallback("fleetinfo", GameFleetEquipment.OnEnhanceFleetinfoChange)
  self:GetFlashObject():InvokeASCallback("_root", "Hidechuancang")
end
function GameArtifact:OnEraseFromGameState()
  GameGlobalData:RemoveDataChangeCallback("fleetinfo", GameFleetEquipment.OnEnhanceFleetinfoChange)
end
GameArtifact.currentTab = 1
GameArtifact.currentFleetId = 1
GameArtifact.currentTabInfo = {}
GameArtifact.currentAlldata = {}
GameArtifact.coreItems = {}
GameArtifact.currentCore = {}
GameArtifact.resource = 0
GameArtifact.resetCost = 0
GameArtifact.currentPoint = 10101
GameArtifact.addBuff = {}
GameArtifact.power = 0
GameArtifact.curLevel = 0
GameArtifact.oneKeyUpgradeData = nil
GameArtifact.curActivePoint = nil
GameArtifact.perfectResetCost = 0
GameArtifact.curUpgrade = nil
function GameArtifact:GetFlashObject()
  return (...), GameFleetEquipment
end
function GameArtifact:Show(fleetId, power)
  DebugOut("hello:test")
  self:requestMainData(fleetId)
  self.power = power
end
function GameArtifact:updateArtifactUI(pageData, isEquipCore, isNewUpdate)
  local addInfo = {}
  local core_level = 0
  addInfo.numBuff = {}
  GameArtifact.curLevel = 0
  if pageData[self.currentTab].now_core.is_corr_type == 1 then
    addInfo.percentBuff = pageData[self.currentTab].now_core.buff
  else
    addInfo.percentBuff = 0
  end
  GameArtifact.curActivePoint = nil
  for k, v in ipairs(pageData[self.currentTab].point_list) do
    if v.level < v.max_level and GameArtifact.curActivePoint == nil then
      GameArtifact.curActivePoint = v.id
    end
    if v.level == 0 then
      break
    end
    GameArtifact.curLevel = GameArtifact.curLevel + v.level
    for k1, v1 in ipairs(v.point_buff) do
      local buffexist = false
      local item = {}
      local existIndex = 1
      for k2, v2 in ipairs(addInfo.numBuff) do
        if v2.buffid == v1.buff_id then
          buffexist = true
          existIndex = k2
          break
        end
      end
      if buffexist then
        addInfo.numBuff[existIndex].buffnum = addInfo.numBuff[existIndex].buffnum + v1.buff_num
      else
        item.buffid = v1.buff_id
        item.buffnum = self:GetBuffNum(v1.buff_id, v1.buff_num)
        item.buffname = self:getBuffName(v1.buff_id)
        table.insert(addInfo.numBuff, item)
      end
    end
    core_level = core_level + 1
  end
  self.currentCore = pageData[self.currentTab].now_core
  self:updateTabStatus(pageData)
  DebugOut("addInfo:")
  DebugTable(addInfo)
  self:setAddInfo(addInfo, pageData[self.currentTab].now_core.is_corr_type)
  self:updatePointInfo(pageData[self.currentTab].point_list, self.currentTab, isNewUpdate, GameArtifact.curActivePoint)
  local now_core = pageData[self.currentTab].now_core
  self:updateCoreInfo(pageData[self.currentTab].core_type, now_core, core_level, isEquipCore)
  self:updateTabText()
  self:updateRedPoint(pageData)
end
function GameArtifact:updateRedPoint(pageData)
  if self:GetFlashObject() then
    local tabRedPointArr = {}
    for _, v in ipairs(pageData) do
      local hasPoint = 0
      if v.equip_red == 1 or v.upgrade_red == 1 then
        hasPoint = 1
      end
      table.insert(tabRedPointArr, hasPoint)
    end
    local curData = pageData[self.currentTab]
    local fleetinfo, fleetIdx = ZhenXinUI():GetCurFleetContent()
    DebugOut("updateRedPoint__", fleetinfo.identity)
    DebugOut("FleetInMatrix_true")
    self:GetFlashObject():InvokeASCallback("_root", "updatePageRedPoint", tabRedPointArr, curData.equip_red, curData.upgrade_red)
  end
end
function GameArtifact:showPointInfo(point_id)
  local point_info = {}
  local index = 0
  for k, v in ipairs(self.currentAlldata[self.currentTab].point_list) do
    if v.id == point_id then
      point_info = v
      index = k
      break
    end
  end
  local param = point_info
  if point_info.can_upgrade == 0 then
    for k, v in ipairs(point_info.point_next_buff) do
      if not param.point_buff then
        param.point_buff = {}
      elseif not param.point_buff[k] then
        param.point_buff[k] = {}
      end
      param.point_buff[k].buffname = self:getBuffName(v.buff_id)
      param.point_buff[k].buff_num = self:GetBuffNum(v.buff_id, 0)
    end
  else
    for k, v in ipairs(point_info.point_buff) do
      param.point_buff[k].buffname = self:getBuffName(v.buff_id)
    end
  end
  point_info.pointname = GameLoader:GetGameText("LC_MENU_ARTIFACT_NODE_NAME_" .. point_info.type)
  DebugOut("here:0123456")
  DebugTable(param)
  if param.can_upgrade == 0 or param.level == param.max_level then
    DebugOut("comes here!!")
    self:GetFlashObject():InvokeASCallback("_root", "showPointInfo", index)
    GameStateEquipEnhance:GetPop():chuancang_showPointInfo(index, param, GameLoader:GetGameText("LC_MENU_Level"), self.pt, self.tip_width)
  end
end
function GameArtifact:updateTabText()
  self:GetFlashObject():InvokeASCallback("_root", "updateTabText")
end
function GameArtifact:updateResAndResetCost(resNum, resetCost)
  self:GetFlashObject():InvokeASCallback("_root", "updateResAndResetCost", resNum)
  GameStateEquipEnhance:GetPop():chuancang_updateResAndResetCost(resetCost)
end
function GameArtifact:updateCoreInfo(coretype, datas, core_level)
  local param = datas
  local coreItem = {}
  coreItem.item_type = "item"
  coreItem.number = datas.type
  coreItem.no = 1
  coreItem.level = datas.level
  DebugOut("coreitem:")
  DebugTable(coreItem)
  param.iconName = "item_" .. datas.type
  param.titleText = GameLoader:GetGameText("LC_MENU_ARTIFACT_EQUIP_NAME_" .. param.type)
  local foralltypedesc = ""
  for k, v in ipairs(param.add_buff) do
    foralltypedesc = foralltypedesc .. GameDataAccessHelper:GetArtifactSkillDesc(v) .. "\n"
  end
  DebugOut("foralltypedesc:" .. foralltypedesc)
  param.infoText = foralltypedesc
  self:GetFlashObject():InvokeASCallback("_root", "updateCoreInfo", coretype, param, core_level, nil, GameLoader:GetGameText("LC_MENU_Level"))
end
function GameArtifact:updatePointInfo(datas, tab, isNewUpdate, curActivePoint)
  DebugOut("pointInfo")
  DebugTable(datas)
  local page = 2
  for k, v in ipairs(datas) do
    if v.level == 0 then
      DebugOut("tp's:key:" .. k)
      if k <= 9 then
        page = 1
      end
      break
    end
  end
  if datas then
    self:GetFlashObject():InvokeASCallback("_root", "updatePointInfo", datas, page, tab, isNewUpdate, curActivePoint)
  end
end
function GameArtifact:updateTabStatus(datas)
  local param = {}
  for _, data in ipairs(datas) do
    table.insert(param, data.is_locked)
  end
  DebugOut("tp:tabStatus")
  DebugTable(param)
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "updateTabStatus", param)
  end
end
function GameArtifact:showCoreList(CoreListData)
  local items = {}
  for _, item in ipairs(CoreListData) do
    item.corename = GameLoader:GetGameText("LC_MENU_ARTIFACT_EQUIP_NAME_" .. item.type)
    local forcorrtype = GameLoader:GetGameText("LC_MENU_ARTIFACT_FOR_CORE_TYPE_" .. item.color_type)
    local forcorrtypedesc = string.gsub(GameLoader:GetGameText("LC_MENU_ARTIFACT_CORE_BASIC_BENIFIT"), "<benifits_num>", item.buff * 1 / 10)
    local foralltype = GameLoader:GetGameText("LC_MENU_ARTIFACT_FOR_CORE_TYPE_0")
    local foralltypedesc = ""
    for k, v in ipairs(item.add_buff) do
      foralltypedesc = foralltypedesc .. GameDataAccessHelper:GetArtifactSkillDesc(v) .. "\n"
    end
    item.descText = forcorrtype .. "\n" .. forcorrtypedesc .. "\n" .. foralltype .. "\n" .. foralltypedesc
    local coreItem = {}
    coreItem.item_type = "item"
    coreItem.number = item.type
    coreItem.no = 1
    coreItem.level = item.level
    DebugOut("coreitem:")
    DebugTable(coreItem)
    item.iconName = "item_" .. item.type
    table.insert(items, item)
  end
  local sortfun = function(a, b)
    if a.is_corr_type == 1 and b.is_corr_type == 0 then
      return true
    elseif a.is_corr_type == 0 and b.is_corr_type == 1 then
      return false
    elseif a.color_type < b.color_type then
      return true
    elseif a.color_type > b.color_type then
      return false
    elseif a.star > b.star then
      return true
    else
      return false
    end
  end
  table.sort(items, sortfun)
  DebugOut("tps:sort")
  DebugTable(items)
  self.coreItems = items
  local now_core = self.currentCore
  if now_core.id ~= 0 then
    now_core.corename = GameLoader:GetGameText("LC_MENU_ARTIFACT_EQUIP_NAME_" .. now_core.type)
    DebugOut("corename: " .. now_core.corename)
    local forcorrtype1 = GameLoader:GetGameText("LC_MENU_ARTIFACT_FOR_CORE_TYPE_" .. now_core.color_type)
    local numstring = now_core.buff * 1 / 10 .. "%"
    local forcorrtypedesc1 = string.gsub(GameLoader:GetGameText("LC_MENU_ARTIFACT_CORE_BASIC_BENIFIT"), "<benifits_num>", now_core.buff * 1 / 10)
    local foralltype1 = GameLoader:GetGameText("LC_MENU_ARTIFACT_FOR_CORE_TYPE_0")
    local foralltypedesc1 = ""
    for k, v in ipairs(now_core.add_buff) do
      foralltypedesc1 = foralltypedesc1 .. GameDataAccessHelper:GetArtifactSkillDesc(v) .. "\n"
    end
    DebugOut("test_tp:" .. foralltypedesc1)
    now_core.descText = forcorrtype1 .. "\n" .. forcorrtypedesc1 .. "\n" .. foralltype1 .. "\n" .. foralltypedesc1
    now_core.iconName = "item_" .. now_core.type
  end
  GameStateEquipEnhance:GetPop():chuancang_showCoreItems(#items, now_core, GameLoader:GetGameText("LC_MENU_Level"))
  local HasEquipedTitle = GameLoader:GetGameText("LC_MENU_NEW_FLEET_EQUIPED")
  local NotEquipedTitle = GameLoader:GetGameText("LC_MENU_NEW_FLEET_NO_EQUIPMENT")
  GameStateEquipEnhance:GetPop():chuancang_showEquipedCoreItem(now_core, HasEquipedTitle, NotEquipedTitle)
end
function GameArtifact:showCoreInfo(coreId)
  local coreInfo = {}
  for _, core in ipairs(self.coreItems) do
    if core.id == coreId then
      coreInfo = core
      break
    end
  end
  DebugOut("coreItems__")
  DebugOut(self.currentAlldata[self.currentTab].now_core.id)
  DebugTable(self.coreItems)
  if coreId == self.currentCore.id then
    coreInfo = self.currentCore
  end
  coreInfo.corename = GameLoader:GetGameText("LC_MENU_ARTIFACT_EQUIP_NAME_" .. coreInfo.type)
  DebugOut("corename:" .. coreInfo.corename)
  local forcorrtype = GameLoader:GetGameText("LC_MENU_ARTIFACT_FOR_CORE_TYPE_" .. coreInfo.color_type)
  local forcorrtypedesc = string.gsub(GameLoader:GetGameText("LC_MENU_ARTIFACT_CORE_BASIC_BENIFIT"), "<benifits_num>", coreInfo.buff * 1 / 10)
  local foralltype = GameLoader:GetGameText("LC_MENU_ARTIFACT_FOR_CORE_TYPE_0")
  local foralltypedesc = ""
  for k, v in ipairs(coreInfo.add_buff) do
    foralltypedesc = foralltypedesc .. GameDataAccessHelper:GetArtifactSkillDesc(v) .. "\n"
  end
  DebugOut("coreInfo:" .. forcorrtypedesc)
  DebugOut("coreInfo:" .. foralltypedesc)
  coreInfo.descText = forcorrtype .. "\n" .. forcorrtypedesc .. "\n" .. foralltype .. "\n" .. foralltypedesc
  local isNowCore = false
  if coreId == self.currentAlldata[self.currentTab].now_core.id then
    isNowCore = true
  end
  local replaceText = GameLoader:GetGameText("LC_MENU_ARTIFACT_REPLACE")
  if self.currentAlldata[self.currentTab].now_core.id == 0 then
    replaceText = GameLoader:GetGameText("LC_MENU_ARTIFACT_EQUIP")
  end
  DebugOut("replaceText : " .. replaceText)
  GameStateEquipEnhance:GetPop():chuancang_showCoreInfo(coreInfo, isNowCore, replaceText, GameLoader:GetGameText("LC_MENU_Level"))
end
function GameArtifact:setAddInfo(buffDatas, is_corr_type)
  local param = buffDatas
  local sortFun = function(a, b)
    return a.buffid < b.buffid
  end
  table.sort(param.numBuff, sortFun)
  local isCorrectType = is_corr_type == 1
  DebugOut("aftersort:")
  DebugTable(param)
  local titleText = GameLoader:GetGameText("LC_MENU_ARTIFACT_CURRENT_ATTR")
  self:GetFlashObject():InvokeASCallback("_root", "updateAddinfo", param, isCorrectType, titleText)
end
function GameArtifact:getBuffName(buffid)
  local buffname = ""
  if buffid < 19 or buffid > 29 then
    buffname = GameLoader:GetGameText("LC_MENU_Equip_param_" .. buffid)
  else
    buffname = GameLoader:GetGameText("LC_MENU_Equip_param_" .. buffid + 2)
  end
  return buffname
end
function GameArtifact:GetBuffNum(buffid, buff_num)
  local buffnum = ""
  if buffid > 8 and buffid < 15 or buffid == 17 or buffid > 18 and buffid < 23 then
    buffnum = buff_num * 1 / 10 .. "%"
  else
    buffnum = buff_num
  end
  return buffnum
end
function GameArtifact:setArtPoint(pointNum)
  local flashObj = self:GetFlashObject()
  if flashObj then
    DebugOut("pointNum: " .. pointNum)
    flashObj:InvokeASCallback("_root", "setArtPoint", pointNum)
  end
end
function GameArtifact:showPointUpgrade(content)
  local pointInfo = content.point_info
  pointInfo.updateAddinfo = {}
  if content.point_info.level == 0 and content.point_info.can_upgrade == 0 then
    for _, v in ipairs(content.point_info.point_next_buff) do
      local item = {}
      item.titleText = self:getBuffName(v.buff_id)
      item.numText = self:GetBuffNum(v.buff_id, 0)
      table.insert(pointInfo.updateAddinfo, item)
    end
  elseif content.point_info.level == 0 then
    for k, v in ipairs(content.point_info.point_next_buff) do
      local item = {}
      item.titleText = self:getBuffName(v.buff_id)
      item.numText = "0 (<font color=\"#00CC00\">+ " .. self:GetBuffNum(v.buff_id, v.buff_num) .. "</font>)"
      table.insert(pointInfo.updateAddinfo, item)
    end
  elseif content.point_info.level == content.point_info.max_level then
    for _, v in ipairs(content.point_info.point_buff) do
      local item = {}
      item.titleText = self:getBuffName(v.buff_id)
      item.numText = self:GetBuffNum(v.buff_id, v.buff_num)
      table.insert(pointInfo.updateAddinfo, item)
    end
  else
    for k, v in ipairs(content.point_info.point_buff) do
      local item = {}
      item.titleText = self:getBuffName(content.point_info.point_next_buff[k].buff_id)
      local addnum = 0
      for k1, v1 in ipairs(content.point_info.point_next_buff) do
        if v1.buff_id == v.buff_id then
          addnum = v1.buff_num - v.buff_num
          break
        end
      end
      item.numText = self:GetBuffNum(v.buff_id, v.buff_num) .. " (<font color=\"#00CC00\">+ " .. self:GetBuffNum(v.buff_id, addnum) .. "</font>)"
      table.insert(pointInfo.updateAddinfo, item)
    end
  end
  pointInfo.pointname = GameLoader:GetGameText("LC_MENU_ARTIFACT_NODE_NAME_" .. content.point_info.type)
  local point_list = self.currentAlldata[self.currentTab].point_list
  local len = #point_list
  if point_list[len].id == pointInfo.id and self.currentTab ~= 3 then
    pointInfo.infoText = GameLoader:GetGameText("LC_MENU_ARTIFACT_NODE_TIP_COMMON_" .. self.currentTab)
  else
    pointInfo.infoText = GameLoader:GetGameText("LC_MENU_ARTIFACT_NODE_TIP_COMMON")
  end
  DebugOut("pointInfo:")
  DebugTable(pointInfo)
  GameStateEquipEnhance:GetPop():chuancang_showPointUpgrade(pointInfo)
end
function GameArtifact:Updatepower(powerNum)
  local flashObj = self:GetFlashObject()
  local isPowerup = false
  DebugOut("hello,power")
  if flashObj then
    flashObj:InvokeASCallback("_root", "Updatepower", GameUtils.numberConversion(powerNum))
  end
end
function GameArtifact:LightPoint()
  self:GetFlashObject():InvokeASCallback("_root", "ligthPoint")
end
function GameArtifact:LightProcessbar()
  self:GetFlashObject():InvokeASCallback("_root", "ligthProcessbar")
end
function GameArtifact:updatePointPop(point_info)
  local pointInfo = point_info
  DebugTable(point_info)
  pointInfo.updateAddinfo = {}
  if point_info.level == 0 and point_info.can_upgrade == 0 then
    for _, v in ipairs(point_info.point_next_buff) do
      local item = {}
      item.titleText = self:getBuffName(v.buff_id)
      item.numText = self:GetBuffNum(v.buff_id, 0)
      table.insert(pointInfo.updateAddinfo, item)
    end
  elseif point_info.level == 0 then
    for k, v in ipairs(point_info.point_next_buff) do
      local item = {}
      item.titleText = self:getBuffName(v.buff_id)
      item.numText = "0 (<font color=\"#00CC00\">+ " .. self:GetBuffNum(v.buff_id, v.buff_num) .. "</font>)"
      table.insert(pointInfo.updateAddinfo, item)
    end
  elseif point_info.level == point_info.max_level then
    for _, v in ipairs(point_info.point_buff or {}) do
      local item = {}
      item.titleText = self:getBuffName(v.buff_id)
      item.numText = self:GetBuffNum(v.buff_id, v.buff_num)
      table.insert(pointInfo.updateAddinfo, item)
    end
  else
    for k, v in ipairs(point_info.point_buff) do
      local item = {}
      item.titleText = self:getBuffName(point_info.point_next_buff[k].buff_id)
      local addnum = 0
      for k1, v1 in ipairs(point_info.point_next_buff) do
        if v1.buff_id == v.buff_id then
          addnum = v1.buff_num - v.buff_num
          break
        end
      end
      item.numText = self:GetBuffNum(v.buff_id, v.buff_num) .. " (<font color=\"#00CC00\">+ " .. self:GetBuffNum(v.buff_id, addnum) .. "</font>)"
      table.insert(pointInfo.updateAddinfo, item)
    end
  end
  pointInfo.pointname = GameLoader:GetGameText("LC_MENU_ARTIFACT_NODE_NAME_" .. point_info.type)
  local point_list = self.currentAlldata[self.currentTab].point_list
  local len = #point_list
  if point_list[len].id == pointInfo.id and self.currentTab ~= 3 then
    pointInfo.infoText = GameLoader:GetGameText("LC_MENU_ARTIFACT_NODE_TIP_COMMON_" .. self.currentTab)
  else
    pointInfo.infoText = GameLoader:GetGameText("LC_MENU_ARTIFACT_NODE_TIP_COMMON")
  end
  DebugOut("pointInfo:")
  DebugTable(pointInfo)
  GameStateEquipEnhance:GetPop():chuancang_updatePointUpgrade(pointInfo)
end
function GameArtifact:setCoreItem(coreIndex)
  local items = {}
  for x = (coreIndex - 1) * 3 + 1, coreIndex * 3 do
    if not self.coreItems[x] then
      local x = {}
      x.id = 0
      table.insert(items, x)
    else
      table.insert(items, self.coreItems[x])
    end
  end
  DebugOut("sorted:")
  DebugTable(items)
  GameStateEquipEnhance:GetPop():chuancang_setCoreItem(coreIndex, items)
end
function GameArtifact:showTabdata(tabID)
  if tabID ~= self.currentTab then
    self.currentTab = tabID
    DebugOut("self.currentAlldata:")
    self:GetFlashObject():InvokeASCallback("_root", "HideUpgradeAnims")
    self:updateArtifactUI(self.currentAlldata, false, false)
    GameArtifact:setUpgradeCost(self.currentTab)
  end
end
function GameArtifact:setTabtext(txt1, txt2, txt3)
  DebugOut("GameArtifact:setTabtext: " .. txt1 .. " " .. txt2 .. " " .. txt3)
  self:GetFlashObject():InvokeASCallback("_root", "setTabtext", txt1, txt2, txt3)
end
function GameArtifact:showAllBuff(is_corr_type)
end
function GameArtifact:showResetPop()
  local free_txt = string.gsub(GameLoader:GetGameText("LC_MENU_ARTIFACT_FEEDBACK_RESOURCES"), "<resources_num>", "80")
  local nofree_txt = string.gsub(GameLoader:GetGameText("LC_MENU_ARTIFACT_FEEDBACK_RESOURCES"), "<resources_num>", "100")
  if self.currentAlldata[1].point_list[1].level == 0 and self.currentCore and self.currentCore.id and self.currentCore.id == 0 then
    GameStateEquipEnhance:GetPop():chuancang_setResetButton(false, "no need to reset", free_txt, nofree_txt)
  else
    GameStateEquipEnhance:GetPop():chuancang_setResetButton(true, "no need to reset", free_txt, nofree_txt)
  end
end
function GameArtifact:requestResetFree()
  if self.currentAlldata[1].point_list[1].level == 0 then
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.OK)
    GameUIMessageDialog:Display(GameLoader:GetGameText("LC_MENU_TITLE_VERIFY"), GameLoader:GetGameText("LC_MENU_ARTIFACT_RESET_ERROR"))
  else
    do
      local param = {}
      param.type = 1
      param.fleet_id = self.currentFleetId
      local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
      local function callback()
        NetMessageMgr:SendMsg(NetAPIList.art_reset_req.Code, param, self.requestResetCallback, true, nil)
      end
      GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
      GameUIMessageDialog:SetNoButton(nil)
      GameUIMessageDialog:SetYesButton(callback)
      DebugOut("Dialog~~~free")
      DebugOut(text_title)
      DebugOut(string.gsub(GameLoader:GetGameText("LC_MENU_ARTIFACT_RESET_FEEADBACK_RESOURCE"), "<resources_num>", "80") .. " " .. GameLoader:GetGameText("LC_MENU_ARTIFACT_RESET_CONFIRM"))
      GameUIMessageDialog:Display(text_title, string.gsub(GameLoader:GetGameText("LC_MENU_ARTIFACT_RESET_FEEADBACK_RESOURCE"), "<resources_num>", "80") .. " " .. GameLoader:GetGameText("LC_MENU_ARTIFACT_RESET_CONFIRM"))
    end
  end
end
function GameArtifact:requestResetNoFree()
  local param = {}
  param.type = 2
  param.fleet_id = self.currentFleetId
  local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
  local function callback()
    NetMessageMgr:SendMsg(NetAPIList.art_reset_req.Code, param, self.requestResetCallback, true, nil)
  end
  GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
  GameUIMessageDialog:SetNoButton(nil)
  GameUIMessageDialog:SetYesButton(callback)
  GameUIMessageDialog:Display(text_title, string.gsub(GameLoader:GetGameText("LC_MENU_ARTIFACT_RESET_FEEADBACK_RESOURCE"), "<resources_num>", "100") .. "\n" .. GameLoader:GetGameText("LC_MENU_ARTIFACT_RESET_CONFIRM"))
end
function GameArtifact:ResetExcute()
  local param = {}
  param.type = 1
  param.fleet_id = self.currentFleetId
  NetMessageMgr:SendMsg(NetAPIList.art_reset_req.Code, param, self.requestResetCallback, true, nil)
  GameFleetEquipment:RefreshKryptonRedPoint()
end
function GameArtifact:PerfectResetExcute()
  local param = {}
  param.type = 2
  param.fleet_id = self.currentFleetId
  local function callback()
    NetMessageMgr:SendMsg(NetAPIList.art_reset_req.Code, param, self.requestResetCallback, true, nil)
    GameFleetEquipment:RefreshKryptonRedPoint()
  end
  GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
  GameUIMessageDialog:SetNoButton(nil)
  GameUIMessageDialog:SetYesButton(callback)
  local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
  GameUIMessageDialog:Display(text_title, string.gsub(GameLoader:GetGameText("LC_MENU_ARTIFACT_RESET_FEEADBACK_RESOURCE_2"), "<credits_num>", tostring(GameArtifact.perfectResetCost)) .. " " .. GameLoader:GetGameText("LC_MENU_ARTIFACT_RESET_CONFIRM"))
end
function GameArtifact:requestPerfectReset()
  if self.currentAlldata[1].point_list[1].level == 0 then
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.OK)
    GameUIMessageDialog:Display(GameLoader:GetGameText("LC_MENU_TITLE_VERIFY"), GameLoader:GetGameText("LC_MENU_ARTIFACT_RESET_ERROR"))
  else
    do
      local param = {}
      param.type = 2
      param.fleet_id = self.currentFleetId
      local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
      local function callback()
        NetMessageMgr:SendMsg(NetAPIList.art_reset_req.Code, param, self.requestResetCallback, true, nil)
      end
      local t1 = string.gsub(GameLoader:GetGameText("LC_MENU_ARTIFACT_RESET_FEEADBACK_RESOURCE"), "<resources_num>", "80") .. " " .. GameLoader:GetGameText("LC_MENU_ARTIFACT_RESET_CONFIRM")
      local t2 = string.gsub(GameLoader:GetGameText("LC_MENU_NEW_FLEET_CABIN_PERFECT_RESET"), "<number>", "500")
      GameStateEquipEnhance:GetPop():chuancang_showPerfectReset(t1, t2)
      DebugOut("Dialog~~~")
      DebugOut(text_title)
      DebugOut(string.gsub(GameLoader:GetGameText("LC_MENU_ARTIFACT_RESET_FEEADBACK_RESOURCE_2"), "<credits_num>", tostring(GameArtifact.perfectResetCost)) .. " " .. GameLoader:GetGameText("LC_MENU_ARTIFACT_RESET_CONFIRM"))
    end
  end
end
function GameArtifact.requestResetCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    end
    return true
  elseif msgType == NetAPIList.art_reset_ack.Code then
    if content.is_success == 1 then
      GameArtifact.currentAlldata = content.part_page_list
      GameArtifact.currentTab = 1
      GameArtifact:HideUpgradeAnims()
      GameArtifact:updateArtifactUI(content.part_page_list, false, false)
      GameArtifact:updateUpgradeCost(content.one_key_upgrade, content.upgrade)
    else
      local GameTip = LuaObjectManager:GetLuaObject("GameTip")
      local tip = "Reset Failed"
      GameTip:Show(tip)
    end
    return true
  end
  return false
end
function GameArtifact:requestPointInfo(pointId)
  local param = {}
  param.id = pointId
  NetMessageMgr:SendMsg(NetAPIList.art_point_req.Code, param, self.requestPointInfoCallback, true, nil)
end
function GameArtifact.requestPointInfoCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.art_point_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.art_point_ack.Code then
    GameArtifact:showPointUpgrade(content)
    return true
  end
  return false
end
function GameArtifact:requestUpgradePoint(pointId)
  local param = {}
  param.id = pointId
  NetMessageMgr:SendMsg(NetAPIList.art_point_upgrade_req.Code, param, self.requestUpgradePointCallback, true, nil)
end
function GameArtifact.requestUpgradePointCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.art_point_upgrade_req.Code then
    if content.code ~= 0 then
      if content.code == 49223 then
        local function callback()
          GameStateManager:GetCurrentGameState():EraseObject(GameFleetEquipment)
          local item = GameUILab:GetLibsItemByName("starwar")
          if item and item.count_down > 0 then
            GameStateManager:SetCurrentGameState(GameStateManager.GameStateMiningWorld)
            local GameUIMiningWorld = LuaObjectManager:GetLuaObject("GameUIMiningWorld")
            GameUIMiningWorld.enterDir = 1
            GameUIMiningWorld:Show()
          else
            GameStateManager.GameStateLab.mCurSubState = STATE_LAB_SUB_STATE.SUBSTATE_NORMAL
            GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
          end
        end
        local text = AlertDataList:GetTextFromErrorCode(content.code)
        GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.Green)
        local cancel = GameLoader:GetGameText("LC_MENU_BUTTON_CAPTION_CANCEL")
        local affairs = GameLoader:GetGameText("LC_MENU_GET_MORE_BUTTON")
        GameUIMessageDialog:SetRightTextButton(cancel)
        GameUIMessageDialog:SetLeftGreenButton(affairs, callback)
        GameUIMessageDialog:Display("", text)
      else
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
    end
    return true
  elseif msgType == NetAPIList.art_point_upgrade_ack.Code then
    if content.is_success == 1 then
      GameArtifact.currentAlldata = content.part_page_list
      local prevcore = {}
      for _, v in ipairs(content.part_page_list[GameArtifact.currentTab].point_list) do
        if GameArtifact.currentPoint == v.id then
          prevcore = v
          break
        end
      end
      local isNewUpdate = false
      if prevcore.level == prevcore.max_level or prevcore == 1 then
        isNewUpdate = true
      end
      DebugOut("puhy_art_point_upgrade_ack")
      local tab = GameArtifact.currentTab
      GameArtifact:updateArtifactUI(content.part_page_list, false, isNewUpdate)
      GameArtifact:updateUpgradeCost(content.one_key_upgrade, content.upgrade)
      GameArtifact:updatePointPop(prevcore)
      GameArtifact:playPointUpragdeAnims(GameArtifact.currentPoint, tab)
    else
      local GameTip = LuaObjectManager:GetLuaObject("GameTip")
      local tip = "upgrade failed"
      GameTip:Show(tip)
    end
    return true
  end
  return false
end
function GameArtifact:requestDownCore(coreId)
  local param = {}
  param.core_id = coreId
  param.fleet_id = self.currentFleetId
  DebugOut("requestDownCore:core_id" .. coreId)
  DebugTable(param)
  NetMessageMgr:SendMsg(NetAPIList.art_core_down_req.Code, param, self.requestDownCoreCallback, true, nil)
  GameFleetEquipment:RefreshKryptonRedPoint()
end
function GameArtifact.requestDownCoreCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.art_core_down_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.art_core_down_ack.Code then
    if content.is_success == 1 then
      DebugOut("requestDownCoreCallback")
      DebugTable(content)
      GameArtifact.currentAlldata = content.part_page_list
      GameArtifact:updateArtifactUI(content.part_page_list, false, false)
    else
      local GameTip = LuaObjectManager:GetLuaObject("GameTip")
      local tip = "down core failed"
      GameTip:Show(tip)
    end
    return true
  end
  return false
end
function GameArtifact:requestBreakCore(coreId)
  local param = {}
  param.core_id = coreId
  param.fleet_id = self.currentFleetId
  param.page = self.currentTab
  DebugOut("GameArtifact:requestBreakCore")
  DebugTable(param)
  NetMessageMgr:SendMsg(NetAPIList.art_core_decompose_req.Code, param, self.requestBreakCoreCallback, true, nil)
end
function GameArtifact.requestBreakCoreCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.art_core_decompose_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.art_core_decompose_ack.Code then
    if content.is_success == 1 then
      GameArtifact.coreItems = content.core_list
      GameArtifact:showCoreList(content.core_list)
    else
      local GameTip = LuaObjectManager:GetLuaObject("GameTip")
      local tip = "break core failed"
      GameTip:Show(tip)
    end
    return true
  end
  return false
end
function GameArtifact:requestEquipCore(coreId)
  local param = {}
  param.core_id = coreId
  param.fleet_id = self.currentFleetId
  param.page = self.currentTab
  NetMessageMgr:SendMsg(NetAPIList.art_core_equip_req.Code, param, self.requestEquipCoreCallback, true, nil)
  GameFleetEquipment:RefreshKryptonRedPoint()
end
function GameArtifact.requestEquipCoreCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.art_core_equip_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.art_core_equip_ack.Code then
    if content.is_success == 1 then
      GameArtifact.currentAlldata = content.part_page_list
      GameArtifact:updateArtifactUI(content.part_page_list, true, false)
    else
    end
    return true
  end
  return false
end
function GameArtifact:requestCoreList()
  local param = {}
  param.fleet_id = tonumber(self.currentFleetId)
  param.user_id = tonumber(GameGlobalData:GetUserInfo().player_id)
  param.page = tonumber(self.currentTab)
  DebugOut("MsgType: " .. NetAPIList.art_core_list_req.Code)
  DebugTable(param)
  NetMessageMgr:SendMsg(NetAPIList.art_core_list_req.Code, param, self.requestCoreListCallback, true, nil)
end
function GameArtifact.requestCoreListCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.art_core_list_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.art_core_list_ack.Code then
    DebugOut("GameArtifact.requestCoreListCallback:")
    DebugTable(content)
    GameArtifact:showCoreList(content.core_list)
    return true
  end
  return false
end
function GameArtifact:requestMainData(fleetId)
  DebugOut("GameArtifact:requestMainData")
  local param = {}
  param.id = fleetId
  DebugOut("userid:" .. GameGlobalData:GetUserInfo().player_id)
  param.user_id = tonumber(GameGlobalData:GetUserInfo().player_id)
  self.currentFleetId = fleetId
  self.currentTab = 1
  NetMessageMgr:SendMsg(NetAPIList.art_main_req.Code, param, self.requestMainDataCallback, true, nil)
end
function GameArtifact.requestMainDataCallback(msgType, content)
  DebugOut("GameArtifact:requestMainDataCallback")
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.art_main_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.art_main_ack.Code then
    DebugOut("GameArtifact:@@@@@@")
    GameArtifact:GetFlashObject():InvokeASCallback("_root", "loadFlashMoveIn")
    local txt1 = GameLoader:GetGameText("LC_MENU_ARTIFACT_TAG_1")
    local txt2 = GameLoader:GetGameText("LC_MENU_ARTIFACT_TAG_2")
    local txt3 = GameLoader:GetGameText("LC_MENU_ARTIFACT_TAG_3")
    GameArtifact:setTabtext(txt1, txt2, txt3)
    GameArtifact.resource = content.resource
    GameArtifact.resetCost = content.now_reset_cost
    GameArtifact:updateResAndResetCost(content.resource, content.now_reset_cost)
    GameArtifact:updatePerfectRest(content.perfect_reset_cost)
    GameArtifact.currentAlldata = content.part_page_list
    GameArtifact:Updatepower(GameArtifact.power)
    GameArtifact:updateArtifactUI(content.part_page_list, false, false)
    GameArtifact:updateUpgradeCost(content.one_key_upgrade, content.upgrade)
    return true
  end
  return false
end
function GameArtifact:updateUpgradeCost(oneKeyUpgradeItem, UpgradeItem)
  local data = {}
  data.oneKeyItem = {}
  data.upGradeItem = {}
  for k, v in ipairs(oneKeyUpgradeItem or {}) do
    local item = {}
    item.count = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
    item.displayCount = GameUtils.numberConversion(math.floor(item.count))
    item.frame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v)
    table.insert(data.oneKeyItem, item)
  end
  for k, v in ipairs(UpgradeItem or {}) do
    local item = {}
    item.count = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
    item.displayCount = GameUtils.numberConversion(math.floor(item.count))
    item.frame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v)
    table.insert(data.upGradeItem, item)
  end
  GameArtifact.curUpgrade = data
  GameArtifact:setUpgradeCost(self.currentTab)
end
function GameArtifact:updateNormalUpgradeCost(UpgradeItem)
  local data = {}
  for k, v in ipairs(UpgradeItem or {}) do
    local item = {}
    item.count = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
    item.displayCount = GameUtils.numberConversion(math.floor(item.count))
    item.frame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v)
    table.insert(data, item)
  end
  GameArtifact.curUpgrade.upGradeItem = data
  GameArtifact:setUpgradeCost(self.currentTab)
end
function GameArtifact:setUpgradeCost(tab)
  if GameArtifact.curUpgrade and GameArtifact:GetFlashObject() then
    GameArtifact:GetFlashObject():InvokeASCallback("_root", "setUpgradeRes", GameArtifact.curUpgrade, self.currentTab)
  end
end
function GameArtifact:updatePerfectRest(creditNum)
  GameArtifact.perfectResetCost = creditNum
  if GameArtifact:GetFlashObject() then
    GameArtifact:GetFlashObject():InvokeASCallback("_root", "setPerfectReset", creditNum)
  end
end
function GameArtifact:HideUpgradeAnims()
  if GameArtifact:GetFlashObject() then
    GameArtifact:GetFlashObject():InvokeASCallback("_root", "HideUpgradeAnims")
  end
end
function GameArtifact:OnNoramlUpgrade()
  local pointList = GameArtifact.currentAlldata[self.currentTab]
  for k, v in ipairs(pointList.point_list or {}) do
    if v.level < v.max_level then
      GameArtifact:OnFSCommand("upgrade_point", v.id)
      return
    end
  end
  GameTip:Show(GameLoader:GetGameText("LC_ALERT_artifact_upgrade_max"))
end
function GameArtifact:OnOneKeyUpgrade()
  local param = {}
  param.fleet_id = tonumber(self.currentFleetId)
  param.user_id = tonumber(GameGlobalData:GetUserInfo().player_id)
  param.page = tonumber(self.currentTab)
  NetMessageMgr:SendMsg(NetAPIList.art_one_upgrade_data_req.Code, param, self.reqOneKeyUpgradeDataCallBack, true, nil)
end
function GameArtifact.reqOneKeyUpgradeDataCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.art_one_upgrade_data_req.Code then
    if content.code ~= 0 then
      if content.code == 49223 then
        local function callback()
          GameStateManager:GetCurrentGameState():EraseObject(GameFleetEquipment)
          local item = GameUILab:GetLibsItemByName("starwar")
          if item and item.count_down == 1 then
            GameStateManager:SetCurrentGameState(GameStateManager.GameStateMiningWorld)
            local GameUIMiningWorld = LuaObjectManager:GetLuaObject("GameUIMiningWorld")
            GameUIMiningWorld.enterDir = 1
            GameUIMiningWorld:Show()
          else
            GameStateManager.GameStateLab.mCurSubState = STATE_LAB_SUB_STATE.SUBSTATE_NORMAL
            GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
          end
        end
        local text = AlertDataList:GetTextFromErrorCode(content.code)
        GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.Green)
        local cancel = GameLoader:GetGameText("LC_MENU_BUTTON_CAPTION_CANCEL")
        local affairs = GameLoader:GetGameText("LC_MENU_GET_MORE_BUTTON")
        GameUIMessageDialog:SetRightTextButton(cancel)
        GameUIMessageDialog:SetLeftGreenButton(affairs, callback)
        GameUIMessageDialog:Display("", text)
      else
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
    end
    return true
  elseif msgType == NetAPIList.art_one_upgrade_data_ack.Code then
    GameArtifact:GenerateOneKeyUpgradeData(content)
    GameArtifact:SetOneKeyUpgradeName(GameArtifact.oneKeyUpgradeData.dstLevel)
    GameArtifact:SetOneKeyUpgradeBuff(GameArtifact.oneKeyUpgradeData.curBuff)
    GameArtifact:SetOneKeyUpLimit(1, GameArtifact.oneKeyUpgradeData.maxLevel)
    GameArtifact:SetOneKeyUpBar(0)
    local dstItem = GameArtifact.oneKeyUpgradeData.dstBuff[GameArtifact.oneKeyUpgradeData.dstLevel]
    GameArtifact:SetOneKeyUpRes(dstItem.curCostCount, dstItem.displayCount, dstItem.frame)
    GameArtifact:SetOneKeyUpInputText(GameArtifact.oneKeyUpgradeData.dstLevel)
    GameArtifact:ShowOneKeyPop()
    return true
  end
  return false
end
function GameArtifact:ShowOneKeyPop()
  if GameArtifact:GetFlashObject() then
    GameStateEquipEnhance:GetPop():chuancang_ShowOneKeyPop()
  end
end
function GameArtifact:GenerateOneKeyUpgradeData(content)
  local data = {}
  data.maxLevel = content.max_level
  data.dstLevel = 1
  data.curBuff = {}
  data.dstBuff = {}
  for k, v in ipairs(content.cur_buff) do
    local item = {}
    item.buffid = v.buff_id
    item.buffnum = self:GetBuffNum(v.buff_id, v.buff_num)
    item.buffname = self:getBuffName(v.buff_id)
    table.insert(data.curBuff, item)
  end
  for k, v in ipairs(content.all_point_buff) do
    local buffList = {}
    buffList.curCostCount = GameHelper:GetAwardCount(v.need_res.item_type, v.need_res.number, v.need_res.no)
    buffList.displayCount = GameUtils.numberConversion(math.floor(buffList.curCostCount))
    buffList.frame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v.need_res)
    buffList.id = v.id
    buffList.level = v.level
    buffList.list = {}
    for k1, v1 in ipairs(v.next_buff) do
      local item = {}
      item.buffid = v1.buff_id
      item.buffnum = self:GetBuffNum(v1.buff_id, v1.buff_num)
      item.buffname = self:getBuffName(v1.buff_id)
      table.insert(buffList.list, item)
    end
    table.insert(data.dstBuff, buffList)
  end
  GameArtifact.oneKeyUpgradeData = data
end
function GameArtifact:SetOneKeyUpgradeName(dstLevel)
  local curName = ""
  local dstName = ""
  local pointList = GameArtifact.currentAlldata[self.currentTab]
  for k, v in ipairs(pointList.point_list or {}) do
    if v.level < v.max_level and curName == "" then
      curName = GameLoader:GetGameText("LC_MENU_ARTIFACT_NODE_NAME_" .. v.type) .. "  " .. GameLoader:GetGameText("LC_MENU_Level") .. v.level
    end
    dstLevel = dstLevel - (v.max_level - v.level)
    if dstLevel <= 0 then
      dstName = GameLoader:GetGameText("LC_MENU_ARTIFACT_NODE_NAME_" .. v.type) .. "  " .. GameLoader:GetGameText("LC_MENU_Level") .. v.max_level + dstLevel
      break
    end
  end
  if GameArtifact:GetFlashObject() then
    GameStateEquipEnhance:GetPop():chuancang_setOneKeyUpName(curName, dstName)
  end
end
function GameArtifact:SetOneKeyUpgradeBuff(curBuff)
  if GameArtifact:GetFlashObject() then
    GameStateEquipEnhance:GetPop():chuancang_setOneKeyUpBuff(#curBuff)
  end
end
function GameArtifact:OnUpdateOneKeyUpgradeItem(itemIndex)
  local item = GameArtifact.oneKeyUpgradeData.curBuff[itemIndex]
  if item then
    local dstItem = GameArtifact:GetDstBuffByID(GameArtifact.oneKeyUpgradeData.dstBuff[GameArtifact.oneKeyUpgradeData.dstLevel].list, item.buffid)
    local flashObj = GameArtifact:GetFlashObject()
    if flashObj then
      GameStateEquipEnhance:GetPop():chuancang_setOneKeyBuffItemData(item, dstItem, itemIndex)
    end
  end
end
function GameArtifact:GetDstBuffByID(list, id)
  for k, v in ipairs(list or {}) do
    if v.buffid == id then
      return v
    end
  end
  return nil
end
function GameArtifact:SetOneKeyUpBar(per)
  local flashObj = GameArtifact:GetFlashObject()
  if flashObj then
    GameStateEquipEnhance:GetPop():chuancang_setPopOneUpBar(per)
  end
end
function GameArtifact:SetOneKeyUpLimit(minNum, maxNum)
  local flashObj = GameArtifact:GetFlashObject()
  if flashObj then
    GameStateEquipEnhance:GetPop():chuancang_setPopOneUpLimit(minNum, maxNum)
  end
end
function GameArtifact:SetOneKeyUpRes(costCount, displayCount, frame)
  local flashObj = GameArtifact:GetFlashObject()
  if flashObj then
    GameStateEquipEnhance:GetPop():chuancang_setPopOneUpRes(costCount, displayCount, frame)
  end
end
function GameArtifact:SetOneKeyUpInputText(str)
  local flashObj = GameArtifact:GetFlashObject()
  if flashObj then
    GameStateEquipEnhance:GetPop():chuancang_setInputText(str)
  end
end
function GameArtifact:Update(dt)
  local flashObject = self:GetFlashObject()
  if flashObject then
    flashObject:InvokeASCallback("_root", "updateRollTextPos", dt)
    GameStateEquipEnhance:GetPop():chuancang_updateRollTextPos(dt)
    flashObject:Update(dt)
  end
  if self.UpdateCall then
    local callfunc = self.UpdateCall
    self.UpdateCall = nil
    callfunc()
  end
end
function GameArtifact:playPointUpragdeAnims(pointID, tab)
  local flashObj = GameArtifact:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "playUpgradeAnims", pointID, tab)
  end
end
function GameArtifact:OnOneKeyUpgradeReq(uplevel)
  local dstItem = GameArtifact.oneKeyUpgradeData.dstBuff[tonumber(uplevel)]
  local param = {}
  param.fleet_id = tonumber(self.currentFleetId)
  param.user_id = tonumber(GameGlobalData:GetUserInfo().player_id)
  param.page = tonumber(self.currentTab)
  param.level = dstItem.level
  param.id = dstItem.id
  NetMessageMgr:SendMsg(NetAPIList.art_one_upgrade_req.Code, param, self.reqOneKeyUpgradeCallBack, true, nil)
  GameFleetEquipment:RefreshKryptonRedPoint()
end
function GameArtifact.reqOneKeyUpgradeCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.art_one_upgrade_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.art_one_upgrade_ack.Code then
    GameArtifact.currentAlldata = content.part_page_list
    local tab = GameArtifact.currentTab
    GameArtifact:updateArtifactUI(content.part_page_list, false, false)
    for k, v in ipairs(content.point_list or {}) do
      GameArtifact:playPointUpragdeAnims(v.id, tab)
    end
    GameArtifact:updateUpgradeCost(content.one_key_upgrade, content.upgrade)
    return true
  end
  return false
end
function GameArtifact:OnFSCommand(cmd, arg)
  GameTextEdit:fscommand(self:GetFlashObject(), cmd, arg)
  if cmd == "init_chuancang" then
    DebugOut("init_chuancang")
  elseif cmd == "clickPoint" then
    self:clickPoint(arg)
  elseif cmd == "_showPointInfo" then
    self:_showPointInfo(arg)
  elseif cmd == "reset_free" then
    self:requestResetFree()
  elseif cmd == "reset_nofree" then
    self:requestResetNoFree()
  elseif cmd == "changeTab" then
    if arg == "tab_advanced" then
      self:showTabdata(1)
    elseif arg == "tab_hardcore" then
      self:showTabdata(2)
    elseif arg == "tab_surreal" then
      self:showTabdata(3)
    end
  elseif cmd == "tablocked" then
    local GameTip = LuaObjectManager:GetLuaObject("GameTip")
    local tip = ""
    if arg == "tab_hardcore" then
      tip = GameLoader:GetGameText("LC_MENU_ARTIFACT_UNLOCK_TIP_2")
    elseif arg == "tab_surreal" then
      tip = GameLoader:GetGameText("LC_MENU_ARTIFACT_UNLOCK_TIP_3")
    end
    GameTip:Show(tip)
  elseif cmd == "closeArtifact" then
    GameStateManager:GetCurrentGameState():EraseObject(GameFleetEquipment)
  elseif cmd == "req_point" then
    DebugOut("req_point: " .. arg)
    self.currentPoint = tonumber(arg)
    self:requestPointInfo(tonumber(arg))
  elseif cmd == "req_point_locked" then
    local GameTip = LuaObjectManager:GetLuaObject("GameTip")
    GameTip:Show(GameLoader:GetGameText("LC_MENU_ARTIFACT_LOCKED"))
    DebugOut("hello2")
  elseif cmd == "upgrade_point" then
    self.currentPoint = tonumber(arg)
    self:requestUpgradePoint(tonumber(arg))
  elseif cmd == "openCoreItem" then
    self:requestCoreList()
  elseif cmd == "core_down" then
    self:requestDownCore(tonumber(arg))
  elseif cmd == "core_break" then
    local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
    local function callback()
      self:requestBreakCore(tonumber(arg))
    end
    local coreId = tonumber(arg)
    local coreInfo = {}
    for _, core in ipairs(self.coreItems) do
      if core.id == coreId then
        coreInfo = core
        break
      end
    end
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
    GameUIMessageDialog:SetNoButton(nil)
    GameUIMessageDialog:SetYesButton(callback)
    GameUIMessageDialog:Display(text_title, string.gsub(GameLoader:GetGameText("LC_MENU_ARTIFACT_BREAK_COMFIRM"), "<number1>", tostring(coreInfo.sell_num)))
  elseif cmd == "core_equip" then
    self:requestEquipCore(tonumber(arg))
  elseif cmd == "core_info" then
    self:showCoreInfo(tonumber(arg))
  elseif cmd == "setCoreItem" then
    self:setCoreItem(tonumber(arg))
  elseif cmd == "showResetPop" then
    self:requestResetFree()
  elseif cmd == "help" then
    local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_MENU_ARTIFACT_HELP"))
  elseif cmd == "showPointInfo" then
    self:showPointInfo(tonumber(arg))
  elseif cmd == "notCorrectFinish" then
    self:GetFlashObject():InvokeASCallback("_root", "continuePlay", self.currentCore)
  elseif cmd == "normal_upgrade" then
    self:OnNoramlUpgrade()
  elseif cmd == "onekey_upgrade" then
    self:OnOneKeyUpgrade()
  elseif cmd == "one_key_upgrade" then
    local level = tonumber(arg)
    self:OnOneKeyUpgradeReq(level)
  elseif cmd == "perfect_rest" then
    GameArtifact:requestPerfectReset()
  elseif cmd == "reset_excute" then
    GameArtifact:ResetExcute()
  elseif cmd == "perfect_reset_excute" then
    GameArtifact:PerfectResetExcute()
  elseif cmd == "update_onekey_buff_item" then
    self:OnUpdateOneKeyUpgradeItem(tonumber(arg))
  elseif cmd == "update_onekey_bar" then
    local dstLevel = tonumber(arg)
    if GameArtifact.oneKeyUpgradeData.dstLevel ~= dstLevel then
      GameArtifact.oneKeyUpgradeData.dstLevel = dstLevel
      GameArtifact:SetOneKeyUpgradeName(dstLevel)
      local dstItem = GameArtifact.oneKeyUpgradeData.dstBuff[GameArtifact.oneKeyUpgradeData.dstLevel]
      GameArtifact:SetOneKeyUpRes(dstItem.curCostCount, dstItem.displayCount, dstItem.frame)
      local flashObj = GameArtifact:GetFlashObject()
      if flashObj then
        GameStateEquipEnhance:GetPop():chuancang_updateAllOneKeyUpBuffList()
      end
    end
  end
end
function GameArtifact:clickPoint(arg)
  GameStateEquipEnhance:GetPop():chuancang_clickPoint(arg)
end
function GameArtifact:_showPointInfo(arg)
  DebugOut("self.pt")
  DebugOut(arg)
  local param = LuaUtils:string_split(arg, "\001")
  self.pt = {}
  self.pt.x = param[1]
  self.pt.y = param[2]
  self.tip_width = param[3]
end
