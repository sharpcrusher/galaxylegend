local BundleIdentifier = "com.tap4fun.galaxyempire2_android"
local IOSOpenUdid = "win32_udid_01"
local UDID = "win32_udid_01"
local win32Gateway = {
  "a.portal-platform.tap4fun.com/gl/url_get/dev/dev1/"
}
local GameName = "GL"
local osVersion = "8.0"
local DeviceLanguage = "en"
local TimeZone = "Asia/Shanghai"
function ext.GetDeviceLanguage()
  return DeviceLanguage
end
function ext.GetIOSOpenUdid()
  return IOSOpenUdid
end
function ext.GetBundleIdentifier()
  return BundleIdentifier
end
function ext.GetTimeZone()
  return TimeZone
end
function ext.GetUDID()
  return UDID
end
function ext.GetGameName()
  return GameName
end
function ext.GetOSVersion()
  return osVersion
end
function ext.GetWin32Gateway()
  return win32Gateway
end
