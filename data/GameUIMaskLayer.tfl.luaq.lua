local GameStateGlobalState = GameStateManager.GameStateGlobalState
local GameUIMaskLayer = LuaObjectManager:GetLuaObject("GameUIMaskLayer")
GameUIMaskLayer.MaskStyle = {}
GameUIMaskLayer.MaskStyle.big = 1
GameUIMaskLayer.MaskStyle.small = 2
GameUIMaskLayer.MaskStyle.mid = 3
GameUIMaskLayer.TextPos = {}
GameUIMaskLayer.TextPos.upleft = 1
GameUIMaskLayer.TextPos.downleft = 2
GameUIMaskLayer.TextPos.upright = 3
GameUIMaskLayer.TextPos.downright = 4
GameUIMaskLayer.TextPos.mid = 5
GameUIMaskLayer.ButtonPos = {}
GameUIMaskLayer.ButtonPos.left = 1
GameUIMaskLayer.ButtonPos.right = 2
GameUIMaskLayer._isShow = false
GameUIMaskLayer._currentTutorial = ""
GameUIMaskLayer._screenSize = nil
function GameUIMaskLayer:OnFSCommand(cmd, arg)
  DebugOut("cmd = ", cmd)
  if cmd == "helptutorial" then
    self:SetIsShow(false)
    self:RemoveLayer()
  end
end
function GameUIMaskLayer:Display(x, y, ButtonPos, text)
  local flash_obj = self:GetFlashObject()
  if not GameStateGlobalState:IsObjectInState(GameUIMaskLayer) then
    GameStateGlobalState:AddObject(GameUIMaskLayer)
  end
  if self._screenSize == nil then
    self:GetScreenSize()
  end
  local TextPos = 0
  TextPos = GameUIMaskLayer:GetTextPos2(x, y)
  DebugOut("TextPos = " .. TextPos)
  flash_obj:InvokeASCallback("_root", "Display", x, y, TextPos, ButtonPos, text)
end
function GameUIMaskLayer:Reload()
  if self:GetFlashObject() then
    self:UnloadFlashObject()
  end
  self:LoadFlashObject()
end
function GameUIMaskLayer:SetMaskStyle(style)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "SetMaskStyle", style)
  end
end
function GameUIMaskLayer:SetIsShow(isShow)
  self._isShow = isShow
end
function GameUIMaskLayer:GetIsShow()
  return self._isShow
end
function GameUIMaskLayer:GetTextPos(x, y)
  if x >= 0 and x <= GameUtils:GetScreenWidth() / 2 and y >= 0 and y <= GameUtils:GetScreenHight() / 2 then
    return GameUIMaskLayer.TextPos.downright
  elseif x > GameUtils:GetScreenWidth() / 2 and x <= GameUtils:GetScreenWidth() and y >= 0 and y < GameUtils:GetScreenHight() / 2 then
    return GameUIMaskLayer.TextPos.downleft
  elseif x > GameUtils:GetScreenWidth() / 2 and x <= GameUtils:GetScreenWidth() and y >= GameUtils:GetScreenHight() / 2 and y < GameUtils:GetScreenHight() then
    return GameUIMaskLayer.TextPos.upleft
  else
    return GameUIMaskLayer.TextPos.upright
  end
end
function GameUIMaskLayer:GetTextPos2(x, y)
  if x >= 0 and x <= self:GetScreenWidth() / 2 and y >= 0 and y <= self:GetScreenHight() / 2 then
    return GameUIMaskLayer.TextPos.downright
  elseif x > self:GetScreenWidth() / 2 and x <= self:GetScreenWidth() and y >= 0 and y < self:GetScreenHight() / 2 then
    return GameUIMaskLayer.TextPos.downleft
  elseif x > self:GetScreenWidth() / 2 and x <= self:GetScreenWidth() and y >= self:GetScreenHight() / 2 and y < self:GetScreenHight() then
    return GameUIMaskLayer.TextPos.upleft
  else
    return GameUIMaskLayer.TextPos.upright
  end
end
function GameUIMaskLayer:RemoveLayer(...)
  GameStateGlobalState:EraseObject(GameUIMaskLayer)
  self:UnloadFlashObject()
end
function GameUIMaskLayer:SetCurrentTutorial(name)
  self._currentTutorial = name
end
function GameUIMaskLayer:GetCurrentTutorial(...)
  return self._currentTutorial
end
function GameUIMaskLayer:GetScreenSize(...)
  if self:GetFlashObject() then
    local screenSize = self:GetFlashObject():InvokeASCallback("_root", "GetScreenSize")
    if screenSize ~= "" then
      self._screenSize = LuaUtils:string_split(screenSize, "\001")
      DebugOut("_screenSize = ")
      DebugTable(self._screenSize)
    end
  end
  return nil
end
function GameUIMaskLayer:GetScreenWidth(...)
  if self._screenSize == nil then
    self:GetScreenSize()
  end
  return (...), self._screenSize[1]
end
function GameUIMaskLayer:GetScreenHight(...)
  if self._screenSize == nil then
    self:GetScreenSize()
  end
  return (...), self._screenSize[2]
end
if AutoUpdate.isAndroidDevice then
  function GameUIMaskLayer.OnAndroidBackCustom()
    GameUIMaskLayer:OnFSCommand("helptutorial")
  end
end
