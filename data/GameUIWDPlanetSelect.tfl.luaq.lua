local GameUIWDPlanetSelect = LuaObjectManager:GetLuaObject("GameUIWDPlanetSelect")
local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameUIActivityNew = LuaObjectManager:GetLuaObject("GameUIActivityNew")
local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
local GameStateWD = GameStateManager.GameStateWD
local GameUIFestival = LuaObjectManager:GetLuaObject("GameUICommonEvent")
GameUIWDPlanetSelect.activityList = nil
GameUIWDPlanetSelect.EnterEventDirectly = false
GameUIWDPlanetSelect.EventDetailFetched = false
GameUIWDPlanetSelect.FetchInProgress = false
GameUIWDPlanetSelect.EnterEventAfterFetched = false
GameUIWDPlanetSelect.fetchNtfForWD = false
GameUIWDPlanetSelect.waitFetchNtfForWD = false
GameUIWDPlanetSelect.planetClicked = false
GameUIWDPlanetSelect.isInBattle = false
EPlanetState = {
  State_NONE = 0,
  State_NotStart = 1,
  State_FetchInfoForStart = 2,
  State_WaitFetch = 3,
  State_CheckBattleInfo = 4,
  State_WaitBattleInfo = 5,
  State_EnterWD = 6
}
local title1to9 = ""
local title10to18 = ""
local currentGalaxyIndex
GameUIWDPlanetSelect.curPlanetState = EPlanetState.State_NONE
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
function GameUIWDPlanetSelect:OnInitGame()
  DebugWD("GameUIWDPlanetSelect:OnInitGame()")
end
function GameUIWDPlanetSelect:Init()
  self:InitCenterPos(1)
end
function GameUIWDPlanetSelect:OnAddToGameState()
  DebugWD("GameUIWDPlanetSelect:OnAddToGameState()")
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  if FlashMemTracker then
    FlashMemTracker.DumpAllFlashMemstate()
  end
  GameUIWDPlanetSelect:CheckDownloadImage()
  self:CheckDonwloadListState()
  self:InitPlanetGalaxySelect()
  self:MoveIn()
  self:SetAllianceState()
  self.curPlanetState = EPlanetState.State_NONE
  DebugOut("self.isInBattle", self.isInBattle)
  if self.isInBattle then
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_hideAllianceBtnAndEventListBtn")
  end
  if not self.EventDetailFetched and not self.FetchInProgress then
    self:TryToFetchWDEvent()
  end
end
function GameUIWDPlanetSelect:OnEraseFromGameState()
  DebugOut("GameUIWDPlanetSelect OnEraseFromGameState")
  self:UnloadFlashObject()
  collectgarbage("collect")
  self.FetchInProgress = false
end
function GameUIWDPlanetSelect:CheckDownloadImage()
  if (ext.crc32.crc32("data2/LAZY_LOAD_map_star_01.png") == "" or ext.crc32.crc32("data2/LAZY_LOAD_map_star_01.png") == nil) and not AutoUpdateInBackground:IsHeroFileUpdatedToServer("data2/LAZY_LOAD_map_star_01.png") then
    self:GetFlashObject():ReplaceTexture("LAZY_LOAD_map_star_01.png", "territorial_map_bg.png")
  end
end
function GameUIWDPlanetSelect:CheckDonwloadListState()
  local galaxyMc1Visible = true
  local galaxyMc2Visible = true
  local galaxyMc3Visible = true
  local galaxyMc4Visible = true
  local galaxyMc5Visible = true
  if (ext.crc32.crc32("data2/LAZY_LOAD_wd_chart_galaxy.png") == "" or ext.crc32.crc32("data2/LAZY_LOAD_wd_chart_galaxy.png") == nil) and not AutoUpdateInBackground:IsHeroFileUpdatedToServer("data2/LAZY_LOAD_wd_chart_galaxy.png") then
    galaxyMc1Visible = false
  end
  if (ext.crc32.crc32("data2/LAZY_LOAD_wd_star1.png") == "" or ext.crc32.crc32("data2/LAZY_LOAD_wd_star1.png") == nil) and not AutoUpdateInBackground:IsHeroFileUpdatedToServer("data2/LAZY_LOAD_wd_star1.png") then
    galaxyMc2Visible = false
    galaxyMc4Visible = false
  end
  if (ext.crc32.crc32("data2/LAZY_LOAD_wd_star2.png") == "" or ext.crc32.crc32("data2/LAZY_LOAD_wd_star2.png") == nil) and not AutoUpdateInBackground:IsHeroFileUpdatedToServer("data2/LAZY_LOAD_wd_star2.png") then
    galaxyMc3Visible = false
  end
  if (ext.crc32.crc32("data2/LAZY_LOAD_wd_star4.png") == "" or ext.crc32.crc32("data2/LAZY_LOAD_wd_star4.png") == nil) and not AutoUpdateInBackground:IsHeroFileUpdatedToServer("data2/LAZY_LOAD_wd_star4.png") then
    galaxyMc5Visible = false
  end
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "UpdateDownloadImage", galaxyMc1Visible, galaxyMc2Visible, galaxyMc3Visible, galaxyMc4Visible, galaxyMc5Visible)
end
function GameUIWDPlanetSelect:RefreshResource()
  local resource = GameGlobalData:GetData("resource")
end
function GameUIWDPlanetSelect:MoveIn()
  DebugWD("GameUIWDPlanetSelect:MoveIn()")
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "animationMoveIn")
end
function GameUIWDPlanetSelect:MoveOut()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "animationMoveOut")
end
function GameUIWDPlanetSelect:TryToFetchWDEvent()
  DebugOut("TryToFetchWDEvent")
  NetMessageMgr:SendMsg(NetAPIList.festival_req.Code, nil, self.QueryFestivalOffOnCallback, true, nil)
  self.FetchInProgress = true
end
function GameUIWDPlanetSelect:Update(dt)
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "OnUpdate")
  flash_obj:Update(dt)
  flash_obj:InvokeASCallback("_root", "onUpdateFrame", dt)
  self:UpdateCDTime()
  self:OnUpdateState()
  self:UpdateGalaxyCDTime()
end
function GameUIWDPlanetSelect:SetEventListItem(itemKey)
  local item = GameUIWDPlanetSelect.activityList[itemKey]
  DebugWD("SetEventListItem")
  DebugWDTable(item)
  local status = 0
  if item.current_time < item.started_at then
    status = 0
  elseif item.current_time >= item.started_at and item.current_time < item.end_at then
    status = 1
  elseif item.current_time >= item.end_at then
    status = 2
  end
  local btnText = GameLoader:GetGameText("LC_MENU_WD_LIST_GO_BUTTON")
  local waitingText = GameLoader:GetGameText("LC_MENU_WD_LIST_WAITING_BUTTON")
  GameUIWDPlanetSelect:GetFlashObject():InvokeASCallback("_root", "SetEventListItem", itemKey, item.date_range, item.title, status, itemKey, btnText, waitingText)
  local url = item.image_path
  if item.small_image_path and item.small_image_path ~= "" then
    url = item.small_image_path
  end
  if not string.find(url, "http:") and not string.find(url, "https:") then
    url = AutoUpdate.gameGateway .. url
  end
  local pngName = string.match(url, ".+/([^/]*%w+)$")
  local path = pngName
  path = "data2/" .. path
  DebugWD(url)
  DebugWD(pngName)
  if GameSetting:IsDownloadFileExsit(path) then
    DebugOut("pngName", pngName)
    if self:GetFlashObject() then
      ext.UpdateAutoUpdateFile(path, 0, 0, 0)
      self:GetFlashObject():ReplaceTexture("NEWS0" .. itemKey .. ".png", pngName)
      DebugOut("ReplaceTexture", "NEWS0" .. itemKey .. ".png", pngName)
    end
  else
    ext.http.requestDownload({
      url,
      method = "GET",
      localpath = path,
      callback = function(statusCode, content, errstr)
        if errstr ~= nil or statusCode ~= 200 then
          return
        end
        ext.UpdateAutoUpdateFile(content, 0, 0, 0)
        if GameUIWDPlanetSelect:GetFlashObject() then
          GameUIWDPlanetSelect:GetFlashObject():ReplaceTexture("NEWS0" .. itemKey .. ".png", pngName)
        end
        GameSetting:SaveDownloadFileName(path)
      end
    })
  end
end
function GameUIWDPlanetSelect:SetHistoryListItem(itemKey)
  local item = GameStateWD.wdList[itemKey]
  DebugWD("SetHistoryListItem")
  DebugWDTable(item)
  local status = 0
  if item.left_time == 0 then
    status = 2
  end
  local unoccupyStr = GameLoader:GetGameText("LC_MENU_WD_HISTORY_UNOCCUPY_TEXT")
  local allianceLeaderTitleStr = GameLoader:GetGameText("LC_MENU_CONTENT_ALLIANCE_POSITION_CHAIRMAN")
  allianceLeaderTitleStr = allianceLeaderTitleStr .. ":"
  local fightName = GameLoader:GetGameText(string.upper("LC_MENU_WD_BATTLE_NAME_" .. item.fight_name))
  GameUIWDPlanetSelect:GetFlashObject():InvokeASCallback("_root", "SetHistoryListItem", itemKey, fightName, unoccupyStr, allianceLeaderTitleStr, item.alliance_name, GameUtils:GetUserDisplayName(item.alliance_leader), GameStateWD:GetIconFrame(item), GameUtils:GetFlagFrameName(item.flag), status)
end
function GameUIWDPlanetSelect:GetActivityList()
  self:RequestActivity()
end
function GameUIWDPlanetSelect:RefreshEventList()
  if GameUIWDPlanetSelect.activityList == nil then
    GameUIWDPlanetSelect:GetFlashObject():InvokeASCallback("_root", "clearEventListItem")
    GameUIWDPlanetSelect:GetActivityList()
  elseif GameUIWDPlanetSelect.activityList ~= nil then
    local flash = GameUIWDPlanetSelect:GetFlashObject()
    if flash then
      flash:InvokeASCallback("_root", "clearEventListItem")
      flash:InvokeASCallback("_root", "initEventList")
      for i = 1, #GameUIWDPlanetSelect.activityList do
        flash:InvokeASCallback("_root", "addEventListItem", i)
      end
      flash:InvokeASCallback("_root", "SetArrowInitVisible")
    end
  end
end
function GameUIWDPlanetSelect:RefreshHistoryList()
  local flash = GameUIWDPlanetSelect:GetFlashObject()
  if flash then
    flash:InvokeASCallback("_root", "clearHistoryListItem")
    flash:InvokeASCallback("_root", "initHistoryList")
    DebugWD("logout history!!!!!!!", #GameStateWD.wdList)
    for i = 1, #GameStateWD.wdList do
      flash:InvokeASCallback("_root", "addHistoryListItem", i)
    end
    flash:InvokeASCallback("_root", "SetArrowInitVisibleHistoryList")
  end
end
function GameUIWDPlanetSelect:OnFSCommand(cmd, arg)
  DebugWD("planet select command: ", cmd, arg)
  local ballNumber = #GameUIWDPlanetSelect.BattleList.Item
  if cmd == "BuyReleased" then
    self:RequestBuyItem()
  elseif cmd == "gotoActivity" then
    local activityIndex = tonumber(arg)
    local currentActivity = GameUIWDPlanetSelect.activityList[activityIndex]
    if not currentActivity then
      DebugOut("error activityIndex", activityIndex)
      return
    end
    DebugOut("currentActivity type:", currentActivity.is_wd)
    if currentActivity.is_wd == GameUIActivityNew.activityType.WDandNormal or currentActivity.is_wd == GameUIActivityNew.activityType.SuperAndWD or currentActivity.is_wd == GameUIActivityNew.activityType.NormalAndWD then
      GameStateManager:GetCurrentGameState():AddObject(GameUIActivityNew)
    elseif currentActivity.is_wd == GameUIActivityNew.activityType.WD then
    end
  elseif cmd == "boxHiden" then
    GameStateManager:SetCurrentGameState(GameStateWD:GetPreState())
  elseif cmd == "Alliance_released" then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateAlliance)
  elseif cmd == "needUpdateEventItem" then
    self:SetEventListItem(tonumber(arg))
  elseif cmd == "needUpdateHistoryItem" then
    self:SetHistoryListItem(tonumber(arg))
  elseif cmd == "Eventlist_released" then
    local flash_obj = self:GetFlashObject()
    if flash_obj then
      if GameUIWDPlanetSelect.activityList and #GameUIWDPlanetSelect.activityList == 0 then
        GameUIWDPlanetSelect.activityList = nil
      end
      self:RefreshEventList()
      flash_obj:InvokeASCallback("_root", "showEventList", GameLoader:GetGameText("LC_MENU_WD_LIST_TITLE"))
      GameUIWDPlanetSelect.IsShowEventListWindow = true
    end
  elseif cmd == "History_released" then
    local flash_obj = self:GetFlashObject()
    if flash_obj then
      self:RefreshHistoryList()
      flash_obj:InvokeASCallback("_root", "showHistory")
    end
  elseif cmd == "need_scroll_left" then
  elseif cmd == "need_scroll_right" then
  elseif cmd == "roll_finished" then
    self.InRollAction = false
    self:SetInvisiblePlanetIcon()
    self:SetPlanetInfo()
    self:SetBattleUIInfo()
  elseif cmd == "Help_released" then
    local flash_obj = self:GetFlashObject()
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "setHelpText", GameLoader:GetGameText("LC_MENU_WD_HELP"))
      flash_obj:InvokeASCallback("_root", "showHelp")
      GameUIWDPlanetSelect.IsShowHelpWindow = true
    end
  elseif cmd == "EventList_close_released" then
    local flash_obj = self:GetFlashObject()
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "hideEventList")
      GameUIWDPlanetSelect.IsShowEventListWindow = false
    end
  elseif cmd == "History_close_released" then
    local flash_obj = self:GetFlashObject()
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "hideHistory")
    end
  elseif cmd == "planetIcon_released" then
    local index = tonumber(arg)
    DebugOut("big index = ", index)
    self:InitCenterPos(index)
    self:SetAllianceState()
    self.planetClicked = true
    self:MoveOut()
  elseif cmd == "GalaxyMoveOutFinished" then
    if self.planetClicked then
      self.planetClicked = false
      local flash_obj = self:GetFlashObject()
      if flash_obj then
        flash_obj:InvokeASCallback("_root", "dominationMoveIn")
        GameUIWDPlanetSelect.IsShowDominationWindow = true
      end
    elseif self.isInBattle then
      GameStateWD:closeHistoryInfoWhenBattle()
    else
      self:OnFSCommand("boxHiden")
    end
  elseif cmd == "DominationMoveoutFinished" then
    self:InitPlanetGalaxySelect()
    self:MoveIn()
    self.IsShowDominationWindow = false
  elseif cmd == "arrowLeftClicked" then
    currentGalaxyIndex = currentGalaxyIndex - 1
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_checkToGalaxyByIndex", currentGalaxyIndex, self:GetTitleText(ballNumber, currentGalaxyIndex))
  elseif cmd == "arrowRightClicked" then
    currentGalaxyIndex = currentGalaxyIndex + 1
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_checkToGalaxyByIndex", currentGalaxyIndex, self:GetTitleText(ballNumber, currentGalaxyIndex))
  elseif cmd == "close_help_window" then
    GameUIWDPlanetSelect.IsShowHelpWindow = false
  elseif cmd == "nextAnimOver" or cmd == "prevAnimOver" then
    GameUIWDPlanetSelect:RecordStarsPos(cmd)
    GameUIWDPlanetSelect:SetPlanetGalaxyInfo()
  end
end
GameUIWDPlanetSelect.BattleList = {}
GameUIWDPlanetSelect.BattleList.Item = {}
GameUIWDPlanetSelect.BattleList.curCenterIndex = 1
GameUIWDPlanetSelect.InRollAction = false
function GameUIWDPlanetSelect:InitCenterPos(pos)
  DebugOut("InitCenterPos:", #GameUIWDPlanetSelect.BattleList.Item, ",", pos)
  if pos < 1 or pos > #GameUIWDPlanetSelect.BattleList.Item then
    pos = 1
  end
  GameUIWDPlanetSelect.BattleList.curCenterIndex = pos
  DebugTable(GameUIWDPlanetSelect.BattleList.Item[pos])
  self:InitPlanetIcons()
  self:SetPlanetInfo()
  self:SetBattleUIInfo()
end
function GameUIWDPlanetSelect:SetInvisiblePlanetIcon()
  local leftbehindItem = self:GetLeftBehindItem()
  local rightbehindItem = self:GetRightBehindItem()
  local leftbehindFrame = "unknown"
  local rightbehindFrame = "unknown"
  if leftbehindItem ~= nil then
    leftbehindFrame = GameStateWD:GetIconFrame(leftbehindItem)
  end
  if rightbehindItem ~= nil then
    rightbehindFrame = GameStateWD:GetIconFrame(rightbehindItem)
  end
  DebugWD("SetInvisiblePlanetIcon")
  DebugWD(leftbehindFrame)
  DebugWD(rightbehindFrame)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "setInvisiblePlanetIcon", leftbehindFrame, rightbehindFrame)
  end
end
function GameUIWDPlanetSelect:ScrollLeft()
  GameUIWDPlanetSelect.BattleList.curCenterIndex = GameUIWDPlanetSelect.BattleList.curCenterIndex + 1
  if GameUIWDPlanetSelect.BattleList.curCenterIndex > #GameUIWDPlanetSelect.BattleList.Item then
    GameUIWDPlanetSelect.BattleList.curCenterIndex = 1
  end
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "scrollLeft", centerItem, leftItem, rightItem, leftBehindItem, rightBehindItem)
    self.InRollAction = true
  end
end
function GameUIWDPlanetSelect:ScrollRight()
  GameUIWDPlanetSelect.BattleList.curCenterIndex = GameUIWDPlanetSelect.BattleList.curCenterIndex - 1
  if GameUIWDPlanetSelect.BattleList.curCenterIndex < 1 then
    GameUIWDPlanetSelect.BattleList.curCenterIndex = #GameUIWDPlanetSelect.BattleList.Item
  end
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "scrollRight", centerItem, leftItem, rightItem, leftBehindItem, rightBehindItem)
    self.InRollAction = true
  end
end
function GameUIWDPlanetSelect:GetCenterItem()
  return GameUIWDPlanetSelect.BattleList.Item[GameUIWDPlanetSelect.BattleList.curCenterIndex]
end
function GameUIWDPlanetSelect:GetLeftItem()
  local index = GameUIWDPlanetSelect.BattleList.curCenterIndex - 1
  if index < 1 then
    index = #GameUIWDPlanetSelect.BattleList.Item
  end
  return GameUIWDPlanetSelect.BattleList.Item[index]
end
function GameUIWDPlanetSelect:GetRightItem()
  local index = GameUIWDPlanetSelect.BattleList.curCenterIndex + 1
  if index > #GameUIWDPlanetSelect.BattleList.Item then
    index = 1
  end
  return GameUIWDPlanetSelect.BattleList.Item[index]
end
function GameUIWDPlanetSelect:GetLeftBehindItem()
  local index = GameUIWDPlanetSelect.BattleList.curCenterIndex - 2
  if index == 0 then
    index = #GameUIWDPlanetSelect.BattleList.Item
  elseif index == -1 then
    index = #GameUIWDPlanetSelect.BattleList.Item - 1
  end
  return GameUIWDPlanetSelect.BattleList.Item[index]
end
function GameUIWDPlanetSelect:GetRightBehindItem()
  local index = GameUIWDPlanetSelect.BattleList.curCenterIndex + 2
  if index - #GameUIWDPlanetSelect.BattleList.Item > 0 then
    index = index - #GameUIWDPlanetSelect.BattleList.Item
  end
  return GameUIWDPlanetSelect.BattleList.Item[index]
end
function GameUIWDPlanetSelect:InitPlanetIcons()
  local centerItemFrame = "unknown"
  if self:GetCenterItem() ~= nil then
    centerItemFrame = GameStateWD:GetIconFrame(self:GetCenterItem())
  end
  local leftItemFrame = "unknown"
  if self:GetLeftItem() ~= nil then
    leftItemFrame = GameStateWD:GetIconFrame(self:GetLeftItem())
  end
  local rightItemFrame = "unknown"
  if self:GetRightItem() ~= nil then
    rightItemFrame = GameStateWD:GetIconFrame(self:GetRightItem())
  end
  local leftBehindItemFrame = "unknown"
  if self:GetLeftBehindItem() ~= nil then
    leftBehindItemFrame = GameStateWD:GetIconFrame(self:GetLeftBehindItem())
  end
  local rightBehindItemFrame = "unknown"
  if self:GetRightBehindItem() ~= nil then
    rightBehindItemFrame = GameStateWD:GetIconFrame(self:GetRightBehindItem())
  end
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    DebugWD("initPlanetIcons", centerItemFrame, leftItemFrame, rightItemFrame, leftBehindItemFrame, rightBehindItemFrame)
    flash_obj:InvokeASCallback("_root", "initPlanetIcons", centerItemFrame, leftItemFrame, rightItemFrame, leftBehindItemFrame, rightBehindItemFrame)
  end
  self.InRollAction = false
end
function GameUIWDPlanetSelect:SetPlanetInfo()
  local flash_obj = self:GetFlashObject()
  local centerItem = self:GetCenterItem()
  if flash_obj and centerItem then
    local timeString = ""
    if not centerItem.left_time then
      flash_obj:InvokeASCallback("_root", "setBattleStateArea", 0, "", "", "", GameUtils:GetFlagFrameName(0))
    elseif centerItem.left_time < 0 then
      local _lefttime = math.abs(centerItem.left_time) - (os.time() - GameStateWD.fetchWDListTime)
      if _lefttime < 0 then
        _lefttime = 0
      end
      if _lefttime > 2592000 then
        timeString = ""
      else
        timeString = GameUtils:formatTimeString(_lefttime)
      end
      flash_obj:InvokeASCallback("_root", "setBattleStateArea", 1, timeString, "", "", GameUtils:GetFlagFrameName(0))
      if _lefttime == 0 then
        GameStateWD:ChangeMainWin()
      end
    elseif centerItem.left_time > 0 then
      local _lefttime = centerItem.left_time - (os.time() - GameStateWD.fetchWDListTime)
      if _lefttime < 0 then
        _lefttime = 0
      end
      timeString = GameUtils:formatTimeString(_lefttime)
      flash_obj:InvokeASCallback("_root", "setBattleStateArea", 2, timeString, "", "", GameUtils:GetFlagFrameName(0))
    elseif centerItem.left_time == 0 then
      flash_obj:InvokeASCallback("_root", "setBattleStateArea", 3, "", centerItem.alliance_name, GameUtils:GetUserDisplayName(centerItem.alliance_leader), GameUtils:GetFlagFrameName(centerItem.flag))
    end
  end
end
function GameUIWDPlanetSelect:UpdateCDTime()
  local timeString = ""
  local _lefttime = 0
  local centerItem = self:GetCenterItem()
  if centerItem and centerItem.left_time then
    _lefttime = math.abs(centerItem.left_time) - (os.time() - GameStateWD.fetchWDListTime)
    if _lefttime < 0 then
      _lefttime = 0
    end
    if _lefttime > 2592000 and 0 > centerItem.left_time then
      timeString = ""
    else
      timeString = GameUtils:formatTimeStringAsTwitterStyle(_lefttime)
    end
    local flash = self:GetFlashObject()
    if flash then
      flash:InvokeASCallback("_root", "setCDTime", timeString)
    end
    if _lefttime == 0 and 0 > centerItem.left_time and self.curPlanetState == EPlanetState.State_NONE then
      self:GotoState(EPlanetState.State_FetchInfoForStart)
    end
  else
    local flash = self:GetFlashObject()
    if flash then
      flash:InvokeASCallback("_root", "setCDTime", "")
    end
  end
end
function GameUIWDPlanetSelect:OnUpdateState()
  if self.curPlanetState == EPlanetState.State_WaitFetch then
    if GameStateManager.GameStateWD.AllDataFetched() or not GameStateWD.HaveAlliance() then
      GameWaiting:HideLoadingScreen()
      self:GotoState(EPlanetState.State_CheckBattleInfo)
    end
  elseif self.curPlanetState == EPlanetState.State_WaitBattleInfo then
    local opponentFetched = true
    if GameStateWD.WDBaseInfo.State == GameStateWD.WDState.BATTLE then
      if GameStateWD.OpponentDataFetched() then
        opponentFetched = true
      else
        opponentFetched = false
      end
    end
    if GameStateWD.AllDataFetched() and opponentFetched then
      GameWaiting:HideLoadingScreen()
      self:GotoState(EPlanetState.State_EnterWD)
    end
  end
end
function GameUIWDPlanetSelect:OnEnterState(state)
  if state == EPlanetState.State_FetchInfoForStart then
    local playerList = {}
    local requestParam = {notifies = playerList}
    NetMessageMgr:SendMsg(NetAPIList.alliance_notifies_req.Code, requestParam, GameUIWDPlanetSelect.fetchNtfForWDCallback, true, nil)
    GameWaiting:ShowLoadingScreen()
  elseif state == EPlanetState.State_CheckBattleInfo then
    if GameStateManager.GameStateWD.wdList ~= nil and (GameStateWD.HaveAlliance() and GameStateWD.AllDataFetched() or not GameStateManager.GameStateWD.HaveAlliance()) then
      if GameStateWD.WDBaseInfo.State == GameStateWD.WDState.BATTLE and not GameStateWD.OpponentDataFetched() then
        GameWaiting:ShowLoadingScreen()
        self:GotoState(EPlanetState.State_WaitBattleInfo)
      else
        self:GotoState(EPlanetState.State_EnterWD)
      end
    end
  elseif state == EPlanetState.State_EnterWD then
    self.curPlanetState = EPlanetState.State_NONE
    GameStateWD:ChangeMainWin()
  end
end
function GameUIWDPlanetSelect:OnLeaveState(state)
end
function GameUIWDPlanetSelect:GotoState(state)
  if state == self.curPlanetState then
    return
  end
  self:OnLeaveState(self.curPlanetState)
  self.curPlanetState = state
  self:OnEnterState(self.curPlanetState)
end
function GameUIWDPlanetSelect:SetBattleUIInfo()
  local item = self:GetCenterItem()
  local title = GameLoader:GetGameText("LC_MENU_WD_TITLE")
  local subTitle = GameLoader:GetGameText("LC_MENU_WD_LEBAL")
  subTitle = string.format(subTitle, item.id)
  if item.id == -1 then
    subTitle = GameLoader:GetGameText("LC_MENU_WD_LIST_WAITING_BUTTON")
  end
  local battleName = GameLoader:GetGameText(string.upper("LC_MENU_WD_BATTLE_NAME_" .. item.fight_name))
  local battleDesc = GameLoader:GetGameText(string.upper("LC_MENU_WD_BATTLE_DESC_" .. item.fight_name))
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "setBattleUIInfo", title, subTitle, battleName, battleDesc)
  end
end
function GameUIWDPlanetSelect:SetAllianceState()
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "setAllianceState", GameStateWD:IfAllianceEnabled())
  end
end
function GameUIWDPlanetSelect:RequestActivity()
  local requestType = "servers/" .. GameUtils:GetActiveServerInfo().id .. "/server_activity"
  local requestParam = {
    lang = GameSettingData.Save_Lang
  }
  GameUtils:HTTP_SendRequest(requestType, requestParam, GameUIWDPlanetSelect.RequestActivityCallback, true, GameUtils.httpRequestFailedCallback, "GET", "notencrypt")
end
function GameUIWDPlanetSelect.RequestActivityCallback(responseContent)
  DebugWD("RequestActivityCallback")
  DebugWDTable(responseContent)
  GameUIWDPlanetSelect.activityList = {}
  if responseContent ~= nil then
    local index = 1
    for i = 1, #responseContent do
      if responseContent[i].is_wd == GameUIActivityNew.activityType.WD or responseContent[i].is_wd == GameUIActivityNew.activityType.WDandNormal or responseContent[i].is_wd == GameUIActivityNew.activityType.SuperAndWD or responseContent[i].is_wd == GameUIActivityNew.activityType.NormalAndWD then
        DebugWD("GetActivityList")
        DebugWDTable(responseContent[i])
        GameUIWDPlanetSelect.activityList[index] = LuaUtils:table_rcopy(responseContent[i])
        index = index + 1
      end
    end
  end
  GameUIWDPlanetSelect:RefreshEventList()
  return true
end
function GameUIWDPlanetSelect.QueryFestivalOffOnCallback(msgType, content)
  if msgType == NetAPIList.festival_ack.Code then
    do
      local avaliableFestivalInfo
      DebugOut("QueryFestivalOffOnCallback")
      DebugTable(content)
      for i, v in ipairs(content.festivals) do
        if v.current_time >= v.start_time and v.current_time <= v.final_time and v.activity_type == 3 then
          DebugOut("server time")
          DebugTable(v)
          avaliableFestivalInfo = v
          break
        end
      end
      if avaliableFestivalInfo ~= nil then
        local festivalUrl = AutoUpdate.gameGateway .. "activities/wd_event?lang=" .. GameSettingData.Save_Lang
        ext.http.request({
          festivalUrl,
          mode = "notencrypt",
          callback = function(statusCode, webContent, errstr)
            GameUIWDPlanetSelect.FetchInProgress = false
            if errstr ~= nil or statusCode ~= 200 then
              GameUIWDPlanetSelect.EventDetailFetched = false
              return
            end
            DebugOut("web content:")
            DebugTable(webContent)
            local festivalDetailDatasetItem = {}
            festivalDetailDatasetItem.detailType = GameUIFestival.DetailType.WD_EVENT
            festivalDetailDatasetItem.timeInfo = LuaUtils:table_rcopy(avaliableFestivalInfo)
            festivalDetailDatasetItem.receiveInfoTime = os.time()
            festivalDetailDatasetItem.details = LuaUtils:table_rcopy(webContent)
            table.insert(GameUIFestival.festivalDetailDataset, festivalDetailDatasetItem)
            GameUIWDPlanetSelect.EventDetailFetched = true
            if GameUIWDPlanetSelect.EnterEventAfterFetched then
              GameUIWDPlanetSelect.EnterEventAfterFetched = false
              GameWaiting:HideLoadingScreen()
              GameUIFestival:ShowEvent(GameUIFestival.DetailType.WD_EVENT)
            end
          end
        })
      else
        DebugOut("no available activity!")
      end
      return true
    end
  end
  return false
end
function GameUIWDPlanetSelect.fetchNtfForWDCallback(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.alliance_notifies_req.Code then
    GameUIWDPlanetSelect.GotoState(GameUIWDPlanetSelect, EPlanetState.State_WaitFetch)
    return true
  end
  return false
end
function GameUIWDPlanetSelect:RefreshTitle(ballNumber)
  DebugOut("RefreshTitle")
  if ballNumber <= 9 then
    title1to9 = GameLoader:GetGameText("LC_MENU_WD_SESSION")
    title1to9 = string.gsub(title1to9, "<start_no>", 1)
    title1to9 = string.gsub(title1to9, "<end_no>", ballNumber)
    title10to18 = ""
  elseif ballNumber == 10 then
    title1to9 = GameLoader:GetGameText("LC_MENU_WD_SESSION")
    title1to9 = string.gsub(title1to9, "<start_no>", 1)
    title1to9 = string.gsub(title1to9, "<end_no>", 9)
    title10to18 = GameLoader:GetGameText("LC_MENU_WD_SESSION")
    title10to18 = string.gsub(title10to18, "<start_no>", 10)
    title10to18 = string.gsub(title10to18, "-", "")
    title10to18 = string.gsub(title10to18, "<end_no>", "")
  elseif ballNumber > 10 then
    title1to9 = GameLoader:GetGameText("LC_MENU_WD_SESSION")
    title1to9 = string.gsub(title1to9, "<start_no>", 1)
    title1to9 = string.gsub(title1to9, "<end_no>", 9)
    title10to18 = GameLoader:GetGameText("LC_MENU_WD_SESSION")
    title10to18 = string.gsub(title10to18, "<start_no>", 10)
    title10to18 = string.gsub(title10to18, "<end_no>", ballNumber)
  end
  DebugOut(title1to9)
  DebugOut(title10to18)
end
function GameUIWDPlanetSelect:GetTitleText(ballNumber, pageIndex)
  DebugOut("GetTitleText:", ballNumber, ",", pageIndex)
  pageIndex = pageIndex - 1
  local pageNum = math.floor(ballNumber / 9)
  local titleText = GameLoader:GetGameText("LC_MENU_WD_SESSION")
  if pageIndex < pageNum then
    titleText = string.gsub(titleText, "<start_no>", pageIndex * 9 + 1)
    titleText = string.gsub(titleText, "<end_no>", (pageIndex + 1) * 9)
  elseif pageIndex == pageNum then
    if ballNumber == pageIndex * 9 + 1 then
      titleText = string.gsub(titleText, "<start_no>", ballNumber)
      titleText = string.gsub(titleText, "-", "")
      titleText = string.gsub(titleText, "<end_no>", "")
    else
      titleText = string.gsub(titleText, "<start_no>", pageIndex * 9 + 1)
      titleText = string.gsub(titleText, "<end_no>", ballNumber)
    end
  end
  DebugOut("result:" .. titleText)
  return titleText
end
function GameUIWDPlanetSelect:InitPlanetGalaxySelect()
  local ballNumber = #GameUIWDPlanetSelect.BattleList.Item
  DebugOut("GameUIWDPlanetSelect:InitPlanetGalaxySelect,ball number:", ballNumber)
  DebugTable(GameUIWDPlanetSelect.BattleList.Item)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "lua2fs_setTotalBallNumber", ballNumber)
  end
  DebugOut("currentGalaxyIndex:", currentGalaxyIndex)
  currentGalaxyIndex = currentGalaxyIndex or math.ceil(ballNumber / 9)
  local title = self:GetTitleText(ballNumber, currentGalaxyIndex)
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "lua2fs_checkToGalaxyByIndex", currentGalaxyIndex, title)
  end
  GameUIWDPlanetSelect:SetPlanetGalaxyInfo()
end
function GameUIWDPlanetSelect:SetPlanetGalaxyInfo()
  local startIndex = (currentGalaxyIndex - 2) * 9 + 1
  local endIndex = (currentGalaxyIndex + 1) * 9
  if startIndex < 0 then
    startIndex = 1
  end
  DebugOut("SetPlanetInfo:", currentGalaxyIndex, ",", startIndex, ",", endIndex)
  for i = startIndex, endIndex do
    if not GameUIWDPlanetSelect.BattleList.Item[i] then
      local planetItem = {fight_name = ""}
    end
    local pageIndex = math.ceil(i / 9)
    local starIndex = i - (pageIndex - 1) * 9
    GameUIWDPlanetSelect:SetStarInfo(pageIndex, starIndex, planetItem)
  end
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "SetStarsPos", "M")
  end
end
function GameUIWDPlanetSelect:RecordStarsPos(cmd)
  local pageIndex = "L"
  if cmd == "nextAnimOver" then
    pageIndex = "R"
  end
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "RecordStarsPos", pageIndex)
  end
end
function GameUIWDPlanetSelect:CheckToGalaxyByIndex(galaxyIndex)
end
function GameUIWDPlanetSelect:SetStarInfo(pageIndex, starIndex, planetItem)
  DebugOut("SetStarInfo:", pageIndex, ",", starIndex)
  local flash_obj = self:GetFlashObject()
  if flash_obj and planetItem then
    local fightName = GameLoader:GetGameText(string.upper("LC_MENU_WD_BATTLE_NAME_" .. planetItem.fight_name))
    local timeString = ""
    if not planetItem.left_time then
      flash_obj:InvokeASCallback("_root", "setGalaxyPlanetInfo", pageIndex, starIndex, "", 0, "", "", "", GameUtils:GetFlagFrameName(0))
    elseif planetItem.left_time < 0 then
      local _lefttime = math.abs(planetItem.left_time) - (os.time() - GameStateWD.fetchWDListTime)
      if _lefttime < 0 then
        _lefttime = 0
      end
      if _lefttime > 2592000 then
        timeString = ""
      else
        timeString = GameUtils:formatTimeString(_lefttime)
      end
      flash_obj:InvokeASCallback("_root", "setGalaxyPlanetInfo", pageIndex, starIndex, fightName, 1, timeString, "", "", GameUtils:GetFlagFrameName(0))
    elseif planetItem.left_time > 0 then
      local _lefttime = planetItem.left_time - (os.time() - GameStateWD.fetchWDListTime)
      if _lefttime < 0 then
        _lefttime = 0
      end
      timeString = GameUtils:formatTimeString(_lefttime)
      flash_obj:InvokeASCallback("_root", "setGalaxyPlanetInfo", pageIndex, starIndex, fightName, 2, timeString, "", "", GameUtils:GetFlagFrameName(0))
    elseif planetItem.left_time == 0 then
      flash_obj:InvokeASCallback("_root", "setGalaxyPlanetInfo", pageIndex, starIndex, fightName, 3, "", planetItem.alliance_name, GameUtils:GetUserDisplayName(planetItem.alliance_leader), GameUtils:GetFlagFrameName(planetItem.flag))
    end
  end
end
function GameUIWDPlanetSelect:UpdateGalaxyCDTime()
  local timeString = ""
  local _lefttime = 0
  local startIndex = (currentGalaxyIndex - 2) * 9 + 1
  local endIndex = (currentGalaxyIndex + 1) * 9
  if startIndex < 0 then
    startIndex = 1
  end
  for i = startIndex, endIndex do
    local planetItem = GameUIWDPlanetSelect.BattleList.Item[i]
    local pageIndex = math.ceil(i / 9)
    local starIndex = i - pageIndex * 9
    if planetItem and planetItem.left_time then
      _lefttime = math.abs(planetItem.left_time) - (os.time() - GameStateWD.fetchWDListTime)
      if _lefttime < 0 then
        _lefttime = 0
      end
      if _lefttime > 2592000 and 0 > planetItem.left_time then
        timeString = ""
      else
        timeString = GameUtils:formatTimeStringAsTwitterStyle(_lefttime)
      end
      local flash = self:GetFlashObject()
      if flash then
        flash:InvokeASCallback("_root", "setGalaxyCDTime", pageIndex, starIndex, timeString)
      end
      if _lefttime == 0 and 0 > planetItem.left_time and self.curPlanetState == EPlanetState.State_NONE then
        self:GotoState(EPlanetState.State_FetchInfoForStart)
      end
    else
      local flash = self:GetFlashObject()
      if flash then
        flash:InvokeASCallback("_root", "setGalaxyCDTime", pageIndex, starIndex, "")
      end
    end
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUIWDPlanetSelect.OnAndroidBack()
    if GameUIWDPlanetSelect.IsShowEventListWindow then
      GameUIWDPlanetSelect:OnFSCommand("EventList_close_released")
      GameUIWDPlanetSelect.IsShowEventListWindow = false
    elseif GameUIWDPlanetSelect.IsShowHelpWindow then
      GameUIWDPlanetSelect:GetFlashObject():InvokeASCallback("_root", "hideHelp")
      GameUIWDPlanetSelect.IsShowHelpWindow = false
    elseif GameUIWDPlanetSelect.IsShowDominationWindow then
      GameUIWDPlanetSelect:GetFlashObject():InvokeASCallback("_root", "dominationMoveOut")
      GameUIWDPlanetSelect.IsShowDominationWindow = false
    else
      GameStateManager:SetCurrentGameState(GameStateWD:GetPreState())
    end
  end
end
