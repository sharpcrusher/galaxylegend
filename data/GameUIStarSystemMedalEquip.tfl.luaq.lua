local GameUIStarSystemPort = LuaObjectManager:GetLuaObject("GameUIStarSystemPort")
local GameUIStarSystemPort_SUB = LuaObjectManager:GetLuaObject("GameUIStarSystemPort_SUB")
GameUIStarSystemPort.medalEquip = {}
local GameUIStarSystemMedalEquip = GameUIStarSystemPort.medalEquip
local GameFleetNewEnhance = LuaObjectManager:GetLuaObject("GameFleetNewEnhance")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameVip = LuaObjectManager:GetLuaObject("GameVip")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
local GameStateStore = GameStateManager.GameStateStore
local DynamicResDownloader = DynamicResDownloader
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameUIStarSystemRecycle = GameUIStarSystemPort.uiRecycle
GameUIStarSystemMedalEquip.CurMaterialMedalList = {}
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local GameFleetEquipment = LuaObjectManager:GetLuaObject("GameFleetEquipment")
GameFleetNewEnhance = LuaObjectManager:GetLuaObject("GameFleetEquipment").xunzhang
function GameUIStarSystemMedalEquip:GetFlashObject()
  return (...), GameUIStarSystemPort_SUB
end
function GameUIStarSystemMedalEquip:OnFSCommand(cmd, arg)
  if cmd == "update_medalEquip_item" then
    self:UpdateMedalListItem(tonumber(arg))
  elseif cmd == "ShowMedalDetail" then
    local medal = self.nowTargetPosMedalList[tonumber(arg)]
    self:ShowMedalDetail(medal, false)
  elseif cmd == "equipMedal" then
    local param = LuaUtils:string_split(arg, "\001")
    self:EquipMedal(tonumber(param[1]), tonumber(param[2]))
    if TutorialQuestManager.QuestTutorialEquipStarSystemItem:IsActive() and GameGlobalData:GetModuleStatus("medal") and GameFleetNewEnhance.curSelectedSlot == 1 then
      TutorialQuestManager.QuestTutorialEquipStarSystemItem:SetFinish(true)
    end
  elseif cmd == "ShowLevelUPMenu" then
    self:ShowLevelUPMenu(tonumber(arg), true)
  elseif cmd == "ShowRankUPMenu" then
    self:ShowRankUPMenu(tonumber(arg), true)
  elseif cmd == "update_material_item" then
    self:UpdateMaterialListItem(tonumber(arg))
  elseif cmd == "SelectedMaterial" then
    local param = LuaUtils:string_split(arg, "\001")
    self:SelectedMaterial(param[1], tonumber(param[2]))
  elseif cmd == "UnLoadMaterial" then
    local param = LuaUtils:string_split(arg, "\001")
    self:UnLoadMaterial(param[1], tonumber(param[2]))
  elseif cmd == "ShowMaterialDetail" then
    local param = LuaUtils:string_split(arg, "\001")
    self:ShowMaterialDetail(param[1], tonumber(param[2]))
  elseif cmd == "AutoAddMeterail" then
    self.checkRank = true
    self.AutoAddMeterail()
  elseif cmd == "ConfirmLevelUp" then
    self:ConfirmLevelUp()
  elseif cmd == "showSelectMenu" then
    GameUIStarSystemMedalEquip.isReplace = true
    self:ShowMedalSelectMenu(GameFleetNewEnhance.curSelectedSlot)
  elseif cmd == "dischangeMedal" then
    self:DisChangeMedal(tonumber(arg))
  elseif cmd == "unLoadMedal" then
    self:UnLoadMedal()
  elseif cmd == "RemoveMeterial" then
    local param = LuaUtils:string_split(arg, "\001")
    self:RemoveMeterial(param[1], tonumber(param[2]))
  elseif cmd == "ConfirmRankUP" then
    self:ConfirmRankUP()
  elseif cmd == "OnLeveUPMenuClosed" then
    GameUIStarSystemMedalEquip.selectedMaterialTable = {}
  elseif cmd == "ShowLevelMenuHelpInfo" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_HELP_MEDAL_LEVEL_UP"))
  elseif cmd == "ShowRankMenuHelpInfo" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_HELP_MEDAL_RANK_UP"))
  elseif cmd == "MedalReset" then
    local medal = GameFleetNewEnhance:GetMedalDetailById(GameFleetNewEnhance.curSelectedMedal)
    if medal and medal.rank > 0 or 0 < medal.exp then
      GameUIStarSystemPort.allMedalInfos = GameFleetNewEnhance._AllMedalData.medals
      GameUIStarSystemPort.uiRecycle:onResetitem("", GameFleetNewEnhance.curSelectedMedal)
    else
    end
  elseif cmd == "NextLevelMedal" then
    self:ChangeLevelUPMedal(1)
  elseif cmd == "PrevLevelMedal" then
    self:ChangeLevelUPMedal(-1)
  elseif cmd == "PrevRankMedal" then
    self:ChangeRankUPMedal(-1)
  elseif cmd == "NextRankMedal" then
    self:ChangeRankUPMedal(1)
  elseif cmd == "Consume_clicked" then
    self:ConsumeClicked(tonumber(arg))
  elseif cmd == "prevSlotMedal" then
    GameFleetNewEnhance:OnFSCommand("prevSlotMedal")
  elseif cmd == "nextSlotMedal" then
    GameFleetNewEnhance:OnFSCommand("nextSlotMedal")
  end
end
GameUIStarSystemMedalEquip.nowTargetPosMedalList = {}
function GameUIStarSystemMedalEquip:ShowMedalSelectMenu(slot)
  if not self:GetFlashObject() then
    GameUIStarSystemPort_SUB:LoadFlashObject()
    self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "TEXT_EQUIP", GameLoader:GetGameText("LC_MENU_MEDAL_EQUIP"))
    self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "TEXT_LEVEL_MENU_TITLE", GameLoader:GetGameText("LC_MENU_NAME_MEDAL_LEVEL_UP"))
    self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "TEXT_RANK_MENU_TITLE", GameLoader:GetGameText("LC_MENU_NAME_MEDAL_RANK_UP"))
    self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "TEXT_LEVEL_FULL", GameLoader:GetGameText("LC_MENU_MEDAL_LEVEL_LEVEL_MAX"))
    self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "TEXT_RANK_FULL", GameLoader:GetGameText("LC_MENU_MEDAL_LEVEL_RANK_MAX"))
    self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "TEXT_LEVEL_UP_BTN", GameLoader:GetGameText("LC_MENU_MEDAL_LEVEL_UP"))
    self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "TEXT_RANK_UP_BTN", GameLoader:GetGameText("LC_MENU_MEDAL_RANK_UP"))
    self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "MEDAL_EXCHANGE", GameLoader:GetGameText("LC_MENU_MEDAL_EXCHANGE"))
    self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "MEDAL_DISBOARD", GameLoader:GetGameText("LC_MENU_MEDAL_DISBOARD"))
  end
  self:GetTargetPosMedalList(slot)
  if GameFleetNewEnhance._AllMedalData then
    self:GetFlashObject():InvokeASCallback("_root", "showSelectMenu", math.ceil(#self.nowTargetPosMedalList / 4))
  end
  if TutorialQuestManager.QuestTutorialEquipStarSystemItem:IsActive() and GameGlobalData:GetModuleStatus("medal") and GameFleetNewEnhance.curSelectedSlot == 1 then
    self:GetFlashObject():InvokeASCallback("_root", "showFirstEuipTip", true)
  end
end
function GameUIStarSystemMedalEquip:GetTargetPosMedalList(slot)
  GameUIStarSystemMedalEquip.nowTargetPosMedalList = {}
  DebugOutPutTable(GameFleetNewEnhance._AllAvailableMedal, "GetTargetPosMedalList :" .. slot)
  for k, v in ipairs(GameFleetNewEnhance._AllAvailableMedal) do
    if v.target_pos == slot then
      table.insert(self.nowTargetPosMedalList, v)
    end
  end
  local sorMedal = function(a, b)
    if a.quality > b.quality then
      return true
    elseif a.quality < b.quality then
      return false
    elseif a.rank > b.rank then
      return true
    elseif a.rank < b.rank then
      return false
    else
      return a.level > b.level
    end
  end
  table.sort(self.nowTargetPosMedalList, sorMedal)
end
function GameUIStarSystemMedalEquip:UpdateMedalListItem(itemIndex)
  if itemIndex then
    local medalDataes = {}
    for i = 1, 4 do
      local item = self.nowTargetPosMedalList[(itemIndex - 1) * 4 + i]
      if item then
        local detail = {}
        local extendInfo = {}
        extendInfo.idx = itemIndex
        extendInfo.subId = i
        detail.iconFram, detail.picName = GameHelper:GetMedalIconFrameAndPic(item.type)
        detail.name = GameHelper:GetAwardTypeText("medal", item.type)
        detail.Lv = item.level
        detail.rank = item.rank
        detail.quality = item.quality
        table.insert(medalDataes, detail)
      end
    end
    DebugOutPutTable(medalDataes, "UpdateMedalListItem " .. itemIndex)
    self:GetFlashObject():InvokeASCallback("_root", "updateMedalListItem", itemIndex, medalDataes)
  end
end
GameUIStarSystemMedalEquip.CurSlidingPool = {}
function GameUIStarSystemMedalEquip:ShowMedalDetail(medal, isFromSlot, isUpdate, isCurFleetInMatrix)
  if not self:GetFlashObject() then
    GameUIStarSystemPort_SUB:LoadFlashObject()
    self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "TEXT_EQUIP", GameLoader:GetGameText("LC_MENU_MEDAL_EQUIP"))
    self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "TEXT_LEVEL_MENU_TITLE", GameLoader:GetGameText("LC_MENU_NAME_MEDAL_LEVEL_UP"))
    self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "TEXT_RANK_MENU_TITLE", GameLoader:GetGameText("LC_MENU_NAME_MEDAL_RANK_UP"))
    self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "TEXT_LEVEL_FULL", GameLoader:GetGameText("LC_MENU_MEDAL_LEVEL_LEVEL_MAX"))
    self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "TEXT_RANK_FULL", GameLoader:GetGameText("LC_MENU_MEDAL_LEVEL_RANK_MAX"))
    self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "TEXT_LEVEL_UP_BTN", GameLoader:GetGameText("LC_MENU_MEDAL_LEVEL_UP"))
    self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "TEXT_RANK_UP_BTN", GameLoader:GetGameText("LC_MENU_MEDAL_RANK_UP"))
    self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "MEDAL_EXCHANGE", GameLoader:GetGameText("LC_MENU_MEDAL_EXCHANGE"))
    self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "MEDAL_DISBOARD", GameLoader:GetGameText("LC_MENU_MEDAL_DISBOARD"))
  end
  if not medal.haveConfig then
    medal = GameUtils:MedalHelper_GetMedalConfig(medal, medal.type, medal.level)
  end
  local param = {
    id = medal.id
  }
  param.iconFram, param.picName = GameHelper:GetMedalIconFrameAndPicNew(medal.type)
  param.rank = medal.rank
  param.Lv = medal.level
  param.name = GameHelper:GetAwardTypeText("medal", medal.type)
  param.desc = GameLoader:GetGameText("LC_MENU_MEDAL_DESC_" .. math.ceil(medal.type / 100000 - 1) * 100000 + medal.type % 10)
  param.quality = medal.quality
  param.levelLimit = GameLoader:GetGameText("LC_MENU_MEDAL_RANK_UP_REQUIRE") .. medal.rank_level_limit
  param.rank_level_limit = medal.rank_level_limit
  param.attr_basic = {}
  param.attr_level = {}
  param.attr_rank = {}
  param.attr_total = {}
  DebugOutPutTable(medal.can_replace, "ShowMedalDetail 11")
  local curMatrixIndex = GameUIStarSystemMedalEquip:GetCurMatrixId()
  param.can_replace = false
  for k, v in ipairs(medal.can_replace) do
    if v.key == curMatrixIndex and v.value == 1 then
      param.can_replace = true
    end
  end
  param.can_rank = medal.can_rank
  param.can_level_up = medal.can_level_up
  for k, v in ipairs(medal.attr) do
    local detail = {}
    detail.propertyName = GameUIStarSystemMedalEquip:GetAttrName(v.attr_id)
    detail.propertyValue = GameUIStarSystemMedalEquip:GetAttrValue(v.attr_id, v.attr_value)
    if v.attr_value > 0 then
      table.insert(param.attr_basic, detail)
    end
  end
  DebugTable(param.attr_basic)
  DebugOutPutTable(medal.attr, "ShowMedalDetail ")
  for k, v in ipairs(medal.attr_level) do
    local detail = {}
    detail.propertyName = GameUIStarSystemMedalEquip:GetAttrName(v.attr_id)
    detail.propertyValue = GameUIStarSystemMedalEquip:GetAttrValue(v.attr_id, v.attr_value)
    if v.attr_value > 0 then
      table.insert(param.attr_level, detail)
    end
  end
  for k, v in ipairs(medal.attr_rank) do
    local detail = {}
    detail.propertyName = GameUIStarSystemMedalEquip:GetAttrName(v.attr_id)
    detail.propertyValue = GameUIStarSystemMedalEquip:GetAttrValue(v.attr_id, v.attr_value)
    table.insert(param.attr_rank, detail)
  end
  param.textLevel = GameLoader:GetGameText("LC_MENU_MEDAL_MEDAL_LEVEL")
  param.textRank = GameLoader:GetGameText("LC_MENU_MEDAL_MEDAL_RANK")
  param.attr_total = self:MergeAllProperty(medal.attr, medal.attr_level, medal.attr_rank)
  DebugOutPutTable(param, "ShowMedalDetail")
  param.exp = medal.exp
  if not isUpdate then
    GameUIStarSystemMedalEquip.CurSlidingPool = {}
    GameUIStarSystemMedalEquip.CurSlidingPool = LuaUtils:table_copy(GameUIStarSystemMedalEquip.nowTargetPosMedalList)
    if isFromSlot then
      table.insert(GameUIStarSystemMedalEquip.CurSlidingPool, 1, medal)
    end
  end
  DebugOutPutTable(self.CurSlidingPool, "CurSlidingPool")
  param.hasPrevMedal = GameFleetNewEnhance.hasPrev
  param.hasNextMedal = GameFleetNewEnhance.hasNext
  if not isUpdate then
    self:GetFlashObject():InvokeASCallback("_root", "ShowMedalDetail", param, isFromSlot, isCurFleetInMatrix)
  else
    self:GetFlashObject():InvokeASCallback("_root", "SetMedalDetailMenuData", param, true)
  end
end
function GameUIStarSystemMedalEquip:MergeAllPropertyWithOutText(basic, level, rank)
  local total = LuaUtils:table_rcopy(basic)
  for k, v in ipairs(level) do
    local hasFind = false
    for k2, v2 in ipairs(total) do
      if v.attr_id == v2.attr_id then
        v2.attr_value = v.attr_value + v2.attr_value
        hasFind = true
      end
    end
    if not hasFind then
      table.insert(total, LuaUtils:table_rcopy(v))
    end
  end
  for k, v in ipairs(rank) do
    local hasFind = false
    for k2, v2 in ipairs(total) do
      if v.attr_id == v2.attr_id then
        v2.attr_value = v.attr_value + v2.attr_value
        hasFind = true
      end
    end
    if not hasFind then
      table.insert(total, LuaUtils:table_rcopy(v))
    end
  end
  return total
end
function GameUIStarSystemMedalEquip:MergeAllProperty(basic, level, rank)
  local total = GameUIStarSystemMedalEquip:MergeAllPropertyWithOutText(basic, level, rank)
  local totalProoerty = {}
  for k, v in ipairs(total) do
    local detail = {}
    detail.propertyName = GameUIStarSystemMedalEquip:GetAttrName(v.attr_id)
    detail.propertyValue = GameUIStarSystemMedalEquip:GetAttrValue(v.attr_id, v.attr_value)
    if v.attr_value ~= 0 then
      table.insert(totalProoerty, detail)
    end
  end
  DebugOutPutTable(totalProoerty, "MergeAllProperty")
  return totalProoerty
end
GameUIStarSystemMedalEquip.curRqstEquipMedal = nil
GameUIStarSystemMedalEquip.curRqstEquipMedalIdx = nil
function GameUIStarSystemMedalEquip:EquipMedal(idx, subId)
  if not self.isReplace then
    local param = {}
    param.medal_id = self.nowTargetPosMedalList[idx].id
    param.fleet_id = GameFleetNewEnhance:GetCurFleetId()
    param.formation_id = GameUIStarSystemMedalEquip:GetCurMatrixId()
    param.pos = GameFleetNewEnhance.curSelectedSlot
    self.curRqstEquipMedal = param.medal_id
    self.curRqstEquipMedalIdx = idx
    NetMessageMgr:SendMsg(NetAPIList.medal_equip_on_req.Code, param, self.EquipMedalCallBack, true, nil)
  else
    local param = {}
    param.medal_id = self.nowTargetPosMedalList[idx].id
    param.fleet_id = GameFleetNewEnhance:GetCurFleetId()
    param.formation_id = GameUIStarSystemMedalEquip:GetCurMatrixId()
    param.pos = GameFleetNewEnhance.curSelectedSlot
    param.replaced_id = GameFleetNewEnhance.curSelectedMedal
    self.curRqstEquipMedal = param.medal_id
    self.curRqstEquipMedalIdx = idx
    NetMessageMgr:SendMsg(NetAPIList.medal_equip_replace_req.Code, param, self.ReplaceMedalCallBack, true, nil)
  end
end
function GameUIStarSystemMedalEquip.EquipMedalCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.medal_equip_on_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.medal_equip_on_ack.Code then
    if true == content.success then
      GameFleetNewEnhance.curFleetMedals[GameFleetNewEnhance.curSelectedSlot] = GameUIStarSystemMedalEquip.curRqstEquipMedal
      local formation_info = {}
      formation_info.medal_id = GameUIStarSystemMedalEquip.curRqstEquipMedal
      formation_info.fleet_id = GameFleetNewEnhance:GetCurFleetId()
      formation_info.formation_id = GameUIStarSystemMedalEquip:GetCurMatrixId()
      formation_info.pos = GameFleetNewEnhance.curSelectedSlot
      GameFleetNewEnhance:AddFormationData(formation_info)
      GameUIStarSystemMedalEquip:GetFlashObject():InvokeASCallback("_root", "HideSelectMenu")
      GameFleetNewEnhance:GenerateAvailableMedal()
      GameFleetNewEnhance:SetMedalEquipSlot()
    end
    return true
  end
  return false
end
function GameUIStarSystemMedalEquip.ReplaceMedalCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.medal_equip_replace_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.medal_equip_replace_ack.Code then
    if true == content.success then
      for k, v in pairs(GameFleetNewEnhance.curFleetMedals) do
        if v == GameFleetNewEnhance.curSelectedMedal then
          GameFleetNewEnhance.curFleetMedals[k] = nil
        end
      end
      GameFleetNewEnhance.curFleetMedals[GameFleetNewEnhance.curSelectedSlot] = GameUIStarSystemMedalEquip.curRqstEquipMedal
      local formation_info = {}
      formation_info.medal_id = GameUIStarSystemMedalEquip.curRqstEquipMedal
      formation_info.fleet_id = GameFleetNewEnhance:GetCurFleetId()
      formation_info.formation_id = GameUIStarSystemMedalEquip:GetCurMatrixId()
      formation_info.pos = GameFleetNewEnhance.curSelectedSlot
      GameFleetNewEnhance:AddFormationData(formation_info)
      formation_info.medal_id = GameFleetNewEnhance.curSelectedMedal
      GameFleetNewEnhance:EraseFormationData(formation_info)
      GameFleetNewEnhance:GenerateAvailableMedal()
      GameFleetNewEnhance.curSelectedMedal = nil
      GameUIStarSystemMedalEquip:GetFlashObject():InvokeASCallback("_root", "HideSelectMenu")
      GameFleetNewEnhance:SetMedalEquipSlot()
    end
    return true
  end
  return false
end
function GameUIStarSystemMedalEquip:OnMedalEquipUpdate(dt)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "OnMedalEquipUpdate", dt)
  end
end
GameUIStarSystemMedalEquip.nowSelectedMedal = nil
function GameUIStarSystemMedalEquip:ShowLevelUPMenu(id, isMoveIn)
  local medal = GameFleetNewEnhance:GetMedalDetailById(id)
  local medalDetail = {}
  self.nowSelectedMedal = medal.id
  medalDetail.iconFram, medalDetail.picName = GameHelper:GetMedalIconFrameAndPicNew(medal.type)
  medalDetail.rank = medal.rank
  medalDetail.Lv = medal.level
  medalDetail.name = GameHelper:GetAwardTypeText("medal", medal.type)
  medalDetail.quality = medal.quality
  medalDetail.attr_basic = self:MergeAllProperty(medal.attr, medal.attr_level, medal.attr_rank)
  self.CurMaterialMedalList = {}
  for k, v in ipairs(GameFleetNewEnhance._CanMaterialMedal) do
    if v.id ~= self.nowSelectedMedal then
      if not v.haveConfig then
        v = GameUtils:MedalHelper_GetMedalConfig(v, v.type, v.level)
      end
      table.insert(self.CurMaterialMedalList, v)
    end
  end
  local sorMaterial = function(a, b)
    if a.quality < b.quality then
      return true
    elseif a.quality > b.quality then
      return false
    elseif a.rank < b.rank then
      return true
    elseif a.rank > b.rank then
      return false
    elseif a.level < b.level then
      return true
    elseif a.level > b.level then
      return false
    else
      return a.type < b.type
    end
  end
  table.sort(self.CurMaterialMedalList, sorMaterial)
  local materialsCnt = #self.CurMaterialMedalList + GameFleetNewEnhance.LevelUpMaterialCnt
  if medal.exp_level[medal.level - 1] then
    medalDetail.nowExp = medal.exp - medal.exp_level[medal.level - 1].value
  else
    medalDetail.nowExp = medal.exp
  end
  medalDetail.addExp = 0
  medalDetail.totalExp = medal.exp_level[medal.level].value or 0
  if medal.exp_level[medal.level - 1] then
    medalDetail.totalExp = medal.exp_level[medal.level].value - medal.exp_level[medal.level - 1].value
  end
  DebugOutPutTable(medalDetail, "ShowLevelUPMenu " .. id, materialsCnt)
  GameUIStarSystemMedalEquip.Slid_idx = 1
  local slid_pool_cnt = #GameUIStarSystemMedalEquip.CurSlidingPool
  for k, v in ipairs(GameUIStarSystemMedalEquip.CurSlidingPool) do
    if v.id == medal.id then
      GameUIStarSystemMedalEquip.Slid_idx = k
      break
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "SetLevelUPMenuData", medalDetail, math.ceil(materialsCnt / 3), GameUIStarSystemMedalEquip.Slid_idx, slid_pool_cnt, isMoveIn)
end
function GameUIStarSystemMedalEquip:ChangeLevelUPMedal(param)
  local medal = self.CurSlidingPool[self.Slid_idx + param]
  if medal then
    self.selectedMaterialTable = {}
    GameUIStarSystemMedalEquip:ShowLevelUPMenu(medal.id, false)
  end
end
function GameUIStarSystemMedalEquip:UpdateMaterialListItem(itemIndex)
  if itemIndex then
    local materialDetail = {}
    for i = 1, 3 do
      local index = (itemIndex - 1) * 3 + i
      if index <= GameFleetNewEnhance.LevelUpMaterialCnt then
        local item = GameFleetNewEnhance._MaterialsDates[index]
        if item then
          local detail = {}
          local extendInfo = {}
          extendInfo.itemIndex = itemIndex
          extendInfo.subId = i
          detail.type = "item"
          detail.iconFram = "medal_item_" .. item.type
          detail.picName = item.type .. ".png"
          detail.idx = index
          detail.quality = item.type % 10
          detail.nameText = GameHelper:GetAwardTypeText("medal_item", item.type)
          detail.isSelected = self:IsMedalOrMaterialSetected("item", index)
          table.insert(materialDetail, detail)
        end
      else
        local medal = self.CurMaterialMedalList[index - GameFleetNewEnhance.LevelUpMaterialCnt]
        if medal then
          local detail = {}
          detail.type = "medal"
          detail.iconFram, detail.picName = GameHelper:GetMedalIconFrameAndPic(medal.type)
          detail.Lv = medal.level
          detail.rank = medal.rank
          detail.quality = medal.quality
          detail.idx = index - GameFleetNewEnhance.LevelUpMaterialCnt
          detail.nameText = GameHelper:GetAwardTypeText("medal", medal.type)
          detail.isSelected = self:IsMedalOrMaterialSetected("medal", detail.idx)
          table.insert(materialDetail, detail)
        end
      end
    end
    DebugOutPutTable(materialDetail, "UpdateMedalListItem " .. itemIndex)
    self:GetFlashObject():InvokeASCallback("_root", "UpdateMaterialListItem", itemIndex, materialDetail)
  end
end
function GameUIStarSystemMedalEquip:AddMaterial(type, idx, isAuto)
  if type == "item" then
    local material = GameFleetNewEnhance._MaterialsDates[idx]
    if material then
      local detail = {}
      detail.type = "item"
      detail.iconFram = "medal_item_" .. material.type
      detail.picName = material.type .. ".png"
      detail.idx = idx
      detail.exp = material.exp
      detail.id = material.type
      detail.quality = material.type % 10
      for i = 1, 6 do
        if self.selectedMaterialTable[i] == nil then
          detail.slot = i
          self.selectedMaterialTable[i] = detail
          break
        end
      end
      self:GetFlashObject():InvokeASCallback("_root", "setMaterialChoose", math.ceil(idx / 3), idx % 3 ~= 0 and idx % 3 or 3)
    end
  elseif type == "medal" then
    local material = self.CurMaterialMedalList[idx]
    if material then
      GameUIStarSystemMedalEquip.curNeedAddMedalMaterial = material
      GameUIStarSystemMedalEquip.curNeedAddMedalMaterialIndex = idx
      if 0 < material.rank and not isAuto then
        local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
        local info = GameLoader:GetGameText("LC_MENU_MEDAL_LEVEL_UP_ALERT")
        GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
        GameUIMessageDialog:SetYesButton(GameUIStarSystemMedalEquip.addMedalMaterial)
        GameUIMessageDialog:SetNoButton(nil)
        GameUIMessageDialog:Display(text_title, info)
      else
        GameUIStarSystemMedalEquip.addMedalMaterial()
      end
    end
  end
end
function GameUIStarSystemMedalEquip.addMedalMaterial()
  if GameUIStarSystemMedalEquip.curNeedAddMedalMaterial then
    local detail = {}
    detail.type = "medal"
    detail.iconFram, detail.picName = GameHelper:GetMedalIconFrameAndPic(GameUIStarSystemMedalEquip.curNeedAddMedalMaterial.type)
    detail.Lv = GameUIStarSystemMedalEquip.curNeedAddMedalMaterial.level
    detail.rank = GameUIStarSystemMedalEquip.curNeedAddMedalMaterial.rank
    detail.idx = GameUIStarSystemMedalEquip.curNeedAddMedalMaterialIndex
    detail.exp = GameUIStarSystemMedalEquip.curNeedAddMedalMaterial.material_exp + GameUIStarSystemMedalEquip.curNeedAddMedalMaterial.exp
    detail.id = GameUIStarSystemMedalEquip.curNeedAddMedalMaterial.id
    detail.quality = GameUIStarSystemMedalEquip.curNeedAddMedalMaterial.quality
    for i = 1, 6 do
      if GameUIStarSystemMedalEquip.selectedMaterialTable[i] == nil then
        detail.slot = i
        GameUIStarSystemMedalEquip.selectedMaterialTable[i] = detail
        break
      end
    end
    local _index = detail.idx + GameFleetNewEnhance.LevelUpMaterialCnt
    GameUIStarSystemMedalEquip:GetFlashObject():InvokeASCallback("_root", "setMaterialChoose", math.ceil(_index / 3), _index % 3 ~= 0 and _index % 3 or 3)
    GameUIStarSystemMedalEquip.curNeedAddMedalMaterial = nil
    local curMedalLevelUPInfo = GameUIStarSystemMedalEquip:GetCurMedalLevelUPProperty()
    DebugOutPutTable(GameUIStarSystemMedalEquip:GetSelectedMaterialTableWithParamFormat(), "curMedalLevelUPInfo")
    GameUIStarSystemMedalEquip:GetFlashObject():InvokeASCallback("_root", "SetLevelUPMenuMedalData", GameUIStarSystemMedalEquip:GetSelectedMaterialTableWithParamFormat(), curMedalLevelUPInfo)
  end
end
GameUIStarSystemMedalEquip.selectedMaterialTable = {}
function GameUIStarSystemMedalEquip:SelectedMaterial(type, idx)
  if self:GetMaterialSlotCnt() == 6 then
    return
  end
  self:AddMaterial(type, idx)
  local curMedalLevelUPInfo = self:GetCurMedalLevelUPProperty()
  DebugOutPutTable(self:GetSelectedMaterialTableWithParamFormat(), "curMedalLevelUPInfo")
  self:GetFlashObject():InvokeASCallback("_root", "SetLevelUPMenuMedalData", self:GetSelectedMaterialTableWithParamFormat(), curMedalLevelUPInfo)
end
function GameUIStarSystemMedalEquip:UnLoadMaterial(type, idx)
  for k, v in pairs(self.selectedMaterialTable) do
    if v.idx == idx and v.type == type then
      self.selectedMaterialTable[k] = nil
      local _index = idx
      if type == "medal" then
        _index = idx + GameFleetNewEnhance.LevelUpMaterialCnt
      end
      self:GetFlashObject():InvokeASCallback("_root", "setMaterialUnChoose", math.ceil(_index / 3), _index % 3 ~= 0 and _index % 3 or 3)
      break
    end
  end
  local curMedalLevelUPInfo = self:GetCurMedalLevelUPProperty()
  DebugOutPutTable(self:GetSelectedMaterialTableWithParamFormat(), "curMedalLevelUPInfo")
  self:GetFlashObject():InvokeASCallback("_root", "SetLevelUPMenuMedalData", self:GetSelectedMaterialTableWithParamFormat(), curMedalLevelUPInfo)
end
function GameUIStarSystemMedalEquip:ShowMaterialDetail(material_type, idx)
  local id = 0
  local e_quality = 0
  if material_type == "medal" then
    local tinfo = {}
    tinfo.type = self.CurMaterialMedalList[idx].type
    tinfo.type_next = self.CurMaterialMedalList[idx].type_next
    tinfo.quality = self.CurMaterialMedalList[idx].quality
    tinfo.rank = self.CurMaterialMedalList[idx].rank
    tinfo.level = self.CurMaterialMedalList[idx].level
    tinfo.exp = self.CurMaterialMedalList[idx].exp
    tinfo.attr = self.CurMaterialMedalList[idx].attr
    tinfo.attr_add = self.CurMaterialMedalList[idx].attr_add
    tinfo.attr_rank = self.CurMaterialMedalList[idx].attr_rank
    tinfo.price = self.CurMaterialMedalList[idx].price
    tinfo.is_material = self.CurMaterialMedalList[idx].is_material
    GameUIStarSystemRecycle:showMedalDetail(tinfo)
  else
    material_type = "medal_item"
    id = GameFleetNewEnhance._MaterialsDates[idx].type
    e_quality = GameFleetNewEnhance._MaterialsDates[idx].quality
    local item = {
      item_type = material_type,
      number = id,
      no = 1,
      quality = e_quality
    }
    ItemBox:showItemBox("Medal", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
  end
end
function GameUIStarSystemMedalEquip:GetMaterialSlotCnt()
  local cnt = 0
  for k, v in pairs(self.selectedMaterialTable) do
    if v then
      cnt = cnt + 1
    end
  end
  DebugOut("GetMaterialSlotCnt :", cnt)
  return cnt
end
function GameUIStarSystemMedalEquip:GetCurMedalLevelUPProperty()
  DebugOutPutTable(self.selectedMaterialTable, "GetCurMedalLevelUPProperty")
  local medal = GameFleetNewEnhance:GetMedalDetailById(self.nowSelectedMedal)
  local LevelInfo = {}
  if medal.exp_level[medal.level - 1] then
    LevelInfo.exp_now_level = medal.exp - medal.exp_level[medal.level - 1].value
  else
    LevelInfo.exp_now_level = medal.exp
  end
  LevelInfo.exp_add = 0
  LevelInfo.level = medal.level
  LevelInfo.exp_next_level_need = medal.exp_level[medal.level].value
  if medal.exp_level[medal.level - 1] then
    LevelInfo.exp_next_level_need = medal.exp_level[medal.level].value - medal.exp_level[medal.level - 1].value
  end
  if self:GetMaterialSlotCnt() == 0 then
    LevelInfo.level_add = 0
    LevelInfo.attrArr = self:MergeAllProperty(medal.attr, medal.attr_level, medal.attr_rank)
    return LevelInfo
  end
  for k, v in pairs(self.selectedMaterialTable) do
    LevelInfo.exp_add = LevelInfo.exp_add + v.exp
  end
  local exp_now = medal.exp + LevelInfo.exp_add
  DebugOut("aj  dfs:", LevelInfo.exp_add)
  LevelInfo.level_should = medal.level
  for k, v in ipairs(medal.exp_level) do
    if medal.exp_level[k - 1] and exp_now >= medal.exp_level[k - 1].value and exp_now < medal.exp_level[k].value then
      LevelInfo.level_should = v.key
    elseif not medal.exp_level[k - 1] and exp_now < medal.exp_level[k].value then
      LevelInfo.level_should = v.key
    elseif not medal.exp_level[k + 1] and exp_now >= medal.exp_level[k].value then
      LevelInfo.level_should = v.key
    end
  end
  LevelInfo.level_add = LevelInfo.level_should - LevelInfo.level
  LevelInfo.attrArr = self:MergeAllPropertyWithOutText(medal.attr, medal.attr_level, medal.attr_rank)
  for k, v in ipairs(medal.attr_add) do
    local detail = {}
    local hasType = false
    detail.propertyName = GameUIStarSystemMedalEquip:GetAttrName(v.attr_id)
    detail.propertyValue = 0
    for k2, v2 in ipairs(LevelInfo.attrArr) do
      if v2.attr_id == v.attr_id then
        v2.propertyName = GameUIStarSystemMedalEquip:GetAttrName(v2.attr_id)
        v2.propertyValue = GameUIStarSystemMedalEquip:GetAttrValue(v2.attr_id, v2.attr_value)
        v2.propertyAdd = GameUIStarSystemMedalEquip:GetAttrValue(v2.attr_id, LevelInfo.level_add * v.attr_value)
        hasType = true
        break
      end
    end
    if not hasType then
      detail.propertyAdd = GameUIStarSystemMedalEquip:GetAttrValue(v2.attr_id, LevelInfo.level_add * v.attr_value)
      table.insert(LevelInfo.attrArr, detail)
    end
  end
  return LevelInfo
end
GameUIStarSystemMedalEquip.checkRank = false
function GameUIStarSystemMedalEquip.AutoAddMeterail()
  if GameUIStarSystemMedalEquip:GetMaterialSlotCnt() == 6 then
    return
  end
  local materialNeedCnt = 6 - GameUIStarSystemMedalEquip:GetMaterialSlotCnt()
  if GameFleetNewEnhance.LevelUpMaterialCnt > 0 then
    for k = 1, GameFleetNewEnhance.LevelUpMaterialCnt do
      if not GameUIStarSystemMedalEquip:IsMedalOrMaterialSetected("item", k) and GameUIStarSystemMedalEquip:GetMaterialSlotCnt() < 6 then
        GameUIStarSystemMedalEquip:AddMaterial("item", k)
        GameUIStarSystemMedalEquip:GetFlashObject():InvokeASCallback("_root", "setMaterialChoose", math.ceil(k / 3), k % 3 ~= 0 and k % 3 or 3)
      end
    end
  end
  materialNeedCnt = 6 - GameUIStarSystemMedalEquip:GetMaterialSlotCnt()
  if materialNeedCnt > 0 then
    local needAddMedalMaterialList = {}
    for k, v in ipairs(GameUIStarSystemMedalEquip.CurMaterialMedalList) do
      if not GameUIStarSystemMedalEquip:IsMedalOrMaterialSetected("medal", k) and GameUIStarSystemMedalEquip:GetMaterialSlotCnt() + #needAddMedalMaterialList < 6 then
        needAddMedalMaterialList[#needAddMedalMaterialList + 1] = k
      end
    end
    local needConfirm = false
    if GameUIStarSystemMedalEquip.checkRank then
      for i = 1, #needAddMedalMaterialList do
        local material = GameUIStarSystemMedalEquip.CurMaterialMedalList[needAddMedalMaterialList[i]]
        if 0 < material.rank then
          needConfirm = true
          break
        end
      end
    end
    if needConfirm then
      local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
      local info = GameLoader:GetGameText("LC_MENU_MEDAL_LEVEL_UP_ALERT")
      GameUIStarSystemMedalEquip.checkRank = false
      GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
      GameUIMessageDialog:SetYesButton(GameUIStarSystemMedalEquip.AutoAddMeterail)
      GameUIMessageDialog:SetNoButton(nil)
      GameUIMessageDialog:Display(text_title, info)
    else
      for k, v in ipairs(needAddMedalMaterialList) do
        GameUIStarSystemMedalEquip:AddMaterial("medal", v, true)
        local index = v + GameFleetNewEnhance.LevelUpMaterialCnt
        GameUIStarSystemMedalEquip:GetFlashObject():InvokeASCallback("_root", "setMaterialChoose", math.ceil(index / 3), index % 3 ~= 0 and index % 3 or 3)
      end
    end
  end
  local curMedalLevelUPInfo = GameUIStarSystemMedalEquip:GetCurMedalLevelUPProperty()
  DebugOutPutTable(curMedalLevelUPInfo, "curMedalLevelUPInfo")
  GameUIStarSystemMedalEquip:GetFlashObject():InvokeASCallback("_root", "SetLevelUPMenuMedalData", GameUIStarSystemMedalEquip:GetSelectedMaterialTableWithParamFormat(), curMedalLevelUPInfo)
end
function GameUIStarSystemMedalEquip:IsMedalOrMaterialSetected(type, idx)
  for k, v in pairs(self.selectedMaterialTable) do
    if v.type == type and idx == v.idx then
      return true
    end
  end
  return false
end
function GameUIStarSystemMedalEquip:RemoveMeterial(type, idx)
  local material_detail = self.selectedMaterialTable[idx]
  if material_detail then
    local index = 1
    if material_detail.type == "item" then
      index = material_detail.idx
    elseif material_detail.type == "medal" then
      index = material_detail.idx + GameFleetNewEnhance.LevelUpMaterialCnt
    end
    DebugOut("RemoveMeterial", math.ceil(index / 3), index % 3)
    self:GetFlashObject():InvokeASCallback("_root", "setMaterialUnChoose", math.ceil(index / 3), index % 3 ~= 0 and index % 3 or 3)
    self.selectedMaterialTable[idx] = nil
    local curMedalLevelUPInfo = self:GetCurMedalLevelUPProperty()
    self:GetFlashObject():InvokeASCallback("_root", "SetLevelUPMenuMedalData", self:GetSelectedMaterialTableWithParamFormat(), curMedalLevelUPInfo)
  end
end
function GameUIStarSystemMedalEquip:GetSelectedMaterialTableWithParamFormat()
  local param = {}
  for k, v in pairs(self.selectedMaterialTable) do
    table.insert(param, v)
  end
  return param
end
function GameUIStarSystemMedalEquip:ConfirmLevelUp()
  DebugOut("self.selectedMaterialTable")
  DebugTable(self.selectedMaterialTable)
  if self.selectedMaterialTable == nil or self.selectedMaterialTable[1] == nil and self.selectedMaterialTable[2] == nil and self.selectedMaterialTable[3] == nil and self.selectedMaterialTable[4] == nil and self.selectedMaterialTable[5] == nil and self.selectedMaterialTable[6] == nil then
    local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
    local info = GameLoader:GetGameText("LC_MENU_MEDAL_LEVEL_NULL")
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.OK)
    GameUIMessageDialog:Display(text_title, info)
    return
  end
  if GameFleetNewEnhance:GetMedalDetailById(self.nowSelectedMedal).level == 30 then
    local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
    local info = GameLoader:GetGameText("LC_MENU_MEDAL_LEVEL_LEVEL_MAX")
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.OK)
    GameUIMessageDialog:Display(text_title, info)
    return
  end
  local param = {}
  param.id = self.nowSelectedMedal
  param.materials = {}
  param.medals = {}
  DebugOutPutTable(self.selectedMaterialTable, "ConfirmLevelUp")
  for k, v in pairs(self.selectedMaterialTable) do
    if v.type == "item" then
      local pair = {
        key = v.id,
        value = 1
      }
      table.insert(param.materials, pair)
    elseif v.type == "medal" then
      table.insert(param.medals, v.id)
    end
  end
  NetMessageMgr:SendMsg(NetAPIList.medal_level_up_req.Code, param, GameUIStarSystemMedalEquip.LevelUPCallback, true, nil)
end
function GameUIStarSystemMedalEquip.LevelUPCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.medal_level_up_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.medal_level_up_ack.Code then
    DebugOutPutTable(content, "LevelUPCallback")
    GameFleetNewEnhance._AllMedalData.materials = content.materials
    GameUIStarSystemMedalEquip:DeleteConsumes(content.consumes)
    GameFleetNewEnhance:GenerateAvailableMedal()
    GameFleetNewEnhance:GenerateMaterials(content.materials)
    GameUIStarSystemMedalEquip.selectedMaterialTable = {}
    GameUIStarSystemMedalEquip:GetTargetPosMedalList(GameFleetNewEnhance.curSelectedSlot)
    GameUIStarSystemMedalEquip:GetFlashObject():InvokeASCallback("_root", "refreshSelctListBox", math.ceil(#GameUIStarSystemMedalEquip.nowTargetPosMedalList / 3))
    GameUIStarSystemMedalEquip:ShowLevelUPMenu(GameUIStarSystemMedalEquip.nowSelectedMedal, false)
    GameUIStarSystemMedalEquip:GetFlashObject():InvokeASCallback("_root", "HideAllMaterialItem")
    GameUIStarSystemMedalEquip:ShowMedalDetail(GameFleetNewEnhance:GetMedalDetailById(GameUIStarSystemMedalEquip.nowSelectedMedal), false, true)
    GameFleetNewEnhance:RefreshFleetMedal()
    return true
  end
  return false
end
function GameUIStarSystemMedalEquip:DeleteConsumes(consumes)
  for k, v in ipairs(consumes) do
    for k2, v2 in ipairs(GameFleetNewEnhance._AllMedalData.medals or {}) do
      if v2.id == v then
        table.remove(GameFleetNewEnhance._AllMedalData.medals, k2)
      end
    end
    for k3, v3 in ipairs(self.CurSlidingPool) do
      if v3.id == v then
        table.remove(self.CurSlidingPool, k3)
      end
    end
  end
end
function GameUIStarSystemMedalEquip:ShowRankUPMenu(id, isMoveIn, isRankUpCallback)
  local medal = GameFleetNewEnhance:GetMedalDetailById(id)
  local medalDetail = {}
  self.nowSelectedMedal = medal.id
  medalDetail.iconFram, medalDetail.picName = GameHelper:GetMedalIconFrameAndPicNew(medal.type)
  medalDetail.rank = medal.rank
  medalDetail.can_rank = true
  medalDetail.Lv = medal.level
  medalDetail.name = GameHelper:GetAwardTypeText("medal", medal.type)
  medalDetail.levelLimit = GameLoader:GetGameText("LC_MENU_MEDAL_RANK_UP_REQUIRE") .. medal.rank_level_limit
  medalDetail.rank_level_limit = medal.rank_level_limit
  medalDetail.quality = medal.quality
  medalDetail.textRank = GameLoader:GetGameText("LC_MENU_MEDAL_MEDAL_RANK")
  local attrs = self:MergeAllPropertyWithOutText(medal.attr, medal.attr_level, medal.attr_rank)
  local ran_up_check = {}
  table.insert(ran_up_check, medal.rank ~= 6 and medal.level >= medal.rank_level_limit)
  DebugOutPutTable(medal, "MergeAllPropertyWithOutText")
  if medal.rank == 6 then
    attrs = self:MergeAllProperty(medal.attr, medal.attr_level, medal.attr_rank)
  else
    for k, v in ipairs(medal.attr_next_rank) do
      local detail = {}
      detail.propertyName = GameUIStarSystemMedalEquip:GetAttrName(v.attr_id)
      for k1, v1 in ipairs(medal.attr_rank) do
        if v1.attr_id == v.attr_id then
          detail.propertyAdd = v.attr_value - v1.attr_value
        end
      end
      if not detail.propertyAdd then
        detail.propertyAdd = v.attr_value
      end
      for k2, v2 in ipairs(attrs) do
        if v2.attr_id == v.attr_id then
          v2.propertyName = GameUIStarSystemMedalEquip:GetAttrName(v2.attr_id)
          v2.propertyValue = GameUIStarSystemMedalEquip:GetAttrValue(v2.attr_id, v2.attr_value)
          v2.propertyAdd = GameUIStarSystemMedalEquip:GetAttrValue(v2.attr_id, detail.propertyAdd)
          hasType = true
        end
      end
      if not hasType then
        detail.propertyValue = 0
        detail.propertyAdd = GameUIStarSystemMedalEquip:GetAttrValue(v.attr_id, detail.propertyAdd)
        table.insert(attrs, detail)
      end
    end
  end
  for k, v in ipairs(attrs) do
    if not v.propertyName then
      if v.attr_value == 0 then
        table.remove(attrs, k)
      else
        v.propertyName = GameUIStarSystemMedalEquip:GetAttrName(v.attr_id)
        v.propertyValue = GameUIStarSystemMedalEquip:GetAttrValue(v.attr_id, v.attr_value)
        v.propertyAdd = 0
      end
    end
  end
  GameUIStarSystemMedalEquip.RankConsumeList = {}
  for k, v in ipairs(medal.rank_consume.materials) do
    if v.type then
      local material_detail = {}
      material_detail.type = "item"
      material_detail.idx = k
      material_detail.iconFram = "medal_item_" .. v.type
      material_detail.picName = v.type .. ".png"
      material_detail.needNum = v.num
      material_detail.curNum = GameFleetNewEnhance:GetMaterialCntByType(v.type)
      material_detail.quality = v.quality
      table.insert(GameUIStarSystemMedalEquip.RankConsumeList, material_detail)
      table.insert(ran_up_check, material_detail.curNum >= material_detail.needNum)
    end
  end
  for k, v in ipairs(medal.rank_consume.medals) do
    if v.type then
      local material_detail = {}
      material_detail.type = "medal"
      material_detail.idx = k
      material_detail.iconFram, material_detail.picName = GameHelper:GetMedalIconFrameAndPic(v.type)
      material_detail.needNum = v.num
      material_detail.quality = v.quality
      material_detail.curNum = GameFleetNewEnhance:GetMedalCanMaterialCntByType(v.type, medal.id)
      table.insert(GameUIStarSystemMedalEquip.RankConsumeList, material_detail)
      table.insert(ran_up_check, material_detail.curNum >= material_detail.needNum)
    end
  end
  for k, v in ipairs(ran_up_check) do
    medalDetail.can_rank = medalDetail.can_rank and v
  end
  DebugOutPutTable(GameUIStarSystemMedalEquip.RankConsumeList, "ShowRankUPMenu:" .. id)
  GameUIStarSystemMedalEquip.Slid_idx = 1
  local slid_pool_cnt = #GameUIStarSystemMedalEquip.CurSlidingPool
  for k, v in ipairs(GameUIStarSystemMedalEquip.CurSlidingPool) do
    if v.id == medal.id then
      GameUIStarSystemMedalEquip.Slid_idx = k
      break
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "SetRankUPMenuData", medalDetail, attrs, GameUIStarSystemMedalEquip.RankConsumeList, self.Slid_idx, slid_pool_cnt, isMoveIn, isRankUpCallback)
end
function GameUIStarSystemMedalEquip:ConsumeClicked(idx)
  local medal = GameFleetNewEnhance:GetMedalDetailById(self.nowSelectedMedal)
  local consume = {}
  local consume_type = ""
  local e_quality = 0
  if idx <= #medal.rank_consume.materials then
    consume = medal.rank_consume.materials[idx]
    consume_type = "medal_item"
    local item = {
      item_type = consume_type,
      number = consume.type,
      no = 1,
      quality = consume.quality
    }
    ItemBox:showItemBox("Medal", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
  else
    local param = {
      type = medal.rank_consume.medals[idx - #medal.rank_consume.materials].type
    }
    NetMessageMgr:SendMsg(NetAPIList.medal_config_info_req.Code, param, _onResetPopIconCallback, true, nil)
  end
end
function _onResetPopIconCallback(msgtype, content)
  if msgtype == NetAPIList.medal_config_info_ack.Code then
    GameUIStarSystemRecycle:showMedalDetail(content.medal)
    return true
  elseif msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.medal_config_info_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  return false
end
function GameUIStarSystemMedalEquip:ChangeRankUPMedal(param)
  local medal = self.CurSlidingPool[self.Slid_idx + param]
  if medal then
    GameUIStarSystemMedalEquip:ShowRankUPMenu(medal.id, false)
  end
end
function GameUIStarSystemMedalEquip:ConfirmRankUP()
  local medal = GameFleetNewEnhance:GetMedalDetailById(self.nowSelectedMedal)
  local param = {}
  param.id = self.nowSelectedMedal
  param.rank_target = medal.rank + 1
  NetMessageMgr:SendMsg(NetAPIList.medal_rank_up_req.Code, param, GameUIStarSystemMedalEquip.RanklUPCallback, true, nil)
end
function GameUIStarSystemMedalEquip.RanklUPCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.medal_rank_up_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.medal_rank_up_ack.Code then
    DebugOutPutTable(content, "RanklUPCallback")
    GameFleetNewEnhance._AllMedalData.materials = content.materials
    GameUIStarSystemMedalEquip:DeleteConsumes(content.consumes)
    GameFleetNewEnhance:GenerateAvailableMedal()
    GameFleetNewEnhance:GenerateMaterials(content.materials)
    GameUIStarSystemMedalEquip:GetTargetPosMedalList(GameFleetNewEnhance.curSelectedSlot)
    GameUIStarSystemMedalEquip:GetFlashObject():InvokeASCallback("_root", "refreshSelctListBox", math.ceil(#GameUIStarSystemMedalEquip.nowTargetPosMedalList / 4))
    GameUIStarSystemMedalEquip:ShowRankUPMenu(GameUIStarSystemMedalEquip.nowSelectedMedal, false, true)
    GameUIStarSystemMedalEquip:ShowMedalDetail(GameFleetNewEnhance:GetMedalDetailById(GameUIStarSystemMedalEquip.nowSelectedMedal), false, true)
    GameFleetNewEnhance:RefreshFleetMedal()
    return true
  end
  return false
end
function GameUIStarSystemMedalEquip:UnLoadMedal()
  local param = {}
  param.medal_id = GameFleetNewEnhance.curSelectedMedal
  param.fleet_id = GameFleetNewEnhance:GetCurFleetId()
  param.formation_id = GameUIStarSystemMedalEquip:GetCurMatrixId()
  param.pos = GameFleetNewEnhance.curSelectedSlot
  NetMessageMgr:SendMsg(NetAPIList.medal_equip_off_req.Code, param, GameUIStarSystemMedalEquip.UnLoadMedalCallBack, true, nil)
end
function GameUIStarSystemMedalEquip.UnLoadMedalCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.medal_equip_off_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.medal_equip_off_ack.Code then
    if true == content.success then
      GameFleetNewEnhance.curFleetMedals[GameFleetNewEnhance.curSelectedSlot] = nil
      local formation_info = {}
      formation_info.medal_id = GameFleetNewEnhance.curSelectedMedal
      formation_info.fleet_id = GameFleetNewEnhance:GetCurFleetId()
      formation_info.formation_id = GameUIStarSystemMedalEquip:GetCurMatrixId()
      formation_info.pos = GameFleetNewEnhance.curSelectedSlot
      GameFleetNewEnhance:EraseFormationData(formation_info)
      GameFleetNewEnhance.curSelectedMedal = nil
      GameUIStarSystemMedalEquip:GetFlashObject():InvokeASCallback("_root", "HideDetailMenu")
      GameFleetNewEnhance:GenerateAvailableMedal()
      GameFleetNewEnhance:SetMedalEquipSlot()
    end
    return true
  end
  return false
end
function GameUIStarSystemMedalEquip:GetAttrName(attr_id)
  return ...
end
function GameUIStarSystemMedalEquip:GetAttrValue(attr_id, attr_value)
  if attr_id == 19 or attr_id == 20 or attr_id == 21 or attr_id == 22 or attr_id == 37 then
    local ret = attr_value / 1000 * 100
    ret = GameUtils:keepTwoDecimalPlaces(ret)
    return string.format("%0.2f", ret) .. "%"
  else
    return attr_value
  end
end
function GameUIStarSystemMedalEquip.OnResetCallback(msgType, content)
  if msgType == NetAPIList.medal_reset_confirm_ack.Code then
    GameFleetNewEnhance._AllMedalData.materials = content.materials
    GameFleetNewEnhance:GenerateAvailableMedal()
    GameFleetNewEnhance:GenerateMaterials(content.materials)
    GameUIStarSystemMedalEquip:GetTargetPosMedalList(GameFleetNewEnhance.curSelectedSlot)
    GameUIStarSystemMedalEquip:GetFlashObject():InvokeASCallback("_root", "refreshSelctListBox", math.ceil(#GameUIStarSystemMedalEquip.nowTargetPosMedalList / 4))
    GameUIStarSystemMedalEquip:ShowMedalDetail(GameFleetNewEnhance:GetMedalDetailById(GameFleetNewEnhance.curSelectedMedal), false, true)
    GameFleetNewEnhance:RefreshFleetMedal()
    GameUIStarSystemRecycle:ShowReset()
    GameUIStarSystemRecycle.resetParam.strtitle = GameLoader:GetGameText("LC_MENU_MEDAL_RECYCLE_RESET_SUCCESS")
    local flash_obj = GameUIStarSystemRecycle:GetFlashObject()
    flash_obj:InvokeASCallback("_root", "ShowResetResult", GameUIStarSystemRecycle.resetParam)
    GameUIStarSystemRecycle.resetParam = nil
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.medal_reset_confirm_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUIStarSystemMedalEquip.OnMedalFormationNtf(content)
  DebugOutPutTable(content, "GameUIStarSystemMedalEquip.OnMedalFormationNtf")
  if GameFleetNewEnhance._AllMedalData then
    if not GameFleetNewEnhance._AllMedalData.formations then
      GameFleetNewEnhance._AllMedalData.formations = content.formations
    else
      for i = 1, #content.formations do
        local has = false
        for k, v in ipairs(GameFleetNewEnhance._AllMedalData.formations) do
          if content.formations[i].fleet_id == v.fleet_id and content.formations[i].formation_id == v.formation_id then
            has = true
            v.equips = content.formations[i].equips
          end
        end
        if not has then
          table.insert(GameFleetNewEnhance._AllMedalData.formations, content.formations[i])
        end
      end
    end
  end
end
function GameUIStarSystemMedalEquip:GetCurMatrixId()
  if GameFleetEquipment ~= nil and GameFleetEquipment.isTeamLeague then
    return (...), GameFleetEquipment
  else
    return GameGlobalData.GlobalData.curMatrixIndex
  end
end
function GameUIStarSystemMedalEquip:DisChangeMedal(medal_id)
  local param = {}
  param.fleet_id = GameFleetNewEnhance:GetCurFleetId()
  param.formation_id = GameUIStarSystemMedalEquip:GetCurMatrixId()
  param.pos = GameFleetNewEnhance.curSelectedSlot
  param.medal_id = medal_id
  DebugOutPutTable(param, "medal_equip_off_req")
  NetMessageMgr:SendMsg(NetAPIList.medal_equip_off_req.Code, param, self.DisChangeMedalCallBack, true, nil)
end
function GameUIStarSystemMedalEquip.DisChangeMedalCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.medal_equip_off_req.Code then
    local text = AlertDataList:GetTextFromErrorCode(content.code)
    GameTip:Show(text, 3000)
    return true
  elseif msgType == NetAPIList.medal_equip_off_ack.Code then
    if content.success == true then
      local medalEquips = {}
      local formation_info = {}
      formation_info.medal_id = GameFleetNewEnhance.curFleetMedals[GameFleetNewEnhance.curSelectedSlot]
      formation_info.fleet_id = GameFleetNewEnhance:GetCurFleetId()
      formation_info.formation_id = GameFleetEquipment:GetCurMatrixId()
      formation_info.pos = GameFleetNewEnhance.curSelectedSlot
      local formation
      for k, v in ipairs(GameFleetNewEnhance._AllMedalData.formations) do
        if v.fleet_id == formation_info.fleet_id and v.formation_id == formation_info.formation_id then
          table.remove(GameFleetNewEnhance._AllMedalData.formations, k)
          break
        end
      end
      GameFleetNewEnhance.curFleetMedals[GameFleetNewEnhance.curSelectedSlot] = nil
      GameUIStarSystemMedalEquip:GetFlashObject():InvokeASCallback("_root", "HideDetailMenu")
      GameFleetNewEnhance:SetMedalEquipSlot()
      GameFleetNewEnhance:GenerateAvailableMedal()
    end
    return true
  end
  return false
end
