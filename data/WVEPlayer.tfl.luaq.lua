WVEPlayer = luaClass(nil)
function WVEPlayer:ctor(id, startPoint, endPoint, speed, movedDistance, playerStatus)
  self.mID = id
  self.mName = name
  self.mStartPoint = startPoint
  self.mEndPoint = endPoint
  self.mCurrentPathID = nil
  self.mCurrentPathDistance = nil
  self.mCurrentMovedDistance = movedDistance
  self.mSpeed = speed
  self.mType = EWVEPlayerType.TYPE_PLAYER
  self.mTypeFrame = EWVEPlayerTypeFrame[self.mType]
  self.mStatus = playerStatus
end
function WVEPlayer:Move(startPointId, endPointID)
end
function WVEPlayer:Refresh(startPoint, endPoint, speed, movedDistance, playerStatus)
  self.mStartPoint = startPoint
  self.mEndPoint = endPoint
  self.mCurrentPathID = nil
  self.mCurrentPathDistance = nil
  self.mCurrentMovedDistance = movedDistance
  self.mSpeed = speed
  self.mType = EWVEPlayerType.TYPE_PLAYER
  self.mTypeFrame = EWVEPlayerTypeFrame[self.mType]
  self.mStatus = playerStatus
end
function WVEPlayer:GetStartPoint()
  DebugOut("WVEPlayer:GetStartPoint")
  return self.mStartPoint
end
function WVEPlayer:GetEndPoint()
  return self.mEndPoint
end
function WVEPlayer:StartBattle()
end
function WVEPlayer:EndBattle(battleResult)
end
function WVEPlayer:Killed()
end
function WVEPlayer:Revived()
end
function WVEPlayer:Teleport(startPointId, endPointID)
end
function WVEPlayer:UpdateStatus(playerContent)
end
