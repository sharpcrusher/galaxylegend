local GlobalData = GameGlobalData.GlobalData
local GameStateFormation = GameStateManager.GameStateFormation
local GameStateBattleMap = GameStateManager.GameStateBattleMap
local QuestTutorialRepairFleet = TutorialQuestManager.QuestTutorialRepairFleet
local GameObjectAdventure = LuaObjectManager:GetLuaObject("GameObjectAdventure")
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameObjectFormationBackground = LuaObjectManager:GetLuaObject("GameObjectFormationBackground")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIEnemyInfo = LuaObjectManager:GetLuaObject("GameUIEnemyInfo")
local GameUIArcaneEnemyInfo = LuaObjectManager:GetLuaObject("GameUIArcaneEnemyInfo")
local GameObjectTutorialCutscene = LuaObjectManager:GetLuaObject("GameObjectTutorialCutscene")
local GameUIEvent = LuaObjectManager:GetLuaObject("GameUIEvent")
local GameUISection = LuaObjectManager:GetLuaObject("GameUISection")
local GameStateConfrontation = GameStateManager.GameStateConfrontation
local GameStateArcane = GameStateManager.GameStateArcane
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local QuestTutorialFirstBattle = TutorialQuestManager.QuestTutorialFirstBattle
local QuestTutorialSkill = TutorialQuestManager.QuestTutorialSkill
local QuestTutorialBattleMapAfterRecruit = TutorialQuestManager.QuestTutorialBattleMapAfterRecruit
local QuestTutorialBuildFactory = TutorialQuestManager.QuestTutorialBuildFactory
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local QuestTutorialsFirstKillBoss = TutorialQuestManager.QuestTutorialsFirstKillBoss
local QuestTutorialBattleFailed = TutorialQuestManager.QuestTutorialBattleFailed
local GameObjectClimbTower = LuaObjectManager:GetLuaObject("GameObjectClimbTower")
local GameStateInstance = GameStateManager.GameStateInstance
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIStarSystemFuben = LuaObjectManager:GetLuaObject("GameUIStarSystemFuben")
local GameUICrusade = require("data1/GameUICrusade.tfl")
local GameUIArena = LuaObjectManager:GetLuaObject("GameUIArena")
local GameUIAdjutant = LuaObjectManager:GetLuaObject("GameUIAdjutant")
local GameUIGameUIAdvancedArenaLayer = require("data1/GameUIAdvancedArenaLayer.tfl")
require("data1/GameUITeamLeagueCup.tfl")
GameObjectFormationBackground.m_tutorialStory = {}
GameObjectFormationBackground.force = 0
GameObjectFormationBackground.isNeedRequestMatrixInfo = true
GameObjectFormationBackground.enemy_name = ""
GameObjectFormationBackground.enemy_avatar = ""
GameObjectFormationBackground.needAddHero = false
GameObjectFormationBackground.m_nextPlayerMatrix = nil
GameObjectFormationBackground.m_nextPlayerRestFleets = nil
GameObjectFormationBackground.mCurFreeMatrixSubIdx = 0
GameObjectFormationBackground.mainFleetList = {}
GameObjectFormationBackground.curMainfleetIndex = 1
local currentMatrixIndex = 0
local maxAvailableMatrix = 1
local haveRequestMatrixInfo = false
require("FleetMatrix.tfl")
function GameObjectFormationBackground.NetMsgCallbacks(msgType, content)
  DebugOut("GameObjectFormationBackground.NetMsgCallbacks ")
  DebugOut("content")
  DebugTable(content)
  if msgType == NetAPIList.battle_matrix_ack.Code then
    GameObjectFormationBackground:SetEnemiesMatrixData(content.monsters.cells)
    GameObjectFormationBackground.force = content.force
    if content.force == nil then
      GameObjectFormationBackground.force = 0
    end
    GameObjectFormationBackground.ExtraHeroData = content.addition
    DebugOut("ExtraHeroData:")
    DebugTable(GameObjectFormationBackground.ExtraHeroData)
    GameObjectFormationBackground:UpdateEnemyInfo()
    GameObjectFormationBackground:SelectCommander(1)
    return true
  end
  return false
end
function GameObjectFormationBackground:AddExtraHero(data)
  if not data then
    return
  end
  DebugOut("AddExtraHero:")
  DebugTable(data)
  for k, v in pairs(data) do
    if self.m_formationFleets[v.pos] == 0 or not self.m_formationFleets[v.pos] then
      self.m_formationFleets[v.pos] = v.identity
    else
      DebugOut("AddExtraHero:pos has fleet already.", v.pos)
    end
  end
end
function GameObjectFormationBackground:IsExtraHero(pos, identity)
  DebugOut("IsExtraHero:", pos, identity)
  DebugTable(self.ExtraHeroData)
  if not self.ExtraHeroData or not pos or not identity then
    return false
  end
  for k, v in pairs(self.ExtraHeroData) do
    if v.pos == pos and v.identity == identity then
      DebugOut("true")
      return true
    end
  end
  DebugOut("false")
  return false
end
function GameObjectFormationBackground:SetEnemiesMatrixData(enemyMatrixData)
  self.enemy_matrix = enemyMatrixData
  self.m_enemysFormation = {
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0
  }
  for _, data_monster in pairs(self.enemy_matrix) do
    GameObjectFormationBackground.m_enemysFormation[data_monster.cell_id] = data_monster.identity
  end
end
function GameObjectFormationBackground:GetEnemySex(identify)
  local result = 2
  for k, v in pairs(self.enemy_matrix) do
    if v.identity == identify then
      result = v.sex
    end
  end
  return result
end
function GameObjectFormationBackground:Init()
  DebugOut("Init()")
  self:Clear()
  if not self.m_enemysFormation then
    self.m_enemysFormation = {}
    for i_cell = 1, 9 do
      self.m_enemysFormation[i_cell] = 0
    end
  end
  local inSystem = GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState()
  if not haveRequestMatrixInfo then
    if inSystem then
      FleetMatrix.Matrix_Type = FleetMatrix.MATRIX_TYPE_BURNING
      FleetMatrix:GetMatrixsReq(GameObjectFormationBackground.requestMultiMatrixAck, FleetMatrix.MATRIX_TYPE_BURNING)
    else
      FleetMatrix.Matrix_Type = FleetMatrix.MATRIX_TYPE_NONE
      FleetMatrix:GetMatrixsReq(GameObjectFormationBackground.requestMultiMatrixAck)
    end
    DebugOut("FleetMatrix.Matrix_Type:" .. FleetMatrix.Matrix_Type)
  end
end
function GameObjectFormationBackground.requestMultiMatrixAck()
  local flash_obj = GameObjectFormationBackground:GetFlashObject()
  if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
    GameObjectFormationBackground:InitForCrusade()
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "setCurrentMatrix", FleetMatrix:getCurrentMartix(FleetMatrix.MATRIX_TYPE_BURNING).name)
    end
  else
    GameObjectFormationBackground.requestMultiMatrixCallBack(NetAPIList.mulmatrix_get_ack.Code, FleetMatrix)
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "setCurrentMatrix", FleetMatrix:getCurrentMartix().name)
    end
  end
end
function GameObjectFormationBackground.CommonNetMessageCallback(msgtype, content)
  DebugOut("CommonNetMessageCallback:", msgtype)
  DebugTable(content)
  if msgtype == NetAPIList.common_ack.Code and (content.api == NetAPIList.user_matrix_save_req.Code or content.api == NetAPIList.mulmatrix_buy_req.Code) then
    if content.api == NetAPIList.user_matrix_save_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      elseif GameObjectFormationBackground.mSaveFormationBeforeGotoAdjutant then
        GameObjectFormationBackground.mSaveFormationBeforeGotoAdjutant()
        GameObjectFormationBackground.mSaveFormationBeforeGotoAdjutant = nil
      else
        GameObjectFormationBackground:BattleNow()
      end
    elseif content.api == NetAPIList.mulmatrix_buy_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      else
        DebugOut("purchase success")
      end
    end
    return true
  end
  return false
end
function GameObjectFormationBackground:HasTutorialStroy()
  return self.m_tutorialStory ~= nil and #self.m_tutorialStory ~= 0
end
function GameObjectFormationBackground:SetTutorialStory(story)
  DebugOut("SetTutorialStory")
  self.m_tutorialStory = story
end
function GameObjectFormationBackground:Clear()
  self.m_formationFleets = {}
  self.m_freeFleets = {}
  self.m_fleetIDToSortedIndex = {}
  self.m_nextPlayerMatrix = {}
  self.m_nextPlayerRestFleets = {}
end
function GameObjectFormationBackground:clearMultiMatrixData()
  GameObjectFormationBackground.m_nextPlayerMatrix = nil
  GameObjectFormationBackground.m_nextPlayerRestFleets = nil
  currentMatrixIndex = 0
  maxAvailableMatrix = 1
  haveRequestMatrixInfo = false
end
function GameObjectFormationBackground:InitFlashObject()
end
function GameObjectFormationBackground:CheckQuestTutorial(currentFleet)
  DebugOut("GameObjectFormationBackground:CheckQuestTutorial:")
  DebugOut(QuestTutorialFirstBattle:IsActive())
  DebugOut(QuestTutorialBattleMapAfterRecruit:IsActive())
  if QuestTutorialFirstBattle:IsActive() then
    self:GetFlashObject():InvokeASCallback("_root", "showBattleTipAnimation")
  else
    self:GetFlashObject():InvokeASCallback("_root", "hideBattleTipAnimation")
  end
  if QuestTutorialBattleMapAfterRecruit:IsActive() then
    local function callback()
      if self:GetFlashObject() then
        self:GetFlashObject():InvokeASCallback("_root", "SetBackup_arrowHighright")
      end
    end
    if immanentversion == 2 then
      GameUICommonDialog:PlayStory({51148}, callback)
    else
      GameUICommonDialog:PlayStory({100021}, callback)
    end
  else
    self:GetFlashObject():InvokeASCallback("_root", "SetBackup_arrowSkillTipDismiss")
  end
  DebugOut("GameObjectFormationBackground:CheckQuestTutorial() ", GameUIEnemyInfo.m_combinedBattleID)
end
function GameObjectFormationBackground:InitFormation(selfMatrix, fleetList)
  DebugOut("InitFormation")
  self:Clear()
  local fleets_matrix = GameGlobalData:GetData("matrix").cells
  if selfMatrix then
    fleets_matrix = selfMatrix.cells
  end
  local leaderlist = GameGlobalData:GetData("leaderlist")
  self.curMainfleetIndex = 1
  self.mainFleetList = leaderlist.options
  local curr_id = leaderlist.leader_ids[currentMatrixIndex]
  if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
    curr_id = GameUICrusade:GetRealFleetID()
  end
  DebugTable(leaderlist)
  DebugOut("currentMatrixIndex", currentMatrixIndex)
  DebugOut("in InitFormation")
  DebugOut("curr_id:" .. curr_id)
  DebugOut("currentMatrixIndex" .. currentMatrixIndex)
  DebugTable(leaderlist)
  for k, v in ipairs(self.mainFleetList) do
    DebugOut("v:" .. v)
    if v == curr_id then
      self.curMainfleetIndex = k
      DebugOut("k ;;", k)
    end
  end
  DebugOut("curMainFleetIndex", self.curMainfleetIndex)
  DebugTable(self.mainFleetList)
  local formationIDs = {}
  for i = 1, 9 do
    self.m_formationFleets[i] = 0
  end
  for i, v in pairs(fleets_matrix) do
    self.m_formationFleets[v.cell_id] = v.fleet_identity
    if v ~= -1 then
      formationIDs[v.fleet_identity] = 1
    end
  end
  self:AddExtraHero(self.ExtraHeroData)
  for i, v in pairs(self.m_formationFleets) do
    self:UpdateBattleCommanderGrid(i, v)
  end
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  if fleetList then
    for i, v in pairs(fleetList) do
      if formationIDs[v.fleet_id] == nil then
        table.insert(GameObjectFormationBackground.m_freeFleets, v.fleet_id)
      end
      GameObjectFormationBackground.m_fleetIDToSortedIndex[v.fleet_id] = i
    end
  else
    for i, v in pairs(fleets) do
      if formationIDs[v.identity] == nil then
        table.insert(GameObjectFormationBackground.m_freeFleets, v.identity)
      end
      GameObjectFormationBackground.m_fleetIDToSortedIndex[v.identity] = i
    end
  end
  DebugOut("m_formationFleets")
  DebugTable(self.m_formationFleets)
  for i = 1, 9 do
    if not self.m_freeFleets[i] then
      self.m_freeFleets[i] = -1
    end
  end
  DebugOutPutTable(self.m_freeFleets, "freeFleets")
  for i, v in pairs(self.m_freeFleets) do
    self:UpdateRestCommanderGrid(i, v)
  end
  if immanentversion == 2 then
    local isLiAngInFormation = false
    for k, v in pairs(self.m_formationFleets) do
      if v == 2 then
        isLiAngInFormation = true
      end
    end
    if isLiAngInFormation and QuestTutorialBattleMapAfterRecruit:IsActive() then
      QuestTutorialBattleMapAfterRecruit:SetFinish(true)
    end
  end
  self:CheckQuestTutorial()
  self.m_nextPlayerMatrix = LuaUtils:table_rcopy(self.m_formationFleets)
  self.m_nextPlayerRestFleets = LuaUtils:table_rcopy(self.m_freeFleets)
end
function GameObjectFormationBackground:UpdatePlayerInfo()
  local player_info = GameGlobalData:GetUserInfo()
  local player_main_fleet = GameGlobalData:GetFleetInfo(1)
  DebugOut("self.curMainfleetIndex:" .. self.curMainfleetIndex)
  DebugTable(self.mainFleetList)
  local player_avatar = GameDataAccessHelper:GetFleetAvatar(self.mainFleetList[self.curMainfleetIndex], player_main_fleet.level)
  local player_name = GameUtils:GetUserDisplayName(player_info.name)
  self:GetFlashObject():InvokeASCallback("_root", "UpdatePlayerInfo", player_name, player_avatar)
end
function GameObjectFormationBackground:UpdateEnemyInfo()
  DebugOut("m_enemysFormation = ", self.m_enemysFormation)
  DebugTable(self.m_enemysFormation)
  for i, v in pairs(self.m_enemysFormation) do
    self:SetEnemyGridData(i, v)
  end
  self:UpdateEnemyPowerPoint()
  local enemyInfo
  if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
    DebugOut("111")
    enemyInfo = GameUICrusade:GetTriggerEnemyInfo()
  elseif GameStateFormation.m_prevState == GameStateManager.GameStateArena then
    DebugOut("2222")
    enemyInfo = GameUIArena.enemyInfo
  elseif self.isNeedRequestMatrixInfo then
    if GameStateFormation.m_currentAreaID ~= -1 or GameStateFormation.m_currentBattleID ~= -1 then
      local enemy_info = GameDataAccessHelper:GetBattleMonsterInfo(GameStateFormation.m_currentAreaID, GameStateFormation.m_currentBattleID)
      local enemy_avatar = enemy_info._avatar
      local enemy_name = GameDataAccessHelper:GetAreaNameText(GameStateFormation.m_currentAreaID, GameStateFormation.m_currentBattleID)
      enemyInfo = {}
      enemyInfo.name = enemy_name
      enemyInfo.avatar = enemy_avatar
      DebugOut("fsdfds =")
      DebugTable(enemyInfo)
    end
    GameObjectFormationBackground:GetFlashObject():SetBtnEnable("_root.main.btn_enter", true)
  else
    enemyInfo = {}
    enemyInfo.avatar = self.enemy_avatar
    enemyInfo.name = self.enemy_name
    DebugOut("GameObjectFormationBackground:UpdateEnemyInfo" .. "name: ", enemyInfo.name, "avatar: ", enemyInfo.avatar)
    self.isNeedRequestMatrixInfo = true
  end
  if enemyInfo then
    self:SetEnemyInfo(enemyInfo)
  end
end
function GameObjectFormationBackground:SetEnemyInfo(enemyInfo)
  DebugOut("enemyInfo ==")
  DebugTable(enemyInfo)
  self:GetFlashObject():InvokeASCallback("_root", "UpdateEnemyInfo", enemyInfo.name, enemyInfo.avatar)
end
GameObjectFormationBackground.mSaveFormationBeforeGotoAdjutant = nil
function GameObjectFormationBackground:SaveFormationToServer(isNeedBlock, beforeGotoAdjutantCallback)
  DebugOut("GameObjectFormationBackground:SaveFormationToServer")
  if GameObjectTutorialCutscene:IsActive() then
    return
  end
  local formation = {}
  formation.cells = {}
  formation.count = 0
  for k, v in pairs(GameObjectFormationBackground.m_formationFleets) do
    local data = {}
    data.cell_id = k
    if v > 0 and not self:IsExtraHero(k, v) then
      local fleet_info = GameGlobalData:GetFleetInfo(v)
      if fleet_info then
        data.fleet_identity = fleet_info.identity
        data.fleet_spell = fleet_info.active_spell
      else
        data.fleet_identity = -1
        data.fleet_spell = -1
      end
      table.insert(formation.cells, data)
    elseif v == -1 then
      data.fleet_identity = -1
      data.fleet_spell = -1
      table.insert(formation.cells, data)
    end
  end
  DebugOutPutTable(formation.cells, "cells")
  GameObjectFormationBackground.mSaveFormationBeforeGotoAdjutant = beforeGotoAdjutantCallback
  if TutorialQuestManager.QuestTutorialComboGachaUseHero:IsActive() then
    for k, v in ipairs(formation.cells) do
      if v.fleet_identity == 215 then
        AddFlurryEvent("Gacha_UseHero", {}, 1)
        TutorialQuestManager.QuestTutorialComboGachaUseHero:SetActive(false, true)
        TutorialQuestManager.QuestTutorialComboGachaUseHero:SetFinish(true, false)
        break
      end
    end
  end
  NetMessageMgr:SendMsg(NetAPIList.user_matrix_save_req.Code, formation, GameObjectFormationBackground.CommonNetMessageCallback, isNeedBlock, nil)
end
function GameObjectFormationBackground.callback(...)
end
function GameObjectFormationBackground:SetEnemyFog(isVisible)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "setEnemyFog", isVisible)
  end
end
function GameObjectFormationBackground:OnAddToGameState(state)
  GameObjectFormationBackground.mSaveFormationBeforeGotoAdjutant = nil
  DebugOut("GameObjectFormationBackground:OnAddToGameState")
  GameObjectFormationBackground:LoadFlashObject()
  self.m_currentSelectFleetId = 1
  GameObjectFormationBackground.mCurFreeMatrixSubIdx = 0
  GameObjectFormationBackground:SetEnemyFog(false)
  DebugOut("ExtraHeroData:set nil")
  DebugOut(GameStateFormation.m_currentAreaID, ",", GameStateFormation.m_currentBattleID)
  self.ExtraHeroData = nil
  if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
    local flashObj = self:GetFlashObject()
    if flashObj then
      flashObj:InvokeASCallback("_root", "SetMatrixChangeBtnVisible", false)
    end
  elseif GameStateFormation.m_currentAreaID == -1 and GameStateFormation.m_currentBattleID == -1 and self.isNeedRequestMatrixInfo then
    DebugOut("A section")
    GameObjectFormationBackground:GetFlashObject():SetBtnEnable("_root.main.btn_enter", false)
  elseif GameStateFormation.m_prevState == GameStateArcane then
    DebugOut("B section")
    DebugTable(self.dataProvider:GetEnemiesMatrix())
    self:SetEnemiesMatrixData(self.dataProvider:GetEnemiesMatrix())
  elseif GameStateFormation.m_prevState == GameStateManager.GameStateArena then
    DebugOut("E section GameStateArena")
    GameObjectFormationBackground:SetEnemyFog(true)
    DebugTable(GameObjectFormationBackground.m_enemysFormation)
  elseif not self.m_enemysFormation then
    DebugOut("C section")
    self:RequestEnemyFormation()
  else
    if GameStateFormation.m_currentAreaID ~= -1 and GameStateFormation.m_currentBattleID ~= -1 and self.isNeedRequestMatrixInfo then
      self:RequestEnemyFormation()
    end
    DebugOut("D section")
  end
  self:Init()
  if immanentversion == 1 and GameStateFormation.m_currentAreaID == 1 and GameStateFormation.m_currentBattleID == 2012 and not QuestTutorialSkill:IsActive() and not QuestTutorialSkill:IsFinished() then
    QuestTutorialSkill:SetActive(true)
  end
  if QuestTutorialSkill:IsActive() then
    self:GetFlashObject():InvokeASCallback("_root", "EnableDragFleet", false)
    local function callback()
      self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialClickMC")
    end
    if immanentversion == 2 then
      GameUICommonDialog:PlayStory({1100015}, callback)
    elseif immanentversion == 1 then
      GameUICommonDialog:PlayStory({100012}, callback)
    end
  end
end
function GameObjectFormationBackground:CheckPlayExtraHeroStory()
  if GameStateFormation.m_currentAreaID == 1 and GameStateFormation.m_currentBattleID == 1002 then
    DebugOut("CheckPlayExtraHeroStory:playstory")
  end
end
function GameObjectFormationBackground:OnEraseFromGameState(state)
  GameObjectFormationBackground:Clear()
  GameObjectFormationBackground:clearMultiMatrixData()
  self.m_enemysFormation = nil
  self.jamedCount = nil
  self.ExtraHeroData = nil
  DebugOut("OnEraseFromGameState: self.ExtraHeroData = nil")
  DebugOut(GameStateFormation.m_currentAreaID, ",", GameStateFormation.m_currentBattleID)
  GameObjectFormationBackground.m_renderFx = nil
  GameObjectFormationBackground.force = 0
  self:UnloadFlashObject()
  FleetMatrix.system_index = nil
end
function GameObjectFormationBackground:AnimationMoveIn()
  DebugOut("GameObjectFormationBackground:AnimationMoveIn")
  self:GetFlashObject():InvokeASCallback("_root", "AnimationMoveIn")
end
function GameObjectFormationBackground:AnimationMoveOut()
  self:GetFlashObject():InvokeASCallback("_root", "AnimationMoveOut")
end
function GameObjectFormationBackground:UpdatePlayerPowerPoint()
  local power_point = 0
  for _, v in pairs(self.m_formationFleets) do
    if v > 0 then
      local fleet_info
      if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
        fleet_info = GameUICrusade:GetShipById(v)
        if 0 < fleet_info.hp then
          power_point = power_point + fleet_info.force
        end
      else
        DebugOut("UpdatePlayerPowerPoint:", v)
        fleet_info = GameGlobalData:GetFleetInfo(v)
        if fleet_info then
          power_point = power_point + fleet_info.force
        end
      end
    end
  end
  local powerText = GameUtils.numberConversion2(power_point)
  if GameStateManager.GameStateFormation.isArenaEnter then
    powerText = GameStateManager.GameStateFormation.ArenaForce
  end
  self:GetFlashObject():InvokeASCallback("_root", "UpdatePowerPoint", GameUtils.numberAddComma(powerText), GameUtils.numberAddComma(GameObjectFormationBackground.force))
end
function GameObjectFormationBackground:UpdateCommanderJamed()
  if self.jamedCount and self.m_formationFleets then
    local jamedLeftCount = self.jamedCount
    local battleCommanderCount = self:CountBattleCommander()
    local jamedTable = {}
    for indexCommander = 9, 1, -1 do
      if self.m_formationFleets[indexCommander] == nil then
        DebugOut("error :indexCommander", indexCommander)
      end
      if jamedLeftCount > 0 and battleCommanderCount > 1 and self.m_formationFleets[indexCommander] ~= nil and self.m_formationFleets[indexCommander] > 0 then
        jamedTable[indexCommander] = 1
        jamedLeftCount = jamedLeftCount - 1
        battleCommanderCount = battleCommanderCount - 1
      else
        jamedTable[indexCommander] = -1
      end
    end
    local jamedDataString = table.concat(jamedTable, ",")
    self:GetFlashObject():InvokeASCallback("_root", "setCommanderJamedData", jamedDataString)
  end
end
function GameObjectFormationBackground:UpdateEnemyPowerPoint()
  self:GetFlashObject():InvokeASCallback("_root", "UpdatePowerPoint", -1, GameUtils.numberAddComma(GameObjectFormationBackground.force))
end
function GameObjectFormationBackground:SelectCommanderSkill(commander_id, skill_id)
  local commander_data = GameGlobalData:GetFleetInfo(commander_id)
  if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
    commander_data = GameUICrusade:GetSelfFleet(commander_id)
    GameUICrusade:SetActiveSpell(skill_id)
  end
  if not commander_data then
    return
  end
  commander_data.active_spell = skill_id
  FleetMatrix:SetCurrentSkill(commander_id, skill_id)
  local leaderlist = GameGlobalData:GetData("leaderlist")
  local curSpells = leaderlist.active_spells
  for indexSkill = 1, GameGlobalData:GetNumMaxSkills() do
    if curSpells[indexSkill] == skill_id then
      local skillinfo = GameDataAccessHelper:GetSkillInfo(skill_id)
      local datatable = {}
      datatable[#datatable + 1] = GameDataAccessHelper:GetSkillNameText(skill_id)
      datatable[#datatable + 1] = GameDataAccessHelper:GetSkillDesc(skill_id)
      local skillrangetype = GameDataAccessHelper:GetSkillRangeType(skill_id)
      datatable[#datatable + 1] = "TYPE_" .. skillrangetype
      datatable[#datatable + 1] = GameLoader:GetGameText("LC_MENU_SKILL_RANGE_TYPE_" .. skillrangetype)
      local datastring = table.concat(datatable, "\001")
      self:GetFlashObject():InvokeASCallback("_root", "selectCommanderSkill", indexSkill, datastring)
      break
    end
  end
  self:UpdateCommanderSkillAttackRange(self:GetBattleCommanderGridIndex(commander_id), skill_id)
end
function GameObjectFormationBackground:ExpandSkillSelectorNew(index)
  local commander_id = self.mainFleetList[index]
  if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
    commander_id = GameUICrusade:GetRealFleetID()
  end
  local grid_type, index_commander = self:GetCommanderGridIndex(1)
  self:UpdateBattleCommanderGrid(index_commander, 1)
  self:UpdatePlayerInfo()
  self:SelectMainFleet(index)
end
function GameObjectFormationBackground:ShowMainFleetSelector(id_commander)
  DebugOut("GameObjectFormationBackground ExpandSkillSelector", id_commander)
  if id_commander ~= 1 then
    return
  end
  local grid_type, index_commander = self:GetCommanderGridIndex(id_commander)
  if grid_type == "rest" then
    return
  end
  local commander_data = GameGlobalData:GetFleetInfo(id_commander)
  local param = {}
  for k, v in ipairs(self.mainFleetList) do
    local param1 = {}
    param1.avatar = GameDataAccessHelper:GetFleetAvatar(v, 0)
    table.insert(param, param1)
  end
  if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
    commander_data = GameUICrusade:GetSelfFleet(id_commander)
  end
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "ShowMainFleetSelector", index_commander, param, self.curMainfleetIndex)
  end
  self:ExpandSkillSelectorNew(self.curMainfleetIndex)
end
function GameObjectFormationBackground:setMainFleet()
  local leaderdata = GameGlobalData:GetData("leaderlist")
  if leaderdata.leader_ids[currentMatrixIndex] == self.mainFleetList[self.curMainfleetIndex] or not self.mainFleetList[self.curMainfleetIndex] then
    DebugOut("nil value")
    return
  end
  local param = {}
  param.id = self.mainFleetList[self.curMainfleetIndex]
  NetMessageMgr:SendMsg(NetAPIList.change_leader_req.Code, param, self.ChangeLeaderCallback, false)
end
function GameObjectFormationBackground.ChangeLeaderCallback(msgType, content)
  DebugOut("hangeLeaderCallback")
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.change_leader_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    else
      local grid_type, index_commander = GameObjectFormationBackground:GetCommanderGridIndex(1)
      GameObjectFormationBackground:UpdateBattleCommanderGrid(index_commander, 1, true)
      GameObjectFormationBackground:UpdateBigHead()
    end
    return true
  end
  return false
end
function GameObjectFormationBackground:UpdateBigHead()
  local player_info = GameGlobalData:GetUserInfo()
  local player_main_fleet = GameGlobalData:GetFleetInfo(1)
  local player_avatar = GameDataAccessHelper:GetFleetAvatar(self.mainFleetList[self.curMainfleetIndex], player_main_fleet.level)
  local player_name = GameUtils:GetUserDisplayName(player_info.name)
  self:GetFlashObject():InvokeASCallback("_root", "UpdatePlayerInfo", player_name, player_avatar)
end
function GameObjectFormationBackground:ShrinkSkillSelector()
  self:GetFlashObject():InvokeASCallback("_root", "ShrinkSkillSelector")
end
function GameObjectFormationBackground:UpdateCommanderSkillAttackRange(attack_grid, id_skill)
  DebugOut("UpdateCommanderSkillAttackRange ", attack_grid, id_skill)
  local attack_type = -1
  local target_pos = -1
  local isBackAttack = false
  if id_skill then
    attack_type = GameDataAccessHelper:GetSkillRangeType(id_skill)
  end
  if attack_type == GameGlobalData.AttackRangeType.Back then
    isBackAttack = true
  end
  if attack_grid then
    target_pos = self:ComputeTargetPos(attack_grid, isBackAttack)
  end
  local target_table = self:ComputeAttackRange(target_pos, attack_type)
  local target_data = ""
  for i = 1, 9 do
    if target_data ~= "" then
      target_data = target_data .. ","
    end
    target_data = target_data .. tostring(target_table[i])
  end
  self:GetFlashObject():InvokeASCallback("_root", "UpdateCommanderSkillAttackRange", target_data)
end
function GameObjectFormationBackground:ComputeTargetPos(index_grid, isBackAttack)
  DebugOut("ComputeTargetPos ", index_grid)
  assert(index_grid and index_grid >= 1 and index_grid <= 9)
  local ypos = (index_grid - 1) % 3 + 1
  local order = GameGlobalData.AttackTargetOrder[ypos]
  local startGrid = 1
  local endGrid = 9
  local step = 1
  if isBackAttack then
    order = GameGlobalData.AttackTargetOrderBack[ypos]
  end
  for i = startGrid, endGrid, step do
    if self.m_enemysFormation[order[i]] and self.m_enemysFormation[order[i]] > 0 then
      DebugOut("target pos - ", order[i])
      return order[i]
    end
  end
end
function GameObjectFormationBackground:ComputeAttackRange(target_pos, attack_type)
  local target_table = {}
  for i = 1, 9 do
    target_table[i] = -1
  end
  local IsTargetInRange = function(target)
    return target >= 1 and target <= 9
  end
  if target_pos and IsTargetInRange(target_pos) then
    local AttackRangeType = GameGlobalData.AttackRangeType
    if attack_type == AttackRangeType.Monomer or attack_type == AttackRangeType.Back then
      target_table[target_pos] = 1
    elseif attack_type == AttackRangeType.Vertical then
      local start_pos = math.floor((target_pos - 1) / 3) * 3
      for i = 1, 3 do
        local attack_pos = start_pos + i
        if IsTargetInRange(attack_pos) then
          target_table[attack_pos] = 1
        end
      end
    elseif attack_type == AttackRangeType.Horizontal then
      local start_pos = math.floor((target_pos - 1) % 3)
      for i = 0, 2 do
        local attack_pos = i * 3 + start_pos + 1
        if IsTargetInRange(attack_pos) then
          target_table[attack_pos] = 1
        end
      end
    elseif attack_type == AttackRangeType.Cross then
      local check_table
      if target_pos == 1 or target_pos == 4 or target_pos == 7 then
        check_table = {
          target_pos,
          target_pos + 1,
          target_pos + 2,
          target_pos - 3,
          target_pos - 6,
          target_pos + 3,
          target_pos + 6
        }
      elseif target_pos == 2 or target_pos == 5 or target_pos == 8 then
        check_table = {
          target_pos,
          target_pos - 1,
          target_pos + 1,
          target_pos - 3,
          target_pos - 6,
          target_pos + 3,
          target_pos + 6
        }
      else
        check_table = {
          target_pos,
          target_pos - 1,
          target_pos - 2,
          target_pos - 3,
          target_pos - 6,
          target_pos + 3,
          target_pos + 6
        }
      end
      for _, target_check in ipairs(check_table) do
        if IsTargetInRange(target_check) then
          target_table[target_check] = 1
        end
      end
    elseif attack_type == AttackRangeType.Plenary then
      for i = 1, 9 do
        target_table[i] = 1
      end
    elseif attack_type == 0 then
    else
      target_table[target_pos] = 1
    end
  end
  return target_table
end
function GameObjectFormationBackground:SelectMainFleet(fleet_index)
  DebugOut("GameObjectFormationBackground:SelectMainFleet:" .. fleet_index)
  DebugTable(self.mainFleetList)
  local fleet_id = self.mainFleetList[fleet_index]
  if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
    fleet_id = GameUICrusade:GetRealFleetID()
  end
  local commander_data = GameGlobalData:GetFleetInfo(1)
  local num_max_skill = GameGlobalData:GetNumMaxSkills()
  if fleet_id == 1 then
    self:GetFlashObject():InvokeASCallback("_root", "ExpandSkillSelectorNew", fleet_index)
    DebugOut("active_spell: ", commander_data.active_spell)
    local leaderlist = GameGlobalData:GetData("leaderlist")
    local curSpells = leaderlist.active_spells
    DebugTable(curSpells)
    for index_skill = 1, num_max_skill do
      local id_skill = curSpells[index_skill]
      DebugOut("id_skill: ", id_skill)
      id_skill = id_skill or -1
      local is_active = id_skill == commander_data.active_spell
      local grid_type, index_commander = self:GetCommanderGridIndex(1)
      self:GetFlashObject():InvokeASCallback("_root", "UpdateCommanderSkillItem", index_commander, index_skill, id_skill, false)
      if is_active then
        self:SelectCommanderSkill(id_commander, id_skill)
      end
    end
  else
    local basic_info = GameDataAccessHelper:GetCommanderBasicInfo(fleet_id)
    local skill_id = basic_info.SPELL_ID
    local skillinfo = GameDataAccessHelper:GetSkillInfo(skill_id)
    local datatable = {}
    datatable[#datatable + 1] = GameDataAccessHelper:GetSkillNameText(skill_id)
    datatable[#datatable + 1] = GameDataAccessHelper:GetSkillDesc(skill_id)
    local skillrangetype = GameDataAccessHelper:GetSkillRangeType(skill_id)
    datatable[#datatable + 1] = "TYPE_" .. skillrangetype
    datatable[#datatable + 1] = GameLoader:GetGameText("LC_MENU_SKILL_RANGE_TYPE_" .. skillrangetype)
    local datastring = table.concat(datatable, "\001")
    DebugOut("dataString:", datastring)
    local grid_type, index_commander = self:GetCommanderGridIndex(1)
    self:GetFlashObject():InvokeASCallback("_root", "shrinkSkillSelectorNew")
    self:GetFlashObject():InvokeASCallback("_root", "selectCommanderSkillNew", fleet_index, datastring, index_commander)
  end
end
function GameObjectFormationBackground:SelectCommander(id_commander)
  local index_commander = -1
  local id_skill = -1
  local commander_identity = -1
  local grid_type
  if id_commander and id_commander > 0 then
    grid_type, index_commander = self:GetCommanderGridIndex(id_commander)
    if id_commander ~= 1 then
      local commander_data = GameGlobalData:GetFleetInfo(id_commander)
      if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
        commander_data = GameUICrusade:GetSelfFleet(id_commander)
      end
      if self:IsExtraHero(index_commander, id_commander) then
        commander_data = self:GenerateExtraFleetData(id_commander)
      end
      DebugOut("SelectCommander:", self:IsExtraHero(index_commander, id_commander))
      DebugTable(commander_data)
      id_skill = commander_data.active_spell
      commander_identity = commander_data.identity
    else
      local commander_data = GameGlobalData:GetFleetInfo(id_commander)
      id_skill = commander_data.active_spell
      local leaderlist = GameGlobalData:GetData("leaderlist")
      if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
        commander_data = GameUICrusade:GetSelfFleet(id_commander)
      end
      if self:IsExtraHero(index_commander, leaderlist.leader_ids[currentMatrixIndex]) then
        commander_data = self:GenerateExtraFleetData(leaderlist.leader_ids[currentMatrixIndex])
      end
      commander_identity = 1
    end
  end
  DebugOut("SelectCommander", id_commander)
  DebugOut("grid_type", grid_type)
  DebugOut("index_commander", index_commander)
  self:GetFlashObject():InvokeASCallback("_root", "SelectCommanderGrid", grid_type, index_commander, commander_identity)
  self:UpdateCommanderSkillAttackRange(index_commander, id_skill)
  self.selected_commander = id_commander
end
function GameObjectFormationBackground:UpdateMainfleetIcon(fleet_index)
  local commander_level = 0
  local commander_info = GameGlobalData:GetFleetInfo(1)
  if commander_info then
    commander_level = commander_info.level
  end
  local avatar = GameDataAccessHelper:GetFleetAvatar(self.mainFleetList[fleet_index], commander_level)
  local vessels = GameDataAccessHelper:GetCommanderVesselsImage(self.mainFleetList[fleet_index], commander_level, false)
end
function GameObjectFormationBackground:SelectCommanderNew(fleet_index)
  local commander_info = GameGlobalData:GetFleetInfo(1)
  local commander_level = 0
  local leaderlist = GameGlobalData:GetData("leaderlist")
  if commander_info then
    commander_level = commander_info.level
  end
  local commander_force = GameGlobalData:GetFleetInfo(1).force or 0
  local commander_name = ""
  local commander_avatar = ""
  local commander_vessels = ""
  local commander_color = ""
  local commander_vesselstype = ""
  local forceContext = GameLoader:GetGameText("LC_MENU_ARENA_CAPTION_FORCE")
  local basic_info = {}
  local player_info = GameGlobalData:GetData("userinfo")
  commander_name = GameUtils:GetUserDisplayName(player_info.name)
  commander_name = GameDataAccessHelper:CreateLevelDisplayName(commander_name, commander_level)
  local commanderType = -1
  commander_avatar = GameDataAccessHelper:GetFleetAvatar(self.mainFleetList[fleet_index], commander_level)
  commander_vesselstype = GameDataAccessHelper:GetCommanderVesselsType(self.mainFleetList[fleet_index], commander_level)
  commander_vessels = GameDataAccessHelper:GetCommanderVesselsImage(self.mainFleetList[fleet_index], commander_level, false)
  commander_color = GameDataAccessHelper:GetCommanderColorFrame(self.mainFleetList[fleet_index], commander_level)
  basic_info = GameDataAccessHelper:GetCommanderBasicInfo(self.mainFleetList[fleet_index], commander_level)
  local skill = basic_info.SPELL_ID
  local skillIconFram = "TYPE_" .. GameDataAccessHelper:GetSkillRangeType(skill)
  if self.mainFleetList[fleet_index] == 1 then
    local commander_data = GameGlobalData:GetFleetInfo(1)
    local num_max_skill = GameGlobalData:GetNumMaxSkills()
    for index_skill = 1, num_max_skill do
      local id_skill = commander_data.spells[index_skill]
      id_skill = id_skill or -1
      local is_active = id_skill == commander_data.active_spell
      if is_active then
        skillIconFram = "TYPE_" .. GameDataAccessHelper:GetSkillRangeType(id_skill)
      end
    end
  end
  local ability = GameDataAccessHelper:GetCommanderAbility(self.mainFleetList[fleet_index], commander_level)
  local sex = GameDataAccessHelper:GetCommanderSex(self.mainFleetList[fleet_index])
  if commander_id == 1 and leaderlist.leader_ids[currentMatrixIndex] ~= 1 and leaderlist.leader_ids[currentMatrixIndex] ~= nil then
    ability = GameDataAccessHelper:GetCommanderAbility(self.mainFleetList[fleet_index], 0)
    sex = GameDataAccessHelper:GetCommanderSex(self.mainFleetList[fleet_index])
  end
  local fleetRank = GameUtils:GetFleetRankFrame(ability.Rank, GameObjectPlayerMatrix.DownloadRankImageCallback)
  commanderType = GameGlobalData:GetCommanderTypeWithIdentity(self.mainFleetList[fleet_index])
  if sex == 1 then
    sex = "man"
  elseif sex == 0 then
    sex = "woman"
  else
    sex = "unknown"
  end
  local grid_type, grid_index = GameObjectPlayerMatrix:GetCommanderGrid(1)
  local dataTable = {}
  dataTable[#dataTable + 1] = grid_type
  dataTable[#dataTable + 1] = grid_index
  dataTable[#dataTable + 1] = commanderType
  dataTable[#dataTable + 1] = 1
  dataTable[#dataTable + 1] = commander_vessels
  dataTable[#dataTable + 1] = commander_avatar
  dataTable[#dataTable + 1] = commander_vesselstype
  dataTable[#dataTable + 1] = commander_color
  dataTable[#dataTable + 1] = sex
  local dataString = table.concat(dataTable, ",")
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "selectCommanderItem", matrix_type, pos_index, commander_name, commander_identity, commander_avatar, commander_vesselstype, GameUtils.numberAddComma(commander_force), commander_color, forceContext, skillIconFram, fleetRank, sex)
    self:GetFlashObject():InvokeASCallback("_root", "updateCommanderItem", dataString)
  end
end
function GameObjectFormationBackground:IsSelectNewSkill()
  local is_select = false
  local commander_data = GameGlobalData:GetFleetInfo(1)
  local id_skill = tonumber(commander_data.active_spell)
  if id_skill == 2 then
    is_select = true
  end
  return is_select
end
function GameObjectFormationBackground:UpdateCommanderGrid(grid_type, grid_index, id_commander)
  if grid_type == "rest" then
    self:UpdateRestCommanderGrid(grid_index, id_commander)
  elseif grid_type == "battle" then
    self:UpdateBattleCommanderGrid(grid_index, id_commander)
    if QuestTutorialBattleMapAfterRecruit:IsActive() then
      self:GetFlashObject():InvokeASCallback("_root", "SetNpcTipDismiss")
      self:GetFlashObject():InvokeASCallback("_root", "Setformation_arrowLight")
      QuestTutorialBattleMapAfterRecruit:SetFinish(true)
    end
  else
    assert(false)
  end
end
function GameObjectFormationBackground:GenerateExtraFleetData(id)
  local data = {}
  data.identity = id
  data.level = 1
  data.cur_adjutant = nil
  data.active_spell = GameDataAccessHelper:GetCommanderAbility(id, 1).SPELL_ID
  return data
end
function GameObjectFormationBackground:UpdateBattleCommanderGrid(index_grid, id_commander, ingoreForce)
  DebugOut("UpdateBattleCommanderGrid - ", index_grid, ", ", id_commander)
  if not self.m_formationFleets[index_grid] then
    self.m_formationFleets[index_grid] = nil
  end
  self.m_formationFleets[index_grid] = id_commander
  local commanderType = -1
  local commander_identity = -1
  local commander_avatar = -1
  local commander_vessels = -1
  local commander_vessels_type = -1
  local commander_level = 0
  local commander_data = GameGlobalData:GetFleetInfo(id_commander)
  local commander_sex = "unknown"
  if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
    commander_data = GameUICrusade:GetSelfFleet(id_commander)
  end
  if self:IsExtraHero(index_grid, id_commander) then
    DebugOut("GenerateExtraFleetData")
    commander_data = self:GenerateExtraFleetData(id_commander)
  end
  local commander_color = "blue"
  if commander_data == nil then
    DebugOut("error: battle id_commander")
  end
  if id_commander > 1 and commander_data ~= nil then
    commander_identity = commander_data.identity
    commander_level = commander_data.level
    commanderType = GameGlobalData:GetCommanderTypeWithIdentity(commander_identity)
    commander_avatar = GameDataAccessHelper:GetFleetAvatar(id_commander, commander_level)
    commander_vessels = GameDataAccessHelper:GetCommanderVesselsImage(id_commander, commander_level, false)
    commander_vessels_type = GameDataAccessHelper:GetCommanderVesselsType(id_commander, commander_level)
    commander_color = GameDataAccessHelper:GetCommanderColorFrame(id_commander, commander_level)
    commander_sex = GameDataAccessHelper:GetCommanderSex(id_commander)
    if commander_sex == 1 then
      commander_sex = "man"
    elseif commander_sex == 0 then
      commander_sex = "woman"
    else
      commander_sex = "unknown"
    end
  elseif id_commander == 1 then
    commander_data = GameGlobalData:GetFleetInfo(1)
    commander_level = commander_data.level
    commanderType = GameGlobalData:GetCommanderTypeWithIdentity(self.mainFleetList[self.curMainfleetIndex])
    commander_avatar = GameDataAccessHelper:GetFleetAvatar(self.mainFleetList[self.curMainfleetIndex], commander_level)
    commander_vessels = GameDataAccessHelper:GetCommanderVesselsImage(self.mainFleetList[self.curMainfleetIndex], commander_level, false)
    commander_vessels_type = GameDataAccessHelper:GetCommanderVesselsType(self.mainFleetList[self.curMainfleetIndex], commander_level)
    commander_color = GameDataAccessHelper:GetCommanderColorFrame(self.mainFleetList[self.curMainfleetIndex], commander_level)
    commander_sex = GameDataAccessHelper:GetCommanderSex(self.mainFleetList[self.curMainfleetIndex])
    if commander_sex == 1 then
      commander_sex = "man"
    elseif commander_sex == 0 then
      commander_sex = "woman"
    else
      commander_sex = "unknown"
    end
  end
  DebugTable(commander_data)
  DebugOut("commander_vessels", commander_vessels)
  DebugOut("commander_vessels_type", commander_vessels_type)
  local dataTable = {}
  dataTable[#dataTable + 1] = commanderType
  dataTable[#dataTable + 1] = id_commander
  dataTable[#dataTable + 1] = commander_avatar
  dataTable[#dataTable + 1] = commander_vessels
  dataTable[#dataTable + 1] = commander_vessels_type
  dataTable[#dataTable + 1] = commander_color
  dataTable[#dataTable + 1] = commander_sex
  local hasAdjutant = "false"
  if commander_data and commander_data.cur_adjutant and 0 < commander_data.cur_adjutant then
    hasAdjutant = "true"
  end
  dataTable[#dataTable + 1] = hasAdjutant
  if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
    local isDamage = "false"
    local damagePercent = "100%"
    local damageColor = "green"
    isDamage = tostring(GameUICrusade:IsDamage(id_commander))
    damagePercent, damageColor = GameUICrusade:GetDamagePercent(id_commander)
    dataTable[#dataTable + 1] = isDamage
    dataTable[#dataTable + 1] = damagePercent
    dataTable[#dataTable + 1] = damageColor
  end
  if GameStateFormation.m_prevState == GameStateManager.GameStateInstance and GameStateManager.GameStateLab.mCurSubState == STATE_LAB_SUB_STATE.SUBSTATE_COSMOS then
    local isDamage = "false"
    local bufferPercent = "0%"
    local bufferColor = "purple"
    if self.fleetsBuffer then
      bufferPercent = self:GetBufferByFleetID(id_commander) .. "%"
    end
    dataTable[#dataTable + 1] = isDamage
    dataTable[#dataTable + 1] = bufferPercent
    dataTable[#dataTable + 1] = bufferColor
  end
  local dataString = table.concat(dataTable, "\001")
  DebugOut("dataString:", dataString)
  if ext.setCommanderItemDataInFormationInC ~= nil then
    ext.setCommanderItemDataInFormationInC(self:GetFlashObject(), "battle", index_grid, dataString)
  else
    self:GetFlashObject():InvokeASCallback("_root", "setCommanderItemData", "battle", index_grid, dataString)
  end
  if not ingoreForce then
    self:UpdatePlayerPowerPoint()
  end
  if self.jamedCount then
    self:UpdateCommanderJamed()
  end
  if immanentversion == 2 and QuestTutorialBattleFailed:IsActive() then
    for k, v in pairs(self.m_formationFleets) do
      if v == 4 and k ~= 2 then
        local function callback()
          self:GetFlashObject():InvokeASCallback("_root", "setNpcTipVisible", false)
        end
        GameUICommonDialog:PlayStory({1100028}, callback)
        AddFlurryEvent("SwitchShipFinish", {}, 2)
      end
    end
  end
  GameObjectFormationBackground:UpdateAdjutantBar()
end
function GameObjectFormationBackground:GetBufferByFleetID(fleetid)
  local result = 100
  DebugOut("GameObjectFormationBackground:GetBufferByFleetID")
  DebugTable(self.fleetsBuffer)
  for k, v in pairs(self.fleetsBuffer) do
    if tostring(v.key) == tostring(fleetid) then
      result = v.value
    end
  end
  return result
end
function GameObjectFormationBackground:UpdateRestCommanderGrid(index_grid, id_commander)
  self.m_freeFleets[index_grid] = id_commander
  local commanderType = -1
  local commander_identity = -1
  local commander_avatar = -1
  local commander_vessels = -1
  local commander_vessels_type = -1
  local commander_level = 0
  local commander_color = "blue"
  local commander_sex = "unknown"
  local commander_data = GameGlobalData:GetFleetInfo(id_commander)
  if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
    commander_data = GameUICrusade:GetSelfFleet(id_commander)
  end
  if commander_data == nil then
    DebugOut("error: Rest id_commander")
  end
  if id_commander > 0 and commander_data ~= nil then
    commander_identity = commander_data.identity
    commander_level = commander_data.level
    commander_color = GameDataAccessHelper:GetCommanderColorFrame(id_commander, commander_level)
    commanderType = GameGlobalData:GetCommanderTypeWithIdentity(commander_identity)
    commander_avatar = GameDataAccessHelper:GetFleetAvatar(id_commander, commander_level)
    commander_vessels = GameDataAccessHelper:GetCommanderVesselsImage(id_commander, commander_level, false)
    commander_vessels_type = GameDataAccessHelper:GetCommanderVesselsType(id_commander, commander_level)
    commander_sex = GameDataAccessHelper:GetCommanderSex(id_commander)
    if commander_sex == 1 then
      commander_sex = "man"
    elseif commander_sex == 0 then
      commander_sex = "woman"
    else
      commander_sex = "unknown"
    end
  end
  local dataTable = {}
  dataTable[#dataTable + 1] = commanderType
  dataTable[#dataTable + 1] = id_commander
  dataTable[#dataTable + 1] = commander_avatar
  dataTable[#dataTable + 1] = commander_vessels
  dataTable[#dataTable + 1] = commander_vessels_type
  dataTable[#dataTable + 1] = commander_color
  dataTable[#dataTable + 1] = commander_sex
  local hasAdjutant = "false"
  if commander_data and commander_data.cur_adjutant and 0 < commander_data.cur_adjutant then
    hasAdjutant = "true"
  end
  dataTable[#dataTable + 1] = hasAdjutant
  local isDamage = "false"
  if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
    isDamage = tostring(GameUICrusade:IsDamage(id_commander))
    dataTable[#dataTable + 1] = isDamage
  end
  if GameStateFormation.m_prevState == GameStateManager.GameStateInstance and GameStateManager.GameStateLab.mCurSubState == STATE_LAB_SUB_STATE.SUBSTATE_COSMOS then
    local bufferPercent = "0%"
    local bufferColor = "purple"
    if self.fleetsBuffer then
      bufferPercent = self:GetBufferByFleetID(id_commander) .. "%"
    end
    dataTable[#dataTable + 1] = isDamage
    dataTable[#dataTable + 1] = bufferPercent
    dataTable[#dataTable + 1] = bufferColor
  end
  local dataString = table.concat(dataTable, "\001")
  if ext.setCommanderItemDataInFormationInC ~= nil then
    ext.setCommanderItemDataInFormationInC(self:GetFlashObject(), "rest", index_grid, dataString)
  else
    self:GetFlashObject():InvokeASCallback("_root", "setCommanderItemData", "rest", index_grid, dataString)
  end
end
function GameObjectFormationBackground:UpdateNextCommanderGrid(index_grid, id_commander, type)
  local commanderType = -1
  local commander_identity = -1
  local commander_avatar = -1
  local commander_vessels = -1
  local commander_vessels_type = -1
  local commander_level = 0
  local commander_color = "blue"
  local commander_sex = "unknown"
  local commander_data = GameGlobalData:GetFleetInfo(id_commander)
  if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
    commander_data = GameUICrusade:GetSelfFleet(id_commander)
  end
  if commander_data == nil then
    DebugOut("error: Next id_commander")
  end
  if id_commander > 0 and commander_data ~= nil then
    commander_identity = commander_data.identity
    commander_level = commander_data.level
    commander_color = GameDataAccessHelper:GetCommanderColorFrame(id_commander, commander_level)
    commanderType = GameGlobalData:GetCommanderTypeWithIdentity(commander_identity)
    commander_avatar = GameDataAccessHelper:GetFleetAvatar(id_commander, commander_level)
    commander_vessels = GameDataAccessHelper:GetCommanderVesselsImage(id_commander, commander_level, false)
    commander_vessels_type = GameDataAccessHelper:GetCommanderVesselsType(id_commander, commander_level)
    commander_sex = GameDataAccessHelper:GetCommanderSex(id_commander)
    if commander_sex == 1 then
      commander_sex = "man"
    elseif commander_sex == 0 then
      commander_sex = "woman"
    else
      commander_sex = "unknown"
    end
  end
  local dataTable = {}
  dataTable[#dataTable + 1] = commanderType
  dataTable[#dataTable + 1] = id_commander
  dataTable[#dataTable + 1] = commander_avatar
  dataTable[#dataTable + 1] = commander_vessels
  dataTable[#dataTable + 1] = commander_vessels_type
  dataTable[#dataTable + 1] = commander_color
  dataTable[#dataTable + 1] = commander_sex
  local hasAdjutant = "false"
  if commander_data and commander_data.cur_adjutant and 0 < commander_data.cur_adjutant then
    hasAdjutant = "true"
  end
  dataTable[#dataTable + 1] = hasAdjutant
  local isDamage = "false"
  if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
    isDamage = tostring(GameUICrusade:IsDamage(id_commander))
    dataTable[#dataTable + 1] = isDamage
  end
  if GameStateFormation.m_prevState == GameStateManager.GameStateInstance and GameStateManager.GameStateLab.mCurSubState == STATE_LAB_SUB_STATE.SUBSTATE_COSMOS then
    local bufferPercent = "0%"
    local bufferColor = "purple"
    if self.fleetsBuffer then
      bufferPercent = self:GetBufferByFleetID(id_commander) .. "%"
    end
    dataTable[#dataTable + 1] = isDamage
    dataTable[#dataTable + 1] = bufferPercent
    dataTable[#dataTable + 1] = bufferColor
  end
  local dataString = table.concat(dataTable, "\001")
  if ext.setCommanderItemDataInFormationInC ~= nil then
    ext.setCommanderItemDataInFormationInC(self:GetFlashObject(), type, index_grid, dataString)
  else
    self:GetFlashObject():InvokeASCallback("_root", "setCommanderItemData", type, index_grid, dataString)
  end
end
function GameObjectFormationBackground:SetEnemyGridData(index_grid, id_enemy)
  local obj_flash = self:GetFlashObject()
  local enemy_avatar = -1
  local enemy_vessels = -1
  local enemy_vessels_type = -1
  local enemy_color = "blue"
  local enmey_sex = "unknown"
  if not id_enemy or id_enemy <= 0 then
    id_enemy = -1
  else
    if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
      local commander_level = 0
      enemyColor = GameDataAccessHelper:GetCommanderColorFrame(id_enemy, commander_level)
      local enemyInfo = GameUICrusade:GetTriggerEnemyInfo()
      enemy_avatar = GameDataAccessHelper:GetFleetAvatar(id_enemy, commander_level, enemyInfo.mSex)
      enemy_vessels = GameDataAccessHelper:GetCommanderVesselsImage(id_enemy, commander_level, false)
      enemy_vessels_type = GameDataAccessHelper:GetCommanderVesselsType(id_enemy, commander_level)
      enemy_color = GameDataAccessHelper:GetCommanderColorFrame(id_enemy, commander_level)
      enmey_sex = self:GetEnemySex(id_enemy)
    elseif GameStateFormation.m_prevState == GameStateManager.GameStateArena then
      local commander_level = 0
      enemyColor = GameDataAccessHelper:GetCommanderColorFrame(id_enemy, commander_level)
      local enemyInfo = GameUIArena.enemyInfo
      enemy_avatar = GameDataAccessHelper:GetFleetAvatar(id_enemy, commander_level, enemyInfo.mSex)
      enemy_vessels = GameDataAccessHelper:GetCommanderVesselsImage(id_enemy, commander_level, false)
      enemy_vessels_type = GameDataAccessHelper:GetCommanderVesselsType(id_enemy, commander_level)
      enemy_color = GameDataAccessHelper:GetCommanderColorFrame(id_enemy, commander_level)
      enmey_sex = GameDataAccessHelper:GetCommanderSex(id_enemy)
    else
      local monster_info = self:GetMonsterInfoWithIdentity(id_enemy)
      if monster_info then
        enemy_avatar = GameDataAccessHelper:GetFleetAvatarByAvateImage(monster_info.avatar)
        enemy_vessels = GameDataAccessHelper:GetShipByShipImage(monster_info.ship)
        enemy_vessels_type = "TYPE_" .. tostring(monster_info.vessels)
      end
      enmey_sex = self:GetEnemySex(id_enemy)
    end
    if enmey_sex == 1 then
      enmey_sex = "man"
    elseif enmey_sex == 0 then
      enmey_sex = "woman"
    else
      enmey_sex = "unknown"
    end
  end
  local dataTable = {}
  dataTable[#dataTable + 1] = -1
  dataTable[#dataTable + 1] = id_enemy
  dataTable[#dataTable + 1] = enemy_avatar
  dataTable[#dataTable + 1] = enemy_vessels
  dataTable[#dataTable + 1] = enemy_vessels_type
  dataTable[#dataTable + 1] = enemy_color
  dataTable[#dataTable + 1] = enmey_sex
  local adjust = "false"
  dataTable[#dataTable + 1] = adjust
  local isDamage = "false"
  if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
    isDamage = tostring(GameUICrusade:IsDamage(id_commander))
  end
  dataTable[#dataTable + 1] = isDamage
  local dataString = table.concat(dataTable, "\001")
  if ext.setCommanderItemDataInFormationInC ~= nil then
    ext.setCommanderItemDataInFormationInC(self:GetFlashObject(), "enemy", index_grid, dataString)
  else
    self:GetFlashObject():InvokeASCallback("_root", "setCommanderItemData", "enemy", index_grid, dataString)
  end
end
function GameObjectFormationBackground:GetSortedFleetPosInFreeList(fleetID)
  DebugOut("GetSortedFleetPosInFreeList ", fleetID)
  local fleetSortedIndex = self.m_fleetIDToSortedIndex[fleetID]
  local pos = 0
  for i, v in ipairs(self.m_freeFleets) do
    local sortedIndex = self.m_fleetIDToSortedIndex[v]
    if fleetSortedIndex > sortedIndex then
      pos = i
    else
      break
    end
  end
  return pos
end
function GameObjectFormationBackground:GetFleetIDInGrid(gridIndex)
  return self.m_formationFleets[gridIndex]
end
function GameObjectFormationBackground:OnLocateCommander(object_dragger)
  DebugOut("OnLocateCommander:")
  local located_type = object_dragger.DragToGridType
  local located_index = object_dragger.DragToGridIndex
  local located_src_commander = self:GetCommanderIDFromGrid(located_type, located_index)
  DebugOut("located_src_commander - ", located_src_commander)
  self:UpdateCommanderGrid(located_type, located_index, object_dragger.DraggedFleetID)
  if located_type == "battle" and object_dragger.DraggedFleetGridType == "battle" then
    FleetMatrix:SwapMatrix(located_index, object_dragger.DraggedFleetGridIndex)
  elseif located_type == "battle" then
    FleetMatrix:SetMatrix(located_index, object_dragger.DraggedFleetID)
  elseif object_dragger.DraggedFleetGridType == "battle" then
    FleetMatrix:SetMatrix(object_dragger.DraggedFleetGridIndex, -1)
  end
  if located_src_commander and located_src_commander > 0 then
    self:UpdateCommanderGrid(object_dragger.DraggedFleetGridType, object_dragger.DraggedFleetGridIndex, located_src_commander)
  end
  self:GetFlashObject():InvokeASCallback("_root", "OnCommanderDragOver")
  self.selected_commander = object_dragger.DraggedFleetID
  self:SelectCommander(object_dragger.DraggedFleetID)
  object_dragger.DraggedFleetID = nil
  object_dragger.DragToGridType = nil
  object_dragger.DragToGridIndex = nil
  object_dragger.DraggedFleetGridType = nil
  object_dragger.DraggedFleetGridIndex = nil
end
function GameObjectFormationBackground:OnBeginDragFleet(gameObjectFleetDragger)
  local draggedFleedID = gameObjectFleetDragger.DraggedFleetID
  DebugOut("OnBeginDragFleet ", draggedFleedID)
  local grid_type, grid_index = self:GetCommanderGridIndex(draggedFleedID)
  if grid_type == "rest" or grid_type == "battle" then
    gameObjectFleetDragger.DraggedFleetGridType = grid_type
    gameObjectFleetDragger.DraggedFleetGridIndex = grid_index
    self:UpdateCommanderGrid(grid_type, grid_index, -1)
  else
    assert(false)
  end
end
function GameObjectFormationBackground:IsFleetInFreeList(fleetID)
  for i, v in ipairs(self.m_freeFleets) do
    if v == fleetID then
      return true
    end
  end
  return false
end
function GameObjectFormationBackground:GetFleetGridIndex(fleetID)
  if fleetID == -1 then
    return -1
  end
  for k, v in pairs(self.m_formationFleets) do
    if v == fleetID then
      return k
    end
  end
  return -1
end
function GameObjectFormationBackground:GetFirstLocatableRestGrid()
  for index, id in pairs(self.m_freeFleets) do
    if id == -1 then
      return index
    end
  end
  return nil
end
function GameObjectFormationBackground:GetCommanderGridIndex(fleet_id)
  DebugOut(fleet_id)
  DebugOutPutTable(self.m_formationFleets, "formation")
  for index, id in pairs(self.m_formationFleets) do
    if id == fleet_id then
      return "battle", index
    end
  end
  DebugOutPutTable(self.m_freeFleets, "free")
  for index, id in pairs(self.m_freeFleets) do
    if id == fleet_id then
      return "rest", index
    end
  end
  return nil
end
function GameObjectFormationBackground:GetCommanderIDFromGrid(grid_type, grid_index)
  local grid_list
  if grid_type == "rest" then
    grid_list = self.m_freeFleets
  elseif grid_type == "battle" then
    grid_list = self.m_formationFleets
  else
    assert(false)
  end
  return grid_list[grid_index]
end
function GameObjectFormationBackground:CountBattleCommander()
  local count = 0
  for index, data in pairs(self.m_formationFleets) do
    if data > 0 and not self:IsExtraHero(index, data) then
      count = count + 1
    end
  end
  return count
end
function GameObjectFormationBackground:GetBattleCommanderGridIndex(fleet_id)
  for index, id in pairs(self.m_formationFleets) do
    if id == fleet_id then
      return index
    end
  end
  return nil
end
function GameObjectFormationBackground:GetRestCommanderGridIndex(fleet_id)
  for index, id in pairs(self.m_freeFleets) do
    if id == fleet_id then
      return index
    end
  end
  return nil
end
function GameObjectFormationBackground:GetMonsterInfoWithIdentity(monster_identity)
  for _, monster_info in pairs(self.enemy_matrix) do
    if monster_info.identity == monster_identity then
      return monster_info
    end
  end
  return nil
end
function GameObjectFormationBackground:RequestEnemyFormation()
  local content = {
    battle_id = GameUtils:CombineBattleID(GameStateFormation.m_currentAreaID, GameStateFormation.m_currentBattleID)
  }
  local request_call
  function request_call()
    DebugOut("RequestEnemyFormation", request_call)
    NetMessageMgr:SendMsg(NetAPIList.battle_matrix_req.Code, content, self.NetMsgCallbacks, true, request_call)
  end
  request_call()
end
function GameObjectFormationBackground:Update(dt)
  if not GameStateFormation.GameObjectDraggerFleet:IsAutoMove() then
  end
  self:GetFlashObject():InvokeASCallback("_root", "UpdateFrame", dt)
  self:GetFlashObject():Update(dt)
end
function GameObjectFormationBackground:GetCommanderLocatedInfo()
  local located_type, located_pos
  local located_info = self:GetFlashObject():InvokeASCallback("_root", "GetCommanderLocatedInfo")
  if located_info then
    local param_list = LuaUtils:string_split(located_info, ",")
    located_type = param_list[1]
    located_pos = param_list[2]
  end
  return located_type, located_pos
end
function GameObjectFormationBackground:CheckFleetdamage()
  local damage = false
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  for i = 1, #fleets do
    if fleets[i].damage < 50 then
      damage = true
    end
  end
  DebugOut("CheckFleetdamage", damage)
  return damage
end
function GameObjectFormationBackground:ShowGotoFactory()
  DebugOut("ShowGotoFactory")
  DebugOut("QuestTutorialRepairFleet:IsFinished", QuestTutorialRepairFleet:IsFinished())
  DebugOut("QuestTutorialRepairFleet:IsActive", QuestTutorialRepairFleet:IsActive())
  if not QuestTutorialRepairFleet:IsFinished() and not QuestTutorialRepairFleet:IsActive() then
    QuestTutorialRepairFleet:SetActive(true)
    DebugOut("QuestTutorialRepairFleet:SetActive true")
    local function callback()
      local factory = GameGlobalData:GetBuildingInfo("factory")
      if factory.level > 0 then
        GameObjectFormationBackground.GotoFixFleet()
      else
        if not QuestTutorialBuildFactory:IsFinished() then
          QuestTutorialBuildFactory:SetActive(true)
        end
        GameStateManager:SetCurrentGameState(GameStateMainPlanet)
      end
    end
    GameUICommonDialog:PlayStory({100039}, callback)
  else
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.Caption)
    GameUIMessageDialog:SetRightTextButton(GameLoader:GetGameText("LC_MENU_DAMAGE_ALERT_BUTTON_REPAIR"), self.GotoFixFleet, {})
    local function leftcallback()
      FleetMatrix:SaveMatrixChanged(false, function()
        GameObjectFormationBackground:BattleNow()
      end)
    end
    GameUIMessageDialog:SetLeftTextButton(GameLoader:GetGameText("LC_MENU_DAMAGE_ALERT_BUTTON_BATTLE"), leftcallback, {})
    local info = GameLoader:GetGameText("LC_MENU_DAMAGE_ALERT")
    GameUIMessageDialog:Display("", info)
  end
end
function GameObjectFormationBackground.GotoFixFleet()
  local GameUIFactory = LuaObjectManager:GetLuaObject("GameUIFactory")
  GameStateManager:GetCurrentGameState():AddObject(GameUIFactory)
end
function GameObjectFormationBackground:BattleNow()
  DebugOut("BattleNow")
  if QuestTutorialBattleMapAfterRecruit:IsActive() then
    if immanentversion == 2 then
      GameUICommonDialog:ForcePlayStory({51149}, nil)
    elseif immanentversion == 1 then
      GameUICommonDialog:ForcePlayStory({100020}, nil)
    end
  elseif GameObjectTutorialCutscene:IsActive() then
    GameObjectTutorialCutscene:GetFlashObject():InvokeASCallback("_root", "setAutoChangeToNextStep", true)
  elseif GameStateFormation.m_prevState == GameStateConfrontation then
    GameObjectAdventure:BattleNow()
  elseif GameStateFormation.m_prevState == GameStateArcane then
    GameUIArcaneEnemyInfo:BattleNow()
  elseif GameStateFormation.m_prevState == GameStateInstance then
    GameObjectClimbTower:BattleNow()
  elseif GameStateFormation.m_prevState == GameStateManager.GameStateArena then
    if GameUIArena.mCurrentArenaType == GameUIArena.ARENA_TYPE.normalArena then
      GameUIArena:ChallengeOpponentWithRank()
    elseif GameUIArena.mCurrentArenaType == GameUIArena.ARENA_TYPE.tlc then
      TeamLeagueCup:StartBattle()
    else
      GameUIAdvancedArenaLayer:StartBattle()
    end
  else
    GameUIEnemyInfo.m_currentBattleID = GameStateFormation.m_currentBattleID
    GameUIEnemyInfo.m_combinedBattleID = GameUtils:CombineBattleID(GameStateFormation.m_currentAreaID, GameUIEnemyInfo.m_currentBattleID)
    GameUIEnemyInfo:BattleNowClicked()
  end
end
function GameObjectFormationBackground:GetCommanderGridPos(grid_type, grid_index)
  local grid_pos
  local pos_info = self:GetFlashObject():InvokeASCallback("_root", "GetCommanderGridPos", grid_type, grid_index)
  DebugOut(pos_info)
  if pos_info then
    local param_list = LuaUtils:string_split(pos_info, ",")
    grid_pos = {}
    grid_pos._x = tonumber(param_list[1])
    grid_pos._y = tonumber(param_list[2])
  end
  return grid_pos
end
function GameObjectFormationBackground:SetJamedCount(jamedCount)
  self.jamedCount = jamedCount
end
function GameObjectFormationBackground.SwitchMatrixAck()
  GameObjectFormationBackground.m_formationFleets = LuaUtils:table_rcopy(GameObjectFormationBackground.m_nextPlayerMatrix)
  GameObjectFormationBackground.m_freeFleets = LuaUtils:table_rcopy(GameObjectFormationBackground.m_nextPlayerRestFleets)
  GameObjectFormationBackground:UpdatePlayerPowerPoint()
end
function GameObjectFormationBackground:OnFSCommand(cmd, args)
  DebugOut("GameObjectFormationBackground:OnFSCommand", cmd, args)
  if cmd == "NeedUpdateFreeFleetInfo" then
    local fleetID = tonumber(args)
    local commander_data = GameGlobalData:GetFleetInfo(fleetID)
    if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
      commander_data = GameUICrusade:GetSelfFleet(fleetID)
    end
    self:GetFlashObject():InvokeASCallback("_root", "UpdateFreeFleetInfo", fleetID, GameDataAccessHelper:GetFleetAvatar(fleetID, commander_data.level))
  elseif cmd == "CloseFormation" then
    local isCrusade = false
    if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
      isCrusade = true
    end
    if GameObjectFormationBackground:CheckAdjutantBeforeSaveCurrentMatrix(true) then
      FleetMatrix:SaveMatrixChanged(true, self.callback)
      if not isCrusade then
        DebugOut("currentMatrixIndex", currentMatrixIndex)
        FleetMatrix:SaveMatrixChanged(false)
      end
      if GameStateFormation.m_prevState == GameStateConfrontation then
        GameStateConfrontation:EnterConfrontation()
      elseif GameStateFormation.m_prevState == GameStateBattleMap then
        GameStateBattleMap:ReEnter(nil)
      elseif GameStateFormation.m_prevState == GameStateInstance then
        GameStateInstance:EnterInstance()
      elseif GameStateFormation.m_prevState == GameStateArcane then
        GameStateManager:SetCurrentGameState(GameStateArcane)
      else
        GameStateManager:SetCurrentGameState(GameStateFormation.m_prevState)
      end
    elseif GameStateFormation.m_prevState == GameStateConfrontation then
      GameStateConfrontation:EnterConfrontation()
    elseif GameStateFormation.m_prevState == GameStateBattleMap then
      GameStateBattleMap:ReEnter(nil)
    elseif GameStateFormation.m_prevState == GameStateInstance then
      GameStateInstance:EnterInstance()
    else
      GameStateManager:SetCurrentGameState(GameStateFormation.m_prevState)
    end
  elseif cmd == "OnMoveInAnimOver" then
    DebugOut("GameObjectFormationBackground:OnMoveInAnimOver")
    if self.m_tutorialStory ~= nil then
    end
  elseif cmd == "begin_drag" then
    local param = LuaUtils:string_split(args, ",")
    local fleetID = tonumber(param[1])
    local initPosX = tonumber(param[2])
    local initPosY = tonumber(param[3])
    local mouseX = tonumber(param[4])
    local mouseY = tonumber(param[5])
    local isInFreeList = param[6] == "true"
    local extraid = self.mainFleetList[self.curMainfleetIndex]
    DebugOut("extraid:", extraid)
    GameStateFormation:BeginDragFleet(fleetID, -1, initPosX, initPosY, mouseX, mouseY, extraid)
  elseif cmd == "start_select_skill" then
    local commander_id = tonumber(args)
    self:UpdateCommanderSkillItem(commander_id)
  elseif cmd == "select_commander_skill" then
    local id_fleet, id_skillgrid = unpack(LuaUtils:string_split(args, ","))
    self:SelectCommanderSkill(tonumber(id_fleet), tonumber(id_skillgrid))
    if QuestTutorialSkill:IsActive() and self:IsSelectNewSkill() then
      self:GetFlashObject():InvokeASCallback("_root", "HideTutorialClickMC")
      self:GetFlashObject():InvokeASCallback("_root", "HideTutorialSkill")
      self:GetFlashObject():InvokeASCallback("_root", "EnableDragFleet", true)
      QuestTutorialSkill:SetFinish(true)
      self.finishKillTutorial = true
      if immanentversion == 2 then
        AddFlurryEvent("Tutorial_SwitchSpellChoose", {}, 2)
        local function callback()
          self:GetFlashObject():InvokeASCallback("_root", "showBattleTipAnimation")
          self:GetFlashObject():InvokeASCallback("_root", "ExpandSkillSelector", -1)
        end
        GameUICommonDialog:PlayStory({11000151}, callback)
      elseif immanentversion == 1 then
        GameUICommonDialog:PlayStory({100013}, nil)
      end
    end
  elseif cmd == "SelectFleet" then
    local fleetID = tonumber(args)
    self.m_currentSelectFleetId = fleetID
    self:UpdateCommanderSkillInfo(fleetID)
  elseif cmd == "unselect_skill" then
    if QuestTutorialSkill:IsActive() then
      self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialClickMC")
    end
  elseif cmd == "select_commander" then
    local commander_id = tonumber(args)
    self:SelectCommander(commander_id)
    if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
    else
      self:ShowMainFleetSelector(commander_id)
    end
    if QuestTutorialSkill:IsActive() then
      local grid_type, index_commander = self:GetCommanderGridIndex(commander_id)
      self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialSkill", index_commander)
      self:GetFlashObject():InvokeASCallback("_root", "HideTutorialClickMC")
    end
  elseif cmd == "battle_now" then
    if GameObjectFormationBackground:CheckAdjutantBeforeSaveCurrentMatrix() then
      if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
        GameUICrusade:RequestFight(self.m_formationFleets)
      elseif GameStateFormation.m_prevState == GameStateManager.GameStateStarSystem then
        FleetMatrix:SaveMatrixChanged(true, function()
          GameUIStarSystemFuben:RequestFight()
        end)
      else
        if currentMatrixIndex > maxAvailableMatrix then
          return
        end
        FleetMatrix:SaveMatrixChanged(true, function()
          GameObjectFormationBackground:BattleLater()
        end)
      end
    end
  elseif cmd == "locate_commander" then
    local param_list = LuaUtils:string_split(args, ",")
    self.locatedType = param_list[1]
    self.locatedPos = tonumber(param_list[2])
  elseif cmd == "move_in_rest_end" then
    if QuestTutorialBattleMapAfterRecruit:IsActive() then
      self:GetFlashObject():InvokeASCallback("_root", "SetNpcTipLight")
    else
      self:GetFlashObject():InvokeASCallback("_root", "SetNpcTipDismiss")
    end
  elseif cmd == "move_out_rest_end" then
    self:GetFlashObject():InvokeASCallback("_root", "Setformation_arrowSkillTipDismiss")
    self:CheckQuestTutorial()
  elseif cmd == "arrow_L" then
    self:trySaveAndExchangeToMatrix(currentMatrixIndex, true)
  elseif cmd == "arrow_R" then
    self:trySaveAndExchangeToMatrix(currentMatrixIndex, false)
  elseif cmd == "matrixIndexButtonClicked" then
    DebugOut("currentMatrixIndex ", currentMatrixIndex)
    local index = tonumber(args)
    local isMoveLeft = true
    if index == currentMatrixIndex then
      return
    elseif index < currentMatrixIndex then
      isMoveLeft = true
    else
      isMoveLeft = false
    end
    GameObjectFormationBackground.mCurFreeMatrixSubIdx = 0
    FleetMatrix:SaveMatrixChanged(true)
    self:loadNextMatrixAndRestFleets(index)
    self:setNextMatrixAndRestFleetsUI(isMoveLeft)
    self:ShrinkSkillSelector()
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_ClickMatrixIndexButton", index)
    if currentMatrixIndex <= maxAvailableMatrix then
      GameGlobalData.GlobalData.curMatrixIndex = currentMatrixIndex
      FleetMatrix:SwitchMatrixReq(currentMatrixIndex, self.SwitchMatrixAck, true)
    end
  elseif cmd == "move_prevRest_over" or cmd == "move_prev_over" or cmd == "move_nextRest_over" or cmd == "move_next_over" then
    self.m_formationFleets = LuaUtils:table_rcopy(self.m_nextPlayerMatrix)
    self.m_freeFleets = LuaUtils:table_rcopy(self.m_nextPlayerRestFleets)
    for i = 1, 9 do
      local commander_id = self.m_formationFleets[i]
      if commander_id == nil then
        commander_id = -1
      end
      self:UpdateBattleCommanderGrid(i, commander_id, true)
    end
    for i = 1, 9 do
      local commander_id = self.m_freeFleets[i]
      if commander_id == nil then
        commander_id = -1
      end
      self:UpdateRestCommanderGrid(i, commander_id)
    end
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_updateLockBtn")
    self:SelectCommander(1)
    GameObjectFormationBackground:CheckAdjutantBeforeSaveCurrentMatrix()
  elseif cmd == "rest_list_move_prev_over" or cmd == "rest_list_move_next_over" then
    self.m_freeFleets = LuaUtils:table_rcopy(self.m_nextPlayerRestFleets)
    for i = 1, 9 do
      local commander_id = self.m_freeFleets[i]
      if commander_id == nil then
        commander_id = -1
      end
      self:UpdateRestCommanderGrid(i, commander_id)
    end
    self:SelectCommander(1)
  elseif cmd == "btn_lock" then
  elseif cmd == "choose_Item" then
    local curr_id = tonumber(args)
    self.curMainfleetIndex = tonumber(args)
    if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
      curr_id = GameUICrusade:GetRealFleetID()
    end
    for k, v in ipairs(self.mainFleetList) do
      DebugOut("v:" .. v)
      if v == curr_id then
        self.curMainfleetIndex = k
      end
    end
    self:ExpandSkillSelectorNew(tonumber(args))
  elseif cmd == "setMainFleet" then
    self:setMainFleet()
  elseif cmd == "adjutantClicked" then
    if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_BRIDGE_TBC_ADJUTANT_INFO_1"))
    elseif GameObjectFormationBackground:CheckAdjutantBeforeSaveCurrentMatrix() then
      local function saveSuccessCallback()
        local function exitAdjutantUICallback()
          GameObjectFormationBackground:RefreshBattleAndRestGrid()
          GameObjectFormationBackground:UpdateAdjutantBar()
          GameObjectFormationBackground:CheckAdjutantBeforeSaveCurrentMatrix()
        end
        GameUIAdjutant.mFleetIdBeforeEnter = 1
        GameUIAdjutant:EnterAdjutantUI(exitAdjutantUICallback)
      end
      FleetMatrix:SaveMatrixChanged(true, saveSuccessCallback)
    end
  end
end
function GameObjectFormationBackground:BattleLater()
  if self.finishKillTutorial then
    AddFlurryEvent("Tutorial_ChooseStartBattle", {}, 2)
  end
  self.finishKillTutorial = nil
  if QuestTutorialFirstBattle:IsActive() then
    QuestTutorialFirstBattle:SetFinish(true)
    AddFlurryEvent("ClickBattleInFormationUI", {}, 1)
    self:GetFlashObject():InvokeASCallback("_root", "hideBattleTipAnimation")
  end
  if QuestTutorialSkill:IsActive() and QuestTutorialSkill:IsActive() then
    local function callback()
      self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialClickMC")
    end
    GameUICommonDialog:ForcePlayStory({100059}, callback)
    return
  end
  if immanentversion == 2 and QuestTutorialBattleFailed:IsActive() then
    if GameObjectFormationBackground:CheckHaveChangeFleet() then
      AddFlurryEvent("SwitchShipBattleStart", {}, 2)
      QuestTutorialBattleFailed:SetFinish(true)
      self:GetFlashObject():InvokeASCallback("_root", "setNpcTipVisible", false)
    else
      GameUICommonDialog:ForcePlayStory({1100029}, nil)
      return
    end
  end
  if GameObjectFormationBackground:CheckFleetdamage() then
    self:ShowGotoFactory()
  else
    GameObjectFormationBackground:BattleNow()
  end
end
function GameObjectFormationBackground:CheckHaveChangeFleet()
  local is_changed = true
  if self:CountBattleCommander() <= 1 then
    return false
  end
  for k, v in pairs(self.m_formationFleets) do
    if k == 2 and v == 4 then
      is_changed = false
    end
  end
  return is_changed
end
function GameObjectFormationBackground:SetDataProvider(dataProvider)
  DebugOut("SetDataProvider")
  self.dataProvider = dataProvider
end
if AutoUpdate.isAndroidDevice then
  function GameObjectFormationBackground.OnAndroidBack()
    GameObjectFormationBackground:GetFlashObject():InvokeASCallback("_root", "AnimationMoveOut")
  end
end
function GameObjectFormationBackground:requestMultiMatrix(isBlock)
  local callback
  function callback()
    NetMessageMgr:SendMsg(NetAPIList.mulmatrix_get_req.Code, {type = 0}, GameObjectFormationBackground.requestMultiMatrixCallBack, isBlock, callback)
  end
  callback()
end
function GameObjectFormationBackground.requestMultiMatrixCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.mulmatrix_get_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.mulmatrix_get_ack.Code then
    DebugOut("GameObjectFormationBackground.requestMultiMatrixCallBack ", msgType)
    DebugTable(content)
    haveRequestMatrixInfo = true
    if content ~= nil and content.blank_num > 1 and content.cur_index and 0 < content.cur_index then
      maxAvailableMatrix = content.blank_num
      GameGlobalData.GlobalData.maxAvailableMatrix = maxAvailableMatrix
      DebugOut("maxAvailableMatrix", maxAvailableMatrix)
      GameObjectFormationBackground:GetFlashObject():InvokeASCallback("_root", "initMatrixIndexButton", content.blank_num)
      currentMatrixIndex = content.cur_index
      GameGlobalData.GlobalData.curMatrixIndex = content.cur_index
      if GameObjectFormationBackground:LoadCurrentMatrix() == false then
        GameObjectFormationBackground:InitFormation()
        currentMatrixIndex = 1
        GameGlobalData.GlobalData.curMatrixIndex = 1
        FleetMatrix:SaveMatrixChanged(false)
      end
      GameObjectFormationBackground:initUI()
    else
      currentMatrixIndex = 1
      if content ~= nil and content.cur_index and 0 < content.cur_index then
        currentMatrixIndex = content.cur_index
        GameGlobalData.GlobalData.curMatrixIndex = content.cur_index
      end
      GameObjectFormationBackground:InitFormation()
      FleetMatrix:SaveMatrixChanged(false)
      GameObjectFormationBackground:initUI()
    end
    return true
  end
  return false
end
function GameObjectFormationBackground:LoadCurrentMatrix()
  DebugOut("LoadCurrentMatrix")
  local currentMatrix = FleetMatrix:getCurrentMartix().cells
  if not currentMatrix then
    DebugOut("empty matrix", currentMatrixIndex)
    return false
  end
  local formationIds = {}
  DebugOutPutTable(currentMatrix, "currentMatrix")
  for _, v in pairs(currentMatrix) do
    self.m_formationFleets[v.cell_id] = v.fleet_identity
    if v.fleet_identity ~= -1 and v.fleet_identity ~= 0 then
      formationIds[v.fleet_identity] = 1
    end
  end
  local leaderlist = GameGlobalData:GetData("leaderlist")
  self.curMainfleetIndex = 1
  self.mainFleetList = leaderlist.options
  local curr_id = leaderlist.leader_ids[currentMatrixIndex]
  for k, v in ipairs(self.mainFleetList) do
    if v == curr_id then
      self.curMainfleetIndex = k
      break
    end
  end
  self.m_nextPlayerMatrix = LuaUtils:table_rcopy(self.m_formationFleets)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  for i, v in pairs(fleets) do
    if formationIds[v.identity] == nil then
      DebugOut(v.identity)
      if self.needAddHero and 0 < v.identity then
        DebugOut("needAddHero", self.needAddHero)
        if 0 >= self.m_formationFleets[4] then
          self.m_formationFleets[4] = v.identity
          break
        elseif 0 >= self.m_formationFleets[5] then
          self.m_formationFleets[5] = v.identity
          break
        end
      end
      table.insert(GameObjectFormationBackground.m_freeFleets, v.identity)
    end
    GameObjectFormationBackground.m_fleetIDToSortedIndex[v.identity] = i
  end
  DebugOut("ExtraHeroData:")
  DebugTable(self.ExtraHeroData)
  self:AddExtraHero(self.ExtraHeroData)
  DebugOut("AddExtraHero after:")
  DebugOut("m_formationFleets")
  DebugTable(self.m_formationFleets)
  for i, v in pairs(self.m_formationFleets) do
    self:UpdateBattleCommanderGrid(i, v)
  end
  for i = 1, 9 do
    if not self.m_freeFleets[i] then
      self.m_freeFleets[i] = -1
    end
  end
  DebugOutPutTable(self.m_freeFleets, "freeFleets")
  for i, v in pairs(self.m_freeFleets) do
    self:UpdateRestCommanderGrid(i, v)
  end
  self.m_nextPlayerRestFleets = LuaUtils:table_rcopy(self.m_freeFleets)
  return true
end
function GameObjectFormationBackground.CheckHasFreeFleets()
  local fleets_matrix = GameGlobalData:GetData("matrix").cells
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  if #fleets_matrix < #fleets then
    return true
  end
  return false
end
function GameObjectFormationBackground:initUI()
  self:GetFlashObject():InvokeASCallback("_root", "chooseMatrix", currentMatrixIndex)
  DebugOut("in initui")
  if immanentversion == 2 then
    local isLiAngInFormation = false
    for k, v in pairs(self.m_formationFleets) do
      if v == 2 then
        isLiAngInFormation = true
      end
    end
    if isLiAngInFormation and QuestTutorialBattleMapAfterRecruit:IsActive() then
      QuestTutorialBattleMapAfterRecruit:SetFinish(true)
    end
  end
  if self.CheckHasFreeFleets and QuestTutorialBattleMapAfterRecruit:IsActive() then
    QuestTutorialBattleMapAfterRecruit:SetFinish(true)
  end
  self:CheckQuestTutorial()
  self:UpdateEnemyInfo()
  self:UpdatePlayerInfo()
  self:UpdatePlayerPowerPoint()
  if self.jamedCount then
    self:UpdateCommanderJamed()
  end
  self:SelectCommander(1)
  self:AnimationMoveIn()
  if immanentversion == 2 and QuestTutorialBattleFailed:IsActive() then
    if 2 <= GameObjectFormationBackground:CountBattleCommander() then
      self:GetFlashObject():InvokeASCallback("_root", "SetShowRestMoveIn", false)
    end
    if GameObjectFormationBackground:CheckHaveChangeFleet() then
      QuestTutorialBattleFailed:SetFinish(true)
      self:GetFlashObject():InvokeASCallback("_root", "setNpcTipVisible", false)
    else
      local function callback()
        self:GetFlashObject():InvokeASCallback("_root", "setNpcTipVisible", true)
      end
      GameUICommonDialog:PlayStory({1100027, 11000271}, callback)
    end
  end
end
function GameObjectFormationBackground:setNextMatrixAndRestFleetsUI(isLeft, isSubRest)
  DebugOut("setNextMatrixAndRestFleetsUI", isLeft)
  DebugTable(self.m_nextPlayerMatrix)
  DebugTable(self.m_nextPlayerRestFleets)
  if isLeft then
    for i = 1, 9 do
      local commander_id = self.m_nextPlayerMatrix[i]
      if commander_id == nil then
        commander_id = -1
      end
      self:UpdateNextCommanderGrid(i, commander_id, "yours_L")
    end
    for i = 1, 9 do
      local commander_id = self.m_nextPlayerRestFleets[i]
      if commander_id == nil then
        commander_id = -1
      end
      local listType = "rest_list_L"
      if isSubRest then
        listType = "rest_list_L2"
      end
      self:UpdateNextCommanderGrid(i, commander_id, listType)
    end
  else
    for i = 1, 9 do
      local commander_id = self.m_nextPlayerMatrix[i]
      if commander_id == nil then
        commander_id = -1
      end
      self:UpdateNextCommanderGrid(i, commander_id, "yours_R")
    end
    for i = 1, 9 do
      local commander_id = self.m_nextPlayerRestFleets[i]
      if commander_id == nil then
        commander_id = -1
      end
      local listType = "rest_list_R"
      if isSubRest then
        listType = "rest_list_R2"
      end
      self:UpdateNextCommanderGrid(i, commander_id, listType)
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "HideAllSelectRound")
end
function GameObjectFormationBackground:loadNextMatrixAndRestFleets(index, subIndex)
  DebugOut("GameObjectFormationBackground loadNextMatrixAndRestFleets")
  currentMatrixIndex = index
  FleetMatrix.cur_index = currentMatrixIndex
  if self:GetFlashObject() then
    if FleetMatrix:getCurrentMartix() then
      self:GetFlashObject():InvokeASCallback("_root", "setCurrentMatrix", FleetMatrix:getCurrentMartix().name)
    else
      self:GetFlashObject():InvokeASCallback("_root", "setCurrentMatrix", "")
    end
  end
  if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
    self.m_nextPlayerRestFleets = {}
    local fleets = GameUICrusade:GetSelfFleetInfos()
    for k, v in pairs(fleets) do
      local isOnBattle = false
      for k2, v2 in pairs(self.m_formationFleets) do
        if v2 == v.fleet_id then
          isOnBattle = true
        end
      end
      if not isOnBattle then
        table.insert(self.m_nextPlayerRestFleets, v.fleet_id)
      end
    end
    DebugOutPutTable(self.m_formationFleets, "self.m_formationFleets")
    DebugOutPutTable(self.m_nextPlayerRestFleets, "self.m_nextPlayerRestFleets")
  else
    self.m_nextPlayerMatrix = {}
    self.m_nextPlayerRestFleets = {}
    local formationIds = {}
    local currentMatrix = FleetMatrix:getCurrentMartix().cells
    DebugOutPutTable(currentMatrix, "currentMatrix")
    for _, v in pairs(currentMatrix) do
      self.m_nextPlayerMatrix[v.cell_id] = v.fleet_identity
      if v.fleet_identity ~= -1 and v.fleet_identity ~= 0 then
        formationIds[v.fleet_identity] = 1
      end
    end
    for i = 1, 9 do
      if self.m_formationFleets[i] == 0 then
        self.m_nextPlayerMatrix[i] = self.m_formationFleets[i]
      end
      self.m_nextPlayerRestFleets[i] = -1
    end
    local leaderlist = GameGlobalData:GetData("leaderlist")
    self.curMainfleetIndex = 1
    self.mainFleetList = leaderlist.options
    local curr_id = leaderlist.leader_ids[currentMatrixIndex]
    for k, v in ipairs(self.mainFleetList) do
      if v == curr_id then
        self.curMainfleetIndex = k
        break
      end
    end
    DebugOut("m_nextPlayerMatrix")
    DebugTable(self.m_nextPlayerMatrix)
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local cell_index = 1
    for i, v in pairs(fleets) do
      if formationIds[v.identity] == nil then
        self.m_nextPlayerRestFleets[cell_index] = v.identity
        cell_index = cell_index + 1
      end
    end
  end
  if subIndex then
    local pageCnt = math.ceil((#self.m_nextPlayerRestFleets + 1) / 9)
    DebugOut("pageCnt " .. tostring(pageCnt) .. " " .. tostring(GameObjectFormationBackground.mCurFreeMatrixSubIdx))
    GameObjectFormationBackground.mCurFreeMatrixSubIdx = (GameObjectFormationBackground.mCurFreeMatrixSubIdx + subIndex + pageCnt) % pageCnt
    local subRestFleets = {}
    for k = 1, 9 do
      if self.m_nextPlayerRestFleets[k + 9 * GameObjectFormationBackground.mCurFreeMatrixSubIdx] then
        subRestFleets[k] = self.m_nextPlayerRestFleets[k + 9 * GameObjectFormationBackground.mCurFreeMatrixSubIdx]
      else
        subRestFleets[k] = -1
      end
    end
    DebugOut("subIndex = " .. tostring(subIndex) .. " mCurFreeMatrixSubIdx " .. tostring(GameObjectFormationBackground.mCurFreeMatrixSubIdx))
    DebugOut("before m_nextPlayerRestFleets")
    DebugTable(self.m_nextPlayerRestFleets)
    self.m_nextPlayerRestFleets = subRestFleets
    DebugOut("after m_nextPlayerRestFleets")
    DebugTable(self.m_nextPlayerRestFleets)
  else
  end
  DebugOut("m_nextPlayerMatrix")
  DebugTable(self.m_nextPlayerRestFleets)
end
function GameObjectFormationBackground:trySaveAndExchangeToMatrix(index, isleft)
  DebugOut("trySaveAndExchangeToMatrix", index)
  if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
  else
    FleetMatrix:SaveMatrixChanged(true)
  end
  local subIndex = 1
  if isleft then
    subIndex = -1
  end
  self:loadNextMatrixAndRestFleets(index, subIndex)
  self:setNextMatrixAndRestFleetsUI(isleft, true)
  if isleft then
    self:GetFlashObject():InvokeASCallback("_root", "RestFormationMoveToPrev")
  else
    self:GetFlashObject():InvokeASCallback("_root", "RestFormationMoveToNext")
  end
end
function GameObjectFormationBackground:InitForCrusade()
  local selfMatrix = GameUICrusade:GetSelfMatrix()
  local fleetList = GameUICrusade:GetSelfFleetInfos()
  currentMatrixIndex = 1
  GameObjectFormationBackground:InitFormation(selfMatrix, fleetList)
  GameObjectFormationBackground:initUI()
end
function GameObjectFormationBackground:RefreshBattleAndRestGrid()
  for i = 1, 9 do
    local commander_id = self.m_formationFleets[i]
    if commander_id == nil then
      commander_id = -1
    end
    self:UpdateBattleCommanderGrid(i, commander_id)
  end
  for i = 1, 9 do
    local commander_id = self.m_freeFleets[i]
    if commander_id == nil then
      commander_id = -1
    end
    self:UpdateRestCommanderGrid(i, commander_id)
  end
  self:SelectCommander(1)
end
function GameObjectFormationBackground:GetMajorIdByAdjutantId(adjutantId)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  for k, v in pairs(fleets) do
    if v.cur_adjutant == adjutantId then
      return v.identity
    end
  end
  return -1
end
function GameObjectFormationBackground:GetAdjutantCount(matrix)
  local cnt = 0
  for k, v in pairs(matrix) do
    local fleet = GameGlobalData:GetFleetInfo(v)
    if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
      fleet = GameUICrusade:GetSelfFleet(v)
    end
    if fleet and 0 < fleet.cur_adjutant then
      cnt = cnt + 1
    end
  end
  return cnt
end
function GameObjectFormationBackground:IsFleetOnMatrix(matrix, fleetId)
  for k, v in pairs(matrix) do
    local fleet = GameGlobalData:GetFleetInfo(v)
    if fleet and fleet.identity == fleetId then
      return true
    end
  end
  return false
end
function GameObjectFormationBackground:CheckAdjutant(src, des, draggedFleedId, fromIndex, toIndex)
  DebugOutPutTable(self.m_formationFleets, "battle_matrix")
  DebugOutPutTable(self.m_freeFleets, "rest_matrix")
  local adjutantMaxCnt = GameGlobalData:GetData("adjutant_max_count")
  DebugOut("adjutantMaxCnt " .. tostring(adjutantMaxCnt))
  local srcMatrix
  if src == "battle" then
    srcMatrix = self.m_formationFleets
  else
    srcMatrix = self.m_freeFleets
  end
  local destMatrix
  if des == "battle" then
    destMatrix = self.m_formationFleets
  else
    destMatrix = self.m_freeFleets
  end
  local destFleetId = destMatrix[toIndex]
  if nil == destFleetId then
    destFleetId = -1
  end
  local newBattleMatrix = {}
  for k, v in pairs(self.m_formationFleets) do
    newBattleMatrix[k] = v
  end
  if src == des then
    return 0
  elseif src == "battle" and des == "rest" and destFleetId == -1 then
    return 0
  elseif destFleetId > 0 then
    local addFleetId = -1
    if src == "battle" and des == "rest" then
      addFleetId = destFleetId
      newBattleMatrix[fromIndex] = destFleetId
    else
      addFleetId = draggedFleedId
      newBattleMatrix[toIndex] = draggedFleedId
    end
    if adjutantMaxCnt < GameObjectFormationBackground:GetAdjutantCount(newBattleMatrix) then
      return -1
    else
      if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
        return 0
      end
      local majorId = GameObjectFormationBackground:GetMajorIdByAdjutantId(addFleetId)
      if majorId > 0 then
        return 1, majorId, addFleetId
      else
        local fleetInfo = GameGlobalData:GetFleetInfo(addFleetId)
        if GameObjectFormationBackground:IsFleetOnMatrix(newBattleMatrix, fleetInfo.cur_adjutant) then
          return 1, addFleetId, fleetInfo.cur_adjutant
        else
          return 0
        end
      end
    end
  elseif src == "rest" and des == "battle" then
    newBattleMatrix[toIndex] = draggedFleedId
    if adjutantMaxCnt < GameObjectFormationBackground:GetAdjutantCount(newBattleMatrix) then
      return -1
    else
      if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
        return 0
      end
      local majorId = GameObjectFormationBackground:GetMajorIdByAdjutantId(draggedFleedId)
      if majorId > 0 then
        return 1, majorId, draggedFleedId
      else
        local fleetInfo = GameGlobalData:GetFleetInfo(draggedFleedId)
        if GameObjectFormationBackground:IsFleetOnMatrix(newBattleMatrix, fleetInfo.cur_adjutant) then
          return 1, draggedFleedId, fleetInfo.cur_adjutant
        else
          return 0
        end
      end
    end
  end
end
function GameObjectFormationBackground:CheckAdjutantBeforeSaveCurrentMatrix(onlyForResult)
  local adjutantMaxCnt = GameGlobalData:GetData("adjutant_max_count")
  if adjutantMaxCnt < GameObjectFormationBackground:GetAdjutantCount(self.m_formationFleets) then
    DebugOut("Out of max count adjutant can battle.")
    GameTip:Show(GameLoader:GetGameText("LC_MENU_BRIDGE_INFO_4"), 3000)
    return false
  end
  if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
    return true
  end
  for k, v in pairs(self.m_formationFleets) do
    if v > 0 and not self:IsExtraHero(k, v) then
      do
        local majorId = GameObjectFormationBackground:GetMajorIdByAdjutantId(v)
        if majorId > 0 then
          if nil == onlyForResult or not onlyForResult then
            do
              local tip = GameLoader:GetGameText("LC_MENU_BRIDGE_INFO_3")
              DebugOut("tip " .. tip)
              local adjutantName = GameDataAccessHelper:GetFleetLevelDisplayName(v)
              local majorName = GameDataAccessHelper:GetFleetLevelDisplayName(majorId)
              tip = string.gsub(tip, "<npc2_name>", adjutantName)
              tip = string.gsub(tip, "<npc1_name>", majorName)
              local function releaseAdjutantResultCallback(success)
                DebugOut("releaseAdjutantResultCallback " .. tostring(success))
                if success then
                  GameObjectFormationBackground:UpdateAdjutantBar()
                  GameObjectFormationBackground:UpdateBattleCommanderByFleetId(majorId)
                  GameObjectFormationBackground:CheckAdjutantBeforeSaveCurrentMatrix()
                else
                end
              end
              local function okCallback()
                GameStateFormation:RequestReleaseAdjutant(majorId, releaseAdjutantResultCallback)
              end
              local cancelCallback = function()
              end
              GameUIGlobalScreen:ShowMessageBox(2, "", tip, okCallback, cancelCallback)
            end
          end
          return false
        end
      end
    end
  end
  return true
end
function GameObjectFormationBackground:SetFleetsBuff(fleetsBuff)
  self.fleetsBuffer = fleetsBuff
end
function GameObjectFormationBackground:UpdateAdjutantBar()
  local flashObj = self:GetFlashObject()
  if flashObj then
    local adjutantCnt = 0
    local adjutantAvatarFrs = ""
    local adjutantbgColor = ""
    for k, v in pairs(self.m_formationFleets) do
      local commander_data2 = GameGlobalData:GetFleetInfo(v)
      local level = 0
      if GameStateFormation.m_prevState == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
        commander_data2 = GameUICrusade:GetSelfFleet(v)
      end
      if v > 0 and commander_data2 ~= nil and commander_data2.cur_adjutant and commander_data2.cur_adjutant > 1 then
        if commander_data2.level then
          level = commander_data2.level
        end
        adjutantAvatarFrs = adjutantAvatarFrs .. GameDataAccessHelper:GetFleetAvatar(commander_data2.cur_adjutant) .. "\001"
        adjutantbgColor = adjutantbgColor .. GameDataAccessHelper:GetCommanderColorFrame(commander_data2.cur_adjutant, commander_data2.adjutant_level) .. "\001"
        adjutantCnt = adjutantCnt + 1
      end
    end
    local adjutantUnlockCnt = GameGlobalData:GetData("adjutant_max_count")
    DebugOut("SetAdjutantBar " .. tostring(adjutantCnt) .. " " .. adjutantAvatarFrs)
    flashObj:InvokeASCallback("_root", "SetAdjutantBar", adjutantUnlockCnt, adjutantCnt, adjutantAvatarFrs, adjutantbgColor)
  end
end
function GameObjectFormationBackground:UpdateBattleCommanderByFleetId(fleetId)
  for k, v in pairs(self.m_formationFleets) do
    if fleetId == v then
      GameObjectFormationBackground:UpdateCommanderGrid("battle", k, fleetId)
      break
    end
  end
end
