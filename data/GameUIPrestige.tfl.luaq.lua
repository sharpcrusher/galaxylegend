local GameUIPrestige = LuaObjectManager:GetLuaObject("GameUIPrestige")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameFleetInfoTabBar = LuaObjectManager:GetLuaObject("GameFleetInfoTabBar")
local QuestTutorialPrestige = TutorialQuestManager.QuestTutorialPrestige
GameUIPrestige.PrestigeStatus = {}
GameUIPrestige.PrestigeStatus.Unfinished = "unfinished"
GameUIPrestige.PrestigeStatus.Wait = "wait"
GameUIPrestige.PrestigeStatus.Received = "received"
local QuestTutorialPrestige = TutorialQuestManager.QuestTutorialPrestige
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local QuestTutorialEnhance_second = TutorialQuestManager.QuestTutorialEnhance_second
GameUIPrestige._PrestigeData = {}
function GameUIPrestige:Show()
  DebugOut("GameUIPrestige:Show")
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "LC_MENU_NEXT_RANK", GameLoader:GetGameText("LC_MENU_NEXT_RANK"))
  GameStateManager:GetCurrentGameState():AddObject(self)
  self:GetFlashObject():InvokeASCallback("_root", "ShowCloseTip", QuestTutorialPrestige:IsActive())
  GameUIPrestige:ReqPrestigeInfo()
end
function GameUIPrestige:ReqPrestigeInfo()
  local function netFailedCallback()
    NetMessageMgr:SendMsg(NetAPIList.prestige_info_req.Code, nil, self._NetCallbackProc, true, netFailedCallback)
  end
  NetMessageMgr:SendMsg(NetAPIList.prestige_info_req.Code, nil, self._NetCallbackProc, true, netFailedCallback)
end
function GameUIPrestige:OnEraseFromGameState()
  if QuestTutorialPrestige:IsActive() and immanentversion == 2 then
    GameUIPrestige:SetPrestigeTutorialFinish()
  end
  self:UnloadFlashObject()
end
function GameUIPrestige:OnFSCommand(cmd, arg)
  DebugOut("GameUIPrestige:OnFSCommand(cmd, arg)", cmd, arg)
  if cmd == "update_prestige_item" then
    self:UpdatePrestigeItem(tonumber(arg))
    return
  elseif cmd == "select_ranking" then
    self:UpdateActivePrestigeInfo(tonumber(arg))
    return
  elseif cmd == "receive_award" then
    self:ObtainAward(tonumber(arg))
    return
  elseif cmd == "close" then
    GameStateManager:GetCurrentGameState():EraseObject(self)
    GameUIPrestige:SetPrestigeTutorialFinish()
  elseif cmd == "btn_help" then
    local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_MENU_PRESTIGE_HELP"))
  elseif cmd == "MenuItemReleased" then
    if immanentversion == 1 then
      local index_item = tonumber(arg)
      local data_prestige = self:GetPrestigeInfo(index_item)
      local prestige_name = GameDataAccessHelper:GetMilitaryRankName(data_prestige.prestige_level)
      local award_data, awardDetail = GameDataAccessHelper:GenerateMilitaryRankAwardInfo(data_prestige.prestige_level)
      DebugOutPutTable(awardDetail, "awardDetail")
      local daily_award = GameDataAccessHelper:GenerateMilitaryRankDailyAwardInfo(data_prestige.prestige_level)
      local firstAwardDetail = awardDetail[1]
      local prestige_info_param = prestige_name
      if firstAwardDetail[1] == "skill" then
        local skill = firstAwardDetail[2]
        local skillName = GameDataAccessHelper:GetSkillNameText(skill)
        local skillDesc = GameDataAccessHelper:GetSkillDesc(skill)
        local skillRangeType = "TYPE_" .. GameDataAccessHelper:GetSkillRangeType(skill)
        local skillRangetypeName = GameLoader:GetGameText("LC_MENU_SKILL_RANGE_TYPE_" .. GameDataAccessHelper:GetSkillRangeType(skill))
        prestige_info_param = prestige_info_param .. "\002" .. "skill\001" .. skill .. "\001" .. skillName .. "\001" .. skillDesc .. "\001" .. skillRangeType .. "\001" .. skillRangetypeName .. "\001"
      elseif firstAwardDetail[1] == "recruit_count_up" then
        local recruit_count = firstAwardDetail[2]
        local recruit_count_name = GameLoader:GetGameText("LC_MENU_RECRUIT_COUNT_NAME")
        local recruit_count_desc = GameLoader:GetGameText("LC_MENU_RECRUIT_COUNT_DESC_" .. recruit_count)
        prestige_info_param = prestige_info_param .. "\002" .. "recruit_count_up\001" .. recruit_count .. "\001" .. recruit_count_name .. "\001" .. recruit_count_desc .. "\001"
      elseif firstAwardDetail[1] == "unlock_formation" then
        local unlock_formation = firstAwardDetail[2]
        local unlock_formation_name = GameLoader:GetGameText("LC_MENU_UNLOCK_FORMATION_NAME")
        local unlock_formation_desc = GameLoader:GetGameText("LC_MENU_UNLOCK_FORMATION_DESC")
        prestige_info_param = prestige_info_param .. "\002" .. "unlock_formation\001" .. unlock_formation .. "\001" .. unlock_formation_name .. "\001" .. unlock_formation_desc .. "\001"
      else
        prestige_info_param = prestige_info_param .. "\002empty"
      end
      prestige_info_param = prestige_info_param .. "\002" .. daily_award
      ItemBox:ShowPrestigeInfo(prestige_info_param)
    elseif immanentversion == 2 then
      local temp = tonumber(arg)
      if tonumber(temp) == 1 then
        GameUIPrestige:showPrestigePopView(temp, 3)
        GameUIPrestige:showPrestigePopView(temp, 2)
      else
        GameUIPrestige:showPrestigePopView(temp - 1, 1)
        GameUIPrestige:showPrestigePopView(temp, 2)
      end
      if QuestTutorialPrestige:IsActive() then
        GameUIPrestige:SetPrestigeTutorialFinish()
        GameUICommonDialog:PlayStory({51131}, nil)
        GameUIPrestige:UpdatePrestigeItem(tonumber(arg))
      end
    end
  end
end
function GameUIPrestige._NetCallbackProc(msgtype, content)
  if msgtype == NetAPIList.prestige_info_ack.Code then
    DebugTable(content)
    GameUIPrestige:ProcessPrestigeData(content)
    GameUIPrestige:InitPrestigeList()
    return true
  end
  if msgtype == NetAPIList.prestige_obtain_award_ack.Code then
    if content.code ~= 0 then
    end
    local data_prestige = GameUIPrestige:GetPrestigeInfo(GameUIPrestige._ObtainLevel)
    data_prestige.obtained = true
    GameUIPrestige:UpdatePrestigeItem(data_prestige.prestige_level)
    return true
  end
  if msgtype == 0 and content.api == NetAPIList.prestige_get_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    if GameStateManager:GetCurrentGameState():IsObjectInState(GameUIPrestige) then
      GameUIPrestige:ReqPrestigeInfo()
    end
    return true
  end
  return false
end
function GameUIPrestige:SetPrestigeTutorialFinish()
  QuestTutorialPrestige:SetFinish(true)
end
function GameUIPrestige:showPrestigePopView(arg, tag)
  local index_item = tonumber(arg)
  local data_prestige = self:GetPrestigeInfo(index_item)
  if index_item < 5 and self:CheckPrestigeStatus(data_prestige) == "received" then
    local flurryNameTmp = "TutorialPrestige" .. index_item .. "_ClickTab"
    AddFlurryEvent(flurryNameTmp, {}, 2)
  end
  if tag == 3 then
    ItemBox:ShowPrestigeUpdate(tag)
    return
  end
  local prestige_name = GameDataAccessHelper:GetMilitaryRankName(data_prestige.prestige_level)
  local award_data, awardDetail = GameDataAccessHelper:GenerateMilitaryRankAwardInfo(data_prestige.prestige_level)
  DebugOutPutTable(awardDetail, "awardDetail")
  local daily_award = GameDataAccessHelper:GenerateMilitaryRankDailyAwardInfo(data_prestige.prestige_level)
  local firstAwardDetail = awardDetail[1]
  local prestige_info_param = prestige_name
  local content_1_desc = ""
  content_1_desc = content_1_desc .. prestige_name .. "\001"
  if firstAwardDetail[1] == "skill" then
    local skill = firstAwardDetail[2]
    local skillName = GameDataAccessHelper:GetSkillNameText(skill)
    local skillDesc = GameDataAccessHelper:GetSkillDesc(skill)
    local skillRangeType = "TYPE_" .. GameDataAccessHelper:GetSkillRangeType(skill)
    local skillRangetypeName = GameLoader:GetGameText("LC_MENU_SKILL_RANGE_TYPE_" .. GameDataAccessHelper:GetSkillRangeType(skill))
    prestige_info_param = prestige_info_param .. "\002" .. "skill\001" .. skill .. "\001" .. skillName .. "\001" .. skillDesc .. "\001" .. skillRangeType .. "\001" .. skillRangetypeName .. "\001"
    content_1_desc = content_1_desc .. skillRangeType .. "\001" .. GameLoader:GetGameText("LC_MENU_PRESTIGE_NEW_SKILL_CHAR") .. "\001" .. skillRangetypeName .. "\001" .. "skill_" .. skill .. "\001"
  elseif firstAwardDetail[1] == "recruit_count_up" then
    local recruit_count = firstAwardDetail[2]
    local recruit_count_name = GameLoader:GetGameText("LC_MENU_RECRUIT_COUNT_NAME")
    local recruit_count_desc = GameLoader:GetGameText("LC_MENU_RECRUIT_COUNT_DESC_" .. recruit_count)
    prestige_info_param = prestige_info_param .. "\002" .. "recruit_count_up\001" .. recruit_count .. "\001" .. recruit_count_name .. "\001" .. recruit_count_desc .. "\001"
    content_1_desc = content_1_desc .. "false" .. "\001" .. recruit_count_name .. "\001" .. recruit_count - 1 .. "+1" .. "\001" .. "recruit_count_up" .. "\001"
  elseif firstAwardDetail[1] == "unlock_formation" then
    local unlock_formation = firstAwardDetail[2]
    local unlock_formation_name = GameLoader:GetGameText("LC_MENU_UNLOCK_FORMATION_NAME")
    local unlock_formation_desc = GameLoader:GetGameText("LC_MENU_UNLOCK_FORMATION_DESC")
    prestige_info_param = prestige_info_param .. "\002" .. "unlock_formation\001" .. unlock_formation .. "\001" .. unlock_formation_name .. "\001" .. unlock_formation_desc .. "\001"
    content_1_desc = content_1_desc .. "false" .. "\001" .. unlock_formation_name .. "\001" .. unlock_formation_desc .. "\001" .. "unlock_formation_" .. unlock_formation .. "\001"
  else
    prestige_info_param = prestige_info_param .. "\002" .. "empty"
    content_1_desc = content_1_desc .. "false" .. "\001" .. " " .. "\001" .. " " .. "\001" .. "empty" .. "\001"
  end
  prestige_info_param = prestige_info_param .. "\002" .. daily_award
  local content_1_parm = ""
  DebugTable(data_prestige.attribute)
  DebugTable(data_prestige)
  if tag == 1 then
    content_1_parm = data_prestige.crit_lv .. "\001" .. "0" .. "\001" .. data_prestige.motility .. "\001" .. "0" .. "\001" .. data_prestige.hit_rate .. "\001" .. "0" .. "\001" .. data_prestige.anti_crit .. "\001" .. "0" .. "\001"
  end
  if tag == 2 then
    local lastData = self:GetPrestigeInfo(index_item - 1)
    if not lastData then
      lastData = {}
      lastData.crit_lv = 0
      lastData.motility = 0
      lastData.hit_rate = 0
      lastData.anti_crit = 0
    end
    content_1_parm = data_prestige.crit_lv .. "\001" .. data_prestige.crit_lv - lastData.crit_lv .. "\001" .. data_prestige.motility .. "\001" .. data_prestige.motility - lastData.motility .. "\001" .. data_prestige.hit_rate .. "\001" .. data_prestige.hit_rate - lastData.hit_rate .. "\001" .. data_prestige.anti_crit .. "\001" .. data_prestige.anti_crit - lastData.anti_crit .. "\001"
  end
  DebugOut("---daily_award--------> ", daily_award, content_1_parm)
  local RankUpVisible = false
  local animaVisible = false
  ItemBox:ShowPrestigeUpdate(tag, content_1_desc, content_1_parm, daily_award, data_prestige.prestige_level, RankUpVisible, animaVisible)
  DebugOut("---> out ")
end
function GameUIPrestige:RankUpPrstige(arg)
end
function GameUIPrestige:OnAddToGameState(state)
  if QuestTutorialPrestige:IsActive() and not QuestTutorialPrestige:IsFinished() then
    GameFleetInfoTabBar.closeAnimTip = "prestige"
  end
end
function GameUIPrestige:HideTitleBar()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "HideTitleBar")
  end
end
function GameUIPrestige:InitFlashObject()
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "LC_MENU_CONTENT_NEED_PRESTIGE", GameLoader:GetGameText("LC_MENU_CONTENT_NEED_PRESTIGE"))
end
function GameUIPrestige:InitPrestigeList()
  self:GetFlashObject():InvokeASCallback("_root", "lua2fs_updatePrestigeList", #self._PrestigeData.prestige_infos)
  if self._PrestigeData.cur_level < self._PrestigeData.max_level then
    self:GetFlashObject():InvokeASCallback("_root", "setActivePrestigeRank", self._PrestigeData.cur_level + 1)
  end
end
function GameUIPrestige:ProcessPrestigeData(prestiges_data)
  self._PrestigeData = prestiges_data
  prestiges_data.cur_level = 0
  prestiges_data.max_level = 0
  for _, rank_info in ipairs(prestiges_data.prestige_infos) do
    if rank_info.obtained and rank_info.prestige_level > prestiges_data.cur_level then
      prestiges_data.cur_level = rank_info.prestige_level
    end
    if rank_info.prestige_level > prestiges_data.max_level then
      prestiges_data.max_level = rank_info.prestige_level
    end
  end
end
function GameUIPrestige:UpdatePrestigeItem(index_item)
  local data_prestige = self:GetPrestigeInfo(index_item)
  local prev_prestige_data = self:GetPrestigeInfo(index_item - 1)
  local award_data = GameDataAccessHelper:GenerateMilitaryRankAwardInfo(data_prestige.prestige_level)
  local canNextLevelTxt = GameLoader:GetGameText("LC_MENU_PRESTIGE_UPGRADE_CHAR")
  local visible = false
  DebugOut("award_data - ", award_data)
  local prestige_item_param = ""
  prestige_item_param = prestige_item_param .. tostring(data_prestige.prestige_level)
  prestige_item_param = prestige_item_param .. "," .. GameDataAccessHelper:GetMilitaryRankName(data_prestige.prestige_level)
  prestige_item_param = prestige_item_param .. "," .. self:CheckPrestigeStatus(data_prestige)
  prestige_item_param = prestige_item_param .. "," .. tostring(self._PrestigeData.prestige)
  prestige_item_param = prestige_item_param .. "," .. tostring(data_prestige.prestige_require)
  prestige_item_param = prestige_item_param .. "," .. tostring(GameUtils.numberConversion(self._PrestigeData.prestige))
  prestige_item_param = prestige_item_param .. "," .. tostring(GameUtils.numberConversion(data_prestige.prestige_require))
  prestige_item_param = prestige_item_param .. "," .. award_data
  local daily_award = GameDataAccessHelper:GenerateMilitaryRankDailyAwardInfo(data_prestige.prestige_level)
  local title = GameLoader:GetGameText("LC_MENU_PRESTIGE_DAILY_REWARDS_TITLE")
  DebugOut("daily_award - ", daily_award, data_prestige.prestige_level)
  self:GetFlashObject():InvokeASCallback("_root", "UpdatePrestigeItem", index_item, prestige_item_param, canNextLevelTxt, QuestTutorialPrestige:IsActive(), tonumber(immanentversion), daily_award, title)
end
function GameUIPrestige:ObtainAward(level)
  if immanentversion == 1 then
    self._ObtainLevel = level
    local packet = {prestige_level = level}
    NetMessageMgr:SendMsg(NetAPIList.prestige_obtain_award_req.Code, packet, self._NetCallbackProc, true, nil)
  end
end
function GameUIPrestige:GetPrestigeInfo(level)
  for _, v in ipairs(self._PrestigeData.prestige_infos) do
    if v.prestige_level == level then
      return v
    end
  end
  return nil
end
function GameUIPrestige:CheckPrestigeStatus(prestige_info)
  local prestige_status = "unfinished"
  if self._PrestigeData.cur_level == prestige_info.prestige_level - 1 then
    prestige_status = "active"
  elseif prestige_info.prestige_require < self._PrestigeData.prestige then
    if prestige_info.obtained then
      prestige_status = "received"
    else
      prestige_status = "unreceived"
    end
  end
  return prestige_status
end
function GameUIPrestige:Update(dt)
  local flash_obj = self:GetFlashObject()
  flash_obj:Update(dt)
  flash_obj:InvokeASCallback("_root", "lua2fs_updateFrame", dt)
end
