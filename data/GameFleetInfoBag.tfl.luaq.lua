local GameStateFleetInfo = GameStateManager.GameStateFleetInfo
local GameFleetInfoBag = LuaObjectManager:GetLuaObject("GameFleetInfoBag")
local GameFleetInfoUI = LuaObjectManager:GetLuaObject("GameFleetInfoUI")
local NetMessageMgr = NetMessageMgr
local NetAPIList = NetAPIList
local AlertDataList = AlertDataList
local GameGlobalData = GameGlobalData
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameItemBag = LuaObjectManager:GetLuaObject("GameItemBag")
local QuestTutorialEquip = TutorialQuestManager.QuestTutorialEquip
local QuestTutorialsChangeFleets1 = TutorialQuestManager.QuestTutorialsChangeFleets1
local GameFleetInfoBackground = LuaObjectManager:GetLuaObject("GameFleetInfoBackground")
local GameFleetInfoUI = LuaObjectManager:GetLuaObject("GameFleetInfoUI")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local QuestTutorialEquipAllByOneKey = TutorialQuestManager.QuestTutorialEquipAllByOneKey
GameFleetInfoBag.BAG_OWNER_USER = 1
GameFleetInfoBag.BAG_OWNER_FLEET = 2
GameUtils.DOUBLE_CLICK_TIMER = 200
GameFleetInfoBag.MAX_LINE = 3
GameFleetInfoBag.LINE_COUNT = 4
function GameFleetInfoBag:Init()
  DebugOut("InitItemList")
  GameFleetInfoBag:GetOnlyEquip()
  self.lastReleasedIndex = -1
  self.pressIndex = -1
  self.clicked = false
  self.doubClicked = false
  self.dragItem = false
  GameFleetInfoBag:ShowEquipmentItem()
end
function GameFleetInfoBag:OnAddToGameState()
  self:LoadFlashObject()
  self:Init()
  if QuestTutorialEquip:IsActive() and GameFleetInfoBag.onlyEquip[1] ~= nil then
    self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialAnimTip")
  elseif QuestTutorialEquip:IsActive() and GameFleetInfoBag.onlyEquip[2] ~= nil then
    self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialAnimTip2")
  elseif QuestTutorialEquip:IsActive() then
    self:GetFlashObject():InvokeASCallback("_root", "HideTutorialAnimTip")
    self:GetFlashObject():InvokeASCallback("_root", "HideTutorialAnimTip2")
  else
    self:GetFlashObject():InvokeASCallback("_root", "HideTutorialAnimTip")
    self:GetFlashObject():InvokeASCallback("_root", "HideTutorialAnimTip2")
  end
  if immanentversion == 2 then
  elseif immanentversion == 1 and QuestTutorialsChangeFleets1:IsActive() then
    self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialAnimTip")
  end
end
function GameFleetInfoBag:OnEraseFromGameState()
  self:Clear()
  self:UnloadFlashObject()
end
function GameFleetInfoBag:CheckTutorial()
  if immanentversion == 1 and not QuestTutorialsChangeFleets1:IsActive() and not QuestTutorialsChangeFleets1:IsFinished() and GameFleetInfoBag.onlyEquip and GameFleetInfoBag.onlyEquip[1] ~= nil then
    QuestTutorialsChangeFleets1:SetActive(true)
    if self:GetFlashObject() then
      self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialAnimTip")
    end
  end
end
function GameFleetInfoBag:SetQueestTutorialsChangeFleet1True()
  if immanentversion == 1 then
    QuestTutorialsChangeFleets1:SetFinish(true)
    if self:GetFlashObject() then
      GameFleetInfoBag:GetFlashObject():InvokeASCallback("_root", "HideTutorialAnimTip")
    end
  end
end
function GameFleetInfoBag:Clear()
end
function GameFleetInfoBag:Update(dt)
  self:GetFlashObject():InvokeASCallback("_root", "UpdateEquipmentInBag")
  self:GetFlashObject():Update(dt)
  self:UpdateReleased(dt)
end
function GameFleetInfoBag:UpdateReleased(dt)
  if self.doubClicked then
    self.doubClicked = false
    DebugOut("self.doubclicked")
    self:DoubClicked()
  elseif self.clicked and self.clickedTimer > 0 then
    self.clickedTimer = self.clickedTimer - dt
    if self.clickedTimer <= 0 then
      DebugOut("self.lastReleasedIndex: ", self.lastReleasedIndex)
      local paramString = self:GetFlashObject():InvokeASCallback("_root", "GetItemRect", self.lastReleasedIndex)
      local param = LuaUtils:string_split(paramString, "\001")
      local equip = GameFleetInfoBag.onlyEquip[self.lastReleasedIndex]
      local equipType = GameFleetInfoBag.onlyEquip[self.lastReleasedIndex].item_type
      if GameDataAccessHelper:IsEquipment(equip.item_id) then
        operateText = GameLoader:GetGameText("LC_MENU_EQUIPMENT_BUTTON")
        operateFunc = self.EquipmentOperatorCallback
        local enhanceText = GameLoader:GetGameText("LC_MENU_ENHANCE_BUTTON")
        local opLeftFunc = GameFleetInfoBag.gotoEnhance
        if not TutorialQuestManager.QuestTutorialCityHall:IsFinished() then
          enhanceText, opLeftFunc = nil, nil
        end
        ItemBox:showItemBox("Equip", equip, equipType, tonumber(param[1]), tonumber(param[2]), tonumber(param[3]), tonumber(param[4]), enhanceText, opLeftFunc, operateText, operateFunc)
      else
        local operateLeftText, operateLeftFunc = "", nil
        GameItemBag.sellIndex = self.lastReleasedIndex
        local operateRightText, operateRightFunc = "", nil
        if equipType > 2000 then
          operateLeftText = GameLoader:GetGameText("LC_MENU_USE_CHAR")
          function operateLeftFunc()
            local use_item_req = {
              pos = self.lastReleasedIndex,
              is_max = false
            }
            NetMessageMgr:SendMsg(NetAPIList.use_item_req.Code, use_item_req, GameFleetInfoBag.UseItemCallBack, false, nil)
          end
        end
        ItemBox:showItemBox("Item", equip, equipType, tonumber(param[1]), tonumber(param[2]), tonumber(param[3]), tonumber(param[4]), operateLeftText, operateLeftFunc, operateRightText, operateRightFunc)
      end
      self.clicked = false
      DebugOut("self.clicked")
    end
  end
end
function GameFleetInfoBag.EquipmentOperatorCallback()
  local equip = GameFleetInfoBag.onlyEquip[GameFleetInfoBag.lastReleasedIndex]
  if equip then
    GameFleetInfoBag:Equip(equip.item_id)
  end
end
function GameFleetInfoBag.gotoEnhance()
  local GameStateEquipEnhance = GameStateManager.GameStateEquipEnhance
  local GameFleetEnhance = LuaObjectManager:GetLuaObject("GameFleetEnhance")
  GameStateEquipEnhance:EnterEquipEnhance(GameFleetEnhance.TAB_ENHANCE)
end
function GameFleetInfoBag.UseItemCallBack(msgType, content)
  if content.code == NetAPIList.common_ack.Code and content.api == NetAPIList.use_item_req.Code then
    GameFleetInfoBag:ShowEquipmentItem()
    return true
  else
    if content.code then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameFleetInfoBag:DoubClicked()
  self:Equip()
end
function GameFleetInfoBag:ShouldEquip(itemId)
  local _, index = self:IsEquipmentInList(itemId)
  if index then
    local itemType = GameFleetInfoBag.onlyEquip[index].item_type
    local slot = GameDataAccessHelper:GetEquipSlot(itemType)
    GameFleetInfoBag:Equip(itemId)
  end
end
function GameFleetInfoBag:Equip(itemId)
  local _, index = self:IsEquipmentInList(itemId)
  index = index or self.lastReleasedIndex
  self.equipIndex = index
  local itemType = GameFleetInfoBag.onlyEquip[index].item_type
  local itemId = GameFleetInfoBag.onlyEquip[index].item_id
  local srcPos = GameFleetInfoBag.onlyEquip[index].pos
  local slot = GameDataAccessHelper:GetEquipSlot(itemType) or -1
  if slot == -1 then
    return
  end
  self.equipSlot = slot
  if self:CanEquip(itemType) then
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local swap_bag_grid_req_content = {
      orig_grid_owner = GameGlobalData:GetData("userinfo").player_id,
      oirg_grid_type = GameFleetInfoBag.BAG_OWNER_USER,
      oirg_grid_pos = srcPos,
      target_grid_owner = fleets[GameFleetInfoUI.currentSlectedIndex].id,
      target_grid_type = GameFleetInfoBag.BAG_OWNER_FLEET,
      target_grid_pos = slot
    }
    local function netFailedCallback()
      GameStateFleetInfo:onEndDragItem()
      GameItemBag:RequestBag()
    end
    NetMessageMgr:SendMsg(NetAPIList.swap_bag_grid_req.Code, swap_bag_grid_req_content, GameFleetInfoBag.EquipCallback, true, netFailedCallback)
  else
    DebugOut("can not Equip")
    if GameFleetInfoBag.dragItemInstance then
      GameStateFleetInfo:onEndDragItem()
    end
  end
end
function GameFleetInfoBag:CanEquip(itemType)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetIdentity = fleets[GameFleetInfoUI.currentSlectedIndex].identity
  if GameDataAccessHelper:GetEquipReqFleet(itemType) == GameDataAccessHelper.ATK_TYPE_energy and GameDataAccessHelper:IsEnergyAttack(fleetIdentity) then
    return true
  elseif GameDataAccessHelper:GetEquipReqFleet(itemType) == GameDataAccessHelper.ATK_TYPE_physics and not GameDataAccessHelper:IsEnergyAttack(fleetIdentity) then
    return true
  elseif GameDataAccessHelper:GetEquipReqFleet(itemType) == GameDataAccessHelper.ATK_TYPE_common then
    return true
  end
  GameTip:Show(GameLoader:GetGameText("LC_MENU_EQUIP_TYPE_ERR"), 3000)
  return false
end
function GameFleetInfoBag.EquipCallback(msgType, content)
  DebugOut("\230\137\147\229\141\176--EquipCallback----")
  if msgType == NetAPIList.swap_bag_grid_ack.Code then
    if content.result == 0 then
      if GameFleetInfoBag.onlyEquip[GameFleetInfoBag.lastReleasedIndex] then
        table.remove(GameFleetInfoBag.onlyEquip, lastReleasedIndex)
      end
      local item = GameFleetInfoUI:IsEquipmentInSlot(GameFleetInfoBag.equipSlot)
      DebugTable(item)
      if GameFleetInfoBag.dragItemInstance and item then
        GameFleetInfoBag.dragItemInstance.DraggedItemID = -1
        local frame = "item_" .. GameFleetInfoBag:GetDynamic(item.item_type, 0, 0)
        GameFleetInfoBag.dragItemInstance:GetFlashObject():InvokeASCallback("_root", "SetBagItem", frame)
        GameFleetInfoBag.dragItemInstance:SetDestPosition(GameFleetInfoBag.dragX, GameFleetInfoBag.dragY)
        GameFleetInfoBag.dragItemInstance:StartAutoMove()
      elseif GameFleetInfoBag.dragItemInstance then
        GameStateFleetInfo:onEndDragItem()
      end
      return true
    else
      GameTip:Show(AlertDataList:GetTextFromErrorCode(content.result), 3000)
      if GameFleetInfoBag.dragItemInstance then
        GameStateFleetInfo:onEndDragItem()
      end
      return true
    end
  end
  return false
end
function GameFleetInfoBag:refreshBag()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "refreshBag")
  end
end
function GameFleetInfoBag:IsEquipment(subId)
  local isEquip = true
  if not GameDataAccessHelper:GetEquipPrice(GameFleetInfoBag.onlyEquip[subId].item_type) ~= -1 then
    isEquip = false
    if GameFleetInfoBag.onlyEquip[subId].item_type > 2000 then
      isEquip = true
    end
  end
  return isEquip
end
function GameFleetInfoBag:OnFSCommand(cmd, arg)
  if cmd == "FleetInfoBagItemReleased" then
    if self.doubClicked then
      return
    end
    local currentReleaasedIndex = tonumber(arg)
    if currentReleaasedIndex == self.lastReleasedIndex and self.clicked then
      self.doubClicked = true
      self.clicked = false
    else
      self.clicked = true
      self.clickedTimer = GameUtils.DOUBLE_CLICK_TIMER
    end
    self.lastReleasedIndex = currentReleaasedIndex
  end
  if cmd == "needUpdateItem" then
    local id = tonumber(arg)
    local frame, isEquip, number = "", "", ""
    local equipments = GameGlobalData:GetData("equipments")
    for i = 0, 3 do
      do
        local subId = id + i
        if GameFleetInfoBag.onlyEquip[subId] then
          local currentEquipInfo = LuaUtils:table_values(LuaUtils:table_filter(equipments, function(k, v)
            return v.equip_id == GameFleetInfoBag.onlyEquip[subId].item_id
          end))[1]
          frame = frame .. tostring("item_" .. GameFleetInfoBag:GetDynamic(GameFleetInfoBag.onlyEquip[subId].item_type), id, i) .. "\001"
          isEquip = isEquip .. tostring(self:IsEquipment(subId)) .. "\001"
          number = number .. GameLoader:GetGameText("LC_MENU_Level") .. currentEquipInfo.equip_level .. "\001"
          DebugOut("equip level", number)
        else
          frame = frame .. "empty" .. "\001"
          isEquip = isEquip .. "true" .. "\001"
          number = number .. 0 .. "\001"
        end
      end
    end
    DebugOut("needUpdateItem: ", frame)
    self:GetFlashObject():InvokeASCallback("_root", "setItem", id, frame, isEquip, number)
  end
  if cmd == "beginPress" then
    self.pressIndex = tonumber(arg)
  elseif cmd == "endPress" then
    self.pressIndex = -1
  end
  if cmd == "dragItem" then
    local param = LuaUtils:string_split(arg, "\001")
    local initX, initY, posX, posY = unpack(param)
    self.dragX = tonumber(initX)
    self.dragY = tonumber(initY)
    if self.pressIndex ~= -1 then
      local id = GameFleetInfoBag.onlyEquip[self.pressIndex].item_id
      local _type = GameFleetInfoBag.onlyEquip[self.pressIndex].item_type
      self.dragItem = true
      GameStateManager:GetCurrentGameState():BeginDragItem(id, _type, initX, initY, posX, posY)
    end
  end
end
function GameFleetInfoBag:GetDynamic(frame, itemKey, index)
  local frame_temp = frame
  if DynamicResDownloader:IsDynamicStuff(frame_temp, DynamicResDownloader.resType.PIC) then
    local fullFileName = DynamicResDownloader:GetFullName(frame_temp .. ".png", DynamicResDownloader.resType.PIC)
    local localPath = "data2/" .. fullFileName
    if DynamicResDownloader:IfResExsit(localPath) then
      ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
    else
      frame_temp = "temp"
      self:AddDownloadPath(frame_temp, itemKey, index)
    end
  end
  return frame_temp
end
function GameFleetInfoBag:AddDownloadPath(itemID, itemKey, index)
  DebugStore("Add download path", itemID, itemKey, index)
  local resName = itemID .. ".png"
  local extInfo = {}
  extInfo.itemKey = itemKey
  extInfo.index = index
  extInfo.item_id = itemID
  extInfo.resPath = "data2/LAZY_LOAD_dynamic_" .. resName
  DynamicResDownloader:AddDynamicRes(resName, DynamicResDownloader.resType.PIC, extInfo, GameFleetInfoBag.DowloadDynamicPicCallback)
end
function GameFleetInfoBag.DowloadDynamicPicCallback(extInfo)
  if DynamicResDownloader:IfResExsit(extInfo.resPath) and GameFleetInfoBag:GetFlashObject() then
    local frame = "item_" .. extInfo.item_id
    GameFleetInfoBag:GetFlashObject():InvokeASCallback("_root", "DowloadDynamicPicCallback", frame, extInfo.itemKey, extInfo.index)
  end
end
function GameFleetInfoBag:ShowEquipmentItem()
  if self:GetFlashObject() then
    GameFleetInfoBag:GetFlashObject():InvokeASCallback("_root", "clearListBox")
    GameFleetInfoBag:GetFlashObject():InvokeASCallback("_root", "InitItemList")
    GameFleetInfoBag:AddEquipmentItem()
  end
end
function GameFleetInfoBag:GetOnlyEquip()
  self.onlyEquip = LuaUtils:table_values(LuaUtils:table_filter(GameItemBag.itemInBag, function(k, v)
    return tonumber(v.item_type) > 100000 and tonumber(v.item_type) < 110000
  end))
  DebugTable(self.onlyEquip)
end
function GameFleetInfoBag:AddEquipmentItem()
  if not self:GetFlashObject() then
    return
  end
  local minItemCount = (self.MAX_LINE - 1) * self.LINE_COUNT + 1
  for id = 1, math.max(GameItemBag:GetItemMaxPos(GameFleetInfoBag.onlyEquip), minItemCount), self.LINE_COUNT do
    DebugOut("AddItemId: ", id, " max: ", math.max(GameItemBag:GetItemMaxPos(GameFleetInfoBag.onlyEquip), minItemCount))
    self:GetFlashObject():InvokeASCallback("_root", "AddItemId", id)
  end
end
function GameFleetInfoBag:InsertItemList(oldMax)
  if not self:GetFlashObject() then
    return
  end
  local minItemCount = (self.MAX_LINE - 1) * self.LINE_COUNT + 1
  if math.floor(GameItemBag:GetItemMaxPos(GameFleetInfoBag.onlyEquip) / self.LINE_COUNT) <= math.floor(minItemCount / self.LINE_COUNT) then
    self:refreshBag()
  else
    self:refreshBag()
    for id = math.ceil(oldMax / self.LINE_COUNT) * self.LINE_COUNT + 1, GameItemBag:GetItemMaxPos(GameFleetInfoBag.onlyEquip), 4 do
      DebugOut("insert id: ", id, " oldMax: ", oldMax)
      self:GetFlashObject():InvokeASCallback("_root", "insertListItem", id)
    end
  end
end
function GameFleetInfoBag:onBeginDragItem(dragitem)
  self.dragItemInstance = dragitem
  local itemId = self.dragItemInstance.DraggedItemID
  local inlist, index = false, -1
  inlist, index = self:IsEquipmentInList(itemId)
  self:GetFlashObject():InvokeASCallback("_root", "onDragItem", index)
end
function GameFleetInfoBag:onEndDragItem()
  self.dragItem = false
  self.dragItemInstance = nil
  self:GetFlashObject():InvokeASCallback("_root", "onEndDragItem")
end
function GameFleetInfoBag:IsEquipmentInList(itemId)
  for k, v in pairs(GameFleetInfoBag.onlyEquip) do
    if v.item_id == itemId then
      return true, k
    end
  end
  return false
end
