local GameObjectAwardCenter = LuaObjectManager:GetLuaObject("GameObjectAwardCenter")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local totalAwardInfos
local waitForReceiveItemIndex = 0
local MAX_SLOT_COUNT = 6
local claimText = GameLoader:GetGameText("LC_MENU_MY_REWARDS_COLLECT_BUTTON")
local aleadyText = GameLoader:GetGameText("LC_MENU_CLAIMED_BUTTON")
function GameObjectAwardCenter:OnInitGame()
end
if AutoUpdate.isAndroidDevice then
  function GameObjectAwardCenter.OnAndroidBack()
    GameObjectAwardCenter:OnFSCommand("closeRewards")
  end
end
function GameObjectAwardCenter:OnAddToGameState()
  DebugOut("GameObjectAwardCenter OnAddToGameState")
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  claimText = GameLoader:GetGameText("LC_MENU_MY_REWARDS_COLLECT_BUTTON")
  aleadyText = GameLoader:GetGameText("LC_MENU_CLAIMED_BUTTON")
  self:GetTotalReward()
  self:GetFlashObject():InvokeASCallback("_root", "Move_In")
end
function GameObjectAwardCenter:OnEraseFromGameState()
  DebugOut("GameObjectAwardCenter:OnEraseFromGameState()")
  self:UnloadFlashObject()
  totalAwardInfos = nil
  waitForReceiveItemIndex = 0
end
function GameObjectAwardCenter:Update(dt)
  local flash_obj = self:GetFlashObject()
  if flash_obj == nil then
    return
  end
  self:GetFlashObject():InvokeASCallback("_root", "OnUpdateList")
  self:GetFlashObject():Update(dt)
end
function GameObjectAwardCenter:GetTotalReward()
  DebugOut("GameObjectAwardCenter:GetTotalReward")
  NetMessageMgr:SendMsg(NetAPIList.award_list_req.Code, nil, GameObjectAwardCenter.GetTotalRewardCallBack, true, nil)
end
function GameObjectAwardCenter.GetTotalRewardCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.award_list_req.Code then
    assert(content.code ~= 0)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.award_list_ack.Code then
    DebugOut("get Rewards " .. msgType)
    DebugTable(content)
    if next(content.award_infos) ~= nil then
      totalAwardInfos = content.award_infos
    else
      DebugOut("no more rewards")
      if totalAwardInfos ~= nil then
        return true
      end
    end
    if totalAwardInfos == nil then
      totalAwardInfos = {}
    end
    if next(totalAwardInfos) ~= nil then
      table.sort(totalAwardInfos, function(v1, v2)
        if v1.time == v2.time then
          return v1.id > v2.id
        else
          return v1.time > v2.time
        end
      end)
    end
    DebugOut("sorted totalAwardInfos")
    DebugTable(totalAwardInfos)
    GameObjectAwardCenter:GetFlashObject():InvokeASCallback("_root", "lua2fs_initRewardList")
    GameObjectAwardCenter:GetFlashObject():InvokeASCallback("_root", "lua2fs_setRewardListItemCount", #totalAwardInfos)
    GameObjectAwardCenter:GetFlashObject():InvokeASCallback("_root", "lua2fs_disableCollectAllBtn", false)
    GameUIBarLeft:updateRewardItemCount(#totalAwardInfos)
    return true
  end
  return false
end
function GameObjectAwardCenter:RequestReceivedReward(index)
  DebugOut("GameObjectAwardCenter:RequestReceivedReward", index)
  local awardInfo = totalAwardInfos[index]
  local requestParam = {
    id = awardInfo.id
  }
  DebugOut("id", awardInfo.id)
  NetMessageMgr:SendMsg(NetAPIList.award_get_req.Code, requestParam, GameObjectAwardCenter.RequestReceivedRewardCallBack, true, nil)
  waitForReceiveItemIndex = index
end
function GameObjectAwardCenter.RequestReceivedRewardCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.award_get_req.Code then
    if content.code ~= 0 then
      DebugOut(" RequestReceivedRewardCallBack not success", content.code)
      GameTip:Show(GameLoader:GetGameText("LC_MENU_MY_REWARDS_BAG_FULL"))
    else
      DebugOut("receive success")
      local awardInfo = totalAwardInfos[waitForReceiveItemIndex]
      if not awardInfo.isReceived then
        awardInfo.isReceived = true
      end
      GameObjectAwardCenter:GetFlashObject():InvokeASCallback("_root", "updateBtnReceivedStatus", waitForReceiveItemIndex, 2, claimText, aleadyText)
      local notClaimedItem = 0
      for k, v in pairs(totalAwardInfos) do
        if not v.isReceived then
          notClaimedItem = notClaimedItem + 1
        end
      end
      GameUIBarLeft:updateRewardItemCount(notClaimedItem)
      GameTip:Show(GameLoader:GetGameText("LC_MENU_MY_REWARDS_ALL_COLLECTED"))
      if notClaimedItem == 0 then
        GameObjectAwardCenter:GetTotalReward()
      end
    end
    return true
  end
  return false
end
function GameObjectAwardCenter.RequestReceivedAllCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.award_get_all_req.Code then
    if content.code ~= 0 then
      DebugOut(" RequestReceivedAllCallBack not success", content.code)
      GameObjectAwardCenter:GetTotalReward()
      GameTip:Show(GameLoader:GetGameText("LC_MENU_MY_REWARDS_BAG_FULL"))
    else
      DebugOut("receive all success")
      for k, v in pairs(totalAwardInfos) do
        if not v.isReceived then
          v.isReceived = true
          GameObjectAwardCenter:GetFlashObject():InvokeASCallback("_root", "updateBtnReceivedStatus", k, 2, claimText, aleadyText)
        end
      end
      GameUIBarLeft:updateRewardItemCount(0)
      GameTip:Show(GameLoader:GetGameText("LC_MENU_MY_REWARDS_ALL_COLLECTED"))
      GameObjectAwardCenter:GetTotalReward()
    end
    return true
  end
  return false
end
function GameObjectAwardCenter:OnFSCommand(cmd, arg)
  if cmd == "clickRewardListItemIcon" then
    local item_indexs = LuaUtils:string_split(arg, "\001")
    local itemId = tonumber(item_indexs[1])
    local indexId = tonumber(item_indexs[2])
    DebugOut("itemId", itemId, "indexId", indexId)
    local curItem = self:getRewardItem(itemId, indexId)
    self:showItemDetil(curItem, curItem.item_type)
  elseif cmd == "btnReceiveClicked" then
    local index = tonumber(arg)
    self:RequestReceivedReward(index)
  elseif cmd == "updateRewardItem" then
    local index = tonumber(arg)
    self:setRewardListItem(index)
  elseif cmd == "collectall" then
    local hasUnReceivedItem = false
    for k, v in pairs(totalAwardInfos) do
      if not v.isReceived then
        hasUnReceivedItem = true
      end
    end
    if hasUnReceivedItem then
      NetMessageMgr:SendMsg(NetAPIList.award_get_all_req.Code, nil, GameObjectAwardCenter.RequestReceivedAllCallBack, true, nil)
    else
      DebugOut("not hasUnReceivedItem")
    end
  elseif cmd == "closeRewards" then
    GameStateManager:GetCurrentGameState():EraseObject(GameObjectAwardCenter)
  end
end
function GameObjectAwardCenter:getRewardItem(itemId, indexId)
  DebugOut("itemId", itemId, "indexId", indexId)
  if next(totalAwardInfos) == nil then
    DebugOut("empty totalAwardInfos")
    return
  end
  local awardInfo = totalAwardInfos[itemId]
  if next(awardInfo) ~= nil then
    local curItem = awardInfo.awards[indexId]
    return curItem
  else
    DebugOut("empty awardInfo")
    return nil
  end
end
function GameObjectAwardCenter:setRewardListItem(itemId)
  DebugOut("setRewardListItem", itemId)
  if totalAwardInfos == nil then
    return
  end
  local awardInfo = totalAwardInfos[itemId]
  local itemType = ""
  local item_pic = ""
  local item_number = ""
  local cnt_number = ""
  local helpDesc = ""
  local nameDesc = ""
  if awardInfo.type ~= 100 then
    helpDesc = self:GetHelpDescByType(awardInfo.type)
    nameDesc = self:GetNameDescByType(awardInfo.type)
  else
    helpDesc = GameLoader:GetGameText("LC_ACHIEVEMENT_desc_" .. tostring(awardInfo.achievement_id))
    nameDesc = string.format(self:GetNameDescByType(awardInfo.type), GameLoader:GetGameText("LC_ACHIEVEMENT_title_" .. tostring(awardInfo.achievement_id)))
  end
  local timeString = os.date("%Y-%m-%d", awardInfo.time)
  local status = 1
  if awardInfo.isReceived then
    status = 2
  end
  for i = 1, MAX_SLOT_COUNT do
    local currentAward = awardInfo.awards[i]
    if currentAward then
      local extInfo = {}
      extInfo.itemId = itemId
      local info = GameHelper:GetItemInfo(currentAward)
      local count = GameHelper:GetAwardCount(currentAward.item_type, currentAward.number, currentAward.no)
      local icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(currentAward, extInfo, GameObjectAwardCenter.DynamicResDowloadFinished)
      count = GameUtils.numberConversion(count)
      itemType = itemType .. info.icon_frame .. "\001"
      item_number = item_number .. count .. "\001"
      item_pic = item_pic .. (info.icon_pic or "") .. "\001"
    else
      itemType = itemType .. "empty" .. "\001"
      item_number = item_number .. 0 .. "\001"
    end
  end
  isShowMore = false
  if #awardInfo.awards > MAX_SLOT_COUNT then
    isShowMore = true
  end
  DebugOut(#awardInfo.awards)
  DebugOut("lua2fs_setRewardListItem ", itemId, itemType, item_number, status, claimText, aleadyText, nameDesc, helpDesc, timeString, isShowMore)
  self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setRewardListItem", itemId, itemType, item_pic, item_number, status, claimText, aleadyText, nameDesc, helpDesc, timeString, isShowMore)
end
function GameObjectAwardCenter:GetHelpDescByType(type)
  local LC_String = "LC_MENU_MY_REWARDS_" .. type .. "_DESC"
  DebugOut("GetNameDescByType", LC_String)
  return ...
end
function GameObjectAwardCenter:GetNameDescByType(type)
  local LC_String = "LC_MENU_MY_REWARDS_" .. type .. "_TITLE"
  DebugOut("GetNameDescByType", LC_String)
  return ...
end
function GameObjectAwardCenter:showItemDetil(item_, item_type)
  if GameHelper:IsResource(item_type) then
    return
  end
  local item = item_
  if item_type == "krypton" then
    if item.level == 0 then
      item.level = 1
    end
    local detail = GameUIKrypton:TryQueryKryptonDetail(item.number, item.level, function(msgType, content)
      local ret = GameUIKrypton.QueryKryptonDetailCallBack(msgType, content)
      if ret then
        local tmp = GameGlobalData:GetKryptonDetail(item.number, item.level)
        if tmp == nil then
          return false
        end
        ItemBox:SetKryptonBox(tmp, item.number)
        local operationTable = {}
        operationTable.btnUnloadVisible = false
        operationTable.btnUpgradeVisible = false
        operationTable.btnUseVisible = false
        operationTable.btnDecomposeVisible = false
        ItemBox:ShowKryptonBox(320, 240, operationTable)
      end
      return ret
    end)
    if detail == nil then
    else
      ItemBox:SetKryptonBox(detail, item.number)
      local operationTable = {}
      operationTable.btnUnloadVisible = false
      operationTable.btnUpgradeVisible = false
      operationTable.btnUseVisible = false
      operationTable.btnDecomposeVisible = false
      ItemBox:ShowKryptonBox(320, 240, operationTable)
    end
  end
  ItemBox:ShowGameItem(item, hide_callback)
end
function GameObjectAwardCenter.DynamicResDowloadFinished(extInfo)
  DebugOut("DynamicResDowloadFinished", extInfo.itemId)
  if GameObjectAwardCenter:GetFlashObject() then
    GameObjectAwardCenter:setRewardListItem(extInfo.itemId)
  end
end
