local GameStateDebug = GameStateManager.GameStateDebug
local GameDebugBox = LuaObjectManager:GetLuaObject("GameDebugBox")
local m_currentFPSTime = 0
local m_frameCounter = 0
local m_fps = 0
local m_millSecond = -1
local m_maxDT = 0
local m_minDT = 1000
local m_infoTable = {isNeedSynFlash = false}
local m_isSwitchStart = false
local m_switchTimeStart = 0
local m_switchTimeEnd = 0
local enableShow = false
local enableCnt = 0
function GameStateDebug:InitGameState()
  self:AddObject(GameDebugBox)
  m_millSecond = ext.getSysTime()
end
function GameStateDebug:SwitchTimeStart()
  m_isSwitchStart = true
  m_switchTimeStart = ext.getSysTime()
end
function GameStateDebug:IsStics()
  return m_isSwitchStart
end
function GameStateDebug:SwitchTimeEnd()
  if m_isSwitchStart then
    m_switchTimeEnd = ext.getSysTime()
    m_isSwitchStart = false
  end
end
function GameStateDebug:ClearInfo(name, value)
  m_infoTable = {}
  m_infoTable.isNeedSynFlash = true
end
function GameStateDebug:AppendInfo(name, value)
  if not enableShow then
    return
  end
  m_infoTable[name] = value
  m_infoTable.isNeedSynFlash = true
  self:UpdateInfo()
end
function GameStateDebug:UpdateInfo()
  if m_infoTable.isNeedSynFlash then
    local tmpText = ""
    for k, v in pairs(m_infoTable) do
      if k ~= "isNeedSynFlash" then
        tmpText = tmpText .. k .. ": " .. v .. " "
      end
    end
    tmpText = LuaUtils:string_trim(tmpText)
    GameDebugBox:SetText(tmpText)
    m_infoTable.isNeedSynFlash = false
  end
end
function GameStateDebug:Update(dt)
  if enableShow then
    GameStateBase.Update(self, dt)
  end
end
function GameStateDebug:OnTouchReleased(x, y)
  if not enableShow then
    if x < 64 and y < 64 then
      enableCnt = enableCnt + 1
    else
      enableCnt = 0
    end
    if enableCnt >= 4 then
      enableShow = true
    end
  end
  GameStateBase.OnTouchReleased(self, x, y)
end
function GameStateDebug:GetFPS(dt)
  local currMillSecond = ext.getSysTime()
  dt = currMillSecond - m_millSecond
  m_millSecond = currMillSecond
  GameStateBase.Update(self, dt)
  m_frameCounter = m_frameCounter + 1
  m_currentFPSTime = m_currentFPSTime + dt
  if dt > m_maxDT then
    m_maxDT = dt
  end
  if dt < m_minDT then
    m_minDT = dt
  end
  if m_currentFPSTime > 1000 then
    m_fps = m_frameCounter
    m_currentFPSTime = m_currentFPSTime - 1000
    m_frameCounter = 0
    self:AppendInfo("F", m_fps)
    self:AppendInfo("A", math.floor(m_maxDT * 10) / 10)
    self:AppendInfo("I", math.floor(m_minDT * 10) / 10)
    if not m_isSwitchStart then
      self:AppendInfo("S", math.floor((m_switchTimeEnd - m_switchTimeStart) * 1000000) / 1000)
    end
    m_maxDT = 0
    m_minDT = 1000
  end
  self:UpdateInfo()
end
function GameStateDebug:Render()
  if not enableShow then
    return
  end
  GameStateBase.Render(self)
end
