local GameUIDice = LuaObjectManager:GetLuaObject("GameUIDice")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameStateDice = GameStateManager.GameStateDice
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
GameUIDice.activeid = nil
GameUIDice.AllDiceData = {}
GameUIDice.currentStep = 0
GameUIDice.currentCircle = 1
GameUIDice.currentShowPos = 20
GameUIDice.ThrowCost = nil
GameUIDice.RefreshCost = nil
GameUIDice.round = 20
function GameUIDice:OnAddToGameState()
  DebugTable("GameUIDice:")
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self:InitLayer()
  self:GetDataFromServer()
end
function GameUIDice:OnEraseFromGameState(...)
  self:UnloadFlashObject()
end
function GameUIDice:InitLayer()
  self:GetFlashObject():InvokeASCallback("_root", "Init", false)
  self:GetFlashObject():InvokeASCallback("_root", "MoveIn")
end
function GameUIDice:AllCellMovein()
  self:GetFlashObject():InvokeASCallback("_root", "Init", true)
  self:GetFlashObject():InvokeASCallback("_root", "AllCellMovein")
end
function GameUIDice:DiceAnimation()
  self:GetFlashObject():InvokeASCallback("_root", "DiceAnimation")
end
function GameUIDice:SetEndPos(id, circle)
  DebugOut("id = " .. id)
  DebugOut("circle = " .. circle)
  self:GetFlashObject():InvokeASCallback("_root", "SetEndId", id, circle)
end
function GameUIDice:SetActiveId(id)
  self.activeid = id
end
function GameUIDice:GetDataFromServer()
  local param = {
    id = self.activeid
  }
  NetMessageMgr:SendMsg(NetAPIList.enter_dice_req.Code, param, GameUIDice.GetDataFromServerCallback, false, false)
end
function GameUIDice:ResetDiceData(state)
  local param = {
    id = self.activeid,
    buy_type = state
  }
  NetMessageMgr:SendMsg(NetAPIList.reset_dice_req.Code, param, GameUIDice.ResetDiceDataCallback, false, false)
end
function GameUIDice.GetDataFromServerCallback(msgType, content)
  if msgType == NetAPIList.enter_dice_ack.Code then
    DebugOutPutTable(content.all_steps, "__content = ")
    GameUIDice.AllDiceData = content.cells
    GameUIDice:GetFlashObject():InvokeASCallback("_root", "SetDiceDataNum", #GameUIDice.AllDiceData)
    GameUIDice.ThrowCost = content.throw_cost
    GameUIDice.RefreshCost = content.refresh_cost
    GameUIDice.currentStep = content.current_cell
    GameUIDice:SetAllDiceData()
    GameUIDice:SetCellSelect()
    GameUIDice:SetBoxData()
    GameUIDice:AllCellMovein()
    GameUIDice:GetFlashObject():InvokeASCallback("_root", "HideBox")
    GameUIDice:SetGachaChestOpenInfo("")
    GameUIDice:SetTip()
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.enter_dice_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUIDice.ResetDiceDataCallback(msgType, content)
  if msgType == NetAPIList.reset_dice_ack.Code then
    DebugOutPutTable(content.all_steps, "content = ")
    GameUIDice.AllDiceData = content.cells
    GameUIDice:GetFlashObject():InvokeASCallback("_root", "SetDiceDataNum", #GameUIDice.AllDiceData)
    GameUIDice.ThrowCost = content.throw_cost
    GameUIDice.RefreshCost = content.refresh_cost
    GameUIDice.currentStep = content.current_cell
    GameUIDice:SetAllDiceData()
    GameUIDice:SetCellSelect()
    GameUIDice:SetBoxData()
    GameUIDice:AllCellMovein()
    GameUIDice:GetFlashObject():InvokeASCallback("_root", "HideBox")
    GameUIDice:SetTip()
    return true
  elseif msgType == NetAPIList.credit_exchange_ack.Code and content.api == NetAPIList.reset_dice_req.Code then
    DebugOut("credit_exchange_ack:--stores_buy_req: = ")
    DebugTable(content)
    local itemName = GameHelper:GetAwardNameText(content.item.item_type, content.item.number)
    local str = string.gsub(GameLoader:GetGameText("LC_MENU_ITEM_INSUFFICIENT_INFO"), "<name1>", itemName)
    local infoText = string.gsub(str, "<name2>", content.credit)
    GameUtils:CreditCostConfirm(infoText, function()
      GameUIDice:ResetDiceData(1)
    end, false, nil)
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.reset_dice_req.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    end
    return true
  end
  return false
end
function GameUIDice:test(content)
  DebugOutPutTable(content.all_steps, "content = ")
  GameUIDice.AllDiceData = content.all_steps
  GameUIDice.ThrowCost = content.throw_cost
  GameUIDice.RefreshCost = content.refresh_cost
  GameUIDice.currentStep = content.current_step
  GameUIDice:SetAllDiceData()
  GameUIDice:AllCellMovein()
end
function GameUIDice:SetBoxData()
  local count = #self.AllDiceData / self.round
  DebugOut("count = " .. count)
  local a = 1
  local b = count
  for i = a, b do
    local boxid = i * self.round
    local boxItem = self.AllDiceData[boxid].status
    DebugOut(boxItem)
    self:GetFlashObject():InvokeASCallback("_root", "SetBoxId", i + 4 - count, boxid, boxItem)
  end
end
function GameUIDice:SetDiceValue(count)
  self:GetFlashObject():InvokeASCallback("_root", "SetDiceValue", count)
end
function GameUIDice:SetAllDiceData()
  local _showCellData = self:GetShowCellData()
  local _currentShowPos = self:GetCurrentPos()
  local showTable = {}
  showTable._itemIcon = ""
  showTable._itemCount = ""
  showTable._itemState = ""
  showTable._type = ""
  showTable.throwIcon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(GameUIDice.ThrowCost, nil, GameUIDice.DownloadImageOverCallback)
  showTable.throwCount = GameHelper:GetAwardCount(GameUIDice.ThrowCost.item_type, GameUIDice.ThrowCost.number, GameUIDice.ThrowCost.no)
  showTable.refreshIcon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(GameUIDice.RefreshCost, nil, GameUIDice.DownloadImageOverCallback)
  showTable.refreshCount = "x" .. GameHelper:GetAwardCount(GameUIDice.RefreshCost.item_type, GameUIDice.RefreshCost.number, GameUIDice.RefreshCost.no)
  local refresh_Count = GameHelper:GetAwardCount(GameUIDice.RefreshCost.item_type, GameUIDice.RefreshCost.number, GameUIDice.RefreshCost.no)
  if tonumber(refresh_Count) == 0 then
    showTable.refreshCount = GameLoader:GetGameText("LC_MENU_CAPTION_FREE")
  end
  for i, v in ipairs(_showCellData) do
    if v.show_item then
      showTable._itemIcon = showTable._itemIcon .. GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v.show_item, nil, GameUIDice.DownloadImageOverCallback) .. "\001"
      showTable._itemCount = showTable._itemCount .. GameHelper:GetAwardCount(v.show_item.item_type, v.show_item.number, v.show_item.no) .. "\001"
    else
      showTable._itemIcon = showTable._itemIcon .. "empty" .. "\001"
      showTable._itemCount = showTable._itemCount .. "empty" .. "\001"
    end
    showTable._itemState = showTable._itemState .. v.status .. "\001"
    showTable._type = showTable._type .. v.type .. "\001"
  end
  self:GetFlashObject():InvokeASCallback("_root", "SetAllDiceData", showTable, _currentShowPos)
  GameUIDice:SetResourceData()
end
function GameUIDice:SetResourceData()
  local ThrowIcon = ""
  local RefreshIcon = ""
  local ThrowCount = 0
  local RefreshCount = 0
  local count = GameGlobalData:GetData("resource")
  if GameHelper:IsResource(self.ThrowCost.item_type) then
    ThrowCount = GameUtils.numberConversion(count[self.ThrowCost.item_type])
    DebugOut("itemCount:" .. ThrowCount)
  else
    ThrowCount = GameGlobalData:GetItemCount(self.ThrowCost.number)
    DebugOut("itemCount:xx" .. ThrowCount)
  end
  local creditIcon = GameHelper:GetAwardTypeIconFrameName("credit", "", "")
  local creditCount = GameUtils.numberConversion(count.credit)
  ThrowIcon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(GameUIDice.ThrowCost, nil, GameUIDice.DownloadImageOverCallback)
  self:GetFlashObject():InvokeASCallback("_root", "SetResourceData", ThrowIcon, creditIcon, ThrowCount, creditCount)
end
function GameUIDice:SetCellSelect()
  local _currentShowPos = self:GetCurrentPos()
  local currentPos = self.currentStep
  DebugOut("currentShowPos = " .. _currentShowPos)
  DebugOut("currentPos = " .. self.currentStep)
  self:GetFlashObject():InvokeASCallback("_root", "SetCellSelect", _currentShowPos)
  self:GetFlashObject():InvokeASCallback("_root", "SetInitPos", currentPos)
end
function GameUIDice:SetRefreshButtonState()
  local currentPos = self.currentStep
  self:GetFlashObject():InvokeASCallback("_root", "UpdateRefreshBut", currentPos)
end
function GameUIDice.DownloadImageOverCallback()
  if GameUIDice:GetFlashObject() then
    GameUIDice:SetAllDiceData()
  end
end
function GameUIDice:GetCurrentCircle()
  self.currentCircle = math.floor(self.currentStep / 20) + 1
  local boundary = #self.AllDiceData / 20
  if boundary < self.currentCircle then
    self.currentCircle = boundary
  end
  return self.currentCircle
end
function GameUIDice:GetCurrentPos()
  if self.currentStep == 0 then
    self.currentShowPos = 20
  else
    self.currentShowPos = self.currentStep - math.ceil(self.currentStep / 20 - 1) * 20
  end
  return self.currentShowPos
end
function GameUIDice:GetShowCellData()
  local cirleNumbe = self:GetCurrentCircle()
  local showCellData = {}
  for i, v in ipairs(self.AllDiceData) do
    if v.cell > (cirleNumbe - 1) * 20 and v.cell <= cirleNumbe * 20 then
      table.insert(showCellData, v)
    end
  end
  table.sort(showCellData, function(v1, v2)
    return v1.cell < v2.cell
  end)
  return showCellData
end
function GameUIDice:GetCurrentCellItem()
  for _, v in ipairs(self.AllDiceData) do
    if v.cell == self.currentStep then
      return v
    end
  end
  return nil
end
function GameUIDice:UpdateAllCellData(data)
  for _, v in ipairs(data) do
    for _, m in ipairs(self.AllDiceData) do
      if v.cell == m.cell then
        m.status = v.status
        m.type = v.type
        m.show_item = v.show_item
        m.loots = v.loots
      end
    end
  end
  DebugOut("alldicedata = ")
  DebugTable(self.AllDiceData)
end
function GameUIDice:UpdateAllCellDataByCell(item)
  for _, m in ipairs(self.AllDiceData) do
    if item.cell == m.cell then
      m = item
    end
  end
end
function GameUIDice:ThrowDice(state)
  local param = {
    id = self.activeid,
    buy_type = state
  }
  NetMessageMgr:SendMsg(NetAPIList.throw_dice_req.Code, param, function(msgType, content)
    if msgType == NetAPIList.throw_dice_ack.Code then
      GameUIDice:UpdateAllCellData(content.update_cells)
      GameUIDice.currentStep = content.current_cell
      GameUIDice.ThrowCost = content.throw_cost
      GameUIDice.RefreshCost = content.refresh_cost
      GameUIDice:SetEndPos(content.current_cell, GameUIDice:GetCurrentCircle())
      GameUIDice:SetDiceValue(content.dice_value)
      GameUIDice:DiceAnimation()
      GameUIDice:SetRefreshButtonState()
      GameUIDice:SetAllDiceData()
      return true
    elseif msgType == NetAPIList.credit_exchange_ack.Code and content.api == NetAPIList.throw_dice_req.Code then
      DebugOut("credit_exchange_ack:--stores_buy_req: = ")
      DebugTable(content)
      local itemName = GameHelper:GetAwardNameText(content.item.item_type, content.item.number)
      local str = string.gsub(GameLoader:GetGameText("LC_MENU_ITEM_INSUFFICIENT_INFO"), "<name1>", itemName)
      local infoText = string.gsub(str, "<name2>", content.credit)
      GameUtils:CreditCostConfirm(infoText, function()
        GameUIDice:ThrowDice(1)
      end, false, nil)
      return true
    elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.throw_dice_req.Code then
      if content.code ~= 0 then
        local GameVip = LuaObjectManager:GetLuaObject("GameVip")
        GameVip:CheckIsNeedShowVip(content.code)
      end
      return true
    end
    return false
  end, true, nil)
end
function GameUIDice:showItemDetil(item_, item_type)
  if GameHelper:IsResource(item_type) then
    return
  end
  local item = item_
  if item_type == "krypton" then
    if item.level == 0 then
      item.level = 1
    end
    local detail = GameUIKrypton:TryQueryKryptonDetail(item.number, item.level, function(msgType, content)
      local ret = GameUIKrypton.QueryKryptonDetailCallBack(msgType, content)
      if ret then
        local tmp = GameGlobalData:GetKryptonDetail(item.number, item.level)
        if tmp == nil then
          return false
        end
        ItemBox:SetKryptonBox(tmp, item.number)
        local operationTable = {}
        operationTable.btnUnloadVisible = false
        operationTable.btnUpgradeVisible = false
        operationTable.btnUseVisible = false
        operationTable.btnDecomposeVisible = false
        ItemBox:ShowKryptonBox(320, 240, operationTable)
      end
      return ret
    end)
    if detail == nil then
    else
      ItemBox:SetKryptonBox(detail, item.number)
      local operationTable = {}
      operationTable.btnUnloadVisible = false
      operationTable.btnUpgradeVisible = false
      operationTable.btnUseVisible = false
      operationTable.btnDecomposeVisible = false
      ItemBox:ShowKryptonBox(320, 240, operationTable)
    end
  end
  if item_type == "fleet" then
    ItemBox:ShowCommanderDetail2(tonumber(item.number))
    return
  end
  ItemBox:ShowGameItem(item)
end
function GameUIDice:GetBoxAward(id)
  local param = {
    cell = id,
    id = self.activeid
  }
  NetMessageMgr:SendMsg(NetAPIList.dice_awards_req.Code, param, GameUIDice.GetBoxAwardCallback, true, nil)
end
function GameUIDice.GetBoxAwardCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.dice_awards_req.Code then
    local item = GameUIDice.AllDiceData[GameUIDice.currentBoxid]
    item.status = 2
    GameUIDice:UpdateAllCellDataByCell(item)
    GameUIDice:SetBoxData()
    return true
  end
  return false
end
function GameUIDice:Update(dt)
  if not GameUIDice:GetFlashObject() then
    return
  end
  if GameUIDice:GetFlashObject() then
    GameUIDice:GetFlashObject():Update(dt)
    if GameUIDice:GetFlashObject() then
      GameUIDice:GetFlashObject():InvokeASCallback("_root", "UpdateTime", dt)
    end
  end
end
function GameUIDice:CreateGachaChestInfo(ChestInfos)
  self.GachaChestInfoStr = ""
  infoNum = math.min(#ChestInfos.boards, 5)
  for i = 1, infoNum do
    local tName = GameUtils:GetUserDisplayName(ChestInfos.boards[i].user)
    local tChestName = GameHelper:GetAwardTypeText(ChestInfos.boards[i].award.item_type, ChestInfos.boards[i].award.number)
    local tChestNum = GameHelper:GetAwardCount(ChestInfos.boards[i].award.item_type, ChestInfos.boards[i].award.number, ChestInfos.boards[i].award.no)
    local str = GameLoader:GetGameText("LC_MENU_COMBO_GACHA_BROADCAST_TEXT")
    str = string.gsub(str, "<item_name>", tChestName)
    str = string.gsub(str, "<point>", "" .. tChestNum)
    str = string.format(str, tName)
    self.GachaChestInfoStr = self.GachaChestInfoStr .. str .. "    "
  end
  DebugOut("GameUIcomboGacha:CreateGachaChestInfo", self.GachaChestInfoStr)
end
function GameUIDice.RecentChestNtfHandler(content)
  DebugTable(content)
  GameUIDice:CreateGachaChestInfo(content)
  GameUIDice:SetGachaChestOpenInfo(GameUIDice.GachaChestInfoStr)
end
function GameUIDice:SetGachaChestOpenInfo(str)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "setRollTextInfo", str)
  end
end
function GameUIDice:SetTip()
  if self:GetFlashObject() then
    local item = self.AllDiceData[#self.AllDiceData].show_item
    DebugOut("item == ")
    DebugTable(item)
    local str = string.gsub(GameLoader:GetGameText("LC_MENU_GALAXY_CASINO_BIGAWARD_TIP"), "<numer1>", GameHelper:GetAwardText(item.item_type, item.number, item.no))
    local count = #self.AllDiceData / 20 + 1 - GameUIDice:GetCurrentCircle()
    local _str = string.gsub(str, "<number2>", count)
    DebugOut(str)
    self:GetFlashObject():InvokeASCallback("_root", "setTip", _str, self.currentStep)
  end
end
function GameUIDice:ShowDiceBoxAwardInfo(awards, _x, _y, width, hight)
  self.diceAwards = awards
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "initDiceBoxAward", #awards)
    self:GetFlashObject():InvokeASCallback("_root", "dicePanelMovein")
    DebugOut("_x:" .. _x .. " " .. _y)
    self:GetFlashObject():InvokeASCallback("_root", "SetDicePos", _x + 50, _y)
  end
end
function GameUIDice:UpdateDiceCellItem(index)
  local item = self.diceAwards[index]
  local iconFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(item, nil, nil)
  local count = GameHelper:GetAwardCount(item.item_type, item.number, item.no)
  local name = GameHelper:GetAwardNameText(item.item_type, item.number)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "UpdateDiceItem", index, iconFrame, count, name)
  end
end
function GameUIDice:OnFSCommand(cmd, arg)
  if cmd == "throw_dice" then
    if self.ThrowCost.item_type == "credit" then
      local textInfo = string.gsub(GameLoader:GetGameText("LC_MENU_MONOPOLY_CREDIT_THROW_INFO"), "<number>", GameHelper:GetItemText(self.ThrowCost.item_type, self.ThrowCost.number, self.ThrowCost.no))
      DebugOut("textInfo = " .. textInfo)
      local function callBack()
        GameUIDice:ThrowDice(0)
      end
      GameUtils:CreditCostConfirm(textInfo, callBack, false, nil)
    else
      GameUIDice:ThrowDice(0)
    end
  elseif cmd == "move_cell_over" then
    local currentItem = GameUIDice:GetCurrentCellItem().loots
    DebugTable(currentItem)
    local msgTipTable = {}
    table.insert(msgTipTable, GameUIDice:GetCurrentCellItem().show_item)
    for _, p in ipairs(currentItem) do
      if not LuaUtils:table_equal(GameUIDice:GetCurrentCellItem().show_item, p) then
        table.insert(msgTipTable, p)
      end
    end
    if #msgTipTable > 0 then
      if self.currentStep % 20 ~= 0 then
        ItemBox:ShowRewardBox(msgTipTable)
      end
    else
      GameUIDice:SetAllDiceData()
    end
  elseif cmd == "next_circle" then
    GameUIDice:SetAllDiceData()
    GameUIDice:SetBoxData()
    GameUIDice:SetTip()
  elseif cmd == "game_over" then
    GameUIDice:SetAllDiceData()
    GameUIDice:SetBoxData()
  elseif cmd == "refresh_dice" then
    if self.RefreshCost.item_type == "credit" then
      local textInfo = string.gsub(GameLoader:GetGameText("LC_MENU_PLANETCRAFT_COST_TIP_REFRESH"), "<credits_num>", GameHelper:GetItemText(self.RefreshCost.item_type, self.RefreshCost.number, self.RefreshCost.no))
      DebugOut("textInfo = " .. textInfo)
      local function callBack()
        GameUIDice:ResetDiceData(0)
      end
      GameUtils:CreditCostConfirm(textInfo, callBack, false, nil)
    else
      GameUIDice:ResetDiceData(0)
    end
  elseif cmd == "click_cell" then
    local ShowCellData = GameUIDice:GetShowCellData()
    local cell = ShowCellData[tonumber(arg)]
    if cell.show_item then
      GameUIDice:showItemDetil(cell.show_item, cell.show_item.item_type)
    end
  elseif cmd == "clickbox" then
    local param = LuaUtils:string_split(arg, "\001")
    self.currentBoxid = tonumber(param[1])
    local boxItem = self.AllDiceData[tonumber(param[1])]
    DebugTable(boxItem)
    if boxItem.status == 1 then
      GameUIDice:GetBoxAward(boxItem.cell)
    else
      GameUIDice:ShowDiceBoxAwardInfo(boxItem.loots, param[2], param[3])
    end
  elseif cmd == "close_menu" then
    GameStateManager:SetCurrentGameState(GameStateDice:GetPreState())
  elseif cmd == "click_item" then
    local item = self.diceAwards[tonumber(arg)]
    GameUIDice:showItemDetil(item, item.item_type)
  elseif cmd == "updateItemdata" then
    GameUIDice:UpdateDiceCellItem(tonumber(arg))
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUIDice.OnAndroidBack()
    if GameUIDice:GetFlashObject() then
      GameUIDice:GetFlashObject():InvokeASCallback("_root", "MoveOut")
    end
  end
end
