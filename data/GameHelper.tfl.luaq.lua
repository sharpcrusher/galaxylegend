local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIChat = LuaObjectManager:GetLuaObject("GameUIChat")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
local GameUICollect = LuaObjectManager:GetLuaObject("GameUICollect")
local GameUIAffairHall = LuaObjectManager:GetLuaObject("GameUIAffairHall")
local GameUITechnology = LuaObjectManager:GetLuaObject("GameUITechnology")
local GameFleetEnhance = LuaObjectManager:GetLuaObject("GameFleetEnhance")
local GameUIPrestigeRankUp = LuaObjectManager:GetLuaObject("GameUIPrestigeRankUp")
local GameStateConfrontation = GameStateManager.GameStateConfrontation
local GameStateKrypton = GameStateManager.GameStateKrypton
local GameStateBattleMap = GameStateManager.GameStateBattleMap
local GameStateEquipEnhance = GameStateManager.GameStateEquipEnhance
local GameUIUserAlliance = LuaObjectManager:GetLuaObject("GameUIUserAlliance")
local GameUIActivityNew = LuaObjectManager:GetLuaObject("GameUIActivityNew")
local QuestTutorialStarCharge = TutorialQuestManager.QuestTutorialStarCharge
local GameStateLab = GameStateManager.GameStateLab
local GameStateStore = GameStateManager.GameStateStore
local GameStateRecruit = GameStateManager.GameStateRecruit
local GameStateDaily = GameStateManager.GameStateDaily
local GameObjectMainPlanet = LuaObjectManager:GetLuaObject("GameObjectMainPlanet")
local GameUIArena = LuaObjectManager:GetLuaObject("GameUIArena")
local showDaily = false
local GameUIHalfPortrait = LuaObjectManager:GetLuaObject("GameUIHalfPortrait")
local HalfPortraitResource = require("data1/Half_Portrait_Resource.tfl")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameItemBag = LuaObjectManager:GetLuaObject("GameItemBag")
local QuestTutorialEquip = TutorialQuestManager.QuestTutorialEquip
local QuestTutorialPrestige = TutorialQuestManager.QuestTutorialPrestige
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameStateScratchGacha = GameStateManager.GameStateScratchGacha
local GameUILab = LuaObjectManager:GetLuaObject("GameUILab")
local GameVip = LuaObjectManager:GetLuaObject("GameVip")
require("FleetMatrix.tfl")
GameHelper.currentTab = -1
GameHelper.TabName = {
  ["affairs"] = 1,
  ["fleets"] = 2,
  ["exp"] = 3,
  ["alliance"] = 4,
  ["function"] = 5,
  ["medal"] = 6
}
GameHelper.EBattleResultType = {
  TYPE_BATTLE = 1,
  TYPE_CHALLENGE = 2,
  TYPE_COLONIAL = 3
}
GameHelper.BattleResultType = {
  [1] = {
    [2] = "battle_lose",
    [1] = "battle_win"
  },
  [2] = {
    [2] = "challenge_lose",
    [1] = "challenge_win"
  },
  [3] = {
    [2] = "colonial_lose",
    [1] = "colonial_win"
  }
}
GameHelper.executer = {
  action = nil,
  category = 0,
  subCategory = 0
}
GameHelper.localRewardData = {}
GameHelper.currenrRewardKey = nil
GameHelper.titleQuestId = -1
GameHelper.currentDay = nil
GameHelper.HalfPortraitTable = {}
GameHelper.HalfPortraitHeroResourceTable = {}
GameHelper.EnterPage = 0
GameHelper.PageGCS = 0
GameHelper.PageQuest = 1
GameHelper.PageRili = 2
GameHelper.PageAchievement = 3
GameHelper.PagePrestige = 4
GameHelper.CurSelTopTab = -1
GameHelper.CurSelGCSTab = -1
GameHelper.TopTabState = nil
function GameHelper:GetDayState(day)
  if day.max_task_count == 0 then
    day.state = 1
    return
  end
  if day.current_task_count == day.max_task_count then
    day.state = 2
    return
  end
  if 0 <= day.today then
    day.state = 1
    return
  end
  if day.can_redo == false then
    day.state = 4
    return
  end
  day.state = 3
end
function GameHelper:isTodayLived()
  local levelInfo = GameGlobalData:GetData("levelinfo")
  if levelInfo.level < 22 then
    return false
  end
  if not showDaily then
    return false
  end
  if GameHelper.todayList.task.is_finished and not GameHelper.todayList.task.is_get_reward then
    self:GetFlashObject():InvokeASCallback("_root", "SetTodayCount", 1)
    return true
  end
  local count = 0
  for k, v in pairs(GameHelper:GetTodaySubQuestList()) do
    if not v.is_get_reward and v.is_finished then
      count = count + 1
    end
  end
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "SetTodayCount", count)
  end
  return count > 0
end
function GameHelper:OnInitGame()
end
function GameHelper:InitTopTabState()
  local topTabState = {}
  topTabState[1] = {open = true}
  topTabState[2] = {open = true}
  topTabState[3] = {open = true}
  topTabState[4] = {open = true}
  topTabState[5] = {open = true}
  local levelInfo = GameGlobalData:GetData("levelinfo")
  if levelInfo.level < 22 then
    topTabState[1].open = false
    topTabState[2].open = false
    topTabState[3].open = false
  elseif levelInfo.level >= 22 and levelInfo.level < 30 then
    topTabState[1].open = false
  end
  if GameHelper.GCSOpen == false then
    topTabState[1].open = false
  end
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "setTopTabState", topTabState)
  end
  if GameHelper.EnterPage == 0 and topTabState[1].open == false then
    GameHelper.EnterPage = 1
  end
  if GameHelper.EnterPage == 1 and topTabState[2].open == false then
    GameHelper.EnterPage = 2
  end
  if GameHelper.EnterPage == 2 and topTabState[3].open == false then
    GameHelper.EnterPage = 3
  end
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "setDailyTab", GameHelper.EnterPage)
  end
  GameHelper.TopTabState = topTabState
end
function GameHelper:Init()
  self.executer.action = nil
  self:LoadFlashObject()
  local flash_obj = self:GetFlashObject()
  local levelInfo = GameGlobalData:GetData("levelinfo")
  if flash_obj then
    DebugOut(" GameHelper:Init   initFlashObject")
    flash_obj:InvokeASCallback("_root", "initFlashObject")
    flash_obj:InvokeASCallback("_root", "setDisplayText", GameLoader:GetGameText("LC_MENU_NEW_DAILY_UNFINISH_CHAR"), GameLoader:GetGameText("LC_MENU_NEW_DAILY_FINISH_CHAR"), GameLoader:GetGameText("LC_MENU_NEW_DAILY_CLOSED_CHAR"), GameLoader:GetGameText("LC_MENU_EXECUTE_CHAR"), GameLoader:GetGameText("LC_MENU_NEW_DAILY_REMEDY_BUTTON"), GameLoader:GetGameText("LC_MENU_NEW_DAILY_CAN_REMEDY_CHAR"), GameLoader:GetGameText("LC_MENU_NEW_DAILY_CLOSED_CHAR"), GameLoader:GetGameText("LC_MENU_FACEBOOK_REWARDS_CREDIT"), GameLoader:GetGameText("LC_MENU_FACEBOOK_REWARDS_HISTORY"), GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_REACHED"))
  end
  GameGlobalData:RegisterDataChangeCallback("levelinfo", GameHelper.RefreshAll)
  GameGlobalData:RegisterDataChangeCallback("task_statistic", GameHelper.RefreshAll)
  GameHelper.GCSTaskData = nil
  GameHelper.GCSAwardsData = nil
  GameHelper.questList = nil
  GameHelper.todayList = nil
  GameHelper.daily = nil
  GameHelper.dailyAward = nil
  GameHelper.achievement = nil
  GameHelper.PrestigeData = nil
  self:RequestGCS(function()
    GameVip:RequestProductIdentifierList()
    GameVip:ReqGiftData()
  end)
  self:RequestQuest()
  self:RequestRili()
  self:RequestAchievement()
  self:RequestPrestige(function()
    GameHelper:InitTopTabState()
    GameHelper:RefreshGCSRedPoint()
    GameHelper.RefreshAll()
  end)
end
function GameHelper:OnAddToGameState()
  if GameUtils:GetTutorialHelp() then
    local GameUIMaskLayer = LuaObjectManager:GetLuaObject("GameUIMaskLayer")
    GameUtils:MaskLayerMidTutorial(GameLoader:GetGameText("LC_MENU_ASSISTANT_INFO_21"))
  end
  self:Init()
  GameTimer:Add(GameHelper._OnGCSTimerTick, 1000)
end
function GameHelper._OnGCSTimerTick()
  if GameHelper.GCSAwardsData ~= nil then
    local data = GameHelper.GCSAwardsData
    local left = data.season_left_time - (os.time() - data.left_time_base)
    if left >= 0 then
      local leftTimeStr = GameUtils:formatTimeStringAsPartion2(left)
      GameHelper.GCSAwardsData.text_seasonLeft = GameLoader:GetGameText("LC_MENU_GCS_SeasonEndIn_CHAR") .. leftTimeStr
      if GameHelper:GetFlashObject() then
        GameHelper:GetFlashObject():InvokeASCallback("_root", "UpdateGCSSeasonLeftTime", GameHelper.GCSAwardsData.text_seasonLeft)
      end
    end
    if GameStateManager:GetCurrentGameState():IsObjectInState(GameHelper) then
      return 1000
    end
    return nil
  end
end
function GameHelper:SetEnterPage(index)
  self.EnterPage = index
end
function GameHelper:RequestGCS(callBack)
  local function msgCallBack(msgType, content)
    if msgType == NetAPIList.gcs_task_data_ack.Code then
      GameHelper:GenerateGCSTaskData(content)
      return true
    elseif msgType == NetAPIList.gcs_awards_data_ack.Code then
      GameHelper.GCSOpen = true
      GameHelper:GenerateGCSAwardsData(content)
      if callBack ~= nil then
        callBack()
      end
      return true
    elseif msgType == NetAPIList.common_ack.Code and (content.api == NetAPIList.gcs_task_data_req.Code or content.api == NetAPIList.gcs_awards_data_req.Code) then
      if content.code == 50207 then
        GameHelper.GCSOpen = false
      elseif content.code ~= 0 then
        GameHelper.GCSOpen = false
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    end
    return false
  end
  DebugOut("GameHelper:RequestGCS --")
  NetMessageMgr:SendMsg(NetAPIList.gcs_task_data_req.Code, nil, msgCallBack, true, nil)
  NetMessageMgr:SendMsg(NetAPIList.gcs_awards_data_req.Code, nil, msgCallBack, true, nil)
end
function GameHelper:ShowGCS()
  GameHelper:ShowGCS_Task()
end
function GameHelper:RequestQuest(callBack)
  local function msgCallBack(msgType, content)
    if msgType == NetAPIList.task_list_ack.Code then
      GameHelper:GenerateQuestData(content)
      return true
    elseif msgType == NetAPIList.today_act_task_ack.Code then
      DebugOutPutTable(content, "today quest")
      GameHelper:GenerateTodayData(content)
      if callBack ~= nil then
        callBack()
      end
      return true
    elseif msgType == NetAPIList.common_ack.Code and (content.api == NetAPIList.task_list_req.Code or content.api == NetAPIList.today_act_task_req.Code) then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    end
    return false
  end
  DebugOut("GameHelper:RequestQuest -- ")
  NetMessageMgr:SendMsg(NetAPIList.task_list_req.Code, nil, msgCallBack, true, nil)
  NetMessageMgr:SendMsg(NetAPIList.today_act_task_req.Code, nil, msgCallBack, true, nil)
end
function GameHelper:GenerateQuestData(content)
  GameHelper.questList = {}
  DebugOut("GameHelper:GenerateQuestData -- ")
  local temp = content.task_list
  for k, v in pairs(temp) do
    GameHelper.questList[v.task_name] = v
  end
  DebugOutPutTable(self.questList, "GameHelper.questList===")
end
function GameHelper:GenerateTodayData(content)
  GameHelper.todayText = content.task.id
  GameHelper.todayTextLocal = GameHelper.todayText
  if content.task.reward_number == -1 then
    return true
  end
  showDaily = true
  GameHelper.todayList = content
  for k, v in pairs(GameHelper.todayList.task.task_content) do
    v.enable = v.action ~= "total_online_time"
  end
end
function GameHelper:ShowQuestCenter()
  DebugOut("GameHelper:ShowQuestCenter")
  GameHelper:GetFlashObject():InvokeASCallback("_root", "ShowQuestCenter")
  if showDaily then
    GameHelper:GetFlashObject():InvokeASCallback("_root", "SetToday", GameLoader:GetGameText(GameHelper.todayList.task.task_name), GameHelper.todayList.title_font, GameHelper.todayList.banner_font)
    if GameHelper.todayList.background ~= "undefined" then
      GameStateDaily:GetTexture(GameHelper.todayList.background, function(extInfo)
        if GameHelper:GetFlashObject() then
          GameHelper:GetFlashObject():ReplaceTexture("LAZY_LOAD_TODAY_PIC_0.png", extInfo.FileName)
        end
      end)
    end
    if GameHelper.todayList.word ~= "undefined" then
      GameStateDaily:GetTexture(GameHelper.todayList.word, function(extInfo)
        if GameHelper:GetFlashObject() then
          GameHelper:GetFlashObject():ReplaceTexture("LAZY_LOAD_TODAY_QuestBAR_PIC.png", extInfo.FileName)
        end
      end)
    end
  end
  GameHelper.currentTab = -1
  GameHelper.RefreshAll(true)
end
function GameHelper:RequestRili(callBack)
  local function msgCallBack(msgType, content)
    if msgType == NetAPIList.enter_monthly_ack.Code or msgType == NetAPIList.other_monthly_ack.Code then
      DebugOutPutTable(content, "daily day")
      GameHelper:GenerateDailyData(content)
      return true
    elseif msgType == NetAPIList.view_award_ack.Code then
      GameHelper:GenerateDailyAwardData(content)
      if callBack ~= nil then
        callBack()
      end
      return true
    end
    return false
  end
  DebugOut("GameHelper:RequestRili -- ")
  NetMessageMgr:SendMsg(NetAPIList.enter_monthly_req.Code, nil, msgCallBack, true, nil)
  NetMessageMgr:SendMsg(NetAPIList.view_award_req.Code, nil, msgCallBack, true, nil)
end
function GameHelper:GenerateDailyData(content)
  if not GameHelper.daily then
    GameHelper.daily = {}
  end
  GameHelper.daily.each_key = {}
  GameHelper.daily.each_day = content.each_day
  local each_day = GameHelper.daily.each_day
  for k, v in ipairs(each_day) do
    GameHelper.daily.each_key[k] = v.id
  end
  local todays = LuaUtils:string_split(GameHelper.todayTextLocal, "-")
  local now_year = tonumber(todays[1])
  local now_month = tonumber(todays[2])
  local now_day = tonumber(todays[3])
  local background = "undefined"
  local activity = 1
  local flashObj = GameHelper:GetFlashObject()
  local isTitle = true
  GameHelper.daily.downloadImgCall = {}
  for k, v in ipairs(GameHelper.daily.each_key) do
    local day = each_day[k]
    local pRet = LuaUtils:string_split(day.id, "-")
    if day.is_today then
      day.today = 0
    else
      local ret_year = tonumber(pRet[1])
      local ret_month = tonumber(pRet[2])
      local ret_day = tonumber(pRet[3])
      if ret_year * 10000 + ret_month * 100 + ret_day > now_year * 10000 + now_month * 100 + now_day then
        day.today = 1
      else
        day.today = -1
      end
    end
    day.tag = pRet[3]
    if day.tag == "01" then
      day.tag = GameLoader:GetGameText("LC_MENU_NEW_DAILY_MONTH_" .. tostring(tonumber(pRet[2])))
      if isTitle then
        isTitle = false
        flashObj:InvokeASCallback("_root", "setMonthTitle", day.tag)
      end
    end
    GameHelper:GetDayState(day)
    if background ~= day.background then
      background = day.background
      if background ~= "undefined" then
        local img = {}
        img.path = day.background
        img.activity = activity
        table.insert(GameHelper.daily.downloadImgCall, img)
        activity = activity + 1
      end
    end
    if day.background ~= "undefined" then
      day.activity = activity - 1
    end
  end
  if content.current then
    GameHelper.daily.current = content.current
  end
  if content.max then
    GameHelper.daily.max = content.max
  end
end
function GameHelper:GenerateDailyAwardData(content)
  DebugOutPutTable(content, "reward daily")
  GameHelper.dailyAward = content.award
end
function GameHelper:ShowRili()
  local flashObj = GameHelper:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowDaily", GameHelper.daily.each_day, GameHelper.daily.current, GameHelper.daily.max)
  end
  for k, v in pairs(GameHelper.daily.downloadImgCall) do
    GameStateDaily:GetTexture(v.path, function(extInfo)
      if GameHelper:GetFlashObject() then
        GameHelper:GetFlashObject():ReplaceTexture("LAZY_LOAD_EVENTDAY_PIC_" .. extInfo.arg.activity .. ".png", extInfo.FileName)
        GameHelper:GetFlashObject():InvokeASCallback("_root", "enabledDayPic", extInfo.arg.activity)
        DebugOut("day.background==>" .. extInfo.arg.activity .. "File:" .. extInfo.FileName)
      end
    end, {
      activity = v.activity
    })
  end
  GameHelper.updateRewardDaily()
end
function GameHelper:RequestAchievement(callBack)
  local function msgCallBack(msgType, content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.achievement_list_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    elseif msgType == NetAPIList.achievement_list_ack.Code then
      GameHelper:GenerateAchievementData(content)
      if callBack ~= nil then
        callBack()
      end
      return true
    end
    return false
  end
  DebugOut("GameHelper:RequestAchievement -- ")
  NetMessageMgr:SendMsg(NetAPIList.achievement_list_req.Code, nil, msgCallBack, true, nil)
end
function GameHelper:GenerateAchievementData(content)
  GameHelper.achievement = {}
  GameHelper.achievement.cup = {}
  GameHelper.achievement.list = {}
  for k, v in ipairs(content.achievement_list or {}) do
    local item = {}
    item = v
    item.title = GameLoader:GetGameText("LC_ACHIEVEMENT_title_" .. tostring(v.id))
    item.desc = GameLoader:GetGameText("LC_ACHIEVEMENT_desc_" .. tostring(v.id))
    item.btnText = GameLoader:GetGameText("LC_MENU_NEW_SPACE_GOTO_BUTTON")
    if item.number >= 100000 then
      item.number = GameUtils.numberConversion(item.number)
    end
    if 100000 <= item.finish_count then
      item.finish_count = GameUtils.numberConversion(item.finish_count)
    end
    item.rewardsList = {}
    if GameHelper.achievement.cup["level_" .. v.level] == nil then
      GameHelper.achievement.cup["level_" .. v.level] = {}
      GameHelper.achievement.cup["level_" .. v.level].total = 0
      GameHelper.achievement.cup["level_" .. v.level].complete = 0
    end
    GameHelper.achievement.cup["level_" .. v.level].total = GameHelper.achievement.cup["level_" .. v.level].total + 1
    if 0 < v.is_finished then
      GameHelper.achievement.cup["level_" .. v.level].complete = GameHelper.achievement.cup["level_" .. v.level].complete + 1
      item.btnText = GameLoader:GetGameText("LC_MENU_CAPTION_RECEIVE")
    end
    if v.is_finished == 0 then
      item.order = v.weight
    elseif v.is_finished == 1 then
      item.order = v.weight - 1000
    elseif v.is_finished == 2 then
      item.order = v.weight + 1000
    end
    for k1, v1 in ipairs(v.rewards or {}) do
      local tmp = {}
      tmp.count = GameHelper:GetAwardCount(v1.item_type, v1.number, v1.no)
      tmp.displayCount = GameUtils.numberConversion(math.floor(tmp.count))
      tmp.frame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v1)
      tmp.pngfile = GameHelper:GetItemInfo(v1).icon_pic or "undefined"
      tmp.model = v1
      item.rewardsList[#item.rewardsList + 1] = tmp
    end
    if v.can_show then
      GameHelper.achievement.list[#GameHelper.achievement.list + 1] = item
    end
  end
  GameHelper:SortAchievemnet()
end
function GameHelper:ShowAchievement()
  GameHelper:InitAchievement()
  GameHelper:SetCupInfo()
  GameHelper:RefreshAchievement()
end
local GameFleetInfoTabBar = LuaObjectManager:GetLuaObject("GameFleetInfoTabBar")
GameHelper.PrestigeStatus = {}
GameHelper.PrestigeStatus.Unfinished = "unfinished"
GameHelper.PrestigeStatus.Wait = "wait"
GameHelper.PrestigeStatus.Received = "received"
local QuestTutorialPrestige = TutorialQuestManager.QuestTutorialPrestige
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local QuestTutorialEnhance_second = TutorialQuestManager.QuestTutorialEnhance_second
GameHelper.PrestigeData = {}
function GameHelper:RequestPrestige(callBack)
  local function msgCallBack(msgType, content)
    if msgType == NetAPIList.prestige_info_ack.Code then
      GameHelper:GeneratePrestigeData(content)
      if callBack ~= nil then
        callBack()
      end
      return true
    elseif msgType == 0 and content.api == NetAPIList.prestige_info_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    end
    return false
  end
  DebugOut("GameHelper:RequestPrestige --")
  NetMessageMgr:SendMsg(NetAPIList.prestige_info_req.Code, nil, msgCallBack, true, nil)
end
function GameHelper:GeneratePrestigeData(prestiges_data)
  prestiges_data.cur_level = 0
  prestiges_data.max_level = 0
  for _, rank_info in ipairs(prestiges_data.prestige_infos) do
    if rank_info.obtained and rank_info.prestige_level > prestiges_data.cur_level then
      prestiges_data.cur_level = rank_info.prestige_level
    end
    if rank_info.prestige_level > prestiges_data.max_level then
      prestiges_data.max_level = rank_info.prestige_level
    end
  end
  GameHelper.PrestigeData = prestiges_data
end
function GameHelper:ShowPrestige()
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "LC_MENU_NEXT_RANK", GameLoader:GetGameText("LC_MENU_NEXT_RANK"))
  self:GetFlashObject():InvokeASCallback("_root", "setLocalText", "LC_MENU_CONTENT_NEED_PRESTIGE", GameLoader:GetGameText("LC_MENU_CONTENT_NEED_PRESTIGE"))
  self:GetFlashObject():InvokeASCallback("_root", "lua2fs_updatePrestigeList", #self.PrestigeData.prestige_infos)
  if self.PrestigeData.cur_level < self.PrestigeData.max_level then
    self:GetFlashObject():InvokeASCallback("_root", "setActivePrestigeRank", self.PrestigeData.cur_level + 1)
  end
end
function GameHelper:OnEraseFromGameState()
  showDaily = false
  GameHelper.todayList = nil
  GameHelper.daily = nil
  GameHelper.achievement = nil
  self:Clear()
  self:UnloadFlashObject()
  GameHelper.EnterPage = 0
  GameHelper.GCSTaskData = nil
  GameHelper.GCSAwardsData = nil
end
function GameHelper:Clear()
  self:GetFlashObject():InvokeASCallback("_root", "clearListBox")
  GameGlobalData:RemoveDataChangeCallback("levelinfo", GameHelper.RefreshAll)
  GameGlobalData:RemoveDataChangeCallback("task_statistic", GameHelper.RefreshAll)
end
function GameHelper:Update(dt)
  self:GetFlashObject():InvokeASCallback("_root", "UpdateQuestList")
  self:GetFlashObject():Update(dt)
end
function GameHelper.GetDayDetailCallback(msgType, content)
  if msgType == NetAPIList.view_each_task_ack.Code then
    GameHelper.cost = content.cost
    GameHelper.touchDailyContent = content.task.task_content
    DebugOutPutTable(content, "GameHelper.GetDayDetailCallback ==== ")
    local day = GameHelper.daily.each_day[GameHelper.currentDay]
    DebugOutPutTable(day, "send day")
    local can_redo = day.can_redo
    local not_redo_all = false
    table.sort(GameHelper.touchDailyContent, GameHelper.sortTodaySubQuestList)
    local isUnLockAll = true
    for k, v in ipairs(GameHelper.touchDailyContent) do
      if immanentversion170 == 4 or immanentversion170 == 5 then
        v.name = GameLoader:GetGameText("LC_QUEST170_TODAY_QUEST_" .. v.action)
        v.info = GameLoader:GetGameText("LC_QUEST170_TODAY_QUEST_DESC_" .. v.action)
      else
        v.name = GameLoader:GetGameText("LC_QUEST_TODAY_QUEST_" .. v.action)
        v.info = GameLoader:GetGameText("LC_QUEST_TODAY_QUEST_DESC_" .. v.action)
      end
      v.enable = v.action ~= "total_online_time"
      local reward = v.rewards[1]
      local extInfo = {}
      extInfo.id = v.id
      v.icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(reward, extInfo, function(extInfo)
        DebugOut("extInfo:" .. extInfo.id .. ":=>item_" .. extInfo.item_id)
        if GameHelper:GetFlashObject() then
          GameHelper:GetFlashObject():InvokeASCallback("_root", "updateDailyFrame", extInfo.id, "item_" .. extInfo.item_id)
        end
      end)
      if not v.is_enabled then
        isUnLockAll = false
      end
      if v.is_enabled and not v.is_finished then
        not_redo_all = true
      end
      v.number = GameHelper:GetAwardCount(reward.item_type, reward.number, reward.no)
    end
    can_redo = can_redo and not_redo_all and isUnLockAll
    local reward = content.task.rewards[1]
    local icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(reward, nil, function(extInfo)
      if GameHelper:GetFlashObject() then
        GameHelper:GetFlashObject():InvokeASCallback("_root", "updateDailyFrame", 0, "item_" .. extInfo.item_id)
      end
    end)
    local number = GameHelper:GetAwardCount(reward.item_type, reward.number, reward.no)
    local allTitle = GameLoader:GetGameText("LC_MENU_NEW_DAILY_FINISH_QUEST_CHAR")
    local allInfo = GameLoader:GetGameText("LC_MENU_NEW_DAILY_INFO_1")
    DebugOutPutTable(content.cost, "day cost")
    DebugOutPutTable(GameHelper.touchDailyContent, "day detail")
    if GameHelper:GetFlashObject() then
      if GameHelper.updateDetail then
        GameHelper:GetFlashObject():InvokeASCallback("_root", "UpdateSelectDay", content.task.id, icon, number, allTitle, allInfo, GameHelper.touchDailyContent, day, content.cost, can_redo and day.today < 0)
      else
        GameHelper:GetFlashObject():InvokeASCallback("_root", "SetSelectDay", content.task.id, icon, number, allTitle, allInfo, GameHelper.touchDailyContent, day, content.cost, can_redo and day.today < 0)
      end
    end
    return true
  end
  return false
end
function GameHelper.UpdatePriceCallback(msgType, content)
  if msgType == NetAPIList.view_each_task_ack.Code then
    GameHelper.cost = content.cost
    if GameHelper:GetFlashObject() then
      GameHelper:GetFlashObject():InvokeASCallback("_root", "updateCost", GameHelper.cost.once_cost)
    end
    local today = GameHelper.daily.each_day[GameHelper.currentDay]
    today.current_task_count = 0
    local isFinishedAll = true
    local updateCall = true
    for k, v in pairs(content.task.task_content) do
      if v.is_finished then
        today.current_task_count = today.current_task_count + 1
      else
        if v.is_enabled then
          updateCall = false
        end
        isFinishedAll = false
      end
    end
    if isFinishedAll then
      if GameHelper:GetFlashObject() then
        GameHelper:GetFlashObject():InvokeASCallback("_root", "increaseDailyProcess")
      end
      GameHelper.daily.current = GameHelper.daily.current + 1
    end
    if updateCall then
      GameHelper:UpdateFinishAll()
    end
    if GameHelper:GetFlashObject() then
      GameHelper:GetFlashObject():InvokeASCallback("_root", "SetTodayQuestFinish", GameHelper.currentDay, today.current_task_count, GameHelper.currentRedo)
    end
    GameHelper.currentRedo = 0
    return true
  end
  return false
end
function GameHelper.GetRedocallback(msgType, content)
  if msgType == NetAPIList.redo_task_ack.Code then
    if content.code == 1 then
      local today_id = GameHelper.daily.each_key[GameHelper.currentDay]
      NetMessageMgr:SendMsg(NetAPIList.view_each_task_req.Code, {id = today_id}, GameHelper.UpdatePriceCallback, true, nil)
    elseif content.code == 0 then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_NOT_ENOUGH_CREDIT"))
    end
    return true
  end
  return false
end
function GameHelper.updateRewardDaily()
  if not GameHelper:GetFlashObject() then
    return
  end
  local hasReward = false
  table.sort(GameHelper.dailyAward, function(v1, v2)
    if v1.is_receive and v2.is_receive then
      return v1.count < v2.count
    end
    if not v1.is_receive and v2.is_receive then
      return true
    end
    if not v1.is_receive and not v2.is_receive then
      return v1.count < v2.count
    end
    return false
  end)
  DebugOutPutTable(GameHelper.dailyAward, "GameHelper.dailyAward == ")
  DebugOut("GameHelper.daily.current =" .. GameHelper.daily.current)
  for k, v in ipairs(GameHelper.dailyAward) do
    v.is_finished = false
    if immanentversion170 == 4 or immanentversion170 == 5 then
      v.rewardTitle = string.format(GameLoader:GetGameText("LC_QUEST170_TODAY_QUEST_REWARD"), v.count)
    else
      v.rewardTitle = string.format(GameLoader:GetGameText("LC_QUEST_TODAY_QUEST_REWARD"), v.count)
    end
    v.is_finished = v.count <= GameHelper.daily.current
    if v.is_finished and not v.is_receive then
      hasReward = true
    end
    for kk, kv in ipairs(v.rewards) do
      local extInfo = {}
      extInfo.rewardId = k
      extInfo.rewardIndex = kk
      kv.icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(kv, extInfo, function(extInfo)
        if GameHelper:GetFlashObject() then
          GameHelper:GetFlashObject():InvokeASCallback("_root", "updateRewardFrame", extInfo.rewardId, extInfo.rewardIndex, "item_" .. extInfo.item_id)
        end
      end)
      kv.count = GameHelper:GetAwardCount(kv.item_type, kv.number, kv.no)
      kv.name = GameHelper:GetAwardTypeText(kv.item_type, kv.number)
    end
  end
  GameHelper:GetFlashObject():InvokeASCallback("_root", "initRewardList", GameHelper.dailyAward, hasReward)
end
function GameHelper:UpdateFinishAll()
  NetMessageMgr:SendMsg(NetAPIList.view_award_req.Code, nil, GameHelper.GetRewardDaily, true, nil)
  GameHelper.updateDetail = true
  local today_id = GameHelper.daily.each_key[GameHelper.currentDay]
  NetMessageMgr:SendMsg(NetAPIList.view_each_task_req.Code, {id = today_id}, GameHelper.GetDayDetailCallback, true, nil)
end
function GameHelper:InitAchievement()
  if GameHelper.achievement and self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "InitAchievement", #GameHelper.achievement.list)
  end
end
function GameHelper:SetCupInfo()
  if GameHelper.achievement and self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "SetAchievementCup", GameHelper.achievement.cup)
  end
end
function GameHelper:OnAchievementItem(itemIndex)
  if GameHelper.achievement.list[itemIndex] and self:GetFlashObject() then
    DebugOut("UpdateAchievementItem")
    DebugOut("haha", itemIndex)
    DebugTable(GameHelper.achievement.list[itemIndex])
    self:GetFlashObject():InvokeASCallback("_root", "UpdateAchievementItem", itemIndex, GameHelper.achievement.list[itemIndex])
  end
end
function GameHelper:GoToAchievement(id)
  local gotoType = ""
  for k, v in pairs(GameHelper.achievement.list) do
    if v.id == id then
      gotoType = v.classify
      break
    end
  end
  if gotoType == "maintask" then
    GameQuestMenu.tabTag = 1
    GameQuestMenu:ShowQuestMenu()
  elseif gotoType == "arena" then
    if GameUtils:IsModuleUnlock("arena") then
      GameUIArena.mCurrentArenaType = GameUIArena.ARENA_TYPE.normalArena
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateArena)
    end
  elseif gotoType == "vip" then
    if not GameGlobalData:FTPVersion() then
      GameVip:showVip(false)
    end
  elseif gotoType == "tech" then
    if GameUtils:IsModuleUnlock("tech") then
      GameUITechnology:EnterTechnology()
    end
  elseif gotoType == "krypton" then
    if GameUtils:IsModuleUnlock("krypton") then
      GameStateKrypton:EnterKrypton(GameStateKrypton.KRYPTON)
    end
  elseif gotoType == "activity" then
    GameStateManager:GetCurrentGameState():AddObject(GameUIActivityNew)
  elseif gotoType == "tc" then
    if GameUtils:IsModuleUnlock("tc") and GameGlobalData.starCraftData.open then
      local GameUIStarCraftEnter = LuaObjectManager:GetLuaObject("GameUIStarCraftEnter")
      GameUIStarCraftEnter:Show()
    else
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
    end
  elseif gotoType == "mining" then
    if GameUtils:IsModuleUnlock("mining") then
      local GameStateMineMap = GameStateManager.GameStateMineMap
      GameStateManager:SetCurrentGameState(GameStateMineMap)
    end
  elseif gotoType == "colonial" then
    if GameUtils:IsModuleUnlock("colonial") then
      GameStateManager:GetCurrentGameState():EraseObject(self)
      local gs = GameStateManager.GameStateColonization
      gs.lastState = GameStateManager:GetCurrentGameState()
      GameStateManager:SetCurrentGameState(gs)
    end
  elseif gotoType == "dicoslab" then
    if GameUtils:IsModuleUnlock("dicoslab") then
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateLab)
    end
  elseif gotoType == "fleets" then
    if GameUtils:IsModuleUnlock("fleets") then
      local GameStateEquipEnhance = GameStateManager.GameStateEquipEnhance
      GameStateEquipEnhance:EnterEquipEnhance(-1)
    end
  elseif gotoType == "hire" then
    if GameUtils:IsModuleUnlock("hire") then
      local GameStatePlayerMatrix = GameStateManager.GameStatePlayerMatrix
      GameStateManager:SetCurrentGameState(GameStatePlayerMatrix)
    end
  elseif gotoType == "remodel" then
    if GameUtils:IsModuleUnlock("remodel") then
      local GameUIFactory = LuaObjectManager:GetLuaObject("GameUIFactory")
      GameStateManager:GetCurrentGameState():AddObject(GameUIFactory)
      GameUIFactory.EnterRebuild = true
    end
  elseif gotoType == "infinite_cosmos" then
    if GameUtils:IsModuleUnlock("infinite_cosmos") then
      local instanceActivityList = GameUIActivityNew:getInstanceData()
      if instanceActivityList then
        GameObjectClimbTower:gotoInstanceSelectScreen(instanceActivityList)
      else
        GameUIActivityNew:RequestActivity(true, GameUILab.gotoInstanceLater)
      end
    end
  elseif gotoType == "wdc" then
    if GameUtils:IsModuleUnlock("arena") then
      if GameUIArena.advancedArenaEnterData == nil then
        NetMessageMgr:SendMsg(NetAPIList.enter_champion_req.Code, nil, function(msgType, content)
          if msgType == NetAPIList.enter_champion_ack.Code then
            GameUIArena.advancedArenaEnterData = content.world_champion
            if GameUIArena.advancedArenaEnterData.states ~= 1 then
              GameUIArena.mCurrentArenaType = GameUIArena.ARENA_TYPE.normalArena
              GameStateManager:SetCurrentGameState(GameStateManager.GameStateArena)
            elseif GameUIArena.advancedArenaEnterData.states == 1 then
              GameUIArena.mCurrentArenaType = GameUIArena.ARENA_TYPE.wdc
              GameStateManager:SetCurrentGameState(GameStateManager.GameStateArena)
            end
            return true
          end
          return false
        end, true)
      elseif GameUIArena.advancedArenaEnterData.states ~= 1 then
        GameUIArena.mCurrentArenaType = GameUIArena.ARENA_TYPE.normalArena
        GameStateManager:SetCurrentGameState(GameStateManager.GameStateArena)
      elseif GameUIArena.advancedArenaEnterData.states == 1 then
        GameUIArena.mCurrentArenaType = GameUIArena.ARENA_TYPE.wdc
        GameStateManager:SetCurrentGameState(GameStateManager.GameStateArena)
      end
    end
  elseif gotoType == "wd" then
    if GameUtils:IsModuleUnlock("wd") then
      if not GameStateMainPlanet.WDActive then
        local QuestTutorialWD = TutorialQuestManager.QuestTutorialWD
        self:GetFlashObject():InvokeASCallback("_root", "HideTutorialWorld")
        self:GetFlashObject():InvokeASCallback("_root", "CloseWDEntrency")
      end
      local GameUIWDPlanetSelect = LuaObjectManager:GetLuaObject("GameUIWDPlanetSelect")
      GameUIWDPlanetSelect.EnterEventDirectly = true
      local gs = GameStateManager.GameStateWD
      DebugOut("1-1-1-1-1-1-1")
      if not GameObjectMainPlanet.fetchNtfForWD then
        local playerList = {}
        local requestParam = {notifies = playerList}
        NetMessageMgr:SendMsg(NetAPIList.alliance_notifies_req.Code, requestParam, GameObjectMainPlanet.fetchNtfForWDCallback, true, nil)
        if not GameStateManager.GameStateWD.AllDataFetched() and GameStateManager.GameStateWD.HaveAlliance() then
          DebugOut("wait here 1")
          GameObjectMainPlanet.waitFetchNtfForWD = true
          GameWaiting:ShowLoadingScreen()
        end
      end
      if GameStateManager.GameStateWD.wdList ~= nil and GameObjectMainPlanet.fetchNtfForWD and (GameStateManager.GameStateWD.HaveAlliance() and GameStateManager.GameStateWD.AllDataFetched() or not GameStateManager.GameStateWD.HaveAlliance()) then
        if GameStateManager.GameStateWD.WDBaseInfo.State == GameStateManager.GameStateWD.WDState.BATTLE and not GameStateManager.GameStateWD.OpponentDataFetched() then
          GameObjectMainPlanet.waitFetchNtfForWD = true
          GameWaiting:ShowLoadingScreen()
        else
          gs.lastState = GameStateManager:GetCurrentGameState()
          GameStateManager:SetCurrentGameState(gs)
          DebugOut("1-1-1-1-1-1-2")
        end
      end
    end
  elseif gotoType == "wc" then
    if GameUtils:IsModuleUnlock("wc") then
      GameUIWorldChampion:RequestEnterWC()
    end
  elseif gotoType == "centalsystem" then
    if GameUtils:IsModuleUnlock("centalsystem") then
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateTacticsCenter)
    end
  elseif gotoType == "shop" then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateStore)
  elseif gotoType == "alliance" and GameUtils:IsModuleUnlock("alliance") then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateAlliance)
  end
end
function GameHelper:ReqAchievementReward(id)
  local param = {}
  param.id = id
  NetMessageMgr:SendMsg(NetAPIList.achievement_reward_receive_req.Code, param, GameHelper.GetAchievementRewardCallBack, true, nil)
end
function GameHelper.GetAchievementRewardCallBack(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.achievement_reward_receive_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgtype == NetAPIList.achievement_reward_receive_ack.Code then
    for k, v in pairs(GameHelper.achievement.list or {}) do
      if v.id == content.id then
        v.is_finished = 2
        v.order = v.weight + 1000
      end
    end
    for k, v in ipairs(content.achievement_list or {}) do
      local item = {}
      item = v
      item.title = GameLoader:GetGameText("LC_ACHIEVEMENT_title_" .. tostring(v.id))
      item.desc = GameLoader:GetGameText("LC_ACHIEVEMENT_desc_" .. tostring(v.id))
      item.btnText = GameLoader:GetGameText("LC_MENU_NEW_SPACE_GOTO_BUTTON")
      if item.number >= 100000 then
        item.number = GameUtils.numberConversion(item.number)
      end
      if 100000 <= item.finish_count then
        item.finish_count = GameUtils.numberConversion(item.finish_count)
      end
      item.rewardsList = {}
      if GameHelper.achievement.cup["level_" .. v.level] == nil then
        GameHelper.achievement.cup["level_" .. v.level] = {}
        GameHelper.achievement.cup["level_" .. v.level].total = 0
        GameHelper.achievement.cup["level_" .. v.level].complete = 0
      end
      GameHelper.achievement.cup["level_" .. v.level].total = GameHelper.achievement.cup["level_" .. v.level].total + 1
      if 0 < v.is_finished then
        GameHelper.achievement.cup["level_" .. v.level].complete = GameHelper.achievement.cup["level_" .. v.level].complete + 1
        item.btnText = GameLoader:GetGameText("LC_MENU_CAPTION_RECEIVE")
      end
      if v.is_finished == 0 then
        item.order = v.weight
      elseif v.is_finished == 1 then
        item.order = v.weight - 1000
      elseif v.is_finished == 2 then
        item.order = v.weight + 1000
      end
      for k1, v1 in ipairs(v.rewards or {}) do
        local tmp = {}
        tmp.count = GameHelper:GetAwardCount(v1.item_type, v1.number, v1.no)
        tmp.displayCount = GameUtils.numberConversion(math.floor(tmp.count))
        tmp.frame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v1)
        tmp.pngfile = GameHelper:GetItemInfo(v1).icon_pic or "undefined"
        tmp.model = v1
        item.rewardsList[#item.rewardsList + 1] = tmp
      end
      if v.can_show then
        GameHelper.achievement.list[#GameHelper.achievement.list + 1] = item
      end
    end
    GameHelper:SortAchievemnet()
    GameHelper:RefreshAchievement()
    if GameHelper:GetFlashObject() then
      GameHelper:GetFlashObject():InvokeASCallback("_root", "RefreshAchievementList", #GameHelper.achievement.list)
    end
    return true
  end
  return false
end
function GameHelper:SortAchievemnet()
  if GameHelper.achievement and GameHelper.achievement.list then
    table.sort(GameHelper.achievement.list, function(a, b)
      return a.order < b.order
    end)
  end
end
function GameHelper:OnFSCommand(cmd, arg)
  if cmd == "TapDaily" then
    GameHelper.currentDay = tonumber(arg)
    local id = GameHelper.daily.each_key[tonumber(arg)]
    DebugOut("TapDaily:" .. arg .. ":" .. id)
    GameHelper.updateDetail = false
    NetMessageMgr:SendMsg(NetAPIList.view_each_task_req.Code, {id = id}, GameHelper.GetDayDetailCallback, true, nil)
  end
  if cmd == "TapBuqian" then
    local str = GameLoader:GetGameText("LC_MENU_COST_CREDIT_ALERT_CHAR")
    local cost = GameHelper.cost.each_cost
    if arg == "0" then
      cost = GameHelper.cost.once_cost
    end
    local infoText = string.format(str, cost)
    GameHelper.currentRedo = tonumber(arg)
    GameUtils:CreditCostConfirm(infoText, function()
      local today_id = GameHelper.daily.each_key[GameHelper.currentDay]
      DebugOut("buqian:" .. today_id .. "=>" .. GameHelper.currentRedo)
      NetMessageMgr:SendMsg(NetAPIList.redo_task_req.Code, {
        id = today_id,
        sub_id = GameHelper.currentRedo
      }, GameHelper.GetRedocallback, true, nil)
    end, true, nil)
  end
  if cmd == "TapAward" then
    do
      local pData = LuaUtils:string_split(arg, "\001")
      local id = tonumber(pData[1])
      local count = tonumber(pData[2])
      NetMessageMgr:SendMsg(NetAPIList.get_award_req.Code, {id = id, count = count}, function(msgType, content)
        if msgType == NetAPIList.get_award_ack.Code then
          for i, v in ipairs(GameHelper.dailyAward) do
            if v.count == count then
              v.is_receive = true
              GameHelper.updateRewardDaily()
              break
            end
          end
          return true
        end
        return false
      end, true, nil)
    end
  end
  if cmd == "TapExecute" then
    local id = tonumber(arg)
    local day
    for k, v in pairs(GameHelper.touchDailyContent) do
      if v.id == id then
        day = v
        break
      end
    end
    if day == nil then
      return
    end
    self.executer.action = day.action
    if self.executer.action then
      self:GotoExecSubQuest(self.executer)
      self.executer.action = nil
    end
  end
  if cmd == "TapTab" then
    local index = tonumber(arg)
    self:GetFlashObject():InvokeASCallback("_root", "SetBtnHelpIsShow", false)
    DebugOut("liyubo command TapTab =" .. index)
    if index == 0 then
      if GameHelper.GCSTaskData == nil then
        GameHelper:RequestGCS(function()
          GameHelper:ShowGCS()
        end)
      else
        self:ShowGCS()
      end
    elseif index == 1 then
      if GameHelper.questList == nil then
        GameHelper:RequestQuest(function()
          GameHelper:ShowQuestCenter()
        end)
      else
        self:ShowQuestCenter()
      end
    elseif index == 2 then
      if GameHelper.daily == nil or GameHelper.dailyAward == nil then
        GameHelper:RequestRili(function()
          GameHelper:ShowRili()
        end)
      else
        self:ShowRili()
      end
    elseif index == 3 then
      if GameHelper.achievement == nil then
        GameHelper:RequestAchievement(function()
          GameHelper:ShowAchievement()
        end)
      else
        self:ShowAchievement()
      end
    elseif index == 4 then
      if GameHelper.PrestigeData == nil then
        GameHelper:RequestPrestige(function()
          self:ShowPrestige()
        end)
      else
        self:ShowPrestige()
      end
    end
    GameHelper.CurSelTopTab = index
  end
  if cmd == "SwitchDaily" then
    local index = tonumber(arg)
    DebugOut("today now text:" .. GameHelper.todayText)
    local todays = LuaUtils:string_split(GameHelper.todayText, "-")
    local year = tonumber(todays[1])
    local month = tonumber(todays[2])
    if index < 0 then
      month = month - 1
      if month == 0 then
        year = year - 1
        month = 12
      end
      GameHelper.todayText = year .. "-" .. month .. "-1"
      NetMessageMgr:SendMsg(NetAPIList.other_monthly_req.Code, {
        id = GameHelper.todayText
      }, GameHelper.GetDailyCallback, true, nil)
    elseif index == 0 then
      NetMessageMgr:SendMsg(NetAPIList.enter_monthly_req.Code, nil, GameHelper.GetDailyCallback, true, nil)
      GameHelper.todayText = GameHelper.todayTextLocal
    else
      month = month + 1
      if month == 13 then
        year = year + 1
        month = 1
      end
      GameHelper.todayText = year .. "-" .. month .. "-1"
      NetMessageMgr:SendMsg(NetAPIList.other_monthly_req.Code, {
        id = GameHelper.todayText
      }, GameHelper.GetDailyCallback, true, nil)
    end
  end
  if cmd == "closeQuestCenter" and self.executer.action then
    self:GotoExecSubQuest(self.executer)
    self.executer.action = nil
  end
  if cmd == "close_state" then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
  end
  if cmd == "tabChanged" then
    GameHelper:TabChanged(tonumber(arg))
  end
  if cmd == "onItemEnter" then
    GameHelper:RefreshItem(tonumber(arg))
  end
  if cmd == "beginPress" then
  elseif cmd == "endPress" then
  end
  if cmd == "getAllReward" then
    if self.currentTab == 0 then
      GameHelper.rewardAll = true
      NetMessageMgr:SendMsg(NetAPIList.get_today_award_req.Code, {
        id = GameHelper.todayList.task.id,
        sub_id = 0
      }, GameHelper.GetTodayQuestRewardCallback, true, nil)
    else
      local name = self:GetTabName(self.currentTab)
      local get_task_reward_req_param = {
        task_id = GameHelper.questList[name].id
      }
      NetMessageMgr:SendMsg(NetAPIList.get_task_reward_req.Code, get_task_reward_req_param, GameHelper.GetQuestRewardCallback, true, nil)
    end
  end
  if cmd == "showRewardDetail" then
    do
      local item_type = ""
      local item_id
      if self.currentTab == 0 and self.todayList then
        item_type = self.todayList.task.rewards[1].item_type
        if not self:IsResource(item_type) then
          item_id = self.todayList.task.rewards[1].number
        end
      else
        local name = self:GetTabName(self.currentTab)
        item_type = self.questList[name].rewards[1].item_type
        if not self:IsResource(item_type) then
          item_id = self.questList[name].rewards[1].number
        end
      end
      DebugOut("item_id", item_id, "item_type: ", item_type)
      if item_id and item_id ~= 0 and not self:IsResource(item_type) then
        local function callback(msgType, content)
          DebugOut("item_id in callback", item_id, "index: ")
          DebugOut("msgType:", msgType)
          DebugTable(content)
          if msgType == NetAPIList.get_reward_detail_ack.Code then
            if GameHelper.localRewardData then
              GameHelper.localRewardData[item_id] = content.details
              DebugOut("message:", item_id)
              DebugTable(content.details)
              GameHelper:showRewardDetail()
            end
            return true
          else
            return false
          end
        end
        if not self:isLocalRewardDataExist(item_id) then
          NetMessageMgr:SendMsg(NetAPIList.get_reward_detail_req.Code, {itemId = item_id}, callback, true, nil)
        else
          GameHelper:showRewardDetail()
        end
      end
    end
  end
  if cmd == "UpdateDicePop" then
    self:GetFlashObject():InvokeASCallback("_root", "UpdateDiceRewardData", tonumber(arg))
  end
  if cmd == "showItemDetail" then
    local item_id
    local item_type = ""
    if self.currentTab == 0 and self.todayList then
      item_type = self.todayList.task.rewards[1].item_type
      if not self:IsResource(item_type) then
        item_id = self.todayList.task.rewards[1].number
      end
    else
      local name = self:GetTabName(self.currentTab)
      item_type = self.questList[name].rewards[1].item_type
      if not self:IsResource(item_type) then
        item_id = self.questList[name].rewards[1].number
      end
    end
    local index = tonumber(arg)
    DebugOut("item_id", item_id, "index: ", index)
    DebugTable(GameHelper.localRewardData)
    local item = GameHelper.localRewardData[item_id][index]
    self:showItemDetil(item, item.item_type)
  end
  if cmd == "questListButtonReleased" then
    self:ExecuteSubQuest(tonumber(arg))
  end
  if cmd == "update_achievement_item" then
    self:OnAchievementItem(tonumber(arg))
  elseif cmd == "TouchHeadBtn" then
    local isGoto, id = unpack(LuaUtils:string_split(arg, ":"))
    if isGoto == "true" then
      GameHelper:GoToAchievement(tonumber(id))
    else
      GameHelper:ReqAchievementReward(tonumber(id))
    end
  elseif cmd == "ShowDetail" then
    local index, ind = unpack(LuaUtils:string_split(arg, ":"))
    local item = GameHelper.achievement.list[tonumber(index)]
    if item and item.rewardsList[tonumber(ind)] then
      local it = item.rewardsList[tonumber(ind)].model
      if it.item_type == "item" then
        it.cnt = 1
        ItemBox:showItemBox("Item", it, tonumber(it.number), 320, 480, 200, 200, "", nil, "", nil)
      elseif it.item_type == "fleet" then
        local fleetID = it.number
        ItemBox:ShowCommanderDetail2(fleetID, nil, nil)
      else
        ItemBox:ShowGameItem(it)
      end
    end
  end
  if cmd == "update_prestige_item" then
    self:UpdatePrestigeItem(tonumber(arg))
    return
  elseif cmd == "select_ranking" then
    self:UpdateActivePrestigeInfo(tonumber(arg))
    return
  elseif cmd == "receive_award" then
    self:ObtainAward(tonumber(arg))
    return
  elseif cmd == "btn_help" then
    local text_title = ""
    local info = ""
    if GameHelper.CurSelTopTab == GameHelper.PageGCS then
      info = GameLoader:GetGameText("LC_MENU_GCS_Help_CHAR")
    elseif GameHelper.CurSelTopTab == GameHelper.PagePrestige then
      info = GameLoader:GetGameText("LC_MENU_PRESTIGE_HELP")
    end
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.HELP)
    GameUIMessageDialog:Display(text_title, info)
  elseif cmd == "MenuItemReleased" then
    if immanentversion == 1 then
      local index_item = tonumber(arg)
      local data_prestige = self:GetPrestigeInfo(index_item)
      local prestige_name = GameDataAccessHelper:GetMilitaryRankName(data_prestige.prestige_level)
      local award_data, awardDetail = GameDataAccessHelper:GenerateMilitaryRankAwardInfo(data_prestige.prestige_level)
      DebugOutPutTable(awardDetail, "awardDetail")
      local daily_award = GameDataAccessHelper:GenerateMilitaryRankDailyAwardInfo(data_prestige.prestige_level)
      local firstAwardDetail = awardDetail[1]
      local prestige_info_param = prestige_name
      if firstAwardDetail[1] == "skill" then
        local skill = firstAwardDetail[2]
        local skillName = GameDataAccessHelper:GetSkillNameText(skill)
        local skillDesc = GameDataAccessHelper:GetSkillDesc(skill)
        local skillRangeType = "TYPE_" .. GameDataAccessHelper:GetSkillRangeType(skill)
        local skillRangetypeName = GameLoader:GetGameText("LC_MENU_SKILL_RANGE_TYPE_" .. GameDataAccessHelper:GetSkillRangeType(skill))
        prestige_info_param = prestige_info_param .. "\002" .. "skill\001" .. skill .. "\001" .. skillName .. "\001" .. skillDesc .. "\001" .. skillRangeType .. "\001" .. skillRangetypeName .. "\001"
      elseif firstAwardDetail[1] == "recruit_count_up" then
        local recruit_count = firstAwardDetail[2]
        local recruit_count_name = GameLoader:GetGameText("LC_MENU_RECRUIT_COUNT_NAME")
        local recruit_count_desc = GameLoader:GetGameText("LC_MENU_RECRUIT_COUNT_DESC_" .. recruit_count)
        prestige_info_param = prestige_info_param .. "\002" .. "recruit_count_up\001" .. recruit_count .. "\001" .. recruit_count_name .. "\001" .. recruit_count_desc .. "\001"
      elseif firstAwardDetail[1] == "unlock_formation" then
        local unlock_formation = firstAwardDetail[2]
        local unlock_formation_name = GameLoader:GetGameText("LC_MENU_UNLOCK_FORMATION_NAME")
        local unlock_formation_desc = GameLoader:GetGameText("LC_MENU_UNLOCK_FORMATION_DESC")
        prestige_info_param = prestige_info_param .. "\002" .. "unlock_formation\001" .. unlock_formation .. "\001" .. unlock_formation_name .. "\001" .. unlock_formation_desc .. "\001"
      else
        prestige_info_param = prestige_info_param .. "\002empty"
      end
      prestige_info_param = prestige_info_param .. "\002" .. daily_award
      ItemBox:ShowPrestigeInfo(prestige_info_param)
    elseif immanentversion == 2 then
    end
  elseif cmd == "onGCSWeekBtnItemEnter" then
    self:OnUpdateGCSWeekBtnItem(tonumber(arg))
  elseif cmd == "Click_GCS_Week_Btn" then
    self:OnGCSClickWeekBtn(tonumber(arg))
  elseif cmd == "onGCSTodayTaskItemEnter" then
    self:OnGCSTodayTaskItem(tonumber(arg))
  elseif cmd == "onGCSWeekTaskItemEnter" then
    self:OnGCSWeekTaskItem(tonumber(arg))
  elseif cmd == "Click_GCS_Tab" then
    self:OnGCSClickTab(tonumber(arg))
  elseif cmd == "Click_GCS_Awards_Pre" then
    self:OnGCSClickAwardsPrePage()
  elseif cmd == "Click_GCS_Awards_Next" then
    self:OnGCSClickAwardsNextPage()
  elseif cmd == "Click_GCS_Get_Task_Reward" then
    local itemKey, taskID = unpack(LuaUtils:string_split(arg, "\001"))
    self:OnGCSClickGetTaskReward(tonumber(itemKey), tonumber(taskID))
  elseif cmd == "Click_GCS_Change_Task" then
    self:OnGCSClickChangeTask(tonumber(arg))
  elseif cmd == "Click_GCS_Buy_Card_Btn" then
    self:OnGCSClickBuyCardBtn()
  elseif cmd == "Click_GCS_Buy_Level_Btn" then
    self:OnGCSClickBuyLevelBtn()
  elseif cmd == "Click_GCS_Reward_All_Btn" then
    self:OnGCSClickRewardAllBtn()
  elseif cmd == "ShowGCSTaskRewardItemDetail" then
    local taskID, rewardIndex = unpack(LuaUtils:string_split(arg, "\001"))
    self:ShowGCSTaskRewardItemDetail(tonumber(taskID), tonumber(rewardIndex))
  elseif cmd == "ShowGCSLvRewardItemDetail" then
    local lv, gridType, rewardIndex = unpack(LuaUtils:string_split(arg, "\001"))
    self:ShowGCSLvRewardItemDetail(tonumber(lv), tonumber(gridType), tonumber(rewardIndex))
  elseif cmd == "GetGCSExtraReward" then
    self:RequestGetGCSExtraReward(tonumber(arg))
  elseif cmd == "ShowGCSExtraRewardItemDetail" then
    self:ShowGCSExtraRewardItemDetail(tonumber(arg))
  elseif cmd == "Click_GCS_buy_card_item" then
    DebugOut("Click_GCS_buy_card_item-----")
    self:OnClickGCSBuyCardItem(arg)
  end
end
function GameHelper:isLocalRewardDataExist(key)
  if GameHelper.localRewardData[key] ~= nil then
    return true
  else
    return false
  end
end
function GameHelper:showItemDetil(item_, item_type)
  local item = item_
  if item_type == "krypton" then
    if item.level == 0 then
      item.level = 1
    end
    local detail = GameUIKrypton:TryQueryKryptonDetail(item.number, item.level, function(msgType, content)
      local ret = GameUIKrypton.QueryKryptonDetailCallBack(msgType, content)
      if ret then
        local tmp = GameGlobalData:GetKryptonDetail(item.number, item.level)
        if tmp == nil then
          return false
        end
        ItemBox:SetKryptonBox(tmp, item.number)
        local operationTable = {}
        operationTable.btnUnloadVisible = false
        operationTable.btnUpgradeVisible = false
        operationTable.btnUseVisible = false
        operationTable.btnDecomposeVisible = false
        ItemBox:ShowKryptonBox(320, 1080, operationTable)
      end
      return ret
    end)
    if detail == nil then
    else
      ItemBox:SetKryptonBox(detail, item.number)
      local operationTable = {}
      operationTable.btnUnloadVisible = false
      operationTable.btnUpgradeVisible = false
      operationTable.btnUseVisible = false
      operationTable.btnDecomposeVisible = false
      ItemBox:ShowKryptonBox(320, 1080, operationTable)
    end
  end
  if item_type == "fleet" then
    local GameUIFirstCharge = LuaObjectManager:GetLuaObject("GameUIFirstCharge")
    GameUIFirstCharge:ShowCommanderInfo(tonumber(item.number))
  end
  if item_type == "item" then
    item.cnt = 1
    ItemBox:showItemBox("ChoosableItem", item, tonumber(item.number), 320, 1080, 200, 200, "", nil, "", nil)
  end
  if item_type == "medal_item" or item_type == "medal" then
    ItemBox:ShowGameItem(item)
  end
end
function GameHelper:showRewardDetail()
  if self:GetFlashObject() then
    local item_id
    local item_type = ""
    if self.currentTab == 0 and self.todayList then
      item_type = self.todayList.task.rewards[1].item_type
      if not self:IsResource(item_type) then
        item_id = self.todayList.task.rewards[1].number
      end
    else
      local name = self:GetTabName(self.currentTab)
      item_type = self.questList[name].rewards[1].item_type
      if not self:IsResource(item_type) then
        item_id = self.questList[name].rewards[1].number
      end
    end
    local param = {}
    DebugOut("hellotp", item_id)
    DebugTable(GameHelper.localRewardData[item_id])
    if item_id and not self:IsResource(item_type) and self.localRewardData[item_id] then
      for k, v in ipairs(self.localRewardData[item_id]) do
        local localparam = {}
        local info = GameHelper:GetItemInfo(v)
        localparam.icon = self:GetAwardTypeIconFrameNameSupportDynamic(v)
        localparam.item_num = self:GetAwardCountWithX(v.item_type, v.number, v.no)
        localparam.pngfile = info.icon_pic
        table.insert(param, localparam)
      end
      DebugOut("fortp:")
      DebugTable(param)
      self:GetFlashObject():InvokeASCallback("_root", "showRewardDetail", param)
    end
  end
end
function GameHelper:ExecuteSubQuest(key)
  self.currentSubQuestKey = key
  local currentSubQuestList
  if self.currentTab == 0 then
    currentSubQuestList = GameHelper:GetTodaySubQuestList()
  else
    currentSubQuestList = GameHelper:GetCurrentSubQuestList()
  end
  local subQuest
  for k, v in pairs(currentSubQuestList) do
    if v.id == key then
      subQuest = v
    end
  end
  if subQuest then
    DebugOut("subQuest: ", subQuest.is_get_reward, subQuest.is_finished)
    if not subQuest.is_get_reward and subQuest.is_finished then
      local get_sub_task_reward_req_param = {
        task_id = subQuest.id
      }
      if self.currentTab == 0 then
        GameHelper.rewardAll = false
        NetMessageMgr:SendMsg(NetAPIList.get_today_award_req.Code, {
          id = GameHelper.todayList.task.id,
          sub_id = tonumber(key)
        }, GameHelper.GetTodayQuestRewardCallback, true, nil)
      else
        NetMessageMgr:SendMsg(NetAPIList.get_task_reward_req.Code, get_sub_task_reward_req_param, GameHelper.GetSubQuestRewardCallback, true, nil)
      end
    elseif not subQuest.is_finished then
      self:GetFlashObject():InvokeASCallback("_root", "GotoExecSubQuest")
      self.executer.action = subQuest.action
      self.executer.category = subQuest.category
      self.executer.subCategory = subQuest.subCategory
    end
  end
end
function GameHelper:OnAddOther()
  DebugOut("GameHelper:OnAddOther")
  self:GetFlashObject():InvokeASCallback("_root", "hideScreen", true)
end
function GameHelper:OnEraseOther()
  if showDaily then
    GameHelper.daily = nil
  end
  DebugOut("GameHelper:OnEraseOther")
  self:OnAddToGameState()
  self:GetFlashObject():InvokeASCallback("_root", "hideScreen", false)
end
function GameHelper:GotoExecSubQuest(executer)
  DebugOutPutTable(executer, "GotoExecSubQuest: ")
  local action = executer.action
  if action == "collection" then
    if GameGlobalData:BuildingCanEnter(GameUICollect.buildingName) then
      GameUICollect:ShowCollect()
    else
      local GameUIBuilding = LuaObjectManager:GetLuaObject("GameUIBuilding")
      GameUIBuilding:DisplayUpgradeDialog(GameUICollect.buildingName)
    end
  elseif action == "affairs_hall" then
    GameStateManager.GameStateAffairInfo:RequestInfo()
  elseif action == "mainplan" then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
  elseif action == "warp_Gate" then
    GameStateRecruit:SetTabtype("warpgate")
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateRecruit)
  elseif action == "recruit" then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateRecruit)
  elseif action == "krypton" then
    GameStateKrypton:EnterKrypton(GameStateKrypton.LAB)
  elseif action == "tech_lab" then
    GameUITechnology:EnterTechnology()
  elseif action == "enhance" then
    GameStateEquipEnhance:EnterEquipEnhance(GameFleetEnhance.TAB_ENHANCE)
  elseif action == "ace_combat" then
    GameStateConfrontation:EnterConfrontation()
  elseif action == "championship" then
    GameUIArena:SetArenaType(GameUIArena.ARENA_TYPE.normalArena)
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateArena)
  elseif action == "champion_get_reward" then
    GameUIArena:SetArenaType(GameUIArena.ARENA_TYPE.normalArena)
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateArena)
  elseif action == "battle_select" then
    local progress = GameGlobalData:GetData("progress")
    GameStateBattleMap:EnterSection(progress.act, progress.chapter, nil)
  elseif action == "function" then
  elseif action == "alliance" then
    GameStateManager.GameStateAlliance:Activate()
  elseif action == "mining" or action == "mining_attack" then
    local GameStateMineMap = GameStateManager.GameStateMineMap
    GameStateManager:SetCurrentGameState(GameStateMineMap)
  elseif action == "donate" or action == "explore" then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateAlliance)
    GameUIUserAlliance:SelectTab("function")
  elseif action == "chat" then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
    GameStateManager:GetCurrentGameState():AddObject(GameUIChat)
    GameUIChat:SelectChannel("alliance")
  elseif action == "colony_action" then
    local gs = GameStateManager.GameStateColonization
    gs.lastState = GameStateManager:GetCurrentGameState()
    GameStateManager:SetCurrentGameState(gs)
  elseif action == "world_boss" then
    GameStateLab.mCurSubState = STATE_LAB_SUB_STATE.SUBSTATE_NORMAL
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateLab)
  elseif action == "player_ac" then
    GameStateLab.mCurSubState = STATE_LAB_SUB_STATE.SUBSTATE_NORMAL
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateLab)
  elseif action == "laba_use" then
    GameStateLab.mCurSubState = STATE_LAB_SUB_STATE.SUBSTATE_NORMAL
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateLab)
  elseif action == "read_activity" then
    GameStateManager:GetCurrentGameState():AddObject(GameUIActivityNew)
  elseif action == "buy_daily_gift" then
    GameStateManager:SetCurrentGameState(GameStateStore)
  elseif action == "remodel" then
    local GameUIFactory = LuaObjectManager:GetLuaObject("GameUIFactory")
    GameStateManager:GetCurrentGameState():AddObject(GameUIFactory)
    GameUIFactory.EnterRebuild = true
  elseif action == "daily_monthly_card" then
    local GameVip = LuaObjectManager:GetLuaObject("GameVip")
    GameVip:showVip(false, nil, 3)
  elseif action == "infinite_cosmos" then
    GameStateLab.mCurSubState = STATE_LAB_SUB_STATE.SUBSTATE_NORMAL
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateLab)
  elseif action == "chaos_quasar" then
    GameStateLab.mCurSubState = STATE_LAB_SUB_STATE.SUBSTATE_NORMAL
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateLab)
  elseif action == "pay" then
    local GameVip = LuaObjectManager:GetLuaObject("GameVip")
    GameVip:showVip(false)
  elseif action == "combo_gacha" then
    GameStateManager:GetCurrentGameState():AddObject(GameUIActivityNew)
  elseif action == "special_store_buy" then
    LuaObjectManager:GetLuaObject("GameGoodsList"):SetEnterStoreType(2)
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateStore)
  elseif action == "tactical_refine" then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateTacticsCenter)
  elseif action == "join_crusade" then
    GameStateLab.mCurSubState = STATE_LAB_SUB_STATE.SUBSTATE_NORMAL
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateLab)
  elseif action == "join_crusade_times" then
    GameStateLab.mCurSubState = STATE_LAB_SUB_STATE.SUBSTATE_NORMAL
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateLab)
  elseif action == "tc_score" then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
    GameObjectMainPlanet:BuildingClicked("territorial_entry")
  elseif action == "try_champion" then
    GameUIArena:SetArenaType(GameUIArena.ARENA_TYPE.normalArena)
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateArena)
  elseif action == "wd_score" then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
    GameObjectMainPlanet:BuildingClicked("world_domination")
  elseif action == "total_online_time" then
  elseif action == "normal_store_buy" then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateStore)
  elseif action == "global_chat" then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
    GameStateManager:GetCurrentGameState():AddObject(GameUIChat)
    GameUIChat:SelectChannel("world")
  elseif action == "glc" then
    GameUIArena:SetArenaType(GameUIArena.ARENA_TYPE.wdc)
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateArena)
  elseif action == "occupy_succ" then
    local item = GameUILab:GetLibsItemByName("starwar")
    if item and item.count_down > 0 then
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateMiningWorld)
      local GameUIMiningWorld = LuaObjectManager:GetLuaObject("GameUIMiningWorld")
      GameUIMiningWorld.enterDir = 1
      GameUIMiningWorld:Show()
    else
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
    end
  elseif action == "medal_boss_challenge" then
    local callback = function()
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateDaily)
    end
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateStarSystem)
    local GameUIStarSystemFuben = LuaObjectManager:GetLuaObject("GameUIStarSystemFuben")
    GameUIStarSystemFuben:Show(nil, callback)
  elseif action == "medal_recruit" then
    local callback = function()
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateDaily)
    end
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateStarSystem)
    local GameUIStarSystemPort = LuaObjectManager:GetLuaObject("GameUIStarSystemPort")
    GameUIStarSystemPort:JumpTo("chouka", callback)
  elseif action == "medal_store_refresh" then
    local callback = function()
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateDaily)
    end
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateStarSystem)
    local GameUIStarSystemPort = LuaObjectManager:GetLuaObject("GameUIStarSystemPort")
    GameUIStarSystemPort:JumpTo("shop", callback)
  end
end
function GameHelper:GetTabName(index)
  for k, v in pairs(self.TabName) do
    if v == index then
      return k
    end
  end
  return nil
end
function GameHelper:TabChanged(index)
  if self.currentTab == index then
    return
  end
  self.currentTab = index
  if index == 0 then
    self:RefreshToday()
  else
    self:RefreshTab(index)
  end
end
function GameHelper:GetTodaySubQuestList(...)
  if self.todayList == nil then
    return {}
  end
  currentSubQuestList = LuaUtils:table_values(self.todayList.task.task_content)
  table.sort(currentSubQuestList, GameHelper.sortTodaySubQuestList)
  return currentSubQuestList
end
function GameHelper:GetCurrentSubQuestList(tabIndex)
  if self.questList == nil then
    return {}
  end
  local tab = tabIndex or self.currentTab
  DebugOut("GameHelper:GetCurrentSubQuestList + tab=" .. tab)
  if tab == -1 then
    tab = 1
  end
  DebugOut("GameHelper:GetCurrentSubQuestList + tab=" .. tab)
  local name = self:GetTabName(tab)
  DebugOut("GameHelper:GetCurrentSubQuestList + tab=" .. name)
  DebugOutPutTable(self.questList, "self.questList==")
  local currentSubQuestList = self.questList[name].task_content
  local player_level = GameGlobalData:GetData("levelinfo").level
  currentSubQuestList = LuaUtils:table_values(LuaUtils:table_filter(currentSubQuestList, function(k, v)
    return player_level >= v.player_level
  end))
  local questHasReward = self.questList[name].is_get_reward
  local questHasFinished = self.questList[name].is_finished
  currentSubQuestList = LuaUtils:table_values(currentSubQuestList)
  DebugOutPutTable(currentSubQuestList, "currentSubQuestList")
  table.sort(currentSubQuestList, GameHelper.sortSubQuestList)
  return currentSubQuestList, name
end
function GameHelper.sortTodaySubQuestList(v1, v2)
  if v1 and v2 then
    if v1.is_enabled and not v2.is_enabled then
      return true
    elseif v1.is_enabled and v2.is_enabled then
      if not v1.is_get_reward and v2.is_get_reward then
        return true
      elseif not v1.is_get_reward and not v2.is_get_reward then
        if v1.is_finished and v2.is_finished then
          return v1.id < v2.id
        elseif not v1.is_finished and v2.is_finished then
          return false
        elseif v1.is_finished and not v2.is_finished then
          return true
        else
          return v1.id < v2.id
        end
      elseif v1.is_get_reward and not v2.is_get_reward then
        return false
      else
        return v1.id < v2.id
      end
    elseif not v1.is_enabled and v2.is_enabled then
      return false
    else
      return v1.id < v2.id
    end
  end
  return false
end
function GameHelper.sortSubQuestList(v1, v2)
  if v1 and v2 then
    if v1.is_finished and not v1.is_get_reward and (not v2.is_finished or v2.is_get_reward) then
      return true
    elseif not v1.is_finished and v2.is_get_reward then
      return true
    end
  end
  return false
end
function GameHelper:RefreshItem(key)
  local currentSubQuestList
  if self.currentTab == 0 then
    currentSubQuestList = GameHelper:GetTodaySubQuestList()
  else
    currentSubQuestList = GameHelper:GetCurrentSubQuestList()
  end
  if key == GameHelper.titleQuestId then
    if self.currentTab == 0 and self.todayList then
      local questCanFinish = not self.todayList.task.is_get_reward and self.todayList.task.is_finished
      DebugOut("\230\137\147\229\141\176 today quest can finish: ", questCanFinish)
      local finishedCount = 0
      for k, v in ipairs(currentSubQuestList) do
        if v.is_finished then
          finishedCount = finishedCount + 1
        end
      end
      local icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(self.todayList.task.rewards[1], nil, function(extInfo)
        if GameHelper:GetFlashObject() then
          GameHelper:GetFlashObject():InvokeASCallback("_root", "updateTitleFrame", -1, "item_" .. extInfo.item_id)
        end
      end)
      local isFinish = self.todayList.task.is_finished
      local rewardNumberSubFinishNeed = self.todayList.task.reward_number
      local claimedText = GameLoader:GetGameText("LC_MENU_CLAIMED_BUTTON")
      DebugOut("TEST_TP:")
      DebugTable(self.todayList.task.rewards[1])
      self:GetFlashObject():InvokeASCallback("_root", "isForDay", true)
      self:GetFlashObject():InvokeASCallback("_root", "setTitleItem", GameHelper.titleQuestId, GameLoader:GetGameText("LC_MENU_NEW_DAILY_EVENT_INFO"), finishedCount, #currentSubQuestList, icon, questCanFinish, GameLoader:GetGameText("LC_MENU_DAILY_TASK_REWARDS_BUTTON"), GameLoader:GetGameText("LC_MENU_TARGET_CHAR"), GameLoader:GetGameText("LC_MENU_DAILY_TASK_OPERATION_BUTTON"), GameLoader:GetGameText("LC_MENU_GET_CHAR"), isFinish, rewardNumberSubFinishNeed, claimedText)
    else
      local name = self:GetTabName(self.currentTab)
      if self.questList and self.questList[name] then
        local questCanFinish = not self.questList[name].is_get_reward and self.questList[name].is_finished
        DebugOut("\230\137\147\229\141\176 questCanFinish", name, questCanFinish)
        DebugTable(self.questList)
        local finishedCount = 0
        for k, v in ipairs(currentSubQuestList) do
          if v.is_finished then
            finishedCount = finishedCount + 1
          end
        end
        local icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(self.questList[name].rewards[1], nil, function(extInfo)
          if GameHelper:GetFlashObject() then
            GameHelper:GetFlashObject():InvokeASCallback("_root", "updateTitleFrame", -1, "item_" .. extInfo.item_id)
          end
        end)
        local isFinish = self.questList[name].is_finished
        self:GetFlashObject():InvokeASCallback("_root", "isForDay", false)
        local rewardNumberSubFinishNeed = self.questList[name].reward_number
        local claimedText = GameLoader:GetGameText("LC_MENU_CLAIMED_BUTTON")
        self:GetFlashObject():InvokeASCallback("_root", "setTitleItem", GameHelper.titleQuestId, GameLoader:GetGameText("LC_QUEST_QUEST_INFO_" .. string.upper(name)), finishedCount, #currentSubQuestList, icon, questCanFinish, GameLoader:GetGameText("LC_MENU_DAILY_TASK_REWARDS_BUTTON"), GameLoader:GetGameText("LC_MENU_TARGET_CHAR"), GameLoader:GetGameText("LC_MENU_DAILY_TASK_OPERATION_BUTTON"), GameLoader:GetGameText("LC_MENU_GET_CHAR"), isFinish, rewardNumberSubFinishNeed, claimedText)
      end
    end
    return
  end
  if not currentSubQuestList then
    return
  end
  local subQuest
  for k, v in ipairs(currentSubQuestList) do
    if v.id == key then
      subQuest = v
    end
  end
  DebugOut("refresh item:" .. key)
  DebugOutPutTable(currentSubQuestList, "refresh")
  if subQuest then
    local stateText = GameLoader:GetGameText("LC_MENU_EXECUTE_CHAR")
    if subQuest.is_finished then
      stateText = GameLoader:GetGameText("LC_MENU_GET_CHAR")
    end
    local rewardFrame = 1
    local number = GameHelper:GetAwardCount(subQuest.rewards[1].item_type, subQuest.rewards[1].number, subQuest.rewards[1].no)
    local rewardCountText = GameUtils.numberConversion(number)
    local rewardPng = GameHelper:GetItemInfo(subQuest.rewards[1]).icon_pic
    local icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(subQuest.rewards[1], {id = key}, function(extInfo)
      if GameHelper:GetFlashObject() then
        GameHelper:GetFlashObject():InvokeASCallback("_root", "updateTitleFrame", extInfo.id, "item_" .. extInfo.item_id)
      end
    end)
    if subQuest.finish_count > subQuest.number then
      subQuest.finish_count = subQuest.number
    end
    local player_level = GameGlobalData:GetData("levelinfo").level
    local unlockLV = 0
    if player_level < subQuest.player_level then
      unlockLV = subQuest.player_level
    end
    if self.currentTab == 0 then
      self:GetFlashObject():InvokeASCallback("_root", "setSubItem", key, subQuest.is_enabled, subQuest.is_get_reward, subQuest.is_finished, GameLoader:GetGameText("LC_QUEST_TODAY_QUEST_" .. subQuest.action), subQuest.finish_count .. "/" .. subQuest.number, rewardFrame, stateText, icon, rewardPng, rewardCountText, GameLoader:GetGameText("LC_QUEST_TODAY_QUEST_DESC_" .. subQuest.action), unlockLV, subQuest.enable, GameLoader:GetGameText("LC_MENU_Level"))
    else
      self:GetFlashObject():InvokeASCallback("_root", "setSubItem", key, subQuest.is_enabled, subQuest.is_get_reward, subQuest.is_finished, GameLoader:GetGameText("LC_QUEST_SUB_QUEST_" .. subQuest.id), subQuest.finish_count .. "/" .. subQuest.number, rewardFrame, stateText, icon, rewardPng, rewardCountText, GameLoader:GetGameText("LC_QUEST_SUB_QUEST_DESC_" .. subQuest.id), unlockLV, true, GameLoader:GetGameText("LC_MENU_Level"))
    end
  end
end
function GameHelper:RefreshToday()
  local currentSubQuestList = GameHelper:GetTodaySubQuestList()
  self:GetFlashObject():InvokeASCallback("_root", "clearListBox")
  self:GetFlashObject():InvokeASCallback("_root", "InitQuestList")
  self:GetFlashObject():InvokeASCallback("_root", "AddItemId", GameHelper.titleQuestId)
  for k, v in ipairs(currentSubQuestList) do
    self:GetFlashObject():InvokeASCallback("_root", "AddItemId", v.id)
  end
  if #currentSubQuestList > 4 then
    self:GetFlashObject():InvokeASCallback("_root", "showRichangArrow", true)
  else
    self:GetFlashObject():InvokeASCallback("_root", "showRichangArrow", false)
  end
end
function GameHelper:RefreshTab(index)
  local currentSubQuestList = GameHelper:GetCurrentSubQuestList(index)
  self:GetFlashObject():InvokeASCallback("_root", "clearListBox")
  self:GetFlashObject():InvokeASCallback("_root", "InitQuestList")
  self:GetFlashObject():InvokeASCallback("_root", "AddItemId", GameHelper.titleQuestId)
  for k, v in ipairs(currentSubQuestList) do
    self:GetFlashObject():InvokeASCallback("_root", "AddItemId", v.id)
  end
  if #currentSubQuestList > 4 then
    self:GetFlashObject():InvokeASCallback("_root", "showRichangArrow", true)
  else
    self:GetFlashObject():InvokeASCallback("_root", "showRichangArrow", false)
  end
end
function GameHelper.RefreshAll(forceRefresh)
  if not GameHelper:GetFlashObject() then
    return
  end
  forceRefresh = forceRefresh or false
  if forceRefresh or GameStateManager:GetCurrentGameState():IsObjectInState(GameHelper) then
    GameHelper:RefreshTodayTab()
    GameHelper:RefreshNews()
    GameHelper:RefreshAchievement()
    if GameHelper.currentTab == 0 then
      GameHelper:RefreshToday()
    else
      GameHelper:RefreshQuestList()
    end
  end
end
function GameHelper:RefreshTodayTab()
  if showDaily and self.currentTab == -1 then
    self.currentTab = 0
  end
  GameHelper:GetFlashObject():InvokeASCallback("_root", "SetTabState", 1, self:isTodayLived())
end
function GameHelper:RefreshAchievement()
  local isNew = false
  if GameHelper.achievement then
    for k, v in pairs(GameHelper.achievement.list or {}) do
      if v.is_finished == 1 then
        isNew = true
        break
      end
    end
  end
  if GameHelper:GetFlashObject() then
    GameHelper:GetFlashObject():InvokeASCallback("_root", "SetTabState", 3, isNew)
  end
end
function GameHelper:RefreshNews()
  local newCount = {}
  local visible = {}
  local isNew = false
  for k, v in pairs(self.TabName) do
    newCount[v] = 0
    visible[v] = 0
  end
  if self.questList and type(self.questList) == "table" then
    for k, v in pairs(self.questList) do
      local index = self.TabName[k]
      if not v.is_get_reward and v.is_finished then
        newCount[index] = newCount[index] + 1
      end
      for tk, tv in pairs(GameHelper:GetCurrentSubQuestList(index)) do
        if not tv.is_get_reward and tv.is_finished then
          newCount[index] = newCount[index] + 1
          isNew = true
        end
      end
    end
  end
  if self.questList and type(self.questList) == "table" then
    for k, v in pairs(self.questList) do
      local index = self.TabName[k]
      local tempList = GameHelper:GetCurrentSubQuestList(index)
      if not LuaUtils:table_empty(tempList) then
        visible[index] = 1
      end
    end
  end
  if self.currentTab == -1 then
    DebugOutPutTable(visible, "visible ===")
    for k, v in ipairs(visible) do
      if v == 1 then
        self.currentTab = k
        DebugOut("setTab === " .. self.currentTab)
        GameHelper:GetFlashObject():InvokeASCallback("_root", "setTab", self.currentTab)
        break
      end
    end
  end
  local news = table.concat(newCount, "\001")
  local visibles = table.concat(visible, "\001")
  self:GetFlashObject():InvokeASCallback("_root", "refreshNews", visibles, news)
  DebugOut("refreshNews")
  DebugTable(visible)
  DebugTable(newCount)
  if isNew then
    GameHelper:GetFlashObject():InvokeASCallback("_root", "SetTabState", 1, true)
  end
end
function GameHelper:RefreshQuestList()
  local currentSubQuestList = GameHelper:GetCurrentSubQuestList()
  self:GetFlashObject():InvokeASCallback("_root", "clearListBox")
  self:GetFlashObject():InvokeASCallback("_root", "InitQuestList")
  self:GetFlashObject():InvokeASCallback("_root", "AddItemId", GameHelper.titleQuestId)
  for k, v in ipairs(currentSubQuestList) do
    self:GetFlashObject():InvokeASCallback("_root", "AddItemId", v.id)
  end
  if #currentSubQuestList > 4 then
    self:GetFlashObject():InvokeASCallback("_root", "showRichangArrow", true)
  else
    self:GetFlashObject():InvokeASCallback("_root", "showRichangArrow", false)
  end
end
function GameHelper:GetAwardTypeTextAndIcon(key, number)
  local typeText = "UnknownAward"
  local icon = key
  if key == "money" then
    typeText = GameLoader:GetGameText("LC_MENU_MONEY_CHAR")
  elseif key == "vip_exp" then
    typeText = GameLoader:GetGameText("LC_MENU_VIP_EXP_CHAR")
  elseif key == "laba_supply" then
    typeText = GameLoader:GetGameText("LC_MENU_DEXTER_COIN")
  elseif key == "technique" then
    typeText = GameLoader:GetGameText("LC_MENU_TECHNIQUE_CHAR")
  elseif key == "exp" then
    typeText = GameLoader:GetGameText("LC_MENU_EXP_CHAR")
  elseif key == "credit" then
    typeText = GameLoader:GetGameText("LC_MENU_LOOT_CREDIT")
  elseif key == "crystal" then
    typeText = GameLoader:GetGameText("LC_MENU_LOOT_CRYSTAL")
  elseif key == "prestige" then
    typeText = GameLoader:GetGameText("LC_MENU_PRESTIGE_CHAR")
  elseif key == "ac_supply" then
    typeText = GameLoader:GetGameText("LC_MENU_AC_SUPPLY_CHAR")
  elseif key == "wd_supply" then
    typeText = GameLoader:GetGameText("LC_MENU_WD_SUPPLY_CHAR")
  elseif key == "brick" then
    typeText = GameLoader:GetGameText("LC_MENU_BRICK_CHAR")
  elseif key == "lepton" then
    typeText = GameLoader:GetGameText("LC_MENU_LOOT_LEPTON")
  elseif key == "item" or key == "equip" then
    if key == "equip" then
      typeText = GameLoader:GetGameText("LC_ITEM_EQUIP_TYPE_SLOT" .. GameDataAccessHelper:GetEquipSlot(number))
    else
      typeText = GameLoader:GetGameText("LC_ITEM_ITEM_CHAR")
    end
    icon = "item_" .. number
  elseif key == "map_point" then
    typeText = GameLoader:GetGameText("LC_MENU_MAP_POINT")
  elseif key == "map_contribution" then
    typeText = GameLoader:GetGameText("LC_MENU_MAP_CONTRIBUTION")
  elseif key == "crusade_repair" then
    typeText = GameLoader:GetGameText("LC_MENU_TBC_REPAIR_ENERGY")
  else
    typeText = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. key)
  end
  return typeText, icon
end
function GameHelper:GetAwardText(key, number, no)
  if GameHelper:IsResource(key) then
    return GameHelper:GetAwardTypeText(key, -1) .. "x" .. number
  else
    return GameHelper:GetAwardTypeText(key, number) .. "x" .. no
  end
end
function GameHelper:GetItemText(key, number, no)
  if GameHelper:IsResource(key) then
    return number .. " " .. GameHelper:GetAwardTypeText(key, -1)
  else
    return no .. " " .. GameHelper:GetAwardTypeText(key, number)
  end
end
function GameHelper:GetAwardCount(key, number, no)
  if GameHelper:IsResource(key) then
    return number
  else
    return no
  end
end
function GameHelper:GetAwardCountWithX(key, number, no)
  if GameHelper:IsResource(key) then
    return "" .. number
  else
    return "x " .. no
  end
end
function GameHelper:GetAwardNameText(key, number)
  if GameHelper:IsResource(key) then
    return ...
  else
    return ...
  end
end
function GameHelper:getAwardNameTextAndCount(key, number, no)
  if GameHelper:IsResource(key) then
    return GameHelper:GetAwardTypeText(key, -1), number
  else
    return GameHelper:GetAwardTypeText(key, number), no
  end
end
function GameHelper:IsScratchGachaResource(key)
  if key == "space_pattern1" or key == "space_pattern2" or key == "space_pattern3" then
    return true
  else
    return false
  end
end
function GameHelper:IsResource(key)
  if key == "money" or key == "vip_exp" or key == "mine_digg_licence_one" or key == "mine_fight_licence_one" or key == "technique" or key == "exp" or key == "medal_resource" or key == "credit" or key == "expedition" or key == "crystal" or key == "prestige" or key == "quark" or key == "pve_supply" or key == "battle_supply" or key == "pvp_supply" or key == "lepton" or key == "kenergy" or key == "laba_supply" or key == "ac_supply" or key == "wd_supply" or key == "brick" or key == "silver" or key == "wdc_supply" or key == "tlc_supply" or key == "crusade_repair" or key == "art_point" or key == "space_pattern1" or key == "space_pattern2" or key == "space_pattern3" or key == "map_point" or key == "map_contribution" then
    return true
  else
    return false
  end
end
function GameHelper:GetAwardTypeText(key, number)
  local typeText = "UnknownAward"
  if key == "money" then
    typeText = GameLoader:GetGameText("LC_MENU_MONEY_CHAR")
  elseif key == "vip_exp" then
    typeText = GameLoader:GetGameText("LC_MENU_VIP_EXP_CHAR")
  elseif key == "mine_digg_licence_one" then
    typeText = GameLoader:GetGameText("LC_MENU_MINE_CHANCE_EVENT")
  elseif key == "brick" then
    typeText = GameLoader:GetGameText("LC_MENU_BRICK_CHAR")
  elseif key == "mine_fight_licence_one" then
    typeText = GameLoader:GetGameText("LC_MENU_PLUNDER_CHANCE_EVENT")
  elseif key == "technique" then
    typeText = GameLoader:GetGameText("LC_MENU_TECHNIQUE_CHAR")
  elseif key == "exp" then
    typeText = GameLoader:GetGameText("LC_MENU_EXP_CHAR")
  elseif key == "credit" then
    typeText = GameLoader:GetGameText("LC_MENU_LOOT_CREDIT")
  elseif key == "medal_resource" then
    typeText = GameLoader:GetGameText("LC_MENU_NAME_MEDAL_RESOURCE")
  elseif key == "crystal" then
    typeText = GameLoader:GetGameText("LC_MENU_LOOT_CRYSTAL")
  elseif key == "expedition" then
    typeText = GameLoader:GetGameText("LC_MENU_EXPEDITION_CHAR")
  elseif key == "prestige" then
    typeText = GameLoader:GetGameText("LC_MENU_PRESTIGE_CHAR")
  elseif key == "quark" then
    typeText = GameLoader:GetGameText("LC_MENU_QUARK_CHAR")
  elseif key == "pve_supply" or key == "battle_supply" then
    typeText = GameLoader:GetGameText("LC_MENU_SUPPLY_CHAR")
  elseif key == "pvp_supply" then
    typeText = GameLoader:GetGameText("LC_MENU_SUPPLY_CHAR")
  elseif key == "wd_supply" then
    typeText = GameLoader:GetGameText("LC_MENU_WD_SUPPLY_CHAR")
  elseif key == "lepton" then
    typeText = GameLoader:GetGameText("LC_MENU_LOOT_LEPTON")
  elseif key == "kenergy" then
    typeText = GameLoader:GetGameText("LC_MENU_krypton_energy")
  elseif key == "laba_supply" then
    typeText = GameLoader:GetGameText("LC_MENU_DEXTER_COIN")
  elseif key == "ac_supply" then
    typeText = GameLoader:GetGameText("LC_MENU_AC_SUPPLY_CHAR")
  elseif key == "crusade_repair" then
    typeText = GameLoader:GetGameText("LC_MENU_TBC_REPAIR_ENERGY")
  elseif key == "fleet" then
    typeText = GameLoader:GetGameText("LC_NPC_" .. GameDataAccessHelper:GetCommanderName(number))
  elseif key == "equip" then
    typeText = GameLoader:GetGameText("LC_ITEM_EQUIP_TYPE_SLOT" .. GameDataAccessHelper:GetEquipSlot(number))
  elseif key == "silver" or tonumber(number) == 99000004 then
    typeText = GameLoader:GetGameText("LC_MENU_GLC_STORE_MONEY")
  elseif key == "art_point" then
    typeText = GameLoader:GetGameText("LC_MENU_ARTIFACT_RESOURCE")
  elseif key == "medal_item" then
    typeText = GameLoader:GetGameText("LC_MENU_MEDAL_ITEM_NAME_" .. number)
  elseif key == "medal" then
    local id = math.ceil(number / 100000 - 1) * 100000 + number % 10
    DebugOut("GetMedal_Name:LC_MENU_MEDAL_NAME_" .. id)
    typeText = GameLoader:GetGameText("LC_MENU_MEDAL_NAME_" .. id)
  elseif key == "map_point" then
    typeText = GameLoader:GetGameText("LC_MENU_MAP_POINT")
  elseif key == "map_contribution" then
    typeText = GameLoader:GetGameText("LC_MENU_MAP_CONTRIBUTION")
  else
    typeText = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. number)
  end
  return typeText
end
function GameHelper:GetAwardTypeDesc(key, number)
  local typeText = "UnknownAward"
  if key == "medal" then
    local id = math.ceil(number / 100000 - 1) * 100000 + number % 10
    typeText = GameUtils:TryGetText("LC_MENU_MEDAL_DESC_" .. id)
  elseif key == "medal_item" then
    typeText = GameUtils:TryGetText("LC_MENU_MEDAL_ITEM_DESC_" .. number)
  elseif key == "item" then
    typeText = GameUtils:TryGetText("LC_ITEM_ITEM_DESC_" .. number)
  else
    print("warn GetAwardTypeDesc", key)
  end
  return typeText
end
function GameHelper:GetAwardTypeIconFrameNameSupportDynamic(gameItem, extInfo, downLoadCallBack)
  local frame = "temp"
  if DynamicResDownloader:IsDynamicStuffByGameItem(gameItem) then
    local fullFileName = DynamicResDownloader:GetFullName(gameItem.number .. ".png", DynamicResDownloader.resType.PIC)
    local localPath = "data2/" .. fullFileName
    DebugWD("localPath = ", localPath)
    if DynamicResDownloader:IfResExsit(localPath) then
      ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
      frame = "item_" .. gameItem.number
    else
      frame = "temp"
      self:AddDownloadPathForGameItem(gameItem.number, extInfo, downLoadCallBack)
    end
  elseif self:IsScratchGachaResource(gameItem.item_type) then
    if GameStateScratchGacha.act_id * 10 + 7 == GameSettingData.ScratchPreActivity then
      frame = gameItem.item_type
    else
      frame = "temp"
      DynamicResDownloader:AddDynamicRes(gameItem.item_type .. ".png", DynamicResDownloader.resType.PIC, extInfo, downLoadCallBack)
    end
  elseif gameItem.item_type == "fleet" then
    frame = GameDataAccessHelper:GetFleetAvatar(gameItem.number, 0)
  else
    frame = GameHelper:GetAwardTypeIconFrameName(gameItem.item_type, gameItem.number, gameItem.no)
  end
  return frame
end
function GameHelper:AddDownloadPathForGameItem(itemID, extInfo, callBack)
  local resName = itemID .. ".png"
  local aExtInfo = extInfo or {}
  aExtInfo.item_id = itemID
  aExtInfo.localPath = "data2/LAZY_LOAD_dynamic_" .. resName
  DynamicResDownloader:AddDynamicRes(resName, DynamicResDownloader.resType.PIC, aExtInfo, callBack)
end
function GameHelper:GetBackgroundImgDynamic(itemId, localImg, downloadImg, downCallback)
  local localPath = "data2/" .. DynamicResDownloader:GetFullName(downloadImg, DynamicResDownloader.resType.FESTIVAL_BANNER)
  DebugOut("localPath : " .. localPath)
  if DynamicResDownloader:IfResExsit(localPath) then
    ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
  else
    local downloadInfo = {}
    downloadInfo.itemId = itemId
    downloadInfo.localImg = localImg
    downloadInfo.localPath = downloadImg
    DynamicResDownloader:AddDynamicRes(downloadImg, DynamicResDownloader.resType.FESTIVAL_BANNER, downloadInfo, downCallback)
  end
end
function GameHelper:GetFoatColor(color, text)
  if not color then
    return text
  end
  local color_new = "#" .. color
  local cl = string.gsub("<font color='#'>", "#", color_new)
  return cl .. text .. "</font>"
end
function GameHelper:GetGameItemDownloadPng(key, number)
  if key == "equip" or key == "item" or key == "krypton" then
    return number .. ".png"
  elseif key == "medal" then
    local id = math.ceil(number / 100000 - 1) * 100000 + number % 10
    return id .. ".png"
  elseif key == "medal_item" then
    return number .. ".png"
  elseif key == "medal_new_icon" then
    local id = math.ceil(number / 100000 - 1) * 100000 + number % 10
    return id .. "_bg.png"
  elseif key == "fleet" then
    local heroBasic = FleetDataAccessHelper:GetFleetBasic(number, 0)
    return heroBasic.AVATAR .. ".png"
  else
    print("GetGameItemDownloadPng fail", key .. number)
    return "undefined"
  end
end
function GameHelper:GetAwardTypeIconFrameName(key, number, no)
  local iconText = "temp"
  if key == "money" then
    iconText = "money"
  elseif key == "vip_exp" then
    iconText = "vip_exp"
  elseif key == "brick" then
    iconText = "brick"
  elseif key == "technique" then
    iconText = "technique"
  elseif key == "exp" then
    iconText = "exp"
  elseif key == "credit" then
    iconText = "credit"
  elseif key == "expedition" then
    iconText = "expedition"
  elseif key == "crystal" then
    iconText = "crystal"
  elseif key == "prestige" then
    iconText = "prestige"
  elseif key == "quark" then
    iconText = "quark"
  elseif key == "pve_supply" or key == "battle_supply" then
    iconText = "pve_supply"
  elseif key == "wd_supply" then
    iconText = "wd_supply"
  elseif key == "lepton" then
    iconText = "lepton"
  elseif key == "kenergy" then
    iconText = "kenergy"
  elseif key == "laba_supply" then
    iconText = "laba_supply"
  elseif key == "ac_supply" then
    iconText = "ac_supply"
  elseif key == "silver" then
    iconText = "silver"
  elseif key == "art_point" then
    iconText = "art_point"
  elseif key == "medal_resource" then
    iconText = "medal_resource"
  elseif key == "fleet" then
    local heroBasic = FleetDataAccessHelper:GetFleetBasic(number, 0)
    iconText = heroBasic.AVATAR
  elseif key == "equip" or key == "item" or key == "krypton" then
    iconText = "item_" .. number
  elseif key == "medal" then
    local id = math.ceil(number / 100000 - 1) * 100000 + number % 10
    iconText = "medal_" .. id
  elseif key == "medal_new_icon" then
    local id = math.ceil(number / 100000 - 1) * 100000 + number % 10
    iconText = "medal_" .. id .. "_bg"
  elseif key == "medal_item" then
    iconText = "medal_item_" .. number
  elseif key == "map_point" then
    iconText = "map_point"
  elseif key == "map_contribution" then
    iconText = "map_contribution"
  end
  return iconText
end
function GameHelper:GetMedalIconFrameAndPic(type)
  local id = math.ceil(type / 100000 - 1) * 100000 + type % 10
  DebugOut("GetMedalIconFrame", type, id)
  return "medal_" .. id, id .. ".png"
end
function GameHelper:GetMedalIconFrameAndPicNew(type)
  local id = math.ceil(type / 100000 - 1) * 100000 + type % 10
  DebugOut("GetMedalIconFrame", type, id)
  return "medal_" .. id .. "_bg", id .. "_bg.png"
end
function GameHelper:GetCommonIconFrame(game_item, callBack)
  local frame = ""
  if DynamicResDownloader:IsDynamicStuffByGameItem(game_item) then
    local fullFileName = DynamicResDownloader:GetFullName(game_item.number .. ".png", DynamicResDownloader.resType.PIC)
    local localPath = "data2/" .. fullFileName
    DebugWD("localPath = ", localPath)
    if DynamicResDownloader:IfResExsit(localPath) then
      DebugOut("DynamicResDownloader:IfResExsit(localPath)) then")
      ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
      frame = "item_" .. game_item.number
    else
      DebugOut("DynamicResDownloader:IfResExsit(localPath)) then not")
      frame = "temp"
      GameHelper:AddDownloadPathForOfficer(game_item.number, callBack)
    end
  elseif game_item.item_type == "fleet" then
    frame = GameDataAccessHelper:GetFleetAvatar(game_item.number, 0)
  else
    frame = GameHelper:GetAwardTypeIconFrameName(game_item.item_type, game_item.number, game_item.no)
  end
  return frame
end
function GameHelper:GetMedalQuality(itemtype, nid)
  if itemtype == "medal" then
    return (...), nid / 100000, nil, nil, nil
  elseif itemtype == "medal_item" then
    local tquality = {
      [901] = 1,
      [902] = 2,
      [903] = 3,
      [9001] = 2,
      [9002] = 2
    }
    if tquality[nid] == nil then
      assert(false, "must have quality " .. nid)
    end
    return tquality[nid]
  else
    print("bad qua", itemtype)
  end
end
function GameHelper:AddDownloadPathForOfficer(itemID, callBack)
  local resName = itemID .. ".png"
  local extInfo = {}
  extInfo.item_id = itemID
  extInfo.localPath = "data2/LAZY_LOAD_dynamic_" .. resName
  DynamicResDownloader:AddDynamicRes(resName, DynamicResDownloader.resType.PIC, extInfo, callBack)
end
function GameHelper:GetRewardInfo(typeText, key, numberText, no)
  local info = GameLoader:GetGameText("LC_MENU_REWARD_INFO")
  info = string.gsub(info, "%[EVENT%]", GameLoader:GetGameText("LC_MENU_PRESTIGE_DAILY_REWARDS_TITLE"))
  local name, count = GameHelper:getAwardNameTextAndCount(key, numberText, no)
  info = string.gsub(info, "%[NUMBER%]", tostring(count))
  info = string.gsub(info, "%[TYPE%]", name)
  return info
end
function GameHelper:GetRewardInfoNew(typeText, numberText)
  local info = string.gsub(GameLoader:GetGameText("LC_MENU_REWARD_INFO"), "%[TYPE%]", typeText)
  info = string.gsub(info, "%[EVENT%]", GameLoader:GetGameText("LC_MENU_PRESTIGE_DAILY_REWARDS_TITLE"))
  info = string.gsub(info, "%[NUMBER%]", numberText)
  return info
end
function GameHelper:GetRewardInfoAndEvent(typeText, numberText, eventText)
  local info = string.gsub(GameLoader:GetGameText("LC_MENU_REWARD_INFO"), "%[TYPE%]", typeText)
  info = string.gsub(info, "%[EVENT%]", eventText)
  info = string.gsub(info, "%[NUMBER%]", numberText)
  return info
end
function GameHelper:ShowGetReward(quest)
  local rewards = quest.rewards
  local typeText = GameLoader:GetGameText("LC_MENU_" .. string.upper(rewards[1].item_type) .. "_CHAR")
  local numberText = rewards[1].number
  ItemBox:ShowRewardBox(rewards)
end
function GameHelper.GetQuestRewardCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.get_task_reward_req.Code then
      if content.code == 0 then
        local name = GameHelper:GetTabName(GameHelper.currentTab)
        GameHelper.questList[name].is_get_reward = true
        GameHelper.RefreshAll()
        GameHelper:ShowGetReward(GameHelper.questList[name], true)
      else
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
    end
    return true
  end
  return false
end
function GameHelper.GetTodayQuestRewardCallback(msgType, content)
  if msgType == NetAPIList.get_today_award_ack.Code then
    if content.code == 2 then
      if GameHelper.rewardAll then
        GameHelper.todayList.task.is_get_reward = true
      else
        local subQuest
        for tk, tv in pairs(GameHelper.todayList.task.task_content) do
          if tv.id == GameHelper.currentSubQuestKey then
            subQuest = tv
            break
          end
        end
        subQuest.is_get_reward = true
      end
      GameHelper.RefreshAll()
    end
    return true
  end
  return false
end
function GameHelper.GetSubQuestRewardCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.get_task_reward_req.Code then
      if content.code == 0 then
        local subQuest
        for k, v in pairs(GameHelper.questList) do
          for tk, tv in pairs(v.task_content) do
            if tv.id == GameHelper.currentSubQuestKey then
              subQuest = GameHelper.questList[k].task_content[tk]
              break
            end
          end
        end
        subQuest.is_get_reward = true
        GameHelper.RefreshAll()
        GameHelper:ShowGetReward(subQuest, false)
      else
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
    end
    return true
  end
  return false
end
function GameHelper:StartUpABattle(battleReport, awards, battleResultType, battleFinishCallback)
  local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
  local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
  DebugOut("battleResultType = ", battleResultType)
  GameUIBattleResult:LoadFlashObject()
  local result = 1
  if battleReport.player1 == GameGlobalData:GetUserInfo().name then
    if battleReport.result == 1 then
      result = 1
    else
      result = 2
    end
  elseif battleReport.player2 == GameGlobalData:GetUserInfo().name then
    if battleReport.result == 2 then
      result = 1
    else
      result = 2
    end
  end
  local windowType = GameHelper.BattleResultType[battleResultType][result]
  DebugOut("windowType = ", windowType)
  local awardDataTable = {}
  if result == 1 then
    local index = 1
    GameUIBattleResult:UpdateAwardItem3("challenge_win", 2, -1, 0, 0)
    for indexAward, dataAward in ipairs(awards) do
      DebugOut("dataAward = ")
      DebugTable(dataAward)
      GameUIBattleResult:UpdateAwardItem3("challenge_win", 2, -1, 0, 0)
      if dataAward.no ~= 0 then
        GameUIBattleResult:UpdateAwardItem3("challenge_win", index, dataAward.item_type, dataAward.no, dataAward.number)
        index = index + 1
      end
    end
  end
  if #battleReport.rounds == 0 and GameStateManager:GetCurrentGameState().fightRoundData then
    battleReport.rounds = GameStateManager:GetCurrentGameState().fightRoundData
  end
  GameUIBattleResult:SetFightReport(battleReport, nil, nil)
  local lastGameState = GameStateManager:GetCurrentGameState()
  GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_HISTORY, battleReport)
  GameStateBattlePlay:RegisterOverCallback(function()
    GameStateManager:SetCurrentGameState(lastGameState)
    if battleFinishCallback then
      battleFinishCallback()
    end
    GameUIBattleResult:LoadFlashObject()
    GameUIBattleResult:AnimationMoveIn(windowType, false)
    lastGameState:AddObject(GameUIBattleResult)
  end)
  GameStateManager:SetCurrentGameState(GameStateBattlePlay)
end
if AutoUpdate.isAndroidDevice then
  function GameHelper.OnAndroidBack()
    GameHelper:GetFlashObject():InvokeASCallback("_root", "closeWindow")
  end
end
function GameHelper:DownloadResIfNeed(typeId, callback)
  DebugOut("DownloadResIfNeed " .. tostring(typeId))
  local isNeedDownload = false
  if DynamicResDownloader:IsDynamicStuff(typeId, DynamicResDownloader.resType.PIC) then
    local fullFileName = DynamicResDownloader:GetFullName(tostring(typeId) .. ".png", DynamicResDownloader.resType.PIC)
    local localPath = "data2/" .. fullFileName
    DebugOut("localPath = ", localPath)
    if DynamicResDownloader:IfResExsit(localPath) then
      DebugOut("DynamicResDownloader:IfResExsit(localPath)) then")
      ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
    else
      DebugOut("DynamicResDownloader:IfResExsit(localPath)) then not")
      GameHelper:AddDownloadPathForOfficer(typeId, callback)
      isNeedDownload = true
    end
  end
  return isNeedDownload
end
function GameHelper:GetFleetsForce()
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local InMatrix = GameGlobalData:GetData("matrix").cells
  local newForce = 0
  for i, fleet in pairs(fleets) do
    if GameUtils:IsInMatrix(fleet.identity) then
      newForce = newForce + fleet.force
    end
  end
  DebugOut("newForce:" .. newForce)
  return newForce
end
function GameHelper:IsHaveFleetById(id)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  for i, fleet in ipairs(fleets) do
    if id == fleet.identity then
      return true
    end
  end
  return false
end
function GameHelper:GetCurrentMatrixFleetAvatarAndRank(cur_index)
  local curMatrix = FleetMatrix.matrixs[cur_index].cells
  DebugOut("curMatrix = ")
  DebugTable(curMatrix)
  local curMatrixAvatar = {}
  if curMatrix and #curMatrix > 0 then
    for k, v in pairs(curMatrix) do
      if 0 < v.fleet_identity then
        local avatarT = {}
        DebugOut("fleet_identity = " .. v.fleet_identity)
        local fleetDetail = GameGlobalData:GetFleetInfo(v.fleet_identity)
        DebugTable(fleetDetail)
        local leaderlist = GameGlobalData:GetData("leaderlist")
        local fleetID = v.fleet_identity
        if v.fleet_identity == 1 and leaderlist then
          avatarT.fleetAvatar = GameDataAccessHelper:GetFleetAvatar(leaderlist.leader_ids[cur_index])
          fleetID = leaderlist.leader_ids[cur_index]
        else
          avatarT.fleetAvatar = GameDataAccessHelper:GetFleetAvatar(v.fleet_identity)
        end
        avatarT.rankid = GameDataAccessHelper:GetCommanderColorFrame(fleetID, fleetDetail.level)
        table.insert(curMatrixAvatar, avatarT)
      end
    end
  end
  DebugOut("curMatrixAvatar = ")
  DebugTable(curMatrixAvatar)
  return curMatrixAvatar
end
function GameHelper.OnHalfPortraitResDowanloaded(extInfo)
  GameUIHalfPortrait:PreLoadRes(extInfo.src .. ".png")
end
function GameHelper:CheckHalfPortraitImg(image)
  local fullFileName = DynamicResDownloader:GetFullName(image, DynamicResDownloader.resType.HALFPORTRAIT)
  local localPath = "data2/" .. fullFileName .. ".tga"
  GameUtils:printByAndroid("GameUIHalfPortrait:CheckHalfPortraitImg:" .. localPath .. "," .. ext.crc32.crc32(localPath))
  if DynamicResDownloader:IfResExsit(localPath) then
    ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
    return true
  elseif ext.crc32.crc32(localPath) ~= "" then
    ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
    return true
  else
    local extInfo = {}
    extInfo.src = image
    DynamicResDownloader:AddDynamicRes(image .. ".tga", DynamicResDownloader.resType.HALFPORTRAIT, extInfo, GameHelper.OnHalfPortraitResDowanloaded)
    DebugOut("GameHelper:CheckHalfPortraitImg", localPath, "not Loaded")
    return false
  end
end
function GameHelper:CheckHalfPortraitImgGroup(Group)
  local retValue = true
  local imageGroup = HalfPortraitResource.classify[Group]
  DebugOut("GameHelper:CheckHalfPortraitImgGroup", Group)
  DebugTable(imageGroup)
  for k, v in ipairs(imageGroup) do
    retValue = self:CheckHalfPortraitImg(v) and retValue
  end
  return retValue
end
function GameHelper:CheckHalfPortraitResourceLoaded(HalfPortraitType)
  if GameHelper.HalfPortraitTable[HalfPortraitType] == true then
    return true
  end
  local isAllLoaded = false
  if HalfPortraitType == 1 then
    isAllLoaded = self:CheckHalfPortraitImgGroup(1)
  elseif HalfPortraitType == 2 then
    isAllLoaded = self:CheckHalfPortraitImgGroup(2)
  elseif HalfPortraitType == 3 then
    isAllLoaded = self:CheckHalfPortraitImgGroup(4)
  elseif HalfPortraitType == 4 then
    local isLoaded1 = self:CheckHalfPortraitImgGroup(3)
    local isLoaded2 = self:CheckHalfPortraitImgGroup(5)
    local isLoaded3 = self:CheckHalfPortraitImgGroup(7)
    isAllLoaded = isLoaded1 and isLoaded2 and isLoaded3
  elseif HalfPortraitType == 6 then
    isAllLoaded = self:CheckHalfPortraitImgGroup(5)
  elseif HalfPortraitType == 7 then
    local isLoaded1 = self:CheckHalfPortraitImgGroup(5)
    local isLoaded2 = self:CheckHalfPortraitImgGroup(7)
    isAllLoaded = isLoaded1 and isLoaded2
  end
  local idLoaded = self:CheckHalfPortraitImgGroup(8)
  isAllLoaded = isAllLoaded and idLoaded
  GameHelper.HalfPortraitTable[HalfPortraitType] = isAllLoaded
  DebugOut("GameHelper:CheckHalfPortraitResourceLoaded", HalfPortraitType, isAllLoaded)
  return isAllLoaded
end
function GameHelper:CheckHalfPortraitAvataAndShip(fleetAbility)
  if GameHelper.HalfPortraitHeroResourceTable[fleetAbility.ID] == true then
    return true
  end
  local avata = self:GetHalfPortratiAvatImage(fleetAbility.AVATAR)
  local isShipLoaded = self:CheckHalfPortraitImg("LAZY_LOAD_DOWN_halfProtrait_" .. fleetAbility.SHIP)
  local isHeadLoaded = self:CheckHalfPortraitImg(avata)
  GameHelper.HalfPortraitHeroResourceTable[fleetAbility.ID] = isShipLoaded and isHeadLoaded
  return isShipLoaded and isHeadLoaded
end
function GameHelper:GetHalfPortratiAvatImage(avata)
  if avata == "head102" then
    return "LAZY_LOAD_DOWN_avater_fire_102"
  elseif avata == "head21" then
    return "LAZY_LOAD_DOWN_avata_256_256_headDuoDuo"
  else
    return "LAZY_LOAD_DOWN_avata_256_256_" .. avata
  end
end
function GameHelper:PreLoadHalfPortraitRes(HalfPortraitType, fleetAbility)
  GameUIHalfPortrait:PreLoadRes(self:GetHalfPortratiAvatImage(fleetAbility.AVATAR) .. ".png")
  GameUIHalfPortrait:PreLoadRes("LAZY_LOAD_DOWN_halfProtrait_" .. fleetAbility.SHIP .. ".png")
  if HalfPortraitType == 1 then
    isAllLoaded = self:PreLoadHalfPortraitGroupRes(1)
  elseif HalfPortraitType == 2 then
    isAllLoaded = self:PreLoadHalfPortraitGroupRes(2)
  elseif HalfPortraitType == 3 then
    isAllLoaded = self:PreLoadHalfPortraitGroupRes(4)
  elseif HalfPortraitType == 4 then
    local isLoaded1 = self:PreLoadHalfPortraitGroupRes(3)
    local isLoaded2 = self:PreLoadHalfPortraitGroupRes(5)
    local isLoaded3 = self:PreLoadHalfPortraitGroupRes(7)
    isAllLoaded = isLoaded1 and isLoaded2 and isLoaded3
  elseif HalfPortraitType == 6 then
    isAllLoaded = self:PreLoadHalfPortraitGroupRes(5)
  elseif HalfPortraitType == 7 then
    local isLoaded1 = self:PreLoadHalfPortraitGroupRes(5)
    local isLoaded2 = self:PreLoadHalfPortraitGroupRes(7)
    isAllLoaded = isLoaded1 and isLoaded2
  end
end
function GameHelper:PreLoadHalfPortraitGroupRes(groupId)
  local imageGroup = HalfPortraitResource.classify[groupId]
  for k, v in ipairs(imageGroup) do
    GameUIHalfPortrait:PreLoadRes(v .. ".png")
  end
end
function GameHelper:SetPrestigeTutorialFinish()
  QuestTutorialPrestige:SetFinish(true)
end
function GameHelper:RankUpPrstige(arg)
end
function GameHelper:UpdatePrestigeItem(index_item)
  local data_prestige = self:GetPrestigeInfo(index_item)
  local prev_prestige_data = self:GetPrestigeInfo(index_item - 1)
  local award_data = GameDataAccessHelper:GenerateMilitaryRankAwardInfo(data_prestige.prestige_level)
  local canNextLevelTxt = GameLoader:GetGameText("LC_MENU_PRESTIGE_UPGRADE_CHAR")
  local visible = false
  DebugOut("award_data - ", award_data)
  local prestige_item_param = ""
  prestige_item_param = prestige_item_param .. tostring(data_prestige.prestige_level)
  prestige_item_param = prestige_item_param .. "," .. GameDataAccessHelper:GetMilitaryRankName(data_prestige.prestige_level)
  prestige_item_param = prestige_item_param .. "," .. self:CheckPrestigeStatus(data_prestige)
  prestige_item_param = prestige_item_param .. "," .. tostring(self.PrestigeData.prestige)
  prestige_item_param = prestige_item_param .. "," .. tostring(data_prestige.prestige_require)
  prestige_item_param = prestige_item_param .. "," .. tostring(GameUtils.numberConversion(self.PrestigeData.prestige))
  prestige_item_param = prestige_item_param .. "," .. tostring(GameUtils.numberConversion(data_prestige.prestige_require))
  prestige_item_param = prestige_item_param .. "," .. award_data
  local daily_award = GameDataAccessHelper:GenerateMilitaryRankDailyAwardInfo(data_prestige.prestige_level)
  local title = GameLoader:GetGameText("LC_MENU_PRESTIGE_DAILY_REWARDS_TITLE")
  DebugOut("daily_award - ", daily_award, data_prestige.prestige_level)
  self:GetFlashObject():InvokeASCallback("_root", "UpdatePrestigeItem", index_item, prestige_item_param, canNextLevelTxt, QuestTutorialPrestige:IsActive(), tonumber(immanentversion), daily_award, title)
end
function GameHelper:ObtainAward(level)
  if immanentversion == 1 then
    local function msgCallback(msgtype, content)
      if msgtype == NetAPIList.prestige_obtain_award_ack.Code then
        local data_prestige = GameHelper:GetPrestigeInfo(GameHelper._ObtainLevel)
        data_prestige.obtained = true
        GameHelper:UpdatePrestigeItem(data_prestige.prestige_level)
        return true
      elseif msgtype == 0 and content.api == NetAPIList.prestige_obtain_award_req.Code then
        if content.code ~= 0 then
          GameUIGlobalScreen:ShowAlert("error", content.code, nil)
        end
        return true
      end
      return false
    end
    self._ObtainLevel = level
    local packet = {prestige_level = level}
    NetMessageMgr:SendMsg(NetAPIList.prestige_obtain_award_req.Code, packet, msgCallback, true, nil)
  end
end
function GameHelper:GetPrestigeInfo(level)
  for _, v in ipairs(self.PrestigeData.prestige_infos) do
    if v.prestige_level == level then
      return v
    end
  end
  return nil
end
function GameHelper:CheckPrestigeStatus(prestige_info)
  local prestige_status = "unfinished"
  if self.PrestigeData.cur_level == prestige_info.prestige_level - 1 then
    prestige_status = "active"
  elseif prestige_info.prestige_require < self.PrestigeData.prestige then
    if prestige_info.obtained then
      prestige_status = "received"
    else
      prestige_status = "unreceived"
    end
  end
  return prestige_status
end
function GameHelper:GetItemInfo(v)
  assert(v.item_type)
  assert(v.number)
  assert(v.no)
  local t = {}
  t.icon_frame = GameHelper:GetAwardTypeIconFrameName(v.item_type, v.number)
  t.icon_pic = GameHelper:GetGameItemDownloadPng(v.item_type, v.number)
  if GameHelper:IsResource(v.item_type) then
    t.cnt = v.number
  else
    t.cnt = v.no
  end
  t.name = GameHelper:GetAwardTypeText(v.item_type, v.number)
  t.desc = GameHelper:GetAwardTypeDesc(v.item_type, v.number)
  if v.item_type == "medal" or v.item_type == "medal_item" then
    t.quality = GameHelper:GetMedalQuality(v.item_type, v.number)
    t.isExt = false
  else
    t.quality = 0
    t.isExt = true
  end
  return t
end
function GameHelper:GenerateGCSTaskData(content)
  GameHelper.GCSTaskData = content
end
function GameHelper:GenerateGCSAwardsData(content)
  local data = content
  data.text_titleLevel = GameLoader:GetGameText("LC_MENU_GCS_ChallengeLV_CHAR")
  data.text_titlePoints = GameLoader:GetGameText("LC_MENU_GCS_ChallengePoint_CHAR")
  data.text_curPoints = data.cur_points .. "/" .. data.lv_up_need
  data.text_curSeason = string.gsub(GameLoader:GetGameText("LC_MENU_GCS_Seasons_CHAR"), "<s>", data.cur_season)
  data.max_lv = #data.lv_awards_list
  if data.cur_lv < data.max_lv then
    data.text_nextLevel = data.cur_lv + 1
  else
    data.text_nextLevel = ""
  end
  if data.season_left_time < 0 then
    data.season_left_time = 0
  end
  local leftTimeStr = GameUtils:formatTimeStringAsPartion2(data.season_left_time)
  data.text_seasonLeft = GameLoader:GetGameText("LC_MENU_GCS_SeasonEndIn_CHAR") .. leftTimeStr
  data.text_freeGridTitle = GameLoader:GetGameText("LC_MENU_GCS_SeasonFreeChallengeReward_CHAR")
  data.text_paidGridTitle = GameLoader:GetGameText("LC_MENU_GCS_SeasonPaidChallengeReward_CHAR")
  data.text_btn_buy_card = GameLoader:GetGameText("LC_MENU_GCS_BuySeasonPass_BTN")
  data.text_btn_buy_level = GameLoader:GetGameText("LC_MENU_GCS_BuyChallengeLV_BTN")
  data.text_desc_buy_card = GameLoader:GetGameText("LC_MENU_GCS_BuySeasonPass_Info_CHAR")
  data.text_desc_buy_level = GameLoader:GetGameText("LC_MENU_GCS_BuyChallengeLV_Info_CHAR")
  data.text_btn_all_reward_claim = GameLoader:GetGameText("LC_MENU_GCS_AllRewardClaim_BTN")
  data.text_btn_all_reward_claimed = GameLoader:GetGameText("LC_MENU_GCS_AllRewardClaimed_BTN")
  data.left_time_base = os.time()
  GameHelper:ProcessGCSAwardsLvGridData(data)
  GameHelper.GCSAwardsData = data
end
function GameHelper:ProcessGCSAwardsLvGridData(data)
  data.hasRewardToClaim = false
  for _, k in ipairs(data.lv_awards_list) do
    for _, v in ipairs(k.free_awards) do
      v.displayFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v, nil, nil)
      v.displayCount = GameUtils.numberConversion(tonumber(GameHelper:GetAwardCount(v.item_type, v.number, v.no)))
    end
    for _, j in ipairs(k.paid_awards) do
      j.displayFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(j, nil, nil)
      j.displayCount = GameUtils.numberConversion(tonumber(GameHelper:GetAwardCount(j.item_type, j.number, j.no)))
      if j.item_type == "fleet" then
        local fleetData = FleetDataAccessHelper:GetFleetBasic(j.number, 0)
        j.fleet_head = FleetDataAccessHelper:GetFleetAvatarFrameByAvatar(fleetData.AVATAR, j.number)
        j.fleet_ship = GameDataAccessHelper:GetCommanderShipByDownloadState(fleetData.SHIP)
        local ability = GameDataAccessHelper:GetCommanderAbility(j.number, 0)
        local fleetRank = GameUtils:GetFleetRankFrame(ability.Rank, nil)
        j.fleet_rank = fleetRank
      end
    end
    if k.lv <= data.cur_lv and 0 < LuaUtils:table_size(k.free_awards) and k.free_received == false then
      data.hasRewardToClaim = true
    end
    if k.lv <= data.cur_lv and 0 < LuaUtils:table_size(k.paid_awards) and data.paid_card == true and k.paid_received == false then
      data.hasRewardToClaim = true
    end
  end
end
function GameHelper:OnUpdateGCSWeekBtnItem(index)
  if GameHelper.GCSTaskData ~= nil and self:GetFlashObject() ~= nil then
    if GameHelper.GCSTaskData.today ~= nil and index == 0 then
      local btnData = {}
      btnData.type = 0
      btnData.txt = GameLoader:GetGameText("LC_MENU_GCS_DailyTask_BTN")
      btnData.locked = false
      btnData.red_point = GameHelper:IsGCSWeekBtnRedPoint(0)
      self:GetFlashObject():InvokeASCallback("_root", "UpdateGCSWeekBtnItem", index, btnData)
    end
    if index > 0 and GameHelper.GCSTaskData.weeks ~= nil and self:GetFlashObject() ~= nil then
      local btnData = {}
      btnData.type = 1
      btnData.txt = string.gsub(GameLoader:GetGameText("LC_MENU_GCS_WeeklyTask_BTN"), "<s>", index)
      for k, v in ipairs(GameHelper.GCSTaskData.weeks) do
        if index == k then
          if 0 >= v.unlock_after then
            btnData.locked = false
          else
            btnData.locked = true
          end
        end
      end
      btnData.red_point = GameHelper:IsGCSWeekBtnRedPoint(index)
      DebugOutPutTable(btnData, "btnData === ")
      self:GetFlashObject():InvokeASCallback("_root", "UpdateGCSWeekBtnItem", index, btnData)
    end
  end
end
GameHelper.GCS_cur_click_week = -1
function GameHelper:OnGCSClickWeekBtn(index)
  if GameHelper.GCSTaskData ~= nil and self:GetFlashObject() ~= nil then
    self:GetFlashObject():InvokeASCallback("_root", "GCS_Select_Week_btn", index)
    if GameHelper.GCSTaskData.today ~= nil and index == 0 then
      GameHelper.GCS_cur_click_week = 0
      local todayData = GameHelper.GCSTaskData.today
      todayData.desc_text = GameLoader:GetGameText("LC_MENU_GCS_DailyTask_Info_CHAR")
      DebugOut("GCS_ShowTodayTask ==== ")
      self:GetFlashObject():InvokeASCallback("_root", "GCS_ShowTodayTask", todayData)
    end
    if GameHelper.GCSTaskData.weeks ~= nil and index > 0 then
      local weekData = GameHelper.GCSTaskData.weeks[index]
      GameHelper.GCS_cur_click_week = index
      weekData.locked = false
      if 0 < weekData.unlock_after then
        weekData.locked = true
      end
      if weekData.locked then
        weekData.desc_text = string.gsub(GameLoader:GetGameText("LC_MENU_GCS_WeeklyTask_DaysUnlock_CHAR"), "<s>", weekData.unlock_after)
      else
        weekData.desc_text = GameLoader:GetGameText("LC_MENU_GCS_WeeklyTask_ExtraReward_Info_CHAR")
      end
      if weekData.extra_award ~= nil then
        weekData.extra_award.displayFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(weekData.extra_award, nil, nil)
        weekData.extra_award.displayCount = GameUtils.numberConversion(tonumber(GameHelper:GetAwardCount(weekData.extra_award.item_type, weekData.extra_award.number, weekData.extra_award.no)))
        weekData.extra_claimedText = GameLoader:GetGameText("LC_MENU_CLAIMED_BUTTON")
      end
      DebugOutPutTable(weekData, "GCS_ShowWeekTask ===")
      self:GetFlashObject():InvokeASCallback("_root", "GCS_ShowWeekTask", weekData)
    end
  end
end
function GameHelper:OnGCSTodayTaskItem(index)
  if GameHelper.GCSTaskData ~= nil and self:GetFlashObject() ~= nil and GameHelper.GCSTaskData.today ~= nil then
    local taskData = GameHelper.GCSTaskData.today.tasks[index]
    taskData.desc_text = GameHelper:GetGCSTaskDataDescText(taskData)
    taskData.reward_btn_text = ""
    if taskData.status == 1 then
      taskData.reward_btn_text = GameLoader:GetGameText("LC_MENU_GCS_TaskRewardClaim_BTN")
    elseif taskData.status == 2 then
      if GameHelper.GCSTaskData.today.change_left > 0 then
        taskData.reward_btn_text = GameLoader:GetGameText("LC_MENU_GCS_TaskChange_BTN")
        taskData.canChange = true
      else
        taskData.reward_btn_text = GameLoader:GetGameText("LC_MENU_GCS_TaskRewardClaim_BTN")
      end
    elseif taskData.status == 3 then
      taskData.reward_btn_text = GameLoader:GetGameText("LC_MENU_GCS_TaskClaimed_BTN")
    end
    for _, v in ipairs(taskData.awards) do
      v.displayFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v, nil, nil)
      v.displayCount = GameUtils.numberConversion(tonumber(GameHelper:GetAwardCount(v.item_type, v.number, v.no)))
    end
    self:GetFlashObject():InvokeASCallback("_root", "UpdateGCSTodayTaskItem", index, taskData)
  end
end
function GameHelper:OnGCSWeekTaskItem(index)
  DebugOut("GameHelper:OnGCSWeekTaskItem 1. ===" .. index)
  DebugOut("GameHelper:OnGCSWeekTaskItem 2. ===" .. GameHelper.GCS_cur_click_week)
  if GameHelper.GCSTaskData ~= nil and GameHelper.GCSTaskData.weeks ~= nil and self:GetFlashObject() ~= nil and GameHelper.GCSTaskData.weeks[GameHelper.GCS_cur_click_week] ~= nil then
    local weekData = GameHelper.GCSTaskData.weeks[GameHelper.GCS_cur_click_week]
    local freeTitleIndex = 1
    local paidTitleIndex = freeTitleIndex + #weekData.free_tasks + 1
    weekData.locked = false
    if weekData.unlock_after > 0 then
      weekData.locked = true
    end
    if index == freeTitleIndex then
      local titleData = {}
      titleData.title_text = GameLoader:GetGameText("LC_MENU_GCS_WeeklyTask_FreeTask_CHAR")
      titleData.type = 0
      titleData.locked = weekData.locked
      self:GetFlashObject():InvokeASCallback("_root", "UpdateGCSWeekTaskTitleItem", index, titleData)
    elseif index > freeTitleIndex and index < paidTitleIndex then
      local real_index = index - freeTitleIndex
      local taskData = weekData.free_tasks[real_index]
      taskData.locked = weekData.locked
      taskData.desc_text = GameHelper:GetGCSTaskDataDescText(taskData)
      taskData.reward_btn_text = ""
      if taskData.status == 1 then
        taskData.reward_btn_text = GameLoader:GetGameText("LC_MENU_GCS_TaskRewardClaim_BTN")
      elseif taskData.status == 2 then
        taskData.reward_btn_text = GameLoader:GetGameText("LC_MENU_GCS_TaskRewardClaim_BTN")
      elseif taskData.status == 3 then
        taskData.reward_btn_text = GameLoader:GetGameText("LC_MENU_GCS_TaskClaimed_BTN")
      end
      local v
      for _, v in ipairs(taskData.awards) do
        v.displayFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v, nil, nil)
        v.displayCount = GameUtils.numberConversion(tonumber(GameHelper:GetAwardCount(v.item_type, v.number, v.no)))
      end
      self:GetFlashObject():InvokeASCallback("_root", "UpdateGCSWeekTaskItem", index, taskData)
    elseif index == paidTitleIndex then
      local titleData = {}
      titleData.title_text = GameLoader:GetGameText("LC_MENU_GCS_WeeklyTask_PaidTask_CHAR")
      titleData.type = 1
      if GameHelper.GCSTaskData.paid_card == true then
        titleData.locked = weekData.locked
      else
        titleData.locked = true
      end
      self:GetFlashObject():InvokeASCallback("_root", "UpdateGCSWeekTaskTitleItem", index, titleData)
    elseif index > paidTitleIndex then
      local real_index = index - paidTitleIndex
      local taskData = weekData.paid_tasks[real_index]
      if GameHelper.GCSTaskData.paid_card == true then
        taskData.locked = weekData.locked
      else
        taskData.locked = true
      end
      if GameHelper.GCSTaskData.paid_card == false then
        taskData.can_buy_card = true
      end
      taskData.desc_text = GameHelper:GetGCSTaskDataDescText(taskData)
      taskData.reward_btn_text = ""
      if taskData.status == 1 then
        taskData.reward_btn_text = GameLoader:GetGameText("LC_MENU_GCS_TaskRewardClaim_BTN")
      elseif taskData.status == 2 then
        if taskData.can_buy_card == true then
          taskData.reward_btn_text = GameLoader:GetGameText("LC_MENU_GCS_UnlockSeason_BTN")
        else
          taskData.reward_btn_text = GameLoader:GetGameText("LC_MENU_GCS_TaskRewardClaim_BTN")
        end
      elseif taskData.status == 3 then
        taskData.reward_btn_text = GameLoader:GetGameText("LC_MENU_GCS_TaskClaimed_BTN")
      end
      for _, v in ipairs(taskData.awards) do
        v.displayFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v, nil, nil)
        v.displayCount = GameUtils.numberConversion(tonumber(GameHelper:GetAwardCount(v.item_type, v.number, v.no)))
      end
      DebugOut("UpdateGCSWeekTaskItem ~~~" .. index)
      DebugOutPutTable(taskData, "UpdateGCSWeekTaskItem ~~~")
      self:GetFlashObject():InvokeASCallback("_root", "UpdateGCSWeekTaskItem", index, taskData)
    end
  end
end
function GameHelper:ShowGCS_Task()
  if self:GetFlashObject() ~= nil then
    self:GetFlashObject():InvokeASCallback("_root", "GCS_Select_Tab", 1)
    GameHelper.CurSelGCSTab = 1
    GameHelper:GetFlashObject():InvokeASCallback("_root", "showGCSTask", GameHelper.GCSTaskData)
    GameHelper:OnGCSClickWeekBtn(0)
  end
end
function GameHelper:ShowGCS_Reward()
  if self:GetFlashObject() ~= nil then
    self:GetFlashObject():InvokeASCallback("_root", "GCS_Select_Tab", 2)
    GameHelper.CurSelGCSTab = 2
    local page = GameHelper:CalculateGCSAwardsCurPage()
    self:GetFlashObject():InvokeASCallback("_root", "showGCSAwards", GameHelper.GCSAwardsData, page)
  end
end
function GameHelper:OnGCSClickTab(tab)
  if self:GetFlashObject() ~= nil then
    if tab == 1 then
      GameHelper:ShowGCS_Task()
    elseif tab == 2 then
      GameHelper:ShowGCS_Reward()
    end
  end
end
function GameHelper:OnGCSClickAwardsPrePage()
  if self:GetFlashObject() ~= nil and GameHelper.GCSAwardsData ~= nil then
    self:GetFlashObject():InvokeASCallback("_root", "showGCSAwardsPage_Pre", GameHelper.GCSAwardsData)
  end
end
function GameHelper:OnGCSClickAwardsNextPage()
  if self:GetFlashObject() ~= nil and GameHelper.GCSAwardsData ~= nil then
    self:GetFlashObject():InvokeASCallback("_root", "showGCSAwardsPage_Next", GameHelper.GCSAwardsData)
  end
end
function GameHelper:OnGCSClickGetTaskReward(itemKey, task_id)
  DebugOut("GameHelper:OnGCSClickGetTaskReward -- " .. task_id)
  if GameHelper.GCS_cur_click_week == 0 then
    GameHelper:RequestGetTodayTaskReward(task_id)
  elseif GameHelper.GCS_cur_click_week > 0 then
    GameHelper:RequestGetWeekTaskReward(GameHelper.GCS_cur_click_week, task_id, itemKey)
  end
end
function GameHelper:RequestGetTodayTaskReward(task_id)
  local function msgCallBack(msgType, content)
    if msgType == NetAPIList.gcs_get_today_task_reward_ack.Code then
      DebugOutPutTable(content, "GameHelper:RequestGetTodayTaskReward--")
      if content.today ~= nil then
        GameHelper.GCSTaskData.today = content.today
        GameHelper:OnGCSClickWeekBtn(0)
        GameHelper:RefreshGCSRedPoint()
      end
      return true
    elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.gcs_get_today_task_reward_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    end
    return false
  end
  local req_param = {}
  req_param.task_id = task_id
  NetMessageMgr:SendMsg(NetAPIList.gcs_get_today_task_reward_req.Code, req_param, msgCallBack, true, nil)
end
function GameHelper:RequestGetWeekTaskReward(week_index, task_id, refreshItemKey)
  local function msgCallBack(msgType, content)
    if msgType == NetAPIList.gcs_get_week_task_reward_ack.Code then
      DebugOutPutTable(content, "GameHelper:RequestGetWeekTaskReward--")
      if content.week_info ~= nil then
        GameHelper.GCSTaskData.weeks[week_index] = content.week_info
        GameHelper:OnGCSClickWeekBtn(week_index)
        GameHelper:RefreshGCSRedPoint()
      end
      return true
    end
    return false
  end
  local req_param = {}
  req_param.task_id = task_id
  req_param.week_id = GameHelper.GCSTaskData.weeks[week_index].week_id
  NetMessageMgr:SendMsg(NetAPIList.gcs_get_week_task_reward_req.Code, req_param, msgCallBack, true, nil)
end
function GameHelper:OnGCSClickChangeTask(task_id)
  local function msgCallBack(msgType, content)
    if msgType == NetAPIList.gcs_today_task_change_ack.Code then
      DebugOutPutTable(content, "GameHelper:OnGCSClickChangeTask--1")
      if content.today ~= nil then
        GameHelper.GCSTaskData.today = content.today
        GameHelper:OnGCSClickWeekBtn(0)
      end
      return true
    elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.gcs_today_task_change_req.Code then
      DebugOutPutTable(content, "GameHelper:OnGCSClickChangeTask--2")
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    end
    return false
  end
  local function okCallback()
    local param = {}
    param.task_id = task_id
    NetMessageMgr:SendMsg(NetAPIList.gcs_today_task_change_req.Code, param, msgCallBack, true, nil)
  end
  local tip = GameLoader:GetGameText("LC_MENU_GCS_TaskChange_Tips_CHAR")
  GameUIGlobalScreen:ShowMessageBox(2, "", tip, okCallback, nil)
end
function GameHelper:OnGCSClickBuyCardBtn()
  GameHelper:RequestGCSBuyCardData(function()
    GameHelper:ShowGCSBuyCardPop()
  end)
end
function GameHelper:OnGCSClickBuyLevelBtn()
  GameHelper:RequestGCSBuyLevelData(function()
    GameHelper:ShowGCSBuyLevelPop()
  end)
end
function GameHelper:RequestGCSBuyCardData(callBack)
  GameHelper:GenerateGCSBuyLevelData()
  GameHelper:GenerateGCSBuyCardData()
  if callBack ~= nil then
    callBack()
  end
end
function GameHelper:GenerateGCSBuyCardData()
  local content = {}
  content.card_data = {}
  content.title_text = GameLoader:GetGameText("LC_MENU_GCS_BuySeasonPass_BTN")
  local t = {}
  t[1] = {
    pay_item_id = "ge2_gcs_card1",
    lv_add = 0,
    title_key = "LC_MENU_GCS_ChallengeSeasonPass_PassTitle1_CHAR",
    desc_key = "LC_MENU_GCS_ChallengeSeasonPass_PassDec1_CHAR",
    full_desc_key = "LC_MENU_GCS_ChallengeSeasonPass_PassFullDec1_CHAR",
    bg_pic = "GCS_seasonpass01.png"
  }
  t[2] = {
    pay_item_id = "ge2_gcs_card2",
    lv_add = 15,
    title_key = "LC_MENU_GCS_ChallengeSeasonPass_PassTitle2_CHAR",
    desc_key = "LC_MENU_GCS_ChallengeSeasonPass_PassDec2_CHAR",
    full_desc_key = "LC_MENU_GCS_ChallengeSeasonPass_PassFullDec2_CHAR",
    bg_pic = "GCS_seasonpass02.png"
  }
  if AutoUpdate.isAndroidDevice or AutoUpdate.isWin32 then
    t[1].pay_item_id = "ge2_gcs_card1_an"
    t[2].pay_item_id = "ge2_gcs_card2_an"
  end
  if ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2.chinese" then
    t[1].pay_item_id = "ge2_gcs_card1_ioscn"
    t[2].pay_item_id = "ge2_gcs_card2_ioscn"
  end
  content.card_data = t
  for _, v in ipairs(content.card_data) do
    local payItem = GameVip:GetGiftItemByID(v.pay_item_id)
    if payItem ~= nil and payItem.product_id ~= "undefined" then
      v.price = GameVip.GetPriceByProductID(payItem.product_id)
    end
    v.titleText = GameLoader:GetGameText(v.title_key)
    v.descText = GameLoader:GetGameText(v.desc_key)
    v.fullText = GameLoader:GetGameText(v.full_desc_key)
    v.btnText = v.price
  end
  local cur_season = GameHelper.GCSAwardsData.text_curSeason
  local season_left = GameHelper.GCSAwardsData.text_seasonLeft
  content.top_text_season = cur_season .. "  " .. season_left
  content.bottom_tip = ""
  GameHelper.GCSBuyCardData = content
end
function GameHelper:ShowGCSBuyCardPop()
  DebugOut("ShowGCSBuyCardPop")
  if self:GetFlashObject() ~= nil and GameHelper.GCSBuyCardData ~= nil then
    self:GetFlashObject():InvokeASCallback("_root", "showGCSBuyCardPop", GameHelper.GCSBuyCardData)
  end
end
function GameHelper:OnGCSClickBuyLevelBtn()
  GameHelper:RequestGCSBuyLevelData(function()
    GameHelper:ShowGCSBuyLevelPop()
  end)
end
function GameHelper:RequestGCSBuyLevelData(callBack)
  GameHelper:GenerateGCSBuyLevelData()
  GameHelper:GenerateGCSBuyCardData()
  if callBack ~= nil then
    callBack()
  end
end
function GameHelper:GenerateGCSBuyLevelData()
  local content = {}
  content.card_data = {}
  content.title_text = GameLoader:GetGameText("LC_MENU_GCS_BuyChallengeLV_BTN")
  local t = {}
  t[1] = {
    pay_item_id = "ge2_gcs_addlv1",
    lv_add = 1,
    title_key = "LC_MENU_GCS_BuyChallengeLV_BTN",
    desc_key = "LC_MENU_GCS_BuyChallengeLVWindow_PaymentDec1_CHAR",
    full_desc_key = "",
    bg_pic = "GCS_payment01.png"
  }
  t[2] = {
    pay_item_id = "ge2_gcs_addlv2",
    lv_add = 10,
    title_key = "LC_MENU_GCS_BuyChallengeLV_BTN",
    desc_key = "LC_MENU_GCS_BuyChallengeLVWindow_PaymentDec2_CHAR",
    full_desc_key = "",
    bg_pic = "GCS_payment02.png"
  }
  t[3] = {
    pay_item_id = "ge2_gcs_addlv3",
    lv_add = 35,
    title_key = "LC_MENU_GCS_BuyChallengeLV_BTN",
    desc_key = "LC_MENU_GCS_BuyChallengeLVWindow_PaymentDec3_CHAR",
    full_desc_key = "",
    bg_pic = "GCS_payment03.png"
  }
  t[4] = {
    pay_item_id = "ge2_gcs_addlv4",
    lv_add = 50,
    title_key = "LC_MENU_GCS_BuyChallengeLV_BTN",
    desc_key = "LC_MENU_GCS_BuyChallengeLVWindow_PaymentDec4_CHAR",
    full_desc_key = "",
    bg_pic = "GCS_payment04.png"
  }
  if AutoUpdate.isAndroidDevice or AutoUpdate.isWin32 then
    t[1].pay_item_id = "ge2_gcs_addlv1_an"
    t[2].pay_item_id = "ge2_gcs_addlv2_an"
    t[3].pay_item_id = "ge2_gcs_addlv3_an"
    t[4].pay_item_id = "ge2_gcs_addlv4_an"
  end
  if ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2.chinese" then
    t[1].pay_item_id = "ge2_gcs_addlv1_ioscn"
    t[2].pay_item_id = "ge2_gcs_addlv2_ioscn"
    t[3].pay_item_id = "ge2_gcs_addlv3_ioscn"
    t[4].pay_item_id = "ge2_gcs_addlv4_ioscn"
  end
  content.card_data = t
  for _, v in ipairs(content.card_data) do
    local payItem = GameVip:GetGiftItemByID(v.pay_item_id)
    if payItem ~= nil and payItem.product_id ~= "undefined" then
      v.price = GameVip.GetPriceByProductID(payItem.product_id)
    end
    v.titleText = GameLoader:GetGameText(v.title_key)
    v.descText = GameLoader:GetGameText(v.desc_key)
    v.fullText = ""
    v.btnText = v.price
  end
  content.top_text_level = GameLoader:GetGameText("LC_MENU_GCS_BuyChallengeLVWindow_ChallengeLV_CHAR")
  content.top_text_curlv = GameHelper.GCSAwardsData.cur_lv .. "/" .. GameHelper.GCSAwardsData.max_lv
  content.bottom_tip = GameLoader:GetGameText("LC_MENU_GCS_BuyChallengeLVWindow_Info_CHAR")
  GameHelper.GCSBuyLevelData = content
end
function GameHelper:ShowGCSBuyLevelPop()
  if self:GetFlashObject() ~= nil and GameHelper.GCSBuyLevelData ~= nil then
    self:GetFlashObject():InvokeASCallback("_root", "showGCSBuyLevelPop", GameHelper.GCSBuyLevelData)
  end
end
function GameHelper.GetDailyCallback(msgType, content)
  if msgType == NetAPIList.enter_monthly_ack.Code or msgType == NetAPIList.other_monthly_ack.Code then
    DebugOutPutTable(content, "daily day")
    GameHelper:GenerateDailyData(content)
    GameHelper:ShowRili()
    return true
  end
  return false
end
function GameHelper.GetRewardDaily(msgType, content)
  if msgType == NetAPIList.view_award_ack.Code then
    DebugOutPutTable(content, "reward daily")
    GameHelper.dailyAward = content.award
    GameHelper.updateRewardDaily()
    return true
  end
  return false
end
function GameHelper:OnGCSClickRewardAllBtn()
  local function msgCallBack(msgType, content)
    if msgType == NetAPIList.gcs_get_lv_all_rewards_ack.Code then
      DebugOutPutTable(content, "GameHelperOnGCSClickRewardAllBtn")
      if content.lv_awards_list ~= nil then
        GameHelper.GCSAwardsData.lv_awards_list = content.lv_awards_list
        GameHelper:ProcessGCSAwardsLvGridData(GameHelper.GCSAwardsData)
        GameHelper:ShowGCS_Reward()
        GameHelper:RefreshGCSRedPoint()
      end
      return true
    elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.gcs_get_lv_all_rewards_req.Code then
      DebugOutPutTable(content, "GameHelperOnGCSClickRewardAllBtn")
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    end
    return false
  end
  NetMessageMgr:SendMsg(NetAPIList.gcs_get_lv_all_rewards_req.Code, nil, msgCallBack, true, nil)
end
function GameHelper:ShowGCSTaskRewardItemDetail(taskID, rewardIndex)
  local _taskData
  if GameHelper.GCS_cur_click_week == 0 and GameHelper.GCSTaskData.today ~= nil then
    for _, v in ipairs(GameHelper.GCSTaskData.today.tasks) do
      if v.id == taskID then
        _taskData = v
      end
    end
  elseif GameHelper.GCS_cur_click_week > 0 and GameHelper.GCSTaskData.weeks ~= nil then
    for _, v in ipairs(GameHelper.GCSTaskData.weeks[GameHelper.GCS_cur_click_week].free_tasks) do
      if v.id == taskID then
        _taskData = v
      end
    end
    for _, v in ipairs(GameHelper.GCSTaskData.weeks[GameHelper.GCS_cur_click_week].paid_tasks) do
      if v.id == taskID then
        _taskData = v
      end
    end
  end
  if _taskData ~= nil and _taskData.awards[rewardIndex] ~= nil then
    local _item = _taskData.awards[rewardIndex]
    self:showItemDetil(_item, _item.item_type)
  end
end
function GameHelper:ShowGCSLvRewardItemDetail(lv, gridType, rewardIndex)
  if GameHelper.GCSAwardsData ~= nil then
    local _item
    for _, v in ipairs(GameHelper.GCSAwardsData.lv_awards_list) do
      if v.lv == lv and gridType == 1 then
        _item = v.free_awards[rewardIndex]
      elseif v.lv == lv and gridType == 2 then
        _item = v.paid_awards[rewardIndex]
      end
    end
    if _item ~= nil then
      self:showItemDetil(_item, _item.item_type)
    end
  end
end
function GameHelper:RequestGetGCSExtraReward(week_id)
  local function msgCallBack(msgType, content)
    if msgType == NetAPIList.gcs_get_week_extra_reward_ack.Code then
      DebugOutPutTable(content, "GameHelper:RequestGetGCSExtraReward--")
      if content.week_info ~= nil then
        for k, v in ipairs(GameHelper.GCSTaskData.weeks) do
          if v.week_id == content.week_info.week_id then
            GameHelper.GCSTaskData.weeks[k] = content.week_info
            GameHelper:OnGCSClickWeekBtn(GameHelper.GCS_cur_click_week)
          end
        end
        GameHelper:RefreshGCSRedPoint()
      end
      return true
    elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.gcs_get_week_extra_reward_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    end
    return false
  end
  local req_param = {}
  req_param.week_id = week_id
  NetMessageMgr:SendMsg(NetAPIList.gcs_get_week_extra_reward_req.Code, req_param, msgCallBack, true, nil)
end
function GameHelper:ShowGCSExtraRewardItemDetail(week_id)
  if GameHelper.GCSTaskData ~= nil then
    local _item
    for _, v in ipairs(GameHelper.GCSTaskData.weeks) do
      if v.week_id == week_id then
        _item = v.extra_award
      end
    end
    if _item ~= nil then
      self:showItemDetil(_item, _item.item_type)
    end
  end
end
function GameHelper:GetGCSTaskDataDescText(taskData)
  local desc_text = ""
  if taskData.locked == true then
    desc_text = "???"
  else
    desc_text = GameLoader:GetGameText(taskData.desc)
    local levelInfo = GameGlobalData:GetData("levelinfo")
    if taskData.recommend_lv ~= -1 and levelInfo.level < taskData.recommend_lv then
      local needLvText = string.gsub(GameLoader:GetGameText("LC_MENU_GCS_TaskNeedLV_CHAR"), "<s>", taskData.recommend_lv)
      desc_text = "<font color='#ff0000'>[" .. needLvText .. "]</font> " .. desc_text
    end
  end
  return desc_text
end
function GameHelper:RefreshGCSRedPoint()
  if GameHelper.TopTabState ~= nil and GameHelper.TopTabState[1].open == true and GameHelper.GCSTaskData ~= nil and GameHelper.GCSAwardsData ~= nil and GameHelper:GetFlashObject() ~= nil then
    local GCSRedPointData = {}
    GCSRedPointData.today = GameHelper:IsGCSWeekBtnRedPoint(0)
    GCSRedPointData.weeks = {}
    for k, v in ipairs(GameHelper.GCSTaskData.weeks) do
      GCSRedPointData.weeks[k] = GameHelper:IsGCSWeekBtnRedPoint(k)
    end
    GCSRedPointData.TaskTab = GameHelper:IsGCSTaskTabRedPoint()
    GCSRedPointData.RewardsTab = GameHelper:IsGCSLvRewardTabRedPoint()
    DebugOutPutTable(GCSRedPointData, "GameHelper:RefreshGCSRedPoint -- ")
    GameHelper:GetFlashObject():InvokeASCallback("_root", "RefreshGCSRedPoint", GCSRedPointData)
  end
end
function GameHelper:IsGCSWeekBtnRedPoint(index)
  if GameHelper.GCSTaskData ~= nil then
    if index == 0 then
      local todayData = GameHelper.GCSTaskData.today
      for _, v in ipairs(todayData.tasks) do
        if v.status == 1 then
          return true
        end
      end
    else
      local weekData = GameHelper.GCSTaskData.weeks[index]
      for _, v in ipairs(weekData.free_tasks) do
        if v.status == 1 then
          return true
        end
      end
      for _, v in ipairs(weekData.paid_tasks) do
        if v.status == 1 then
          return true
        end
      end
      if weekData.extra_status == 1 then
        return true
      end
    end
  end
  return false
end
function GameHelper:IsGCSTaskTabRedPoint()
  if GameHelper.GCSTaskData ~= nil then
    if GameHelper:IsGCSWeekBtnRedPoint(0) == true then
      return true
    end
    for k, v in ipairs(GameHelper.GCSTaskData.weeks) do
      if GameHelper:IsGCSWeekBtnRedPoint(k) == true then
        return true
      end
    end
  end
  return false
end
function GameHelper:IsGCSLvRewardTabRedPoint()
  if GameHelper.GCSAwardsData ~= nil then
    local data = GameHelper.GCSAwardsData
    for _, k in ipairs(data.lv_awards_list) do
      if k.lv <= data.cur_lv and LuaUtils:table_size(k.free_awards) > 0 and k.free_received == false then
        return true
      end
      if k.lv <= data.cur_lv and data.paid_card == true and 0 < LuaUtils:table_size(k.paid_awards) and k.paid_received == false then
        return true
      end
    end
  end
  return false
end
function GameHelper:CalculateGCSAwardsCurPage()
  local curPage = 1
  if GameHelper.GCSAwardsData ~= nil then
    local data = GameHelper.GCSAwardsData
    local page, mod = math.modf(data.cur_lv / 5)
    if mod > 0 then
      curPage = page + 1
    else
      curPage = page
    end
  end
  return curPage
end
function GameHelper.GCS_PointUpdateNTF(content)
  if content ~= nil then
    GameHelper:GenerateGCSAwardsData(content)
    if GameHelper:GetFlashObject() ~= nil then
      if GameHelper.CurSelTopTab == GameHelper.PageGCS and GameHelper.CurSelGCSTab == 2 then
        GameHelper:CloseGCSPopBuy()
        GameHelper:ShowGCS_Reward()
      end
      GameHelper:RefreshGCSRedPoint()
    end
  end
end
function GameHelper.GCS_BuyCardUpdateNTF(content)
  if content ~= nil then
    GameHelper:GenerateGCSTaskData(content)
    if GameHelper:GetFlashObject() ~= nil then
      if GameHelper.CurSelTopTab == GameHelper.PageGCS and GameHelper.CurSelGCSTab == 1 then
        GameHelper:CloseGCSPopBuy()
        GameHelper:ShowGCS_Task()
      end
      GameHelper:RefreshGCSRedPoint()
    end
  end
end
function GameHelper:OnClickGCSBuyCardItem(arg)
  DebugOut("OnClickGCSBuyCardItem 1=")
  local id = arg
  local card_data
  if id == "buy_card_1" then
    card_data = GameHelper.GCSBuyCardData.card_data[1]
  elseif id == "buy_card_2" then
    card_data = GameHelper.GCSBuyCardData.card_data[2]
  elseif id == "buy_level_1" then
    card_data = GameHelper.GCSBuyLevelData.card_data[1]
  elseif id == "buy_level_2" then
    card_data = GameHelper.GCSBuyLevelData.card_data[2]
  elseif id == "buy_level_3" then
    card_data = GameHelper.GCSBuyLevelData.card_data[3]
  elseif id == "buy_level_4" then
    card_data = GameHelper.GCSBuyLevelData.card_data[4]
  end
  local pay_item_id = card_data.pay_item_id
  local lv_add = card_data.lv_add
  local function okCallback()
    GameVip:BuyGCSItem(pay_item_id)
  end
  DebugOut("OnClickGCSBuyCardItem 2=" .. id)
  if GameHelper.GCSAwardsData.cur_lv + lv_add > GameHelper.GCSAwardsData.max_lv then
    local tip = GameLoader:GetGameText("LC_MENU_GCS_BuyChallengeLVWindow_MaxLV_CHAR")
    GameUIGlobalScreen:ShowMessageBox(2, "", tip, okCallback, nil)
  else
    GameVip:BuyGCSItem(pay_item_id)
  end
end
function GameHelper:CloseGCSPopBuy()
  if GameHelper:GetFlashObject() ~= nil then
    GameHelper:GetFlashObject():InvokeASCallback("_root", "CloseGCSPopBuy")
  end
end
