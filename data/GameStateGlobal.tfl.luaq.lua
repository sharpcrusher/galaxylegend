local GameStateGlobalState = GameStateManager.GameStateGlobalState
local GameObjectLevelUp = LuaObjectManager:GetLuaObject("GameObjectLevelUp")
local GameUIPrestigeRankUp = LuaObjectManager:GetLuaObject("GameUIPrestigeRankUp")
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameNewMenuItem = LuaObjectManager:GetLuaObject("GameNewMenuItem")
local GameQuestMenu = LuaObjectManager:GetLuaObject("GameQuestMenu")
GameUICommonDialog.WindowPopupWeight = 200
GameObjectLevelUp.WindowPopupWeight = 190
GameUIPrestigeRankUp.WindowPopupWeight = 180
GameNewMenuItem.WindowPopupWeight = 170
GameQuestMenu.WindowPopupWeight = 160
GameStateGlobalState.ObjectStack = {}
function GameStateGlobalState:AddObject(luaobject)
  if DebugConfig.isAssertAddObject then
    assert(not GameStateGlobalState:IsObjectInState(luaobject))
  end
  self:AddObjectToStack(luaobject)
end
function GameStateGlobalState:CheckStackObject(luaobject)
  if #self.ObjectStack > 0 then
    for objindex, obj in ipairs(self.ObjectStack) do
      if luaobject == obj then
        return objindex
      end
    end
  end
  return false
end
function GameStateGlobalState:AddObjectToStack(luaobject)
  if self:CheckStackObject(luaobject) then
    return
  end
  if luaobject.WindowPopupWeight then
    local insertindex
    for objindex, obj in ipairs(self.ObjectStack) do
      if luaobject.WindowPopupWeight > obj.WindowPopupWeight then
        insertindex = objindex
      end
    end
    if insertindex then
      table.insert(self.ObjectStack, insertindex, luaobject)
    else
      table.insert(self.ObjectStack, luaobject)
    end
  else
    GameStateBase.AddObject(self, luaobject)
  end
end
function GameStateGlobalState:Update(dt)
  if #self.m_menuObjectList == 0 and 0 < #self.ObjectStack then
    for objindex, obj in ipairs(self.ObjectStack) do
      if not obj.DisplayAllowed or obj:DisplayAllowed() then
        table.remove(self.ObjectStack, objindex)
        GameStateBase.AddObject(self, obj)
        break
      end
    end
  end
  GameStateBase.Update(self, dt)
end
