function dofile(n)
  local f, err = loadfile(n)
  if f then
    return f(), err
  else
    return nil, err
  end
end
immanentversion = nil
function ext.dofile(n)
  ext.UpdateAutoUpdateFile(n, "", 0, 0)
  dofile(n)
end
g_SaveNewFteData = false
if g_SaveNewFteData then
  math.randomseed(tostring(os.time()):reverse():sub(1, 6))
  g_FTEGroupId = math.random(1, 9999999)
end
function ext.sendNewFteData(step)
  if g_SaveNewFteData then
    print(" ext.sendNewFteData ", step, g_FTEGroupId)
    local url = "https://a.portal-platform.tap4fun.com/gl/url_get/online/an_world/gamers/fte"
    local httpheader = {
      ["User-Agent"] = "Tap4fun client",
      ["Accept-Encoding"] = "gzip",
      ["Appid"] = ext.GetBundleIdentifier(),
      ["clientv"] = ext.GetAppVersion(),
      ["shiqu"] = ext.GetTimeZone(),
      ["platform"] = ext.GetChannel(),
      ["macAddress"] = ext.GetIOSMacAddress(),
      ["udid"] = ext.GetIOSOpenUdid(),
      ["OSVersion"] = ext.GetOSVersion()
    }
    ext.http.requestBasic({
      url,
      param = nil,
      body = ext.ENC2(ext.json.generate({progress = step, group = g_FTEGroupId})),
      format = nil,
      method = "POST",
      mode = "normal",
      timeout = nil,
      localpath = nil,
      header = httpheader,
      callback = function(statusCode, responseContent, errstr)
        print("sendNewFteData---------------NOW CALL BACK HTTP------------")
        print("NOW STATUS CODE::" .. statusCode .. tostring(responseContent))
      end,
      progressCallback = nil,
      authCallback = nil
    })
  end
end
if g_SaveNewFteData then
  ext.sendNewFteData("FTE_InitLua")
end
function ext.getFontFile(fontName)
  print("======================fuck fontName ", fontName)
  if GameSettingData and GameSettingData.Save_Lang and GameSettingData.Save_Lang ~= "en" then
    return "data2/GalaxyEmpire2.ttf"
  end
  if fontName == "GalaxyEmpire2TitleGlobal-Bold" then
    return "data2/GalaxyEmpire2TitleGlobal.ttf"
  elseif fontName == "GalaxyEmpire2Title-Bold" then
    return "data2/GalaxyEmpire2Title.ttf"
  elseif fontName == "Galaxy Empire 2 Light" then
    return "data2/GalaxyEmpire2Light.ttf"
  else
    return "data2/GalaxyEmpire2.ttf"
  end
end
function ext.http.requestDownload(varTable)
  ext.http.requestBasic({
    varTable[1],
    body = varTable.body,
    format = varTable.format,
    method = varTable.method,
    mode = "download",
    timeout = varTable.timeout,
    localpath = varTable.localpath,
    header = {
      ["User-Agent"] = "Tap4fun client",
      ["Accept-Encoding"] = "gzip"
    },
    callback = function(statusCode, ...)
