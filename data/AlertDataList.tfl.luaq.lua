AlertDataList = {
  DataList = {
    ok = "LC_ALERT_ok",
    bad_param = "LC_ALERT_bad_param",
    server_timeout = "LC_ALERT_server_timeout",
    timeout = "LC_ALERT_timeout",
    inner_error = "LC_ALERT_inner_error",
    database_error = "LC_ALERT_database_error",
    database_busy = "LC_ALERT_database_busy",
    player_process_not_exist = "LC_ALERT_player_process_not_exist",
    udid_error = "LC_ALERT_udid_error",
    server_try_again = "LC_ALERT_server_try_again",
    server_ask_for_reload = "LC_ALERT_server_ask_for_reload",
    server_ask_for_offline = "LC_ALERT_server_ask_for_offline",
    client_version_up = "LC_ALERT_client_version_up",
    invalid_player_id = "LC_ALERT_invalid_player_id",
    get_buff_error = "LC_ALERT_get_buff_error",
    player_init_error = "LC_ALERT_player_init_error",
    resource_limit_error = "LC_ALERT_resource_limit_error",
    usual_error = "LC_ALERT_usual_error",
    bad_user_name = "LC_ALERT_bad_user_name",
    bad_password = "LC_ALERT_bad_password",
    user_not_exist = "LC_ALERT_user_not_exist",
    user_login_already = "LC_ALERT_user_login_already",
    user_register_error = "LC_ALERT_user_register_error",
    user_register_db_exception = "LC_ALERT_user_register_db_exception",
    user_login_error = "LC_ALERT_user_login_error",
    user_login_db_exception = "LC_ALERT_user_login_db_exception",
    user_find_db_exception = "LC_ALERT_user_find_db_exception",
    user_passport_and_player_not_match = "LC_ALERT_user_passport_and_player_not_match",
    username_exist = "LC_ALERT_username_exist",
    userudid_not_match = "LC_ALERT_userudid_not_match",
    usercenter_error = "LC_ALERT_usercenter_error",
    usercenter_login_valid_error = "LC_ALERT_usercenter_login_valid_error",
    usercenter_register_device_error = "LC_ALERT_usercenter_register_device_error",
    usercenter_create_user_and_device = "LC_ALERT_usercenter_create_user_and_device",
    usercenter_bind_user_and_device = "LC_ALERT_usercenter_bind_user_and_device",
    usercenter_create_error = "LC_ALERT_usercenter_create_error",
    usercenter_device_unvalid = "LC_ALERT_usercenter_device_unvalid",
    username_too_short = "LC_ALERT_username_too_short",
    login_faild = "LC_ALERT_login_faild",
    usercenter_timeout = "LC_ALERT_usercenter_timeout",
    username_too_long = "LC_ALERT_username_too_long",
    username_unvalid = "LC_ALERT_username_unvalid",
    facebook_account_wrong = "LC_ALERT_facebook_account_wrong",
    facebook_account_timeout = "LC_ALERT_facebook_account_timeout",
    player_ban_already = "LC_ALERT_player_ban_already",
    username_unfriendly = "LC_ALERT_username_unfriendly",
    award_e203_one = "LC_ALERT_award_e203_one",
    award_e203_two = "LC_ALERT_award_e203_two",
    award_e203_three = "LC_ALERT_award_e203_three",
    award_e203_four = "LC_ALERT_award_e203_four",
    award_e203_five = "LC_ALERT_award_e203_five",
    award_e203_six = "LC_ALERT_award_e203_six",
    award_e203_seven = "LC_ALERT_award_e203_seven",
    award_w304 = "LC_ALERT_award_w304",
    award_w304_timeout = "LC_ALERT_award_w304_timeout",
    award_e305 = "LC_ALERT_award_e305",
    wrong_action_type_purchase = "LC_ALERT_wrong_action_type_purchase",
    colony_defender_same_as_attcker = "LC_ALERT_colony_defender_same_as_attcker",
    colony_slave_yet_owned = "LC_ALERT_colony_slave_yet_owned",
    colony_actual_not_expect_one = "LC_ALERT_colony_actual_not_expect_one",
    colony_cdtime_not_enough = "LC_ALERT_colony_cdtime_not_enough",
    colony_capture_diff_level_more_than_max = "LC_ALERT_colony_capture_diff_level_more_than_max",
    colony_capture_position_as_slave = "LC_ALERT_colony_capture_position_as_slave",
    colony_capture_times_more_than_max = "LC_ALERT_colony_capture_times_more_than_max",
    colony_capture_slave_more_than_max = "LC_ALERT_colony_capture_slave_more_than_max",
    colony_capture_actual_not_slaveholder = "LC_ALERT_colony_capture_actual_not_slaveholder",
    colony_help_attcker_same_as_slave = "LC_ALERT_colony_help_attcker_same_as_slave",
    colony_capture_actual_not_civilian = "LC_ALERT_colony_capture_actual_not_civilian",
    colony_help_position_as_not_slave = "LC_ALERT_colony_help_position_as_not_slave",
    colony_capture_any_slave_not_owned = "LC_ALERT_colony_capture_any_slave_not_owned",
    colony_help_times_more_than_max = "LC_ALERT_colony_help_times_more_than_max",
    colony_rescue_slave_same_as_owner = "LC_ALERT_colony_rescue_slave_same_as_owner",
    colony_resist_defender_different_from_owner = "LC_ALERT_colony_resist_defender_different_from_owner",
    colony_rescue_attcker_same_as_slave = "LC_ALERT_colony_rescue_attcker_same_as_slave",
    colony_resist_position_as_not_slave = "LC_ALERT_colony_resist_position_as_not_slave",
    colony_rescue_position_as_slave = "LC_ALERT_colony_rescue_position_as_slave",
    colony_resist_times_more_than_max = "LC_ALERT_colony_resist_times_more_than_max",
    colony_rescue_times_more_than_max = "LC_ALERT_colony_rescue_times_more_than_max",
    colony_release_owner_same_as_slave = "LC_ALERT_colony_release_owner_same_as_slave",
    colony_rescue_actual_not_slave = "LC_ALERT_colony_rescue_actual_not_slave",
    colony_release_position_as_not_slaveholder = "LC_ALERT_colony_release_position_as_not_slaveholder",
    colony_rescue_actual_not_slaveholder = "LC_ALERT_colony_rescue_actual_not_slaveholder",
    colony_release_slave_not_owned = "LC_ALERT_colony_release_slave_not_owned",
    colony_help_slave_same_as_owner = "LC_ALERT_colony_help_slave_same_as_owner",
    colony_release_actual_not_slave = "LC_ALERT_colony_release_actual_not_slave",
    colony_exploit_position_as_not_slaveholder = "LC_ALERT_colony_exploit_position_as_not_slaveholder",
    colony_exploit_slave_not_owned = "LC_ALERT_colony_exploit_slave_not_owned",
    colony_exploit_actual_not_slave = "LC_ALERT_colony_exploit_actual_not_slave",
    colony_exploit_owner_changed = "LC_ALERT_colony_exploit_owner_changed",
    colony_exploit_require_more_credit = "LC_ALERT_colony_exploit_require_more_credit",
    colony_colony_fawn_position_as_not_slaveholder = "LC_ALERT_colony_colony_fawn_position_as_not_slaveholder",
    colony_fawn_slave_not_owned = "LC_ALERT_colony_fawn_slave_not_owned",
    colony_fawn_times_more_than_max_times = "LC_ALERT_colony_fawn_times_more_than_max_times",
    colony_fawn_actual_not_slave = "LC_ALERT_colony_fawn_actual_not_slave",
    colony_fawn_owner_change = "LC_ALERT_colony_fawn_owner_change",
    colony_fawn_position_as_not_slave = "LC_ALERT_colony_fawn_position_as_not_slave",
    colony_fawn_owner_changed = "LC_ALERT_colony_fawn_owner_changed",
    colony_fawn_owner_more_than_max_times = "LC_ALERT_colony_fawn_owner_more_than_max_times",
    recruit_fleet_bad_fleet_id = "LC_ALERT_recruit_fleet_bad_fleet_id",
    recruit_fleet_exist = "LC_ALERT_recruit_fleet_exist",
    recruit_fleet_level_err = "LC_ALERT_recruit_fleet_level_err",
    recruit_fleet_prestige_err = "LC_ALERT_recruit_fleet_prestige_err",
    recruit_fleet_quest_err = "LC_ALERT_recruit_fleet_quest_err",
    recruit_fleet_lepton_err = "LC_ALERT_recruit_fleet_lepton_err",
    recruit_fleet_quark_err = "LC_ALERT_recruit_fleet_quark_err",
    recruit_fleet_udid_err = "LC_ALERT_recruit_fleet_udid_err",
    recruit_fleet_challenge_err = "LC_ALERT_recruit_fleet_challenge_err",
    recruit_fleet_krypton_err = "LC_ALERT_recruit_fleet_krypton_err",
    recruit_fleet_max_cnt_err = "LC_ALERT_recruit_fleet_max_cnt_err",
    friend_free_over_max = "LC_ALERT_friend_free_over_max",
    friend_lock_over_max = "LC_ALERT_friend_lock_over_max",
    friend_not_exist = "LC_ALERT_friend_not_exist",
    friend_already = "LC_ALERT_friend_already",
    friend_ban_already = "LC_ALERT_friend_ban_already",
    friend_del_already = "LC_ALERT_friend_del_already",
    friend_ban_del_already = "LC_ALERT_friend_ban_del_already",
    friend_add_self = "LC_ALERT_friend_add_self",
    friend_ban_self = "LC_ALERT_friend_ban_self",
    refresh_max_times = "LC_ALERT_refresh_max_times",
    cost_limit = "LC_ALERT_cost_limit",
    alliance_kick_not_allowed_in_wd_activity = "LC_ALERT_alliance_kick_not_allowed_in_wd_activity",
    alliance_quit_not_allowed_in_wd_activity = "LC_ALERT_alliance_quit_not_allowed_in_wd_activity",
    alliance_delete_not_allowed_in_wd_activity = "LC_ALERT_alliance_delete_not_allowed_in_wd_activity",
    alliance_kick_not_allowed_in_matched_wd = "LC_ALERT_alliance_kick_not_allowed_in_matched_wd",
    alliance_quit_not_allowed_in_matched_wd = "LC_ALERT_alliance_quit_not_allowed_in_matched_wd",
    alliance_delete_not_allowed_in_matched_wd = "LC_ALERT_alliance_delete_not_allowed_in_matched_wd",
    alliance_ratify_not_allowed_in_matched_wd = "LC_ALERT_alliance_ratify_not_allowed_in_matched_wd",
    alliance_defence_repair_not_allowed_in_matched_wd = "LC_ALERT_alliance_defence_repair_not_allowed_in_matched_wd",
    alliance_set_defender_not_allowed_in_matched_wd = "LC_ALERT_alliance_set_defender_not_allowed_in_matched_wd",
    alliance_wd_join_not_allowed_in_non_activity = "LC_ALERT_alliance_wd_join_not_allowed_in_non_activity",
    alliance_wd_join_yet_matched = "LC_ALERT_alliance_wd_join_yet_matched",
    alliance_wd_challenge_is_over = "LC_ALERT_alliance_wd_challenge_is_over",
    alliance_wd_challenge_waiting_to_match = "LC_ALERT_alliance_wd_challenge_waiting_to_match",
    alliance_wd_level_limit = "LC_ALERT_alliance_wd_level_limit",
    alliance_wd_challenge_defence_leader_alive = "LC_ALERT_alliance_wd_challenge_defence_leader_alive",
    alliance_wd_challenge_require_more_credit = "LC_ALERT_alliance_wd_challenge_require_more_credit",
    alliance_wd_challenge_defence_leader_yet_dead = "LC_ALERT_alliance_wd_challenge_defence_leader_yet_dead",
    alliance_wd_defence_wall_yet_destroied = "LC_ALERT_alliance_wd_defence_wall_yet_destroied",
    alliance_wd_defence_core_under_protected = "LC_ALERT_alliance_wd_defence_core_under_protected",
    alliance_wd_memeber_not_enough = "LC_ALERT_alliance_wd_memeber_not_enough",
    alliance_flag_modification_permission_not_owned = "LC_ALERT_alliance_flag_modification_permission_not_owned",
    alliance_flag_yet_setted = "LC_ALERT_alliance_flag_yet_setted",
    alliance_defence_leader_not_dead = "LC_ALERT_alliance_defence_leader_not_dead",
    alliance_defence_leader_revive_require_more_credit = "LC_ALERT_alliance_defence_leader_revive_require_more_credit",
    alliance_defence_leader_revive_not_allowed_in_non_battle = "LC_ALERT_alliance_defence_leader_revive_not_allowed_in_non_battle",
    server_busy = "LC_ALERT_server_busy",
    alliance_defender_not_kicked = "LC_ALERT_alliance_defender_not_kicked",
    alliance_brick_donation_require_more_brick = "LC_ALERT_alliance_brick_donation_require_more_brick",
    alliance_repair_permission_not_owned = "LC_ALERT_alliance_repair_permission_not_owned",
    alliance_repair_require_more_brick = "LC_ALERT_alliance_repair_require_more_brick",
    alliance_assignment_permission_not_owned = "LC_ALERT_alliance_assignment_permission_not_owned",
    alliance_defence_leader_yet_owned = "LC_ALERT_alliance_defence_leader_yet_owned",
    activity_task_reward_wd_unfinished = "LC_ALERT_activity_task_reward_wd_unfinished",
    wd_last_hour_cannot_fight = "LC_ALERT_wd_last_hour_cannot_fight",
    alliance_defence_leader_actual_not_leader = "LC_ALERT_alliance_defence_leader_actual_not_leader",
    alliance_defence_leader_actual_is_leader = "LC_ALERT_alliance_defence_leader_actual_is_leader",
    alliance_approval_permission_not_owned = "LC_ALERT_alliance_approval_permission_not_owned",
    alliance_apply_not_found = "LC_ALERT_alliance_apply_not_found",
    alliance_full = "LC_ALERT_alliance_full",
    alliance_not_yet_ratified = "LC_ALERT_alliance_not_yet_ratified",
    alliance_member_not_found = "LC_ALERT_alliance_member_not_found",
    alliance_transfer_permission_not_owned = "LC_ALERT_alliance_transfer_permission_not_owned",
    alliance_creator_not_quit = "LC_ALERT_alliance_creator_not_quit",
    alliance_creator_can_delete = "LC_ALERT_alliance_creator_can_delete",
    alliance_creator_not_kicked = "LC_ALERT_alliance_creator_not_kicked",
    alliance_not_found = "LC_ALERT_alliance_not_found",
    alliance_player_level_limit = "LC_ALERT_alliance_player_level_limit",
    alliance_require_more_money = "LC_ALERT_alliance_require_more_money",
    alliance_name_existed = "LC_ALERT_alliance_name_existed",
    alliance_only_one_create_allowed = "LC_ALERT_alliance_only_one_create_allowed",
    alliance_already_applied = "LC_ALERT_alliance_already_applied",
    alliance_applied_full = "LC_ALERT_alliance_applied_full",
    alliance_already_ratified = "LC_ALERT_alliance_already_ratified",
    alliance_not_applied = "LC_ALERT_alliance_not_applied",
    alliance_not_yet_owned = "LC_ALERT_alliance_not_yet_owned",
    alliance_name_not_empty = "LC_ALERT_alliance_name_not_empty",
    alliance_name_is_invalid = "LC_ALERT_alliance_name_is_invalid",
    alliance_manager_count_limit = "LC_ALERT_alliance_manager_count_limit",
    alliance_aready_received_award = "LC_ALERT_alliance_aready_received_award",
    alliance_self_not_kicked = "LC_ALERT_alliance_self_not_kicked",
    get_donate_error = "LC_ALERT_get_donate_error",
    error_donate_type = "LC_ALERT_error_donate_type",
    tc_player_already_die = "LC_ALERT_tc_player_already_die",
    tc_jump_path_error = "LC_ALERT_tc_jump_path_error",
    tc_path_is_empty = "LC_ALERT_tc_path_is_empty",
    tc_dot_mismatch_line = "LC_ALERT_tc_dot_mismatch_line",
    tc_line_not_found = "LC_ALERT_tc_line_not_found",
    tc_player_not_found = "LC_ALERT_tc_player_not_found",
    tc_dot_not_found = "LC_ALERT_tc_dot_not_found",
    tc_battle_not_found = "LC_ALERT_tc_battle_not_found",
    tc_start_first_dot_mismatch_position = "LC_ALERT_tc_start_first_dot_mismatch_position",
    tc_start_on_road = "LC_ALERT_tc_start_on_road",
    tc_transfer_required_more_credit = "LC_ALERT_tc_transfer_required_more_credit",
    tc_transfer_first_dot_mismatch_position = "LC_ALERT_tc_transfer_first_dot_mismatch_position",
    tc_transfer_station_occupied = "LC_ALERT_tc_transfer_station_occupied",
    tc_transfer_station_empty = "LC_ALERT_tc_transfer_station_empty",
    tc_revive_required_more_credit = "LC_ALERT_tc_revive_required_more_credit",
    tc_revive_is_alive = "LC_ALERT_tc_revive_is_alive",
    equipment_not_found = "LC_ALERT_equipment_not_found",
    equipment_more_than_engineering_bay_onlevel = "LC_ALERT_equipment_more_than_engineering_bay_onlevel",
    equipment_arrive_at_maxlevel = "LC_ALERT_equipment_arrive_at_maxlevel",
    equipment_require_more_money = "LC_ALERT_equipment_require_more_money",
    equipment_no_evolution_info = "LC_ALERT_equipment_no_evolution_info",
    equipment_require_more_cdtime = "LC_ALERT_equipment_require_more_cdtime",
    equipment_fleet_not_found = "LC_ALERT_equipment_fleet_not_found",
    equipment_not_found_in_bag = "LC_ALERT_equipment_not_found_in_bag",
    equipment_less_than_minlevel = "LC_ALERT_equipment_less_than_minlevel",
    equipment_require_recipe_not_found = "LC_ALERT_equipment_require_recipe_not_found",
    equipment_evolution_not_found = "LC_ALERT_equipment_evolution_not_found",
    equipment_enhance_level_limit = "LC_ALERT_equipment_enhance_level_limit",
    building_max_level = "LC_ALERT_building_max_level",
    building_not_exist = "LC_ALERT_building_not_exist",
    building_data_not_exist = "LC_ALERT_building_data_not_exist",
    building_up_max_level = "LC_ALERT_building_up_max_level",
    building_cdtime_not = "LC_ALERT_building_cdtime_not",
    building_level_greater_than_player_5 = "LC_ALERT_building_level_greater_than_player_5",
    building_level_greater_than_base = "LC_ALERT_building_level_greater_than_base",
    building_level_limit_user = "LC_ALERT_building_level_limit_user",
    building_level_limit_user_5 = "LC_ALERT_building_level_limit_user_5",
    player_is_mining = "LC_ALERT_player_is_mining",
    mine_cnt_limit = "LC_ALERT_mine_cnt_limit",
    mine_idx_error = "LC_ALERT_mine_idx_error",
    refresh_mine_type_error = "LC_ALERT_refresh_mine_type_error",
    refresh_mine_res_limit = "LC_ALERT_refresh_mine_res_limit",
    mine_atk_target_error = "LC_ALERT_mine_atk_target_error",
    mine_atk_error = "LC_ALERT_mine_atk_error",
    mine_atk_mine_done = "LC_ALERT_mine_atk_mine_done",
    mine_atk_robbed_cnt_limit = "LC_ALERT_mine_atk_robbed_cnt_limit",
    mine_not_digg = "LC_ALERT_mine_not_digg",
    mine_speedup_time_limit = "LC_ALERT_mine_speedup_time_limit",
    mine_atk_cd_limit = "LC_ALERT_mine_atk_cd_limit",
    mine_boost_cnt_limit = "LC_ALERT_mine_boost_cnt_limit",
    mine_atk_cnt_limit = "LC_ALERT_mine_atk_cnt_limit",
    mine_speedup_fail = "LC_ALERT_mine_speedup_fail",
    mine_can_not_speedup = "LC_ALERT_mine_can_not_speedup",
    krypton_gacha_no_money = "LC_ALERT_krypton_gacha_no_money",
    krypton_not_exist = "LC_ALERT_krypton_not_exist",
    krypton_not_enough_kenergy = "LC_ALERT_krypton_not_enough_kenergy",
    krypton_equip_type_conflict = "LC_ALERT_krypton_equip_type_conflict",
    krypton_equip_slot_conflict = "LC_ALERT_krypton_equip_slot_conflict",
    krypton_equip_already = "LC_ALERT_krypton_equip_already",
    krypton_tmp_full = "LC_ALERT_krypton_tmp_full",
    krypton_store_full = "LC_ALERT_krypton_store_full",
    krypton_postion_already_used = "LC_ALERT_krypton_postion_already_used",
    krypton_not_tmp = "LC_ALERT_krypton_not_tmp",
    krypton_not_store = "LC_ALERT_krypton_not_store",
    krypton_equip_not_decom = "LC_ALERT_krypton_equip_not_decom",
    krypton_sort_not_valid = "LC_ALERT_krypton_sort_not_valid",
    krypton_lab_level_not = "LC_ALERT_krypton_lab_level_not",
    krypton_not_equiped = "LC_ALERT_krypton_not_equiped",
    krypton_max_level = "LC_ALERT_krypton_max_level",
    krypton_can_not_decompose = "LC_ALERT_krypton_can_not_decompose",
    battle_level_limit = "LC_ALERT_battle_level_limit",
    battle_not_allow = "LC_ALERT_battle_not_allow",
    battle_not_exist = "LC_ALERT_battle_not_exist",
    battle_adventure_no_supply = "LC_ALERT_battle_adventure_no_supply",
    battle_not_allow_today = "LC_ALERT_battle_not_allow_today",
    chapter_not_exist = "LC_ALERT_chapter_not_exist",
    battle_supply_not_enough = "LC_ALERT_battle_supply_not_enough",
    ace_supply_not_enough = "LC_ALERT_ace_supply_not_enough",
    battle_cant_because_item_null = "LC_ALERT_battle_cant_because_item_null",
    battle_cant_rush = "LC_ALERT_battle_cant_rush",
    act_not_exist = "LC_ALERT_act_not_exist",
    warpgate_charge_type_err = "LC_ALERT_warpgate_charge_type_err",
    warpgate_can_not_upgrade_err = "LC_ALERT_warpgate_can_not_upgrade_err",
    warpgate_upgrade_type_err = "LC_ALERT_warpgate_upgrade_type_err",
    warpgate_can_not_collect_err = "LC_ALERT_warpgate_can_not_collect_err",
    warpgate_charge_resource_limit = "LC_ALERT_warpgate_charge_resource_limit",
    warpgate_upgrade_resource_limit = "LC_ALERT_warpgate_upgrade_resource_limit",
    warpgate_exchange_resource_limit = "LC_ALERT_warpgate_exchange_resource_limit",
    shop_bad_equip_id = "LC_ALERT_shop_bad_equip_id",
    shop_purchase_money_limit = "LC_ALERT_shop_purchase_money_limit",
    shop_sell_bad_grid_info = "LC_ALERT_shop_sell_bad_grid_info",
    shop_user_bag_full = "LC_ALERT_shop_user_bag_full",
    shop_bad_purchase_back_idx = "LC_ALERT_shop_bad_purchase_back_idx",
    shop_bag_grid_not_found = "LC_ALERT_shop_bag_grid_not_found",
    shop_bag_grid_is_filled = "LC_ALERT_shop_bag_grid_is_filled",
    shop_sell_bad_item_cnt = "LC_ALERT_shop_sell_bad_item_cnt",
    stores_item_time_out = "LC_ALERT_stores_item_time_out",
    item_time_expired = "LC_ALERT_item_time_expired",
    crystal_not_enough = "LC_ALERT_crystal_not_enough",
    pay_verify_error = "LC_ALERT_pay_verify_error",
    pay_verify_token_error = "LC_ALERT_pay_verify_token_error",
    pay_verify_not_ready = "LC_ALERT_pay_verify_not_ready",
    pay_verify_already = "LC_ALERT_pay_verify_already",
    pay_verify_not_debug = "LC_ALERT_pay_verify_not_debug",
    pay_verify_mycard_used = "LC_ALERT_pay_verify_mycard_used",
    pay_verify_mycard_not_match = "LC_ALERT_pay_verify_mycard_not_match",
    order_not_found = "LC_ALERT_order_not_found",
    order_confirm_yet_confirmed = "LC_ALERT_order_confirm_yet_confirmed",
    order_confirm_mycard_billing_error = "LC_ALERT_order_confirm_mycard_billing_error",
    order_confirm_mycard_card_not_found = "LC_ALERT_order_confirm_mycard_card_not_found",
    order_confirm_mycard_point_error = "LC_ALERT_order_confirm_mycard_point_error",
    ac_energy_charge_level_limit = "LC_ALERT_ac_energy_charge_level_limit",
    ac_energy_charge_ac_supply_limit = "LC_ALERT_ac_energy_charge_ac_supply_limit",
    ac_energy_charge_tech_limit = "LC_ALERT_ac_energy_charge_tech_limit",
    ac_battle_id_error = "LC_ALERT_ac_battle_id_error",
    ac_battle_completed_today_error = "LC_ALERT_ac_battle_completed_today_error",
    ac_battle_level_limit = "LC_ALERT_ac_battle_level_limit",
    technique_not_found = "LC_ALERT_technique_not_found",
    technique_more_than_player_onlevel = "LC_ALERT_technique_more_than_player_onlevel",
    technique_more_than_techlab_onlevel = "LC_ALERT_technique_more_than_techlab_onlevel",
    technique_less_than_minlevel = "LC_ALERT_technique_less_than_minlevel",
    technique_arrive_at_maxlevel = "LC_ALERT_technique_arrive_at_maxlevel",
    technique_require_more_techpoint = "LC_ALERT_technique_require_more_techpoint",
    ladder_reset_less = "LC_ALERT_ladder_reset_less",
    ladder_reset_one = "LC_ALERT_ladder_reset_one",
    ladder_raids_max = "LC_ALERT_ladder_raids_max",
    ladder_search_cost = "LC_ALERT_ladder_search_cost",
    ladder_reset_cost = "LC_ALERT_ladder_reset_cost",
    ladder_reset_max = "LC_ALERT_ladder_reset_max",
    dungeon_ladder_close = "LC_ALERT_dungeon_ladder_close",
    dungeon_ladder_level = "LC_ALERT_dungeon_ladder_level",
    ladder_search_max = "LC_ALERT_ladder_search_max",
    ladder_search_error = "LC_ALERT_ladder_search_error",
    ladder_raids_end = "LC_ALERT_ladder_raids_end",
    ladder_search_on_max = "LC_ALERT_ladder_search_on_max",
    mulmatrix_not_open = "LC_ALERT_mulmatrix_not_open",
    mulmatrix_buy_credit = "LC_ALERT_mulmatrix_buy_credit",
    mulmatrix_condition = "LC_ALERT_mulmatrix_condition",
    mulmatrix_buy_money = "LC_ALERT_mulmatrix_buy_money",
    ladder_jump_type_error = "LC_ALERT_ladder_jump_type_error",
    ladder_report_error = "LC_ALERT_ladder_report_error",
    ladder_max_step_error = "LC_ALERT_ladder_max_step_error",
    ladder_award_unfinish = "LC_ALERT_ladder_award_unfinish",
    ladder_award_already = "LC_ALERT_ladder_award_already",
    ladder_raids_level_less = "LC_ALERT_ladder_raids_level_less",
    normal_ladder_raids_over = "LC_ALERT_normal_ladder_raids_over",
    expedition_wormhole_score_lack = "LC_ALERT_expedition_wormhole_score_lack",
    expedition_wormhole_award_got = "LC_ALERT_expedition_wormhole_award_got",
    expedition_challenge_on_max = "LC_ALERT_expedition_challenge_on_max",
    expedition_challenge_end = "LC_ALERT_expedition_challenge_end",
    expedition_reset_max = "LC_ALERT_expedition_reset_max",
    expedition_challenge_max = "LC_ALERT_expedition_challenge_max",
    expedition_challenge_cost = "LC_ALERT_expedition_challenge_cost",
    expedition_reset_cost = "LC_ALERT_expedition_reset_cost",
    expedition_not_enough = "LC_ALERT_expedition_not_enough",
    fleet_not_exist = "LC_ALERT_fleet_not_exist",
    fleet_enhance_level_limit = "LC_ALERT_fleet_enhance_level_limit",
    fleet_enhance_arrives_end = "LC_ALERT_fleet_enhance_arrives_end",
    fleet_enhance_required_more_items = "LC_ALERT_fleet_enhance_required_more_items",
    fleet_enhance_gacha = "LC_ALERT_fleet_enhance_gacha",
    fleet_weaken_minimality = "LC_ALERT_fleet_weaken_minimality",
    fleet_weaken_on_returning = "LC_ALERT_fleet_weaken_on_returning",
    fleet_enhance_upgrade_required_more_items = "LC_ALERT_fleet_enhance_upgrade_required_more_items",
    fleet_weaken_bag_full = "LC_ALERT_fleet_weaken_bag_full",
    fleet_equip_level_limit = "LC_ALERT_fleet_equip_level_limit",
    fleet_already_on_matrix = "LC_ALERT_fleet_already_on_matrix",
    matrix_cell_already_been_taken = "LC_ALERT_matrix_cell_already_been_taken",
    fleet_not_on_matrix = "LC_ALERT_fleet_not_on_matrix",
    fleet_have_not_spell = "LC_ALERT_fleet_have_not_spell",
    spell_not_exist = "LC_ALERT_spell_not_exist",
    fleet_already_have_spell = "LC_ALERT_fleet_already_have_spell",
    fleet_spell_over_max = "LC_ALERT_fleet_spell_over_max",
    fleet_count_over_max = "LC_ALERT_fleet_count_over_max",
    challenge_user_info_error = "LC_ALERT_challenge_user_info_error",
    challenge_rank_error = "LC_ALERT_challenge_rank_error",
    challenge_fail_cd_error = "LC_ALERT_challenge_fail_cd_error",
    challenge_max_cnt_error = "LC_ALERT_challenge_max_cnt_error",
    no_need_reset_fail_time = "LC_ALERT_no_need_reset_fail_time",
    reset_fail_time_resource_limit = "LC_ALERT_reset_fail_time_resource_limit",
    champion_award_fail = "LC_ALERT_champion_award_fail",
    champion_player_on_fight = "LC_ALERT_champion_player_on_fight",
    champion_get_player_info_error = "LC_ALERT_champion_get_player_info_error",
    champion_no_top_record = "LC_ALERT_champion_no_top_record",
    challenge_can_fight_again = "LC_ALERT_challenge_can_fight_again",
    error_get_champion_reward = "LC_ALERT_error_get_champion_reward",
    vip_no_enough = "LC_ALERT_vip_no_enough",
    vip_no_enough_to_buy_store = "LC_ALERT_vip_no_enough_to_buy_store",
    vip_limit = "LC_ALERT_vip_limit",
    vip_level_not_exist = "LC_ALERT_vip_level_not_exist",
    adv_refresh_time_full = "LC_ALERT_adv_refresh_time_full",
    chat_user_banned = "LC_ALERT_chat_user_banned",
    chat_bad_channel = "LC_ALERT_chat_bad_channel",
    chat_receiver_bad_name = "LC_ALERT_chat_receiver_bad_name",
    chat_too_fast = "LC_ALERT_chat_too_fast",
    chat_not_tc = "LC_ALERT_chat_not_tc",
    mail_content_too_short = "LC_ALERT_mail_content_too_short",
    mail_receiver_list_null = "LC_ALERT_mail_receiver_list_null",
    mail_bag_to_mail = "LC_ALERT_mail_bag_to_mail",
    mail_goods_have_get = "LC_ALERT_mail_goods_have_get",
    player_level_not_enough_mail = "LC_ALERT_player_level_not_enough_mail",
    open_box_error = "LC_ALERT_open_box_error",
    open_box_have_get = "LC_ALERT_open_box_have_get",
    open_box_credit_error = "LC_ALERT_open_box_credit_error",
    redeem_code_activity_aready_in = "LC_ALERT_redeem_code_activity_aready_in",
    redeem_code_param_empty = "LC_ALERT_redeem_code_param_empty",
    redeem_code_not_found = "LC_ALERT_redeem_code_not_found",
    redeem_code_aready_used = "LC_ALERT_redeem_code_aready_used",
    not_in_world_boss_time = "LC_ALERT_not_in_world_boss_time",
    player_level_not_enough_world_boss = "LC_ALERT_player_level_not_enough_world_boss",
    world_boss_dead = "LC_ALERT_world_boss_dead",
    world_boss_max_force_rate = "LC_ALERT_world_boss_max_force_rate",
    credit_not_enough_world_boss = "LC_ALERT_credit_not_enough_world_boss",
    tech_not_enough_world_boss = "LC_ALERT_tech_not_enough_world_boss",
    world_boss_in_ready = "LC_ALERT_world_boss_in_ready",
    boss_robot_duplicately_added = "LC_ALERT_boss_robot_duplicately_added",
    boss_robot_attackment_is_on_work = "LC_ALERT_boss_robot_attackment_is_on_work",
    boss_robot_terminated = "LC_ALERT_boss_robot_terminated",
    in_cd_world_boss = "LC_ALERT_in_cd_world_boss",
    money_not_enough = "LC_ALERT_money_not_enough",
    technique_not_enough = "LC_ALERT_technique_not_enough",
    credit_not_enough = "LC_ALERT_credit_not_enough",
    cdtime_clear_no_credit = "LC_ALERT_cdtime_clear_no_credit",
    revenue_exchange_not_enough_credit = "LC_ALERT_revenue_exchange_not_enough_credit",
    credit_not_enough_donate = "LC_ALERT_credit_not_enough_donate",
    money_not_enough_donate = "LC_ALERT_money_not_enough_donate",
    laba_supply_not_enough = "LC_ALERT_laba_supply_not_enough",
    supply_exchange_no_credit = "LC_ALERT_supply_exchange_no_credit",
    officer_require_more_credit = "LC_ALERT_officer_require_more_credit",
    revenue_time_not_enough = "LC_ALERT_revenue_time_not_enough",
    warpgate_upgrade_cnt_limit = "LC_ALERT_warpgate_upgrade_cnt_limit",
    revenue_exchange_time_full = "LC_ALERT_revenue_exchange_time_full",
    supply_exchange_time_full = "LC_ALERT_supply_exchange_time_full",
    commander_change_time_full = "LC_ALERT_commander_change_time_full",
    officer_more_than_maxtimes = "LC_ALERT_officer_more_than_maxtimes",
    vip_level_lower_donate = "LC_ALERT_vip_level_lower_donate",
    beyond_day_buy_max = "LC_ALERT_beyond_day_buy_max",
    beyond_all_buy_max = "LC_ALERT_beyond_all_buy_max",
    beyond_once_buy_max = "LC_ALERT_beyond_once_buy_max",
    can_not_buy = "LC_ALERT_can_not_buy",
    level_no_enough = "LC_ALERT_level_no_enough",
    not_on_level = "LC_ALERT_not_on_level",
    rush_deny_case_item_full = "LC_ALERT_rush_deny_case_item_full",
    bad_fight_record_id = "LC_ALERT_bad_fight_record_id",
    bad_prestige_level = "LC_ALERT_bad_prestige_level",
    prestige_not_achieve = "LC_ALERT_prestige_not_achieve",
    prestige_already_obtained = "LC_ALERT_prestige_already_obtained",
    add_friends_error = "LC_ALERT_add_friends_error",
    del_friends_error = "LC_ALERT_del_friends_error",
    supply_type_error = "LC_ALERT_supply_type_error",
    quest_id_unvalid = "LC_ALERT_quest_id_unvalid",
    quest_un_finish = "LC_ALERT_quest_un_finish",
    rush_module_not_enabled = "LC_ALERT_rush_module_not_enabled",
    task_not_finish = "LC_ALERT_task_not_finish",
    officer_not_found = "LC_ALERT_officer_not_found",
    tipoff_self = "LC_ALERT_tipoff_self",
    special_battle_id_error = "LC_ALERT_special_battle_id_error",
    player_munte_already = "LC_ALERT_player_munte_already",
    officer_less_than_minlevel = "LC_ALERT_officer_less_than_minlevel",
    laba_need_get_award = "LC_ALERT_laba_need_get_award",
    laba_empty = "LC_ALERT_laba_empty",
    got_reward_error = "LC_ALERT_got_reward_error",
    not_enough_stars_error = "LC_ALERT_not_enough_stars_error",
    send_allstars_reward_error = "LC_ALERT_send_allstars_reward_error",
    fleet_sale_with_rebate_timeout = "LC_ALERT_fleet_sale_with_rebate_timeout",
    prestige_award_cannot_got = "LC_ALERT_prestige_award_cannot_got",
    prestige_award_has_got = "LC_ALERT_prestige_award_has_got",
    fleet_recruit_unlock = "LC_ALERT_fleet_recruit_unlock",
    coating_buff_added = "LC_ALERT_coating_buff_added",
    same_buff_cannot_use = "LC_ALERT_same_buff_cannot_use",
    item_20013_use_confirm_alert = "LC_ALERT_item_20013_use_confirm_alert",
    item_full = "LC_ALERT_item_full",
    item_add_failed = "LC_ALERT_item_add_failed",
    item_owner_error = "LC_ALERT_item_owner_error",
    equip_can_not_use = "LC_ALERT_equip_can_not_use",
    item_can_not_use = "LC_ALERT_item_can_not_use",
    item_use_error = "LC_ALERT_item_use_error",
    item_can_not_use_level_limit = "LC_ALERT_item_can_not_use_level_limit",
    equip_can_not_use_level_limit = "LC_ALERT_equip_can_not_use_level_limit",
    item_cnt_not_enough = "LC_ALERT_item_cnt_not_enough",
    ph_atk_equip_can_not_use = "LC_ALERT_ph_atk_equip_can_not_use",
    en_atk_equip_can_not_use = "LC_ALERT_en_atk_equip_can_not_use",
    item_use_max_error = "LC_ALERT_item_use_max_error",
    event_krypton_three_alert = "LC_ALERT_event_krypton_three_alert",
    event_krypton_four_alert = "LC_ALERT_event_krypton_four_alert",
    event_krypton_five_alert = "LC_ALERT_event_krypton_five_alert",
    event_krypton_chip_alert = "LC_ALERT_event_krypton_chip_alert",
    error_bag_full_cannot_get_krypton_chip = "LC_ALERT_error_bag_full_cannot_get_krypton_chip",
    event_krypton_core_three_alert = "LC_ALERT_event_krypton_core_three_alert",
    event_krypton_core_four_alert = "LC_ALERT_event_krypton_core_four_alert",
    item_move_not_allowed = "LC_ALERT_item_move_not_allowed",
    item_not_exist = "LC_ALERT_item_not_exist",
    shop_item_can_not_sell = "LC_ALERT_shop_item_can_not_sell",
    error_bug_full_rewards_cannot_get = "LC_ALERT_error_bug_full_rewards_cannot_get",
    suipian_not_enough = "LC_ALERT_suipian_not_enough",
    suipian_compose_already = "LC_ALERT_suipian_compose_already",
    christmasday_has_gone = "LC_ALERT_christmasday_has_gone",
    pay_gift_item_full = "LC_ALERT_pay_gift_item_full",
    sign_up_can_not_get = "LC_ALERT_sign_up_can_not_get",
    sign_up_already_get = "LC_ALERT_sign_up_already_get",
    activity_not_active = "LC_ALERT_activity_not_active",
    activity_not_begin = "LC_ALERT_activity_not_begin",
    activity_already_end = "LC_ALERT_activity_already_end",
    already_in_block_devil = "LC_ALERT_already_in_block_devil",
    not_in_activity_time = "LC_ALERT_not_in_activity_time",
    wrong_cutoff_id = "LC_ALERT_wrong_cutoff_id",
    activity_task_reward_unfinished = "LC_ALERT_activity_task_reward_unfinished",
    activity_task_reward_rewarded = "LC_ALERT_activity_task_reward_rewarded",
    activity_store_fleet_failed = "LC_ALERT_activity_store_fleet_failed",
    activity_store_item_failed = "LC_ALERT_activity_store_item_failed",
    store_day_exchanged_times_limit = "LC_ALERT_store_day_exchanged_times_limit",
    store_all_exchanged_times_limit = "LC_ALERT_store_all_exchanged_times_limit",
    already_joined_activity = "LC_ALERT_already_joined_activity",
    loot_already_get = "LC_ALERT_loot_already_get",
    error_active_rewards_type_req = "LC_ALERT_error_active_rewards_type_req",
    error_active_rewards_lost = "LC_ALERT_error_active_rewards_lost",
    can_not_award_in_promotion = "LC_ALERT_can_not_award_in_promotion",
    enter_next_activity_error = "LC_ALERT_enter_next_activity_error",
    read_activity_config_error = "LC_ALERT_read_activity_config_error",
    charge_activity_dna_error = "LC_ALERT_charge_activity_dna_error",
    already_had_fleet = "LC_ALERT_already_had_fleet",
    refresh_times_limited = "LC_ALERT_refresh_times_limited",
    matrix_cell_not_active = "LC_ALERT_matrix_cell_not_active",
    pve_supply_loot_not_valid = "LC_ALERT_pve_supply_loot_not_valid",
    krypton_six_need_level_50 = "LC_ALERT_krypton_six_need_level_50",
    mail_send_not_complete = "LC_ALERT_mail_send_not_complete",
    mail_send_failed = "LC_ALERT_mail_send_failed",
    pay_duplicated_verify = "LC_ALERT_pay_duplicated_verify",
    pay_apple_verify_error = "LC_ALERT_pay_apple_verify_error",
    pay_360_price_error = "LC_ALERT_pay_360_price_error",
    tc_mission_condition_error = "LC_ALERT_tc_mission_condition_error",
    tc_not_in_activity = "LC_ALERT_tc_not_in_activity",
    wormhole_reset_less = "LC_ALERT_wormhole_reset_less",
    wormhole_reset_one = "LC_ALERT_wormhole_reset_one",
    wormhole_raids_max = "LC_ALERT_wormhole_raids_max",
    wormhole_search_cost = "LC_ALERT_wormhole_search_cost",
    wormhole_reset_cost = "LC_ALERT_wormhole_reset_cost",
    wormhole_reset_max = "LC_ALERT_wormhole_reset_max",
    dungeon_wormhole_level = "LC_ALERT_dungeon_wormhole_level",
    wormhole_search_max = "LC_ALERT_wormhole_search_max",
    wormhole_search_error = "LC_ALERT_wormhole_search_error",
    wormhole_raids_end = "LC_ALERT_wormhole_raids_end",
    wormhole_search_on_max = "LC_ALERT_wormhole_search_on_max",
    dungeon_expedition_close = "LC_ALERT_dungeon_expedition_close",
    expedition_level_limited = "LC_ALERT_expedition_level_limited",
    interference_equipe_not_exist = "LC_ALERT_interference_equipe_not_exist",
    interference_max_level = "LC_ALERT_interference_max_level",
    interference_config_not_exist = "LC_ALERT_interference_config_not_exist",
    interference_item_not_enough = "LC_ALERT_interference_item_not_enough",
    interference_addition_item_not_enough = "LC_ALERT_interference_addition_item_not_enough",
    interference_addition_credit_not_enough = "LC_ALERT_interference_addition_credit_not_enough",
    tbc_level_less = "LC_ALERT_tbc_level_less",
    tbc_cross_cost = "LC_ALERT_tbc_cross_cost",
    tbc_no_self_fleet = "LC_ALERT_tbc_no_self_fleet",
    tbc_repair_max = "LC_ALERT_tbc_repair_max",
    tbc_reward_pos = "LC_ALERT_tbc_reward_pos",
    tbc_reward_pass = "LC_ALERT_tbc_reward_pass",
    tbc_fight_pos = "LC_ALERT_tbc_fight_pos",
    tbc_special_error = "LC_ALERT_tbc_special_error",
    tbc_cross_less = "LC_ALERT_tbc_cross_less",
    tbc_get_enemy_fail = "LC_ALERT_tbc_get_enemy_fail",
    tbc_fight_no_fleet = "LC_ALERT_tbc_fight_no_fleet",
    tbc_fight_fleet_not_enough = "LC_ALERT_tbc_fight_fleet_not_enough",
    budo_join_limit = "LC_ALERT_budo_join_limit",
    budo_did_not_open = "LC_ALERT_budo_did_not_open",
    can_not_join = "LC_ALERT_can_not_join",
    sign_up_error = "LC_ALERT_sign_up_error",
    get_reward_error = "LC_ALERT_get_reward_error",
    get_rank_error = "LC_ALERT_get_rank_error",
    get_replay_error = "LC_ALERT_get_replay_error",
    change_formotion_error = "LC_ALERT_change_formotion_error",
    already_support = "LC_ALERT_already_support",
    wrong_round = "LC_ALERT_wrong_round",
    change_formotion_error_2 = "LC_ALERT_change_formotion_error_2",
    change_formotion_error_3 = "LC_ALERT_change_formotion_error_3",
    over_max_adjutant = "LC_ALERT_over_max_adjutant",
    already_major = "LC_ALERT_already_major",
    already_adjutant = "LC_ALERT_already_adjutant",
    vessels_not_map = "LC_ALERT_vessels_not_map",
    major_on_matrix = "LC_ALERT_major_on_matrix",
    adjutant_on_matrix = "LC_ALERT_adjutant_on_matrix",
    fleet_already_dismiss = "LC_ALERT_fleet_already_dismiss",
    no_mech_levelup = "LC_ALERT_no_mech_levelup",
    mech_levelup_credit_error = "LC_ALERT_mech_levelup_credit_error",
    mech_is_levelup = "LC_ALERT_mech_is_levelup",
    mech_already_max_level = "LC_ALERT_mech_already_max_level",
    remodel_money_not_enough = "LC_ALERT_remodel_money_not_enough",
    remodel_server_error = "LC_ALERT_remodel_server_error",
    need_join_alliance = "LC_ALERT_need_join_alliance",
    mech_type_error = "LC_ALERT_mech_type_error",
    help_list_is_null = "LC_ALERT_help_list_is_null",
    no_enough_item = "LC_ALERT_no_enough_item",
    already_max_level = "LC_ALERT_already_max_level",
    no_facebook_bound = "LC_ALERT_no_facebook_bound",
    financial_use_out = "LC_ALERT_financial_use_out",
    krypton_inject_condition_lack = "LC_ALERT_krypton_inject_condition_lack",
    krypton_refine_required_error = "LC_ALERT_krypton_refine_required_error",
    krypton_inject_times_lack = "LC_ALERT_krypton_inject_times_lack",
    krypton_kenergy_lack = "LC_ALERT_krypton_kenergy_lack",
    krypton_credit_lack = "LC_ALERT_krypton_credit_lack",
    krypton_vip_lack = "LC_ALERT_krypton_vip_lack",
    krypton_refine_max_rank = "LC_ALERT_krypton_refine_max_rank",
    krypton_refine_exp_lack = "LC_ALERT_krypton_refine_exp_lack",
    krypton_refine_can_not = "LC_ALERT_krypton_refine_can_not",
    krypton_refine_equiped = "LC_ALERT_krypton_refine_equiped",
    krypton_can_not_find = "LC_ALERT_krypton_can_not_find",
    krypton_inject_item_lack = "LC_ALERT_krypton_inject_item_lack",
    times_can_not_buy = "LC_ALERT_times_can_not_buy",
    wve_duplicate_powerup = "LC_ALERT_wve_duplicate_powerup",
    wve_dot_conflict = "LC_ALERT_wve_dot_conflict",
    wve_jump_not_in_dot = "LC_ALERT_wve_jump_not_in_dot",
    wve_player_not_in_dot = "LC_ALERT_wve_player_not_in_dot",
    wve_player_already_prepare_march = "LC_ALERT_wve_player_already_prepare_march",
    wve_not_open = "LC_ALERT_wve_not_open",
    straight_award_can_not = "LC_ALERT_straight_award_can_not",
    straight_award_server_error = "LC_ALERT_straight_award_server_error",
    all_welfare_over = "LC_ALERT_all_welfare_over",
    all_welfare_no_level = "LC_ALERT_all_welfare_no_level",
    all_welfare_no_enough_person = "LC_ALERT_all_welfare_no_enough_person",
    all_wefare_already_loot = "LC_ALERT_all_wefare_already_loot",
    open_server_fund_level_not_enough = "LC_ALERT_open_server_fund_level_not_enough",
    open_server_fund_no_level = "LC_ALERT_open_server_fund_no_level",
    open_server_fund_vip_error = "LC_ALERT_open_server_fund_vip_error",
    open_server_fund_credit_error = "LC_ALERT_open_server_fund_credit_error",
    already_charged = "LC_ALERT_already_charged",
    open_server_fund_already_loot = "LC_ALERT_open_server_fund_already_loot",
    open_server_fund_not_charged = "LC_ALERT_open_server_fund_not_charged",
    use_item_1011_succ = "LC_ALERT_use_item_1011_succ",
    daily_benefits_over = "LC_ALERT_daily_benefits_over",
    no_benefits_award = "LC_ALERT_no_benefits_award",
    benefits_awards_server_error = "LC_ALERT_benefits_awards_server_error",
    purchase_credit_not_enough = "LC_ALERT_purchase_credit_not_enough",
    benefits_award_already = "LC_ALERT_benefits_award_already",
    benefits_open_lock = "LC_ALERT_benefits_open_lock",
    cutoff_item_over = "LC_ALERT_cutoff_item_over",
    cutoff_item_locked = "LC_ALERT_cutoff_item_locked",
    cutoff_server_error = "LC_ALERT_cutoff_server_error",
    cutoff_item_already = "LC_ALERT_cutoff_item_already",
    cutoff_item_over_max = "LC_ALERT_cutoff_item_over_max",
    cutoff_resource_not_enough = "LC_ALERT_cutoff_resource_not_enough",
    tactical_equip_max = "LC_ALERT_tactical_equip_max",
    tactical_vessel_invalid = "LC_ALERT_tactical_vessel_invalid",
    tactical_equip_invalid = "LC_ALERT_tactical_equip_invalid",
    tactical_lock_max = "LC_ALERT_tactical_lock_max",
    tactical_attribute_invalid = "LC_ALERT_tactical_attribute_invalid",
    tactical_unreach_condition = "LC_ALERT_tactical_unreach_condition",
    tactical_on_slot = "LC_ALERT_tactical_on_slot",
    plante_explore_cd_limit = "LC_ALERT_plante_explore_cd_limit",
    plante_explore_activity_id_error = "LC_ALERT_plante_explore_activity_id_error",
    star_id_error = "LC_ALERT_star_id_error",
    wdc_no_match = "LC_ALERT_wdc_no_match",
    wdc_enemy_protected = "LC_ALERT_wdc_enemy_protected",
    ac_level_max = "LC_ALERT_ac_level_max",
    krypton_seven_need_level_70 = "LC_ALERT_krypton_seven_need_level_70",
    equipment_more_than_player_level = "LC_ALERT_equipment_more_than_player_level",
    sw_planet_state_changed = "LC_ALERT_sw_planet_state_changed",
    sw_no_remaining_plunder_times = "LC_ALERT_sw_no_remaining_plunder_times",
    sw_has_owned_aplanet = "LC_ALERT_sw_has_owned_aplanet",
    limit_by_pve = "LC_ALERT_limit_by_pve",
    iap_monthly_card_invaild = "LC_ALERT_iap_monthly_card_invaild",
    team_full = "LC_ALERT_team_full",
    already_in_team = "LC_ALERT_already_in_team",
    not_in_team = "LC_ALERT_not_in_team",
    team_not_exist = "LC_ALERT_team_not_exist",
    power_not_match = "LC_ALERT_power_not_match",
    canot_create_team = "LC_ALERT_canot_create_team",
    fair_award_failed = "LC_ALERT_fair_award_failed",
    can_not_update_formation = "LC_ALERT_can_not_update_formation",
    artifact_upgrade_max = "LC_ALERT_artifact_upgrade_max",
    artifact_reset_error = "LC_ALERT_artifact_reset_error",
    artifact_upgrade_no_art_point = "LC_ALERT_artifact_upgrade_no_art_point",
    equip_change_not_enough_space = "LC_ALERT_equip_change_not_enough_space",
    addition_item_level_limit = "LC_ALERT_addition_item_level_limit",
    medal_not_exist = "LC_ALERT_medal_not_exist",
    medal_level_up_failed = "LC_ALERT_medal_level_up_failed",
    medal_rank_up_level_limit = "LC_ALERT_medal_rank_up_level_limit",
    medal_rank_up_material_limit = "LC_ALERT_medal_rank_up_material_limit",
    medal_rank_up_target_error = "LC_ALERT_medal_rank_up_target_error",
    medal_rank_up_failed = "LC_ALERT_medal_rank_up_failed",
    medal_decompose_failed = "LC_ALERT_medal_decompose_failed",
    medal_reset_failed = "LC_ALERT_medal_reset_failed",
    medal_recycle_close = "LC_ALERT_medal_recycle_close",
    fleet_medal_equip_on_failed = "LC_ALERT_fleet_medal_equip_on_failed",
    fleet_medal_equip_off_failed = "LC_ALERT_fleet_medal_equip_off_failed",
    fleet_medal_replace_failed = "LC_ALERT_fleet_medal_replace_failed",
    fleet_medal_target_pos_already_equip_on = "LC_ALERT_fleet_medal_target_pos_already_equip_on",
    fleet_medal_target_pos_type_error = "LC_ALERT_fleet_medal_target_pos_type_error",
    item_already_buy = "LC_ALERT_item_already_buy",
    item_can_not_buy = "LC_ALERT_item_can_not_buy",
    resource_not_enough = "LC_ALERT_resource_not_enough",
    activity_not_start = "LC_ALERT_activity_not_start",
    refresh_all_limit = "LC_ALERT_refresh_all_limit",
    medal_recruit_all_limit = "LC_ALERT_medal_recruit_all_limit",
    item_not_enough = "LC_ALERT_item_not_enough",
    medal_glory_already_activate = "LC_ALERT_medal_glory_already_activate",
    medal_glory_not_activate = "LC_ALERT_medal_glory_not_activate",
    medal_glory_cannot_activate = "LC_ALERT_medal_glory_cannot_activate",
    medal_glory_cannot_rank_up = "LC_ALERT_medal_glory_cannot_rank_up",
    medal_collection_not_open = "LC_ALERT_medal_collection_not_open",
    medal_achievement_no_activate = "LC_ALERT_medal_achievement_no_activate",
    challenge_time_limit = "LC_ALERT_challenge_time_limit",
    is_not_in_activity = "LC_ALERT_is_not_in_activity",
    in_marching = "LC_ALERT_in_marching",
    packet_overflow = "LC_ALERT_packet_overflow",
    map_duplicate_legion_name = "LC_ALERT_map_duplicate_legion_name",
    map_quit_legion = "LC_ALERT_map_quit_legion",
    map_no_fuel = "LC_ALERT_map_no_fuel",
    map_full_player = "LC_ALERT_map_full_player",
    map_join_legion_fail = "LC_ALERT_map_join_legion_fail",
    map_team_change = "LC_ALERT_map_team_change",
    map_team_full = "LC_ALERT_map_team_full",
    map_repeat_team = "LC_ALERT_map_repeat_team",
    map_assemble_fail = "LC_ALERT_map_assemble_fail",
    map_assemble_not_enough = "LC_ALERT_map_assemble_not_enough",
    map_occupy_quit_legion = "LC_ALERT_map_occupy_quit_legion",
    map_power_limit = "LC_ALERT_map_power_limit",
    map_in_marching_occupy_quit = "LC_ALERT_map_in_marching_occupy_quit",
    map_accept_join_legion_fail = "LC_ALERT_map_accept_join_legion_fail",
    tlc_no_match = "LC_ALERT_tlc_no_match",
    tlc_enemy_protected = "LC_ALERT_tlc_enemy_protected",
    gcs_no_available_change_times = "LC_ALERT_gcs_no_available_change_times",
    gcs_get_award_status_wrong = "LC_ALERT_gcs_get_award_status_wrong",
    gcs_no_available_lv_awards = "LC_ALERT_gcs_no_available_lv_awards",
    gcs_cannot_find_new_task = "LC_ALERT_gcs_cannot_find_new_task",
    gcs_task_not_in_list = "LC_ALERT_gcs_task_not_in_list",
    gcs_close = "LC_ALERT_gcs_close"
  }
}
AlertDataList.K = {}
AlertDataList.K[0] = {kind = "Tip", text = "ok"}
AlertDataList.K[5] = {kind = "Tip", text = "bad_param"}
AlertDataList.K[100004] = {
  kind = "Tip",
  text = "server_timeout"
}
AlertDataList.K[100005] = {kind = "Tip", text = "timeout"}
AlertDataList.K[100002] = {
  kind = "Tip",
  text = "inner_error"
}
AlertDataList.K[100000] = {
  kind = "Tip",
  text = "database_error"
}
AlertDataList.K[100001] = {
  kind = "Tip",
  text = "database_busy"
}
AlertDataList.K[101001] = {
  kind = "Tip",
  text = "player_process_not_exist"
}
AlertDataList.K[200] = {kind = "Tip", text = "udid_error"}
AlertDataList.K[302] = {
  kind = "Tip",
  text = "server_try_again"
}
AlertDataList.K[11005] = {
  kind = "Tip",
  text = "server_ask_for_reload"
}
AlertDataList.K[11006] = {
  kind = "Tip",
  text = "server_ask_for_offline"
}
AlertDataList.K[10107] = {
  kind = "Tip",
  text = "client_version_up"
}
AlertDataList.K[11004] = {
  kind = "Tip",
  text = "invalid_player_id"
}
AlertDataList.K[10110] = {
  kind = "Tip",
  text = "get_buff_error"
}
AlertDataList.K[100003] = {
  kind = "Tip",
  text = "player_init_error"
}
AlertDataList.K[100006] = {
  kind = "Tip",
  text = "resource_limit_error"
}
AlertDataList.K[15] = {
  kind = "Tip",
  text = "usual_error"
}
AlertDataList.K[1] = {
  kind = "Tip",
  text = "bad_user_name"
}
AlertDataList.K[2] = {
  kind = "Tip",
  text = "bad_password"
}
AlertDataList.K[3] = {
  kind = "Tip",
  text = "user_not_exist"
}
AlertDataList.K[4] = {
  kind = "Tip",
  text = "user_login_already"
}
AlertDataList.K[6] = {
  kind = "Tip",
  text = "user_register_error"
}
AlertDataList.K[7] = {
  kind = "Tip",
  text = "user_register_db_exception"
}
AlertDataList.K[8] = {
  kind = "Tip",
  text = "user_login_error"
}
AlertDataList.K[9] = {
  kind = "Tip",
  text = "user_login_db_exception"
}
AlertDataList.K[10] = {
  kind = "Tip",
  text = "user_find_db_exception"
}
AlertDataList.K[11] = {
  kind = "Tip",
  text = "user_passport_and_player_not_match"
}
AlertDataList.K[103] = {
  kind = "Tip",
  text = "username_exist"
}
AlertDataList.K[104] = {
  kind = "Tip",
  text = "userudid_not_match"
}
AlertDataList.K[105] = {
  kind = "Tip",
  text = "usercenter_error"
}
AlertDataList.K[106] = {
  kind = "Tip",
  text = "usercenter_login_valid_error"
}
AlertDataList.K[107] = {
  kind = "Tip",
  text = "usercenter_register_device_error"
}
AlertDataList.K[108] = {
  kind = "Tip",
  text = "usercenter_create_user_and_device"
}
AlertDataList.K[109] = {
  kind = "Tip",
  text = "usercenter_bind_user_and_device"
}
AlertDataList.K[110] = {
  kind = "Tip",
  text = "usercenter_create_error"
}
AlertDataList.K[111] = {
  kind = "Tip",
  text = "usercenter_device_unvalid"
}
AlertDataList.K[112] = {
  kind = "Tip",
  text = "username_too_short"
}
AlertDataList.K[113] = {
  kind = "Tip",
  text = "login_faild"
}
AlertDataList.K[114] = {
  kind = "Tip",
  text = "usercenter_timeout"
}
AlertDataList.K[115] = {
  kind = "Tip",
  text = "username_too_long"
}
AlertDataList.K[116] = {
  kind = "Tip",
  text = "username_unvalid"
}
AlertDataList.K[101016] = {
  kind = "Tip",
  text = "facebook_account_wrong"
}
AlertDataList.K[101017] = {
  kind = "Tip",
  text = "facebook_account_timeout"
}
AlertDataList.K[11002] = {
  kind = "Tip",
  text = "player_ban_already"
}
AlertDataList.K[118] = {
  kind = "Tip",
  text = "username_unfriendly"
}
AlertDataList.K[800002] = {
  kind = "Tip",
  text = "award_e203_one"
}
AlertDataList.K[800003] = {
  kind = "Tip",
  text = "award_e203_two"
}
AlertDataList.K[800004] = {
  kind = "Tip",
  text = "award_e203_three"
}
AlertDataList.K[800005] = {
  kind = "Tip",
  text = "award_e203_four"
}
AlertDataList.K[800006] = {
  kind = "Tip",
  text = "award_e203_five"
}
AlertDataList.K[800007] = {
  kind = "Tip",
  text = "award_e203_six"
}
AlertDataList.K[800008] = {
  kind = "Tip",
  text = "award_e203_seven"
}
AlertDataList.K[300001] = {kind = "Tip", text = "award_w304"}
AlertDataList.K[300002] = {
  kind = "Tip",
  text = "award_w304_timeout"
}
AlertDataList.K[300003] = {kind = "Tip", text = "award_e305"}
AlertDataList.K[101018] = {
  kind = "Tip",
  text = "wrong_action_type_purchase"
}
AlertDataList.K[110001] = {
  kind = "Tip",
  text = "colony_defender_same_as_attcker"
}
AlertDataList.K[110002] = {
  kind = "Tip",
  text = "colony_slave_yet_owned"
}
AlertDataList.K[110003] = {
  kind = "Tip",
  text = "colony_actual_not_expect_one"
}
AlertDataList.K[110004] = {
  kind = "Tip",
  text = "colony_cdtime_not_enough"
}
AlertDataList.K[110101] = {
  kind = "Tip",
  text = "colony_capture_diff_level_more_than_max"
}
AlertDataList.K[110102] = {
  kind = "Tip",
  text = "colony_capture_position_as_slave"
}
AlertDataList.K[110103] = {
  kind = "Tip",
  text = "colony_capture_times_more_than_max"
}
AlertDataList.K[110104] = {
  kind = "Tip",
  text = "colony_capture_slave_more_than_max"
}
AlertDataList.K[110105] = {
  kind = "Tip",
  text = "colony_capture_actual_not_slaveholder"
}
AlertDataList.K[110302] = {
  kind = "Tip",
  text = "colony_help_attcker_same_as_slave"
}
AlertDataList.K[110106] = {
  kind = "Tip",
  text = "colony_capture_actual_not_civilian"
}
AlertDataList.K[110303] = {
  kind = "Tip",
  text = "colony_help_position_as_not_slave"
}
AlertDataList.K[110107] = {
  kind = "Tip",
  text = "colony_capture_any_slave_not_owned"
}
AlertDataList.K[110304] = {
  kind = "Tip",
  text = "colony_help_times_more_than_max"
}
AlertDataList.K[110201] = {
  kind = "Tip",
  text = "colony_rescue_slave_same_as_owner"
}
AlertDataList.K[110401] = {
  kind = "Tip",
  text = "colony_resist_defender_different_from_owner"
}
AlertDataList.K[110202] = {
  kind = "Tip",
  text = "colony_rescue_attcker_same_as_slave"
}
AlertDataList.K[110402] = {
  kind = "Tip",
  text = "colony_resist_position_as_not_slave"
}
AlertDataList.K[110203] = {
  kind = "Tip",
  text = "colony_rescue_position_as_slave"
}
AlertDataList.K[110403] = {
  kind = "Tip",
  text = "colony_resist_times_more_than_max"
}
AlertDataList.K[110204] = {
  kind = "Tip",
  text = "colony_rescue_times_more_than_max"
}
AlertDataList.K[110501] = {
  kind = "Tip",
  text = "colony_release_owner_same_as_slave"
}
AlertDataList.K[110205] = {
  kind = "Tip",
  text = "colony_rescue_actual_not_slave"
}
AlertDataList.K[110502] = {
  kind = "Tip",
  text = "colony_release_position_as_not_slaveholder"
}
AlertDataList.K[110206] = {
  kind = "Tip",
  text = "colony_rescue_actual_not_slaveholder"
}
AlertDataList.K[110503] = {
  kind = "Tip",
  text = "colony_release_slave_not_owned"
}
AlertDataList.K[110301] = {
  kind = "Tip",
  text = "colony_help_slave_same_as_owner"
}
AlertDataList.K[110504] = {
  kind = "Tip",
  text = "colony_release_actual_not_slave"
}
AlertDataList.K[110701] = {
  kind = "Tip",
  text = "colony_exploit_position_as_not_slaveholder"
}
AlertDataList.K[110702] = {
  kind = "Tip",
  text = "colony_exploit_slave_not_owned"
}
AlertDataList.K[110703] = {
  kind = "Tip",
  text = "colony_exploit_actual_not_slave"
}
AlertDataList.K[110704] = {
  kind = "Tip",
  text = "colony_exploit_owner_changed"
}
AlertDataList.K[110705] = {
  kind = "Tip",
  text = "colony_exploit_require_more_credit"
}
AlertDataList.K[110801] = {
  kind = "Tip",
  text = "colony_colony_fawn_position_as_not_slaveholder"
}
AlertDataList.K[110802] = {
  kind = "Tip",
  text = "colony_fawn_slave_not_owned"
}
AlertDataList.K[110803] = {
  kind = "Tip",
  text = "colony_fawn_times_more_than_max_times"
}
AlertDataList.K[110804] = {
  kind = "Tip",
  text = "colony_fawn_actual_not_slave"
}
AlertDataList.K[110805] = {
  kind = "Tip",
  text = "colony_fawn_owner_change"
}
AlertDataList.K[110901] = {
  kind = "Tip",
  text = "colony_fawn_position_as_not_slave"
}
AlertDataList.K[110902] = {
  kind = "Tip",
  text = "colony_fawn_owner_changed"
}
AlertDataList.K[110903] = {
  kind = "Tip",
  text = "colony_fawn_owner_more_than_max_times"
}
AlertDataList.K[501] = {
  kind = "Tip",
  text = "recruit_fleet_bad_fleet_id"
}
AlertDataList.K[502] = {
  kind = "Tip",
  text = "recruit_fleet_exist"
}
AlertDataList.K[503] = {
  kind = "Tip",
  text = "recruit_fleet_level_err"
}
AlertDataList.K[504] = {
  kind = "Tip",
  text = "recruit_fleet_prestige_err"
}
AlertDataList.K[505] = {
  kind = "Tip",
  text = "recruit_fleet_quest_err"
}
AlertDataList.K[506] = {
  kind = "Tip",
  text = "recruit_fleet_lepton_err"
}
AlertDataList.K[508] = {
  kind = "Tip",
  text = "recruit_fleet_quark_err"
}
AlertDataList.K[509] = {
  kind = "Tip",
  text = "recruit_fleet_udid_err"
}
AlertDataList.K[510] = {
  kind = "Tip",
  text = "recruit_fleet_challenge_err"
}
AlertDataList.K[511] = {
  kind = "Tip",
  text = "recruit_fleet_krypton_err"
}
AlertDataList.K[512] = {
  kind = "Tip",
  text = "recruit_fleet_max_cnt_err"
}
AlertDataList.K[601] = {
  kind = "Tip",
  text = "friend_free_over_max"
}
AlertDataList.K[602] = {
  kind = "Tip",
  text = "friend_lock_over_max"
}
AlertDataList.K[603] = {
  kind = "Tip",
  text = "friend_not_exist"
}
AlertDataList.K[604] = {
  kind = "Tip",
  text = "friend_already"
}
AlertDataList.K[605] = {
  kind = "Tip",
  text = "friend_ban_already"
}
AlertDataList.K[606] = {
  kind = "Tip",
  text = "friend_del_already"
}
AlertDataList.K[607] = {
  kind = "Tip",
  text = "friend_ban_del_already"
}
AlertDataList.K[608] = {
  kind = "Tip",
  text = "friend_add_self"
}
AlertDataList.K[609] = {
  kind = "Tip",
  text = "friend_ban_self"
}
AlertDataList.K[6191] = {
  kind = "Tip",
  text = "refresh_max_times"
}
AlertDataList.K[6192] = {kind = "Tip", text = "cost_limit"}
AlertDataList.K[8814] = {
  kind = "Tip",
  text = "alliance_kick_not_allowed_in_wd_activity"
}
AlertDataList.K[8815] = {
  kind = "Tip",
  text = "alliance_quit_not_allowed_in_wd_activity"
}
AlertDataList.K[8816] = {
  kind = "Tip",
  text = "alliance_delete_not_allowed_in_wd_activity"
}
AlertDataList.K[8817] = {
  kind = "Tip",
  text = "alliance_kick_not_allowed_in_matched_wd"
}
AlertDataList.K[8818] = {
  kind = "Tip",
  text = "alliance_quit_not_allowed_in_matched_wd"
}
AlertDataList.K[8819] = {
  kind = "Tip",
  text = "alliance_delete_not_allowed_in_matched_wd"
}
AlertDataList.K[8820] = {
  kind = "Tip",
  text = "alliance_ratify_not_allowed_in_matched_wd"
}
AlertDataList.K[8821] = {
  kind = "Tip",
  text = "alliance_defence_repair_not_allowed_in_matched_wd"
}
AlertDataList.K[8822] = {
  kind = "Tip",
  text = "alliance_set_defender_not_allowed_in_matched_wd"
}
AlertDataList.K[8823] = {
  kind = "Tip",
  text = "alliance_wd_join_not_allowed_in_non_activity"
}
AlertDataList.K[8824] = {
  kind = "Tip",
  text = "alliance_wd_join_yet_matched"
}
AlertDataList.K[8825] = {
  kind = "Tip",
  text = "alliance_wd_challenge_is_over"
}
AlertDataList.K[8826] = {
  kind = "Tip",
  text = "alliance_wd_challenge_waiting_to_match"
}
AlertDataList.K[8860] = {
  kind = "Tip",
  text = "alliance_wd_level_limit"
}
AlertDataList.K[8861] = {
  kind = "Tip",
  text = "alliance_wd_challenge_defence_leader_alive"
}
AlertDataList.K[8862] = {
  kind = "Tip",
  text = "alliance_wd_challenge_require_more_credit"
}
AlertDataList.K[8863] = {
  kind = "Tip",
  text = "alliance_wd_challenge_defence_leader_yet_dead"
}
AlertDataList.K[8864] = {
  kind = "Tip",
  text = "alliance_wd_defence_wall_yet_destroied"
}
AlertDataList.K[8865] = {
  kind = "Tip",
  text = "alliance_wd_defence_core_under_protected"
}
AlertDataList.K[8866] = {
  kind = "Tip",
  text = "alliance_wd_memeber_not_enough"
}
AlertDataList.K[8871] = {
  kind = "Tip",
  text = "alliance_flag_modification_permission_not_owned"
}
AlertDataList.K[8872] = {
  kind = "Tip",
  text = "alliance_flag_yet_setted"
}
AlertDataList.K[8873] = {
  kind = "Tip",
  text = "alliance_defence_leader_not_dead"
}
AlertDataList.K[8874] = {
  kind = "Tip",
  text = "alliance_defence_leader_revive_require_more_credit"
}
AlertDataList.K[8875] = {
  kind = "Tip",
  text = "alliance_defence_leader_revive_not_allowed_in_non_battle"
}
AlertDataList.K[8880] = {
  kind = "Tip",
  text = "server_busy"
}
AlertDataList.K[8881] = {
  kind = "Tip",
  text = "alliance_defender_not_kicked"
}
AlertDataList.K[8882] = {
  kind = "Tip",
  text = "alliance_brick_donation_require_more_brick"
}
AlertDataList.K[8883] = {
  kind = "Tip",
  text = "alliance_repair_permission_not_owned"
}
AlertDataList.K[8884] = {
  kind = "Tip",
  text = "alliance_repair_require_more_brick"
}
AlertDataList.K[8885] = {
  kind = "Tip",
  text = "alliance_assignment_permission_not_owned"
}
AlertDataList.K[8886] = {
  kind = "Tip",
  text = "alliance_defence_leader_yet_owned"
}
AlertDataList.K[900006] = {
  kind = "Tip",
  text = "activity_task_reward_wd_unfinished"
}
AlertDataList.K[900007] = {
  kind = "Tip",
  text = "wd_last_hour_cannot_fight"
}
AlertDataList.K[8887] = {
  kind = "Tip",
  text = "alliance_defence_leader_actual_not_leader"
}
AlertDataList.K[8888] = {
  kind = "Tip",
  text = "alliance_defence_leader_actual_is_leader"
}
AlertDataList.K[8890] = {
  kind = "Tip",
  text = "alliance_approval_permission_not_owned"
}
AlertDataList.K[8891] = {
  kind = "Tip",
  text = "alliance_apply_not_found"
}
AlertDataList.K[8892] = {
  kind = "Tip",
  text = "alliance_full"
}
AlertDataList.K[8893] = {
  kind = "Tip",
  text = "alliance_not_yet_ratified"
}
AlertDataList.K[8894] = {
  kind = "Tip",
  text = "alliance_member_not_found"
}
AlertDataList.K[8895] = {
  kind = "Tip",
  text = "alliance_transfer_permission_not_owned"
}
AlertDataList.K[8896] = {
  kind = "Tip",
  text = "alliance_creator_not_quit"
}
AlertDataList.K[8897] = {
  kind = "Tip",
  text = "alliance_creator_can_delete"
}
AlertDataList.K[8898] = {
  kind = "Tip",
  text = "alliance_creator_not_kicked"
}
AlertDataList.K[8800] = {
  kind = "Tip",
  text = "alliance_not_found"
}
AlertDataList.K[8801] = {
  kind = "Tip",
  text = "alliance_player_level_limit"
}
AlertDataList.K[8802] = {
  kind = "Tip",
  text = "alliance_require_more_money"
}
AlertDataList.K[8803] = {
  kind = "Tip",
  text = "alliance_name_existed"
}
AlertDataList.K[8804] = {
  kind = "Tip",
  text = "alliance_only_one_create_allowed"
}
AlertDataList.K[8805] = {
  kind = "Tip",
  text = "alliance_already_applied"
}
AlertDataList.K[8806] = {
  kind = "Tip",
  text = "alliance_applied_full"
}
AlertDataList.K[8807] = {
  kind = "Tip",
  text = "alliance_already_ratified"
}
AlertDataList.K[8808] = {
  kind = "Tip",
  text = "alliance_not_applied"
}
AlertDataList.K[8809] = {
  kind = "Tip",
  text = "alliance_not_yet_owned"
}
AlertDataList.K[8810] = {
  kind = "Tip",
  text = "alliance_name_not_empty"
}
AlertDataList.K[8811] = {
  kind = "Tip",
  text = "alliance_name_is_invalid"
}
AlertDataList.K[8812] = {
  kind = "Tip",
  text = "alliance_manager_count_limit"
}
AlertDataList.K[8813] = {
  kind = "Tip",
  text = "alliance_aready_received_award"
}
AlertDataList.K[8899] = {
  kind = "Tip",
  text = "alliance_self_not_kicked"
}
AlertDataList.K[10111] = {
  kind = "Tip",
  text = "get_donate_error"
}
AlertDataList.K[10115] = {
  kind = "Tip",
  text = "error_donate_type"
}
AlertDataList.K[7000] = {
  kind = "Tip",
  text = "tc_player_already_die"
}
AlertDataList.K[7001] = {
  kind = "Tip",
  text = "tc_jump_path_error"
}
AlertDataList.K[7002] = {
  kind = "Tip",
  text = "tc_path_is_empty"
}
AlertDataList.K[7003] = {
  kind = "Tip",
  text = "tc_dot_mismatch_line"
}
AlertDataList.K[7004] = {
  kind = "Tip",
  text = "tc_line_not_found"
}
AlertDataList.K[7005] = {
  kind = "Tip",
  text = "tc_player_not_found"
}
AlertDataList.K[7006] = {
  kind = "Tip",
  text = "tc_dot_not_found"
}
AlertDataList.K[7007] = {
  kind = "Tip",
  text = "tc_battle_not_found"
}
AlertDataList.K[7010] = {
  kind = "Tip",
  text = "tc_start_first_dot_mismatch_position"
}
AlertDataList.K[7011] = {
  kind = "Tip",
  text = "tc_start_on_road"
}
AlertDataList.K[7020] = {
  kind = "Tip",
  text = "tc_transfer_required_more_credit"
}
AlertDataList.K[7021] = {
  kind = "Tip",
  text = "tc_transfer_first_dot_mismatch_position"
}
AlertDataList.K[7022] = {
  kind = "Tip",
  text = "tc_transfer_station_occupied"
}
AlertDataList.K[7023] = {
  kind = "Tip",
  text = "tc_transfer_station_empty"
}
AlertDataList.K[7030] = {
  kind = "Tip",
  text = "tc_revive_required_more_credit"
}
AlertDataList.K[7031] = {
  kind = "Tip",
  text = "tc_revive_is_alive"
}
AlertDataList.K[8600] = {
  kind = "Tip",
  text = "equipment_not_found"
}
AlertDataList.K[8602] = {
  kind = "Tip",
  text = "equipment_more_than_engineering_bay_onlevel"
}
AlertDataList.K[8603] = {
  kind = "Tip",
  text = "equipment_arrive_at_maxlevel"
}
AlertDataList.K[8604] = {
  kind = "Tip",
  text = "equipment_require_more_money"
}
AlertDataList.K[8605] = {
  kind = "Tip",
  text = "equipment_no_evolution_info"
}
AlertDataList.K[8606] = {
  kind = "Tip",
  text = "equipment_require_more_cdtime"
}
AlertDataList.K[8701] = {
  kind = "Tip",
  text = "equipment_fleet_not_found"
}
AlertDataList.K[8702] = {
  kind = "Tip",
  text = "equipment_not_found_in_bag"
}
AlertDataList.K[8704] = {
  kind = "Tip",
  text = "equipment_less_than_minlevel"
}
AlertDataList.K[8705] = {
  kind = "Tip",
  text = "equipment_require_recipe_not_found"
}
AlertDataList.K[8706] = {
  kind = "Tip",
  text = "equipment_evolution_not_found"
}
AlertDataList.K[8707] = {
  kind = "Tip",
  text = "equipment_enhance_level_limit"
}
AlertDataList.K[117] = {
  kind = "Tip",
  text = "building_max_level"
}
AlertDataList.K[120] = {
  kind = "Tip",
  text = "building_not_exist"
}
AlertDataList.K[121] = {
  kind = "Tip",
  text = "building_data_not_exist"
}
AlertDataList.K[122] = {
  kind = "Tip",
  text = "building_up_max_level"
}
AlertDataList.K[123] = {
  kind = "Tip",
  text = "building_cdtime_not"
}
AlertDataList.K[124] = {
  kind = "Tip",
  text = "building_level_greater_than_player_5"
}
AlertDataList.K[125] = {
  kind = "Tip",
  text = "building_level_greater_than_base"
}
AlertDataList.K[126] = {
  kind = "Tip",
  text = "building_level_limit_user"
}
AlertDataList.K[180] = {
  kind = "Tip",
  text = "building_level_limit_user_5"
}
AlertDataList.K[9100] = {
  kind = "Tip",
  text = "player_is_mining"
}
AlertDataList.K[9101] = {
  kind = "Tip",
  text = "mine_cnt_limit"
}
AlertDataList.K[9102] = {
  kind = "Tip",
  text = "mine_idx_error"
}
AlertDataList.K[9103] = {
  kind = "Tip",
  text = "refresh_mine_type_error"
}
AlertDataList.K[9104] = {
  kind = "Tip",
  text = "refresh_mine_res_limit"
}
AlertDataList.K[9105] = {
  kind = "Tip",
  text = "mine_atk_target_error"
}
AlertDataList.K[9106] = {
  kind = "Tip",
  text = "mine_atk_error"
}
AlertDataList.K[9107] = {
  kind = "Tip",
  text = "mine_atk_mine_done"
}
AlertDataList.K[9108] = {
  kind = "Tip",
  text = "mine_atk_robbed_cnt_limit"
}
AlertDataList.K[9109] = {
  kind = "Tip",
  text = "mine_not_digg"
}
AlertDataList.K[9110] = {
  kind = "Tip",
  text = "mine_speedup_time_limit"
}
AlertDataList.K[9111] = {
  kind = "Tip",
  text = "mine_atk_cd_limit"
}
AlertDataList.K[9112] = {
  kind = "Tip",
  text = "mine_boost_cnt_limit"
}
AlertDataList.K[9113] = {
  kind = "Tip",
  text = "mine_atk_cnt_limit"
}
AlertDataList.K[9114] = {
  kind = "Tip",
  text = "mine_speedup_fail"
}
AlertDataList.K[9115] = {
  kind = "Tip",
  text = "mine_can_not_speedup"
}
AlertDataList.K[2001] = {
  kind = "Tip",
  text = "krypton_gacha_no_money"
}
AlertDataList.K[2002] = {
  kind = "Tip",
  text = "krypton_not_exist"
}
AlertDataList.K[2005] = {
  kind = "Tip",
  text = "krypton_not_enough_kenergy"
}
AlertDataList.K[2006] = {
  kind = "Tip",
  text = "krypton_equip_type_conflict"
}
AlertDataList.K[2007] = {
  kind = "Tip",
  text = "krypton_equip_slot_conflict"
}
AlertDataList.K[2008] = {
  kind = "Tip",
  text = "krypton_equip_already"
}
AlertDataList.K[2011] = {
  kind = "Tip",
  text = "krypton_tmp_full"
}
AlertDataList.K[2012] = {
  kind = "Tip",
  text = "krypton_store_full"
}
AlertDataList.K[2013] = {
  kind = "Tip",
  text = "krypton_postion_already_used"
}
AlertDataList.K[2014] = {
  kind = "Tip",
  text = "krypton_not_tmp"
}
AlertDataList.K[2015] = {
  kind = "Tip",
  text = "krypton_not_store"
}
AlertDataList.K[2016] = {
  kind = "Tip",
  text = "krypton_equip_not_decom"
}
AlertDataList.K[2017] = {
  kind = "Tip",
  text = "krypton_sort_not_valid"
}
AlertDataList.K[2018] = {
  kind = "Tip",
  text = "krypton_lab_level_not"
}
AlertDataList.K[2019] = {
  kind = "Tip",
  text = "krypton_not_equiped"
}
AlertDataList.K[2020] = {
  kind = "Tip",
  text = "krypton_max_level"
}
AlertDataList.K[2021] = {
  kind = "Tip",
  text = "krypton_can_not_decompose"
}
AlertDataList.K[151] = {
  kind = "Tip",
  text = "battle_level_limit"
}
AlertDataList.K[160] = {
  kind = "Tip",
  text = "battle_not_allow"
}
AlertDataList.K[161] = {
  kind = "Tip",
  text = "battle_not_exist"
}
AlertDataList.K[162] = {
  kind = "Tip",
  text = "battle_adventure_no_supply"
}
AlertDataList.K[163] = {
  kind = "Tip",
  text = "battle_not_allow_today"
}
AlertDataList.K[164] = {
  kind = "Tip",
  text = "chapter_not_exist"
}
AlertDataList.K[165] = {
  kind = "Tip",
  text = "battle_supply_not_enough"
}
AlertDataList.K[166] = {
  kind = "Tip",
  text = "ace_supply_not_enough"
}
AlertDataList.K[167] = {
  kind = "Tip",
  text = "battle_cant_because_item_null"
}
AlertDataList.K[168] = {
  kind = "Tip",
  text = "battle_cant_rush"
}
AlertDataList.K[169] = {
  kind = "Tip",
  text = "act_not_exist"
}
AlertDataList.K[700] = {
  kind = "Tip",
  text = "warpgate_charge_type_err"
}
AlertDataList.K[701] = {
  kind = "Tip",
  text = "warpgate_can_not_upgrade_err"
}
AlertDataList.K[702] = {
  kind = "Tip",
  text = "warpgate_upgrade_type_err"
}
AlertDataList.K[703] = {
  kind = "Tip",
  text = "warpgate_can_not_collect_err"
}
AlertDataList.K[704] = {
  kind = "Tip",
  text = "warpgate_charge_resource_limit"
}
AlertDataList.K[705] = {
  kind = "Tip",
  text = "warpgate_upgrade_resource_limit"
}
AlertDataList.K[706] = {
  kind = "Tip",
  text = "warpgate_exchange_resource_limit"
}
AlertDataList.K[708] = {
  kind = "Tip",
  text = "shop_bad_equip_id"
}
AlertDataList.K[709] = {
  kind = "Tip",
  text = "shop_purchase_money_limit"
}
AlertDataList.K[710] = {
  kind = "Tip",
  text = "shop_sell_bad_grid_info"
}
AlertDataList.K[711] = {
  kind = "Tip",
  text = "shop_user_bag_full"
}
AlertDataList.K[712] = {
  kind = "Tip",
  text = "shop_bad_purchase_back_idx"
}
AlertDataList.K[713] = {
  kind = "Tip",
  text = "shop_bag_grid_not_found"
}
AlertDataList.K[714] = {
  kind = "Tip",
  text = "shop_bag_grid_is_filled"
}
AlertDataList.K[715] = {
  kind = "Tip",
  text = "shop_sell_bad_item_cnt"
}
AlertDataList.K[10302] = {
  kind = "Tip",
  text = "stores_item_time_out"
}
AlertDataList.K[10303] = {
  kind = "Tip",
  text = "item_time_expired"
}
AlertDataList.K[10304] = {
  kind = "Tip",
  text = "crystal_not_enough"
}
AlertDataList.K[5001] = {
  kind = "Tip",
  text = "pay_verify_error"
}
AlertDataList.K[5002] = {
  kind = "Tip",
  text = "pay_verify_token_error"
}
AlertDataList.K[5003] = {
  kind = "Tip",
  text = "pay_verify_not_ready"
}
AlertDataList.K[5004] = {
  kind = "Tip",
  text = "pay_verify_already"
}
AlertDataList.K[5005] = {
  kind = "Tip",
  text = "pay_verify_not_debug"
}
AlertDataList.K[5006] = {
  kind = "Tip",
  text = "pay_verify_mycard_used"
}
AlertDataList.K[5007] = {
  kind = "Tip",
  text = "pay_verify_mycard_not_match"
}
AlertDataList.K[120001] = {
  kind = "Tip",
  text = "order_not_found"
}
AlertDataList.K[120201] = {
  kind = "Tip",
  text = "order_confirm_yet_confirmed"
}
AlertDataList.K[120202] = {
  kind = "Tip",
  text = "order_confirm_mycard_billing_error"
}
AlertDataList.K[120203] = {
  kind = "Tip",
  text = "order_confirm_mycard_card_not_found"
}
AlertDataList.K[120204] = {
  kind = "Tip",
  text = "order_confirm_mycard_point_error"
}
AlertDataList.K[9130] = {
  kind = "Tip",
  text = "ac_energy_charge_level_limit"
}
AlertDataList.K[9131] = {
  kind = "Tip",
  text = "ac_energy_charge_ac_supply_limit"
}
AlertDataList.K[9132] = {
  kind = "Tip",
  text = "ac_energy_charge_tech_limit"
}
AlertDataList.K[9133] = {
  kind = "Tip",
  text = "ac_battle_id_error"
}
AlertDataList.K[9134] = {
  kind = "Tip",
  text = "ac_battle_completed_today_error"
}
AlertDataList.K[9135] = {
  kind = "Tip",
  text = "ac_battle_level_limit"
}
AlertDataList.K[8911] = {
  kind = "Tip",
  text = "technique_not_found"
}
AlertDataList.K[8912] = {
  kind = "Tip",
  text = "technique_more_than_player_onlevel"
}
AlertDataList.K[8913] = {
  kind = "Tip",
  text = "technique_more_than_techlab_onlevel"
}
AlertDataList.K[8914] = {
  kind = "Tip",
  text = "technique_less_than_minlevel"
}
AlertDataList.K[8915] = {
  kind = "Tip",
  text = "technique_arrive_at_maxlevel"
}
AlertDataList.K[8916] = {
  kind = "Tip",
  text = "technique_require_more_techpoint"
}
AlertDataList.K[1000001] = {
  kind = "Tip",
  text = "ladder_reset_less"
}
AlertDataList.K[1000002] = {
  kind = "Tip",
  text = "ladder_reset_one"
}
AlertDataList.K[1000003] = {
  kind = "Tip",
  text = "ladder_raids_max"
}
AlertDataList.K[1000004] = {
  kind = "Tip",
  text = "ladder_search_cost"
}
AlertDataList.K[1000005] = {
  kind = "Tip",
  text = "ladder_reset_cost"
}
AlertDataList.K[1000006] = {
  kind = "Tip",
  text = "ladder_reset_max"
}
AlertDataList.K[1000007] = {
  kind = "Tip",
  text = "dungeon_ladder_close"
}
AlertDataList.K[1000008] = {
  kind = "Tip",
  text = "dungeon_ladder_level"
}
AlertDataList.K[1000009] = {
  kind = "Tip",
  text = "ladder_search_max"
}
AlertDataList.K[1000010] = {
  kind = "Tip",
  text = "ladder_search_error"
}
AlertDataList.K[1000011] = {
  kind = "Tip",
  text = "ladder_raids_end"
}
AlertDataList.K[1000012] = {
  kind = "Tip",
  text = "ladder_search_on_max"
}
AlertDataList.K[1000013] = {
  kind = "Tip",
  text = "mulmatrix_not_open"
}
AlertDataList.K[1000014] = {
  kind = "Tip",
  text = "mulmatrix_buy_credit"
}
AlertDataList.K[1000015] = {
  kind = "Tip",
  text = "mulmatrix_condition"
}
AlertDataList.K[1000016] = {
  kind = "Tip",
  text = "mulmatrix_buy_money"
}
AlertDataList.K[1000031] = {
  kind = "Tip",
  text = "ladder_jump_type_error"
}
AlertDataList.K[1000032] = {
  kind = "Tip",
  text = "ladder_report_error"
}
AlertDataList.K[1000033] = {
  kind = "Tip",
  text = "ladder_max_step_error"
}
AlertDataList.K[1000034] = {
  kind = "Tip",
  text = "ladder_award_unfinish"
}
AlertDataList.K[1000035] = {
  kind = "Tip",
  text = "ladder_award_already"
}
AlertDataList.K[1000036] = {
  kind = "Tip",
  text = "ladder_raids_level_less"
}
AlertDataList.K[1000037] = {
  kind = "Tip",
  text = "normal_ladder_raids_over"
}
AlertDataList.K[1000038] = {
  kind = "Tip",
  text = "expedition_wormhole_score_lack"
}
AlertDataList.K[1000039] = {
  kind = "Tip",
  text = "expedition_wormhole_award_got"
}
AlertDataList.K[1000040] = {
  kind = "Tip",
  text = "expedition_challenge_on_max"
}
AlertDataList.K[1000041] = {
  kind = "Tip",
  text = "expedition_challenge_end"
}
AlertDataList.K[1000042] = {
  kind = "Tip",
  text = "expedition_reset_max"
}
AlertDataList.K[1000043] = {
  kind = "Tip",
  text = "expedition_challenge_max"
}
AlertDataList.K[1000044] = {
  kind = "Tip",
  text = "expedition_challenge_cost"
}
AlertDataList.K[1000045] = {
  kind = "Tip",
  text = "expedition_reset_cost"
}
AlertDataList.K[1000046] = {
  kind = "Tip",
  text = "expedition_not_enough"
}
AlertDataList.K[901] = {
  kind = "Tip",
  text = "fleet_not_exist"
}
AlertDataList.K[911] = {
  kind = "Tip",
  text = "fleet_enhance_level_limit"
}
AlertDataList.K[912] = {
  kind = "Tip",
  text = "fleet_enhance_arrives_end"
}
AlertDataList.K[913] = {
  kind = "Tip",
  text = "fleet_enhance_required_more_items"
}
AlertDataList.K[914] = {
  kind = "Tip",
  text = "fleet_enhance_gacha"
}
AlertDataList.K[921] = {
  kind = "Tip",
  text = "fleet_weaken_minimality"
}
AlertDataList.K[922] = {
  kind = "Tip",
  text = "fleet_weaken_on_returning"
}
AlertDataList.K[923] = {
  kind = "Tip",
  text = "fleet_enhance_upgrade_required_more_items"
}
AlertDataList.K[924] = {
  kind = "Tip",
  text = "fleet_weaken_bag_full"
}
AlertDataList.K[925] = {
  kind = "Tip",
  text = "fleet_equip_level_limit"
}
AlertDataList.K[3001] = {
  kind = "Tip",
  text = "fleet_already_on_matrix"
}
AlertDataList.K[3002] = {
  kind = "Tip",
  text = "matrix_cell_already_been_taken"
}
AlertDataList.K[3003] = {
  kind = "Tip",
  text = "fleet_not_on_matrix"
}
AlertDataList.K[3004] = {
  kind = "Tip",
  text = "fleet_have_not_spell"
}
AlertDataList.K[3005] = {
  kind = "Tip",
  text = "spell_not_exist"
}
AlertDataList.K[3006] = {
  kind = "Tip",
  text = "fleet_already_have_spell"
}
AlertDataList.K[3007] = {
  kind = "Tip",
  text = "fleet_spell_over_max"
}
AlertDataList.K[3008] = {
  kind = "Tip",
  text = "fleet_count_over_max"
}
AlertDataList.K[1001] = {
  kind = "Tip",
  text = "challenge_user_info_error"
}
AlertDataList.K[1002] = {
  kind = "Tip",
  text = "challenge_rank_error"
}
AlertDataList.K[1003] = {
  kind = "Tip",
  text = "challenge_fail_cd_error"
}
AlertDataList.K[1004] = {
  kind = "Tip",
  text = "challenge_max_cnt_error"
}
AlertDataList.K[1005] = {
  kind = "Tip",
  text = "no_need_reset_fail_time"
}
AlertDataList.K[1006] = {
  kind = "Tip",
  text = "reset_fail_time_resource_limit"
}
AlertDataList.K[1007] = {
  kind = "Tip",
  text = "champion_award_fail"
}
AlertDataList.K[1009] = {
  kind = "Tip",
  text = "champion_player_on_fight"
}
AlertDataList.K[1010] = {
  kind = "Tip",
  text = "champion_get_player_info_error"
}
AlertDataList.K[1011] = {
  kind = "Tip",
  text = "champion_no_top_record"
}
AlertDataList.K[4001] = {
  kind = "Tip",
  text = "challenge_can_fight_again"
}
AlertDataList.K[10116] = {
  kind = "Tip",
  text = "error_get_champion_reward"
}
AlertDataList.K[52] = {
  kind = "Tip",
  text = "vip_no_enough"
}
AlertDataList.K[53] = {
  kind = "Tip",
  text = "vip_no_enough_to_buy_store"
}
AlertDataList.K[100] = {kind = "Tip", text = "vip_limit"}
AlertDataList.K[40] = {
  kind = "Tip",
  text = "vip_level_not_exist"
}
AlertDataList.K[150] = {
  kind = "Tip",
  text = "adv_refresh_time_full"
}
AlertDataList.K[820] = {
  kind = "Tip",
  text = "chat_user_banned"
}
AlertDataList.K[821] = {
  kind = "Tip",
  text = "chat_bad_channel"
}
AlertDataList.K[822] = {
  kind = "Tip",
  text = "chat_receiver_bad_name"
}
AlertDataList.K[823] = {
  kind = "Tip",
  text = "chat_too_fast"
}
AlertDataList.K[824] = {
  kind = "Tip",
  text = "chat_not_tc"
}
AlertDataList.K[800] = {
  kind = "Tip",
  text = "mail_content_too_short"
}
AlertDataList.K[801] = {
  kind = "Tip",
  text = "mail_receiver_list_null"
}
AlertDataList.K[802] = {
  kind = "Tip",
  text = "mail_bag_to_mail"
}
AlertDataList.K[803] = {
  kind = "Tip",
  text = "mail_goods_have_get"
}
AlertDataList.K[806] = {
  kind = "Tip",
  text = "player_level_not_enough_mail"
}
AlertDataList.K[900001] = {
  kind = "Tip",
  text = "open_box_error"
}
AlertDataList.K[900002] = {
  kind = "Tip",
  text = "open_box_have_get"
}
AlertDataList.K[900003] = {
  kind = "Tip",
  text = "open_box_credit_error"
}
AlertDataList.K[12000] = {
  kind = "Tip",
  text = "redeem_code_activity_aready_in"
}
AlertDataList.K[12001] = {
  kind = "Tip",
  text = "redeem_code_param_empty"
}
AlertDataList.K[12002] = {
  kind = "Tip",
  text = "redeem_code_not_found"
}
AlertDataList.K[12003] = {
  kind = "Tip",
  text = "redeem_code_aready_used"
}
AlertDataList.K[100120] = {
  kind = "Tip",
  text = "not_in_world_boss_time"
}
AlertDataList.K[100121] = {
  kind = "Tip",
  text = "player_level_not_enough_world_boss"
}
AlertDataList.K[100122] = {
  kind = "Tip",
  text = "world_boss_dead"
}
AlertDataList.K[100123] = {
  kind = "Tip",
  text = "world_boss_max_force_rate"
}
AlertDataList.K[100124] = {
  kind = "Tip",
  text = "credit_not_enough_world_boss"
}
AlertDataList.K[100125] = {
  kind = "Tip",
  text = "tech_not_enough_world_boss"
}
AlertDataList.K[100126] = {
  kind = "Tip",
  text = "world_boss_in_ready"
}
AlertDataList.K[100127] = {
  kind = "Tip",
  text = "boss_robot_duplicately_added"
}
AlertDataList.K[100128] = {
  kind = "Tip",
  text = "boss_robot_attackment_is_on_work"
}
AlertDataList.K[100129] = {
  kind = "Tip",
  text = "boss_robot_terminated"
}
AlertDataList.K[100130] = {
  kind = "Tip",
  text = "in_cd_world_boss"
}
AlertDataList.K[130] = {
  kind = "Tip",
  text = "money_not_enough"
}
AlertDataList.K[131] = {
  kind = "Tip",
  text = "technique_not_enough"
}
AlertDataList.K[135] = {
  kind = "Tip",
  text = "credit_not_enough"
}
AlertDataList.K[140] = {
  kind = "Tip",
  text = "cdtime_clear_no_credit"
}
AlertDataList.K[301] = {
  kind = "Tip",
  text = "revenue_exchange_not_enough_credit"
}
AlertDataList.K[10113] = {
  kind = "Tip",
  text = "credit_not_enough_donate"
}
AlertDataList.K[10114] = {
  kind = "Tip",
  text = "money_not_enough_donate"
}
AlertDataList.K[136] = {
  kind = "Tip",
  text = "laba_supply_not_enough"
}
AlertDataList.K[1200] = {
  kind = "Tip",
  text = "supply_exchange_no_credit"
}
AlertDataList.K[8904] = {
  kind = "Tip",
  text = "officer_require_more_credit"
}
AlertDataList.K[300] = {
  kind = "Tip",
  text = "revenue_time_not_enough"
}
AlertDataList.K[716] = {
  kind = "Tip",
  text = "warpgate_upgrade_cnt_limit"
}
AlertDataList.K[1202] = {
  kind = "Tip",
  text = "revenue_exchange_time_full"
}
AlertDataList.K[1201] = {
  kind = "Tip",
  text = "supply_exchange_time_full"
}
AlertDataList.K[1400] = {
  kind = "Tip",
  text = "commander_change_time_full"
}
AlertDataList.K[8903] = {
  kind = "Tip",
  text = "officer_more_than_maxtimes"
}
AlertDataList.K[10112] = {
  kind = "Tip",
  text = "vip_level_lower_donate"
}
AlertDataList.K[10300] = {
  kind = "Tip",
  text = "beyond_day_buy_max"
}
AlertDataList.K[10301] = {
  kind = "Tip",
  text = "beyond_all_buy_max"
}
AlertDataList.K[10305] = {
  kind = "Tip",
  text = "beyond_once_buy_max"
}
AlertDataList.K[10306] = {
  kind = "Tip",
  text = "can_not_buy"
}
AlertDataList.K[51] = {
  kind = "Tip",
  text = "level_no_enough"
}
AlertDataList.K[143] = {
  kind = "Tip",
  text = "not_on_level"
}
AlertDataList.K[172] = {
  kind = "Tip",
  text = "rush_deny_case_item_full"
}
AlertDataList.K[1008] = {
  kind = "Tip",
  text = "bad_fight_record_id"
}
AlertDataList.K[1101] = {
  kind = "Tip",
  text = "bad_prestige_level"
}
AlertDataList.K[1102] = {
  kind = "Tip",
  text = "prestige_not_achieve"
}
AlertDataList.K[1103] = {
  kind = "Tip",
  text = "prestige_already_obtained"
}
AlertDataList.K[400] = {
  kind = "Tip",
  text = "add_friends_error"
}
AlertDataList.K[401] = {
  kind = "Tip",
  text = "del_friends_error"
}
AlertDataList.K[1203] = {
  kind = "Tip",
  text = "supply_type_error"
}
AlertDataList.K[1301] = {
  kind = "Tip",
  text = "quest_id_unvalid"
}
AlertDataList.K[1303] = {
  kind = "Tip",
  text = "quest_un_finish"
}
AlertDataList.K[1500] = {
  kind = "Tip",
  text = "rush_module_not_enabled"
}
AlertDataList.K[6001] = {
  kind = "Tip",
  text = "task_not_finish"
}
AlertDataList.K[8901] = {
  kind = "Tip",
  text = "officer_not_found"
}
AlertDataList.K[8920] = {
  kind = "Tip",
  text = "tipoff_self"
}
AlertDataList.K[9000] = {
  kind = "Tip",
  text = "special_battle_id_error"
}
AlertDataList.K[11003] = {
  kind = "Tip",
  text = "player_munte_already"
}
AlertDataList.K[8902] = {
  kind = "Tip",
  text = "officer_less_than_minlevel"
}
AlertDataList.K[9120] = {
  kind = "Tip",
  text = "laba_need_get_award"
}
AlertDataList.K[9121] = {kind = "Tip", text = "laba_empty"}
AlertDataList.K[101002] = {
  kind = "Tip",
  text = "got_reward_error"
}
AlertDataList.K[101003] = {
  kind = "Tip",
  text = "not_enough_stars_error"
}
AlertDataList.K[101004] = {
  kind = "Tip",
  text = "send_allstars_reward_error"
}
AlertDataList.K[101005] = {
  kind = "Tip",
  text = "fleet_sale_with_rebate_timeout"
}
AlertDataList.K[110910] = {
  kind = "Tip",
  text = "prestige_award_cannot_got"
}
AlertDataList.K[110911] = {
  kind = "Tip",
  text = "prestige_award_has_got"
}
AlertDataList.K[110912] = {
  kind = "Tip",
  text = "fleet_recruit_unlock"
}
AlertDataList.K[110913] = {
  kind = "Tip",
  text = "coating_buff_added"
}
AlertDataList.K[110914] = {
  kind = "Tip",
  text = "same_buff_cannot_use"
}
AlertDataList.K[800001] = {
  kind = "Tip",
  text = "item_20013_use_confirm_alert"
}
AlertDataList.K[127] = {kind = "Tip", text = "item_full"}
AlertDataList.K[128] = {
  kind = "Tip",
  text = "item_add_failed"
}
AlertDataList.K[129] = {
  kind = "Tip",
  text = "item_owner_error"
}
AlertDataList.K[132] = {
  kind = "Tip",
  text = "equip_can_not_use"
}
AlertDataList.K[133] = {
  kind = "Tip",
  text = "item_can_not_use"
}
AlertDataList.K[134] = {
  kind = "Tip",
  text = "item_use_error"
}
AlertDataList.K[137] = {
  kind = "Tip",
  text = "item_can_not_use_level_limit"
}
AlertDataList.K[138] = {
  kind = "Tip",
  text = "equip_can_not_use_level_limit"
}
AlertDataList.K[139] = {
  kind = "Tip",
  text = "item_cnt_not_enough"
}
AlertDataList.K[141] = {
  kind = "Tip",
  text = "ph_atk_equip_can_not_use"
}
AlertDataList.K[142] = {
  kind = "Tip",
  text = "en_atk_equip_can_not_use"
}
AlertDataList.K[173] = {
  kind = "Tip",
  text = "item_use_max_error"
}
AlertDataList.K[101009] = {
  kind = "Tip",
  text = "event_krypton_three_alert"
}
AlertDataList.K[101010] = {
  kind = "Tip",
  text = "event_krypton_four_alert"
}
AlertDataList.K[101011] = {
  kind = "Tip",
  text = "event_krypton_five_alert"
}
AlertDataList.K[101012] = {
  kind = "Tip",
  text = "event_krypton_chip_alert"
}
AlertDataList.K[101013] = {
  kind = "Tip",
  text = "error_bag_full_cannot_get_krypton_chip"
}
AlertDataList.K[101014] = {
  kind = "Tip",
  text = "event_krypton_core_three_alert"
}
AlertDataList.K[101015] = {
  kind = "Tip",
  text = "event_krypton_core_four_alert"
}
AlertDataList.K[170] = {
  kind = "Tip",
  text = "item_move_not_allowed"
}
AlertDataList.K[171] = {
  kind = "Tip",
  text = "item_not_exist"
}
AlertDataList.K[717] = {
  kind = "Tip",
  text = "shop_item_can_not_sell"
}
AlertDataList.K[10119] = {
  kind = "Tip",
  text = "error_bug_full_rewards_cannot_get"
}
AlertDataList.K[4201] = {
  kind = "Tip",
  text = "suipian_not_enough"
}
AlertDataList.K[4202] = {
  kind = "Tip",
  text = "suipian_compose_already"
}
AlertDataList.K[110904] = {
  kind = "Tip",
  text = "christmasday_has_gone"
}
AlertDataList.K[110905] = {
  kind = "Tip",
  text = "pay_gift_item_full"
}
AlertDataList.K[201] = {
  kind = "Tip",
  text = "sign_up_can_not_get"
}
AlertDataList.K[202] = {
  kind = "Tip",
  text = "sign_up_already_get"
}
AlertDataList.K[203] = {
  kind = "Tip",
  text = "activity_not_active"
}
AlertDataList.K[204] = {
  kind = "Tip",
  text = "activity_not_begin"
}
AlertDataList.K[205] = {
  kind = "Tip",
  text = "activity_already_end"
}
AlertDataList.K[206] = {
  kind = "Tip",
  text = "already_in_block_devil"
}
AlertDataList.K[101006] = {
  kind = "Tip",
  text = "not_in_activity_time"
}
AlertDataList.K[101007] = {
  kind = "Tip",
  text = "wrong_cutoff_id"
}
AlertDataList.K[900004] = {
  kind = "Tip",
  text = "activity_task_reward_unfinished"
}
AlertDataList.K[900005] = {
  kind = "Tip",
  text = "activity_task_reward_rewarded"
}
AlertDataList.K[9200] = {
  kind = "Tip",
  text = "activity_store_fleet_failed"
}
AlertDataList.K[9201] = {
  kind = "Tip",
  text = "activity_store_item_failed"
}
AlertDataList.K[10400] = {
  kind = "Tip",
  text = "store_day_exchanged_times_limit"
}
AlertDataList.K[10401] = {
  kind = "Tip",
  text = "store_all_exchanged_times_limit"
}
AlertDataList.K[101008] = {
  kind = "Tip",
  text = "already_joined_activity"
}
AlertDataList.K[4101] = {
  kind = "Tip",
  text = "loot_already_get"
}
AlertDataList.K[10117] = {
  kind = "Tip",
  text = "error_active_rewards_type_req"
}
AlertDataList.K[10118] = {
  kind = "Tip",
  text = "error_active_rewards_lost"
}
AlertDataList.K[10200] = {
  kind = "Tip",
  text = "can_not_award_in_promotion"
}
AlertDataList.K[207] = {
  kind = "Tip",
  text = "enter_next_activity_error"
}
AlertDataList.K[208] = {
  kind = "Tip",
  text = "read_activity_config_error"
}
AlertDataList.K[209] = {
  kind = "Tip",
  text = "charge_activity_dna_error"
}
AlertDataList.K[10402] = {
  kind = "Tip",
  text = "already_had_fleet"
}
AlertDataList.K[10403] = {
  kind = "Tip",
  text = "refresh_times_limited"
}
AlertDataList.K[4004] = {
  kind = "Tip",
  text = "matrix_cell_not_active"
}
AlertDataList.K[4100] = {
  kind = "Tip",
  text = "pve_supply_loot_not_valid"
}
AlertDataList.K[54] = {
  kind = "Tip",
  text = "krypton_six_need_level_50"
}
AlertDataList.K[804] = {
  kind = "Tip",
  text = "mail_send_not_complete"
}
AlertDataList.K[805] = {
  kind = "Tip",
  text = "mail_send_failed"
}
AlertDataList.K[5008] = {
  kind = "Tip",
  text = "pay_duplicated_verify"
}
AlertDataList.K[5009] = {
  kind = "Tip",
  text = "pay_apple_verify_error"
}
AlertDataList.K[5010] = {
  kind = "Tip",
  text = "pay_360_price_error"
}
AlertDataList.K[7032] = {
  kind = "Tip",
  text = "tc_mission_condition_error"
}
AlertDataList.K[7033] = {
  kind = "Tip",
  text = "tc_not_in_activity"
}
AlertDataList.K[1000017] = {
  kind = "Tip",
  text = "wormhole_reset_less"
}
AlertDataList.K[1000018] = {
  kind = "Tip",
  text = "wormhole_reset_one"
}
AlertDataList.K[1000019] = {
  kind = "Tip",
  text = "wormhole_raids_max"
}
AlertDataList.K[1000020] = {
  kind = "Tip",
  text = "wormhole_search_cost"
}
AlertDataList.K[1000021] = {
  kind = "Tip",
  text = "wormhole_reset_cost"
}
AlertDataList.K[1000022] = {
  kind = "Tip",
  text = "wormhole_reset_max"
}
AlertDataList.K[1000023] = {
  kind = "Tip",
  text = "dungeon_wormhole_level"
}
AlertDataList.K[1000024] = {
  kind = "Tip",
  text = "wormhole_search_max"
}
AlertDataList.K[1000025] = {
  kind = "Tip",
  text = "wormhole_search_error"
}
AlertDataList.K[1000026] = {
  kind = "Tip",
  text = "wormhole_raids_end"
}
AlertDataList.K[1000027] = {
  kind = "Tip",
  text = "wormhole_search_on_max"
}
AlertDataList.K[1000028] = {
  kind = "Tip",
  text = "dungeon_expedition_close"
}
AlertDataList.K[1000029] = {
  kind = "Tip",
  text = "expedition_level_limited"
}
AlertDataList.K[1001001] = {
  kind = "Tip",
  text = "interference_equipe_not_exist"
}
AlertDataList.K[1001002] = {
  kind = "Tip",
  text = "interference_max_level"
}
AlertDataList.K[1001003] = {
  kind = "Tip",
  text = "interference_config_not_exist"
}
AlertDataList.K[1001004] = {
  kind = "Tip",
  text = "interference_item_not_enough"
}
AlertDataList.K[1001005] = {
  kind = "Tip",
  text = "interference_addition_item_not_enough"
}
AlertDataList.K[1001006] = {
  kind = "Tip",
  text = "interference_addition_credit_not_enough"
}
AlertDataList.K[1000100] = {
  kind = "Tip",
  text = "tbc_level_less"
}
AlertDataList.K[1000101] = {
  kind = "Tip",
  text = "tbc_cross_cost"
}
AlertDataList.K[1000102] = {
  kind = "Tip",
  text = "tbc_no_self_fleet"
}
AlertDataList.K[1000103] = {
  kind = "Tip",
  text = "tbc_repair_max"
}
AlertDataList.K[1000104] = {
  kind = "Tip",
  text = "tbc_reward_pos"
}
AlertDataList.K[1000105] = {
  kind = "Tip",
  text = "tbc_reward_pass"
}
AlertDataList.K[1000106] = {
  kind = "Tip",
  text = "tbc_fight_pos"
}
AlertDataList.K[1000107] = {
  kind = "Tip",
  text = "tbc_special_error"
}
AlertDataList.K[1000108] = {
  kind = "Tip",
  text = "tbc_cross_less"
}
AlertDataList.K[1000109] = {
  kind = "Tip",
  text = "tbc_get_enemy_fail"
}
AlertDataList.K[1000110] = {
  kind = "Tip",
  text = "tbc_fight_no_fleet"
}
AlertDataList.K[1000111] = {
  kind = "Tip",
  text = "tbc_fight_fleet_not_enough"
}
AlertDataList.K[40001] = {
  kind = "Tip",
  text = "budo_join_limit"
}
AlertDataList.K[40002] = {
  kind = "Tip",
  text = "budo_did_not_open"
}
AlertDataList.K[40003] = {
  kind = "Tip",
  text = "can_not_join"
}
AlertDataList.K[40004] = {
  kind = "Tip",
  text = "sign_up_error"
}
AlertDataList.K[40005] = {
  kind = "Tip",
  text = "get_reward_error"
}
AlertDataList.K[40006] = {
  kind = "Tip",
  text = "get_rank_error"
}
AlertDataList.K[40007] = {
  kind = "Tip",
  text = "get_replay_error"
}
AlertDataList.K[40008] = {
  kind = "Tip",
  text = "change_formotion_error"
}
AlertDataList.K[40009] = {
  kind = "Tip",
  text = "already_support"
}
AlertDataList.K[400010] = {
  kind = "Tip",
  text = "wrong_round"
}
AlertDataList.K[400011] = {
  kind = "Tip",
  text = "change_formotion_error_2"
}
AlertDataList.K[400012] = {
  kind = "Tip",
  text = "change_formotion_error_3"
}
AlertDataList.K[41001] = {
  kind = "Tip",
  text = "over_max_adjutant"
}
AlertDataList.K[41002] = {
  kind = "Tip",
  text = "already_major"
}
AlertDataList.K[41003] = {
  kind = "Tip",
  text = "already_adjutant"
}
AlertDataList.K[41004] = {
  kind = "Tip",
  text = "vessels_not_map"
}
AlertDataList.K[41005] = {
  kind = "Tip",
  text = "major_on_matrix"
}
AlertDataList.K[41006] = {
  kind = "Tip",
  text = "adjutant_on_matrix"
}
AlertDataList.K[41007] = {
  kind = "Tip",
  text = "fleet_already_dismiss"
}
AlertDataList.K[42001] = {
  kind = "Tip",
  text = "no_mech_levelup"
}
AlertDataList.K[42002] = {
  kind = "Tip",
  text = "mech_levelup_credit_error"
}
AlertDataList.K[42003] = {
  kind = "Tip",
  text = "mech_is_levelup"
}
AlertDataList.K[42004] = {
  kind = "Tip",
  text = "mech_already_max_level"
}
AlertDataList.K[42005] = {
  kind = "Tip",
  text = "remodel_money_not_enough"
}
AlertDataList.K[42006] = {
  kind = "Tip",
  text = "remodel_server_error"
}
AlertDataList.K[42007] = {
  kind = "Tip",
  text = "need_join_alliance"
}
AlertDataList.K[42008] = {
  kind = "Tip",
  text = "mech_type_error"
}
AlertDataList.K[42009] = {
  kind = "Tip",
  text = "help_list_is_null"
}
AlertDataList.K[43001] = {
  kind = "Tip",
  text = "no_enough_item"
}
AlertDataList.K[43002] = {
  kind = "Tip",
  text = "already_max_level"
}
AlertDataList.K[43003] = {
  kind = "Tip",
  text = "no_facebook_bound"
}
AlertDataList.K[43004] = {
  kind = "Tip",
  text = "financial_use_out"
}
AlertDataList.K[44001] = {
  kind = "Tip",
  text = "krypton_inject_condition_lack"
}
AlertDataList.K[44002] = {
  kind = "Tip",
  text = "krypton_refine_required_error"
}
AlertDataList.K[44003] = {
  kind = "Tip",
  text = "krypton_inject_times_lack"
}
AlertDataList.K[44004] = {
  kind = "Tip",
  text = "krypton_kenergy_lack"
}
AlertDataList.K[44005] = {
  kind = "Tip",
  text = "krypton_credit_lack"
}
AlertDataList.K[44006] = {
  kind = "Tip",
  text = "krypton_vip_lack"
}
AlertDataList.K[44008] = {
  kind = "Tip",
  text = "krypton_refine_max_rank"
}
AlertDataList.K[44009] = {
  kind = "Tip",
  text = "krypton_refine_exp_lack"
}
AlertDataList.K[44010] = {
  kind = "Tip",
  text = "krypton_refine_can_not"
}
AlertDataList.K[44011] = {
  kind = "Tip",
  text = "krypton_refine_equiped"
}
AlertDataList.K[44012] = {
  kind = "Tip",
  text = "krypton_can_not_find"
}
AlertDataList.K[44013] = {
  kind = "Tip",
  text = "krypton_inject_item_lack"
}
AlertDataList.K[44014] = {
  kind = "Tip",
  text = "times_can_not_buy"
}
AlertDataList.K[45001] = {
  kind = "Tip",
  text = "wve_duplicate_powerup"
}
AlertDataList.K[45002] = {
  kind = "Tip",
  text = "wve_dot_conflict"
}
AlertDataList.K[45003] = {
  kind = "Tip",
  text = "wve_jump_not_in_dot"
}
AlertDataList.K[45004] = {
  kind = "Tip",
  text = "wve_player_not_in_dot"
}
AlertDataList.K[45005] = {
  kind = "Tip",
  text = "wve_player_already_prepare_march"
}
AlertDataList.K[45006] = {
  kind = "Tip",
  text = "wve_not_open"
}
AlertDataList.K[46001] = {
  kind = "Tip",
  text = "straight_award_can_not"
}
AlertDataList.K[46002] = {
  kind = "Tip",
  text = "straight_award_server_error"
}
AlertDataList.K[47001] = {
  kind = "Tip",
  text = "all_welfare_over"
}
AlertDataList.K[47002] = {
  kind = "Tip",
  text = "all_welfare_no_level"
}
AlertDataList.K[47003] = {
  kind = "Tip",
  text = "all_welfare_no_enough_person"
}
AlertDataList.K[47004] = {
  kind = "Tip",
  text = "all_wefare_already_loot"
}
AlertDataList.K[47005] = {
  kind = "Tip",
  text = "open_server_fund_level_not_enough"
}
AlertDataList.K[47006] = {
  kind = "Tip",
  text = "open_server_fund_no_level"
}
AlertDataList.K[47007] = {
  kind = "Tip",
  text = "open_server_fund_vip_error"
}
AlertDataList.K[47008] = {
  kind = "Tip",
  text = "open_server_fund_credit_error"
}
AlertDataList.K[47009] = {
  kind = "Tip",
  text = "already_charged"
}
AlertDataList.K[47010] = {
  kind = "Tip",
  text = "open_server_fund_already_loot"
}
AlertDataList.K[47011] = {
  kind = "Tip",
  text = "open_server_fund_not_charged"
}
AlertDataList.K[47012] = {
  kind = "Tip",
  text = "use_item_1011_succ"
}
AlertDataList.K[48001] = {
  kind = "Tip",
  text = "daily_benefits_over"
}
AlertDataList.K[48002] = {
  kind = "Tip",
  text = "no_benefits_award"
}
AlertDataList.K[48003] = {
  kind = "Tip",
  text = "benefits_awards_server_error"
}
AlertDataList.K[48004] = {
  kind = "Tip",
  text = "purchase_credit_not_enough"
}
AlertDataList.K[48005] = {
  kind = "Tip",
  text = "benefits_award_already"
}
AlertDataList.K[48006] = {
  kind = "Tip",
  text = "benefits_open_lock"
}
AlertDataList.K[49001] = {
  kind = "Tip",
  text = "cutoff_item_over"
}
AlertDataList.K[49002] = {
  kind = "Tip",
  text = "cutoff_item_locked"
}
AlertDataList.K[49003] = {
  kind = "Tip",
  text = "cutoff_server_error"
}
AlertDataList.K[49004] = {
  kind = "Tip",
  text = "cutoff_item_already"
}
AlertDataList.K[49005] = {
  kind = "Tip",
  text = "cutoff_item_over_max"
}
AlertDataList.K[49006] = {
  kind = "Tip",
  text = "cutoff_resource_not_enough"
}
AlertDataList.K[49100] = {
  kind = "Tip",
  text = "tactical_equip_max"
}
AlertDataList.K[49101] = {
  kind = "Tip",
  text = "tactical_vessel_invalid"
}
AlertDataList.K[49102] = {
  kind = "Tip",
  text = "tactical_equip_invalid"
}
AlertDataList.K[49103] = {
  kind = "Tip",
  text = "tactical_lock_max"
}
AlertDataList.K[49104] = {
  kind = "Tip",
  text = "tactical_attribute_invalid"
}
AlertDataList.K[49105] = {
  kind = "Tip",
  text = "tactical_unreach_condition"
}
AlertDataList.K[49106] = {
  kind = "Tip",
  text = "tactical_on_slot"
}
AlertDataList.K[49201] = {
  kind = "Tip",
  text = "plante_explore_cd_limit"
}
AlertDataList.K[49202] = {
  kind = "Tip",
  text = "plante_explore_activity_id_error"
}
AlertDataList.K[49203] = {
  kind = "Tip",
  text = "star_id_error"
}
AlertDataList.K[49204] = {
  kind = "Tip",
  text = "wdc_no_match"
}
AlertDataList.K[49205] = {
  kind = "Tip",
  text = "wdc_enemy_protected"
}
AlertDataList.K[49206] = {
  kind = "Tip",
  text = "ac_level_max"
}
AlertDataList.K[49207] = {
  kind = "Tip",
  text = "krypton_seven_need_level_70"
}
AlertDataList.K[49208] = {
  kind = "Tip",
  text = "equipment_more_than_player_level"
}
AlertDataList.K[49209] = {
  kind = "Tip",
  text = "sw_planet_state_changed"
}
AlertDataList.K[49210] = {
  kind = "Tip",
  text = "sw_no_remaining_plunder_times"
}
AlertDataList.K[49211] = {
  kind = "Tip",
  text = "sw_has_owned_aplanet"
}
AlertDataList.K[49212] = {
  kind = "Tip",
  text = "limit_by_pve"
}
AlertDataList.K[49213] = {
  kind = "Tip",
  text = "iap_monthly_card_invaild"
}
AlertDataList.K[49214] = {kind = "Tip", text = "team_full"}
AlertDataList.K[49215] = {
  kind = "Tip",
  text = "already_in_team"
}
AlertDataList.K[49216] = {
  kind = "Tip",
  text = "not_in_team"
}
AlertDataList.K[49217] = {
  kind = "Tip",
  text = "team_not_exist"
}
AlertDataList.K[49218] = {
  kind = "Tip",
  text = "power_not_match"
}
AlertDataList.K[49219] = {
  kind = "Tip",
  text = "canot_create_team"
}
AlertDataList.K[49220] = {
  kind = "Tip",
  text = "fair_award_failed"
}
AlertDataList.K[6193] = {
  kind = "Tip",
  text = "can_not_update_formation"
}
AlertDataList.K[49221] = {
  kind = "Tip",
  text = "artifact_upgrade_max"
}
AlertDataList.K[49222] = {
  kind = "Tip",
  text = "artifact_reset_error"
}
AlertDataList.K[49223] = {
  kind = "Tip",
  text = "artifact_upgrade_no_art_point"
}
AlertDataList.K[49224] = {
  kind = "Tip",
  text = "equip_change_not_enough_space"
}
AlertDataList.K[49225] = {
  kind = "Tip",
  text = "addition_item_level_limit"
}
AlertDataList.K[50001] = {
  kind = "Tip",
  text = "medal_not_exist"
}
AlertDataList.K[50002] = {
  kind = "Tip",
  text = "medal_level_up_failed"
}
AlertDataList.K[50003] = {
  kind = "Tip",
  text = "medal_rank_up_level_limit"
}
AlertDataList.K[50004] = {
  kind = "Tip",
  text = "medal_rank_up_material_limit"
}
AlertDataList.K[50005] = {
  kind = "Tip",
  text = "medal_rank_up_target_error"
}
AlertDataList.K[50006] = {
  kind = "Tip",
  text = "medal_rank_up_failed"
}
AlertDataList.K[50007] = {
  kind = "Tip",
  text = "medal_decompose_failed"
}
AlertDataList.K[50008] = {
  kind = "Tip",
  text = "medal_reset_failed"
}
AlertDataList.K[50009] = {
  kind = "Tip",
  text = "medal_recycle_close"
}
AlertDataList.K[50010] = {
  kind = "Tip",
  text = "fleet_medal_equip_on_failed"
}
AlertDataList.K[50011] = {
  kind = "Tip",
  text = "fleet_medal_equip_off_failed"
}
AlertDataList.K[50012] = {
  kind = "Tip",
  text = "fleet_medal_replace_failed"
}
AlertDataList.K[50013] = {
  kind = "Tip",
  text = "fleet_medal_target_pos_already_equip_on"
}
AlertDataList.K[50014] = {
  kind = "Tip",
  text = "fleet_medal_target_pos_type_error"
}
AlertDataList.K[50015] = {
  kind = "Tip",
  text = "item_already_buy"
}
AlertDataList.K[50016] = {
  kind = "Tip",
  text = "item_can_not_buy"
}
AlertDataList.K[50017] = {
  kind = "Tip",
  text = "resource_not_enough"
}
AlertDataList.K[50018] = {
  kind = "Tip",
  text = "activity_not_start"
}
AlertDataList.K[50019] = {
  kind = "Tip",
  text = "refresh_all_limit"
}
AlertDataList.K[50020] = {
  kind = "Tip",
  text = "medal_recruit_all_limit"
}
AlertDataList.K[50021] = {
  kind = "Tip",
  text = "item_not_enough"
}
AlertDataList.K[50022] = {
  kind = "Tip",
  text = "medal_glory_already_activate"
}
AlertDataList.K[50023] = {
  kind = "Tip",
  text = "medal_glory_not_activate"
}
AlertDataList.K[50024] = {
  kind = "Tip",
  text = "medal_glory_cannot_activate"
}
AlertDataList.K[50025] = {
  kind = "Tip",
  text = "medal_glory_cannot_rank_up"
}
AlertDataList.K[50026] = {
  kind = "Tip",
  text = "medal_collection_not_open"
}
AlertDataList.K[50027] = {
  kind = "Tip",
  text = "medal_achievement_no_activate"
}
AlertDataList.K[50028] = {
  kind = "Tip",
  text = "challenge_time_limit"
}
AlertDataList.K[50029] = {
  kind = "Tip",
  text = "is_not_in_activity"
}
AlertDataList.K[50100] = {
  kind = "Tip",
  text = "in_marching"
}
AlertDataList.K[50111] = {
  kind = "Tip",
  text = "packet_overflow"
}
AlertDataList.K[50112] = {
  kind = "Tip",
  text = "map_duplicate_legion_name"
}
AlertDataList.K[50113] = {
  kind = "Tip",
  text = "map_quit_legion"
}
AlertDataList.K[50114] = {
  kind = "Tip",
  text = "map_no_fuel"
}
AlertDataList.K[50115] = {
  kind = "Tip",
  text = "map_full_player"
}
AlertDataList.K[50116] = {
  kind = "Tip",
  text = "map_join_legion_fail"
}
AlertDataList.K[50117] = {
  kind = "Tip",
  text = "map_team_change"
}
AlertDataList.K[50118] = {
  kind = "Tip",
  text = "map_team_full"
}
AlertDataList.K[50119] = {
  kind = "Tip",
  text = "map_repeat_team"
}
AlertDataList.K[50120] = {
  kind = "Tip",
  text = "map_assemble_fail"
}
AlertDataList.K[50121] = {
  kind = "Tip",
  text = "map_assemble_not_enough"
}
AlertDataList.K[50122] = {
  kind = "Tip",
  text = "map_occupy_quit_legion"
}
AlertDataList.K[50123] = {
  kind = "Tip",
  text = "map_power_limit"
}
AlertDataList.K[50124] = {
  kind = "Tip",
  text = "map_in_marching_occupy_quit"
}
AlertDataList.K[50125] = {
  kind = "Tip",
  text = "map_accept_join_legion_fail"
}
AlertDataList.K[50200] = {
  kind = "Tip",
  text = "tlc_no_match"
}
AlertDataList.K[50201] = {
  kind = "Tip",
  text = "tlc_enemy_protected"
}
AlertDataList.K[50202] = {
  kind = "Tip",
  text = "gcs_no_available_change_times"
}
AlertDataList.K[50203] = {
  kind = "Tip",
  text = "gcs_get_award_status_wrong"
}
AlertDataList.K[50204] = {
  kind = "Tip",
  text = "gcs_no_available_lv_awards"
}
AlertDataList.K[50205] = {
  kind = "Tip",
  text = "gcs_cannot_find_new_task"
}
AlertDataList.K[50206] = {
  kind = "Tip",
  text = "gcs_task_not_in_list"
}
AlertDataList.K[50207] = {kind = "Tip", text = "gcs_close"}
function AlertDataList:GetTextId(key)
  return self.DataList[key]
end
function AlertDataList:GetTextIdFromCode(code)
  if not self.K[code] then
    if AlertDataListExtra ~= nil and AlertDataListExtra[code] ~= nil then
      return AlertDataListExtra[code]
    else
      print("AlertDataList:Unknow code ", code)
      return nil
    end
  end
  return ...
end
function AlertDataList:GetTypeFromCode(code)
  if not self.K[code] then
    return "Tip"
  end
  return self.K[code].kind
end
function AlertDataList:GetTextFromErrorCode(code)
  if not self.K[code] then
    if AlertDataListExtra ~= nil and AlertDataListExtra[code] ~= nil then
      return ...
    else
      return code
    end
  end
  return ...
end
