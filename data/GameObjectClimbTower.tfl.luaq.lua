local GameObjectClimbTower = LuaObjectManager:GetLuaObject("GameObjectClimbTower")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameStateFormation = GameStateManager.GameStateFormation
local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameStateInstance = GameStateManager.GameStateInstance
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
local GameUIActivityNew = LuaObjectManager:GetLuaObject("GameUIActivityNew")
local QuestTutorialInfiniteCosmos = TutorialQuestManager.QuestTutorialInfiniteCosmos
local QuestTutorialInfiniteJumpStep = TutorialQuestManager.QuestTutorialInfiniteJumpStep
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameObjectBattleMapBG = LuaObjectManager:GetLuaObject("GameObjectBattleMapBG")
local GameObjectBattleReplay = LuaObjectManager:GetLuaObject("GameObjectBattleReplay")
local GameGoodsList = LuaObjectManager:GetLuaObject("GameGoodsList")
local GameStateStore = GameStateManager.GameStateStore
local needRecoverUI = false
local nextLadder
local needRecoverInstance = false
local currentFlashLayer = 0
local instanceActivityList
local isShowSelectGate = false
local rewardlist
local awardDataTable = {}
local window_type = ""
local checkForBattleResult = false
local hasLoadInstance = false
local isKilledFinalBoss = false
local currentStepInfo
local waitForClaimReward = false
local waitForAnimation = false
local gainedReward, specialAwards
local hasInitRewardslist = false
local hasInitRewardBox = false
local rankList
local hasInitRankList = false
local intervalTime, rushEndTime
local isStartRush = false
local needAdditionalRequest = false
local preLoadMsgtype, preLoadContent
GameObjectClimbTower.currentInstanceId = 0
local rushContext = ""
local stopRushContext = ""
local gainRewardContext = ""
function GameObjectClimbTower:OnInitGame()
end
function GameObjectClimbTower:Init()
  GameGlobalData:RegisterDataChangeCallback("resource", self.RefreshResource)
end
if AutoUpdate.isAndroidDevice then
  function GameObjectClimbTower.OnAndroidBack()
    DebugOut("hello world")
    if GameUIWDStuff.currentMenu == GameUIWDStuff.menuType.menu_type_help then
      local flash_obj = GameUIWDStuff:GetFlashObject()
      if flash_obj then
        flash_obj:InvokeASCallback("_root", "hideHelp")
      end
    elseif hasInitRewardslist then
      GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "CloseRewardListWindow")
    elseif hasInitRankList then
      GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "CloseRankListWindow")
    elseif currentFlashLayer == 4 then
      GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "backFatherLayer")
    elseif currentFlashLayer == 5 then
      DebugOut("hello world 5555")
      GameObjectClimbTower:OnFSCommand("closePopJump")
    elseif currentFlashLayer == 6 then
      DebugOut("hello world 6666")
      GameObjectClimbTower:OnFSCommand("closePopReplay")
    elseif GameObjectClimbTower.ExpeditionRankReward:IsShow() then
      GameObjectClimbTower.ExpeditionRankReward:Hide()
    else
      GameObjectClimbTower:OnFSCommand("Close_Window")
    end
  end
end
function GameGoodsList.RefreshResource()
  local crystalNum = 0
  local resource = GameGlobalData:GetData("resource")
  if tonumber(resource.crystal) ~= nil then
    crystalNum = tonumber(resource.crystal)
  end
  GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "setCrystalNum", crystalNum)
end
function GameObjectClimbTower:Clear()
  self:GetFlashObject():InvokeASCallback("_root", "ClearRewardList")
  self:GetFlashObject():InvokeASCallback("_root", "ClearGainedRewardBox")
end
function GameObjectClimbTower:OnAddToGameState()
  rushContext = GameLoader:GetGameText("LC_MENU_DUNGEON_SPACE_RUSH_BUTTON")
  stopRushContext = GameLoader:GetGameText("LC_MENU_DUNGEON_SPACE_END_RUSH_BUTTON")
  gainRewardContext = GameLoader:GetGameText("LC_MENU_DUNGEON_SPACE_COLLECT_BUTTON")
  DebugOut("GameObjectClimbTower:OnAddToGameState")
  self:LoadFlashObject()
  waitForClaimReward = false
  self:GetFlashObject():InvokeASCallback("_root", "Init")
  GameObjectClimbTower:CheckDownloadImage()
  if needRecoverUI then
    GameObjectClimbTower:recoverLadderUI()
    needRecoverUI = false
  elseif preLoadContent ~= nil then
    self:gotoInstanceDirectly(preLoadContent)
  elseif instanceActivityList ~= nil then
    self:refreshGateSelectUI()
  else
    assert(0)
    DebugOut("have not request instance data")
  end
end
function GameObjectClimbTower.DunGeonTutarialFinish()
  if GameObjectClimbTower:GetFlashObject() then
    GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "hideTutorial")
  end
  QuestTutorialInfiniteCosmos:SetFinish(true)
  if not QuestTutorialInfiniteJumpStep:IsFinished() then
    if currentStepInfo.cur_step > 1 then
      if not QuestTutorialInfiniteJumpStep:IsActive() then
        QuestTutorialInfiniteJumpStep:SetActive(true)
      end
      GameUICommonDialog:PlayStory({1100085}, GameObjectClimbTower.TutorialInfiniteJumpStepFinish)
    else
      QuestTutorialInfiniteJumpStep:SetFinish(true)
    end
  end
end
function GameObjectClimbTower:CheckDownloadImage()
  if (ext.crc32.crc32("data2/LAZY_LOAD_domination_bg.png") == "" or ext.crc32.crc32("data2/LAZY_LOAD_domination_bg.png") == nil) and not AutoUpdateInBackground:IsHeroFileUpdatedToServer("data2/LAZY_LOAD_domination_bg.png") then
    self:GetFlashObject():ReplaceTexture("LAZY_LOAD_domination_bg.png", "territorial_map_bg.png")
  end
end
function GameObjectClimbTower:clearLocalData()
  needRecoverUI = false
  nextLadder = nil
  needRecoverInstance = false
  currentFlashLayer = 0
  hasLoadInstance = false
  isKilledFinalBoss = false
  currentStepInfo = nil
  waitForClaimReward = false
  waitForAnimation = false
  gainedReward = nil
  specialAwards = nil
  hasInitRewardslist = false
  hasInitRewardBox = false
  rankList = false
  hasInitRankList = false
  intervalTime = nil
  rushEndTime = nil
  isStartRush = false
  needAdditionalRequest = false
  preLoadMsgtype = nil
  preLoadContent = nil
  instanceActivityList = nil
  needAnimation = false
  hasLoadInstance = false
  isShowSelectGate = false
  rewardlist = nil
  checkForBattleResult = false
end
function GameObjectClimbTower:QuitToGateSelect()
  needRecoverUI = false
  nextLadder = nil
  needRecoverInstance = false
  currentFlashLayer = 0
  hasLoadInstance = false
  isKilledFinalBoss = false
  currentStepInfo = nil
  waitForClaimReward = false
  waitForAnimation = false
  gainedReward = nil
  specialAwards = nil
  hasInitRewardslist = false
  hasInitRewardBox = false
  rankList = false
  hasInitRankList = false
  intervalTime = nil
  rushEndTime = nil
  isStartRush = false
  needAdditionalRequest = false
  preLoadMsgtype = nil
  needAnimation = false
  hasLoadInstance = false
  isShowSelectGate = true
  rewardlist = nil
  checkForBattleResult = false
end
function GameObjectClimbTower:OnEraseFromGameState(state)
  DebugOut("GameObjectClimbTower OnEraseFromGameState")
  self:UnloadFlashObject()
end
function GameObjectClimbTower:Update(dt)
  GameObjectClimbTower:UpdateInstanceTime()
  if isStartRush then
    GameObjectClimbTower:UpdateRushTime()
  end
  if self:GetFlashObject() then
    self:GetFlashObject():Update(dt)
  end
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "onUpdateFrame", dt)
  end
  if hasInitRewardslist then
    self:GetFlashObject():InvokeASCallback("_root", "OnUpdateList")
  end
  if hasInitRankList then
    self:GetFlashObject():InvokeASCallback("_root", "OnUpdateRankList")
  end
  if hasInitRewardBox then
    self:GetFlashObject():InvokeASCallback("_root", "OnUpdateGainedRewardBox")
  end
end
function GameObjectClimbTower:UpdateInstanceTime()
  if instanceActivityList == nil then
    return
  end
  local timeText = ""
  for i, item in pairs(instanceActivityList) do
    if item.status == 0 then
      item.timeDescription = GameLoader:GetGameText("LC_MENU_EVENT_OVER_CHAR")
    elseif 0 < item.left_seconds and item.left_stamp <= os.time() then
      item.left_seconds = 0
      item.timeDescription = GameLoader:GetGameText("LC_MENU_EVENT_OVER_CHAR")
      item.status = 0
    elseif 0 > item.left_seconds and item.left_stamp <= os.time() then
      DebugOut("UpdateInstanceTime \232\189\172\230\141\162\228\184\186\230\180\187\229\138\168\229\188\128\229\167\139\231\154\132\231\138\182\230\128\129")
      item.left_stamp = item.persistent_time + item.left_stamp
      item.left_seconds = item.persistent_time
      item.timeDescription = GameLoader:GetGameText("LC_MENU_EVENT_TIME_LEFT_CHAR")
      item.status = 1
    elseif 0 < item.left_seconds and item.left_stamp > os.time() then
      item.timeDescription = GameLoader:GetGameText("LC_MENU_EVENT_TIME_LEFT_CHAR")
      item.status = 1
    elseif 0 > item.left_seconds and item.left_stamp > os.time() then
      item.timeDescription = GameLoader:GetGameText("LC_MENU_EVENT_UNSTART_CHAR")
      item.status = -1
    end
    local showLock = 1
    if item.status == 1 then
      showLock = 0
    else
      showLock = 1
    end
    timeText = self:GetItemTimeText(i)
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_UpdateInstanceTime", item.sub_category, timeText, showLock)
  end
end
local previousSendInTime = 0
local sendNumber = 0
function GameObjectClimbTower:UpdateRushTime()
  if isStartRush then
    if rushEndTime - os.time() < 0 then
      GameObjectClimbTower.stopRushNow()
      return
    end
    if (rushEndTime - os.time()) % intervalTime == 0 then
      if previousSendInTime ~= os.time() then
        previousSendInTime = os.time()
        sendNumber = 0
      else
        sendNumber = 1
      end
      local function netRequestCall()
        local requestParam = {
          type = GameObjectClimbTower.currentInstanceId
        }
        NetMessageMgr:SendMsg(NetAPIList.ladder_fresh_req.Code, requestParam, GameObjectClimbTower.RefreshCallBack, false, nil)
      end
      if needAdditionalRequest then
        DebugOut("needAdditionalRequest")
        netRequestCall()
        needAdditionalRequest = false
        return
      end
      if sendNumber == 0 then
        DebugOut("prepare to send ladder_fresh_req rushEndTime " .. rushEndTime)
        DebugOut(isStartRush)
        netRequestCall()
      end
    end
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_updateRushTime", rushEndTime - os.time())
  end
end
function GameObjectClimbTower:Init()
  DebugOut("GameObjectClimbTower:Init()")
  waitForClaimReward = false
  DebugOut("checkForBattleResult set false")
  checkForBattleResult = false
  self:GetFlashObject():InvokeASCallback("_root", "Init")
end
function GameObjectClimbTower.CommonNetMessageCallBack(msgType, content)
  DebugOut("GameObjectClimbTower:CommonNetMessageCallBack111 " .. msgType)
  DebugTable(content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.ladder_gain_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    else
      DebugOut("ladder_gain_req success")
    end
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.items_req.Code then
    return true
  end
  return false
end
function GameObjectClimbTower.GetRankCallBack(msgType, content)
  DebugOut("GameObjectClimbTower:GetRankCallBack " .. msgType)
  DebugTable(content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.ladder_rank_req.Code then
    assert(content.code ~= 0)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.ladder_rank_ack.Code then
    rankList = content
    GameObjectClimbTower:buildDataForRankList(content)
    return true
  end
  return false
end
function GameObjectClimbTower.RefreshCallBack(msgType, content)
  DebugOut("GameObjectClimbTower:RefreshCallBack " .. msgType)
  DebugTable(content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.ladder_fresh_req.Code then
    assert(content.code ~= 0)
    local GameVip = LuaObjectManager:GetLuaObject("GameVip")
    GameVip:CheckIsNeedShowVip(content.code)
    return true
  elseif msgType == NetAPIList.ladder_fresh_ack.Code then
    gainedReward = nil
    GameObjectClimbTower.SyncRushTime(content.info.end_time + os.time())
    if content.info.raids_end then
      DebugOut("ResetCallBack raids_end")
      DebugOut(content.info.raids_end)
      gainedReward = content.info.has_rewards
      if isStartRush then
        GameObjectClimbTower.stopRushNow()
      end
    end
    if content.info.on_max_step and content.info.monster_hp == 0 then
      DebugOut(isKilledFinalBoss)
      isKilledFinalBoss = true
    else
      isKilledFinalBoss = false
    end
    if currentStepInfo and content.info.cur_step ~= currentStepInfo.cur_step then
      GameObjectClimbTower:exchangeLadderInfo(content.info)
      GameObjectClimbTower.nextLadder = content.info
    else
      GameObjectClimbTower:setLadderInfo(content.info)
      GameObjectClimbTower:SetUpUI()
    end
    return true
  end
  return false
end
function GameObjectClimbTower.ResetCallBack(msgType, content)
  DebugOut("GameObjectClimbTower:ResetCallBack " .. msgType)
  DebugTable(content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.ladder_reset_req.Code then
    assert(content.code ~= 0)
    if content.code == 1000001 then
      if GameObjectClimbTower:IsDungeon() then
        DebugOut("ladder_reset_cost")
        GameObjectClimbTower:GetCostCredit("ladder_reset_cost")
      elseif GameObjectClimbTower:IsWormHole() then
        DebugOut("wormhole_reset_cost")
        GameObjectClimbTower:GetCostCredit("wormhole_reset_cost")
      elseif GameObjectClimbTower:IsExpedition() then
        DebugOut("expedition_reset_cost")
        GameObjectClimbTower:GetCostCredit("expedition_reset_cost")
      end
    else
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    end
    return true
  elseif msgType == NetAPIList.ladder_reset_ack.Code then
    isKilledFinalBoss = false
    if content.info.cur_step ~= currentStepInfo.cur_step then
      GameObjectClimbTower:exchangeLadderInfo(content.info)
      GameObjectClimbTower.nextLadder = content.info
    else
      GameObjectClimbTower:setLadderInfo(content.info)
      GameObjectClimbTower:SetUpUI()
    end
    return true
  end
  return false
end
function GameObjectClimbTower.SyncRushTime(newTime)
  if not newTime or not isStartRush then
    return
  end
  if not rushEndTime then
    rushEndTime = newTime
  end
  if newTime ~= rushEndTime then
    DebugOut("new end time: " .. newTime)
    DebugOut("old end time: " .. rushEndTime)
    rushEndTime = newTime
    needAdditionalRequest = true
  end
end
function GameObjectClimbTower.RushRequestCallBack(msgType, content)
  DebugOut("GameObjectClimbTower:RushRequestCallBack " .. msgType)
  DebugTable(content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.ladder_raids_req.Code then
    assert(content.code ~= 0)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.ladder_raids_ack.Code then
    GameObjectClimbTower:startRushNow(content)
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.ladder_cancel_req.Code then
    DebugOut("rush stoped")
    return true
  end
  return false
end
function GameObjectClimbTower.BuyResetTimeCallBack(msgType, content)
  DebugOut("GameObjectClimbTower:BuyResetTimeCallBack " .. msgType)
  DebugTable(content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.ladder_buy_reset_req.Code then
    assert(content.code ~= 0)
    local GameVip = LuaObjectManager:GetLuaObject("GameVip")
    GameVip:CheckIsNeedShowVip(content.code)
    return true
  elseif msgType == NetAPIList.ladder_buy_reset_ack.Code then
    currentStepInfo.reset_num = content.num
    GameObjectClimbTower:SetResetTimes(content.num)
    local requestParam = {
      type = GameObjectClimbTower.currentInstanceId
    }
    NetMessageMgr:SendMsg(NetAPIList.ladder_reset_req.Code, requestParam, GameObjectClimbTower.ResetCallBack, true, nil)
    return true
  end
  return false
end
function GameObjectClimbTower.BuySearchTimeCallBack(msgType, content)
  DebugOut("GameObjectClimbTower:BuySearchTimeCallBack " .. msgType)
  DebugTable(content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.ladder_buy_search_req.Code then
    assert(content.code ~= 0)
    local GameVip = LuaObjectManager:GetLuaObject("GameVip")
    GameVip:CheckIsNeedShowVip(content.code)
    return true
  elseif msgType == NetAPIList.ladder_buy_search_ack.Code then
    currentStepInfo.search_num = content.num
    GameObjectClimbTower:SetSearchTimes(content.num)
    return true
  end
  return false
end
function GameObjectClimbTower:GetCostCredit(content)
  local requestParam = {
    price_type = content,
    type = GameObjectClimbTower.currentInstanceId
  }
  NetMessageMgr:SendMsg(NetAPIList.price_req.Code, requestParam, self.getCostCreditCallback, true, nil)
end
function GameObjectClimbTower.getCostCreditCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.price_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  if msgType == NetAPIList.price_ack.Code then
    if content.price >= 0 then
      local info = ""
      local callback
      if content.price_type == "ladder_search_cost" or content.price_type == "wormhole_search_cost" or content.price_type == "expedition_search_cost" then
        info = GameLoader:GetGameText("LC_MENU_BATTLE_SUPPLY_NOT_ENOUGH_BUY_ASK")
        info = string.gsub(info, "<credits_num>", content.price)
        info = string.gsub(info, "<supply_num>", 1)
        if content.price_type == "expedition_search_cost" then
          info = string.format(GameLoader:GetGameText("LC_MENU_EXPEDITION_CHALLENGE_BUY_TIME_ALERT"), content.price)
        end
        function callback()
          local requestParam = {
            type = GameObjectClimbTower.currentInstanceId
          }
          NetMessageMgr:SendMsg(NetAPIList.ladder_buy_search_req.Code, requestParam, GameObjectClimbTower.BuySearchTimeCallBack, true, nil)
        end
      elseif content.price_type == "ladder_reset_cost" or content.price_type == "wormhole_reset_cost" or content.price_type == "expedition_reset_cost" then
        info = string.format(GameLoader:GetGameText("LC_MENU_BOSS_REFRESH_ASK"), content.price)
        if content.price_type == "expedition_reset_cost" then
          info = string.format(GameLoader:GetGameText("LC_MENU_EXPEDITION_RESET_BUY_TIME_ALERT"), content.price)
        end
        function callback()
          local requestParam = {
            type = GameObjectClimbTower.currentInstanceId
          }
          NetMessageMgr:SendMsg(NetAPIList.ladder_buy_reset_req.Code, requestParam, GameObjectClimbTower.BuyResetTimeCallBack, true, nil)
        end
      end
      local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
      GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
      GameUIMessageDialog:SetYesButton(callback)
      GameUIMessageDialog:Display(text_title, info)
    end
    return true
  end
  return false
end
function GameObjectClimbTower:buildDataForRankList(content)
  DebugOut("buildDataForRankList")
  local selfInfo = content.self
  DebugTable(selfInfo)
  self:GetFlashObject():InvokeASCallback("_root", "setPlayerRankInfo", selfInfo.rank, selfInfo.name, selfInfo.level, selfInfo.step)
  self:GetFlashObject():InvokeASCallback("_root", "syncRankList", #content.rank)
end
function GameObjectClimbTower.GetSpecialAwardsCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.ladder_special_req.Code then
    assert(content.code ~= 0)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.ladder_special_ack.Code then
    GameObjectClimbTower:buildDataForRewardList(content.info)
    return true
  end
  return false
end
function GameObjectClimbTower:gotoInstanceSelectScreen(activitylist)
  DebugOut("GameObjectClimbTower:gotoInstanceSelectScreen")
  DebugTable(activitylist)
  instanceActivityList = activitylist
  NetMessageMgr:SendMsg(NetAPIList.dungeon_open_req.Code, nil, GameObjectClimbTower.gotoInstanceSelectCallback, true, nil)
end
function GameObjectClimbTower.gotoInstanceSelectCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.dungeon_open_req.Code then
    assert(content.code ~= 0)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.dungeon_open_ack.Code then
    instanceActivityList = instanceActivityList or GameUIActivityNew:getInstanceData()
    for i, item in pairs(instanceActivityList) do
      for iC, vC in ipairs(content.info) do
        DebugOut("item.sub_category = ", item.sub_category)
        DebugOut("vC.active_id = ", vC.active_id)
        if vC.active_id == item.sub_category then
          item.current_holiday = vC.pic_name
          item.current_holidatName = vC.activity_name
          item.storeOpened = vC.store_tag
          item.hasNews = vC.red_point
        end
      end
    end
    GameStateManager.GameStateLab:GoToSubState(STATE_LAB_SUB_STATE.SUBSTATE_COSMOS)
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateInstance)
    return true
  end
  return false
end
function GameObjectClimbTower:preRequestInstanceData(id)
  DebugOut("GameObjectClimbTower:preRequestInstanceData", id)
  isStartRush = false
  gainedReward = nil
  local requestParam = {active_id = id}
  NetMessageMgr:SendMsg(NetAPIList.dungeon_enter_req.Code, requestParam, GameObjectClimbTower.preRequestInstanceDataCallBack, true, nil)
end
function GameObjectClimbTower.preRequestInstanceDataCallBack(msgType, content)
  DebugOut("preRequestInstanceDataCallBack", msgType)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.dungeon_enter_req.Code then
    assert(content.code ~= 0)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.dungeon_enter_ack.Code then
    DebugOut("preRequestInstanceDataCallBack dungeon_enter_ack")
    DebugTable(content)
    local tipString = ""
    if content.code == 1000008 then
      tipString = GameLoader:GetGameText("LC_ALERT_dungeon_ladder_level")
      tipString = string.format(tipString, content.lev)
      GameTip:Show(tipString)
      hasInitRewardslist = false
      hasInitRankList = false
      hasInitRewardBox = false
      hasLoadInstance = false
      return true
    elseif content.code == 1000023 then
      tipString = GameLoader:GetGameText("LC_ALERT_dungeon_wormhole_level")
      tipString = string.format(tipString, content.lev)
      GameTip:Show(tipString)
      hasInitRewardslist = false
      hasInitRankList = false
      hasInitRewardBox = false
      hasLoadInstance = false
      return true
    elseif content.code == 1000029 then
      tipString = GameLoader:GetGameText("LC_ALERT_expedition_level_limited")
      tipString = string.format(tipString, content.lev)
      GameTip:Show(tipString)
      hasInitRewardslist = false
      hasInitRankList = false
      hasInitRewardBox = false
      hasLoadInstance = false
      return true
    end
    DebugOut("error content", content.code)
    return true
  elseif msgType == NetAPIList.ladder_enter_ack.Code then
    DebugOut("pre ladder_enter_ack")
    preLoadMsgtype = msgType
    preLoadContent = content
    NetMessageMgr:SendMsg(NetAPIList.dungeon_open_req.Code, nil, GameObjectClimbTower.gotoInstanceSelectCallback, true, nil)
    return true
  end
  return false
end
function GameObjectClimbTower:gotoInstanceDirectly(content)
  DebugOut("gotoInstanceDirectly")
  local instanceName = GameObjectClimbTower:getInstanceNameById()
  DebugOut("current id", GameObjectClimbTower.currentInstanceId, instanceName)
  GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "lua2fs_gotoInstance", nil, instanceName)
  DebugOut("GameObjectClimbTower.category = ", GameObjectClimbTower.category)
  local canShowStore = GameObjectClimbTower:getCanShowShopBySubCategory(GameObjectClimbTower.currentInstanceId)
  DebugOut("canShowStore 222 = ", canShowStore)
  if canShowStore and canShowStore > 0 then
    local crystalNum = 0
    local resource = GameGlobalData:GetData("resource")
    if tonumber(resource.crystal) ~= nil then
      crystalNum = tonumber(resource.crystal)
    end
    GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "showShopButton", true, crystalNum)
  else
    GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "showShopButton", false, crystalNum)
  end
  GameObjectClimbTower:setLadderInfo(content.info)
  if currentStepInfo.on_max_step and currentStepInfo.monster_hp == 0 then
    DebugOut(isKilledFinalBoss)
    isKilledFinalBoss = true
  end
  if 0 < currentStepInfo.end_time then
    GameObjectClimbTower:startRushNow(currentStepInfo)
  elseif 0 >= currentStepInfo.end_time and currentStepInfo.has_rewards and 0 < #currentStepInfo.has_rewards then
    GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "lua2fs_setRushState", gainRewardContext, 3, nil)
    waitForClaimReward = true
  else
    GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "lua2fs_setRushState", rushContext, 2, nil)
  end
  GameObjectClimbTower:SetUpUI()
end
function GameObjectClimbTower:RequestInstanceData(id)
  DebugOut("GameObjectClimbTower:RequestInstanceData", id)
  isStartRush = false
  gainedReward = nil
  GameObjectClimbTower.currentInstanceId = id
  GameStateInstance.currentInstanceId = id
  local requestParam = {
    active_id = GameObjectClimbTower.currentInstanceId
  }
  NetMessageMgr:SendMsg(NetAPIList.dungeon_enter_req.Code, requestParam, GameObjectClimbTower.RequestInstanceDataCallBack, true, nil)
end
function GameObjectClimbTower.RequestInstanceDataCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.dungeon_enter_req.Code then
    assert(content.code ~= 0)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.dungeon_enter_ack.Code then
    DebugOut("dungeon_enter_ack")
    local tipString = ""
    if content.code == 1000008 then
      tipString = GameLoader:GetGameText("LC_ALERT_dungeon_ladder_level")
      tipString = string.format(tipString, content.lev)
      GameTip:Show(tipString)
      hasInitRewardslist = false
      hasInitRankList = false
      hasInitRewardBox = false
      hasLoadInstance = false
      return true
    elseif content.code == 1000023 then
      tipString = GameLoader:GetGameText("LC_ALERT_dungeon_wormhole_level")
      tipString = string.format(tipString, content.lev)
      GameTip:Show(tipString)
      hasInitRewardslist = false
      hasInitRankList = false
      hasInitRewardBox = false
      hasLoadInstance = false
      return true
    end
    DebugOut("error content", content.code)
    return true
  elseif msgType == NetAPIList.ladder_enter_ack.Code then
    DebugOut("RequestInstanceDataCallBack ladder_enter_ack")
    local instanceName = GameObjectClimbTower:getInstanceNameById()
    DebugOut("current id", GameObjectClimbTower.currentInstanceId, instanceName)
    GameStateInstance:LoadBackGround()
    DebugOut("GameObjectClimbTower.category = ", GameObjectClimbTower.category)
    local canShowStore = GameObjectClimbTower:getCanShowShopBySubCategory(GameObjectClimbTower.currentInstanceId)
    DebugOut("canShowStore 333 = ", canShowStore)
    if canShowStore and canShowStore > 0 then
      local crystalNum = 0
      local resource = GameGlobalData:GetData("resource")
      if tonumber(resource.crystal) ~= nil then
        crystalNum = tonumber(resource.crystal)
      end
      GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "showShopButton", true, crystalNum)
    else
      GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "showShopButton", false, crystalNum)
    end
    GameObjectClimbTower:setLadderInfo(content.info)
    GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "lua2fs_gotoInstance", nil, instanceName)
    if currentStepInfo.on_max_step and currentStepInfo.monster_hp == 0 then
      DebugOut(isKilledFinalBoss)
      isKilledFinalBoss = true
    end
    if 0 < currentStepInfo.end_time then
      GameObjectClimbTower:startRushNow(currentStepInfo)
    elseif 0 >= currentStepInfo.end_time and currentStepInfo.has_rewards and 0 < #currentStepInfo.has_rewards then
      GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "lua2fs_setRushState", gainRewardContext, 3, nil)
      waitForClaimReward = true
    else
      GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "lua2fs_setRushState", rushContext, 2, nil)
    end
    DebugOut("QuestTutorialInfiniteCosmos climb here")
    DebugOut("GameObjectClimbTower.category = ", GameObjectClimbTower.category)
    if GameObjectClimbTower.category == GameUIActivityNew.category.TYPE_DUNGEON then
      if not QuestTutorialInfiniteCosmos:IsFinished() then
        if not QuestTutorialInfiniteCosmos:IsActive() then
          QuestTutorialInfiniteCosmos:SetActive(true)
          local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
          GameUIBarLeft:ShowIsNewFunctionInLab()
        end
        if GameObjectClimbTower:GetFlashObject() then
          GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "showTutorial")
        end
        GameUICommonDialog:PlayStory({1100055}, GameObjectClimbTower.DunGeonTutarialFinish)
      elseif not QuestTutorialInfiniteJumpStep:IsFinished() then
        if content.is_first and currentStepInfo.cur_step > 1 then
          if not QuestTutorialInfiniteJumpStep:IsActive() then
            QuestTutorialInfiniteJumpStep:SetActive(true)
          end
          GameUICommonDialog:PlayStory({1100085}, GameObjectClimbTower.TutorialInfiniteJumpStepFinish)
        else
          QuestTutorialInfiniteJumpStep:SetFinish(true)
        end
      end
    end
    GameObjectClimbTower:SetUpUI()
    return true
  end
  return false
end
function GameObjectClimbTower.TutorialInfiniteJumpStepFinish()
  QuestTutorialInfiniteJumpStep:SetFinish(true)
end
function GameObjectClimbTower:BattleNow()
  DebugOut("GameObjectClimbTower:BattleNow")
  local requestParam = {
    type = GameObjectClimbTower.currentInstanceId
  }
  if GameStateManager:GetCurrentGameState().fightRoundData then
    GameStateManager:GetCurrentGameState().fightRoundData = nil
  end
  NetMessageMgr:SendMsg(NetAPIList.ladder_search_req.Code, requestParam, GameObjectClimbTower.searchCallBack, true, nil)
  awardDataTable = nil
end
function GameObjectClimbTower.searchCallBack(msgType, content)
  DebugOut("GameObjectClimbTower:searchCallBack " .. msgType)
  if msgType == NetAPIList.ladder_search_ack.Code and content.result == 1 then
    checkForBattleResult = true
    DebugOut("checkForBattleResult set true")
  end
  local function callback()
    DebugOut("GameObjectClimbTower:battle callback")
    window_type = ""
    GameUIBattleResult:LoadFlashObject()
    if GameObjectClimbTower:IsExpedition() then
      GameUIBattleResult:SetFightReport(GameObjectBattleReplay.GroupBattleReportArr, nil, nil, true)
    else
      GameUIBattleResult:SetFightReport(content.report, -1, -1)
    end
    GameUIBattleResult:UpdateExperiencePercent(true)
    GameGlobalData:UpdateFleetInfoAfterBattle(content.fleets)
    GameGlobalData:RefreshData("fleetinfo")
    if content.info.on_max_step and content.info.monster_hp == 0 then
      isKilledFinalBoss = true
    end
    GameObjectClimbTower.nextLadder = content.info
    GameStateInstance:EnterInstance()
    if content.result == 1 then
      awardDataTable = {}
      for _, award_data in ipairs(content.rewards) do
        local count = GameHelper:GetAwardCount(award_data.item_type, award_data.number, award_data.no)
        local itemName = GameHelper:GetAwardNameText(award_data.item_type, award_data.number)
        local icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(award_data, nil, GameObjectClimbTower.DynamicResDowloadFinished)
        local flag = false
        if award_data.item_type == "money" or award_data.item_type == "technique" then
          flag = false
        else
          flag = true
        end
        DebugOut("itemName" .. itemName .. "icon " .. icon)
        if award_data.item_type ~= "exp" then
          table.insert(awardDataTable, {
            itemType = award_data.item_type,
            itemDesc = itemName,
            itemNumber = count,
            itemIcon = icon,
            isDirectSet = flag,
            itemID = award_data.number,
            itemLevel = award_data.level
          })
        end
      end
      window_type = "battle_win"
      GameUIBattleResult:UpdateAwardList(window_type, awardDataTable)
    else
      GameUIBattleResult:RandomizeImprovement()
      window_type = "battle_lose"
      DebugOut("content.info.monster_hp" .. content.info.monster_hp)
      if content.result == -1 then
        DebugOut("isConditionNotMeet")
        GameUIBattleResult.isConditionNotMeet = true
      end
    end
    GameUIBattleResult:AnimationMoveIn(window_type, false)
    GameStateInstance:AddObject(GameUIBattleResult)
  end
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.ladder_search_req.Code then
    if content.code == 1000010 then
      DebugOut("content.code == 1000010")
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      GameStateBattlePlay:RegisterOverCallback(callback)
      GameStateBattlePlay.curBattleType = "climbtower"
    else
      assert(content.code ~= 0)
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    end
    return true
  elseif msgType == NetAPIList.ladder_search_ack.Code then
    DebugOut("GameObjectClimbTower:NetAPIList.ladder_search_ack.Code")
    DebugTable(content)
    local function mycallback()
      GameStateBattlePlay:RegisterOverCallback(callback)
      GameStateBattlePlay.curBattleType = "climbtower"
      if #content.report.rounds == 0 and GameStateManager:GetCurrentGameState().fightRoundData then
        content.report.rounds = GameStateManager:GetCurrentGameState().fightRoundData
      end
      if GameObjectClimbTower:IsExpedition() then
        GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_PLAY, GameObjectBattleReplay.GroupBattleReportArr[1].headers[1].report, -1, -1)
      else
        GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_PLAY, content.report, -1, -1)
      end
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateBattlePlay)
    end
    if GameStateManager:GetCurrentGameState():IsObjectInState(ItemBox) and ItemBox.currentBox == "rewardInfo" then
      ItemBox:SetDelayShowAfterReward(mycallback, nil)
    else
      mycallback()
    end
    return true
  end
  return false
end
function GameObjectClimbTower:startRushNow(content)
  rushEndTime = content.end_time
  intervalTime = content.interval
  DebugOut("startRushNow")
  DebugOut("rushEndTime " .. rushEndTime)
  DebugOut("intervalTime " .. intervalTime)
  rushEndTime = rushEndTime + os.time()
  isStartRush = true
  self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setRushState", stopRushContext, 1, content.end_time)
end
function GameObjectClimbTower.stopRushNow()
  local restTime = rushEndTime - os.time()
  isStartRush = false
  DebugOut("stopRushNow restTime: " .. restTime, "isStartRush ", isStartRush)
  DebugTable(gainedReward)
  if gainedReward and #gainedReward > 0 then
    GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "lua2fs_setRushState", gainRewardContext, 3, nil)
    waitForClaimReward = true
  else
    GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "lua2fs_setRushState", rushContext, 2, nil)
  end
  if restTime > 0 then
    local requestParam = {
      type = GameObjectClimbTower.currentInstanceId
    }
    NetMessageMgr:SendMsg(NetAPIList.ladder_cancel_req.Code, requestParam, GameObjectClimbTower.RushRequestCallBack, true, nil)
    currentStepInfo.end_time = 0
  else
  end
  rushEndTime = 0
  intervalTime = 0
end
function GameObjectClimbTower:GetItemTimeText(index)
  local _time = 0
  local timeText = ""
  local item = instanceActivityList[index]
  if item.left_stamp then
    item.left_stamp = tonumber(item.left_stamp)
    _time = item.left_stamp - os.time()
    if _time < 0 then
      _time = 0
    end
    timeText = self:GetFlashObject():InvokeASCallback("_root", "formatTimeStringToNBit", _time)
  end
  return ...
end
local testHolidayPic = {
  "instance_bg_test1.png",
  "instance_bg_test2.png",
  "instance_bg_test3.png",
  "instance_bg_test4.png"
}
local testHolidayBattlePic = {
  "instance_battlebg_test1.png",
  "instance_battlebg_test2.png",
  "instance_battlebg_test3.png",
  "instance_battlebg_test4.png"
}
function GameObjectClimbTower:refreshGateSelectUI()
  DebugOut("GameObjectClimbTower:refreshGateSelectUI")
  isShowSelectGate = true
  local timeTable = {}
  local isOpenTable = {}
  local IdTable = {}
  local nameTable = {}
  local subLockTable = {}
  local posTable = {}
  local hasHolidayPicTable = {}
  local noTimeTable = {}
  local hasNewsTable = {}
  local lockTextTable = GameLoader:GetGameText("LC_MENU_COMMANDER_RECRUIT_STATUS_LOCKED")
  local IndexData = 1
  for i, item in pairs(instanceActivityList) do
    timeTable[IndexData] = self:GetItemTimeText(i)
    DebugOut("item.status", item.status)
    if item.category == GameUIActivityNew.category.TYPE_DUNGEON then
      noTimeTable[IndexData] = 1
    else
      noTimeTable[IndexData] = 0
    end
    item.pos = IndexData
    isOpenTable[IndexData] = item.status
    IdTable[IndexData] = item.sub_category
    local title = item.title
    if item.current_holidatName and item.current_holidatName ~= "none" then
      title = GameLoader:GetGameText(item.current_holidatName)
    end
    nameTable[IndexData] = title
    posTable[IndexData] = IndexData
    if item.status == 1 then
      subLockTable[IndexData] = 0
    else
      subLockTable[IndexData] = 1
    end
    hasNewsTable[IndexData] = item.hasNews
    DebugTable(item)
    if item.current_holiday and item.current_holiday ~= "none" then
      local holidayPicName = item.current_holiday
      local localPath = "data2/" .. holidayPicName
      if DynamicResDownloader:IfResExsit(localPath) then
        hasHolidayPicTable[IndexData] = "1"
        local origName = "instance_bg_p" .. item.pos .. ".png"
        DebugOut("origName = ", origName)
        DebugOut("holidayPicName = ", holidayPicName)
        GameObjectClimbTower:GetFlashObject():ReplaceTexture(origName, holidayPicName)
      else
        local extInfo = {}
        extInfo.pos = item.pos
        extInfo.resName = holidayPicName
        local function callback(extInfo)
          if extInfo and extInfo.resName and extInfo.pos then
            local origName = "instance_bg_p" .. extInfo.pos .. ".png"
            DebugOut("origName = ", origName)
            DebugOut("extInfo.pos = ", extInfo.pos)
            DebugOut("extInfo.resName = ", extInfo.resName)
            GameObjectClimbTower:GetFlashObject():ReplaceTexture(origName, extInfo.resName)
            GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "updateInstanceState", extInfo.pos - 1, "1")
          end
        end
        hasHolidayPicTable[IndexData] = "-1"
        DynamicResDownloader:AddDynamicRes(holidayPicName, DynamicResDownloader.resType.INFINITE_PIC, extInfo, callback)
      end
    else
      hasHolidayPicTable[IndexData] = "-1"
    end
    IndexData = IndexData + 1
  end
  if #timeTable == 1 then
    timeTable[1] = timeTable[1] .. "\001"
  end
  if #isOpenTable == 1 then
    isOpenTable[1] = isOpenTable[1] .. "\001"
  end
  if #IdTable == 1 then
    IdTable[1] = IdTable[1] .. "\001"
  end
  if #nameTable == 1 then
    nameTable[1] = nameTable[1] .. "\001"
  end
  if #subLockTable == 1 then
    subLockTable[1] = subLockTable[1] .. "\001"
  end
  if #posTable == 1 then
    posTable[1] = posTable[1] .. "\001"
  end
  if #hasHolidayPicTable == 1 then
    hasHolidayPicTable[1] = hasHolidayPicTable[1] .. "\001"
  end
  if #noTimeTable == 1 then
    noTimeTable[1] = noTimeTable[1] .. "\001"
  end
  if #hasNewsTable == 1 then
    hasNewsTable[1] = hasNewsTable[1] .. "\001"
  end
  if not hasLoadInstance then
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_loadInstanceState", table.concat(timeTable, "\001"), table.concat(isOpenTable, "\001"), table.concat(IdTable, "\001"), needAnimation, table.concat(nameTable, "\001"), table.concat(subLockTable, "\001"), table.concat(posTable, "\001"), table.concat(hasHolidayPicTable, "\001"), table.concat(noTimeTable, "\001"), lockTextTable, table.concat(hasNewsTable, "\001"))
    hasLoadInstance = true
  end
end
function GameObjectClimbTower:getInstanceNameById()
  local name = ""
  if self:IsDungeon() then
    name = GameLoader:GetGameText("LC_MENU_DUNGEON_SPACE_TITLE")
  elseif self:IsWormHole() then
    name = GameLoader:GetGameText("LC_MENU_DUNGEON_WORM_TITLE")
  elseif self:IsExpedition() then
    name = GameLoader:GetGameText("LC_MENU_EXPEDITION_TITLE")
  else
    DebugOut("error instance category", GameObjectClimbTower.category)
  end
  DebugOut("getInstanceNameById", GameObjectClimbTower.category, name)
  return name
end
function GameObjectClimbTower:getCategoryBySubCategory(subId)
  DebugOut("getCategoryBySubCategory", subId)
  DebugTable(instanceActivityList)
  for i, item in pairs(instanceActivityList) do
    if item.sub_category == subId then
      DebugOut("category", item.category)
      return item.category
    end
  end
  return nil
end
function GameObjectClimbTower:getContentBySubCategory(subId)
  for i, item in pairs(instanceActivityList) do
    if item.sub_category == subId then
      DebugOut("category", item.category)
      return item.content
    end
  end
  return ""
end
function GameObjectClimbTower:getCanShowShopBySubCategory(subId)
  DebugOut("getCategoryBySubCategory", subId)
  for i, item in pairs(instanceActivityList) do
    if item.sub_category == subId then
      DebugOut("category", item.category)
      return item.storeOpened
    end
  end
  return nil
end
function GameObjectClimbTower:setLadderInfo(content)
  currentFlashLayer = 4
  if content ~= nil then
    currentStepInfo = content
    GameDataAccessHelper.LadderCurrentStepInfo = content
  else
    DebugOut("setLadderInfo error: content")
    return
  end
  if not self:GetFlashObject() then
    return
  end
  DebugOutPutTable(content, "setLadderInfo")
  GameObjectClimbTower:SetResetTimes(content.reset_num)
  GameObjectClimbTower:SetSearchTimes(content.search_num)
  local dialog = GameLoader:GetGameText("LC_EVENT_DUNDEON_DIALOG_" .. content.dialog)
  DebugOut("dialog: " .. dialog)
  local monsterName = GameLoader:GetGameText("LC_NPC_NPC_" .. content.monster_name)
  DebugOut("monsterName: " .. monsterName)
  DebugOut(content.monster_name)
  local monsterAvatar = self:GetMonsterHeadIconFromMatrix(self:GetMultiMatrixLastSection(content.matrix), content.captain_index)
  local forceStr = GameLoader:GetGameText("LC_MENU_NEW_SPACE_BASE_FORCE_CHAR") .. " " .. GameUtils.numberAddComma(currentStepInfo.force)
  DebugOut("forceStr = ", forceStr)
  DebugOut("currentStepInfo = ", currentStepInfo)
  self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setLadderInfo", content.cur_step, monsterName, monsterAvatar, content.monster_hp, GameObjectClimbTower.concatCondition(content.condition), content.level, dialog, nil, forceStr, content.can_award)
  if self:IsDungeon() then
    GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "isShowRankButton", true)
  else
    GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "isShowRankButton", false)
  end
  self:SetRankRewardBtn(content.can_award)
  rewardlist = content.may_rewards
  self:setAwardsInfo(rewardlist)
  gainedReward = nil
  if content.has_rewards ~= nil and #content.has_rewards > 0 then
    gainedReward = content.has_rewards
    if not isStartRush then
      GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "lua2fs_setRushState", gainRewardContext, 3, nil)
      waitForClaimReward = true
    end
  end
  if self:IsExpedition() and GameObjectClimbTower.milestoneRewards_exp ~= nil then
    self:SetMilestoneRewardInfo_exp(GameObjectClimbTower.milestoneRewards_exp)
  end
  if self:IsExpedition() and GameObjectClimbTower.leaderboard_exp ~= nil then
    self:SetLeaderboardInfo_exp(GameObjectClimbTower.leaderboard_exp)
  end
end
function GameObjectClimbTower:isShowRushButton()
  DebugOut("---isShowRushButton = ")
  DebugTable(GameUIActivityNew.speedResource)
  DebugOut(GameObjectClimbTower.currentInstanceId)
  if GameUIActivityNew.speedResource then
    for k, v in ipairs(GameUIActivityNew.speedResource) do
      if v.dungeon_id == GameObjectClimbTower.currentInstanceId then
        return false
      end
    end
  end
  return true
end
function GameObjectClimbTower.concatCondition(condtion)
  local config_round_String = GameLoader:GetGameText("LC_MENU_DUNGEON_SPACE_COMPLETION_ROUND")
  local config_hp_String = GameLoader:GetGameText("LC_MENU_DUNGEON_SPACE_COMPLETION_LOST_HP")
  local config_fleet_String = GameLoader:GetGameText("LC_MENU_DUNGEON_SPACE_COMPLETION_LOST_FLEET")
  local config_none_condition = GameLoader:GetGameText("LC_MENU_DUNGEON_SPACE_COMPLETION_NONE")
  if not condtion then
    return config_none_condition
  end
  DebugOut("concatCondition" .. condtion)
  local retString = ""
  local roundNum = condtion:match("{round,(%d+)}")
  if roundNum then
    DebugOut(roundNum)
    roundNum = tonumber(roundNum)
    if type(roundNum) == "number" then
      retString = retString .. string.format(config_round_String, roundNum)
      retString = retString .. " "
    end
  end
  local fleetNum = condtion:match("{lost_fleet,(%d+)}")
  if fleetNum then
    DebugOut(fleetNum)
    fleetNum = tonumber(fleetNum)
    if type(fleetNum) == "number" then
      retString = retString .. string.format(config_fleet_String, fleetNum)
      retString = retString .. " "
    end
  end
  local hpNum = condtion:match("{lost_hp,(%d+)}")
  if hpNum then
    DebugOut(hpNum)
    hpNum = hpNum .. "%"
    retString = retString .. string.format(config_hp_String, hpNum)
    retString = retString .. " "
  end
  DebugOut(retString)
  if retString == "" then
    retString = config_none_condition
  end
  return retString
end
function GameObjectClimbTower:exchangeLadderInfo(content)
  if content ~= nil then
  else
    assert(content ~= nil)
    return
  end
  waitForAnimation = true
  local old = GameObjectClimbTower:GetMonsterHeadIconFromMatrix(self:GetMultiMatrixLastSection(currentStepInfo.matrix), currentStepInfo.captain_index)
  local new = GameObjectClimbTower:GetMonsterHeadIconFromMatrix(self:GetMultiMatrixLastSection(content.matrix), content.captain_index)
  DebugOut("old:" .. old)
  DebugOut("new:" .. new)
  self:GetFlashObject():InvokeASCallback("_root", "lua2fs_exchangeBossHeadAnimation", old, new)
end
function GameObjectClimbTower:setRecoverData(ladderInfo)
  needRecoverUI = true
  hasLoadInstance = false
  needRecoverInstance = true
  if ladderInfo ~= nil then
    currentStepInfo = ladderInfo
  end
end
function GameObjectClimbTower:recoverLadderUI()
  DebugOut("GameObjectClimbTower:recoverLadderUI")
  DebugOut("currentInstanceId ", GameObjectClimbTower.currentInstanceId)
  DebugTable(currentStepInfo)
  DebugOut("GameObjectClimbTower.category = ", GameObjectClimbTower.category)
  local canShowStore = GameObjectClimbTower:getCanShowShopBySubCategory(GameObjectClimbTower.currentInstanceId)
  DebugOut("canShowStore = ", canShowStore)
  if canShowStore and canShowStore > 0 then
    local crystalNum = 0
    local resource = GameGlobalData:GetData("resource")
    if tonumber(resource.crystal) ~= nil then
      crystalNum = tonumber(resource.crystal)
    end
    GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "showShopButton", true, crystalNum)
  else
    GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "showShopButton", false, crystalNum)
  end
  GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "lua2fs_gotoInstance", nil, self:getInstanceNameById())
  GameObjectClimbTower:setLadderInfo(currentStepInfo)
  if 0 < currentStepInfo.end_time then
    GameObjectClimbTower:startRushNow(currentStepInfo)
  elseif 0 >= currentStepInfo.end_time and currentStepInfo.has_rewards ~= nil and 0 < #currentStepInfo.has_rewards then
    DebugTable(currentStepInfo.has_rewards)
    GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "lua2fs_setRushState", gainRewardContext, 3, nil)
    waitForClaimReward = true
  else
    GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "lua2fs_setRushState", rushContext, 2, nil)
  end
  GameObjectClimbTower:SetUpUI()
end
function GameObjectClimbTower:setAwardsInfo(content)
  if content == nil then
    DebugOut("[error] GameObjectClimbTower:setAwardsInfo get nil content")
    return
  end
  local i = 1
  for k, v in pairs(content) do
    local itemName = GameHelper:GetAwardNameText(v.item_type, v.number)
    local icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v, nil, GameObjectClimbTower.DynamicResDowloadFinished)
    local count = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
    DebugOut("itemName: " .. itemName .. "icon: " .. icon .. "count: " .. count)
    if self:GetFlashObject() then
      self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setAwardInfo", icon, "", count, i)
    end
    i = i + 1
  end
  for i = i, 4 do
    if self:GetFlashObject() then
      self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setAwardInfo", -1, "", nil, i)
    end
  end
end
function GameObjectClimbTower:UpdateToprankItem(itemIndex)
  DebugOut("GameObjectClimbTower:UpdateToprankItem" .. itemIndex)
  local playerData = rankList.rank[itemIndex]
  DebugTable(playerData)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "setRankItem", itemIndex, playerData.rank, playerData.name, playerData.level, playerData.step)
  end
end
function GameObjectClimbTower.CheckToNextLadder()
  if GameObjectClimbTower.nextLadder.cur_step ~= currentStepInfo.cur_step then
    GameObjectClimbTower:exchangeLadderInfo(GameObjectClimbTower.nextLadder)
  else
    GameObjectClimbTower:setLadderInfo(GameObjectClimbTower.nextLadder)
    GameObjectClimbTower:SetUpUI()
  end
  if gainedReward ~= nil and #gainedReward > 0 then
    GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "lua2fs_setRushState", gainRewardContext, 3, nil)
  else
    GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "lua2fs_setRushState", rushContext, 2, nil)
  end
end
function GameObjectClimbTower:ShowGainedRewardBox()
  if not gainedReward or #gainedReward <= 0 then
    DebugOut("error ShowGainedRewardBox")
    GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "lua2fs_setRushState", rushContext, 2, nil)
    return
  end
  waitForClaimReward = false
  GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "InitGainedRewardBox", 1)
  self:GetFlashObject():InvokeASCallback("_root", "lua2fs_gotoSubPage", 3, nil)
  hasInitRewardBox = true
  currentFlashLayer = 3
  DebugOut("show reward")
  DebugTable(gainedReward)
  for i = 1, #gainedReward do
    self:GetFlashObject():InvokeASCallback("_root", "addGainedRewardItem", i)
  end
end
function GameObjectClimbTower:OnFSCommand(cmd, arg)
  DebugOut("GameObjectClimbTower:OnFSCommand " .. cmd)
  if cmd == "needUpdateItem" then
    if specialAwards == nil then
      DebugOut("specialAwards not ok ")
      return
    end
    local itemKey = tonumber(arg)
    local curAward = specialAwards[itemKey].rewards
    local rewardStatus = specialAwards[itemKey].status
    local rewardStep = specialAwards[itemKey].step
    DebugOut("needUpdateItem: ")
    DebugTable(curAward)
    local rewardsNameTable = {}
    local rewardsIconTable = {}
    local rewardsNumTable = {}
    local IndexData = 1
    for i, v in pairs(curAward) do
      local itemKey = tonumber(arg)
      local itemName = GameHelper:GetAwardNameText(v.item_type, v.number)
      local icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v, nil, GameObjectClimbTower.DynamicResDowloadFinished)
      DebugOut("itemName" .. itemName .. "icon: " .. icon)
      local count = GameUtils.numberConversion(GameHelper:GetAwardCount(v.item_type, v.number, v.no))
      rewardsNameTable[IndexData] = itemName
      rewardsIconTable[IndexData] = icon
      rewardsNumTable[IndexData] = count
      IndexData = IndexData + 1
    end
    DebugTable(rewardsNameTable)
    DebugTable(rewardsIconTable)
    DebugTable(rewardsNumTable)
    if #rewardsNameTable == 1 then
      rewardsNameTable[1] = rewardsNameTable[1] .. "\001"
    end
    if #rewardsIconTable == 1 then
      rewardsIconTable[1] = rewardsIconTable[1] .. "\001"
    end
    if #rewardsNumTable == 1 then
      rewardsNumTable[1] = rewardsNumTable[1] .. "\001"
    end
    local layerContext = GameLoader:GetGameText("LC_MENU_DUNGEON_WORM_RANK_LAYER_LABEL")
    layerContext = layerContext .. " " .. (rewardStep or itemKey)
    local receiveText = GameLoader:GetGameText("LC_MENU_CAPTION_RECEIVE")
    local receivedText = GameLoader:GetGameText("LC_MENU_CARNIVAL_CLAIMED_BUTTON")
    local unfinishText = GameLoader:GetGameText("LC_MENU_CARNIVAL_INCOMPLETE_BUTTON")
    self:GetFlashObject():InvokeASCallback("_root", "UpdateItem", itemKey, layerContext, table.concat(rewardsNameTable, "\001"), table.concat(rewardsIconTable, "\001"), table.concat(rewardsNumTable, "\001"), rewardStatus, receiveText, receivedText, unfinishText)
  elseif "UpdInsRankItem" == cmd then
    GameObjectClimbTower.ExpeditionRankList:UpdateListItem(tonumber(arg))
  elseif "EnterExpeditionShop" == cmd then
    local GameGoodsList = LuaObjectManager:GetLuaObject("GameGoodsList")
    GameGoodsList:SetEnterStoreType(GameGoodsList.STORE_TYPE_MYSTERY_EXPEDITION)
    GameStateManager:GetCurrentGameState():AddObject(GameGoodsList)
  elseif "showRankList" == cmd then
    GameObjectClimbTower.ExpeditionRankList:Show(tonumber(arg), false)
  elseif cmd == "needUpdateRewardBoxItem" then
    DebugOut("needUpdateRewardBoxItem")
    if not gainedReward or #gainedReward <= 0 then
      DebugOut("gainedReward not ok ")
      return
    else
      DebugTable(gainedReward)
    end
    local itemKey = tonumber(arg)
    local rewardName = GameHelper:GetAwardNameText(gainedReward[itemKey].item_type, gainedReward[itemKey].number)
    local rewardIcon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(gainedReward[itemKey], nil, GameObjectClimbTower.DynamicResDowloadFinished)
    local rewardNum = GameHelper:GetAwardCount(gainedReward[itemKey].item_type, gainedReward[itemKey].number, gainedReward[itemKey].no)
    self:GetFlashObject():InvokeASCallback("_root", "SetGainedRewardItem", itemKey, rewardName, rewardIcon, rewardNum)
  elseif cmd == "clickBoss" then
    if waitForAnimation then
      return
    end
    if isKilledFinalBoss then
      GameTip:Show(GameLoader:GetGameText("LC_ALERT_ladder_search_on_max"))
      return
    end
    DebugOut("GameObjectClimbTower onFSCommand clickBoss")
    DebugTable(currentStepInfo.matrix)
    if 0 >= currentStepInfo.search_num then
      if self:IsDungeon() then
        DebugOut("ladder_search_cost")
        GameObjectClimbTower:GetCostCredit("ladder_search_cost")
      elseif self:IsWormHole() then
        DebugOut("wormhole_search_cost")
        GameObjectClimbTower:GetCostCredit("wormhole_search_cost")
      elseif self:IsExpedition() then
        DebugOut("expedition_search_cost")
        GameObjectClimbTower:GetCostCredit("expedition_search_cost")
      end
      return
    end
    local GameStateFormation = GameStateManager.GameStateFormation
    GameStateFormation:SetBattleID(-1, -1)
    local monsterName = GameLoader:GetGameText("LC_NPC_NPC_" .. currentStepInfo.monster_name)
    local monsterAvatar = self:GetMonsterHeadIconFromMatrix(self:GetMultiMatrixLastSection(currentStepInfo.matrix), currentStepInfo.captain_index)
    GameStateFormation:SetEnemyInfo(monsterName, monsterAvatar, currentStepInfo.force)
    GameStateFormation:SetFleetsBuffer(currentStepInfo.buffs)
    GameStateFormation.isNeedRequestMatrixInfo = false
    local lastSection = self:GetMultiMatrixLastSection(currentStepInfo.matrix)
    GameStateFormation.enemyMatrixData = lastSection
    GameStateManager:SetCurrentGameState(GameStateFormation)
  elseif cmd == "onUpdateRankItem" then
    self:UpdateToprankItem(tonumber(arg))
  elseif cmd == "gainReward" then
    DebugOut("fscommand btn_confirm")
    local requestParam = {
      type = GameObjectClimbTower.currentInstanceId
    }
    NetMessageMgr:SendMsg(NetAPIList.ladder_gain_req.Code, requestParam, GameObjectClimbTower.CommonNetMessageCallBack, true, nil)
    waitForClaimReward = false
    gainedReward = nil
    currentStepInfo.has_rewards = nil
    GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "lua2fs_setRushState", rushContext, 2, nil)
  elseif cmd == "nextAnimOver" or cmd == "prevAnimOver" then
    DebugOut("nextAnimOver")
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_slideOver")
    self:GetFlashObject():InvokeASCallback("_root", "updateVisibleInstanceStateToMC")
  elseif cmd == "Close_Window" then
    DebugOut("kkskdakskkaskd:", isShowSelectGate, GameStateManager.GameStateInstance.m_preState)
    if isShowSelectGate then
      self:clearLocalData()
      GameStateManager.GameStateLab.mCurSubState = STATE_LAB_SUB_STATE.SUBSTATE_NORMAL
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateLab)
    else
      self:clearLocalData()
      if GameStateManager.GameStateInstance.m_preState == GameStateManager.GameStateBattleMap then
        GameStateManager.GameStateBattleMap:ReEnter(nil)
      else
        GameStateManager:SetCurrentGameState(GameStateManager.GameStateInstance.m_preState)
      end
    end
  elseif cmd == "Close_subWindow" then
    DebugOut("currentFlashLayer " .. currentFlashLayer)
    if currentFlashLayer == 1 then
      hasInitRankList = false
      currentFlashLayer = 4
    elseif currentFlashLayer == 2 then
      hasInitRewardslist = false
      currentFlashLayer = 4
    elseif currentFlashLayer == 3 then
      hasInitRewardBox = false
      currentFlashLayer = 4
      currentStepInfo.has_rewards = nil
    elseif currentFlashLayer == 4 then
      if isShowSelectGate then
        self:QuitToGateSelect()
        self:GetFlashObject():InvokeASCallback("_root", "updateVisibleInstanceStateToMC")
        currentFlashLayer = 0
      else
        self:clearLocalData()
        if isShowSelectGate then
          GameStateManager:SetCurrentGameState(GameStateManager.GameStateLab)
        else
          GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
        end
      end
    elseif currentFlashLayer == 0 then
      self:clearLocalData()
      if isShowSelectGate then
        GameStateManager:SetCurrentGameState(GameStateManager.GameStateLab)
      else
        GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
      end
    end
  elseif cmd == "Enter_Instance" then
    local active_id = tonumber(arg)
    self:RequestInstanceData(active_id)
    self.category = self:getCategoryBySubCategory(active_id)
    GameStateInstance.category = self.category
  elseif cmd == "btn_instance_rank_list" then
    GameObjectClimbTower.ExpeditionRankList:Show(1, true)
  elseif cmd == "btn_reset" then
    if waitForAnimation then
      return
    end
    local function callback()
      local requestParam = {
        type = GameObjectClimbTower.currentInstanceId
      }
      NetMessageMgr:SendMsg(NetAPIList.ladder_reset_req.Code, requestParam, GameObjectClimbTower.ResetCallBack, true, nil)
    end
    local resetToStep = 1
    if GameObjectClimbTower:IsDungeon() then
      resetToStep = currentStepInfo.cur_max_step - currentStepInfo.radis_step
    end
    resetToStep = math.max(1, resetToStep)
    local info = string.format(GameLoader:GetGameText("LC_MENU_NEW_SPACE_BACK_INFO"), resetToStep)
    if GameObjectClimbTower:IsExpedition() then
      info = GameLoader:GetGameText("LC_MENU_EXPEDITION_RESET_ALERT")
    end
    local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
    GameUIMessageDialog:SetYesButton(callback)
    GameUIMessageDialog:Display(text_title, info)
  elseif cmd == "btn_rush" then
    if waitForAnimation then
      return
    end
    if waitForClaimReward then
      GameObjectClimbTower:ShowGainedRewardBox()
      return
    end
    if not isStartRush then
      gainedReward = nil
      local requestParam = {
        type = GameObjectClimbTower.currentInstanceId
      }
      NetMessageMgr:SendMsg(NetAPIList.ladder_raids_req.Code, requestParam, GameObjectClimbTower.RushRequestCallBack, true, nil)
    elseif isStartRush and rushEndTime >= 0 then
      local cancel = GameLoader:GetGameText("LC_MENU_BUTTON_CAPTION_CANCEL")
      local affairs = GameLoader:GetGameText("LC_MENU_GET_MORE_BUTTON")
      GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
      GameUIMessageDialog:SetRightTextButton(cancel)
      GameUIMessageDialog:SetLeftTextButton(affairs)
      GameUIMessageDialog:SetYesButton(GameObjectClimbTower.stopRushNow, nil)
      DebugOut(GameLoader:GetGameText("LC_MENU_DUNGEON_STOP_RAID_ALERT"))
      GameUIMessageDialog:Display(GameLoader:GetGameText("LC_MENU_TITLE_VERIFY"), GameLoader:GetGameText("LC_MENU_DUNGEON_STOP_RAID_ALERT"))
    end
  elseif cmd == "btn_rank" then
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_gotoSubPage", 1)
    currentFlashLayer = 1
    local requestParam = {
      type = GameObjectClimbTower.currentInstanceId
    }
    NetMessageMgr:SendMsg(NetAPIList.ladder_rank_req.Code, requestParam, GameObjectClimbTower.GetRankCallBack, true, nil)
    GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "initRankFlashObject")
    hasInitRankList = true
  elseif cmd == "btn_reward" then
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_gotoSubPage", 2)
    currentFlashLayer = 2
    local requestParam = {
      type = GameObjectClimbTower.currentInstanceId
    }
    NetMessageMgr:SendMsg(NetAPIList.ladder_special_req.Code, requestParam, GameObjectClimbTower.GetSpecialAwardsCallBack, true, nil)
    hasInitRewardslist = true
  elseif cmd == "exchangeBossComplete" then
    DebugOut("exchangeBossComplete")
    GameObjectClimbTower:setLadderInfo(GameObjectClimbTower.nextLadder)
    GameObjectClimbTower:SetUpUI()
    waitForAnimation = false
  elseif cmd == "helpClicked" then
    local activity_id = tonumber(arg)
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    if GameObjectClimbTower.category == GameUIActivityNew.category.TYPE_DUNGEON then
      local desc = self:getContentBySubCategory(activity_id)
      GameUIWDStuff:SetHelpText(desc)
    elseif GameObjectClimbTower.category == GameUIActivityNew.category.TYPE_WORM_HOLE then
      local desc = self:getContentBySubCategory(activity_id)
      GameUIWDStuff:SetHelpText(desc)
    elseif GameObjectClimbTower.category == GameUIActivityNew.category.TYPE_EXPEDITION then
      local desc = self:getContentBySubCategory(activity_id)
      GameUIWDStuff:SetHelpText(desc)
    end
  elseif cmd == "awardItemReleased" then
    local awardItemIndex, awardItemSubIndex = arg:match("(%d+)\001(%d+)")
    DebugOut("item index = awardItemIndex, awardItemSubItemIndex", awardItemIndex, awardItemSubIndex)
    local clickedItem = specialAwards[tonumber(awardItemIndex)].rewards
    DebugTable(clickedItem)
    local clickedSubItem = clickedItem[tonumber(awardItemSubIndex)]
    DebugTable(clickedSubItem)
    self:showItemDetil(clickedSubItem, clickedSubItem.item_type)
  elseif cmd == "openShop" then
    GameGoodsList:SetEnterStoreType(2)
    GameStateManager:SetCurrentGameState(GameStateStore)
  elseif cmd == "showReport" then
    GameObjectClimbTower:ShowBattleReport(arg)
    if currentFlashLayer == 6 then
      currentFlashLayer = 4
      if self:GetFlashObject() then
        self:GetFlashObject():InvokeASCallback("_root", "closePopReplay")
      end
    end
  elseif cmd == "replayHistoryClicked" then
    GameObjectClimbTower:ShowBattleHistory()
  elseif cmd == "openJumpPanle" then
    if not isStartRush then
      GameObjectClimbTower:ShowJumpPortal()
    else
      GameTip:Show(GameLoader:GetGameText("LC_MENU_CAN_NOT_USE_PORTAL"))
    end
  elseif cmd == "JumpToStep" then
    DebugOut("currentFlashLayer = " .. currentFlashLayer)
    GameObjectClimbTower:JumpToStep(tonumber(arg))
    if currentFlashLayer == 5 then
      currentFlashLayer = 4
      if self:GetFlashObject() then
        self:GetFlashObject():InvokeASCallback("_root", "closePopJump")
      end
    end
  elseif cmd == "rewardCollectReleased" then
    GameObjectClimbTower:GetRewards(tonumber(arg))
  elseif cmd == "closePopJump" then
    DebugOut("currentFlashLayer = " .. currentFlashLayer)
    if currentFlashLayer == 5 then
      currentFlashLayer = 4
      if self:GetFlashObject() then
        self:GetFlashObject():InvokeASCallback("_root", "closePopJump")
      end
    end
  elseif cmd == "closePopReplay" then
    DebugOut("currentFlashLayer = " .. currentFlashLayer)
    if currentFlashLayer == 6 then
      currentFlashLayer = 4
      if self:GetFlashObject() then
        self:GetFlashObject():InvokeASCallback("_root", "closePopReplay")
      end
    end
  elseif "TreasureBoxClicked" == cmd then
    self:ShowTreasureBoxDetail(tonumber(arg))
  elseif cmd == "updateBoxAwardItem" then
    self:UpdateBoxAwardItem(tonumber(arg))
  elseif cmd == "ShowBoxAwardItemDetail" then
    self:ShowBoxAwardItemDetail(tonumber(arg))
  elseif cmd == "getBoxReward" then
    self:getBoxReward(tonumber(arg))
  end
  GameObjectClimbTower.ExpeditionRankReward:OnFSCommand(cmd, arg)
  GameTextEdit:fscommand(self:GetFlashObject(), cmd, arg)
end
function GameObjectClimbTower:GetMonsterHeadIconFromMatrix(fleets_matrix, id)
  DebugOut("GetMonsterHeadIconFromMatrix " .. id)
  if fleets_matrix[id] ~= nil then
    fleet = fleets_matrix[id]
    local headicon = fleet.avatar
    if headicon and GameDataAccessHelper:IsHeadResNeedDownload(headicon) then
      if GameDataAccessHelper:CheckFleetHasAvataImage(headicon) then
        return headicon
      else
        return "head9001"
      end
    end
    return headicon
  else
    return ""
  end
end
function GameObjectClimbTower:checkDynamicItem(award)
  DebugTable(award)
  local frame = ""
  if DynamicResDownloader:IsDynamicStuffByGameItem(award) then
    DebugOut("IsDynamicStuffByGameItem = true ")
    local fullFileName = DynamicResDownloader:GetFullName(award.number .. ".png", DynamicResDownloader.resType.PIC)
    local localPath = "data2/" .. fullFileName
    if DynamicResDownloader:IfResExsit(localPath) then
      DebugOut("IfResExsit = true")
      ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
      frame = "item_" .. award.number
    else
      frame = "temp"
      local itemID = award.number
      local resName = itemID .. ".png"
      local extInfo = {}
      extInfo.item_id = itemID
      extInfo.localPath = "data2/LAZY_LOAD_dynamic_" .. resName
      DynamicResDownloader:AddDynamicRes(resName, DynamicResDownloader.resType.PIC, extInfo, nil)
    end
  elseif award.item_type == "fleet" then
    frame = GameDataAccessHelper:GetFleetAvatar(award.number, 0)
  else
    frame = GameHelper:GetAwardTypeIconFrameName(award.item_type, award.number, award.no)
  end
  DebugOut("frame", frame)
  return frame
end
function GameObjectClimbTower:showItemDetil(item_, item_type)
  if GameHelper:IsResource(item_type) then
    return
  end
  local item = item_
  if item_type == "krypton" then
    if item.level == 0 then
      item.level = 1
    end
    local detail = GameUIKrypton:TryQueryKryptonDetail(item.number, item.level, function(msgType, content)
      local ret = GameUIKrypton.QueryKryptonDetailCallBack(msgType, content)
      if ret then
        local tmp = GameGlobalData:GetKryptonDetail(item.number, item.level)
        if tmp == nil then
          return false
        end
        ItemBox:SetKryptonBox(tmp, item.number)
        local operationTable = {}
        operationTable.btnUnloadVisible = false
        operationTable.btnUpgradeVisible = false
        operationTable.btnUseVisible = false
        operationTable.btnDecomposeVisible = false
        ItemBox:ShowKryptonBox(320, 240, operationTable)
      end
      return ret
    end)
    if detail == nil then
    else
      ItemBox:SetKryptonBox(detail, item.number)
      local operationTable = {}
      operationTable.btnUnloadVisible = false
      operationTable.btnUpgradeVisible = false
      operationTable.btnUseVisible = false
      operationTable.btnDecomposeVisible = false
      ItemBox:ShowKryptonBox(320, 240, operationTable)
    end
  end
  if item_type == "item" then
    item.cnt = 1
    ItemBox:showItemBox("Item", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
  end
  if item_type == "fleet" then
    ItemBox:ShowCommanderDetail2(tonumber(item.number))
  end
end
function GameObjectClimbTower.DynamicResDowloadFinished()
  DebugOut("DynamicResDowloadFinished", checkForBattleResult)
  GameObjectClimbTower:setAwardsInfo(rewardlist)
end
function GameObjectClimbTower:SetUpUI()
  if self:IsDungeon() then
    local lootList = {}
    for i, v in pairs(currentStepInfo.ratio_award) do
      local loot = {}
      loot.itemName = GameHelper:GetAwardNameText(v.item_type, v.number)
      loot.lootIcon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v, nil, GameObjectClimbTower.DynamicResDowloadFinished)
      loot.lootCount = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
      table.insert(lootList, loot)
    end
    if self:GetFlashObject() then
      self:GetFlashObject():InvokeASCallback("_root", "SetUpUIForDungeon", currentStepInfo.cur_step, currentStepInfo.cur_max_step - currentStepInfo.reset_radis_step, lootList, currentStepInfo.can_transmit, currentStepInfo.reset_num)
      self:GetFlashObject():SetText("_root.climbtower.tips_board.LC_MENU_DUNGEON_SPACE_EXPLORE_TIME_LABEL", GameLoader:GetGameText("LC_MENU_DUNGEON_SPACE_EXPLORE_TIME_LABEL"))
    end
  elseif self:IsWormHole() then
    if self:GetFlashObject() then
      self:GetFlashObject():InvokeASCallback("_root", "SetUpUIForWormHole")
      self:GetFlashObject():SetText("_root.climbtower.tips_board.LC_MENU_DUNGEON_SPACE_EXPLORE_TIME_LABEL", GameLoader:GetGameText("LC_MENU_DUNGEON_SPACE_EXPLORE_TIME_LABEL"))
    end
  elseif self:IsExpedition() and self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "SetUpUIForExpedition")
    self:GetFlashObject():SetText("_root.climbtower.tips_board.LC_MENU_DUNGEON_SPACE_EXPLORE_TIME_LABEL", GameLoader:GetGameText("LC_MENU_EXPEDITION_CHALLENGE_TIMES"))
  end
  if not GameObjectClimbTower:isShowRushButton() then
    GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "isShowRushButton", false)
  end
end
function GameObjectClimbTower:ShowBattleHistory()
  local requestParam = {}
  requestParam.step = currentStepInfo.cur_step
  NetMessageMgr:SendMsg(NetAPIList.ladder_report_req.Code, requestParam, GameObjectClimbTower.GetBattleHistoryCallback, true, nil)
end
function GameObjectClimbTower.GetBattleHistoryCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.ladder_report_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.ladder_report_ack.Code then
    if GameObjectClimbTower:GetFlashObject() then
      currentFlashLayer = 6
      GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "ShowBattleHistory", content)
    end
    return true
  end
  return false
end
function GameObjectClimbTower:ShowBattleReport(battleId)
  local requestParam = {}
  requestParam.id = battleId
  NetMessageMgr:SendMsg(NetAPIList.ladder_report_detail_req.Code, requestParam, GameObjectClimbTower.ShowBattleReportCallback, true, nil)
end
function GameObjectClimbTower.ShowBattleReportCallback(msgType, content)
  if msgType == NetAPIList.ladder_report_detail_ack.Code then
    if content and content.report then
      GameStateBattlePlay.curBattleType = "climbtower"
      GameStateManager.GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_HISTORY, content.report, nil, nil)
      GameStateBattlePlay:RegisterOverCallback(function()
        GameStateInstance:EnterInstance()
      end, nil)
      GameStateManager:SetCurrentGameState(GameStateBattlePlay)
    else
      GameTip:Show(GameLoader:GetGameText("LC_MENU_REPLAY_NOT_EXIST"))
    end
    return true
  elseif msgType == NetAPIList.common_ack.Code and content and content.api == NetAPIList.ladder_report_detail_req.Code then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_REPLAY_NOT_EXIST"))
    return true
  end
  return false
end
function GameObjectClimbTower:ShowJumpPortal()
  local content = {}
  content.type = GameObjectClimbTower.currentInstanceId
  NetMessageMgr:SendMsg(NetAPIList.max_step_req.Code, content, GameObjectClimbTower.ShowJumpPortalCallback, true, nil)
end
function GameObjectClimbTower.ShowJumpPortalCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.max_step_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.max_step_ack.Code then
    if GameObjectClimbTower:GetFlashObject() then
      local hasRewardsText = string.format(GameLoader:GetGameText("LC_MENU_NEW_SPACE_JUMP_INFO_2"), content.award_step)
      DebugOut("hasRewardsText = ", hasRewardsText)
      hasRewardsText = string.format(hasRewardsText, content.award_step)
      DebugOut("hasRewardsText = ", hasRewardsText)
      currentFlashLayer = 5
      GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "ShowJumpPortal", content.max, hasRewardsText)
    end
    return true
  end
  return false
end
function GameObjectClimbTower:JumpToStep(step)
  local requestParam = {}
  requestParam.type = GameObjectClimbTower.currentInstanceId
  requestParam.step = tonumber(step)
  NetMessageMgr:SendMsg(NetAPIList.ladder_jump_req.Code, requestParam, GameObjectClimbTower.JumpToStepCallback, true, nil)
end
function GameObjectClimbTower.JumpToStepCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.ladder_jump_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.ladder_jump_ack.Code then
    gainedReward = nil
    if isStartRush then
      GameObjectClimbTower.stopRushNow()
    end
    if content.info.on_max_step and content.info.monster_hp == 0 then
      isKilledFinalBoss = true
    else
      isKilledFinalBoss = false
    end
    if content.info.cur_step ~= currentStepInfo.cur_step then
      GameObjectClimbTower:exchangeLadderInfo(content.info)
      GameObjectClimbTower.nextLadder = content.info
    else
      GameObjectClimbTower:setLadderInfo(content.info)
      GameObjectClimbTower:SetUpUI()
    end
    return true
  end
  return false
end
function GameObjectClimbTower:buildDataForRewardList(content)
  DebugOut("buildDataForAlwaysRewardList")
  specialAwards = {}
  notFinishedRwardslist = {}
  canReceiveRwardslist = {}
  receivedRwardslist = {}
  for i, v in ipairs(content) do
    if v.status == 1 then
      table.insert(notFinishedRwardslist, v)
    elseif v.status == 2 then
      table.insert(canReceiveRwardslist, v)
    elseif v.status == 3 then
      table.insert(receivedRwardslist, v)
    end
  end
  function sortRwardTable(a, b)
    return a.step < b.step
  end
  table.sort(notFinishedRwardslist, sortRwardTable)
  table.sort(canReceiveRwardslist, sortRwardTable)
  table.sort(receivedRwardslist, sortRwardTable)
  for i, v in ipairs(canReceiveRwardslist) do
    table.insert(specialAwards, v)
  end
  for i, v in ipairs(notFinishedRwardslist) do
    table.insert(specialAwards, v)
  end
  for i, v in ipairs(receivedRwardslist) do
    table.insert(specialAwards, v)
  end
  DebugOut("specialAwards")
  DebugTable(specialAwards)
  self:GetFlashObject():InvokeASCallback("_root", "initRewardListBox", #specialAwards)
end
function GameObjectClimbTower:GetRewards(itemKey)
  local requestParam = {}
  requestParam.type = GameObjectClimbTower.currentInstanceId
  requestParam.step = specialAwards[tonumber(itemKey)].step
  GameObjectClimbTower.currentRewardItemIndex = tonumber(itemKey)
  DebugOut("requestParam = ")
  DebugTable(requestParam)
  NetMessageMgr:SendMsg(NetAPIList.ladder_award_req.Code, requestParam, GameObjectClimbTower.GetRewardsCallBack, true, nil)
end
function GameObjectClimbTower.GetRewardsCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.ladder_award_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    else
      specialAwards[GameObjectClimbTower.currentRewardItemIndex].status = 3
      GameObjectClimbTower:OnFSCommand("needUpdateItem", GameObjectClimbTower.currentRewardItemIndex)
      local stillAwards = false
      for i, v in ipairs(specialAwards) do
        if v.status == 2 then
          stillAwards = true
        end
      end
      if not stillAwards then
        GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "updateInstanceNews", GameObjectClimbTower.currentInstanceId, 0)
      end
      currentStepInfo.can_award = stillAwards
      GameObjectClimbTower:SetRankRewardBtn(currentStepInfo.can_award)
    end
    GameObjectClimbTower.currentRewardStep = -1
    return true
  end
  return false
end
function GameObjectClimbTower:IsDungeon()
  return GameObjectClimbTower.category == GameUIActivityNew.category.TYPE_DUNGEON
end
function GameObjectClimbTower:IsWormHole()
  return GameObjectClimbTower.category == GameUIActivityNew.category.TYPE_WORM_HOLE
end
function GameObjectClimbTower:IsExpedition()
  return GameObjectClimbTower.category == GameUIActivityNew.category.TYPE_EXPEDITION
end
function GameObjectClimbTower:SetSearchTimes(searchTimes)
  if not GameObjectClimbTower:GetFlashObject() then
    return
  end
  if self:IsDungeon() or self:IsWormHole() then
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setEnergy", searchTimes)
  elseif self:IsExpedition() then
    self:GetFlashObject():InvokeASCallback("_root", "setEnergy_exp", searchTimes)
  end
end
function GameObjectClimbTower:SetResetTimes(resetTimes)
  if not GameObjectClimbTower:GetFlashObject() then
    return
  end
  DebugOut("resetTimes = ", resetTimes)
  if self:IsDungeon() then
    GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "lua2fs_setResetTimes_Dungeon", resetTimes)
  elseif self:IsWormHole() then
    GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "lua2fs_setResetTimes_WormHole", resetTimes)
  elseif self:IsExpedition() then
    GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "lua2fs_setResetTimes_Expedition", resetTimes)
  end
end
function GameObjectClimbTower:SetRankRewardBtn(canReward)
  if not GameObjectClimbTower:GetFlashObject() then
    return
  end
  if self:IsDungeon() or self:IsWormHole() then
    GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "setRewardBtnState", canReward)
  elseif self:IsExpedition() then
    GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "setRankRewardBtnState_exp", false)
  end
end
GameObjectClimbTower.SelectedBoxDetail = nil
function GameObjectClimbTower:ShowTreasureBoxDetail(boxIndex)
  SelectedBoxDetail = GameObjectClimbTower.milestoneRewards_exp.progress_awards[boxIndex]
  self:GetFlashObject():InvokeASCallback("_root", "InitBoxAwardsList", #SelectedBoxDetail.awards)
end
function GameObjectClimbTower:UpdateBoxAwardItem(index)
  local awardsList = SelectedBoxDetail.awards[index]
  local tmp_type = awardsList.item_type
  local tmp_number = awardsList.number
  local no = awardsList.no
  no = no or 1
  local isItem = awardsList.item_type == "item"
  tmp_type, tmp_number = self:getAwardItemDetailInfo(tmp_type, tmp_number, no)
  local theitem = awardsList
  tmp_type = GameHelper:GetAwardTypeIconFrameName(theitem.item_type, theitem.number)
  local nquality, iconpic
  if theitem.item_type == "medal_item" or theitem.item_type == "medal" then
    nquality = GameHelper:GetMedalQuality(theitem.item_type, theitem.number)
    iconpic = GameHelper:GetGameItemDownloadPng(theitem.item_type, theitem.number)
    isItem = true
  end
  self:GetFlashObject():InvokeASCallback("_root", "SetBoxAwardsListItem", index, tmp_type, tmp_number, isItem, no, iconpic)
end
function GameObjectClimbTower:getAwardItemDetailInfo(itemType, number, no)
  local itemType = itemType
  local rewards = tonumber(number)
  no = no or 1
  if itemType == "fleet" then
    itemType = GameDataAccessHelper:GetFleetAvatar(rewards)
    rewards = GameLoader:GetGameText("LC_NPC_" .. GameDataAccessHelper:GetCommanderName(rewards))
  elseif itemType == "item" or itemType == "krypton" then
    local isitem = itemType == "item"
    if DynamicResDownloader:IsDynamicStuff(rewards, DynamicResDownloader.resType.PIC) then
      local fullFileName = DynamicResDownloader:GetFullName(rewards .. ".png", DynamicResDownloader.resType.PIC)
      local localPath = "data2/" .. fullFileName
      if DynamicResDownloader:IfResExsit(localPath) then
        ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
        itemType = "item_" .. rewards
      else
        itemType = "temp"
        self:AddDownloadPath(rewards, 0, 0)
      end
    else
      itemType = "item_" .. rewards
    end
    if isitem then
      rewards = GameLoader:GetGameText("LC_MENU_ITEM_CHAR")
    else
      rewards = GameLoader:GetGameText("LC_MENU_KRYPTON_TAB")
    end
  elseif itemType == "pve_supply" then
    rewards = GameLoader:GetGameText("LC_MENU_SUPPLY_CHAR")
  else
    rewards = GameUtils.numberConversion(rewards)
  end
  return itemType, rewards
end
function GameObjectClimbTower:AddDownloadPath(itemID, itemKey, index)
  DebugOut("Add download path", itemID, itemKey, index)
  local resName = itemID .. ".png"
  local extInfo = {}
  extInfo.itemKey = itemKey
  extInfo.index = index
  extInfo.item_id = itemID
  extInfo.resPath = "data2/LAZY_LOAD_dynamic_" .. resName
  DynamicResDownloader:AddDynamicRes(resName, DynamicResDownloader.resType.PIC, extInfo, nil)
end
function GameObjectClimbTower:getBoxReward(step)
  param = {}
  param.process_num = step
  DebugOut("GameObjectClimbTower:getBoxReward")
  NetMessageMgr:SendMsg(NetAPIList.expedition_wormhole_award_req.Code, param, getBoxRewardCallback, true)
end
function GameObjectClimbTower:getBoxRewardCallback(msgType, content)
  DebugOut("GameObjectClimbTower:getBoxRewardCallback--" .. msgType)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.expedition_wormhole_award_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.expedition_wormhole_status_ack.Code then
    if GameObjectClimbTower:IsExpedition() and content.all_tasks ~= nil then
      GameObjectClimbTower:SetMilestoneRewardInfo_exp(content.all_tasks)
    end
    return true
  end
  return false
end
function GameObjectClimbTower:ShowBoxAwardItemDetail(index)
  local award = SelectedBoxDetail.awards[index]
  if award then
    self:showItemDetil(award, award.item_type)
  end
end
function GameObjectClimbTower:showItemDetil(item_, item_type)
  local item = item_
  if item_type == "krypton" then
    if item.level == 0 then
      item.level = 1
    end
    local detail = GameUIKrypton:TryQueryKryptonDetail(item.number, item.level, function(msgType, content)
      local ret = GameUIKrypton.QueryKryptonDetailCallBack(msgType, content)
      if ret then
        local tmp = GameGlobalData:GetKryptonDetail(item.number, item.level)
        if tmp == nil then
          return false
        end
        ItemBox:SetKryptonBox(tmp, item.number)
        local operationTable = {}
        operationTable.btnUnloadVisible = false
        operationTable.btnUpgradeVisible = false
        operationTable.btnUseVisible = false
        operationTable.btnDecomposeVisible = false
        ItemBox:ShowKryptonBox(320, 1080, operationTable)
      end
      return ret
    end)
    if detail == nil then
    else
      ItemBox:SetKryptonBox(detail, item.number)
      local operationTable = {}
      operationTable.btnUnloadVisible = false
      operationTable.btnUpgradeVisible = false
      operationTable.btnUseVisible = false
      operationTable.btnDecomposeVisible = false
      ItemBox:ShowKryptonBox(320, 1080, operationTable)
    end
  end
  if item_type == "fleet" then
    GameUIFirstCharge:ShowCommanderInfo(tonumber(item.number))
  end
  if item_type == "item" then
    item.cnt = 1
    ItemBox:showItemBox("ChoosableItem", item, tonumber(item.number), 320, 1080, 200, 200, "", nil, "", nil)
  end
  if item_type == "medal_item" then
    item.quality = item.quality or 1
    ItemBox:showItemBox("Medal", item, item.number, 320, 480, 200, 200, "", nil, "", nil)
  end
  if item_type == "medal" then
    ItemBox:ShowMedalDetail(item.number)
  end
end
GameObjectClimbTower.milestoneRewards_exp = nil
function GameObjectClimbTower.MilestoneReward_ExpNtf(content)
  GameObjectClimbTower.milestoneRewards_exp = content
  DebugOutPutTable(content, "GameObjectClimbTower.MilestoneReward_ExpNtf====")
  if GameObjectClimbTower:GetFlashObject() and GameObjectClimbTower:IsExpedition() then
    GameObjectClimbTower:SetMilestoneRewardInfo_exp(content)
  end
end
function GameObjectClimbTower:SetMilestoneRewardInfo_exp(content)
  GameObjectClimbTower.milestoneRewards_exp = content
  GameObjectClimbTower:GetFlashObject():InvokeASCallback("_root", "setMilestoneReward_exp", content)
end
GameObjectClimbTower.leaderboard_exp = nil
function GameObjectClimbTower.LeaderBoard_ExpNtf(content)
  GameObjectClimbTower.leaderboard_exp = content
  DebugOutPutTable(content, "GameObjectClimbTower.LeaderBoard_ExpNtf====")
  if GameObjectClimbTower:GetFlashObject() and GameObjectClimbTower:IsExpedition() then
    GameObjectClimbTower:SetLeaderboardInfo_exp(content)
  end
end
function GameObjectClimbTower:SetLeaderboardInfo_exp(content)
  self:GetFlashObject():InvokeASCallback("_root", "SetLeaderboardInfo_exp", content)
end
function GameObjectClimbTower:GetMultiMatrixLastSection(matrixData)
  if matrixData == nil then
    return nil
  end
  local sections = {}
  local n = 1
  for _, data_monster in pairs(matrixData) do
    n = math.floor(data_monster.cell_id / 10) + 1
    if sections[n] == nil then
      sections[n] = {}
    end
    local v = LuaUtils:table_copy(data_monster)
    v.cell_id = data_monster.cell_id % 10
    table.insert(sections[n], v)
  end
  return sections[n]
end
